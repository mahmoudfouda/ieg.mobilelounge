﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMS.DAL
{
	public class AirlineDBAdapter : DBConnection
	{
		protected AirlineDBAdapter (string path) : base (path)
		{
		}

		private static AirlineDBAdapter _current;
		public static AirlineDBAdapter Current {
			get{
				if (_current == null)
					_current = new AirlineDBAdapter (DatabaseFilePath);
				return _current;
			}
		} 

		public IEnumerable<Airline> GetAirlines () 
		{
			lock (DBConnection.locker) {
				return (from i in Table<Airline> () select i).ToList ();
			}
		}

		public Airline GetAirline (decimal id)
		{
			lock (DBConnection.locker) {
				return Table<Airline>().FirstOrDefault(x => x.ID == id);
			}
		}

		public decimal SaveAirline (Airline item) 
		{
			var airline = GetAirline (item.ID);
			lock (DBConnection.locker) {
				if (airline != null) {
					Update (item);
					return item.ID;
				} else {
					return Insert (item);
				}
			}
		}

        //public decimal SaveAirlineImage(Airline item)
        //{
        //    var airline = GetAirline(item.ID);
        //    lock (DBConnection.locker)
        //    {
        //        if (airline != null)
        //        {
        //            airline.AirlineLogoBytes = item.AirlineLogoBytes;
        //            Update(airline);
        //            return item.ID;
        //        }
        //        else
        //        {
        //            return 0;
        //        }
        //    }
        //}
        
        public int DeleteAirline(Airline item) 
		{
			lock (DBConnection.locker) {
				return Delete<Airline> (item.ID);
			}
		}
        
        public void DeleteAllAirlines()
        {
            var airlines = GetAirlines();
            lock (DBConnection.locker)
            {
                foreach (var item in airlines)
                {
                    Delete<Airline>(item.ID);
                }
            }
        }
    }
}