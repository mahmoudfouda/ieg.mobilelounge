﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AIMS.DAL
{
	public class PassengerDBAdapter : DBConnection
	{
		protected PassengerDBAdapter (string path) : base (path)
		{
		}

		private static PassengerDBAdapter _current;
		public static PassengerDBAdapter Current {
			get{
				if (_current == null)
					_current = new PassengerDBAdapter (DatabaseFilePath);
				return _current;
			}
		}

		public IEnumerable<Passenger> GetAllPassengers () 
		{
			lock (DBConnection.locker) {
                var hourAgo = DateTime.Now.AddHours(-1);
                var expiredRecords = (from i in Table<Passenger>() where i.TrackingTimestamp < hourAgo select i).ToList();
                foreach (var record in expiredRecords)
                    Delete<Passenger>(record.Id);
                
                return (from i in Table<Passenger> () select i).OrderByDescending(x => x.TrackingTimestamp).ToList();
			}
		}

		public Passenger GetPassenger (int id)
		{
			lock (DBConnection.locker) {
				return Table<Passenger>().FirstOrDefault(x => x.Id == id);
			}
		}

        public Passenger GetPassengerByTrackingRecordId(decimal id)
        {
            lock (DBConnection.locker)
            {
                return Table<Passenger>().FirstOrDefault(x => x.TrackingRecordID == id);
            }
        }

        public decimal SavePassenger (Passenger item) 
		{
			var passenger = GetPassenger (item.Id);
			lock (DBConnection.locker) {
				if (passenger != null) {
					Update (item);
					return item.Id;
				} else {
					return Insert (item);
				}
			}
		}

		public int DeletePassenger(Passenger item) 
		{
			lock (DBConnection.locker) {
				return Delete<Passenger> (item.Id);
			}
		}
	}
}

