﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.DAL
{
    public class ImageDBAdapter : DBConnection
    {
        protected ImageDBAdapter(string path) : base(path)
        {
        }

        private static ImageDBAdapter _current;
        public static ImageDBAdapter Current
        {
            get
            {
                if (_current == null)
                    _current = new ImageDBAdapter(DatabaseFilePath);
                return _current;
            }
        }

        public IEnumerable<Image> GetAllImages()
        {
            lock (DBConnection.locker)
            {
                return (from i in Table<Image>() select i).ToList();
            }
        }
        
        public Image GetImage(string imageHandle)
        {
            lock (DBConnection.locker)
            {
                return Table<Image>().FirstOrDefault(x => x.ImageHandle == imageHandle);
            }
        }

        public int SaveImage(Image item)
        {
            var image = GetImage(item.ImageHandle);
            lock (DBConnection.locker)
            {
                if (image != null)
                {
                    Update(item);
                    return 1;
                }
                else
                {
                    return Insert(item);
                }
            }
        }
        
        public int DeleteImage(Image item)
        {
            lock (DBConnection.locker)
            {
                return Delete<Image>(item.ImageHandle);
            }
        }

        public void DeleteAllImages()
        {
            var images = GetAllImages();
            lock (DBConnection.locker)
            {
                foreach (var item in images)
                {
                    Delete<Image>(item.ImageHandle);
                }
            }
        }
    }
}
