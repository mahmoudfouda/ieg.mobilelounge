﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMS.DAL
{
    public class AirportDBAdapter : DBConnection
    {
        protected AirportDBAdapter(string path) : base (path)
		{
        }

        private static AirportDBAdapter _current;
        public static AirportDBAdapter Current
        {
            get
            {
                if (_current == null)
                    _current = new AirportDBAdapter(DatabaseFilePath);
                return _current;
            }
        }
        
        public IEnumerable<Airport> GetAirports()
        {
            lock (DBConnection.locker)
            {
                return (from i in Table<Airport>() select i).ToList();
            }
        }
        
        public Airport GetAirport(long id)
        {
            lock (DBConnection.locker)
            {
                return Table<Airport>().FirstOrDefault(x => x.Id == id);
            }
        }

        public long SaveAirport(Airport item)
        {
            var airport = GetAirport(item.Id);
            lock (DBConnection.locker)
            {
                if (airport != null)
                {
                    Update(item);
                    return item.Id;
                }
                else
                {
                    return Insert(item);
                }
            }
        }

        public void SaveAirports(List<Airport> items)//TODO: remove local repo (not a good idea)
        {
            if (items == null) return;

            lock (DBConnection.locker)
            {
                var updating = (from i in Table<Airport>() join a in items on i.Id equals a.Id select i).ToList();
                var adding = items.Where(x => !updating.Any(y=>y.Id == x.Id)).Except(updating).ToList();

                foreach (var item in updating)
                {
                    Update(item);
                }

                foreach (var item in adding)
                {
                    Insert(item);
                }
            }
        }

        public int DeleteAirport(Airport item)
        {
            lock (DBConnection.locker)
            {
                return Delete<Airport>(item.Id);
            }
        }

        public void DeleteAllAirports()
        {
            var airports = GetAirports();
            lock (DBConnection.locker)
            {
                foreach (var item in airports)
                {
                    Delete<Airport>(item.Id);
                }
            }
        }
    }
}
