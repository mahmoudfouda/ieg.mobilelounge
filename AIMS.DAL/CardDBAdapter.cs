﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMS.DAL
{
	public class CardDBAdapter : DBConnection
	{
		protected CardDBAdapter (string path) : base (path)
		{
		}

		private static CardDBAdapter _current;
		public static CardDBAdapter Current {
			get{
				if (_current == null)
					_current = new CardDBAdapter (DatabaseFilePath);
				return _current;
			}
		}
         
		public IEnumerable<Card> GetAllCards () 
		{
			lock (DBConnection.locker) {
				return (from i in Table<Card> () select i).ToList ();
			}
		}

		public IEnumerable<Card> GetAirlineCards (decimal airlineId) 
		{
			lock (DBConnection.locker) {
				return (from i in Table<Card> () where i.AirlineID==airlineId select i).ToList ();
			}
		}

		public Card GetCard (decimal id)
		{
			lock (DBConnection.locker) {
				return Table<Card>().FirstOrDefault(x => x.ID == id);
			}
		}

		public decimal SaveCard(Card item) 
		{
			var card = GetCard (item.ID);
			lock (DBConnection.locker) {
				if (card != null) {
					Update (item);
					return item.ID;
				} else {
					return Insert (item);
				}
			}
		}

        //public decimal SaveCardImage(Card item)
        //{
        //    var card = GetCard(item.ID);
        //    lock (DBConnection.locker)
        //    {
        //        if (card != null)
        //        {
        //            card.CardPictureBytes = item.CardPictureBytes;
        //            Update(card);
        //            return item.ID;
        //        }
        //        else
        //        {
        //            return 0;
        //        }
        //    }
        //}

        public int DeleteCard(Card item) 
		{
			lock (DBConnection.locker) {
				return Delete<Card> (item.ID);
			}
		}

        public void DeleteAllCards()
        {
            var cards = GetAllCards();
            lock (DBConnection.locker)
            {
                foreach (var item in cards)
                {
                    Delete<Card>(item.ID);
                }
            }
        }
    }
}

