﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMS.DAL
{
	public class WorkstationDBAdapter : DBConnection
	{
		protected WorkstationDBAdapter (string path) : base (path)
		{
		}

		private static WorkstationDBAdapter _current;
		public static WorkstationDBAdapter Current {
			get{
				if (_current == null)
					_current = new WorkstationDBAdapter (DatabaseFilePath);
				return _current;
			}
		} 

		public IEnumerable<Workstation> GetWorkstations () 
		{
			lock (DBConnection.locker) {
				return (from i in Table<Workstation> () select i).ToList ();
			}
		}

		public Workstation GetWorkstation (decimal id)
		{
			lock (DBConnection.locker) {
				return Table<Workstation>().FirstOrDefault(x => x.WorkstationID == id);
			}
		}

		public decimal SaveWorkstation (Workstation item) 
		{
			var launge = GetWorkstation(item.WorkstationID);
			lock (DBConnection.locker) {
				if (launge != null) {
					Update (item);
					return item.WorkstationID;
				} else {
					return Insert (item);
				}
			}
		}

		public int DeleteWorkstation(Workstation item) 
		{
			lock (DBConnection.locker) {
				return Delete<Workstation> (item.WorkstationID);
			}
		}
	}
}