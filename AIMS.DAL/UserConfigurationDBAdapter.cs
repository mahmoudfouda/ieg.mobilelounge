﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.DAL
{
    public class UserConfigurationDBAdapter : DBConnection
    {
        public const string CameraConfigName = "DefaultCamera";
        public const string LanguageConfigName = "DefaultLanguage";
        public const string SessionTimeoutConfigName = "SessionTimeoutInterval";
        public const string SBIConfigName = "SucceededBarcodeInterval";
        public const string FBIConfigName = "FailedBarcodeInterval";
        public const string RefreshIntervalConfigName = "RefreshInterval";
        public const string HoursOccupancyHistoryConfigName = "HoursOccupancyHistory";

        protected UserConfigurationDBAdapter(string path) : base (path)
		{
        }

        private static UserConfigurationDBAdapter _current;
        public static UserConfigurationDBAdapter Current
        {
            get
            {
                if (_current == null)
                    _current = new UserConfigurationDBAdapter(DatabaseFilePath);
                return _current;
            }
        }

        public IEnumerable<MobileUserConfiguration> GetAllConfigurations()
        {
            lock (DBConnection.locker)
            {
                return (from i in Table<MobileUserConfiguration>() select i).ToList();
            }
        }
        
        public MobileUserConfiguration GetConfiguration(int id)
        {
            lock (DBConnection.locker)
            {
                return Table<MobileUserConfiguration>().FirstOrDefault(x => x.ID == id);
            }
        }

        public MobileUserConfiguration GetConfiguration(string name, string username = "")
        {
            lock (DBConnection.locker)
            {
                if(string.IsNullOrWhiteSpace(username))
                    return Table<MobileUserConfiguration>().FirstOrDefault(x => x.Name.Equals(name));
                return Table<MobileUserConfiguration>().FirstOrDefault(x => x.Name.Equals(name) && x.Username.Equals(username));
            }
        }

        public int SaveConfiguration(MobileUserConfiguration item)
        {
            var configuration = item.ID == 0 ? GetConfiguration(item.Name, item.Username) : GetConfiguration(item.ID);
            
            lock (DBConnection.locker)
            {
                if (configuration != null)
                {
                    configuration.Value = item.Value;
                    Update(configuration);
                    return configuration.ID;
                }
                else
                {
                    return Insert(item);
                }
            }
        }
        
        public int DeleteConfiguration(MobileUserConfiguration item)
        {
            lock (DBConnection.locker)
            {
                return Delete<MobileUserConfiguration>(item.ID);
            }
        }
    }
}
