﻿using System;
using SQLite;

namespace AIMS.DAL
{
    public class Message
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        public decimal BulletinId { get; set; }
        
        public DateTime Time { get; set; }

        [MaxLength(128)]
        public string Title { get; set; }

        [MaxLength(128)]
        public string From { get; set; }

        public decimal Type { get; set; }

        public string Body { get; set; }

        public string Handle { get; set; }

        public string Attachment { get; set; }
    }
}
