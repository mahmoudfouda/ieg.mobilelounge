﻿using SQLite;

namespace AIMS.DAL
{
	public class Card
	{
		[PrimaryKey, Column("_id")]
		public decimal ID {
			get;
			set;
		}
        
		private string _name;
		[MaxLength(128)]
		public string Name {
			get{
				return _name;
			}
			set{
				_name = value;
				if (_name.Length > 16)
					DisplayName = string.Format("{0}..",_name.Substring (0, 13).Trim());
				else
					DisplayName = _name;
			}
		}

		[Ignore]
		public string DisplayName {
			get;
			private set;
		}

		public string Currency {
			get;
			set;
		}

		public string AcceptedClass {
			get;
			set;
		}
        
        [Indexed, Column("_airlineid")]
		public decimal AirlineID {
			get;
			set;
		}

        //public string AirlineName {
        //	get;
        //	set;
        //}

        //public byte[] CardPictureBytes {
        //	get;
        //	set;
        //}
        public int ComplimentaryGuests
        {
            get;
            set;
        }

        public int FamilyGuests
        {
            get;
            set;
        }

        public int PaidGuests
        {
            get;
            set;
        }

        public string ImageHandle { get; set; }

        public string Description {
			get;
			set;
		}

		public int PictureResourceID {
			get;
			set;
		}
	}
}

