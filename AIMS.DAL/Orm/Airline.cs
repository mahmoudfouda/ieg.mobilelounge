﻿using System;
using SQLite;

namespace AIMS.DAL
{
	public class Airline
	{
		[PrimaryKey, Column("_id")]
		public decimal ID {
			get;
			set;
		}

		public string Code {
			get;
			set;
		}

		private string _name;
		public string Name {
			get{
				return _name;
			}
			set{
				_name = value;
				if (_name.Length > 16)
					DisplayName = string.Format("{0}..",_name.Substring (0, 13).Trim());
				else
					DisplayName = _name;
			}
		}
        
		[MaxLength(15)]
		public string DisplayName {
			get;
			set;
		}

        public string ImageHandle { get; set; }

        //public byte[] AirlineLogoBytes {
		//	get;
		//	set;
		//}

		public string Description {
			get;
			set;
		}

		public int LogoResourceID {
			get;
			set;
		}


	}
}

