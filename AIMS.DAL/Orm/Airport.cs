﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.DAL
{
    public class Airport
    {
        [PrimaryKey, Column("_id")]
        public long Id { get; set; }

        //[Unique, Column("_iata")]//TODO: Check with everybody (Why do we have duplications?)
        public string IATA_Code { get; set; }

        public string Name { get; set; }

        public string Continent { get; set; }

        public string Country { get; set; }

        public string Municipality { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
