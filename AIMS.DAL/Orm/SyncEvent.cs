﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.DAL
{
    public class SyncEvent
    {
        public SyncEvent()
        {
            SyncLocalDate = DateTime.Now;
        }

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        public int Scope { get; set; }

        public DateTime SyncLocalDate { get; private set; }

        public DateTime SyncServerDate { get; set; }
        
        public string Description { get; set; }
    }
}
