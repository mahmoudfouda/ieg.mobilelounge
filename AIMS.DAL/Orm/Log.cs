﻿using System;
using SQLite;

namespace AIMS.DAL
{
	public class Log {
		
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }

        [Unique]
        public string GlobalUniqueId { get; set; }

		private DateTime _time = DateTime.UtcNow;
		//[NotNull]
		public DateTime Time { 
			get{ return _time; }
			set{ _time = value; }
		}

        private DateTime _localTime = DateTime.Now;
        //[NotNull]
        public DateTime LocalTime
        {
            get { return _localTime; }
            set { _localTime = value; }
        }

        [MaxLength(50)]
        public string Username { get; set; }

        [MaxLength(10)]
        public string MobileKey { get; set; }

        [MaxLength(200)]
        public string DeviceId { get; set; }

        [MaxLength(128)]
		public string Title { get; set; }

        [MaxLength(128)]
        public string Section { get; set; }

        public int LogType { get; set;}

		public int LogLevel{ get; set;}

        #region Position
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Altitude { get; set; }

        public float Accuracy { get; set; }
        #endregion

        public string Description { get; set; }

        #region Convertors
        public override string ToString()
        {
            return string.Format("LocalTime:{0:MM/dd/yyyy HH:mm:ss fff}\tUtcTime:{1:MM/dd/yyyy HH:mm:ss fff}\tLogTitle:{2}\t(AppSection:{3})\t\t\t- [LogLevel:{4}\tLogType:{5}\tUser:{6}\tMobileKey:{7}\tDevice:{8}\tPosition:{9}, {10}\t({11})]\t\t\n----------\n{12}\n----------\n#ENDOFLOG#\r\n",
                LocalTime, Time, Title, Section, LogLevel, LogType, Username, MobileKey, DeviceId, Latitude, Longitude, Accuracy, Description);
        }

        public static Log Parse(string text)
        {
            if (string.IsNullOrWhiteSpace(text) || !text.StartsWith("LocalTime:"))
                return null;

            int index, endIndex;
            var res = new Log();


            index = text.IndexOf("\t");
            if (index == -1) return null;
            res.LocalTime = DateTime.ParseExact(text.Substring(10, index - 10), "MM/dd/yyyy HH:mm:ss fff", null);

            if (text.Contains("\tUtcTime:"))
            {
                index = text.IndexOf("\tUtcTime:") + 9;
                endIndex = text.IndexOf("\t", index);
                if (endIndex == -1)
                    res.Time = DateTime.ParseExact(text.Substring(index), "MM/dd/yyyy HH:mm:ss fff", null);
                else
                    res.Time = DateTime.ParseExact(text.Substring(index, endIndex - index), "MM/dd/yyyy HH:mm:ss fff", null);
            }

            if (text.Contains("\tLogTitle:"))
            {
                index = text.IndexOf("\tLogTitle:") + 10;
                endIndex = text.IndexOf("\t", index);
                if (endIndex == -1)
                    res.Title = text.Substring(index);
                else
                    res.Title = text.Substring(index, endIndex - index);
            }

            if (text.Contains("\t(AppSection:"))
            {
                index = text.IndexOf("\t(AppSection:") + 13;
                endIndex = text.IndexOf(")\t\t\t-", index);
                if (endIndex == -1)
                    res.Section = text.Substring(index);
                else
                    res.Section = text.Substring(index, endIndex - index);
            }

            if (text.Contains(" [LogLevel:"))
            {
                index = text.IndexOf(" [LogLevel:") + 11;
                endIndex = text.IndexOf("\t", index);
                try
                {
                    if (endIndex == -1)
                        res.LogLevel = int.Parse(text.Substring(index).Trim());
                    else
                        res.LogLevel = int.Parse(text.Substring(index, endIndex - index).Trim());
                }
                catch { }
            }

            if (text.Contains("\tLogType:"))
            {
                index = text.IndexOf("\tLogType:") + 9;
                endIndex = text.IndexOf("\t", index);
                try
                {
                    if (endIndex == -1)
                        res.LogType = int.Parse(text.Substring(index).Trim());
                    else
                        res.LogType = int.Parse(text.Substring(index, endIndex - index).Trim());
                }
                catch { }
            }

            if (text.Contains("\tUser:"))
            {
                index = text.IndexOf("\tUser:") + 6;
                endIndex = text.IndexOf("\t", index);
                if (endIndex == -1)
                    res.Username = text.Substring(index);
                else
                    res.Username = text.Substring(index, endIndex - index);
            }

            if (text.Contains("\tMobileKey:"))
            {
                index = text.IndexOf("\tMobileKey:") + 11;
                endIndex = text.IndexOf("\t", index);
                if (endIndex == -1)
                    res.MobileKey = text.Substring(index);
                else
                    res.MobileKey = text.Substring(index, endIndex - index);
            }

            if (text.Contains("\tDevice:"))
            {
                index = text.IndexOf("\tDevice:") + 8;
                endIndex = text.IndexOf("\t", index);
                if (endIndex == -1)
                    res.DeviceId = text.Substring(index);
                else
                    res.DeviceId = text.Substring(index, endIndex - index);
            }

            if (text.Contains("\tPosition:"))
            {
                index = text.IndexOf("\tPosition:") + 10;
                endIndex = text.IndexOf(",", index);
                if (endIndex > -1)
                    try
                    {
                        res.Latitude = double.Parse(text.Substring(index, endIndex - index).Trim());
                    }
                    catch { }

                index = endIndex + 1;
                endIndex = text.IndexOf("\t(", index);
                if (endIndex > -1)
                    try
                    {
                        res.Longitude = double.Parse(text.Substring(index, endIndex - index).Trim());
                    }
                    catch { }

                index = endIndex + 2;
                endIndex = text.IndexOf(")]\t", index);
                if (endIndex > -1)
                    try
                    {
                        res.Accuracy = float.Parse(text.Substring(index, endIndex - index).Trim());
                    }
                    catch { }
            }

            if (text.Contains(")]\t\t\n----------\n"))
            {
                index = text.IndexOf(")]\t\t\n----------\n") + 16;
                endIndex = text.IndexOf("\n----------\n#ENDOFLOG#\r\n", index);
                if (endIndex == -1)
                    res.Description = text.Substring(index);
                else
                    res.Description = text.Substring(index, endIndex - index);
            }

            return res;
        }
        #endregion
    }
}

