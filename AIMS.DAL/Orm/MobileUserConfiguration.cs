﻿using SQLite;

namespace AIMS.DAL
{
    public class MobileUserConfiguration
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int ID { get; set; }

        [MaxLength(50)]
        public string Username { get; set; }
        
        public string Name { get; set; }
        
        public string Value { get; set; }
    }
}
