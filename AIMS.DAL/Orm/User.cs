﻿using System;

namespace AIMS.DAL
{
	public class User
	{
        public decimal AirlineId {
            get;
            set;
        }

        public string MobileKey
        {
            get;
            set;
        }

        public string AirportName {
			get;
			set;
		}
		
		public string Username {
			get;
			set;
		}

        public string Password
        {
            get;
            set;
        }

        public string FirstName {
			get;
			set;
		}

		public string LastName {
			get;
			set;
		}

		public byte[] ProfileImage {
			get;
			set;
		}

		public DateTime LastLoginDate {
			get;
			set;
		}

		public override string ToString ()
		{
			return string.Format ("[User: Username={0}, FirstName={1}, LastName={2}]", Username, FirstName, LastName);
		}
	}
}

