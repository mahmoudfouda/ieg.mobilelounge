﻿using System;
using SQLite;

namespace AIMS.DAL
{
    /// <summary>
    /// This is in fact Workstation carrying Lounge Information.
    /// </summary>
	public class Workstation
	{
		public decimal LoungeID {
			get;
			set; 
		}

        public decimal DefaultAirlineId {
            get;
            set;
        }

        [PrimaryKey, Column("_id")]
        public decimal WorkstationID { get; set; }

        public int PAXAvgStayMinutes { get; set; }

        public int Capacity { get; set; }

        [MaxLength(3)]
		public string AirportCode
        {
			get;
			set;
		}

		[MaxLength(80)]
		public string LoungeName {
			get;
			set;
		} 

        [MaxLength(50)]
        public string WorkstationName { get; set; }
        
		public string Description {
			get;
			set;
		}
	}
}