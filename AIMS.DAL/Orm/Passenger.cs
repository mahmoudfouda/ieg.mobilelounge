﻿using System;
using SQLite;

namespace AIMS.DAL
{
	public class Passenger
	{
        [PrimaryKey, AutoIncrement, Column("_id")]//TODO: Check the passenger saving progress
        public int Id { get; set; }

		public decimal TrackingRecordID { get; set; }

        public decimal CardID { get; set; }

        public string CardName { get; set; }

        public decimal OtherCardID { get; set; }
        
        public string FFN { get; set; }

        public string FlightCarrier { get; set; }

        public string FlightNumber { get; set; }

        public string BoardingPassFlightCarrier { get; set; }

        public string BoardingPassFlightNumber { get; set; }

        public string BoardingPassCompCode { get; set; }

        public string BoardingPassFFAirline { get; set; }

        public string BoardingPassFFNumber { get; set; }

        public string BoardingPassPaxDesc { get; set; }

        public string BoardingPassSeq { get; set; }

        public string BoardingPassEIndicator { get; set; }

        public DateTime BoardingPassFlightDate { get; set; }

        public string BoardingPassName { get; set; }

        public string Title { get; set; }

        //[MaxLength(128)]
        //public string FirstName {
        //	get;
        //	set;
        //}

        //[MaxLength(128)]
        //public string LastName {
        //	get;
        //	set;
        //}

        //[Ignore]
        [MaxLength(50)]
        public string FullName {
            //get{
            //	return string.Format ("{0}{1} {2}", string.Format ("{0} ", string.IsNullOrWhiteSpace (Title) ? "" : Title), FirstName.Trim(), LastName.Trim());
            //}
            get;
            set;
		}

        [MaxLength(50)]
        public string HostName { get; set; }

        public string FromAirport { get; set; }

        public string FromCity { get; set; }

        public string ToAirport { get; set; }

        public string ToCity { get; set; }

        public DateTime DepartureDate { get; set; }

        public DateTime BoardingTime { get; set; }

        public DateTime DepartureTime { get; set; }

        public DateTime ArrivalDate { get; set; }

        public DateTime ArrivalTime { get; set; }

        public string PNR { get; set; }

		public string PassengerStatus { get; set;}

		public string TrackingClassOfService { get; set; }

        public string TrackingPassCombinedFlightNumber { get; set; }

        public string TrackingSystemInfo { get; set; }

        public int TrackingStatus { get; set; }
        
        public string BarcodeString { get; set; }

        public bool IsValid { get; set; }

        public decimal AirlineId { get; set; }

        public decimal OtherAirlineId { get; set; }

        public string Notes { get; set; }

        public string Terminal { get; set; }

        public string GateNumber { get; set; }

        public string SeatNumber { get; set; }

        public decimal GuestOf { get; set; }

        public decimal WorkstationId { get; set; }

        public decimal LoungeId { get; set; }

        public string LoungeName { get; set; }

        public DateTime TrackingTimestamp { get; set; }

        public DateTime TimestampUtc { get; set; }

        public string FailedReason { get; set; }

        public string AirlineDesignatorBoardingPassIssuer { get; set; }

        public string GUID { get; set; }

        public bool HasPnrHistory { get; set; }

        public decimal Type { get; set; }

        public decimal SpecialHandling { get; set; }
    }
}

