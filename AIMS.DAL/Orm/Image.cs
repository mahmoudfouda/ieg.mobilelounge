﻿using SQLite;

namespace AIMS.DAL
{
    public class Image
    {
        [PrimaryKey, Column("_handle")]
        public string ImageHandle { get; set; }

        public byte[] PictureBytes { get; set; }
    }
}
