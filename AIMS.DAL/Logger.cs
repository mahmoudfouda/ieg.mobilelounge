﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace AIMS.DAL
{
	public class Logger : DBConnection
	{
		private static Logger _current;
		public static Logger Current { 
			get{ 
				if (_current == null)
					_current = new Logger (DatabaseFilePath);
				return _current;
			} 
		}

//		public static void SetCurrentPath(string path)
//		{
//			SetCurrentLogger (new Logger(Path.Combine(path, _dbFileName)));
//		} 

//		public static void SetCurrentLogger(Logger logger)
//		{
//			if(logger == null)
//				throw new ArgumentNullException("logger");
//			
//			Current = logger;
//		}

		protected Logger (string path) : base (path)
		{
		}

		public IEnumerable<Log> GetLogsByLevel (int level) 
		{
			lock (DBConnection.locker) {
				return (from i in Table<Log> () where i.LogLevel == level select i).OrderByDescending(x=>x.Time).ToList ();
			}
		}

		public IEnumerable<Log> GetLogsByType (int type) 
		{
			lock (DBConnection.locker) {
				return (from i in Table<Log> () where i.LogType == type select i).OrderByDescending(x => x.Time).ToList ();
			}
		}

		public IEnumerable<Log> GetLogs (int level, int type) 
		{
			lock (DBConnection.locker) {
				return (from i in Table<Log> () where i.LogLevel == level && i.LogType == type select i).OrderByDescending(x => x.Time).ToList ();
			}
		}

		public IEnumerable<Log> GetLogs () 
		{
			lock (DBConnection.locker) {
				return (from i in Table<Log> () select i).OrderByDescending(x => x.Time).ToList ();
			}
		}
        
		public Log GetLog (int id)
		{
			lock (DBConnection.locker) {
				return Table<Log>().FirstOrDefault(x => x.Id == id);
			}
		}

		public int SaveLog (Log item) 
		{
			lock (DBConnection.locker) {

                var day = DateTime.Now.AddDays(-5);

                var logs = (from i in Table<Log>() select i);
                logs = logs.Where(i => i.Time < day);

                if(logs.Count() > 30)
                {
                    foreach (var log in logs)
                    {
                        Delete<Log>(log.Id);
                    }
                }
                
                if (item.Id != 0) {
					Update (item);
					return item.Id;
				} else {
					return Insert (item);
				}
			}
		}

		public int DeleteLog(Log log) 
		{
			lock (DBConnection.locker) {
				return Delete<Log> (log.Id);
			}
		}

        public void DeleteAllLogs()
        {
            lock (DBConnection.locker)
            {
                var logs = (from i in Table<Log>() select i).ToList();
                foreach (var log in logs)
                {
                    Delete<Log>(log.Id);
                }
            }
        }
    }
}