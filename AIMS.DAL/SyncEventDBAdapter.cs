﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.DAL
{
    public class SyncEventDBAdapter : DBConnection
    {
        protected SyncEventDBAdapter(string path) : base (path)
		{
        }

        private static SyncEventDBAdapter _current;
        public static SyncEventDBAdapter Current
        {
            get
            {
                if (_current == null)
                    _current = new SyncEventDBAdapter(DatabaseFilePath);
                return _current;
            }
        }

        public IEnumerable<SyncEvent> GetAllSyncEvents()
        {
            lock (DBConnection.locker)
            {
                return (from i in Table<SyncEvent>() select i).ToList();
            }
        }
        
        public SyncEvent GetSyncEvent(int id)
        {
            lock (DBConnection.locker)
            {
                return Table<SyncEvent>().FirstOrDefault(x => x.Id == id);
            }
        }

        public SyncEvent GetLastSyncEvent(int scope)
        {
            lock (DBConnection.locker)
            {
                return (from i in Table<SyncEvent>() where i.Scope == scope select i).OrderByDescending(x=>x.SyncServerDate).FirstOrDefault();
            }
        }

        public int SaveSyncEvent(SyncEvent item)
        {
            var syncEvent = GetSyncEvent(item.Id);
            lock (DBConnection.locker)
            {
                if (syncEvent != null)
                {
                    Update(item);
                    return item.Id;
                }
                else
                {
                    return Insert(item);
                }
            }
        }

        public int DeleteSyncEvent(SyncEvent item)
        {
            lock (DBConnection.locker)
            {
                return Delete<SyncEvent>(item.Id);
            }
        }
    }
}
