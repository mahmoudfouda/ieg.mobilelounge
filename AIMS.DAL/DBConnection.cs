﻿using SQLite;
using System.IO;

namespace AIMS.DAL
{
    public class DBConnection : SQLiteConnection
    {
        public static object locker = new object();

        protected const string _dbFileName = "AIMSLogsSQLite.db3";
        private static string _dbPath;
        private static bool _dbIsCreated = false;

        public static string DatabaseFilePath
        {
            get
            {
                return _dbPath;
            }
        }

        public static void SetCurrentPath(string path)
        {
            _dbPath = Path.Combine(path, _dbFileName);
        }

        protected DBConnection(string path) : base(path)
        {
            if (_dbIsCreated)
                return;
            _dbIsCreated = true;
            _dbPath = path;
            CreateTable<Log>();
            CreateTable<Image>();
            CreateTable<Airline>();
            CreateTable<Airport>();
            CreateTable<Workstation>();
            CreateTable<Card>();
            CreateTable<Passenger>();
            CreateTable<MobileUserConfiguration>();
            CreateTable<SyncEvent>();
        }

    }
}

