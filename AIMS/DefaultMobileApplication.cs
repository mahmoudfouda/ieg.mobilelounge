﻿
using AIMS.Contracts;
using AIMS.DAL;
using System;
using System.Collections.Generic;
using Ieg.Mobile.Localization;
using Ieg.Mobile.DataContracts.MobileLounge.ServiceModel;
using AIMS.Models;

namespace AIMS
{
    public class DefaultMobileApplication : IMobileApplication
    {
        private static DefaultMobileApplication _current = null;
        public static DefaultMobileApplication Current
        {
            get
            {
                if (_current == null)
                    _current = new AIMS.DefaultMobileApplication();
                return _current;
            }
        }

        private const int authenticationCheckInterval = 5000;
        public const int MaxSessionTimeoutSeconds = 18000;
        public const int MinSessionTimeoutSeconds = 300;

        public const int MinRefreshIntervalMinutes = 2;
        public const int MaxRefreshIntervalMinutes = 10;

        public const int MinHoursOccupancyHistory = 1;
        public const int MaxHoursOccupancyHistory = 12;


        private static int sessionTimeoutSeconds = 0;

        //private const int locationExpireInterval = 10000;

        private static DateTime lastUserActivityTime = DateTime.Now;

        private Ieg.Mobile.DataContracts.Models.Position _position;
        public Ieg.Mobile.DataContracts.Models.Position Position
        {
            get
            {
                //if (LastLocationUpdate.AddMilliseconds(locationExpireInterval).CompareTo(DateTime.Now) < 0)
                //    _position = null;
                return _position;
            }
            set
            {
                _position = value;
                LastLocationUpdate = DateTime.Now;
            }
        }

        public IReadOnlyCollection<Models.ReadOnlyFence> Fences
        {
            get
            {
                //Not a good Idea
                //if (MembershipProvider.Current?.WhiteFences == null)
                //    return new System.Collections.ObjectModel.ReadOnlyCollection<Models.ReadOnlyFence>(new List<Models.ReadOnlyFence>());
                return MembershipProvider.Current?.WhiteFences;
            }
        }

        public Dictionary<string, string> ThirdPartySerialNumbers { get; private set; }

        public DateTime LastLocationUpdate { get; private set; }

        public IGpsAdapter GpsAdapter { get; private set; }

        public bool ContinueScanning { get; set; }

        private CameraType? activeCamera = null;
        public CameraType? ActiveCamera
        {
            get
            {
                return activeCamera;
            }
            set
            {
                if (activeCamera != value)
                {
                    if (!value.HasValue) return;

                    var id = UserConfigurationDBAdapter.Current.SaveConfiguration(new MobileUserConfiguration
                    {
                        Name = UserConfigurationDBAdapter.CameraConfigName,
                        Username = "",
                        Value = ((int)value).ToString()
                    });

                    if (id > 0)
                    {
                        activeCamera = value;
                    }
                }
            }
        }

        private int refreshIntervalMinutes = MinRefreshIntervalMinutes;
        public int RefreshIntervalMinutes
        {
            get
            {
                return refreshIntervalMinutes;
            }
            set
            {
                if (value >= MinRefreshIntervalMinutes)
                {
                    var id = UserConfigurationDBAdapter.Current.SaveConfiguration(new MobileUserConfiguration
                    {
                        Name = UserConfigurationDBAdapter.RefreshIntervalConfigName,
                        Username = "",
                        Value = value.ToString()
                    });

                    if (id > 0)
                    {
                        refreshIntervalMinutes = value;
                    }
                }
                else
                {
                    var error = string.Format("The refresh interval value must be between {0} and {1} minutes.", MinRefreshIntervalMinutes, MaxRefreshIntervalMinutes);
                    LogsRepository.AddError("Error in setting the RefreshIntervalSeconds", error);
                    throw new Exception(error);
                }
            }
        }


        private int hoursOccupancyHistory = 3;// MinHoursOccupancyHistory;
        public int HoursOccupancyHistory
        {
            get
            {
                return hoursOccupancyHistory;
            }
            set
            {
                if (value >= MinHoursOccupancyHistory && value <= MaxHoursOccupancyHistory)
                {
                    var id = UserConfigurationDBAdapter.Current.SaveConfiguration(new MobileUserConfiguration
                    {
                        Name = UserConfigurationDBAdapter.HoursOccupancyHistoryConfigName,
                        Username = "",
                        Value = value.ToString()
                    });

                    if (id > 0)
                    {
                        hoursOccupancyHistory = value;
                    }
                }
                else
                {
                    var error = string.Format("The hours occupancy history scope value must be between {0} and {1} hours.", MinHoursOccupancyHistory, MaxHoursOccupancyHistory);
                    LogsRepository.AddError("Error in setting the HoursOccupancyHistory", error);
                    throw new Exception(error);
                }
            }
        }


        //Values less than 5 and more than 300 minutes are not acceptable
        public static int SessionTimeoutSeconds
        {
            get
            {
                if (sessionTimeoutSeconds == 0)
                    return 1200;//20 Minutes
                return sessionTimeoutSeconds;
            }
            set
            {
                if (sessionTimeoutSeconds != value)
                {
                    if (value >= MinSessionTimeoutSeconds && value <= MaxSessionTimeoutSeconds)
                    {
                        try
                        {
                            var id = UserConfigurationDBAdapter.Current.SaveConfiguration(new MobileUserConfiguration
                            {
                                Name = UserConfigurationDBAdapter.SessionTimeoutConfigName,
                                Username = "",
                                Value = value.ToString()
                            });

                            if (id > 0)
                            {
                                sessionTimeoutSeconds = value;
                            }
                            else LogsRepository.AddError("Error in SaveConfiguration()", string.Format("The returned id for changing {0} is {1}", UserConfigurationDBAdapter.SessionTimeoutConfigName, id));
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Exception in setting DefaultMobileApplication.SessionTimeoutSeconds", ex);
                        }
                    }
                    else
                    {
                        var error = string.Format("The session timeout value must be between {0} and {1} minutes", MinSessionTimeoutSeconds / 60, MaxSessionTimeoutSeconds / 60);
                        LogsRepository.AddError("Error in setting the SessionTimeout", error);
                        throw new Exception(error);
                    }
                }
            }
        }

        //Values less than 5 and more than 300 minutes are not acceptable
        public static int SessionTimeoutMinutes
        {
            get
            {
                return SessionTimeoutSeconds / 60;
            }
            set
            {
                SessionTimeoutSeconds = value * 60;
            }
        }

        public bool IsDeveloperModeEnabled { get; set; }

        public bool CheckAccuracy { get; set; } = true;

        public string StartupFolder { get; private set; }

        private LanguageRepository textProvider;
        public LanguageRepository TextProvider
        {
            get
            {
                this.UpdateLastUserActivityTime();
                return this.textProvider;
            }
            private set
            {
                this.textProvider = value;
            }
        }

        public Models.MobileDevice CurrentDevice { get; private set; }

        private DefaultMobileApplication()
        {
            this.ApplicationStart += BootstrapApplication;
        }

        public static DefaultMobileApplication Create()//We never create second instance
        {
            return Current;
        }

        private void BootstrapApplication(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Triggers the application start.
        /// </summary>
        public void TriggerApplicationStart()
        {
            if (this.ApplicationStart != null)
                this.ApplicationStart(this, EventArgs.Empty);

            //try
            //{
            //    if (_authenticationChecker == null || _authenticationChecker.Status != TaskStatus.Created)
            //        _authenticationChecker = new Task(new Action(AuthenticationCheckerMethod), TaskCreationOptions.LongRunning);
            //    _authenticationChecker.Start();
            //}
            //catch (Exception ex){
            //    NotifyValidationFailed(ErrorCode.UnknownException);
            //    var exception = new Exception("Failed to start the AuthenticationChecker", ex);
            //    LogsRepository.AddError("Error in getting TriggerApplicationStart()", exception);
            //    throw ex;
            //}
        }

        /// <summary>
        /// Triggers the application init.
        /// </summary>
        public void TriggerApplicationInit(string applicationStartupFolder, IGpsAdapter gpsAdapter, Models.MobileDevice device)
        {
            if (ApplicationInit != null)
                ApplicationInit.Invoke(this, EventArgs.Empty);

            if (device == null)
            {
                var ex = new Exception("AIMS | Failed initializing application:\nDevice info are required", new Exception("device is null"));
                //LogsRepository.AddError("Error in TriggerApplicationInit()", ex);//No logger available yet
                throw ex;
            }
            CurrentDevice = device;

            if (string.IsNullOrEmpty(device.DeviceUniqueId) || string.IsNullOrEmpty(applicationStartupFolder))
            {
                var ex = new Exception("AIMS | Failed initializing application:\nSerialNumber and StartupFolder is required");
                //LogsRepository.AddError("Error in getting TriggerApplicationInit()", ex);//No logger available yet
                throw ex;
            }
            StartupFolder = applicationStartupFolder;

            // Initialization of the logger.
            DBConnection.SetCurrentPath(StartupFolder);

            #region Initiating the SessionTimeout configuration
            try
            {
                var sessionConf = UserConfigurationDBAdapter.Current.GetConfiguration(UserConfigurationDBAdapter.SessionTimeoutConfigName);
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[Setting] - GetConfiguration() for SessionTimeout returned {0}", sessionConf == null ? "null" : sessionConf.Value);
#endif
                if (sessionConf != null)
                {
                    if (int.TryParse(sessionConf.Value, out sessionTimeoutSeconds))
                    {
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("[Setting] - SessionTimeout was set to {0}", sessionTimeoutSeconds);
#endif
                    }
                    else
                    {
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("[Setting] - SessionTimeout was set to {0}", sessionConf.Value);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Exception in TriggerApplicationInit()",
                    new Exception(
                        string.Format("Loading the configuration {0}", UserConfigurationDBAdapter.SessionTimeoutConfigName),
                        ex));
            }
            #endregion

            #region Initiating the default camera type
            try
            {
                var cameraConf = UserConfigurationDBAdapter.Current.GetConfiguration(UserConfigurationDBAdapter.CameraConfigName);
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[Setting] - GetConfiguration() for Camera returned {0}", cameraConf == null ? "null" : cameraConf.Value);
#endif
                if (cameraConf != null)//previousely selected camera
                {
                    int cameraTypeId = 0;
                    if (int.TryParse(cameraConf.Value, out cameraTypeId))
                    {
                        activeCamera = (CameraType)cameraTypeId;
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("[Setting] - Camera was set to {0}", activeCamera.Value);
#endif
                    }
                }
                //if (!activeCamera.HasValue)
                //    ActiveCamera = preferedDefaultCamera;
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Exception in TriggerApplicationInit()",
                    new Exception(
                        string.Format("Loading the configuration {0}", UserConfigurationDBAdapter.CameraConfigName),
                        ex));
            }
            #endregion

            #region Initiating default language and saving process
            try
            {//Loading and instantiation of Languages
                TextProvider = LanguageRepository.Current;
            }
            catch (Exception exception)
            {
                var ex = new Exception("AIMS | Failed initializing application", exception);
                LogsRepository.AddError("Error in loading language strings", ex);
                throw ex;
            }

            TextProvider.LoadLanguage += () =>
            {
                try
                {
                    var langConf = UserConfigurationDBAdapter.Current.GetConfiguration(UserConfigurationDBAdapter.LanguageConfigName);
                    if (langConf != null)//previousely selected lang
                    {
                        int langId = 0;
                        int.TryParse(langConf.Value, out langId);
                        return (Language)langId;
                    }
                }
                catch (Exception exception)
                {
                    var ex = new Exception("AIMS | Failed loading the language", exception);
                    LogsRepository.AddError("Error in reading the selected language", ex);
                }
                return null;
            };

            LanguageRepository.Current.OnLanguageChanged += (language) =>
            {
                try
                {
                    UserConfigurationDBAdapter.Current.SaveConfiguration(new MobileUserConfiguration
                    {
                        Name = UserConfigurationDBAdapter.LanguageConfigName,
                        Username = "",
                        Value = ((int)language).ToString()
                    });
                }
                catch (Exception exception)
                {
                    var ex = new Exception("AIMS | Failed saving the language", exception);
                    LogsRepository.AddError("Error in writing the selected language in configurations", ex);
                }
            };
            #endregion

            #region Initiating the Statistics configuration
            try
            {
                var refreshIntervalConf = UserConfigurationDBAdapter.Current.GetConfiguration(UserConfigurationDBAdapter.RefreshIntervalConfigName);
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[Setting] - GetConfiguration() for RefreshInterval returned {0}", refreshIntervalConf == null ? "null" : refreshIntervalConf.Value);
#endif
                if (refreshIntervalConf != null)
                {
                    if (int.TryParse(refreshIntervalConf.Value, out refreshIntervalMinutes))
                    {
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("[Setting] - RefreshInterval was set to {0}", sessionTimeoutSeconds);
#endif
                    }
                    else
                    {
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("[Setting] - RefreshInterval was set to {0}", refreshIntervalConf.Value);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Exception in TriggerApplicationInit()",
                    new Exception(
                        string.Format("Loading the configuration {0}", UserConfigurationDBAdapter.RefreshIntervalConfigName),
                        ex));
            }

            try
            {
                var hoursOccupancyHistoryConf = UserConfigurationDBAdapter.Current.GetConfiguration(UserConfigurationDBAdapter.HoursOccupancyHistoryConfigName);
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[Setting] - GetConfiguration() for HoursOccupancyHistory returned {0}", hoursOccupancyHistoryConf == null ? "null" : hoursOccupancyHistoryConf.Value);
#endif
                if (hoursOccupancyHistoryConf != null)
                {
                    if (int.TryParse(hoursOccupancyHistoryConf.Value, out hoursOccupancyHistory))
                    {
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("[Setting] - HoursOccupancyHistory was set to {0}", hoursOccupancyHistory);
#endif
                    }
                    else
                    {
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("[Setting] - HoursOccupancyHistory was set to {0}", hoursOccupancyHistoryConf.Value);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Exception in TriggerApplicationInit()",
                    new Exception(
                        string.Format("Loading the configuration {0}", UserConfigurationDBAdapter.HoursOccupancyHistoryConfigName),
                        ex));
            }


            #endregion

            if (gpsAdapter == null)
            {
                var ex = new Exception("AIMS | Failed initializing application:\nGpsAdapter is required");
                LogsRepository.AddError("Error in TriggerApplicationInit()", ex);
                throw ex;
            }

            //GpsValidationProvider = new GpsValidationProvider(new WebApiServiceContext());
            GpsAdapter = gpsAdapter;
            if (GpsAdapter != null)
            {
                GpsAdapter.GpsAdapter_OnLocationChanged += (object sender, Ieg.Mobile.DataContracts.Models.Position position) =>
                {
                    Position = position;
                    if (Position == null) LogsRepository.AddError("Error in GpsAdapter_OnLocationChanged", new Exception("Position is null"));
#if DEBUG
                    if (Position != null)
                        System.Diagnostics.Debug.WriteLine("Position update: ({0} | {1} - Acc.:{2})", Position.Latitude, Position.Longitude, Position.Accuracy);
                    else
                        System.Diagnostics.Debug.WriteLine("Error in GpsAdapter_OnLocationChanged: Position is null");
#endif
                };
                GpsAdapter.Start();
            }
        }

        //TODO: In the AuthenticationCheckerMethod() reuse this (Needs a real refactoring)
        /// <summary>
        /// Just checks the secondary token and GPS position
        /// </summary>
        /// <returns>true if the secondary token and Position are valid</returns>
        public bool SecondaryAuthenticationCheck()
        {
            if (MembershipProvider.Current == null) return false;

            var userRefreshed = false;
            var userIsValid = MembershipProvider.Current.IsAuthenticated;
            var hasSecondVlidator = MembershipProvider.Current.SecondValidator != null;
            var isSecondTokenExpired = hasSecondVlidator && MembershipProvider.Current.SecondValidator.IsTokenExpired;
            if (userIsValid && isSecondTokenExpired)//External token needs to be refereshed
            {
                LogsRepository.AddWarning("Token", "Secondary token has expired");
                userRefreshed = true;
                var refreshTokenResult = MembershipProvider.Current.SecondValidator.RefreshToken(MembershipProvider.Current.LoggedinUser.Username, MembershipProvider.Current.LoggedinUser.MobileKey, Position.ToContractPosition()).Result;
                userIsValid = refreshTokenResult.IsSucceeded;
                if (userIsValid)
                    LogsRepository.AddInfo("Token", "Secondary token has renewed (refreshed)");
                else LogsRepository.AddError("Token", "Secondary token has failed to renew");
            }

            if (!userIsValid)
            {
                if (userRefreshed)//Token needs to be regenerated
                    NotifyValidationFailed(ErrorCode.SessionExpiredError);//User(token) refresh has initiated, but unsuccessful. (Network is down or Token can not be refreshed anymore)
                else
                    NotifyValidationFailed(ErrorCode.UserValidationError);//Credentials were wrong
                if (MembershipProvider.Current != null)
                    MembershipProvider.Current.LogOutUser();
            }
            else if (!GpsValidationProvider.CheckValidity())//Member is out of the Geo-Fence
            {
                LogsRepository.AddError("GPS validation", "Fence validation failed");
                NotifyValidationFailed(ErrorCode.LocationValidationException);
                MembershipProvider.Current.LogOutUser();
            }
            else if (lastUserActivityTime.AddSeconds(SessionTimeoutSeconds).CompareTo(DateTime.Now) <= 0)//Member's session is expired
            {
                LogsRepository.AddError("Session Expired", string.Format("User session has been expired. Session timeout: {0}", SessionTimeoutSeconds));
                NotifyValidationFailed(ErrorCode.SessionExpiredError);
                MembershipProvider.Current.LogOutUser();
            }
            else return true;
            return false;
        }

        private void UpdateLastUserActivityTime()
        {
            lastUserActivityTime = DateTime.Now;
        }

        /// <summary>
        /// Triggers the application stop.
        /// </summary>
        public void TriggerApplicationStop()
        {
            if (this.ApplicationStop != null)
                this.ApplicationStop(this, EventArgs.Empty);
        }

        private static void NotifyValidationFailed(ErrorCode errorCode)
        {
            if (OnValidationFailed != null)
                OnValidationFailed.Invoke(null, errorCode);
        }

        #region IMobileApplication implementation
        public event EventHandler ApplicationStart;
        public event EventHandler ApplicationInit;
        public event EventHandler ApplicationStop;
        public static event EventHandler<ErrorCode> OnValidationFailed;
        #endregion
    }
}