﻿using System;
using System.Collections.Generic;
using System.Linq;
using AIMS.DAL;
//using Ieg.Aims.Business.Contracts.Data_Contracts;
using AIMS.Contracts;
using AIMS.Contracts.AIMSClient;
using System.ServiceModel;
using AIMS.Models;

namespace AIMS
{
    internal class AirlineAdorner : Models.Airline, Models.IAdorner
    {
        //public object DALObject { get { return this._airline; } }

        public AirlineAdorner(DAL.Airline airline)
        {
            if (airline == null)
                throw new ArgumentNullException("airline");
            else this._airline = airline;
        }
    }

    public class AirlinesRepository : RepositoryBase
    {
        public AirlinesRepository() : base()//(new WebApiServiceContext())
        {
            OnChangeNeeded = AirlinesRepository_OnChangeNeededSpecified;
        }

        private void AirlinesRepository_OnChangeNeededSpecified(bool isNeeded)
        {
            if (isNeeded)
            {
                //In case of synchronization request, we need to remove all local images.
                ImageDBAdapter.Current.DeleteAllImages();
                ReloadAirlinesAndCardsFromServer();
            }
            else NotifyRepositoryChanged(Airlines);
        }

        protected static IEnumerable<Models.Airline> Airlines
        {
            get;
            set;
        }

        protected static IEnumerable<Models.Card> Cards
        {
            get;
            set;
        }

        protected void ReloadAirlinesAndCardsFromServer()
        {
            try
            {
                string localErrMessage = string.Empty;

                //In case of any new configuration (airlines and cards) request, we need to clear all local airlines and cards.
                CardDBAdapter.Current.DeleteAllCards();
                AirlineDBAdapter.Current.DeleteAllAirlines();

                //_proxy.GetJson<ResultPack<List<AIRLINE>>>(string.Format("https://aims2.ieg-america.com/aims_2_services/mobile/MobileWebAPI_AC/workstation/{0}/airlines", MembershipProvider.Current.SelectedLounge.WorkstationID), DefaultMobileApplication.Current.Position,
                _proxy.GetJson<ResultPack<List<AIRLINE>>>(string.Format("workstation/{0}/airlines", (int)MembershipProvider.Current.SelectedLounge.WorkstationID), DefaultMobileApplication.Current.Position.ToContractPosition(),
                    (aimsSvrResponse, log) =>
                    {
                        try
                        {
                            var res = (ResultPack<List<AIRLINE>>)aimsSvrResponse;
                            List<AIRLINE> serverAirlines = res.ReturnParam;

                            #region Setting and saving Airlines
                            Airlines = serverAirlines.Select(x => x.ConvertToAirline()).ToList();

                            if (Airlines != null || Airlines.Count() > 0)
                            {
                                foreach (IAdorner airline in Airlines)
                                {
                                    AirlineDBAdapter.Current.SaveAirline((DAL.Airline)airline.DALObject);//TODO: can happen after images loaded also
                                }
                            }
                            #endregion

                            #region Setting and saving Cards
                            Cards = serverAirlines.
                                SelectMany(x => x.CARD_LIST).
                                Select(x => x.ConvertToCard()).ToList();

                            if (Cards != null || Cards.Count() > 0)
                            {
                                foreach (IAdorner card in Cards)
                                {
                                    CardDBAdapter.Current.SaveCard((DAL.Card)card.DALObject);
                                }
                            }
                            #endregion

                            NotifyRepositoryChanged(Airlines);
                            if (MembershipProvider.Current == null)
                            {
                                LogsRepository.AddError("Error in ReloadAirlinesAndCardsFromServer()", "User logged out in the middle of airlines and cards Sync process");
                            }
                            else if (MembershipProvider.Current.SelectedAirline != null)
                                NotifyRepositoryChanged(GetAirlineCards());

                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Error in loading airlines", ex);
                            Airlines = new List<Models.Airline>();
                            Cards = new List<Models.Card>();
                        }
                    },
                    (aimsResult, log) =>
                    {
                        LogsRepository.AddError("Error in loading airlines", aimsResult.ReturnParam);
                        Airlines = new List<Models.Airline>();
                        Cards = new List<Models.Card>();
                    });
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in BeginGetAirlines", ex);
                Airlines = new List<Models.Airline>();
                Cards = new List<Models.Card>();
            }
        }

        //private void CallbackOn_GetAirlines(IAsyncResult ar)
        //{
        //    string localErrMessage = string.Empty;
        //    IAimsClientService proxyAims = ar.AsyncState as AimsClientServiceClient;
        //    try
        //    {
        //        List<AIRLINE> serverAirlines = proxyAims.EndGetAirlines(ref localErrMessage, ar);
        //        ((ICommunicationObject)proxyAims).Close();

        //        #region Setting and saving Airlines
        //        Airlines = serverAirlines.Select(x => x.ConvertToAirline()).ToList();

        //        if (Airlines != null || Airlines.Count() > 0)
        //        {
        //            foreach (var airline in Airlines)
        //            {
        //                AirlineDBAdapter.Current.SaveAirline(((AirlineAdorner)airline).DALAirline);//TODO: can happen after images loaded also
        //            }
        //        }
        //        #endregion

        //        #region Setting and saving Cards
        //        Cards = serverAirlines.
        //            SelectMany(x => x.CARD_LIST).
        //            Select(x => x.ConvertToCard()).ToList();

        //        if (Cards != null || Cards.Count() > 0)
        //        {
        //            foreach (var card in Cards)
        //            {
        //                CardDBAdapter.Current.SaveCard(((CardAdorner)card).DALCard);//TODO: can happen after images loaded also
        //            }
        //        }
        //        #endregion
        //        //if (MembershipProvider.Current.SelectedAirline != null)//if a specific card is selected
        //        //    Cards = Cards.Where(x => x.AirlineID == MembershipProvider.Current.SelectedAirline.ID).ToList();
        //        //else Cards = new List<Card>();

        //        //Update - notification
        //        NotifyRepositoryChanged(Airlines);
        //        if (MembershipProvider.Current.SelectedAirline != null)
        //            NotifyRepositoryChanged(GetAirlineCards());
        //    }
        //    catch (Exception ex)
        //    {
        //        ((ICommunicationObject)proxyAims).Abort();
        //        LogsRepository.AddError("Error in loading airlines", ex);
        //        Airlines = new List<Models.Airline>();
        //        Cards = new List<Models.Card>();
        //    }
        //}

        public void SearchAirlines(string keyword)
        {
            if (Airlines == null || Airlines.Count() == 0 || string.IsNullOrWhiteSpace(keyword)) return;

            keyword = keyword.ToLower();

            var scoredResult = Airlines.Where(x => x.Code.ToLower().StartsWith(keyword) || x.Name.ToLower().Contains(keyword)).Distinct().ToList().Select(x => {
                var nameMatchIndex = x.Name.ToLower().IndexOf(keyword);
                var codeMatchIndex = x.Code.ToLower().IndexOf(keyword);
                return new ScoredItem
                {
                    Item = x,
                    CodeMatch = codeMatchIndex == 0 ? (x.Code.Length == keyword.Length ? 3 : 2) : (codeMatchIndex > 0 ? 1 : 0),
                    NameMatch = nameMatchIndex == 0 ? (x.Name.Length == keyword.Length ? 3 : 2) : (nameMatchIndex > 0 ? 1 : 0)
                };
            }).ToList().OrderByDescending(x => x.CodeMatch + x.NameMatch).ThenByDescending(x => x.CodeMatch).ThenByDescending(x => x.NameMatch).ToList();

            var codeMatched = scoredResult.Count(x => x.CodeMatch >= 2);//need a separator
            if (codeMatched != scoredResult.Count)
                scoredResult.Insert(codeMatched, new ScoredItem
                {
                    Item = new Models.Airline
                    {
                        Code = "######",
                        Name = "######"
                    }
                });

            IEnumerable<Models.Airline> result = scoredResult.Select(x => (Models.Airline)x.Item).ToList();
            NotifyRepositoryChanged(result);
        }

        public void LoadAirlines(bool forceGet = false)
        {
            try
            {
                if (forceGet)//(Airlines == null || Airlines.Count() == 0 || forceGet)
                {
                    ReloadAirlinesAndCardsFromServer();
                }
                else
                {
                    var als = AirlineDBAdapter.Current.GetAirlines();
                    if (als == null)
                        Airlines = null;
                    else
                        Airlines = als.Select(x => new AirlineAdorner(x)).ToList();
                    CheckIfSynchronizationNeeded();
                }
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in loading Airlines", ex);
                Airlines = new List<Models.Airline>();
                Cards = new List<Models.Card>();
            }
        }

        public List<Models.Card> GetAirlineCards(decimal? selectedAirlineId = null)
        {
            if (selectedAirlineId.HasValue)
                return Cards.Where(x => x.AirlineID == selectedAirlineId.Value).ToList();
            if (MembershipProvider.Current.SelectedAirline != null)
                return Cards.Where(x => x.AirlineID == MembershipProvider.Current.SelectedAirline.ID).ToList();
            return new List<Models.Card>();
        }

        public static Models.Airline GetAirlineFromDB(decimal airlineId)
        {
            try
            {
                var dbAirline = AirlineDBAdapter.Current.GetAirline(airlineId);
                if (dbAirline == null)
                    return null;
                return new AirlineAdorner(dbAirline);
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in GetAirlineFromDB", ex);
                return null;
            }
        }

        //public static OperationResult<byte[]> SaveAirlineImage(Models.Airline airline)
        //{
        //    if (airline == null)
        //        return new OperationResult<byte[]> {
        //            IsSucceeded = false,
        //            Message = "Airline object is null"
        //        };
        //    try
        //    {
        //        var dalAirline = ((IAdorner)airline).DALObject as DAL.Airline;
        //        AirlineDBAdapter.Current.SaveAirlineImage(dalAirline);
        //        return new OperationResult<byte[]>
        //        {
        //            IsSucceeded = true,
        //            ReturnParam = airline.AirlineLogoBytes
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LogsRepository.AddError("Error in saving Airline with Image", ex);
        //        return new OperationResult<byte[]>
        //        {
        //            IsSucceeded = false,
        //            Message = ex.Message
        //        };
        //    }
        //}

        class ScoredItem
        {
            public int NameMatch { get; set; }
            public int CodeMatch { get; set; }
            public object Item { get; set; }
        }
    }
}