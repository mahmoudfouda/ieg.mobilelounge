﻿using AIMS.Contracts;
using AIMS.Contracts.DataContracts;
using AIMS.Models;
using Ieg.Mobile.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AIMS
{
    /// <summary>
    /// EventHandler delegate for getting one lounge occupancy
    /// </summary>
    /// <param name="isSucceeded"></param>
    /// <param name="errorCode"></param>
    /// <param name="errorMetadata"></param>
    /// <param name="message"></param>
    /// <param name="occupancyPercentage"></param>
    public delegate void LoungeOccupancyReceivedHandler(bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancy occupancy);

    /// <summary>
    /// EventHandler delegate for getting the list of lounge occupancies
    /// </summary>
    /// <param name="isSucceeded"></param>
    /// <param name="errorCode"></param>
    /// <param name="errorMetadata"></param>
    /// <param name="message"></param>
    /// <param name="occupancyList"></param>
    public delegate void LoungeOccupancyListReceivedHandler(bool isSucceeded, int? errorCode, string errorMetadata, string message, List<LoungeOccupancy> occupancyList);


    public delegate void LoungeOccupancyCachedReceivedHandler(bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancyElapsedData occupancyList);

    public class OccupancyRepository : RepositoryBase
    {
        #region Events
        /// <summary>
        /// Event for notifying lounge occupancy received
        /// </summary>
        public event LoungeOccupancyReceivedHandler OnLoungeOccupancyReceived;

        /// <summary>
        /// Event for notifying lounge occupancy list received
        /// </summary>
        public event LoungeOccupancyListReceivedHandler OnLoungeOccupancyListReceived;

        public event LoungeOccupancyCachedReceivedHandler OnLoungeOccupancyCachdReceived;
        #endregion

        #region Constructor
        public OccupancyRepository() : base()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the latest updated percentage of the lounge Occupancy for the selected lounge
        /// </summary>
        /// <param name="workstation">Selected worstation by the user</param>
        public void GetLoungeOccupancy(Models.Workstation workstation)
        {
            if (workstation == null || workstation.LoungeID == 0)
                NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                {
                    IsSucceeded = false,
                    Message = "Selected Lounge is null or the ID is zero"
                });

            System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    //var clienteID = WebApiServiceContext.Principal.ClientId;

                    //var capacityRecord = WebApiServiceContext.GetCapacity(new CapacityDataRequest() { ClientId = clienteID, LoungeId = loungeID });

                    //if (capacityRecord == null || capacityRecord.ReturnParam == null)
                    //{
                    //    LogsRepository.AddError("Error in loungeoccupancy result processing");
                    //    NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                    //    {
                    //        IsSucceeded = false,
                    //        ErrorCode = (int)ErrorCode.UnknownException,
                    //        Message = "Error in loungeoccupancy result processing",
                    //        ErrorMetadata = capacityRecord != null ? capacityRecord.Message + "\n" + capacityRecord.ErrorMetadata : string.Empty
                    //    });

                    //    return;
                    //}

                    var dateTimeNow = DateTime.Now;

                    _proxy.SendJson<ResultPack<LoungeOccupancyRecord>>("retrieveoccupancy",
                        new LoungeDataRequest
                        {
                            LoungeId = workstation.LoungeID,
                            Capacity = workstation.Capacity == 0 ? 100 : workstation.Capacity,
                           // Capacity = workstation.Capacity,
                            PAXAvgStayMinutes = workstation.PAXAvgStayMinutes,
                            WorkstationId = workstation.WorkstationID,
                            AirportCode = workstation.AirportCode,
                            ToLocalDate = dateTimeNow,
                            FromLocalDate = dateTimeNow,
                            ToUtcDate = dateTimeNow.ToUniversalTime(),
                            FromUtcDate = dateTimeNow.ToUniversalTime()
                        },
                       DefaultMobileApplication.Current.Position.ToContractPosition(),
                    //_proxy.GetJson<ResultPack<LoungeOccupancyRecord>>(string.Format("lounge/{0}/occupancy", workstation.LoungeID), (Contracts.Position)DefaultMobileApplication.Current.Position,
                            (successResponse, log) =>
                            {
                                try
                                {
                                    LogsRepository.TrackEvents(log);

                                    var result = (ResultPack<LoungeOccupancyRecord>)successResponse;
                                    if (result.IsSucceeded)
                                    {
                                        if (result.ReturnParam == null)
                                        {
                                            NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                                            {
                                                IsSucceeded = false,
                                                ErrorCode = (int)ErrorCode.UnknownException,
                                                Message = String.Format("Retreived occupancy is empty {0}", result.Message)
                                            });
                                            return;
                                        }

                                        NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                                        {
                                            ReturnParam = new LoungeOccupancy
                                            {
                                                OccupancyTotal = result.ReturnParam.OccupancyTotal,
                                                Percentage = result.ReturnParam.OccupancyPercentage,
                                                Time = result.ReturnParam.OccupancyTime
                                            },
                                            IsSucceeded = result.IsSucceeded
                                        });
                                    }
                                    else
                                    {
                                        LogsRepository.AddError("Error in loungeoccupancy result processing", result.Message);
                                        NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                                        {
                                            IsSucceeded = false,
                                            ErrorCode = (int)ErrorCode.UnknownException,
                                            Message = result.Message
                                        });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogsRepository.AddError("Error in loungeoccupancy successResponse processing", ex);
                                    NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                                    {
                                        IsSucceeded = false,
                                        ErrorCode = (int)ErrorCode.UnknownException,
                                        Message = LanguageRepository.Current.GetText((int)ErrorCode.UnknownException),
                                        ErrorMetadata = ex.StackTrace
                                    });
                                }
                            },
                            (failed, log) =>
                            {
                                LogsRepository.AddError("Error in result of get loungeoccupancy call", failed.Message);
                                NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                                {
                                    IsSucceeded = false,
                                    Message = "Error in result of get loungeoccupancy call",
                                    ErrorMetadata = failed.Message
                                });
                            });
                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Error in get loungeoccupancy call", ex);
                    NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                    {
                        IsSucceeded = false,
                        Message = "Error in get loungeoccupancy call",
                        ErrorMetadata = ex.StackTrace
                    });
                }
            });
        }

        public void RetrieveLastOccupanciesList(List<decimal> loungeIds, List<decimal> workstationIds, List<string> airportCodes, DateTime startLocalDate, DateTime endLocalDate, int intervalInSeconds)
        {
            if (loungeIds == null || loungeIds.Count == 0 ||
                workstationIds == null || workstationIds.Count == 0 ||
                airportCodes == null || airportCodes.Count == 0)
                NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                {
                    IsSucceeded = false,
                    Message = "The workstationIds, LoungeIDs and Airpots are required"
                });

            try
            {
                var paramRequest = loungeIds.Select(c => new CapacityDataRequest() { LoungeId = c, ClientId = WebApiServiceContext.Principal.ClientId }).ToList();

                var resultCapacityDataRequest = WebApiServiceContext.GetCapacities(paramRequest);

                if (resultCapacityDataRequest == null || resultCapacityDataRequest.ReturnParam == null)
                {
                    NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                    {
                        IsSucceeded = false,
                        ErrorCode = (int)ErrorCode.UnknownException,
                        Message = "Retreived occupancy list is empty"
                    });
                    return;
                }

                List<LoungeDataRequest> payload = new List<LoungeDataRequest>();
                var capacityResult = resultCapacityDataRequest.ReturnParam;

                for (int i = 0; i < capacityResult.Count; i++)
                {
                    payload.Add(new LoungeDataRequest()
                    {
                        Capacity = capacityResult[i].Capacity,
                        PAXAvgStayMinutes = capacityResult[i].PAXAvgStayMinutes,
                        LoungeId = loungeIds[i],
                        WorkstationId = workstationIds[i],
                        AirportCode = airportCodes[i],
                        FromLocalDate = startLocalDate,
                        ToLocalDate = endLocalDate,
                        FromUtcDate = startLocalDate.ToUniversalTime(),
                        ToUtcDate = endLocalDate.ToUniversalTime(),
                        IntervalInSeconds = intervalInSeconds
                    });
                }

                _proxy.SendJson<ResultPack<List<LoungeOccupancyRecord>>>("retrieveoccupancyforlounges", payload, DefaultMobileApplication.Current.Position.ToContractPosition(),
                        (successResponse, log) =>
                        {
                            try
                            {
                                LogsRepository.TrackEvents(log);

                                var result = (ResultPack<List<LoungeOccupancyRecord>>)successResponse;
                                if (result.IsSucceeded)
                                {
                                    if (result.ReturnParam == null)
                                    {
                                        NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                                        {
                                            IsSucceeded = false,
                                            ErrorCode = (int)ErrorCode.UnknownException,
                                            Message = String.Format("Retreived occupancy list is empty {0}", result.Message)
                                        });
                                        return;
                                    }

                                    NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                                    {
                                        ReturnParam = result.ReturnParam.Select(x => new LoungeOccupancy
                                        {
                                            LoungeId = x.LoungeId,
                                            Percentage = x.OccupancyPercentage,
                                            Time = x.OccupancyTime,
                                            OccupancyTotal = x.OccupancyTotal > 0 ? x.OccupancyTotal : 0,
                                            OccupancyTypePassenger = x.OccupancyTypePassenger,
                                            OccupancyCapacity = payload.SingleOrDefault(c => c.LoungeId == x.LoungeId)?.Capacity
                                        }).ToList(),
                                        IsSucceeded = result.IsSucceeded
                                    });
                                }
                                else
                                {
                                    LogsRepository.AddError("Error in retrieveoccupancies result processing", result.Message);
                                    NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                                    {
                                        IsSucceeded = false,
                                        ErrorCode = (int)ErrorCode.UnknownException,
                                        Message = result.Message
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Error in retrieveoccupancies successResponse processing", ex);
                                NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                                {
                                    IsSucceeded = false,
                                    ErrorCode = (int)ErrorCode.UnknownException,
                                    Message = LanguageRepository.Current.GetText((int)ErrorCode.UnknownException),
                                    ErrorMetadata = ex.StackTrace
                                });
                            }
                        },
                        (failed, log) =>
                        {
                            LogsRepository.AddError("Error in result of post retrieveoccupancies call", failed.Message);
                            NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                            {
                                IsSucceeded = false,
                                Message = "Error in result of post retrieveoccupancies call",
                                ErrorMetadata = failed.Message
                            });
                        });
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in post retrieveoccupancies call", ex);
                NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                {
                    IsSucceeded = false,
                    Message = "Error in post retrieveoccupancies call",
                    ErrorMetadata = ex.StackTrace
                });
            }
        }

        public void RetrieveCachedLoungeOccupancies(decimal loungeId, decimal workstationId, string airportCode, DateTime startLocalDate, DateTime endLocalDate, int intervalInSeconds)
        {
            if (loungeId == 0 || workstationId == 0)
                NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                {
                    IsSucceeded = false,
                    Message = "The workstationIds or LoungeIDs are required"
                });

            try
            {
                var paramRequest = new CapacityDataRequest() { LoungeId = loungeId, ClientId = WebApiServiceContext.Principal.ClientId };

                var resultCapacityDataRequest = WebApiServiceContext.GetCapacity(paramRequest);

                if (resultCapacityDataRequest == null || resultCapacityDataRequest.ReturnParam == null)
                {
                    NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                    {
                        IsSucceeded = false,
                        ErrorCode = (int)ErrorCode.UnknownException,
                        Message = "Retreived occupancy list is empty"
                    });
                    return;
                }

                var payload = new LoungeDataRequest()
                {
                    Capacity = resultCapacityDataRequest.ReturnParam.Capacity,
                    PAXAvgStayMinutes = resultCapacityDataRequest.ReturnParam.PAXAvgStayMinutes,
                    LoungeId = resultCapacityDataRequest.ReturnParam.LoungeId,
                    WorkstationId = workstationId,
                    AirportCode = airportCode,
                    FromLocalDate = startLocalDate,
                    ToLocalDate = endLocalDate,
                    FromUtcDate = startLocalDate.ToUniversalTime(),
                    ToUtcDate = endLocalDate.ToUniversalTime(),
                    IntervalInSeconds = intervalInSeconds
                };

                _proxy.SendJson<ResultPack<LoungeRecord<OccupancyElapsedData>>>("retrievecachedloungeoccupancies", payload, DefaultMobileApplication.Current.Position.ToContractPosition(),
                        (successResponse, log) =>
                        {
                            try
                            {
                                LogsRepository.TrackEvents(log);

                                var result = (ResultPack<LoungeRecord<OccupancyElapsedData>>)successResponse;
                                if (result.IsSucceeded)
                                {
                                    if (result.ReturnParam == null)
                                    {
                                        NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                                        {
                                            IsSucceeded = false,
                                            ErrorCode = (int)ErrorCode.UnknownException,
                                            Message = String.Format("Retreived occupancy list is empty {0}", result.Message)
                                        });
                                        return;
                                    }

                                    NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                                    {
                                        ReturnParam = new LoungeOccupancyElapsedData()
                                        {
                                            ClientId = result.ReturnParam.ClientId,
                                            LoungeId = result.ReturnParam.LoungeId,
                                            LoungeName = result.ReturnParam.LoungeName,
                                            WorkstationName = result.ReturnParam.WorkstationName,
                                            Capacity = result.ReturnParam.Capacity,
                                            OccupancyRecords = result.ReturnParam.OccupancyRecords,
                                        },
                                        IsSucceeded = result.IsSucceeded
                                    });
                                }
                                else
                                {
                                    LogsRepository.AddError("Error in retrieveoccupancies result processing", result.Message);
                                    NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                                    {
                                        IsSucceeded = false,
                                        ErrorCode = (int)ErrorCode.UnknownException,
                                        Message = result.Message
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Error in retrieveoccupancies successResponse processing", ex);
                                NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                                {
                                    IsSucceeded = false,
                                    ErrorCode = (int)ErrorCode.UnknownException,
                                    Message = LanguageRepository.Current.GetText((int)ErrorCode.UnknownException),
                                    ErrorMetadata = ex.StackTrace
                                });
                            }
                        },
                        (failed, log) =>
                        {
                            LogsRepository.AddError("Error in result of post retrieveoccupancies call", failed.Message);
                            NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                            {
                                IsSucceeded = false,
                                Message = "Error in result of post retrieveoccupancies call",
                                ErrorMetadata = failed.Message
                            });
                        });
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in post retrieveoccupancies call", ex);
                NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                {
                    IsSucceeded = false,
                    Message = "Error in post retrieveoccupancies call",
                    ErrorMetadata = ex.StackTrace
                });
            }
        }

        public void GetDwellLoungeOccupancies(decimal loungeId, decimal workstationId, string airportCode, DateTime startLocalDate, DateTime endLocalDate, int intervalInSeconds)
        {
            if (loungeId == 0 || workstationId == 0)
                NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                {
                    IsSucceeded = false,
                    Message = "The workstationIds or LoungeIDs are required"
                });

            try
            {
                var paramRequest = new CapacityDataRequest() { LoungeId = loungeId, ClientId = WebApiServiceContext.Principal.ClientId };

                var resultCapacityDataRequest = WebApiServiceContext.GetCapacity(paramRequest);

                if (resultCapacityDataRequest == null || resultCapacityDataRequest.ReturnParam == null)
                {
                    NotifyLoungeOccupancyListReceived(new ResultPack<List<LoungeOccupancy>>
                    {
                        IsSucceeded = false,
                        ErrorCode = (int)ErrorCode.UnknownException,
                        Message = "Retreived occupancy list is empty"
                    });
                    return;
                }

                var payload = new LoungeDataRequest()
                {
                    Capacity = resultCapacityDataRequest.ReturnParam.Capacity,
                    PAXAvgStayMinutes = resultCapacityDataRequest.ReturnParam.PAXAvgStayMinutes,
                    LoungeId = resultCapacityDataRequest.ReturnParam.LoungeId,
                    WorkstationId = workstationId,
                    AirportCode = airportCode,
                    FromLocalDate = startLocalDate,
                    ToLocalDate = endLocalDate,
                    FromUtcDate = startLocalDate.ToUniversalTime(),
                    ToUtcDate = endLocalDate.ToUniversalTime(),
                    IntervalInSeconds = intervalInSeconds
                };

                _proxy.SendJson<ResultPack<LoungeRecord<OccupancyElapsedData>>>("retrievedwelloccupancy", payload, DefaultMobileApplication.Current.Position.ToContractPosition(),
                        (successResponse, log) =>
                        {
                            try
                            {
                                LogsRepository.TrackEvents(log);

                                var result = (ResultPack<LoungeRecord<OccupancyElapsedData>>)successResponse;
                                if (result.IsSucceeded)
                                {
                                    if (result.ReturnParam == null)
                                    {
                                        NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                                        {
                                            IsSucceeded = false,
                                            ErrorCode = (int)ErrorCode.UnknownException,
                                            Message = String.Format("Retreived occupancy list is empty {0}", result.Message)
                                        });
                                        return;
                                    }

                                    NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                                    {
                                        ReturnParam = new LoungeOccupancyElapsedData()
                                        {
                                            ClientId = result.ReturnParam.ClientId,
                                            LoungeId = result.ReturnParam.LoungeId,
                                            LoungeName = result.ReturnParam.LoungeName,
                                            WorkstationName = result.ReturnParam.WorkstationName,
                                            Capacity = result.ReturnParam.Capacity,
                                            OccupancyRecords = result.ReturnParam.OccupancyRecords,
                                        },
                                        IsSucceeded = result.IsSucceeded
                                    });
                                }
                                else
                                {
                                    LogsRepository.AddError("Error in retrieveoccupancies result processing", result.Message);
                                    NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                                    {
                                        IsSucceeded = false,
                                        ErrorCode = (int)ErrorCode.UnknownException,
                                        Message = result.Message
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Error in retrieveoccupancies successResponse processing", ex);
                                NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                                {
                                    IsSucceeded = false,
                                    ErrorCode = (int)ErrorCode.UnknownException,
                                    Message = LanguageRepository.Current.GetText((int)ErrorCode.UnknownException),
                                    ErrorMetadata = ex.StackTrace
                                });
                            }
                        },
                        (failed, log) =>
                        {
                            LogsRepository.AddError("Error in result of post retrieveoccupancies call", failed.Message);
                            NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                            {
                                IsSucceeded = false,
                                Message = "Error in result of post retrieveoccupancies call",
                                ErrorMetadata = failed.Message
                            });
                        });
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in post retrieveoccupancies call", ex);
                NotifyLoungeOccupancyCachedReceived(new ResultPack<LoungeOccupancyElapsedData>
                {
                    IsSucceeded = false,
                    Message = "Error in post retrieveoccupancies call",
                    ErrorMetadata = ex.StackTrace
                });
            }
        }

        #endregion

        #region Private Members
        private void NotifyLoungeOccupancyReceived(ResultPack<LoungeOccupancy> occupancy)
        {
            if (OnLoungeOccupancyReceived == null || OnLoungeOccupancyReceived == null || occupancy == null) return;

            OnLoungeOccupancyReceived.Invoke(
                occupancy.IsSucceeded, occupancy.ErrorCode, occupancy.ErrorMetadata, occupancy.Message,
                occupancy.ReturnParam);
        }

        private void NotifyLoungeOccupancyListReceived(ResultPack<List<LoungeOccupancy>> occupancyList)
        {
            if (OnLoungeOccupancyListReceived == null || OnLoungeOccupancyListReceived == null || occupancyList == null) return;

            OnLoungeOccupancyListReceived.Invoke(
                occupancyList.IsSucceeded, occupancyList.ErrorCode, occupancyList.ErrorMetadata, occupancyList.Message,
                occupancyList.ReturnParam);
        }

        private void NotifyLoungeOccupancyCachedReceived(ResultPack<LoungeOccupancyElapsedData> occupancyList)
        {
            if (OnLoungeOccupancyCachdReceived == null || OnLoungeOccupancyCachdReceived == null || occupancyList == null) return;

            OnLoungeOccupancyCachdReceived.Invoke(
                occupancyList.IsSucceeded, occupancyList.ErrorCode, occupancyList.ErrorMetadata, occupancyList.Message,
                occupancyList.ReturnParam);
        }

        #endregion

        #region Queue Implementation 
#if DEBUG
        private static TimeSpan occupancyRequestTimeout = TimeSpan.FromSeconds(10);
#else
        private static TimeSpan occupancyRequestTimeout = TimeSpan.FromMinutes(2);
#endif
        private static bool isWorking = false;
        private static Task requestChecker;
        private static object requestLocker = new object();
        private static object sentRequestLocker = new object();
        private static object isWorkingLocker = new object();
        private static object timerLocker = new object();
        private static Timer timer;
        const int requestInterval = 1000;
        const int maxParallelRequests = 2;


        private static Queue<KeyValuePair<LoungeDataRequest, LoungeOccupancyReceivedHandler>> PendingRequests { get; set; } = new Queue<KeyValuePair<LoungeDataRequest, LoungeOccupancyReceivedHandler>>();

        private static List<LoungeDataRequest> SentRequests { get; set; } = new List<LoungeDataRequest>();

        public bool IsWorking
        {
            get
            {
                lock (isWorkingLocker)
                {
                    return isWorking;
                }
            }
            set
            {
                lock (isWorkingLocker)
                {
                    isWorking = value;
                }
            }
        }

        public static void ClearQueue()
        {
            try {
                PendingRequests.Clear ();
            }
            catch {

            }
        }

        public void LoadOccupancyWithQueue(Models.Workstation workstation, LoungeOccupancyReceivedHandler callback)
        {
            if (callback == null || workstation == null || workstation.LoungeID == 0)
            {
                NotifyLoungeOccupancyReceived(new ResultPack<LoungeOccupancy>
                {
                    IsSucceeded = false,
                    Message = "Selected Lounge is null or the ID is zero or callback is null"
                });

                return;
            }

            var dateTimeNow = DateTime.Now;

            var loungeDataRequest = new LoungeDataRequest
            {
                LoungeId = workstation.LoungeID,
                Capacity = workstation.Capacity == 0 ? 100 : workstation.Capacity,
               // Capacity = workstation.Capacity,
                PAXAvgStayMinutes = workstation.PAXAvgStayMinutes,
                WorkstationId = workstation.WorkstationID,
                AirportCode = workstation.AirportCode,
                ToLocalDate = dateTimeNow,
                FromLocalDate = dateTimeNow,
                ToUtcDate = dateTimeNow.ToUniversalTime(),
                FromUtcDate = dateTimeNow.ToUniversalTime()
            };

            bool isInSent = false;
            lock (sentRequestLocker)
            {
                isInSent = SentRequests.Any(x => x.LoungeId == loungeDataRequest.LoungeId && x.WorkstationId == loungeDataRequest.WorkstationId);
            }
            lock (requestLocker)
            {
                if (!PendingRequests.Any(x => x.Key.LoungeId == loungeDataRequest.LoungeId && x.Key.WorkstationId == loungeDataRequest.WorkstationId) && !isInSent)
                    PendingRequests.Enqueue(new KeyValuePair<LoungeDataRequest, LoungeOccupancyReceivedHandler>(loungeDataRequest, callback));
            }

            CheckCalls();
        }

        private void CheckCalls()
        {
            if (!IsWorking || requestChecker.Status != TaskStatus.Running)
            {
                IsWorking = true;

                requestChecker = new Task(new Action(() =>
                {
                    lock (timerLocker)
                    {
                        if (timer == null)
                            timer = new Timer(OnTimerCalledBack, null, requestInterval, requestInterval);
                    }
                }));
                requestChecker.Start();
            }
        }

        private void OnTimerCalledBack(object state)
        {
            int sentReqCount = 0, reqCount = 0;
            lock (sentRequestLocker)
            {
                var expiringOnes = SentRequests.Where(x => x.UtcGeneratedTime < DateTime.UtcNow.Subtract(occupancyRequestTimeout)).ToList();
                foreach (var eo in expiringOnes)//To timeout the requests that are taking long
                {
                    SentRequests.Remove(eo);
                }
                sentReqCount = SentRequests.Count;
            }

            lock (requestLocker)
            {
                reqCount = PendingRequests.Count;
            }

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("Occupancy Requests queue size: {0}\tPending requests: {1}", reqCount, sentReqCount));
#endif
            if (sentReqCount >= maxParallelRequests)
                return;
            //practically when we get the value of the property the locker locks and returns the value and unlocks
            //Then outside of property (after locking) you are accessing the *.Count which is not locked anymore


            //OR
            //while (SentRequests.Count >= maxParallelRequests)
            //{
            //    Task.Delay(100).Wait();
            //}

            if (reqCount > 0)
            {
                KeyValuePair<LoungeDataRequest, LoungeOccupancyReceivedHandler> req;

                lock (requestLocker)
                {
                    req = PendingRequests.Dequeue();
                }

                lock (sentRequestLocker)
                {
                    SentRequests.Add(req.Key);
                }
                try
                {
#if DEBUG
    //                return;
#endif

                    _proxy.SendJson<ResultPack<LoungeOccupancyRecord>>("retrieveoccupancy", req.Key,
                       DefaultMobileApplication.Current.Position.ToContractPosition(),
                            (successResponse, log) =>
                            {
                                lock (sentRequestLocker)
                                {
                                    SentRequests.Remove(req.Key);
                                }

                                try
                                {
                                    LogsRepository.TrackEvents(log);

#if DEBUG
                                    System.Diagnostics.Debug.WriteLine("<<<<< retrieveoccupancy Done: Lounge {0} >>>>>>", req.Key.LoungeId);
#endif

                                    var result = (ResultPack<LoungeOccupancyRecord>)successResponse;
                                    if (result.IsSucceeded)
                                    {
                                        if (result.ReturnParam == null)
                                        {
                                            req.Value.Invoke(
                                                false,
                                                (int)ErrorCode.UnknownException,
                                                result.ErrorMetadata,
                                                String.Format("Retreived occupancy is empty {0}", result.Message), null
                                            );
                                            return;
                                        }

                                        req.Value.Invoke(true, null, null, null,
                                            new LoungeOccupancy
                                            {
                                                Percentage = result.ReturnParam.OccupancyPercentage,
                                                Time = result.ReturnParam.OccupancyTime
                                            }
                                        );
                                    }
                                    else
                                    {
                                        LogsRepository.AddError("Error in loungeoccupancy result processing", result.Message);
                                        req.Value.Invoke(
                                                false,
                                                (int)ErrorCode.UnknownException,
                                                result.ErrorMetadata,
                                                result.Message, null
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogsRepository.AddError("Error in loungeoccupancy successResponse processing", ex);
                                    req.Value.Invoke(
                                                false,
                                                (int)ErrorCode.UnknownException,
                                                ex.StackTrace,
                                                LanguageRepository.Current.GetText((int)ErrorCode.UnknownException), null
                                            );
                                }
                            },
                            (failed, log) =>
                            {
                                lock (sentRequestLocker)
                                {
                                    SentRequests.Remove(req.Key);
                                }

                                LogsRepository.AddError("Error in result of get loungeoccupancy call", failed.Message);

                                req.Value.Invoke(
                                                false,
                                                null,
                                                failed.Message,
                                                "Error in result of get loungeoccupancy call",
                                                null
                                            );
                            });
                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Error in get loungeoccupancy call", ex);
                    req.Value.Invoke(
                                    false,
                                    null,
                                    ex.StackTrace,
                                    "Error in result of get loungeoccupancy call",
                                    null
                                );
                }
            }
            else
            {
                IsWorking = false;
                return;
            }

        }

        public void StopTimer()
        {
            if (timer == null)
                return;

            timer.Dispose();
            timer = null;
        }
        #endregion
    }

    internal delegate void TimerCallback(object state);

    internal sealed class Timer : CancellationTokenSource, IDisposable
    {
        public Timer(TimerCallback callback, object state, int dueTime, int period)
        {
            Task.Delay(dueTime, Token).ContinueWith(async (t, s) =>
            {
                var tuple = (Tuple<TimerCallback, object>)s;

                while (true)
                {
                    if (IsCancellationRequested)
                        break;

                    Task.Run(() => tuple.Item1(tuple.Item2));
                    await Task.Delay(period);
                }

            }, Tuple.Create(callback, state), CancellationToken.None,
                TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Default);
        }

        public new void Dispose()
        {
            base.Cancel();
            base.Dispose();
        }
    }
}