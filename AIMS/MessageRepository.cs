﻿using AIMS.Contracts;
using AIMS.Contracts.AIMSClient;
using AIMS.DAL;
using AIMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AIMS
{
    public class MessageRepository : RepositoryBase
    {
        protected static IEnumerable<Models.Message> Messages
        {
            get;
            set;
        }

        public MessageRepository() : base()//(new WebApiServiceContext())
        {

        }

        public void LoadAllMessages()
        {
            try
            {
                string localErrMessage = string.Empty;

                _proxy.GetJson<ResultPack<List<BULLETIN>>>(
                    //string.Format("https://aims2.ieg-america.com/aims_2_services/mobile/MobileWebAPI_AC/workstation/{0}/bulletins", 
                    string.Format("workstation/{0}/bulletins",
                    MembershipProvider.Current.SelectedLounge.WorkstationID),
                    DefaultMobileApplication.Current.Position.ToContractPosition(),
                    (aimsSvrResponse, log) =>
                    {
                        try
                        {
                            var res = (ResultPack<List<BULLETIN>>)aimsSvrResponse;
                            if (res.ReturnParam == null) Messages = new List<Models.Message>();
                            else Messages = res.ReturnParam.Select(x => x.ConvertToMessage()).ToList();
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Error in loading bulletins", ex);
                            Messages = new List<Models.Message>();
                        }
                        NotifyRepositoryChanged(Messages);
                    },
                    (aimsResult, log) =>
                    {
                        LogsRepository.AddError("Error in loading bulletins", aimsResult.ReturnParam);
                        Messages = new List<Models.Message>();
                        NotifyRepositoryChanged(Messages);
                    });
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in GetBulletins", ex);
                Messages = new List<Models.Message>();
                NotifyRepositoryChanged(Messages);
            }
        }

        public void LoadMessageBody(string handle, ReporitoryChangedHandler<string> callback)
        {
            try
            {
                //_proxy.GetJson<ResultPack<byte[]>>(string.Format("https://aims2.ieg-america.com/aims_2_services/mobile/MobileWebAPI_AC/image/{0}/", handle), DefaultMobileApplication.Current.Position,
                _proxy.GetJson<ResultPack<byte[]>>(string.Format("image/{0}/", handle), DefaultMobileApplication.Current.Position.ToContractPosition(),
                    (aimsSvrResponse, log) =>
                    {
                        string message = string.Empty;
                        try
                        {
                            var res = (ResultPack<byte[]>)aimsSvrResponse;
                            if (res.ReturnParam.Length > 0)
                            {
                                MemoryStream ms = new MemoryStream(res.ReturnParam);

                                var array = ms.ToArray();
                                message = Encoding.UTF8.GetString(array, 0, array.Length);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Error in EndLoadMessageBody", ex);
                        }
                        if (callback != null) callback.Invoke(message);
                        //NotifyRepositoryChanged(message);
                    },
                    (aimsResult, log) =>
                    {
                        LogsRepository.AddError("Error in LoadMessageBody inside GetImage", aimsResult.ReturnParam);
                        if (callback != null) callback.Invoke("");
                    });
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in BeingLoadMessageBody", ex);
                if (callback != null) callback.Invoke("");
            }
        }
    }
}