﻿using AIMS.Commons;
using AIMS.Contracts;
using AIMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS
{
    internal static class GpsValidationProvider
    {
        private static bool IsGeolocationValid(List<Position> positions, double latitude, double longitude, float accuracy)
        {
            if (DefaultMobileApplication.Current != null && 
                DefaultMobileApplication.Current.CheckAccuracy && accuracy > 200)
            {
                LogsRepository.AddClientError("GPS Error", string.Format("The GPS accuracy returned by your device is {0}", accuracy));
                LogsRepository.AddError("Error in GpsValidationProvider.IsGeolocationValid()", string.Format("The GPS accuracy was {0}, when user was standing at the Position({1}, {2})", accuracy, latitude, longitude));
                return false;
            }

            if (positions == null || positions.Count == 0) return false;
            
            var result = positions.Any(userLocation =>
            {
                var dist = GeoCalculator.CalculateDistance(userLocation.Latitude, userLocation.Longitude, latitude, longitude, GeoCalculatorMeasurementTypes.Kilometers) * 1000;
                dist -= accuracy;//to go easy with GPS accuracy problems
                var res = dist <= userLocation.MaxGpsFence;
                return res;
            });
            
            return result;
        }

        public static bool CheckValidity()
        {
            try
            {
                bool isSucceeded = false;

                //Check if user GEO location validation should be bypassed
                if (WebApiServiceContext.Configurations != null &&
                    WebApiServiceContext.Configurations.Exceptions != null &&
                    WebApiServiceContext.Configurations.Exceptions.DisableGPSCheck.HasValue &&
                    WebApiServiceContext.Configurations.Exceptions.DisableGPSCheck.Value)
                    return true;

                //Now the user must have fences
                if (WebApiServiceContext.Configurations.Fences == null || 
                    WebApiServiceContext.Configurations.Fences.Count == 0 ||
                    WebApiServiceContext.Configurations.Fences.All(x=>x.Positions.Count() == 0))
                    return isSucceeded;

                //If GPS of the device is returning position
                if (DefaultMobileApplication.Current.Position == null)
                {
                    LogsRepository.AddClientError("GPS Error", "Your device is not returning any GPS positions");
                    LogsRepository.AddError("Error in GpsValidationProvider.CheckValidity()", "Your device is not returning any GPS positions");
                    return isSucceeded;
                }


                //Why it was here???? (Only check the location validation)
                //if (MembershipProvider.Current == null || MembershipProvider.Current.LoggedinUser == null)
                //    return isSucceeded;


                var positions = WebApiServiceContext.Configurations.Fences.SelectMany(x=>x.Positions.ToList()).ToList();
                var position = DefaultMobileApplication.Current.Position;

                if (!position.Accuracy.HasValue)
                {
                    LogsRepository.AddClientError("GPS Error", "Your device is not returning the GPS accuracy");
                    LogsRepository.AddError("Error in GpsValidationProvider.CheckValidity()", string.Format("The device GPS accuracy was null, when user was standing at the Position({0}, {1})", position.Latitude, position.Longitude));
                    return isSucceeded;
                }

                isSucceeded = IsGeolocationValid(positions, position.Latitude, position.Longitude, position.Accuracy.Value);
                #region Thoughts
                /*
Suggestion/thoughts
20 seconds out of zone -> warn (vibrate?) that out of zone.
30 seconds out of zone -> warn (vibrate?) that out of zone and is locked pending return to zone. Option to logout.
40 seconds out of zone -> logout
To review if lounges are a few seconds apart. 
                 */
                #endregion
                return isSucceeded;
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Exception in GpsValidationProvider.CheckValidity()", ex);
                return false;
            }
        }
    }
}
