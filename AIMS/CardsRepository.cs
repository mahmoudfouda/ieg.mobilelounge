﻿using AIMS.Contracts;
using AIMS.DAL;
using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMS
{
    internal class CardAdorner : Models.Card, Models.IAdorner
    {
        //public object DALObject
        //{
        //    get
        //    {
        //        return this._card;
        //    }
        //}

        public CardAdorner(DAL.Card card)
        {
            if (card == null)
                throw new ArgumentNullException("card");
            else
            {
                this._card = card;
                #region Converting supported services
                //TODO: update services with names
                var services = new List<Models.PassengerService>();
                if (card.ComplimentaryGuests > 0)
                    services.Add(new Models.PassengerService
                    {
                        Id = (int)PassengerServiceType.GuestComplimentary,
                        Name = DefaultMobileApplication.Current.TextProvider.GetText(2162), //"Complementary"
                        MaximumQuantity = (int)card.ComplimentaryGuests
                    });
                if (card.FamilyGuests > 0)
                    services.Add(new Models.PassengerService
                    {
                        Id = (int)PassengerServiceType.GuestFamily,
                        Name = DefaultMobileApplication.Current.TextProvider.GetText(2163), //"Family",
                        MaximumQuantity = (int)card.FamilyGuests
                    });

                if (card.PaidGuests > 0)
                    services.Add(new Models.PassengerService
                    {
                        Id = (int)PassengerServiceType.GuestPaid,
                        Name = DefaultMobileApplication.Current.TextProvider.GetText(2181), //"Paid",
                        MaximumQuantity = (int)card.PaidGuests
                    });
                this.AllowedServices = services;
                #endregion
            }
        }
    }

    public class CardsRepository : AirlinesRepository
    {
        #region Properties

        #endregion

        public CardsRepository() : base()
        {
            OnChangeNeeded = CardsRepository_OnChangeNeededSpecified;
        }

        private void CardsRepository_OnChangeNeededSpecified(bool isNeeded)
        {
            if (isNeeded) ReloadAirlinesAndCardsFromServer();
            else NotifyRepositoryChanged(GetAirlineCards());
        }
        
        public void SearchCards(string keyword)
        {
            if(string.IsNullOrWhiteSpace(keyword))
            {
                NotifyRepositoryChanged(null);
                return;
            }

            var cards = GetAirlineCards();
            if (cards == null || cards.Count == 0)
            {
                NotifyRepositoryChanged(null);
                return;
            }

            var result = cards.Where(x => x.Name.Contains(keyword)).Distinct().ToList();

            NotifyRepositoryChanged(result);
        }
        
        public void LoadCards(bool forceGet = false)
        {
            try
            {
                if (forceGet)
                {
                    ReloadAirlinesAndCardsFromServer();
                }
                else
                {
                    Cards = CardDBAdapter.Current.GetAllCards().Select(x => new CardAdorner(x)).ToList();

                    CheckIfSynchronizationNeeded();
                }
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in loading Cards", ex);
                Airlines = new List<Models.Airline>();
                Cards = new List<Models.Card>();
            }
        }
        
        public Models.Card GetCard(decimal cardId)
        {
            return Cards.FirstOrDefault(x => x.ID == cardId);
        }

        public static Models.Card GetCardFromDB(decimal cardId)
        {
            try
            {
                var dbCard = CardDBAdapter.Current.GetCard(cardId);
                if (dbCard == null) return null;
                var res = new CardAdorner(dbCard);
                return res;
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in GetCardFromDB", ex);
                return null;
            }
        }

        /// <summary>
        /// Calculates and returns the guest services and their limitations based on all cardIds
        /// </summary>
        /// <param name="cardIds">a decimal list of card ids</param>
        /// <returns>List of Passenger Services with all the service limitations</returns>
        public static List<PassengerService> GetCardServices(IEnumerable<decimal> cardIds)
        {
            if (cardIds == null || cardIds.Count() == 0) return new List<PassengerService>();
            var passengerServices = new List<PassengerService>();

            foreach (var cardId in cardIds)
            {
                var card = GetCardFromDB(cardId);
                if (card != null)
                {
                    foreach (var service in card.AllowedServices)
                    {
                        var s = passengerServices.FirstOrDefault(x => x.Id == service.Id);
                        if (s != null)
                            s.MaximumQuantity += service.MaximumQuantity;
                        else passengerServices.Add(service);
                    }
                }
            }

            return passengerServices;
        }

        //public static OperationResult<byte[]> SaveCardImage(Models.Card card)
        //{
        //    if (card == null)
        //        return new OperationResult<byte[]>
        //        {
        //            IsSucceeded = false,
        //            Message = "Card object is null"
        //        };
        //    try
        //    {
        //        var dalCard = ((IAdorner)card).DALObject as DAL.Card;
        //        CardDBAdapter.Current.SaveCardImage(dalCard);
        //        return new OperationResult<byte[]>
        //        {
        //            IsSucceeded = true,
        //            ReturnParam = card.CardPictureBytes
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        LogsRepository.AddError("Error in saving Card with Image", ex);
        //        return new OperationResult<byte[]>
        //        {
        //            IsSucceeded = false,
        //            Message = ex.Message
        //        };
        //    }
        //}
    }
}
