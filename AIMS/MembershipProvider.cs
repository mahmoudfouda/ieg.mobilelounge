﻿using System;
using System.Collections.Generic;
using System.Linq;
using AIMS.DAL;
using AIMS.Contracts;
using AIMS.Contracts.AIMSClient;
using AIMS.Models;
using AIMS.Validators;
using AIMS.Contracts.Validators;
using AIMS.Contracts.DataContracts;
using System.Collections.ObjectModel;

namespace AIMS
{
    public class MembershipProvider
    {
        private IWebApiServiceContext _proxy;

        //private event EventHandler<Tuple<bool, ErrorCode>> OnLogin;

        /// <summary>
        /// Gets the loggedin user.
        /// </summary>
        /// <value>The currently loggedin user.</value>
        public Models.User LoggedinUser
        {
            get;
            private set;
        }

        public Models.User LoggedinDeveloper
        {
            get;
            private set;
        }

        public bool IsTokenExpired
        {
            get { return _proxy != null && _proxy.IsExpired; }
        }

        public bool IsAuthenticated
        {
            get
            {
                return LoggedinUser != null && _proxy != null && _proxy.IsAuthenticated;
            }
        }

        public IMobileValidator SecondValidator { get; private set; }

        public IEnumerable<Models.Workstation> UserLounges
        {
            get;
            private set;
        }

        public ReadOnlyCollection<ReadOnlyFence> WhiteFences { get; private set; }

        private Models.Workstation selectedLounge;
        public Models.Workstation SelectedLounge
        {
            get
            {
                return selectedLounge;
            }
            set
            {
                if (value != selectedLounge)
                {
                    selectedLounge = value;
                    if (value != null && LoggedinUser != null)
                    {
                        LoggedinUser.AirportName = value.AirportCode;
                        LoggedinUser.AirlineId = value.DefaultAirlineId;
                    }
                }
            }
        }

        public Models.Airline SelectedAirline
        {
            get;
            set;
        }

        private Models.Passenger selectedPassenger;
        public Models.Passenger SelectedPassenger
        {
            get
            {
                return selectedPassenger;
            }
            set
            {
                if (selectedPassenger != value)
                {
                    selectedPassenger = value;
                    SelectedGuest = null;
                    if (value != null)
                    {
                        selectedPassenger.WorkstationId = SelectedLounge.WorkstationID;
                        selectedPassenger.LoungeId = SelectedLounge.LoungeID;
                        try
                        {
                            if (SelectedCard == null || SelectedCard.ID != value.CardID)
                            {
                                var c = CardDBAdapter.Current.GetCard(value.CardID);
                                if (c != null)//TODO: Might cause bugs due to last change
                                    SelectedCard = new CardAdorner(c);//(Selecting the passengers card)
                                else SelectedCard = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Error in GetCard", ex);
                        }
                        try
                        {
                            if (SelectedAirline == null || SelectedAirline.ID != value.AirlineId)
                            {
                                var al = AirlineDBAdapter.Current.GetAirline(value.AirlineId);
                                if (al != null)
                                    SelectedAirline = new AirlineAdorner(al);//TODO: Select the passengers card
                                else SelectedAirline = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Error in GetAirline", ex);
                        }
                    }
                }
            }
        }

        public Models.Passenger SelectedGuest { get; set; }

        public Models.PassengerService SelectedPassengerService { get; set; }

        public Models.Card SelectedCard
        {
            get;
            set;
        }

        public List<BarcodeScannerSerialNumber> BarcodeScannerAPIKeys { get; private set; }

        public bool IsTrialUser
        {
            get
            {
                if (_proxy == null) return true;
                if (WebApiServiceContext.Configurations == null) return true;
                //if (WebApiServiceContext.Configurations.Configurations == null || WebApiServiceContext.Configurations.Configurations.Count == 0) return true;
                if (WebApiServiceContext.Configurations.Exceptions != null &&
                    WebApiServiceContext.Configurations.Exceptions.UseTrialBarcodeScanner.HasValue &&
                    WebApiServiceContext.Configurations.Exceptions.UseTrialBarcodeScanner.Value) return true;

                if (BarcodeScannerAPIKeys == null || BarcodeScannerAPIKeys.Count == 0) return true;

                return false;
            }
        }

        public string UserHeaderText
        {
            get
            {
                if (LoggedinUser == null)
                    return "";
                return string.Format("{0} | {1}", SelectedLounge.LoungeName, LoggedinUser.LastName);
            }
        }

        public string UserFullName
        {
            get
            {
                if (LoggedinUser == null) return "Unknown User";

                var hasFName = !string.IsNullOrWhiteSpace(LoggedinUser.FirstName);
                var hasLName = !string.IsNullOrWhiteSpace(LoggedinUser.LastName);

                return hasFName ?
                    string.Format("{0}{1}", LoggedinUser.FirstName,
                    (hasLName ? string.Format(" {0}", LoggedinUser.LastName) : "")) :
                    (hasLName ? LoggedinUser.LastName : "");
            }
        }

        //private static MembershipProvider _current;
        public static MembershipProvider Current
        {
            get;
            private set;
            //get
            //{
            //    return _current ?? (_current = new MembershipProvider());
            //}
            //private set
            //{
            //    _current = value;
            //}
        }

        private MembershipProvider(IWebApiServiceContext proxy)
        {
            _proxy = proxy;
            if (!_proxy.IsAuthenticated) return;

            //We are logged in here :D

            #region Synchronizing the LanguageTexts
            //TODO: Transfer all the language packs to the server side
            #endregion

            #region Retrieving and putting on the SNs
            //TODO: (Transfer to server side) /Or/ Obfuscate these lines of codes
            BarcodeScannerAPIKeys = new List<BarcodeScannerSerialNumber>();
            if (WebApiServiceContext.Configurations != null)
            {
                if (WebApiServiceContext.Configurations.Configurations != null)
                {
                    var serializedSerialNumbers = WebApiServiceContext.Configurations.Configurations.Where(x => x.Name.Equals(BarcodeScannerSerialNumber.SERIALIZER_IDENTIFIER)).ToList();
                    foreach (var sn in serializedSerialNumbers)
                    {
                        BarcodeScannerAPIKeys.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<BarcodeScannerSerialNumber>(sn.Value));
                    }
                }
                if (WebApiServiceContext.Configurations.Fences != null)
                {

                    try
                    {
                        WhiteFences = new ReadOnlyCollection<ReadOnlyFence>(WebApiServiceContext.Configurations.Fences.Select(x => new ReadOnlyFence(x)).ToList());
                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        var ee = ex.Message + "\n" + ex.StackTrace;
                        System.Diagnostics.Debug.WriteLine("Exception: " + ee);
#endif
                    }
                }
            }
            #endregion
        }

        public static MembershipProvider CreateMembershipProvider(string username, string password, System.Net.IWebProxy proxy)
        {
            if (DefaultMobileApplication.Current.CurrentDevice == null || string.IsNullOrWhiteSpace(DefaultMobileApplication.Current.CurrentDevice.DeviceUniqueId))
            {
                throw new Exception("Error in CreateMembershipProvider()", new Exception("the DefaultMobileApplication.Current.CurrentDevice is null or has no Serial Number set"));
            }
            else
                return Current = new MembershipProvider(new WebApiServiceContext(username, password, DefaultMobileApplication.Current.CurrentDevice, proxy));
        }

        private void NotifyLoginEvent(bool result, ErrorCode errorCode, EventHandler<Tuple<bool, ErrorCode>> onLogin)
        {
            if (onLogin != null)
                onLogin.Invoke(this, new Tuple<bool, ErrorCode>(result, errorCode));
        }

        private void ProceedToLounge(string username, string loungeId, EventHandler<Tuple<bool, ErrorCode>> onLoginCallBack)
        {
            //UserLounges = null;
            if (!string.IsNullOrEmpty(loungeId))
            {
                try
                {
                    //Getting the list of MobileWorkstations for the user assigned lounge IDs
                    var loungeIDs = loungeId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();

                    //Changed to post method to be able to process large number of lounge ids assigned to user
                    //_proxy.GetJson<ResultPack<List<WORKSTATION>>>(string.Format("lounge/{0}/workstations", loungeId), (Contracts.Position)DefaultMobileApplication.Current.Position,
                    _proxy.SendJson<ResultPack<List<WORKSTATION>>>("mobileworkstationsforlounges", loungeIDs, DefaultMobileApplication.Current.Position.ToContractPosition(),
                        (loungesResponse, log) =>
                        {
                            LogsRepository.TrackEvents(log);

                            var loungesResult = (ResultPack<List<WORKSTATION>>)loungesResponse;
                            if (loungesResult.IsSucceeded)
                            {
                                var tempLounges = loungesResult.ReturnParam.Select(x => x.ConvertToWorkstation()).ToList();
                                if (tempLounges.Count == 1 && tempLounges.First().WorkstationID == 0)
                                {
                                    LogsRepository.AddError("Error in Login - No workstations", string.Format("There are no workstations available for the user '{0}' [with LoungeID: {1}]", username, loungeId));
                                    NotifyLoginEvent(false, ErrorCode.NoLoungesForUser, onLoginCallBack);
                                }
                                else
                                {
                                    UserLounges = tempLounges;
                                    var capacities = WebApiServiceContext.GetCapacities(UserLounges.Select(x => new CapacityDataRequest
                                    {
                                        ClientId = WebApiServiceContext.Principal.ClientId,
                                        LoungeId = x.LoungeID
                                    }).ToList());

                                    if (capacities.IsSucceeded)
                                    {
                                        foreach (var cap in capacities.ReturnParam)
                                        {
                                            var lounge = UserLounges.FirstOrDefault(x => x.LoungeID == cap.LoungeId);
                                            if (lounge != null)
                                            {
                                                lounge.Capacity = cap.Capacity;
                                                lounge.PAXAvgStayMinutes = cap.PAXAvgStayMinutes;
                                            }
                                        }

#if DEBUG
                                        foreach (var longe in UserLounges.Where(c => c.Capacity == 0))
                                        {
                                            var cap = capacities.ReturnParam.FirstOrDefault(x => x.LoungeId == longe.LoungeID);
                                            longe.Capacity = cap.Capacity;
                                            longe.PAXAvgStayMinutes = cap.PAXAvgStayMinutes;
                                        }
#endif
                                    }

                                    if (UserLounges.Count() == 1)
                                        SelectedLounge = UserLounges.FirstOrDefault();
                                    NotifyLoginEvent(true, ErrorCode.NoError, onLoginCallBack);
                                }
                            }
                            else
                            {
                                LogsRepository.AddError("Error in getting WorkstationsForUser", loungesResult.Message);
                                NotifyLoginEvent(false, ErrorCode.WorkstationsError, onLoginCallBack);
                            }
                        },
                        (aimsResult, log) =>
                        {
                            LogsRepository.AddError("Error in EndGetMobileWorkstationsForUser", aimsResult.ReturnParam);
                            NotifyLoginEvent(false, ErrorCode.CommunicationError, onLoginCallBack);
                        });
                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Error in BeginGetMobileWorkstationsForUser", ex);
                    NotifyLoginEvent(false, ErrorCode.WorkstationRetrievalException, onLoginCallBack);
                }
            }
            else
            {
                LogsRepository.AddError("Error in Login - No lounges are available for this user");
                NotifyLoginEvent(false, ErrorCode.NoLoungesForUser, onLoginCallBack);
            }
        }

        public void Login(string userName, string password/*, string deviceUniqueId, Position position*/, EventHandler<Tuple<bool, ErrorCode>> onLoginCallBack)
        {
            string localErrMessage = string.Empty;

            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                NotifyLoginEvent(false, ErrorCode.IncompleteUserInfo, onLoginCallBack);
                return;
            }

            if (DefaultMobileApplication.Current.Position == null)
            {
                NotifyLoginEvent(false, ErrorCode.LocationRetrievalException, onLoginCallBack);
                return;
            }

            try
            {
                if (_proxy.IsAuthenticated)
                {
                    LoggedinUser = new Models.User
                    {
                        Username = userName,
                        FirstName = WebApiServiceContext.Principal.FirstName,
                        LastName = WebApiServiceContext.Principal.LastName,
                        //Password = password,
                        MobileKey = WebApiServiceContext.Principal.MobileKey
                    };

                    SecondValidator = SecondValidatorFactory.CreateValidator(LoggedinUser.MobileKey, WebApiServiceContext.Principal.DualTokenMobileKeysString);
                    SecondValidator.Authenticate(userName, password, LoggedinUser.MobileKey, _proxy, DefaultMobileApplication.Current.Position.ToContractPosition(), (tokenResult) =>
                    {
                        if (tokenResult.IsSucceeded)
                        {
                            //if (string.IsNullOrEmpty(WebApiServiceContext.Principal.LoungeIDs))
                            //{
                            //    LogsRepository.AddError("Error in ProceedToLounge", "No Lounge is assigned to you");
                            //    LogOutUser();
                            //    NotifyLoginEvent(false, ErrorCode.NoLoungesForUser, onLoginCallBack);
                            //}
                            //else
                            //{
                            //    var lounges = WebApiServiceContext.Principal.LoungeIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                            //    if (lounges.Count > 30)
                            //    {
                            //        LogsRepository.AddError("Error in ProceedToLounge", "Too many lounge Ids ar assigned to you");
                            //        LogOutUser();
                            //        NotifyLoginEvent(false, ErrorCode.WorkstationsError, onLoginCallBack);
                            //    }
                            //    else 
                            ProceedToLounge(userName, WebApiServiceContext.Principal.LoungeIDs, onLoginCallBack);
                            //}
                        }
                        else
                        {
                            LogsRepository.AddError("Error in RetrieveExternalToken", tokenResult.Message);
                            LogOutUser();
                            NotifyLoginEvent(false, ErrorCode.UserValidationError, onLoginCallBack);
                        }
                    });
                }
                else
                {
                    if (string.IsNullOrEmpty(_proxy.Message) || _proxy.Message.Equals("invalid_client") || _proxy.Message.Equals("Bad Request"))//TODO: hardcoded error text in AIMS | WebAPI
                        NotifyLoginEvent(false, ErrorCode.UserValidationError, onLoginCallBack);
                    else if (_proxy.Message.ToLower().Contains("blacklisted"))
                        NotifyLoginEvent(false, ErrorCode.BlackListed, onLoginCallBack);
                    else if (_proxy.Message.ToLower().Contains("service plan is expired"))
                        NotifyLoginEvent(false, ErrorCode.ServicePlanExpiredError, onLoginCallBack);
                    else if (_proxy.Message.ToLower().Contains("service plan is full"))
                        NotifyLoginEvent(false, ErrorCode.ServicePlanIsFullError, onLoginCallBack);
                    else if (_proxy.Message.ToLower().Equals("internal server error"))
                        NotifyLoginEvent(false, ErrorCode.UnknownException, onLoginCallBack);
                    else
                    {
                        LogsRepository.AddError("_proxy-error", new Exception(_proxy.Message));
                        NotifyLoginEvent(false, ErrorCode.CommunicationError, onLoginCallBack);
                    }
                }
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in BeginValidateUsernamePassword", ex);
                NotifyLoginEvent(false, ErrorCode.UserValidationException, onLoginCallBack);
            }

        }

        public void ValidateDeveloper(string devUsername, string devPassword, Ieg.Mobile.DataContracts.Models.Position position, EventHandler<Tuple<bool, ErrorCode>> onDeveloperLoginCallBack)
        {
            LoggedinDeveloper = null;
            if (string.IsNullOrWhiteSpace(devUsername) || string.IsNullOrWhiteSpace(devPassword))
            {
                NotifyLoginEvent(false, ErrorCode.IncompleteUserInfo, onDeveloperLoginCallBack);
                return;
            }

            if (position == null)
            {
                NotifyLoginEvent(false, ErrorCode.LocationRetrievalException, onDeveloperLoginCallBack);
                return;
            }

            if (_proxy.IsAuthenticated)
            {
                try
                {
                    //_proxy.GetJson<ResultPack<string>>(string.Format("authenticateDeveloper/{0}/{1}/", devUsername, devPassword), (Contracts.Position)position,
                    _proxy.SendJson<ResultPack<string>>("validatedeveloper", new UserValidationInput { Username = devUsername, Password = devPassword }, DefaultMobileApplication.Current.Position.ToContractPosition(), (devLoginSuccessResponse, log) =>
                    {
                        LogsRepository.TrackEvents(log);

                        var successResponse = (ResultPack<string>)devLoginSuccessResponse;
                        if (successResponse.IsSucceeded)
                        {
                            LogsRepository.AddClientInfo("Client has changed to Developer", string.Format("The client {0}:[{1}] logged in as a developer", UserFullName, LoggedinUser.Username));
                            LoggedinDeveloper = new Models.User
                            {
                                Username = devUsername,
                                //Password = devPassword,
                                LastLoginDate = DateTime.Now
                            };
                            NotifyLoginEvent(true, ErrorCode.NoError, onDeveloperLoginCallBack);
                        }
                        else
                        {
                            LogsRepository.AddError("Error in validatedeveloper", successResponse.Message);
                            NotifyLoginEvent(false, ErrorCode.UserValidationError, onDeveloperLoginCallBack);
                        }
                    },
                    (devLoginFailedResponse, log) =>
                    {
                        LogsRepository.AddError("Error in end validatedeveloper", devLoginFailedResponse.ReturnParam);
                        if (devLoginFailedResponse.ReturnParam != null && devLoginFailedResponse.ReturnParam.InnerException != null && devLoginFailedResponse.ReturnParam.InnerException.Message.Contains("401"))
                            NotifyLoginEvent(false, ErrorCode.AuthorizationError, onDeveloperLoginCallBack);
                        else
                            NotifyLoginEvent(false, ErrorCode.UnknownException, onDeveloperLoginCallBack);
                    });
                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Error in begin validatedeveloper", ex);
                    if (ex.InnerException != null && ex.InnerException.Message.Contains("401"))
                        NotifyLoginEvent(false, ErrorCode.AuthorizationError, onDeveloperLoginCallBack);
                    else
                        NotifyLoginEvent(false, ErrorCode.UserRetrievalException, onDeveloperLoginCallBack);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(_proxy.Message) || _proxy.Message.Equals("invalid_client") || _proxy.Message.Equals("Bad Request"))//TODO: hardcoded error text in AIMS | WebAPI
                    NotifyLoginEvent(false, ErrorCode.UserValidationError, onDeveloperLoginCallBack);
                else if (_proxy.Message.ToLower().Equals("internal server error"))
                    NotifyLoginEvent(false, ErrorCode.UnknownException, onDeveloperLoginCallBack);
                else NotifyLoginEvent(false, ErrorCode.InternetConnectionError, onDeveloperLoginCallBack);
            }
        }

        public void LogOutUser()
        {
            LoggedinUser = null;
            LoggedinDeveloper = null;
            _proxy.SignOutUser();
            Current = null;
        }

        public void PushUserLogs(List<Models.Log> logs, EventHandler<bool> callback = null)
        {
            if (logs == null || LoggedinUser == null || IsAuthenticated == false && callback != null) callback.Invoke(this, false);
            else
            {
                var stringLogs = new List<string>();
                foreach (var log in logs)
                {
                    stringLogs.Add(string.Format("[{0} {1}]\tLevel:{2}\tType:{3}\t\"{4}\" :\t\t{5}",
                        log.Time.ToString("ddd d MMM, yyyy", System.Globalization.CultureInfo.InvariantCulture),
                        log.Time.ToString("h:m:ss.fff tt", System.Globalization.CultureInfo.InvariantCulture),
                        log.LogLevel,
                        log.LogType,
                        log.Title,
                        log.Description));//TODO : we must have a general log (ToString) parser
                }
                //_proxy.SendJson<string>("https://aims2.ieg-america.com/aims_2_services/mobile/MobileWebAPI_AC/pushlogs", stringLogs, DefaultMobileApplication.Current.Position,
                _proxy.SendJson<string>("pushlogs", stringLogs, DefaultMobileApplication.Current.Position.ToContractPosition(),
                    (aimsSvrResponse, log) => {
                        LogsRepository.TrackEvents(log);

                        if (callback != null) callback.Invoke(this, true);
                    },
                    (aimsResult, log) => {
                        if (callback != null) callback.Invoke(this, false);
                    });
            }
        }
    }
}