﻿using AIMS.Contracts;
using AIMS.Contracts.AIMSClient;
using AIMS.DAL;
using AIMS.Models;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace AIMS
{
    public class RepositoryBase : INotifyRepositoryChanged, INotifyChangeNeeded, IDisposable
    {
        #region Delegates
        //public event ChangeNeededHandler OnChangeNeeded;
        //public event ReporitoryChangedHandler OnRepositoryChanged;
        public ChangeNeededHandler OnChangeNeeded { get; set; }
        public ReporitoryChangedHandler OnRepositoryChanged { get; set; }
        #endregion

        #region Fields
        protected IWebApiServiceContext _proxy;
        #endregion

        public RepositoryBase(IWebApiServiceContext customProxy)
        {
            _proxy = customProxy;
        }

        public RepositoryBase()
        {
            _proxy = new WebApiServiceContext();
        }

        public void CheckIfSynchronizationNeeded()
        {
            try
            {
                _proxy.GetJson<ResultPack<DateTime>>(string.Format("workstation/{0}/lastupdate", (int)MembershipProvider.Current.SelectedLounge.WorkstationID), DefaultMobileApplication.Current.Position.ToContractPosition(),
                    (aimsSvrResponse, log) =>
                    {
                        try
                        {
                            var res = (ResultPack<DateTime>)aimsSvrResponse;
                            if (!res.IsSucceeded)
                            {
                                NotifyChangeNeeded(true);
                                return;
                            }
                            var lastServerUpdate = res.ReturnParam;
                            var syncEvent = SyncEventDBAdapter.Current.GetLastSyncEvent((int)SyncScope.AccessDocuments);
                            if (syncEvent == null || syncEvent.SyncServerDate < lastServerUpdate)
                            {
                                SyncEventDBAdapter.Current.SaveSyncEvent(new SyncEvent
                                {
                                    Scope = (int)SyncScope.AccessDocuments,
                                    Description = "Local database is now up to date",//TODO: description can be changed
                                    SyncServerDate = lastServerUpdate
                                });
                                NotifyChangeNeeded(true);
                            }
                            else
                                NotifyChangeNeeded(false);
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Error in EndGetWorkstationLastUpdate", ex);
                            NotifyChangeNeeded(true);//When server is not responding (Siavash: I want the client UI not to work)
                        }
                    },
                    (aimsResult, log) =>
                    {
                        LogsRepository.AddError("Error in EndGetWorkstationLastUpdate", aimsResult.ReturnParam);
                        NotifyChangeNeeded(true);
                    });
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in BeginGetWorkstationLastUpdate", ex);
                NotifyChangeNeeded(true);
            }
        }

        //private void CallbackOn_GetWorkstationLastUpdate(IAsyncResult ar)
        //{
        //    string localErrMessage = string.Empty;
        //    IAimsClientService proxyAims = ar.AsyncState as AimsClientServiceClient;
        //    try
        //    {
        //        var lastServerUpdate = proxyAims.EndGetWorkstationLastUpdate(ref localErrMessage, ar);
        //        ((ICommunicationObject)proxyAims).Close();
        //        var syncEvent = SyncEventDBAdapter.Current.GetLastSyncEvent();
        //        if (syncEvent == null || syncEvent.SyncServerDate < lastServerUpdate)
        //        {
        //            SyncEventDBAdapter.Current.SaveSyncEvent(new SyncEvent
        //            {
        //                Description = "Local database is new up to date",//TODO: description can be changed
        //                SyncServerDate = lastServerUpdate
        //            });
        //            NotifyChangeNeeded(true);
        //        }
        //        else
        //            NotifyChangeNeeded(false);
        //    }
        //    catch (Exception ex)
        //    {
        //        ((ICommunicationObject)proxyAims).Abort();
        //        LogsRepository.AddError("Error in EndGetWorkstationLastUpdate", ex);
        //        NotifyChangeNeeded(true);//When server is not responding (Siavash: I want the client UI not to work)
        //    }
        //}

        public void NotifyRepositoryChanged(object objects)
        {
            if (OnRepositoryChanged != null)
                OnRepositoryChanged.Invoke(objects);
        }

        public void NotifyChangeNeeded(bool isNeeded)
        {
            if (OnChangeNeeded != null)
                OnChangeNeeded.Invoke(isNeeded);
        }

        public void Dispose()
        {
            _proxy.Dispose();
        }
    }
}