﻿using AIMS.Contracts;
using AIMS.DAL;
using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIMS
{
    internal class AirportAdorner : Models.Airport, Models.IAdorner
    {
        public AirportAdorner(DAL.Airport airport)
        {
            if (airport == null)
                throw new ArgumentNullException("airport");
            else this._airport = airport;
        }
    }

    public class AirportsRepository : RepositoryBase
    {
        #region Fields
        static object loaderLocker = new object();
        static object requestLocker = new object();
        static object sentRequestLocker = new object();
        static object isWorkingLocker = new object();
        private static Queue<KeyValuePair<string, ReporitoryChangedHandler<OperationResult<byte[]>>>> imageRequests = new Queue<KeyValuePair<string, ReporitoryChangedHandler<OperationResult<byte[]>>>>();
        private static List<string> sentRequests = new List<string>();

        private static bool isWorking = false;

        //private static Task requestChecker;
        #endregion

        #region Properties
        public static Queue<KeyValuePair<string, ReporitoryChangedHandler<OperationResult<byte[]>>>> PendingRequests
        {
            get
            {
                lock (requestLocker)
                {
                    return imageRequests;
                }
            }
            private set
            {
                lock (requestLocker)
                {
                    imageRequests = value;
                }
            }
        }

        public static List<string> SentRequests
        {
            get
            {
                lock (sentRequestLocker)
                {
                    return sentRequests;
                }
            }
            private set
            {
                lock (sentRequestLocker)
                {
                    sentRequests = value;
                }
            }
        }

        public bool IsWorking
        {
            get
            {
                lock (isWorkingLocker)
                {
                    return isWorking;
                }
            }
            set
            {
                lock (isWorkingLocker)
                {
                    isWorking = value;
                }
            }
        }

        protected static IEnumerable<Models.Airport> Airports { get; set; }
        #endregion

        public AirportsRepository() : base()
        {
            OnChangeNeeded = OnChangeNeededSpecified;
        }

        private void OnChangeNeededSpecified(bool isNeeded)
        {
            if (isNeeded) ReloadAirportsFromServer(true);
            else NotifyRepositoryChanged(Airports);
        }

        private void ReloadAirportsFromServer(bool cash = false)
        {
            lock (loaderLocker)
            {
                try
                {
                    string localErrMessage = string.Empty;

                    AirportDBAdapter.Current.DeleteAllAirports();

                    _proxy.GetJson<ResultPack<List<Models.Airport>>>("allairports", DefaultMobileApplication.Current.Position.ToContractPosition(),
                        (aimsSvrResponse, log) =>
                        {
                            try
                            {
                                var res = (ResultPack<List<Models.Airport>>)aimsSvrResponse;
                                Airports = res.ReturnParam;

                                #region Setting and saving Airlines
                                if (cash && (Airports != null || Airports.Count() > 0))
                                {
                                    var dalItems = Airports.Select(x => (DAL.Airport)x.DALObject).ToList();
                                    AirportDBAdapter.Current.SaveAirports(dalItems);
                                }
                                #endregion

                                NotifyRepositoryChanged(Airports);
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Error in loading airports", ex);
                                Airports = new List<Models.Airport>();
                            }
                        },
                        (aimsResult, log) =>
                        {
                            LogsRepository.AddError("Error in loading airports", aimsResult.ReturnParam);
                            Airports = new List<Models.Airport>();
                        });
                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Error in GetAllAirports", ex);
                    Airports = new List<Models.Airport>();
                }
            }
        }

        public new void CheckIfSynchronizationNeeded()
        {
            try
            {
                _proxy.GetJson<ResultPack<DateTime>>("airportslastupdate", DefaultMobileApplication.Current.Position.ToContractPosition(),
                    (aimsSvrResponse, log) =>
                    {
                        try
                        {
                            var res = (ResultPack<DateTime>)aimsSvrResponse;
                            if (!res.IsSucceeded)
                            {
                                NotifyChangeNeeded(true);
                                return;
                            }
                            var lastServerUpdate = res.ReturnParam;
                            var syncEvent = SyncEventDBAdapter.Current.GetLastSyncEvent((int)SyncScope.Airports);
                            if (syncEvent == null || syncEvent.SyncServerDate < lastServerUpdate)
                            {
                                SyncEventDBAdapter.Current.SaveSyncEvent(new SyncEvent
                                {
                                    Scope = (int)SyncScope.Airports,
                                    Description = "Local database is now up to date",//TODO: description can be changed
                                    SyncServerDate = lastServerUpdate
                                });
                                NotifyChangeNeeded(true);
                            }
                            else
                                NotifyChangeNeeded(false);
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Error in GetAirportsLastUpdate", ex);
                            NotifyChangeNeeded(true);//When server is not responding (Siavash: I want the client UI not to work)
                        }
                    },
                    (aimsResult, log) =>
                    {
                        LogsRepository.AddError("Error in GetAirportsLastUpdate", aimsResult.ReturnParam);
                        NotifyChangeNeeded(true);
                    });
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in GetAirportsLastUpdate", ex);
                NotifyChangeNeeded(true);
            }
        }

        /// <summary>
        /// A really slow method which asynchronousely syncs or loads all the airports to the local DB
        /// </summary>
        /// <param name="cash">Makes the method slow. it stores all the airlines into the local DB</param>
        /// <param name="forceGet">Set it true if you want to load regardless of the local DB</param>
        public void LoadAirports(bool cash = false, bool forceGet = false)//TODO: TOTALLY WRONG (Fix it)
        {
            try
            {
                if (forceGet)
                {
                    ReloadAirportsFromServer(cash);
                }
                else
                {
                    var aps = AirportDBAdapter.Current.GetAirports();
                    if (aps == null || aps.Count() == 0)
                        Airports = new List<Models.Airport>();
                    else
                        Airports = aps.Select(x => new AirportAdorner(x)).ToList();

                    if (cash)
                        CheckIfSynchronizationNeeded();
                }
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in loading Airports", ex);
                Airports = new List<Models.Airport>();
            }
        }

        public void SearchAirports(string keyword, bool online = false)
        {
            if (string.IsNullOrWhiteSpace(keyword)) return;
            if (keyword.Length < 2) return;
            keyword = keyword.ToUpper().Trim();

            lock (loaderLocker)
            {
                if (online)
                {
                    try
                    {
                        _proxy.GetJson<ResultPack<List<Models.Airport>>>(string.Format("airports/{0}/bykeyword", keyword), DefaultMobileApplication.Current.Position.ToContractPosition(),
                            (aimsSvrResponse, log) =>
                            {
                                var res = (ResultPack<List<Models.Airport>>)aimsSvrResponse;
                                if (res == null)
                                {
                                    LogsRepository.AddError("Error in loading airports", "Null response is returned");
                                    return;
                                }
                                if (res.IsSucceeded)
                                {
                                    var items = new List<Models.Airport>();
                                    if (res.ReturnParam != null && res.ReturnParam.Count > 0 && !string.IsNullOrEmpty(keyword))
                                    {
                                        keyword = keyword.ToLower();
                                        var nameKeyword = string.Format(" {0}", keyword);
                                        res.ReturnParam = keyword.Length <= 3 ?
                                            res.ReturnParam.Where(x => x.IATA_Code.ToLower().Contains(keyword)).ToList() :
                                            res.ReturnParam.Where(x => x.IATA_Code.ToLower().Contains(keyword) ||
                                                                x.Name.ToLower().Contains(nameKeyword) ||
                                                                x.Name.ToLower().StartsWith(keyword)).ToList();


                                        items.AddRange(res.ReturnParam.Where(x => x.IATA_Code.ToLower().StartsWith(keyword)).ToList().OrderBy(x => x.IATA_Code));

                                        items.AddRange(res.ReturnParam.Where(x => !x.IATA_Code.ToLower().StartsWith(keyword) &&
                                                                            x.IATA_Code.ToLower().Contains(keyword)).ToList().OrderBy(x => x.IATA_Code));

                                        items.AddRange(res.ReturnParam.Where(x => !x.IATA_Code.ToLower().StartsWith(keyword) &&
                                                                            !x.IATA_Code.ToLower().Contains(keyword) &&
                                                                            x.Name.ToLower().StartsWith(keyword)).ToList().OrderBy(x => x.Name));

                                        items.AddRange(res.ReturnParam.Where(x => !x.IATA_Code.ToLower().StartsWith(keyword) &&
                                                                            !x.IATA_Code.ToLower().Contains(keyword) &&
                                                                            !x.Name.ToLower().StartsWith(keyword)).ToList().OrderBy(x => x.Name));

                                    }

                                    NotifyRepositoryChanged(items);
                                }
                                else LogsRepository.AddError("Error in loading airports", res.Message);
                            },
                            (aimsResult, log) =>
                            {
                                LogsRepository.AddError("Error in loading airports", aimsResult.ReturnParam);
                            });
                    }
                    catch (Exception ex)
                    {
                        LogsRepository.AddError("Error in SearchAirports", ex);
                    }
                }
                else if (Airports != null && Airports.Count() > 0)
                {
                    var result = Airports.Where(x => x.IATA_Code.ToUpper().Contains(keyword) || x.Name.ToUpper().Contains(keyword)).ToList();
                    NotifyRepositoryChanged(result);
                }
                else NotifyRepositoryChanged(new List<Models.Airport>());
            }
        }

        public void GetAirport(string IATA_Code, bool online = false)
        {
            if (string.IsNullOrWhiteSpace(IATA_Code)) return;

            IATA_Code = IATA_Code.ToUpper().Trim();

            lock (loaderLocker)
            {
                if (online)
                {
                    try
                    {
                        _proxy.GetJson<ResultPack<List<Models.Airport>>>(string.Format("airports/{0}/bykeyword", IATA_Code), DefaultMobileApplication.Current.Position.ToContractPosition(),
                            (aimsSvrResponse, log) =>
                            {
                                try
                                {
                                    var res = (ResultPack<List<Models.Airport>>)aimsSvrResponse;
                                    if (res == null)
                                    {
                                        LogsRepository.AddError("Error in loading airports", "Null response is returned");
                                        return;
                                    }
                                    if (res.IsSucceeded)
                                    {
                                        if (res.ReturnParam.Count > 0)
                                            NotifyRepositoryChanged(res.ReturnParam.FirstOrDefault(x => x.IATA_Code.ToUpper().Equals(IATA_Code)));
                                        else
                                            NotifyRepositoryChanged(null);
                                    }
                                    else LogsRepository.AddError("Error in loading airports", res.Message);
                                }
                                catch (Exception ex)
                                {
                                    LogsRepository.AddError("Error in loading airports", ex);
                                }
                            },
                            (aimsResult, log) =>
                            {
                                LogsRepository.AddError("Error in loading airports", aimsResult.ReturnParam);
                            });
                    }
                    catch (Exception ex)
                    {
                        LogsRepository.AddError("Error in GetAirport", ex);
                    }
                }
                else if (Airports != null && Airports.Count() > 0)
                {
                    NotifyRepositoryChanged(Airports.FirstOrDefault(x => x.IATA_Code.ToUpper().Equals(IATA_Code)));
                }
                else NotifyRepositoryChanged(null);
            }
        }

        public async Task<List<Models.Airport>> SearchAirportsAsync(string keyword)
        {
            if (string.IsNullOrWhiteSpace(keyword)) return new List<Models.Airport>();
            if (keyword.Length < 2) return new List<Models.Airport>();
            keyword = keyword.ToUpper().Trim();

            try
            {
                var result = await _proxy.GetJsonAsync<ResultPack<List<Models.Airport>>>(string.Format("airports/{0}/bykeyword", keyword), DefaultMobileApplication.Current.Position.ToContractPosition()).ConfigureAwait(false);

                if (result.IsSucceeded)
                    return result.ReturnParam;
                else LogsRepository.AddError("Error in SearchAirportsAsync", result.Message);
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in SearchAirportsAsync", ex);
            }
            return new List<Models.Airport>();
        }

        public void GetAllAirports()
        {
            lock (loaderLocker)
            {
                try
                {
                    _proxy.GetJson<ResultPack<List<Models.Airport>>>("allairports", DefaultMobileApplication.Current.Position.ToContractPosition(),
                        (aimsSvrResponse,log) =>
                        {
                            try
                            {
                                var res = (ResultPack<List<Models.Airport>>)aimsSvrResponse;

                                NotifyRepositoryChanged(res.ReturnParam);
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Error in loading airports", ex);
                            }
                        },
                        (aimsResult, log) =>
                        {
                            LogsRepository.AddError("Error in loading airports", aimsResult.ReturnParam);
                        });
                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Error in SearchAirports", ex);
                }
            }
        }

        //public async Task<List<Models.Airport>> SearchAirportsAsync(string keyword, bool online = false)
        //{
        //    if (string.IsNullOrWhiteSpace(keyword)) return new List<Models.Airport>();
        //    if (keyword.Length < 2) return new List<Models.Airport>();
        //    keyword = keyword.ToUpper().Trim();

        //    if (online)
        //    {
        //        try
        //        {
        //            var resStr = await _proxy.GetJson<List<Models.Airport>>(string.Format("airports/{0}/bykeyword", keyword), (Contracts.Position)DefaultMobileApplication.Current.Position);

        //            ResultPack<List<Models.Airport>> res = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultPack<List<Models.Airport>>>(resStr);

        //            if (res.IsSucceeded)
        //                return res.ReturnParam;
        //            else LogsRepository.AddError("Error in SearchAirports", res.Message);
        //        }
        //        catch (Exception ex)
        //        {
        //            LogsRepository.AddError("Error in SearchAirports", ex);
        //        }
        //    }
        //    else if (Airports != null && Airports.Count() > 0)
        //    {
        //        var result = Airports.Where(x => x.IATA_Code.ToUpper().Contains(keyword) || x.Name.ToUpper().Contains(keyword)).ToList();
        //        return result;
        //    }

        //    return new List<Models.Airport>();
        //}
    }
}