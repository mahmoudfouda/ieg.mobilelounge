﻿using AIMS.Contracts;
using AIMS.DAL;
using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AIMS
{
    internal class LogAdorner : Models.Log
    {
        public LogAdorner(DAL.Log log)
        {
            if (log == null)
                throw new ArgumentNullException("log");
            else this._log = log;
        }
    }

    public class LogsRepository : RepositoryBase
    {
        public static event ReporitoryChangedHandler OnSavedLog;

        public static event TrackEventsHandler OnTrackEvents;

        public LogsRepository() : base()//(new WebApiServiceContext())
        {
        }

        public void LoadClientOnlyLogs()
        {
            var logs = Logger.Current.GetLogsByLevel((int)LogLevel.Client).Select(x => new LogAdorner(x)).ToList();
            NotifyRepositoryChanged(logs);
        }

        public void LoadDeveloperOnlyLogs()
        {
            //var logs = Logger.Current.GetLogsByLevel((int)LogLevel.Developer).Select(x => new LogAdorner(x)).ToList();
            var logs = Logger.Current.GetLogs().Select(x => new LogAdorner(x)).ToList();
            NotifyRepositoryChanged(logs);
        }

        public void LoadAllLogs()
        {
            var logs = Logger.Current.GetLogs().ToList();
            NotifyRepositoryChanged(logs);
        }

        public void DeleteAllLogs()
        {
            Logger.Current.DeleteAllLogs();
        }

        public void UploadLogs(EventHandler callback = null)
        {
            try
            {
                //TODO: Make pushlogs a developer secured method in serverside
                var logsInput = Logger.Current.GetLogs().Select(x => x.ToString()).ToList();

                _proxy.SendJson<object>("pushlogs", logsInput, DefaultMobileApplication.Current.Position.ToContractPosition(), //(Contracts.Position)DefaultMobileApplication.Current.Position,
                    (aimsSvrResponse, log) =>
                    {
                        LogsRepository.TrackEvents(log);

                        if (callback != null)
                            callback.Invoke(this, null);
                    },
                    (aimsResult, log) =>
                    {
                        AddError("Error in pushlogs", aimsResult.ReturnParam);
                    });
            }
            catch (CommunicationException ex)
            {
                AddError("Error in UploadLogs", ex);
            }
            catch (Exception ex)
            {
                AddError("Error in UploadLogs", ex);
            }
        }

        private static void AddLog(string title, LogType type, string description = "")
        {
            AddLog(title, type, LogLevel.Developer, description);
        }

        private static void AddLog(string title, LogType type, LogLevel level, string description = "")
        {
            try
            {
                var position = DefaultMobileApplication.Current.Position == null ? new Ieg.Mobile.DataContracts.Models.Position() : DefaultMobileApplication.Current.Position;
                var l = new DAL.Log()
                {
                    Title = title,
                    LogType = (int)type,
                    LogLevel = (int)level,
                    Username = (MembershipProvider.Current != null && MembershipProvider.Current.LoggedinUser != null) ? MembershipProvider.Current.LoggedinUser.Username : "Unknown",
                    MobileKey = (MembershipProvider.Current != null && MembershipProvider.Current.LoggedinUser != null) ? MembershipProvider.Current.LoggedinUser.MobileKey : "",
                    DeviceId = DefaultMobileApplication.Current.CurrentDevice.DeviceUniqueId,
                    Latitude = position.Latitude,
                    Longitude = position.Longitude,
                    Altitude = position.Altitude.HasValue ? position.Altitude.Value : 0,
                    Accuracy = position.Accuracy.HasValue ? position.Accuracy.Value : 0
                };
                if (!string.IsNullOrWhiteSpace(description))
                    l.Description = description;

                Logger.Current.SaveLog(l);

                try
                {
                    if (OnSavedLog != null)
                    {
                        OnSavedLog.Invoke(new AIMS.Models.Log()
                        {
                            Title = l.Title,
                            Description = l.Description,
                            LogType = (LogType)l.LogType
                        });
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine(string.Format("Exception in AddLog():\n{0}\n", ex.StackTrace));//life saving one!!!
#endif
            }
        }

        public static void AddError(string title, string description = "")
        {
            AddError(title, new Exception(description));
        }

        public static void AddError(string title, Exception ex)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Error occurred: " + ex.Message);
            if (!string.IsNullOrWhiteSpace(ex.StackTrace))
            {
                sb.AppendLine("Stacktrace: ");
                sb.AppendLine(ex.StackTrace);
            }

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("AIMS Exception: {0}\n{1}\n-------------------------\n", title, sb.ToString()));//life saving one!!!
#endif
            AddLog(title, LogType.Error, sb.ToString());
        }

        public static void AddWarning(string title, string description = "")
        {
            AddLog(title, LogType.Warning, description);
        }

        public static void AddInfo(string title, string description = "")
        {
            AddLog(title, LogType.Info, description);
        }

        public static void AddClientError(string title, string description = "")
        {
            AddLog(title, LogType.Error, LogLevel.Client, description);
        }

        public static void AddClientWarning(string title, string description = "")
        {
            AddLog(title, LogType.Warning, LogLevel.Client, description);
        }

        public static void AddClientInfo(string title, string description = "")
        {
            AddLog(title, LogType.Info, LogLevel.Client, description);
        }

        internal static void TrackEvents(Dictionary<string, Dictionary<string, string>> log)
        {
            if (OnTrackEvents != null)
                OnTrackEvents(log);
        }
    }

    public delegate void TrackEventsHandler(Dictionary<string, Dictionary<string, string>> log);
}