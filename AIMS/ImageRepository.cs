﻿using AIMS.Contracts;
using AIMS.DAL;
using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AIMS
{
    public class ImageRepository : RepositoryBase
    {
        #region Fields
        //private string _imageHandle;
        //private byte[] _imageBytes;
        const int imageRequestInterval = 100;
        const int maxParallelRequests = 3;

        private static object requestLocker = new object();
        private static object sentRequestLocker = new object();
        private static object isWorkingLocker = new object();

        private static Queue<KeyValuePair<string, ReporitoryChangedHandler<OperationResult<byte[]>>>> imageRequests = new Queue<KeyValuePair<string, ReporitoryChangedHandler<OperationResult<byte[]>>>>();
        private static List<string> sentRequests = new List<string>();

        private static bool isWorking = false;

        private static Task requestChecker;
        #endregion

        #region Properties (Thread safe)
        public static Queue<KeyValuePair<string, ReporitoryChangedHandler<OperationResult<byte[]>>>> PendingRequests
        {
            get
            {
                lock (requestLocker)
                {
                    return imageRequests;
                }
            }
            private set
            {
                lock (requestLocker)
                {
                    imageRequests = value;
                }
            }
        }

        public static List<string> SentRequests
        {
            get
            {
                lock (sentRequestLocker)
                {
                    return sentRequests;
                }
            }
            private set
            {
                lock (sentRequestLocker)
                {
                    sentRequests = value;
                }
            }
        }

        public bool IsWorking
        {
            get
            {
                lock (isWorkingLocker)
                {
                    return isWorking;
                }
            }
            set
            {
                lock (isWorkingLocker)
                {
                    isWorking = value;
                }
            }
        }

        #endregion

        public ImageRepository() : base()//(new WebApiServiceContext())
        {
        }

        public void LoadImage(string imageHandle, ReporitoryChangedHandler<OperationResult<byte[]>> callback, bool forceGet = false)
        {
            if (callback == null) return;
            try
            {
                if (!forceGet)
                {
                    var image = ImageDBAdapter.Current.GetImage(imageHandle);
                    if (image != null && image.PictureBytes != null && image.PictureBytes.Length != 0)
                    {
                        callback.Invoke(new OperationResult<byte[]>
                        {
                            IsSucceeded = true,
                            ReturnParam = image.PictureBytes,
                            Message = "Retrieved from local DB"
                        });
                        NotifyRepositoryChanged(image.PictureBytes);
                        return;
                    }
                }

                //For dictionary
                //if (!Requests.ContainsKey(imageHandle))
                //    Requests.Add(imageHandle, callback);

                //For Queue
                PendingRequests.Enqueue(new KeyValuePair<string, AIMS.ReporitoryChangedHandler<OperationResult<byte[]>>>(imageHandle, callback));

                checkThread();
                #region TODO: for Queued Image Requests (bellow lines are commented)
                ////_proxy.GetJson<ResultPack<byte[]>>(string.Format("https://aims2.ieg-america.com/aims_2_services/mobile/MobileWebAPI_AC/image/{0}/", imageHandle), DefaultMobileApplication.Current.Position,
                //_proxy.GetJson<ResultPack<byte[]>>(string.Format("image/{0}/", imageHandle), (Contracts.Position)DefaultMobileApplication.Current.Position,
                //    (aimsSvrResponse) =>
                //    {
                //        try
                //        {
                //            var res = (ResultPack<byte[]>)aimsSvrResponse;
                //            if (callback != null) callback.Invoke(res.ReturnParam);
                //            NotifyRepositoryChanged(res.ReturnParam);
                //        }
                //        catch (Exception ex)
                //        {
                //            LogsRepository.AddError("Error in EndGetImage", ex);
                //        }
                //    },
                //    (aimsResult) =>
                //    {
                //        LogsRepository.AddError("Error in EndGetImage", aimsResult.ReturnParam);
                //    });
                #endregion
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in ImageRepository.LoadImage", ex);
                callback.Invoke(new OperationResult<byte[]>
                {
                    IsSucceeded = false,
                    Message = "Could not add the request to image loader queue."
                });
            }
        }

        private void checkThread()
        {
            if (!IsWorking || requestChecker.Status != TaskStatus.Running)
            {
                IsWorking = true;

                requestChecker = new Task(new Action(() =>
                {
                    while (true)
                    {
#if DEBUG
                        System.Diagnostics.Debug.WriteLine(string.Format("Image Requests queue size: {0}\tPending requests: {1}", PendingRequests.Count, SentRequests.Count));
#endif
                        Task.Delay(imageRequestInterval).Wait();

                        while (SentRequests.Count >= maxParallelRequests)
                        {
                            continue;
                        }

                        if (PendingRequests.Count > 0)
                        {
                            var req = PendingRequests.Dequeue();//TODO: Possible Bug: the req object might change sooner than its corresponding image gets loaded
                            SentRequests.Add(req.Key);
                            try
                            {
                                _proxy.GetJson<ResultPack<byte[]>>(string.Format("image/{0}/", req.Key), DefaultMobileApplication.Current.Position.ToContractPosition(),
                                    (aimsSvrResponse, log) =>
                                    {
                                        SentRequests.Remove(req.Key);
                                        try
                                        {
                                            var res = (ResultPack<byte[]>)aimsSvrResponse;
                                            var image = new Image
                                            {
                                                ImageHandle = req.Key,
                                                PictureBytes = res.ReturnParam
                                            };
                                            ImageDBAdapter.Current.SaveImage(image);
                                            if (req.Value != null)
                                                req.Value.Invoke(new OperationResult<byte[]>
                                                {
                                                    IsSucceeded = true,
                                                    ReturnParam = image.PictureBytes,
                                                    Message = "Retrieved from server"
                                                });
                                            NotifyRepositoryChanged(image.PictureBytes);
                                        }
                                        catch (Exception ex)
                                        {
                                            LogsRepository.AddError("Error in EndGetImage", ex.StackTrace);
                                            if (req.Value != null)
                                                req.Value.Invoke(new OperationResult<byte[]>
                                                {
                                                    IsSucceeded = false,
                                                    Message = "Error parsing the getImage() result for : " + req.Key + "\nError:" + ex.Message
                                                });
                                        }
                                    },
                                    (aimsResult, log) =>
                                    {
                                        SentRequests.Remove(req.Key);
                                        LogsRepository.AddError("Error in EndGetImage", aimsResult.ReturnParam);
                                        if (req.Value != null)
                                            req.Value.Invoke(new OperationResult<byte[]>
                                            {
                                                IsSucceeded = false,
                                                Message = "Failed callback from getImage() for : " + req.Key + "\nError:" + aimsResult.ReturnParam
                                            });
                                    });
                            }
                            catch (Exception exx)
                            {
                                SentRequests.Remove(req.Key);
                                LogsRepository.AddError("Error in BeginGetImage", exx);
                                if (req.Value != null)
                                    req.Value.Invoke(new OperationResult<byte[]>
                                    {
                                        IsSucceeded = false,
                                        Message = "Can not call getImage() for : " + req.Key + "\nError: " + exx.Message
                                    });
                            }
                        }
                        else
                        {
                            IsWorking = false;

                            return;
                        }
                    }
                }));
                requestChecker.Start();
            }
        }
    }
}