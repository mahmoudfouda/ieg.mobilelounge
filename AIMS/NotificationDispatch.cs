﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS
{
    public delegate void LogHandler(Models.Log receivedLog);
    public static class NotificationDispatch
    {
        private static object locker = new object();
        
        public static event LogHandler OnLogReceived;

        public static void Log(Exception ex)
        {
            //TODO: convert Exception to Log
            Log(new Models.Log());
        }

        public static void Log(string ex)
        {
            //TODO: convert string to Log
            Log(new Models.Log());
        }

        public static void Log(Models.Log log)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine(log.ToString());
#endif
            lock (locker)
            {
                if (OnLogReceived == null) return;
            }

            OnLogReceived.Invoke(log);
        }
    }
}
