﻿using System;
using System.Collections.Generic;
using AIMS.DAL;
using AIMS.Contracts;
using AIMS.Contracts.AIMSClient;
using System.ServiceModel;
using System.Linq;
using AIMS.Models;
using Ieg.Mobile.Localization;
using AIMS.Contracts.DataContracts;
using System.Text;

namespace AIMS
{
    public delegate void OnPassengerCheckedHandler (bool isSucceeded, int? errorCode, string errorMetadata, string message, Models.Passenger passenger);// ResultPack<Models.Passenger> passengerCheckResult);

    internal class PassengerAdorner : Models.Passenger, Models.IAdorner
    {
        public PassengerAdorner (DAL.Passenger passenger)
        {
            this.passenger = passenger;
        }
    }

    public class PassengersRepository : RepositoryBase
    {
        private static Dictionary<string, DateTime> SucceededBarcodes { get; set; }
        private static Dictionary<string, DateTime> FailedBarcodes { get; set; }

        private static int _succeededBarcodeInterval;
        public static int SucceededBarcodeInterval {
            get {
                if (_succeededBarcodeInterval == 0) {
                    try {
                        var conf = UserConfigurationDBAdapter.Current.GetConfiguration (UserConfigurationDBAdapter.SBIConfigName);
                        if (conf != null)//previousely selected threshold
                        {
                            int.TryParse (conf.Value, out _succeededBarcodeInterval);
                        }
                    } catch (Exception ex) {
                        LogsRepository.AddError ("Exception in getting PasengersRepository.SucceededBarcodeInterval", ex);
                    }
                }
                if (_succeededBarcodeInterval == 0) _succeededBarcodeInterval = 10;//default interval settings
                return _succeededBarcodeInterval;
            }
            set {
                if (_succeededBarcodeInterval != value) {
                    try {
                        var id = UserConfigurationDBAdapter.Current.SaveConfiguration (new MobileUserConfiguration {
                            Name = UserConfigurationDBAdapter.SBIConfigName,
                            Username = "",
                            Value = value.ToString ()
                        });
                        if (id > 0) {
                            _succeededBarcodeInterval = value;
                        } else LogsRepository.AddError ("Error in SaveConfiguration()", string.Format ("The returned id for changing {0} is {1}", UserConfigurationDBAdapter.SBIConfigName, id));
                    } catch (Exception ex) {
                        LogsRepository.AddError ("Exception in setting PasengersRepository.SucceededBarcodeInterval", ex);
                    }
                }
            }
        }

        private static int _failedBarcodeInterval;
        public static int FailedBarcodeInterval {
            get {
                if (_failedBarcodeInterval == 0) {
                    try {
                        var conf = UserConfigurationDBAdapter.Current.GetConfiguration (UserConfigurationDBAdapter.FBIConfigName);
                        if (conf != null)//previousely selected threshold
                        {
                            int.TryParse (conf.Value, out _failedBarcodeInterval);
                        }
                    } catch (Exception ex) {
                        LogsRepository.AddError ("Exception in getting PasengersRepository.FailedBarcodeInterval", ex);
                    }
                }
                if (_failedBarcodeInterval == 0) _failedBarcodeInterval = 5;//default interval settings
                return _failedBarcodeInterval;
            }
            set {
                if (_failedBarcodeInterval != value) {
                    try {
                        var id = UserConfigurationDBAdapter.Current.SaveConfiguration (new MobileUserConfiguration {
                            Name = UserConfigurationDBAdapter.FBIConfigName,
                            Username = "",
                            Value = value.ToString ()
                        });
                        if (id > 0) {
                            _failedBarcodeInterval = value;
                        } else LogsRepository.AddError ("Error in SaveConfiguration()", string.Format ("The returned id for changing {0} is {1}", UserConfigurationDBAdapter.FBIConfigName, id));
                    } catch (Exception ex) {
                        LogsRepository.AddError ("Exception in setting PasengersRepository.FailedBarcodeInterval", ex);
                    }
                }
            }
        }

        public void SearchPassengers (string keyWord, bool airportSearch, DateTime dateTime, int pageIndex = 1, bool loadMore = false)
        {
            try {

                string url = string.Format ("passengers?query.AirportSearch={0}&query.Name={1}&query.TimeStamp={2}&query.PageIndex={3}", airportSearch, keyWord, dateTime.ToString("MM-dd-yyyy HH:mm:ss"), pageIndex);

                if (!airportSearch)
                    url += "&query.LoungeIdList=" + MembershipProvider.Current.SelectedLounge.LoungeID.ToString();
                else
                    url += "&query.workstationIdList=" + MembershipProvider.Current.SelectedLounge.WorkstationID.ToString();


                _proxy.GetJson<ResultPack<List<LightTrackingInfo>>> (url, DefaultMobileApplication.Current.Position.ToContractPosition (),
                    (aimsSvrResponse, log) => {
                        var res = (ResultPack<List<LightTrackingInfo>>)aimsSvrResponse;
                        try {
                            if (res.ReturnParam == null)
                                Passengers = new List<Models.Passenger> ();
                            else {
                                if (loadMore) {
                                    var result = ProcessTrackingList (res.ReturnParam);
                                    result.InsertRange (0, Passengers.ToList ());
                                    Passengers = result;
                                } else {
#if DEBUG
                                    /*  if (res.ReturnParam.Count() > 0)
                                      {
                                          for (int i = 0; i < 20; i++)
                                          {
                                              res.ReturnParam.Add(res.ReturnParam.ToList()[0]);
                                          }
                                      } */
#endif


                                    Passengers = ProcessTrackingList (res.ReturnParam);
                                }
                            }

                        } catch (Exception ex) {
                            LogsRepository.AddError ("Error in loading passengers", ex);
                            Passengers = new List<Models.Passenger> ();
                        }
                        NotifyRepositoryChanged (Passengers);
                    },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in loading passengers", aimsResult.ReturnParam);
                        Passengers = new List<Models.Passenger> ();
                        NotifyRepositoryChanged (Passengers);
                    });
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in BeginSearchTracking", ex);
                Passengers = new List<Models.Passenger> ();
                NotifyRepositoryChanged (Passengers);
            }
        }

        public event OnPassengerCheckedHandler OnPassengerChecked;

        public IEnumerable<Models.Passenger> Passengers {
            get;
            set;
        }

        public PassengersRepository () : base ()//(new WebApiServiceContext())
        {
            //LoadPassengers();

            if (SucceededBarcodes == null)
                SucceededBarcodes = new Dictionary<string, DateTime> ();

            if (FailedBarcodes == null)
                FailedBarcodes = new Dictionary<string, DateTime> ();
        }

        public static List<Models.Passenger> GroupPassengers (IEnumerable<Models.Passenger> passengers)
        {
            //TODO: sort passengers and their guests
            if (passengers == null || passengers.Count () == 0) return new List<Models.Passenger> ();

            var res = new List<Models.Passenger> ();
            var hosts = passengers.Where (x => x.GuestOf == 0).ToList ();
            var allGuests = passengers.Where (x => x.GuestOf != 0).ToList ();
            foreach (var host in hosts) {
                //getting the corresponding guests
                var guests = allGuests.Where (x => x.GuestOf == host.TrackingRecordID).ToList ();

                res.Add (host);
                res.AddRange (guests);

                //removing the added guests
                foreach (var guest in guests) {
                    allGuests.Remove (guest);
                }
            }
            //adding the remaining guests without host
            res.AddRange (allGuests);

            //The ordered list
            return res;
        }

        public void LoadLastHourPassengers ()
        {
            try {
                _proxy.GetJson<ResultPack<List<LightTrackingInfo>>> (string.Format ("lounge/{0}/lasthourpassengers", (int)MembershipProvider.Current.SelectedLounge.LoungeID), DefaultMobileApplication.Current.Position.ToContractPosition (),
                    (aimsSvrResponse, log) => {
                        var res = (ResultPack<List<LightTrackingInfo>>)aimsSvrResponse;
                        try {
                            if (res.ReturnParam == null) Passengers = new List<Models.Passenger> ();
                            else Passengers = ProcessTrackingList (res.ReturnParam);
                        } catch (Exception ex) {
                            LogsRepository.AddError ("Error in loading passengers", ex);
                            Passengers = new List<Models.Passenger> ();
                        }
                        NotifyRepositoryChanged (Passengers);
                    },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in loading passengers", aimsResult.ReturnParam);
                        Passengers = new List<Models.Passenger> ();
                        NotifyRepositoryChanged (Passengers);
                    });
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in BeginSearchLastHour", ex);
                Passengers = new List<Models.Passenger> ();
                NotifyRepositoryChanged (Passengers);
            }
        }

        public void LoadFailedPassengers ()
        {
            try {
                Passengers = PassengerDBAdapter.Current.GetAllPassengers ().Select (x => new PassengerAdorner (x)).ToList ();
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in retreiving DB passengers", ex);
                Passengers = new List<Models.Passenger> ();
            }
            NotifyRepositoryChanged (Passengers);
        }

        public static OperationResult<Models.Passenger> AddFailedPassenger (Models.Passenger passenger, string reason)
        {
            try {
                var passengerClone = ((Models.IAdorner)passenger).DALObject as DAL.Passenger;
                passengerClone.FailedReason = reason;
                passengerClone.TrackingTimestamp = DateTime.Now;
                //var p = (PassengerAdorner)passengerClone;
                PassengerDBAdapter.Current.SavePassenger (passengerClone);
                return new OperationResult<Models.Passenger> {
                    IsSucceeded = true,
                    ReturnParam = new PassengerAdorner (passengerClone)
                };
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in adding failed passenger", ex);
                return new OperationResult<Models.Passenger> {
                    Message = ex.Message,
                    IsSucceeded = false
                };
            }
        }

        public void SearchPassengers (string keyword)
        {
            try {
                _proxy.GetJson<ResultPack<List<LightTrackingInfo>>> (string.Format ("lounge/{0}/workstation/{1}/lasthourpassengersby/{2}", (int)MembershipProvider.Current.SelectedLounge.LoungeID, (int)MembershipProvider.Current.SelectedLounge.WorkstationID, keyword), DefaultMobileApplication.Current.Position.ToContractPosition (),
                    (aimsSvrResponse, log) => {
                        var res = (ResultPack<List<LightTrackingInfo>>)aimsSvrResponse;
                        try {
                            if (res.ReturnParam == null) Passengers = new List<Models.Passenger> ();
                            else Passengers = ProcessTrackingList (res.ReturnParam);
                        } catch (Exception ex) {
                            LogsRepository.AddError ("Error in loading passengers", ex);
                            Passengers = new List<Models.Passenger> ();
                        }
                        NotifyRepositoryChanged (Passengers);
                    },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in loading passengers", aimsResult.ReturnParam);
                        Passengers = new List<Models.Passenger> ();
                        NotifyRepositoryChanged (Passengers);
                    });
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in BeginSearchTracking", ex);
                Passengers = new List<Models.Passenger> ();
                NotifyRepositoryChanged (Passengers);
            }
        }

        public void AgentRejectPassenger (Models.Workstation workstation, Models.Passenger passenger = null)
        {
            //If we give it the passenger, it will use it. If not, it will use the last passenger (selected passenger)
            //from our current workflow
            try {
                var exTrackingInput = new ExtendedMobileTrackingInput ();
                exTrackingInput.TrackingInput = new MobileTrackingInput {
                    ScannerInput = null,
                    TrackingPayload = passenger == null ? MembershipProvider.Current.SelectedPassenger.ConvertToTracking () : passenger.ConvertToTracking (),
                    Workstation = workstation.ConvertToWORKSTATION (),
                    AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username
                };
                exTrackingInput.TrackingInput.TrackingPayload.TRACKING_PASSENGER_NAME = "Passenger was denied entry into the lounge";
                if (exTrackingInput.TrackingInput.TrackingPayload.TRACKING_TIMESTAMP == default (DateTime)) {
                    exTrackingInput.TrackingInput.TrackingPayload.TRACKING_TIMESTAMP = DateTime.Now;
                    exTrackingInput.LocalTime = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                    exTrackingInput.TrackingInput.TrackingPayload.TIMESTAMP_UTC = DateTime.UtcNow;
                    exTrackingInput.UTCTime = DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss");
                }

                if (!DefaultMobileApplication.Current.SecondaryAuthenticationCheck ()) return;
                if (MembershipProvider.Current.SecondValidator.ExternalToken != null)
                    exTrackingInput.TrackingInput.ExternalOauthToken = MembershipProvider.Current.SecondValidator.ExternalToken.AccessToken;

                _proxy.SendJson<ResultPack<TRACKING>> ("denyentry", exTrackingInput, DefaultMobileApplication.Current.Position.ToContractPosition (),
                    (aimsSvrResponse, log) => {
                        //nothing to show
                        LogsRepository.TrackEvents (log);
                    },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in RejectPassenger", aimsResult.ReturnParam);
                    });

            } catch (CommunicationException ex) {
                LogsRepository.AddError ("Error in RejectPassenger", ex);
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in RejectPassenger", ex);
            }
        }

        public void ValidatePassengerWithCard (Models.Workstation workstation, string cardBarcode)
        {
            try {
                var exTrackingInput = new ExtendedMobileTrackingInput ();
                exTrackingInput.TrackingInput = new MobileTrackingInput {
                    ScannerInput = cardBarcode,
                    TrackingPayload = MembershipProvider.Current.SelectedPassenger.ConvertToTracking (),
                    Workstation = workstation.ConvertToWORKSTATION (),
                    AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username
                };
                if (string.IsNullOrEmpty (cardBarcode)) {
                    LogsRepository.AddError ("Error in ValidatePassengerWithCard", "Missing second barcode read");
                    NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                        ReturnParam = null,
                        GeneratedDate = DateTime.Now,
                        IsSucceeded = false,
                        ErrorCode = null,
                        Message = "Card barcode is empty"
                    });
                }
                if (!DefaultMobileApplication.Current.SecondaryAuthenticationCheck ()) return;
                if (MembershipProvider.Current.SecondValidator.ExternalToken != null)
                    exTrackingInput.TrackingInput.ExternalOauthToken = MembershipProvider.Current.SecondValidator.ExternalToken.AccessToken;
                if (exTrackingInput.TrackingInput.TrackingPayload.TRACKING_TIMESTAMP == default (DateTime)) {
                    exTrackingInput.TrackingInput.TrackingPayload.TRACKING_TIMESTAMP = DateTime.Now;
                    exTrackingInput.LocalTime = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                    exTrackingInput.TrackingInput.TrackingPayload.TIMESTAMP_UTC = DateTime.UtcNow;
                    exTrackingInput.UTCTime = DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss");
                }
                #region DEBUG Simulation
#if DEBUG//TODO: Remove the whole (DEBUG) section below
                //var successTest = false;
                //if (successTest)
                //{
                //    NotifyPassengerChecked(new ResultPack<Models.Passenger>
                //    {
                //        ReturnParam =
                //        new Models.Passenger()
                //        {
                //            AirlineId = 3,
                //            CardID = 1303,
                //            BarcodeString = cardBarcode,
                //            FullName = "TEST",
                //            FFN = "1234",
                //            FromAirport = "LAX",
                //            ToAirport = "YUL",
                //            FlightCarrier = "UA",
                //            FlightNumber = "1234",
                //            SeatNumber = "13B",
                //            TrackingClassOfService = "J",
                //        },
                //        ErrorCode = (int)ErrorCode.NoError,
                //        ErrorMetadata = "",
                //        GeneratedDate = DateTime.UtcNow,
                //        UID = Guid.NewGuid(),
                //        IsSucceeded = true,
                //        Message = "",
                //    });
                //}
                //else
                //{
                //    NotifyPassengerChecked(new ResultPack<Models.Passenger>
                //    {
                //        ReturnParam =
                //        new Models.Passenger()
                //        {
                //            AirlineId = 3,
                //            CardID = 1303,
                //            BarcodeString = cardBarcode,
                //            FullName = "TEST",
                //            FFN = "1234",
                //            FromAirport = "LAX",
                //            ToAirport = "YUL",
                //            FlightCarrier = "UA",
                //            FlightNumber = "1234",
                //            SeatNumber = "13B",
                //            TrackingClassOfService = "J",
                //        },
                //        ErrorCode = (int)ErrorCode.PassengerNameMismatch,
                //        ErrorMetadata = "TEST;DOE/JOHN",
                //        GeneratedDate = DateTime.UtcNow,
                //        UID = Guid.NewGuid(),
                //        IsSucceeded = false,
                //        Message = "Name mismatch",
                //    });
                //}
                //return;//The rest will not work in DEBUG (I dont want to call server for this test)
#endif 
                #endregion
                _proxy.SendJson<ResultPack<TRACKING>> ("validatepassenger", exTrackingInput, DefaultMobileApplication.Current.Position.ToContractPosition (),
                    (aimsSvrResponse, log) => {
                        try {
                            LogsRepository.TrackEvents (log);

                            var result = (ResultPack<TRACKING>)aimsSvrResponse;
                            var passenger = ProcessTracking (result.ReturnParam);
                            NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                ReturnParam = passenger,
                                ErrorCode = result.ErrorCode,
                                ErrorMetadata = result.ErrorMetadata,
                                GeneratedDate = result.GeneratedDate,
                                UID = result.UID,
                                IsSucceeded = result.IsSucceeded,
                                Message =
                                    (result.ErrorCode.HasValue &&
                                    (result.ErrorCode.Value != (int)ErrorCode.ExternalValidationFailed) &&
                                    (result.ErrorCode.Value != (int)ErrorCode.PassengerNameMismatch))
                                    ? LanguageRepository.Current.GetText (result.ErrorCode.Value) :
                                        result.Message
                            });
                        } catch (Exception ex) {
                            LogsRepository.AddError ("Error in ValidatePassengerWithCard", ex);
                            NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                ReturnParam = null,
                                GeneratedDate = DateTime.Now,
                                IsSucceeded = false,
                                ErrorCode = (int)ErrorCode.UnknownException,
                                Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                            });
                        }
                    },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in ValidatePassengerWithCard", aimsResult.ReturnParam);
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.UnknownException,
                            Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                        });
                    });

            } catch (CommunicationException ex) {
                LogsRepository.AddError ("Error in ValidatePassengerWithCard", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.CommunicationError,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.CommunicationError)//"Error in communicating with server"
                });
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in ValidatePassengerWithCard", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
            }
        }

        public void ValidatePassengerInfo (Models.Workstation workstation)
        {
            try {
                var exTrackingInput = new ExtendedMobileTrackingInput ();
                exTrackingInput.TrackingInput = new MobileTrackingInput {
                    ScannerInput = null,
                    TrackingPayload = MembershipProvider.Current.SelectedPassenger.ConvertToTracking (),
                    Workstation = workstation.ConvertToWORKSTATION (),
                    AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username
                };

                if (!DefaultMobileApplication.Current.SecondaryAuthenticationCheck ()) return;
                if (MembershipProvider.Current.SecondValidator.ExternalToken != null)
                    exTrackingInput.TrackingInput.ExternalOauthToken = MembershipProvider.Current.SecondValidator.ExternalToken.AccessToken;
                if (exTrackingInput.TrackingInput.TrackingPayload.TRACKING_TIMESTAMP == default (DateTime)) {
                    exTrackingInput.TrackingInput.TrackingPayload.TRACKING_TIMESTAMP = DateTime.Now;
                    exTrackingInput.LocalTime = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                    exTrackingInput.TrackingInput.TrackingPayload.TIMESTAMP_UTC = DateTime.UtcNow;
                    exTrackingInput.UTCTime = DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss");
                }

                #region DEBUG Simulations
#if DEBUG//TODO: Remove the whole (DEBUG) section below
                //var successTest = false;
                //if (successTest)
                //{
                //    NotifyPassengerChecked(new ResultPack<Models.Passenger>
                //    {
                //        ReturnParam =
                //        new Models.Passenger()
                //        {
                //            AirlineId = 3,
                //            CardID = 1303,
                //            //BarcodeString = "2165651965169519684",
                //            FullName = "TEST",
                //            FFN = "1234",
                //            FromAirport = "LAX",
                //            ToAirport = "YUL",
                //            FlightCarrier = "UA",
                //            FlightNumber = "1234",
                //            SeatNumber = "13B",
                //            TrackingClassOfService = "J",
                //        },
                //        ErrorCode = (int)ErrorCode.NoError,
                //        ErrorMetadata = "",
                //        GeneratedDate = DateTime.UtcNow,
                //        UID = Guid.NewGuid(),
                //        IsSucceeded = true,
                //        Message = "",
                //    });
                //}
                //else
                //{
                //    var nameMismatch = false;
                //    if (nameMismatch)
                //    {
                //        NotifyPassengerChecked(new ResultPack<Models.Passenger>
                //        {
                //            ReturnParam =
                //                new Models.Passenger()
                //                {
                //                    AirlineId = 3,
                //                    CardID = 1303,
                //                    //BarcodeString = "66649651965165461646",
                //                    FullName = "TEST",
                //                    FFN = "1234",
                //                    FromAirport = "LAX",
                //                    ToAirport = "YUL",
                //                    FlightCarrier = "UA",
                //                    FlightNumber = "1234",
                //                    SeatNumber = "13B",
                //                    TrackingClassOfService = "J",
                //                },
                //            ErrorCode = (int)ErrorCode.PassengerNameMismatch,
                //            ErrorMetadata = "TEST;DOE/JOHN",
                //            GeneratedDate = DateTime.UtcNow,
                //            UID = Guid.NewGuid(),
                //            IsSucceeded = false,
                //            Message = "Name mismatch",
                //        });
                //    }
                //    else
                //    {
                //        NotifyPassengerChecked(new ResultPack<Models.Passenger>
                //        {
                //            ReturnParam =
                //                new Models.Passenger()
                //                {
                //                    AirlineId = 3,
                //                    CardID = 1303,
                //                    //BarcodeString = "66649651965165461646",
                //                    FullName = "TEST",
                //                    FFN = "1234",
                //                    FromAirport = "LAX",
                //                    ToAirport = "YUL",
                //                    FlightCarrier = "UA",
                //                    FlightNumber = "1234",
                //                    SeatNumber = "13B",
                //                    TrackingClassOfService = "J",
                //                },
                //            ErrorCode = (int)ErrorCode.ExternalValidationFailed,
                //            ErrorMetadata = "\r\nPNR invalid",
                //            GeneratedDate = DateTime.UtcNow,
                //            UID = Guid.NewGuid(),
                //            IsSucceeded = false,
                //            Message = "PNR invalid",
                //        });
                //    }
                //}
                //return;//The rest will not work in DEBUG (I dont want to call server for this test)
#endif
                #endregion

                _proxy.SendJson<ResultPack<TRACKING>> ("validatepassenger", exTrackingInput, DefaultMobileApplication.Current.Position.ToContractPosition (),
                    (aimsSvrResponse, log) => {
                        try {
                            LogsRepository.TrackEvents (log);
                            var result = (ResultPack<TRACKING>)aimsSvrResponse;
                            var passenger = ProcessTracking (result.ReturnParam);
                            NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                ReturnParam = passenger,
                                ErrorCode = result.ErrorCode,
                                ErrorMetadata = result.ErrorMetadata,
                                GeneratedDate = result.GeneratedDate,
                                UID = result.UID,
                                IsSucceeded = result.IsSucceeded,
                                Message =
                                    (result.ErrorCode.HasValue &&
                                    (result.ErrorCode.Value != (int)ErrorCode.ExternalValidationFailed) &&
                                    (result.ErrorCode.Value != (int)ErrorCode.PassengerNameMismatch))
                                    ? LanguageRepository.Current.GetText (result.ErrorCode.Value) :
                                        result.Message
                            });
                        } catch (Exception ex) {
                            LogsRepository.AddError ("Error in ValidateMobileTrackingInput", ex);
                            NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                ReturnParam = null,
                                GeneratedDate = DateTime.Now,
                                IsSucceeded = false,
                                ErrorCode = (int)ErrorCode.UnknownException,
                                Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                            });
                        }
                    },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in ValidateMobileTrackingInput", aimsResult.ReturnParam);
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.UnknownException,
                            Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                        });
                    });
            } catch (CommunicationException ex) {
                LogsRepository.AddError ("Error in ValidatePassengerInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.CommunicationError,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.CommunicationError)//"Error in communicating with server"
                });
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in ValidatePassengerInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
            }
        }

        public void GetPassengerInfo (string barcode, Models.Workstation workstation)
        {
            #region Avoiding second immediate scan to get validated
            try {
                if (SucceededBarcodes.ContainsKey (barcode)) {
                    if (DateTime.UtcNow.Subtract (SucceededBarcodes [barcode]).TotalSeconds < SucceededBarcodeInterval) {
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.TaskAlreadyPerformedException,
                            Message = ""
                        });
                        return;
                    } else SucceededBarcodes.Remove (barcode);
                }
                if (FailedBarcodes.ContainsKey (barcode)) {
                    if (DateTime.UtcNow.Subtract (FailedBarcodes [barcode]).TotalSeconds < FailedBarcodeInterval) {
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.TaskAlreadyPerformedException,
                            Message = ""
                        });
                        return;
                    } else FailedBarcodes.Remove (barcode);
                }
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in GetPassengerInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
                return;
            }
            #endregion

            try {
                var exTrackingInput = new ExtendedMobileTrackingInput ();
                exTrackingInput.LocalTime = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                exTrackingInput.UTCTime = DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss");
                exTrackingInput.TrackingInput = new MobileTrackingInput {
                    ScannerInput = barcode,
                    TrackingPayload = new TRACKING {
                        TRACKING_TIMESTAMP = DateTime.Now,
                        TIMESTAMP_UTC = DateTime.UtcNow,
                        GUID = Guid.NewGuid ().ToString ("N").ToUpper ()
                    },
                    Workstation = workstation.ConvertToWORKSTATION (),
                    AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username,
                };

                if (!DefaultMobileApplication.Current.SecondaryAuthenticationCheck ()) return;
                if (MembershipProvider.Current.SecondValidator.ExternalToken != null)
                    exTrackingInput.TrackingInput.ExternalOauthToken = MembershipProvider.Current.SecondValidator.ExternalToken.AccessToken;
                #region DEBUG simulations
#if DEBUG//TODO: Remove the whole (DEBUG) section below
                //var successTest = false;
                //if (successTest)
                //{
                //    NotifyPassengerChecked(new ResultPack<Models.Passenger>
                //    {
                //        ReturnParam =
                //        new Models.Passenger()
                //        {
                //            AirlineId = 3,
                //            CardID = 1303,
                //            BarcodeString = barcode,
                //            FullName = "TEST",
                //            FFN = "1234",
                //            FromAirport = "LAX",
                //            ToAirport = "YUL",
                //            FlightCarrier = "UA",
                //            FlightNumber = "1234",
                //            SeatNumber = "13B",
                //            TrackingClassOfService = "J",
                //        },
                //        ErrorCode = (int)ErrorCode.NoError,
                //        ErrorMetadata = "",
                //        GeneratedDate = DateTime.UtcNow,
                //        UID = Guid.NewGuid(),
                //        IsSucceeded = true,
                //        Message = "",
                //    });
                //}
                //else
                //{
                //    var incompleteInfo = true;
                //    if (incompleteInfo)
                //    {
                //        NotifyPassengerChecked(new ResultPack<Models.Passenger>
                //        {
                //            ReturnParam =
                //                new Models.Passenger()
                //                {
                //                    AirlineId = 3,
                //                    CardID = 1303,
                //                    BarcodeString = barcode,
                //                    FullName = "TEST",
                //                    FFN = "4321",
                //                    FromAirport = "LAX",
                //                    ToAirport = "YUL",
                //                    FlightCarrier = "UA",
                //                    FlightNumber = "4321",
                //                    SeatNumber = "13B",
                //                    TrackingClassOfService = "J",
                //                },
                //            ErrorCode = (int)ErrorCode.NeedToSwipeBoardingPass,
                //            ErrorMetadata = "5",
                //            GeneratedDate = DateTime.UtcNow,
                //            UID = Guid.NewGuid(),
                //            IsSucceeded = false,
                //            Message = "",
                //        });
                //    }
                //    else
                //    {
                //        NotifyPassengerChecked(new ResultPack<Models.Passenger>
                //        {
                //            ReturnParam =
                //                new Models.Passenger()
                //                {
                //                    AirlineId = 3,
                //                    CardID = 1303,
                //                    BarcodeString = barcode,
                //                    FullName = "TEST",
                //                    FFN = "1234",
                //                    FromAirport = "LAX",
                //                    ToAirport = "YUL",
                //                    FlightCarrier = "UA",
                //                    FlightNumber = "1234",
                //                    SeatNumber = "13B",
                //                    TrackingClassOfService = "J",
                //                },
                //            ErrorCode = (int)ErrorCode.PassengerNameMismatch,
                //            ErrorMetadata = "TEST;DOE/JOHN",
                //            GeneratedDate = DateTime.UtcNow,
                //            UID = Guid.NewGuid(),
                //            IsSucceeded = false,
                //            Message = "Name mismatch",
                //        });
                //    }
                //}
                //return;//The rest will not work in DEBUG (I dont want to call server for this test)
#endif 
                #endregion
                _proxy.SendJson<ResultPack<TRACKING>> ("validatepassenger", exTrackingInput, DefaultMobileApplication.Current.Position.ToContractPosition (),
                     (aimsSvrResponse, log) => {
                         try {
                             LogsRepository.TrackEvents (log);

                             var result = (ResultPack<TRACKING>)aimsSvrResponse;
                             var passenger = ProcessTracking (result.ReturnParam); //result.ReturnParam.ConvertToPassenger();
                             if (result.IsSucceeded)
                                 AddSucceededBarcode (barcode);// It was "AddSucceededBarcode(passenger.BarcodeString);" before - it might change in server side
                             else AddFailedBarcode (barcode);
                             NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                 ReturnParam = passenger,
                                 ErrorCode = result.ErrorCode,
                                 ErrorMetadata = result.ErrorMetadata,
                                 GeneratedDate = result.GeneratedDate,
                                 UID = result.UID,
                                 IsSucceeded = result.IsSucceeded,
                                 Message =
                                    (result.ErrorCode.HasValue &&
                                    (result.ErrorCode.Value != (int)ErrorCode.ExternalValidationFailed) &&
                                    (result.ErrorCode.Value != (int)ErrorCode.PassengerNameMismatch))
                                    ? LanguageRepository.Current.GetText (result.ErrorCode.Value) :
                                        result.Message
                             });
                         } catch (Exception ex) {
                             AddFailedBarcode (barcode);
                             LogsRepository.AddError ("Error in ValidateMobileTrackingInput", ex);
                             NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                 ReturnParam = null,
                                 GeneratedDate = DateTime.Now,
                                 IsSucceeded = false,
                                 ErrorCode = (int)ErrorCode.UnknownException,
                                 Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                             });
                         }
                     },
                    (aimsResult, log) => {
                        LogsRepository.TrackEvents (log);

                        AddFailedBarcode (barcode);
                        LogsRepository.AddError ("Error in ValidateMobileTrackingInput", aimsResult.ReturnParam);
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.UnknownException,
                            Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                        });
                    });
            } catch (CommunicationException ex) {
                AddFailedBarcode (barcode);
                LogsRepository.AddError ("Error in GetPassengerInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.CommunicationError,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.CommunicationError)//"Error in communicating with server"
                });
            } catch (Exception ex) {
                AddFailedBarcode (barcode);
                LogsRepository.AddError ("Error in GetPassengerInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
            }
        }

        public void RetrieveMainPassenger (Models.Passenger selectedTracking, Models.Workstation workstation)
        {
            try {
                var trackingInput = new MobileTrackingInput {
                    TrackingPayload = new TRACKING {
                        TRACKING_TIMESTAMP = selectedTracking.TrackingTimestamp,
                        AIRLINE_ID = selectedTracking.AirlineId,
                        CARD_ID = selectedTracking.CardID,
                        TRACKING_RECORD_ID = selectedTracking.TrackingRecordID,
                        TRACKING_LINKED_RECORD = selectedTracking.GuestOf,
                        GUID = Guid.NewGuid ().ToString ("N").ToUpper ()
                    },
                    Workstation = new WORKSTATION {
                        LOUNGE_ID = selectedTracking.LoungeId,
                        WORKSTATION_ID = -1
                    },
                    AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username,
                };

                if (!DefaultMobileApplication.Current.SecondaryAuthenticationCheck ()) return;
                _proxy.SendJson<ResultPack<TRACKING>> ("retrievepassenger", trackingInput, DefaultMobileApplication.Current.Position.ToContractPosition (),
                     (aimsSvrResponse, log) => {
                         try {
                             LogsRepository.TrackEvents (log);

                             var result = (ResultPack<TRACKING>)aimsSvrResponse;
                             if (result.IsSucceeded) {
                                 var passenger = ProcessTracking (result.ReturnParam);

                                 NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                     ReturnParam = passenger,
                                     ErrorCode = result.ErrorCode,
                                     ErrorMetadata = result.ErrorMetadata,
                                     GeneratedDate = result.GeneratedDate,
                                     UID = result.UID,
                                     IsSucceeded = result.IsSucceeded,
                                     Message = result.Message
                                 });
                             } else {
                                 NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                     ReturnParam = null,
                                     GeneratedDate = DateTime.Now,
                                     IsSucceeded = false,
                                     ErrorCode = (int)ErrorCode.PassengersRetrievalError,
                                     Message = result.Message
                                 });
                             }
                         } catch (Exception ex) {
                             LogsRepository.AddError ("Error in GetTracking", ex);
                             NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                 ReturnParam = null,
                                 GeneratedDate = DateTime.Now,
                                 IsSucceeded = false,
                                 ErrorCode = (int)ErrorCode.UnknownException,
                                 Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                             });
                         }
                     },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in GetTracking", aimsResult.ReturnParam);
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.UnknownException,
                            Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                        });
                    });
            } catch (CommunicationException ex) {
                LogsRepository.AddError ("Error in GetTracking", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.CommunicationError,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.CommunicationError)//"Error in communicating with server"
                });
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in GetTracking", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
            }

        }

        public void RetrieveGuestPassenger (Models.Passenger selectedTracking, Models.Workstation workstation)
        {
            /*
             * serverTracking = 
             * proxyAims.GetTracking(
             * IdentityConventions.NOT_AVAILABLE_ENTITY_ID, 
             * WndMain.m_LoungeId, 
             * m_SelectedPax.AIRLINE_ID, 
             * m_SelectedPax.CARD_ID, 
             * m_SelectedPax.TRACKING_RECORD_ID, 
             * m_SelectedPax.TRACKING_LINKED_RECORD, 
             * m_SelectedPax.TRACKING_TIMESTAMP, 
             * WndMain.m_ConfigAimsConnection, 
             * ref sErrMessage);
             */
            try {
                //No need for date time
                //var exTrackingInput = new ExtendedMobileTrackingInput();
                //exTrackingInput.LocalTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //exTrackingInput.UTCTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                var trackingInput = new MobileTrackingInput {
                    TrackingPayload = new TRACKING {
                        TRACKING_TIMESTAMP = selectedTracking.TrackingTimestamp,
                        AIRLINE_ID = selectedTracking.AirlineId,
                        CARD_ID = selectedTracking.CardID,
                        TRACKING_RECORD_ID = selectedTracking.TrackingRecordID,
                        TRACKING_LINKED_RECORD = selectedTracking.GuestOf,
                        GUID = Guid.NewGuid ().ToString ("N").ToUpper ()
                    },
                    Workstation = new WORKSTATION {
                        LOUNGE_ID = selectedTracking.LoungeId,
                        WORKSTATION_ID = -1
                    },
                    AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username,
                };

                if (!DefaultMobileApplication.Current.SecondaryAuthenticationCheck ()) return;
                _proxy.SendJson<ResultPack<TRACKING>> ("retrievepassenger", trackingInput, DefaultMobileApplication.Current.Position.ToContractPosition (),
                     (aimsSvrResponse, log) => {
                         try {
                             LogsRepository.TrackEvents (log);

                             var result = (ResultPack<TRACKING>)aimsSvrResponse;
                             if (result.IsSucceeded) {
                                 if (result.ReturnParam == null || (selectedTracking.GuestOf > 0 && result.ReturnParam.GUEST_LIST == null)) {
                                     NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                         ReturnParam = null,
                                         GeneratedDate = DateTime.Now,
                                         IsSucceeded = false,
                                         ErrorCode = (int)ErrorCode.PassengersRetrievalError,
                                         Message = String.Format ("Retreived PAX is empty {0}", result.Message)
                                     });
                                     return;
                                 }

                                 Models.Passenger passenger;

                                 if (selectedTracking.GuestOf > 0 && result.ReturnParam.GUEST_LIST != null)//It's a guest
                                     passenger = ProcessTracking (result.ReturnParam.GUEST_LIST.FirstOrDefault (x => x.TRACKING_RECORD_ID == selectedTracking.TrackingRecordID));
                                 else//It's a main pax
                                     passenger = ProcessTracking (result.ReturnParam);

                                 NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                     ReturnParam = passenger,
                                     ErrorCode = result.ErrorCode,
                                     ErrorMetadata = result.ErrorMetadata,
                                     GeneratedDate = result.GeneratedDate,
                                     UID = result.UID,
                                     IsSucceeded = result.IsSucceeded,
                                     Message = result.Message
                                 });
                             } else {
                                 NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                     ReturnParam = null,
                                     GeneratedDate = DateTime.Now,
                                     IsSucceeded = false,
                                     ErrorCode = (int)ErrorCode.PassengersRetrievalError,
                                     Message = result.Message
                                 });
                             }
                         } catch (Exception ex) {
                             LogsRepository.AddError ("Error in GetTracking", ex);
                             NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                 ReturnParam = null,
                                 GeneratedDate = DateTime.Now,
                                 IsSucceeded = false,
                                 ErrorCode = (int)ErrorCode.UnknownException,
                                 Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                             });
                         }
                     },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in GetTracking", aimsResult.ReturnParam);
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.UnknownException,
                            Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                        });
                    });
            } catch (CommunicationException ex) {
                LogsRepository.AddError ("Error in GetTracking", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.CommunicationError,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.CommunicationError)//"Error in communicating with server"
                });
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in GetTracking", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
            }

        }

        public void ValidateGuestInfo (Models.Passenger hostPassenger, Models.Passenger guestPassenger, PassengerServiceType guestType, Models.Workstation workstation)
        {
            try {
                var exGuestValidationInput = new ExtendedGuestValidationInput ();
                exGuestValidationInput.GuestValidationInput = new GuestValidationInput {
                    GuestPassenger = new MobileTrackingInput {
                        ScannerInput = null,
                        TrackingPayload = guestPassenger.ConvertToTracking (),
                        Workstation = workstation.ConvertToWORKSTATION (),
                        AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username
                    },
                    MainPaxTracking = hostPassenger.ConvertToTracking (),
                    PassengerServiceType = (Contracts.AIMSClient.PassengerServiceTypes)((int)guestType)
                };
                exGuestValidationInput.GuestValidationInput.GuestPassenger.TrackingPayload.WORKSTATION_ID = MembershipProvider.Current.SelectedLounge.WorkstationID;
                exGuestValidationInput.GuestValidationInput.GuestPassenger.TrackingPayload.LOUNGE_ID = MembershipProvider.Current.SelectedLounge.LoungeID;
                //{
                //    ScannerInput = null,
                //    TrackingPayload = MembershipProvider.Current.SelectedPassenger.ConvertToTracking(),
                //    Workstation = workstation.ConvertToWORKSTATION(),
                //    AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username
                //};
                if (!DefaultMobileApplication.Current.SecondaryAuthenticationCheck ()) return;
                if (MembershipProvider.Current.SecondValidator.ExternalToken != null)
                    exGuestValidationInput.GuestValidationInput.GuestPassenger.ExternalOauthToken = MembershipProvider.Current.SecondValidator.ExternalToken.AccessToken;
                if (exGuestValidationInput.GuestValidationInput.GuestPassenger.TrackingPayload.TRACKING_TIMESTAMP == default (DateTime)) {
                    exGuestValidationInput.GuestValidationInput.GuestPassenger.TrackingPayload.TRACKING_TIMESTAMP = DateTime.Now;
                    exGuestValidationInput.LocalTime = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                    exGuestValidationInput.GuestValidationInput.GuestPassenger.TrackingPayload.TIMESTAMP_UTC = DateTime.UtcNow;
                    exGuestValidationInput.UTCTime = DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss");

                }

                _proxy.SendJson<ResultPack<TRACKING>> ("validateguest", exGuestValidationInput, DefaultMobileApplication.Current.Position.ToContractPosition (),
                    (aimsSvrResponse, log) => {
                        try {
                            LogsRepository.TrackEvents (log);

                            var result = (ResultPack<TRACKING>)aimsSvrResponse;
                            var passenger = ProcessTracking (result.ReturnParam);

                            if (result.IsSucceeded) {
                                #region Handling the services and retrieving the guest
                                if (passenger == null) {
                                    NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                        ReturnParam = null,
                                        GeneratedDate = DateTime.Now,
                                        IsSucceeded = false,
                                        ErrorCode = (int)ErrorCode.UnknownException,
                                        ErrorMetadata = "Host passenger is not returned properly",
                                        Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                                    });
                                    return;
                                }
                                if (!passenger.PassengerServices.Any (x => x.Key.Id == (int)guestType)) {
                                    NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                        ReturnParam = null,
                                        GeneratedDate = DateTime.Now,
                                        IsSucceeded = false,
                                        ErrorCode = (int)ErrorCode.UnknownException,
                                        ErrorMetadata = "The required guest ServiceType can not be found in the host passenger",
                                        Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                                    });
                                    return;
                                }
                                var passengerService = passenger.PassengerServices.FirstOrDefault (x => x.Key.Id == (int)guestType);
                                passenger = passengerService.Value.FirstOrDefault (x => x.GUID.Equals (exGuestValidationInput.GuestValidationInput.GuestPassenger.TrackingPayload.GUID));
                                if (passenger == null) {
                                    NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                        ReturnParam = null,
                                        GeneratedDate = DateTime.Now,
                                        IsSucceeded = false,
                                        ErrorCode = (int)ErrorCode.UnknownException,
                                        ErrorMetadata = "Guest is not returned properly",
                                        Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                                    });
                                    return;
                                }
                                #endregion

                                #region Adding guest to the list of SelectedPassenger
                                //TODO: Add successful guest to the host 
                                if (MembershipProvider.Current.SelectedPassenger.PassengerServices [MembershipProvider.Current.SelectedPassengerService] == null)
                                    MembershipProvider.Current.SelectedPassenger.PassengerServices [MembershipProvider.Current.SelectedPassengerService] = new List<Models.Passenger> ();

                                MembershipProvider.Current.SelectedGuest = passenger;
                                //In serverside we are checking the passenger's guest limitations (No need for this)
                                if (MembershipProvider.Current.SelectedPassengerService.MaximumQuantity > MembershipProvider.Current.SelectedPassenger.PassengerServices [MembershipProvider.Current.SelectedPassengerService].Count)
                                    MembershipProvider.Current.SelectedPassenger.PassengerServices [MembershipProvider.Current.SelectedPassengerService].Add (passenger);
                                #endregion
                            } else if (!result.ErrorCode.HasValue ||
                              (result.ErrorCode.Value != (int)ErrorCode.PassengerNameMismatch &&
                              result.ErrorCode.Value != (int)ErrorCode.NeedToSwipeBoardingPass))
                                passenger = null;//TODO: Make sure its tested


                            NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                ReturnParam = passenger,
                                ErrorCode = result.ErrorCode,
                                ErrorMetadata = result.ErrorMetadata,
                                GeneratedDate = result.GeneratedDate,
                                UID = result.UID,
                                IsSucceeded = result.IsSucceeded,
                                Message =
                                    (result.ErrorCode.HasValue &&
                                    (result.ErrorCode.Value != (int)ErrorCode.ExternalValidationFailed) &&
                                    (result.ErrorCode.Value != (int)ErrorCode.PassengerNameMismatch))
                                    ? LanguageRepository.Current.GetText (result.ErrorCode.Value) :
                                        result.Message
                            });
                        } catch (Exception ex) {
                            LogsRepository.AddError ("Error in ValidateGuestMobileTrackingInput", ex);
                            NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                ReturnParam = null,
                                GeneratedDate = DateTime.Now,
                                IsSucceeded = false,
                                ErrorCode = (int)ErrorCode.UnknownException,
                                Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                            });
                        }
                    },
                    (aimsResult, log) => {
                        LogsRepository.AddError ("Error in ValidateGuestMobileTrackingInput", aimsResult.ReturnParam);
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.UnknownException,
                            Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                        });
                    });

            } catch (CommunicationException ex) {
                LogsRepository.AddError ("Error in ValidateGuestInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.CommunicationError,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.CommunicationError)//"Error in communicating with server"
                });
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in ValidateGuestInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
            }
        }

        public void GetGuestInfo (Models.Passenger hostPassenger, string barcode, PassengerServiceType guestType, Models.Workstation workstation)
        {
            #region Checking the immediate duplicate scan calls
            try {
                if (SucceededBarcodes.ContainsKey (barcode)) {
                    if (DateTime.UtcNow.Subtract (SucceededBarcodes [barcode]).TotalSeconds < SucceededBarcodeInterval) {
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.TaskAlreadyPerformedException,
                            Message = ""
                        });
                        return;
                    } else SucceededBarcodes.Remove (barcode);
                }
                if (FailedBarcodes.ContainsKey (barcode)) {
                    if (DateTime.UtcNow.Subtract (FailedBarcodes [barcode]).TotalSeconds < FailedBarcodeInterval) {
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.TaskAlreadyPerformedException,
                            Message = ""
                        });
                        return;
                    } else FailedBarcodes.Remove (barcode);
                }
            } catch (Exception ex) {
                LogsRepository.AddError ("Error in GetPassengerInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
                return;
            }
            #endregion

            try {
                var exGuestValidationInput = new ExtendedGuestValidationInput ();
                exGuestValidationInput.LocalTime = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                exGuestValidationInput.UTCTime = DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss");
                exGuestValidationInput.GuestValidationInput = new GuestValidationInput {
                    GuestPassenger = new MobileTrackingInput {
                        ScannerInput = barcode,
                        TrackingPayload = new TRACKING {
                            TRACKING_TIMESTAMP = DateTime.Now,
                            TIMESTAMP_UTC = DateTime.UtcNow,
                            GUID = Guid.NewGuid ().ToString ("N").ToUpper ()
                        },
                        Workstation = workstation.ConvertToWORKSTATION (),
                        AuthenticatedUser = MembershipProvider.Current.LoggedinUser.Username,
                    },
                    MainPaxTracking = hostPassenger.ConvertToTracking (),
                    PassengerServiceType = (Contracts.AIMSClient.PassengerServiceTypes)((int)guestType)
                };

                if (!DefaultMobileApplication.Current.SecondaryAuthenticationCheck ()) return;
                if (MembershipProvider.Current.SecondValidator.ExternalToken != null)
                    exGuestValidationInput.GuestValidationInput.GuestPassenger.ExternalOauthToken = MembershipProvider.Current.SecondValidator.ExternalToken.AccessToken;

                _proxy.SendJson<ResultPack<TRACKING>> ("validateguest", exGuestValidationInput, DefaultMobileApplication.Current.Position.ToContractPosition (),
                     (aimsSvrResponse, log) => {
                         try {
                             LogsRepository.TrackEvents (log);

                             var result = (ResultPack<TRACKING>)aimsSvrResponse;
                             var passenger = ProcessTracking (result.ReturnParam); //result.ReturnParam.ConvertToPassenger();
                             if (result.IsSucceeded)
                                 AddSucceededBarcode (barcode);// It was "AddSucceededBarcode(passenger.BarcodeString);" before - it might change in server side
                             else AddFailedBarcode (barcode);

                             if (result.IsSucceeded) {
                                 #region Handling the services and retrieving the guest
                                 if (passenger == null) {
                                     NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                         ReturnParam = null,
                                         GeneratedDate = DateTime.Now,
                                         IsSucceeded = false,
                                         ErrorCode = (int)ErrorCode.UnknownException,
                                         ErrorMetadata = "Host passenger is not returned properly",
                                         Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                                     });
                                     return;
                                 } else if (!passenger.PassengerServices.Any (x => x.Key.Id == (int)guestType)) {
                                     NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                         ReturnParam = null,
                                         GeneratedDate = DateTime.Now,
                                         IsSucceeded = false,
                                         ErrorCode = (int)ErrorCode.UnknownException,
                                         ErrorMetadata = "The required guest ServiceType can not be found in the host passenger",
                                         Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                                     });
                                     return;
                                 }
                                 var passengerService = passenger.PassengerServices.FirstOrDefault (x => x.Key.Id == (int)guestType);
                                 passenger = passengerService.Value.FirstOrDefault (x => x.GUID.Equals (exGuestValidationInput.GuestValidationInput.GuestPassenger.TrackingPayload.GUID));
                                 if (passenger == null) {
                                     NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                         ReturnParam = null,
                                         GeneratedDate = DateTime.Now,
                                         IsSucceeded = false,
                                         ErrorCode = (int)ErrorCode.UnknownException,
                                         ErrorMetadata = "Guest is not returned properly",
                                         Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                                     });
                                     return;
                                 }
                                 #endregion

                                 #region Adding guest to the list of SelectedPassenger
                                 //TODO: Add successful guest to the host 
                                 if (MembershipProvider.Current.SelectedPassenger.PassengerServices [MembershipProvider.Current.SelectedPassengerService] == null)
                                     MembershipProvider.Current.SelectedPassenger.PassengerServices [MembershipProvider.Current.SelectedPassengerService] = new List<Models.Passenger> ();

                                 MembershipProvider.Current.SelectedGuest = passenger;
                                 //In serverside we are checking the passenger's guest limitations (No need for this)
                                 if (MembershipProvider.Current.SelectedPassengerService.MaximumQuantity > MembershipProvider.Current.SelectedPassenger.PassengerServices [MembershipProvider.Current.SelectedPassengerService].Count)
                                     MembershipProvider.Current.SelectedPassenger.PassengerServices [MembershipProvider.Current.SelectedPassengerService].Add (passenger);
                                 #endregion
                             } else if (!result.ErrorCode.HasValue ||
                               (result.ErrorCode.Value != (int)ErrorCode.PassengerNameMismatch &&
                               result.ErrorCode.Value != (int)ErrorCode.NeedToSwipeBoardingPass))
                                 passenger = null;//TODO: Make sure its tested

                             NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                 ReturnParam = passenger,
                                 ErrorCode = result.ErrorCode,
                                 ErrorMetadata = result.ErrorMetadata,
                                 GeneratedDate = result.GeneratedDate,
                                 UID = result.UID,
                                 IsSucceeded = result.IsSucceeded,
                                 Message =
                                    (result.ErrorCode.HasValue &&
                                    (result.ErrorCode.Value != (int)ErrorCode.ExternalValidationFailed) &&
                                    (result.ErrorCode.Value != (int)ErrorCode.PassengerNameMismatch))
                                    ? LanguageRepository.Current.GetText (result.ErrorCode.Value) :
                                        result.Message
                             });
                         } catch (Exception ex) {
                             AddFailedBarcode (barcode);
                             LogsRepository.AddError ("Error in ValidateGuestMobileTrackingInput", ex);
                             NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                                 ReturnParam = null,
                                 GeneratedDate = DateTime.Now,
                                 IsSucceeded = false,
                                 ErrorCode = (int)ErrorCode.UnknownException,
                                 Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                             });
                         }
                     },
                    (aimsResult, log) => {
                        AddFailedBarcode (barcode);
                        LogsRepository.AddError ("Error in ValidateGuestMobileTrackingInput", aimsResult.ReturnParam);
                        NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                            ReturnParam = null,
                            GeneratedDate = DateTime.Now,
                            IsSucceeded = false,
                            ErrorCode = (int)ErrorCode.UnknownException,
                            Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                        });
                    });
            } catch (CommunicationException ex) {
                AddFailedBarcode (barcode);
                LogsRepository.AddError ("Error in GetGuestInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.CommunicationError,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.CommunicationError)//"Error in communicating with server"
                });
            } catch (Exception ex) {
                AddFailedBarcode (barcode);
                LogsRepository.AddError ("Error in GetGuestInfo", ex);
                NotifyPassengerChecked (new ResultPack<Models.Passenger> {
                    ReturnParam = null,
                    GeneratedDate = DateTime.Now,
                    IsSucceeded = false,
                    ErrorCode = (int)ErrorCode.UnknownException,
                    Message = LanguageRepository.Current.GetText ((int)ErrorCode.UnknownException)
                });
            }
        }

        private void NotifyPassengerChecked (ResultPack<Models.Passenger> passengerCheckResult)
        {
            if (OnPassengerChecked == null || passengerCheckResult == null) return;

            #region All the Client side validation rules (Just to manage the case that the returned Client card is incorrect)

            //if (!passengerCheckResult.IsSucceeded && passengerCheckResult.ErrorCode.HasValue &&
            //    passengerCheckResult.ErrorCode.Value == (int)ErrorCode.NeedToSwipeBoardingPass)//TODO: remove this part (no logic on client-side)
            //{
            //    if (!string.IsNullOrWhiteSpace(passengerCheckResult.ErrorMetadata))
            //        if (passengerCheckResult.ErrorMetadata.ToLower().Contains("error") || passengerCheckResult.ErrorMetadata.ToLower().Contains("external validation - "))
            //        {
            //            //passengerCheckResult.Message = passengerCheckResult.ErrorMetadata;
            //            passengerCheckResult.ErrorMetadata = "";
            //            passengerCheckResult.ErrorCode = 10;
            //            passengerCheckResult.Message = "Card and Airline info are incorrect";
            //        }
            //}

            if (!passengerCheckResult.IsSucceeded && passengerCheckResult.ErrorCode.Value == (int)ErrorCode.PassengerNameMismatch)//TODO: remove this part (no logic on client-side)
                passengerCheckResult.Message = "Passenger is denied due to name mismatch:\n" + passengerCheckResult.Message;//we dont have error code
            #endregion

            OnPassengerChecked.Invoke (passengerCheckResult.IsSucceeded, passengerCheckResult.ErrorCode, passengerCheckResult.ErrorMetadata, passengerCheckResult.Message, passengerCheckResult.ReturnParam);
        }

        private void AddSucceededBarcode (string barcode)
        {
            var removingOnes = SucceededBarcodes.Where (x => DateTime.UtcNow.Subtract (x.Value).TotalSeconds > SucceededBarcodeInterval).Select (x => x.Key).ToList ();
            foreach (var removingBarcode in removingOnes) {
                SucceededBarcodes.Remove (removingBarcode);
            }

            if (SucceededBarcodes.ContainsKey (barcode)) {
                SucceededBarcodes [barcode] = DateTime.UtcNow;
            } else SucceededBarcodes.Add (barcode, DateTime.UtcNow);
        }

        private void AddFailedBarcode (string barcode)
        {
            var removingOnes = FailedBarcodes.Where (x => DateTime.UtcNow.Subtract (x.Value).TotalSeconds > FailedBarcodeInterval).Select (x => x.Key).ToList ();
            foreach (var removingBarcode in removingOnes) {
                FailedBarcodes.Remove (removingBarcode);
            }

            if (FailedBarcodes.ContainsKey (barcode)) {
                FailedBarcodes [barcode] = DateTime.UtcNow;
            } else FailedBarcodes.Add (barcode, DateTime.UtcNow);
        }

        private List<Models.Passenger> ProcessTrackingList (IEnumerable<LightTrackingInfo> passengers)
        {
            if (passengers == null) return new List<Models.Passenger> ();
            var results = new List<Models.Passenger> ();

            passengers = passengers.Where (c => c.TrackingRecordType != 3 && c.TrackingRecordType != 5);

            //This ordering should be done in server-side
            var allGuests = passengers.Where (x => x.TrackingLinkedRecordId != 0).OrderByDescending (x => x.TrackingTimestamp).ToList ();
            var hosts = passengers.Where (x => x.TrackingLinkedRecordId == 0).OrderByDescending (x => x.TrackingTimestamp).ToList ();

            foreach (var host in hosts) {
                var guests = allGuests.Where (x => x.TrackingLinkedRecordId == host.TrackingRecordId).ToList ();

                results.Add (host.ConvertToPassenger ());

                //Do not remove the guests as they still need to be displayed in the main list
                foreach (var g in guests) {
                    results.Add (g.ConvertToPassenger ());
                    allGuests.Remove (g);
                }
            }
            foreach (var guest in allGuests) {
                results.Add (guest.ConvertToPassenger ());
            }

            return results.GroupBy (c => c.TrackingRecordID).Select (g => g.First ()).ToList ();
        }

        private Models.Passenger ProcessTracking (Models.Passenger passenger, List<TRACKING> guestList, decimal cardId, decimal otherCardId)
        {
            if (passenger == null) return passenger;
            var passengerServices = CardsRepository.GetCardServices (new List<decimal> { cardId, otherCardId });

            foreach (var service in passengerServices) {
                passenger.PassengerServices.Add (service, new List<Models.Passenger> ());
            }

            if (guestList != null) {
                foreach (var trackingObject in guestList)//TODO: Update is required
                {
                    decimal compGuests = 0, famGuests = 0, paidGuests = 0;
                    Models.Passenger guest;

                    guest = trackingObject.ConvertToPassenger ();
                    compGuests = trackingObject.TRACKING_NB_GUESTS_COMP;
                    famGuests = trackingObject.TRACKING_NB_GUESTS_FAM;
                    paidGuests = trackingObject.TRACKING_NB_GUESTS_PAY;


                    KeyValuePair<PassengerService, List<Models.Passenger>> service;
                    if (compGuests > 0) {
                        service = passenger.PassengerServices.FirstOrDefault (x => x.Key.Id == 1);
                    } else if (famGuests > 0) {
                        service = passenger.PassengerServices.FirstOrDefault (x => x.Key.Id == 2);
                    } else if (paidGuests > 0) {
                        service = passenger.PassengerServices.FirstOrDefault (x => x.Key.Id == 3);
                    }

                    try {
                        service.Value.Add (guest);
                    } catch {
                    }
                }
            }

            return passenger;
        }

        private Models.Passenger ProcessTracking (TRACKING tracking)
        {
            if (tracking == null) return null;
            return ProcessTracking (tracking.ConvertToPassenger (), tracking.GUEST_LIST, tracking.CARD_ID, tracking.CARD_ID_OTHER);
            #region Old Codes (must be removed)
            //var passenger = tracking.ConvertToPassenger();

            //var passengerServices = CardsRepository.GetCardServices(new List<decimal> { tracking.CARD_ID, tracking.CARD_ID_OTHER });

            //foreach (var service in passengerServices)
            //{
            //    passenger.PassengerServices.Add(service, new List<Models.Passenger>());
            //}

            //if (tracking.GUEST_LIST != null)
            //    foreach (var guestTracking in tracking.GUEST_LIST)//TODO: Update is required
            //    {
            //        var guest = guestTracking.ConvertToPassenger();
            //        KeyValuePair<Models.PassengerService, List<Models.Passenger>> service;
            //        if (guestTracking.TRACKING_NB_GUESTS_COMP > 0)
            //        {
            //            service = passenger.PassengerServices.FirstOrDefault(x => x.Key.Id == 1);
            //        }
            //        else if (guestTracking.TRACKING_NB_GUESTS_FAM > 0)
            //        {
            //            service = passenger.PassengerServices.FirstOrDefault(x => x.Key.Id == 2);
            //        }
            //        else if (guestTracking.TRACKING_NB_GUESTS_PAY > 0)
            //        {
            //            service = passenger.PassengerServices.FirstOrDefault(x => x.Key.Id == 3);
            //        }

            //        try
            //        {
            //            service.Value.Add(guest);
            //        }
            //        catch
            //        {
            //        }
            //    }

            //return passenger; 
            #endregion
        }

    }
}