﻿using AIMS.Contracts.AIMSClient;
using AIMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMS
{
    public static class Extensions
    {
        private static string ExtractBarcode(string TRACKING_SYSTEM_INFO)
        {
#if DEBUG
            if (!string.IsNullOrEmpty(TRACKING_SYSTEM_INFO))
            {
                var values = TRACKING_SYSTEM_INFO.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                var barcodeVal = values.FirstOrDefault(x => x.StartsWith("Source"));
                if (barcodeVal != null)
                    return barcodeVal.Substring(7);
            }
#endif
            return string.Empty;
        }

        //public static LAST_HOUR ConvertToLAST_HOUR(this Passenger passenger)
        //{
        //    return new LAST_HOUR
        //    {
        //        BOARDING_PASS_TO = passenger.ToAirport,
        //        BOARDING_PASS_FROM = passenger.FromAirport,
        //        TRACKING_PASSENGER_NAME = passenger.FullName,
        //        TRACKING_PASSENGER_FF_NUMBER = passenger.FFN,
        //        BOARDING_PASS_OP_CARRIER = passenger.FlightCarrier,
        //        TRACKING_PASS_FLIGHT_NUMBER = passenger.FlightNumber,
        //        TRACKING_CLASS_OF_SERVICE = passenger.TrackingClassOfService,
        //        BOARDING_PASS_PNR = passenger.PNR,
        //        BOARDING_PASS_STATUS = passenger.PassengerStatus,
        //        TRACKING_RECORD_ID = passenger.TrackingRecordID,
        //        CARD_ID = passenger.CardID,
        //        TRACKING_LINKED_RECORD = passenger.GuestOf,
        //        AIRLINE_ID = passenger.AirlineId,
        //        TRACKING_OTHER_INFO = passenger.Notes,
        //        CARD_ID_OTHER = passenger.OtherCardID,
        //        BOARDING_PASS_SEAT_NB = passenger.SeatNumber,
        //        WORKSTATION_ID = passenger.WorkstationId,
        //    };
        //}

        //public static DAL.Passenger ConvertToDALPassenger(this Models.Passenger passenger)
        //{
        //    return new DAL.Passenger
        //    {
        //        ToAirport = passenger.ToAirport,
        //        FromAirport = passenger.FromAirport,
        //        FullName = passenger.FullName,
        //        TrackingTimestamp = passenger.TrackingTimestamp,
        //        TimestampUtc = passenger.TimestampUtc,
        //        FFN = passenger.FFN,
        //        TrackingPassCombinedFlightNumber = passenger.TrackingPassCombinedFlightNumber,
        //        FlightCarrier = passenger.FlightCarrier,
        //        FlightNumber = passenger.FlightNumber,
        //        TrackingClassOfService = passenger.TrackingClassOfService,
        //        TrackingSystemInfo = passenger.TrackingSystemInfo,
        //        BoardingPassCompCode = passenger.BoardingPassCompCode,
        //        BoardingPassFlightCarrier = passenger.BoardingPassFlightCarrier,
        //        BoardingPassFlightNumber = passenger.BoardingPassFlightNumber,
        //        BoardingPassFFAirline = passenger.BoardingPassFFAirline,
        //        BoardingPassFFNumber = passenger.BoardingPassFFNumber,
        //        BoardingPassPaxDesc = passenger.BoardingPassPaxDesc,
        //        BoardingPassEIndicator = passenger.BoardingPassEIndicator,
        //        BoardingPassFlightDate = passenger.BoardingPassFlightDate,
        //        BoardingPassSeq = passenger.BoardingPassSeq,
        //        BoardingPassName = passenger.BoardingPassName,
        //        DepartureDate = passenger.DepartureDate,
        //        DepartureTime = passenger.DepartureTime,
        //        PNR = passenger.PNR,
        //        PassengerStatus = passenger.PassengerStatus,
        //        TrackingRecordID = passenger.TrackingRecordID,
        //        CardID = passenger.CardID,
        //        GuestOf = passenger.GuestOf,
        //        AirlineId = passenger.AirlineId,
        //        Notes = passenger.Notes,
        //        OtherCardID = passenger.OtherCardID,
        //        SeatNumber = passenger.SeatNumber,
        //        WorkstationId = passenger.WorkstationId,
        //        TrackingStatus = passenger.TrackingStatus,
        //        BarcodeString = passenger.BarcodeString,
        //        FailedReason = passenger.FailedReason,
        //    };
        //}

        public static Models.Message ConvertToMessage(this BULLETIN bulletin)
        {
            return new Models.Message
            {
                BulletinId = bulletin.BULLETIN_ID,
                Title = bulletin.BULLETIN_TITLE,
                Handle = bulletin.BULLETIN_HANDLE,
                Time = bulletin.BULLETIN_DATE_RECEIVED,
                Attachment = bulletin.BULLETIN_ATTACHMENT_HANDLE,
                Type = bulletin.BULLETIN_TYPE,
                From = "IEG"
            };
        }

        public static Models.Passenger ConvertToPassenger(this LightTrackingInfo passenger)
        {
            if (passenger == null) return null;
            var passengerServices = new Dictionary<Models.PassengerService, List<Models.Passenger>>();
            
            return new Models.Passenger
            {
                FullName = passenger.TrackingPassengerName,
                HostName = passenger.HostName,
                CardName = passenger.CardName,
                LoungeName = passenger.LoungeName,
                LoungeId = passenger.LoungeId,
                TrackingTimestamp = passenger.TrackingTimestamp,
                FFN = passenger.TrackingPassengerFfNumber,
                TrackingRecordID = passenger.TrackingRecordId,
                FlightNumber = passenger.TrackingPassFlightNumber,
                TrackingClassOfService = passenger.TrackingClassOfService,
                HasPnrHistory = passenger.HasPnrHistory,
                BoardingPassFlightNumber = passenger.TrackingPassFlightNumber,
                BoardingPassFFNumber = passenger.TrackingPassengerFfNumber,
                GuestOf = passenger.TrackingLinkedRecordId,
                Type = passenger.TrackingRecordType,
                SpecialHandling = passenger.TrackingSpecialHandling,
                BoardingPassName = passenger.TrackingPassengerName,

                TrackingStatus = (int)TrackingStatusEnum.SYNCHRONIZED,
                GUID = Guid.NewGuid().ToString("N").ToUpper(),
                PassengerServices = passengerServices,

                //TimestampUtc = passenger.TRACKING_TIMESTAMP,
                //TrackingPassCombinedFlightNumber = passenger.TrackingPassFlightNumber,
                //FlightCarrier = !string.IsNullOrWhiteSpace(passenger.TRACKING_PASS_FLIGHT_NUMBER) ? new string(passenger.TRACKING_PASS_FLIGHT_NUMBER.ToCharArray().Take(2).ToArray()) : string.Empty,
                //TrackingSystemInfo = passenger.TRACKING_SYSTEM_INFO,
                //BoardingPassCompCode = passenger.BOARDING_PASS_COMP_CODE,
                //BoardingPassFlightCarrier = passenger.BOARDING_PASS_OP_CARRIER,
                //BoardingPassFFAirline = passenger.BOARDING_PASS_FF_AIRLINE,
                //BoardingPassPaxDesc = passenger.BOARDING_PASS_PAX_DESC,
                //BoardingPassEIndicator = passenger.BOARDING_PASS_E_INDICATOR,
                //BoardingPassFlightDate = passenger.BOARDING_PASS_FLIGHT_DATE,
                //BoardingPassSeq = passenger.BOARDING_PASS_SEQ,
                //DepartureDate = passenger.ACT_DEPT_TIME,
                //DepartureTime = passenger.ACT_DEPT_TIME,
                //PNR = passenger.BOARDING_PASS_PNR,
                //PassengerStatus = passenger.BOARDING_PASS_STATUS,
                //CardID = passenger.CARD_ID,
                //AirlineId = passenger.AIRLINE_ID,
                //Notes = passenger.TRACKING_OTHER_INFO,
                //OtherCardID = passenger.CARD_ID_OTHER,
                //SeatNumber = passenger.BOARDING_PASS_SEAT_NB,
                //GateNumber = passenger.GATE,
                //Terminal = passenger.//TODO: from flightstat
                //WorkstationId = passenger.WORKSTATION_ID,
                //BarcodeString = ExtractBarcode(passenger.TRACKING_SYSTEM_INFO),
            };
        }

        public static Models.Passenger ConvertToPassenger(this TRACKING passenger)
        {
            if (passenger == null) return null;

            var passengerServices = new Dictionary<Models.PassengerService, List<Models.Passenger>>();
            //string fName = "", lName = "";
            //if (tracking.BOARDING_PASS_NAME.Contains("/"))
            //{
            //    lName = tracking.BOARDING_PASS_NAME.Substring(0, tracking.BOARDING_PASS_NAME.IndexOf("/"));
            //    fName = tracking.BOARDING_PASS_NAME.Substring(tracking.BOARDING_PASS_NAME.IndexOf("/") + 1);
            //}
            //else
            //    lName = tracking.BOARDING_PASS_NAME;

            return new Models.Passenger
            {
                IsReEntry = passenger.TRACKING_RECORD_TYPE == 3,
                IsLoungeHopping = passenger.TRACKING_RECORD_TYPE == 6,
                ToAirport = passenger.BOARDING_PASS_TO,
                FromAirport = passenger.BOARDING_PASS_FROM,
                //FirstName = fName,
                //LastName = lName,
                FullName = passenger.TRACKING_PASSENGER_NAME,
                TrackingTimestamp = passenger.TRACKING_TIMESTAMP,
                TimestampUtc = passenger.TRACKING_TIMESTAMP,
                FFN = passenger.TRACKING_PASSENGER_FF_NUMBER,
                TrackingPassCombinedFlightNumber = passenger.TRACKING_PASS_FLIGHT_NUMBER,
                //FlightCarrier = new string(passenger.TRACKING_PASS_FLIGHT_NUMBER.ToCharArray().Take(2).ToArray()),
                //FlightNumber = new string(passenger.TRACKING_PASS_FLIGHT_NUMBER.ToCharArray().Skip(2).Take(4).ToArray()),
                //TrackingClassOfService = passenger.TRACKING_CLASS_OF_SERVICE,
                FlightCarrier = !string.IsNullOrWhiteSpace(passenger.TRACKING_PASS_FLIGHT_NUMBER) ? new string(passenger.TRACKING_PASS_FLIGHT_NUMBER.ToCharArray().Take(2).ToArray()) : string.Empty,
                FlightNumber = !string.IsNullOrWhiteSpace(passenger.TRACKING_PASS_FLIGHT_NUMBER) ? new string(passenger.TRACKING_PASS_FLIGHT_NUMBER.ToCharArray().Skip(2).Take(4).ToArray()) : string.Empty,
                TrackingClassOfService = !string.IsNullOrWhiteSpace(passenger.TRACKING_CLASS_OF_SERVICE) ? passenger.TRACKING_CLASS_OF_SERVICE : passenger.BOARDING_PASS_COMP_CODE,
                TrackingSystemInfo = passenger.TRACKING_SYSTEM_INFO,
                BoardingPassCompCode = passenger.BOARDING_PASS_COMP_CODE,
                BoardingPassFlightCarrier = passenger.BOARDING_PASS_OP_CARRIER,
                BoardingPassFlightNumber = passenger.BOARDING_PASS_FLIGHT_NUMBER,
                BoardingPassFFAirline = passenger.BOARDING_PASS_FF_AIRLINE,
                BoardingPassFFNumber = passenger.BOARDING_PASS_FF_NUMBER,
                BoardingPassPaxDesc = passenger.BOARDING_PASS_PAX_DESC,
                BoardingPassEIndicator = passenger.BOARDING_PASS_E_INDICATOR,
                BoardingPassSeq = passenger.BOARDING_PASS_SEQ,
                BoardingPassFlightDate = passenger.BOARDING_PASS_FLIGHT_DATE,
                BoardingPassName = passenger.BOARDING_PASS_NAME,
                DepartureDate = passenger.ACT_DEPT_TIME,
                DepartureTime = passenger.ACT_DEPT_TIME,
                PNR = passenger.BOARDING_PASS_PNR,
                PassengerStatus = passenger.BOARDING_PASS_STATUS,
                TrackingRecordID = passenger.TRACKING_RECORD_ID,
                CardID = passenger.CARD_ID,
                IsValid = passenger.TRACKING_IS_VALID,
                GuestOf = passenger.TRACKING_LINKED_RECORD,
                AirlineId = passenger.AIRLINE_ID,
                OtherAirlineId = passenger.AIRLINE_ID_OTHER,
                Notes = passenger.TRACKING_OTHER_INFO,
                OtherCardID = passenger.CARD_ID_OTHER,
                SeatNumber = passenger.BOARDING_PASS_SEAT_NB,
                //GateNumber = passenger.//TODO: from flightstat
                //Terminal = passenger.//TODO: from flightstat
                WorkstationId = passenger.WORKSTATION_ID,
                LoungeId = passenger.LOUNGE_ID,
                TrackingStatus = (int)passenger.TRACKING_STATUS,
                BarcodeString = ExtractBarcode(passenger.TRACKING_SYSTEM_INFO),
                AirlineDesignatorBoardingPassIssuer = passenger.AIRLINE_DESIGATOR_BP_ISSUER,
                PassengerServices = passengerServices,
                GUID = passenger.GUID,
            };
        }

        public static TRACKING ConvertToTracking(this Models.Passenger passenger)
        {
            if (passenger == null) return null;
            //TODO: Like PassengerRepository.ProcessTracking() we have to parse back the guest list
            #region Filling the Guest List of this passenger
            //var guestList = new List<TRACKING>();
            //if (passenger.PassengerServices != null && passenger.PassengerServices.Count > 0)
            //{
            //    foreach (var service in passenger.PassengerServices)
            //    {
            //        guestList.AddRange(service.Value.Select(x => {
            //            var r = x.ConvertToTracking();
            //            r.TRACKING_RECORD_TYPE
            //            return r;
            //        }).ToList());
            //    }
            //}
            #endregion

            return new TRACKING
            {
                BOARDING_PASS_FROM = passenger.FromAirport,
                BOARDING_PASS_TO = passenger.ToAirport,
                TRACKING_PASSENGER_NAME = passenger.FullName,
                //TIMESTAMP_UTC = passenger.TimestampUtc,
                //TRACKING_TIMESTAMP = passenger.TrackingTimestamp,
                TRACKING_PASSENGER_FF_NUMBER = passenger.FFN,
                TRACKING_PASS_FLIGHT_NUMBER = string.IsNullOrWhiteSpace(passenger.FlightCarrier) ? "" : passenger.FlightCarrier.Trim() + passenger.FlightNumber.Trim(),
                TRACKING_CLASS_OF_SERVICE = passenger.TrackingClassOfService,
                TRACKING_SYSTEM_INFO = passenger.TrackingSystemInfo,
                BOARDING_PASS_COMP_CODE = passenger.BoardingPassCompCode,
                BOARDING_PASS_OP_CARRIER = passenger.BoardingPassFlightCarrier,
                BOARDING_PASS_FLIGHT_NUMBER = passenger.BoardingPassFlightNumber,
                BOARDING_PASS_PNR = passenger.PNR,
                BOARDING_PASS_STATUS = passenger.PassengerStatus,
                BOARDING_PASS_FF_AIRLINE = passenger.BoardingPassFFAirline,
                BOARDING_PASS_FF_NUMBER = passenger.BoardingPassFFNumber,
                BOARDING_PASS_PAX_DESC = passenger.BoardingPassPaxDesc,
                BOARDING_PASS_SEQ = passenger.BoardingPassSeq,
                BOARDING_PASS_E_INDICATOR = passenger.BoardingPassEIndicator,
                BOARDING_PASS_FLIGHT_DATE = passenger.BoardingPassFlightDate,
                BOARDING_PASS_NAME = passenger.BoardingPassName,
                TRACKING_RECORD_ID = passenger.TrackingRecordID,
                CARD_ID = passenger.CardID,
                TRACKING_LINKED_RECORD = passenger.GuestOf,
                TRACKING_IS_VALID = passenger.IsValid,
                AIRLINE_ID = passenger.AirlineId,
                AIRLINE_ID_OTHER = passenger.OtherAirlineId,
                TRACKING_OTHER_INFO = passenger.Notes,
                CARD_ID_OTHER = passenger.OtherCardID,
                BOARDING_PASS_SEAT_NB = passenger.SeatNumber,
                WORKSTATION_ID = passenger.WorkstationId,
                LOUNGE_ID = passenger.LoungeId,
                TRACKING_STATUS = (TrackingStatusEnum)passenger.TrackingStatus,
                AIRLINE_DESIGATOR_BP_ISSUER = passenger.AirlineDesignatorBoardingPassIssuer,
                //GUEST_LIST = new List<TRACKING>(),TODO: fill with real data from passenger services
                GUID = string.IsNullOrWhiteSpace(passenger.GUID) ? Guid.NewGuid().ToString("N").ToUpper() : passenger.GUID
            };
        }

        public static WORKSTATION ConvertToWORKSTATION(this Models.Workstation workstation)
        {
            return new WORKSTATION
            {
                AIRPORT_DETAIL = new AIRPORT
                {
                    AIRPORT_CODE = workstation.AirportCode
                },
                //AIRPORT_CODE = workstation.AirportCode,
                WORKSTATION_ID = workstation.WorkstationID,
                WORKSTATION_NAME = workstation.WorkstationName,
                LOUNGE_DETAIL = new LOUNGE
                {
                    LOUNGE_ID = workstation.LoungeID,
                    LOUNGE_NAME = workstation.LoungeName
                },
                WORKSTATION_DEFAULTAIRLINEID = workstation.DefaultAirlineId
            };
        }

        public static Models.Workstation ConvertToWorkstation(this WORKSTATION workstation)
        {
            return new Models.Workstation
            {
                //AirportCode = workstation.AIRPORT_CODE,
                AirportCode = workstation.AIRPORT_DETAIL != null ? workstation.AIRPORT_DETAIL.AIRPORT_CODE : "N/A",
                LoungeID = workstation.LOUNGE_DETAIL.LOUNGE_ID,
                LoungeName = workstation.LOUNGE_DETAIL.LOUNGE_NAME,
                WorkstationID = workstation.WORKSTATION_ID,
                WorkstationName = workstation.WORKSTATION_NAME,
                DefaultAirlineId = workstation.WORKSTATION_DEFAULTAIRLINEID
            };
        }

        public static Models.Card ConvertToCard(this CARD card)//TODO: Must be more dynamic
        {
            #region Converting supported services
            //TODO: update services with names
            var services = new List<Models.PassengerService>();
            if (card.CARD_MAX_GUEST_COMP > 0)
                services.Add(new Models.PassengerService
                {
                    Id = 1,
                    Name = "Complementary Guests",
                    MaximumQuantity = (int)card.CARD_MAX_GUEST_COMP
                });
            if (card.CARD_MAX_GUEST_FAM > 0)
                services.Add(new Models.PassengerService
                {
                    Id = 2,
                    Name = "Family Guests",
                    MaximumQuantity = (int)card.CARD_MAX_GUEST_FAM
                });

            if (card.CARD_MAX_GUEST_PAY > 0)
                services.Add(new Models.PassengerService
                {
                    Id = 3,
                    Name = "Paid Guests",
                    MaximumQuantity = (int)card.CARD_MAX_GUEST_PAY
                });
            #endregion

            return new Models.Card
            {
                ID = card.CARD_ID,
                Name = card.CARD_NAME,
                Currency = card.CARD_CURRENCY,
                AcceptedClass = card.CARD_ACCEPTED_CLASS,
                AirlineID = card.AIRLINE_ID,
                ImageHandle = card.CARD_IMAGE_HANDLE,
                AllowedServices = services,//No need to convert back
                ComplementaryGuests = (int)card.CARD_MAX_GUEST_COMP,
                FamilyGuests = (int)card.CARD_MAX_GUEST_FAM,
                PaidGuests = (int)card.CARD_MAX_GUEST_PAY,
            };
        }

        public static CARD ConvertToCARD(this Models.Card card)
        {
            return new CARD
            {
                CARD_ID = card.ID,
                CARD_NAME = card.Name,
                CARD_CURRENCY = card.Currency,
                CARD_ACCEPTED_CLASS = card.AcceptedClass,
                AIRLINE_ID = card.AirlineID,
                CARD_IMAGE_HANDLE = card.ImageHandle,
                CARD_MAX_GUEST_COMP = card.ComplementaryGuests,
                CARD_MAX_GUEST_FAM = card.FamilyGuests,
                CARD_MAX_GUEST_PAY = card.PaidGuests,
            };
        }

        public static Models.Airline ConvertToAirline(this AIRLINE airline)
        {
            return new Models.Airline
            {
                ID = airline.AIRLINE_ID,
                Code = airline.AIRLINE_CODE,
                Name = airline.AIRLINE_NAME,
                Description = airline.ACCEPTED_CLASS,
                ImageHandle = airline.AIRLINE_IMAGE_HANDLE
            };
        }

        public static AIRLINE ConvertToAIRLINE(this Airline airline)
        {
            return new AIRLINE
            {
                AIRLINE_ID = airline.ID,
                AIRLINE_CODE = airline.Code,
                AIRLINE_NAME = airline.Name,
                ACCEPTED_CLASS = airline.Description,
                AIRLINE_IMAGE_HANDLE = airline.ImageHandle
            };
        }
    }
}
