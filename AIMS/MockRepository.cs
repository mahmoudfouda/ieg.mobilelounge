﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using AIMS.DAL;
using System.Linq;

namespace AIMS
{
   // public class MockRepository
   // {
   //     private static MockRepository _current;
   //     public static MockRepository Current
   //     {
   //         get
   //         {
   //             return _current ?? (_current = new MockRepository());
   //         }
   //         private set
   //         {
   //             _current = value;
   //         }
   //     }

   //     private MockRepository()
   //     {
   //         #region Filling Lounges
   //         Lounges = new List<Workstation>{
   //             new Workstation{
   //                 WorkstationID = 1,
   //                 AirportCode = "YUL",
   //                 LoungeName = "Transborder",
   //             },
   //             new Workstation{
   //                 WorkstationID = 2,
   //                 AirportCode = "YUL",
   //                 LoungeName = "International"
   //             },
   //             new Workstation{
   //                 WorkstationID = 3,
   //                 AirportCode = "YUL",
   //                 LoungeName = "Domestic"
   //             }/*,
			//	new Lounge{
			//		ID = 4,
			//		AirportName = "Montréal-Trudeau",
			//		LoungeName = "World MasterCard – VIP Lounge"
			//	}*/
			//};
   //         #endregion

   //         #region Filling Airlines
   //         Airlines = new List<Airline>{
   //             new Airline{
   //                 ID = 1,
   //                 Code = "JP",
   //                 Name = "Adria",
   //                 LogoResourceID = 2130837512
   //             },
   //             new Airline{
   //                 ID = 2,
   //                 Code = "AC",
   //                 Name = "Air Canada",
   //                 LogoResourceID = 2130837505
   //             },
   //             new Airline{
   //                 ID = 3,
   //                 Code = "A3",
   //                 Name = "Aegean",
   //                 LogoResourceID = 2130837504
   //             },
   //             new Airline{
   //                 ID = 4,
   //                 Code = "CA",
   //                 Name = "Air China",
   //                 LogoResourceID = 2130837509
   //             },
   //             new Airline{
   //                 ID = 5,
   //                 Code = "AI",
   //                 Name = "Air India",
   //                 LogoResourceID = 2130837506
   //             },
   //             new Airline{
   //                 ID = 6,
   //                 Code = "CM",
   //                 Name = "Copa Airlines",
   //                 LogoResourceID = 2130837510
   //             },
   //             new Airline{
   //                 ID = 7,
   //                 Code = "NZ",
   //                 Name = "Air New Zealand",
   //                 LogoResourceID = 2130837518
   //             },
   //             new Airline{
   //                 ID = 8,
   //                 Code = "NH",
   //                 Name = "ANA",
   //                 LogoResourceID = 2130837517
   //             },
   //             new Airline{
   //                 ID = 9,
   //                 Code = "OZ",
   //                 Name = "Asiana Airlines",
   //                 LogoResourceID = 2130837521
   //             },
   //             new Airline{
   //                 ID = 10,
   //                 Code = "OS",
   //                 Name = "Austrian Airlines",
   //                 LogoResourceID = 2130837519
   //             },
   //             new Airline{
   //                 ID = 11,
   //                 Code = "AV",
   //                 Name = "Avianca",
   //                 LogoResourceID = 2130837507
   //             },
   //             new Airline{
   //                 ID = 12,
   //                 Code = "SN",
   //                 Name = "Brussels Airlines",
   //                 LogoResourceID = 2130837524
   //             },
   //             new Airline{
   //                 ID = 13,
   //                 Code = "OU",
   //                 Name = "Croatian Airlines",
   //                 LogoResourceID = 2130837520
   //             },
   //             new Airline{
   //                 ID = 14,
   //                 Code = "MS",
   //                 Name = "Egyptair",
   //                 LogoResourceID = 2130837516
   //             },
   //             new Airline{
   //                 ID = 15,
   //                 Code = "ET",
   //                 Name = "Ethiopian Airlines",
   //                 LogoResourceID = 2130837511
   //             },
   //             new Airline{
   //                 ID = 16,
   //                 Code = "BR",
   //                 Name = "Eva Air",
   //                 LogoResourceID = 2130837508
   //             },
   //             new Airline{
   //                 ID = 17,
   //                 Code = "LO",
   //                 Name = "LOT",
   //                 LogoResourceID = 2130837514
   //             },
   //             new Airline{
   //                 ID = 18,
   //                 Code = "LH",
   //                 Name = "Lufthansa",
   //                 LogoResourceID = 2130837513
   //             },
   //             new Airline{
   //                 ID = 19,
   //                 Code = "SK",
   //                 Name = "Scandinavian Airlines",
   //                 LogoResourceID = 2130837523
   //             },
   //             new Airline{
   //                 ID = 20,
   //                 Code = "ZH",
   //                 Name = "Shenzhen Airlines",
   //                 LogoResourceID = 2130837530
   //             },
   //             new Airline{
   //                 ID = 21,
   //                 Code = "SQ",
   //                 Name = "Singapore Airlines",
   //                 LogoResourceID = 2130837525
   //             },
   //             new Airline{
   //                 ID = 22,
   //                 Code = "SA",
   //                 Name = "South African Airways",
   //                 LogoResourceID = 2130837522
   //             },
   //             new Airline{
   //                 ID = 23,
   //                 Code = "LX",
   //                 Name = "Swiss International Air Lines",
   //                 LogoResourceID = 2130837515
   //             },
   //             new Airline{
   //                 ID = 24,
   //                 Code = "TP",
   //                 Name = "TAP Portugal",
   //                 LogoResourceID = 2130837528
   //             },
   //             new Airline{
   //                 ID = 25,
   //                 Code = "TG",
   //                 Name = "Thai International Airlines",
   //                 LogoResourceID = 2130837526
   //             },
   //             new Airline{
   //                 ID = 26,
   //                 Code = "TK",
   //                 Name = "Turkish Airlines",
   //                 LogoResourceID = 2130837527
   //             },
   //             new Airline{
   //                 ID = 27,
   //                 Code = "UA",
   //                 Name = "United Airlines",
   //                 LogoResourceID = 2130837529
   //             }
   //         };
   //         #endregion

   //         #region Filling PassengerServices
   //         Services = new List<Models.PassengerService>{
   //             new Models.PassengerService{
   //                 Id = 0,
   //                 Name = "Complementary",
   //                 Quantity=0,
   //                 UnitPrice = 0
   //             },
   //             new Models.PassengerService{
   //                 Id = 1,
   //                 Name = "Family",
   //                 Quantity = 0,
   //                 UnitPrice = 249
   //             },
   //             new Models.PassengerService{
   //                 Id = 2,
   //                 Name = "Paid",
   //                 Quantity = 0,
   //                 UnitPrice = 349
   //             },
   //             new Models.PassengerService{
   //                 Id = 3,
   //                 Name = "Other Services",
   //                 Quantity = 0,
   //                 UnitPrice = 0
   //             }
   //         };
   //         #endregion

   //         #region Filling Passengers
   //         Passengers = new List<Passenger>{
   //             new Passenger{
   //                 TrackingRecordID = 1,
   //                 FullName = "DONNA ROSENBURG",
   //                 BarcodeString = "M1ROSENBERG/DONNA     ENSCR8E LHRYULAC 0865 190Y031B0071A3B1>2182AA4190BAC 001414722000329016241132760202AC UA TE270420G        1PC*20603003P0922UAG    AC 23ESOALHRTLHRJ2                                                  0F          NNN  ",
   //                 ToAirport = "YUL - Montreal",
   //                 FromAirport = "London-2",
   //                 ArrivalDate = System.DateTime.Now.Date,
   //                 ArrivalTime = System.DateTime.Now.AddHours(6),
   //                 DepartureDate = System.DateTime.Now.Date,
   //                 DepartureTime = System.DateTime.Now.AddHours(1),
   //                 BoardingTime = System.DateTime.Now.AddMinutes(40),
   //                 FFN = "TE270420G",
   //                 FlightCarrier = "AC",
   //                 FlightNumber = "AC 865",
   //                 TrackingClassOfService = "Y"
   //             },
   //             new Passenger{
   //                 TrackingRecordID = 1,
   //                 FullName = "JAMES FARRELL",
   //                 BarcodeString = "M1FARRELL/JAMES       EMA3NYZ LGAYYZAC 7431 009Y027D0042A1B1>2181KA5009BAC              29014214332050102ZX AC 0561832494F      3PC*20600000P0922ACS    AC 23EUO          15                                                0F          NNN  ",
   //                 ToAirport = "Toronto",
   //                 FromAirport = "NEW YORK LGA",
   //                 ArrivalDate = System.DateTime.Now.Date,
   //                 ArrivalTime = System.DateTime.Now.AddHours(5),
   //                 DepartureDate = System.DateTime.Now.Date,
   //                 DepartureTime = System.DateTime.Now.AddHours(1),
   //                 BoardingTime = System.DateTime.Now.AddMinutes(40),
   //                 FFN = "0561832494F",
   //                 FlightCarrier = "AC",
   //                 FlightNumber = "AC 7431",
   //                 TrackingClassOfService = "Y"
   //             },
   //             new Passenger{
   //                 TrackingRecordID = 1,
   //                 FullName = "PETER PHILLIPS",
   //                 BarcodeString = "M1PHILLIPS/PETER      EMPHV5V LGAYYZAC 0723 009Y021F0061A1B1>2181AA5009BAC              29014676891797702AC AC 0518004007E      0PC*20600000P0922ACG    AC 23ESOALGA200640                                                  0F          NNN  ",
   //                 ToAirport = "Toronto",
   //                 FromAirport = "NEW YORK LGA",
   //                 ArrivalDate = System.DateTime.Now.Date,
   //                 ArrivalTime = System.DateTime.Now.AddHours(4),
   //                 DepartureDate = System.DateTime.Now.Date,
   //                 DepartureTime = System.DateTime.Now.AddMinutes(50),
   //                 BoardingTime = System.DateTime.Now.AddMinutes(20),
   //                 FFN = "0518004007E",
   //                 FlightCarrier = "AC",
   //                 FlightNumber = "AC 723",
   //                 TrackingClassOfService = "Y"
   //             },
   //             new Passenger{
   //                 TrackingRecordID = 1,
   //                 FullName = "ALANCARNEGIE BROWN",
   //                 BarcodeString = "M1BROWN/ALANCARNEGIE  EKZXC5Z LHRYULAC 0865 190J003A0192A1B1>2181WA4190BAC              29014542760257801AC AC 0543779193A      2PC*20600000P0921       AC 23EUO          5                                                 0F          NNN  ",
   //                 ToAirport = "YUL - Montreal",
   //                 FromAirport = "London-2",
   //                 ArrivalDate = System.DateTime.Now.Date,
   //                 ArrivalTime = System.DateTime.Now.AddHours(7),
   //                 DepartureDate = System.DateTime.Now.Date,
   //                 DepartureTime = System.DateTime.Now.AddMinutes(70),
   //                 BoardingTime = System.DateTime.Now.AddMinutes(50),
   //                 FFN = "0543779193A",
   //                 FlightCarrier = "AC",
   //                 FlightNumber = "AC 865",
   //                 TrackingClassOfService = "J"
   //             },
   //             new Passenger{
   //                 TrackingRecordID = 1,
   //                 FullName = "JULIE CARON",
   //                 BarcodeString = "M1CARON/JULIE         ENU3JUE LGAYULAC 7463 008J003F0027A3B1>2182MA5009BAC 001498434400129014676895232002ZX AC 0739562692D      3PC*20601001P0922ACG    AC 23EUO          15                                                0F          NNN  ",
   //                 ToAirport = "YUL - Montreal",
   //                 FromAirport = "NEW YORK LGA",
   //                 ArrivalDate = System.DateTime.Now.Date,
   //                 ArrivalTime = System.DateTime.Now.AddHours(1),
   //                 DepartureDate = System.DateTime.Now.Date,
   //                 DepartureTime = System.DateTime.Now.AddMinutes(20),
   //                 BoardingTime = System.DateTime.Now,
   //                 FFN = "0739562692D",
   //                 FlightCarrier = "AC",
   //                 FlightNumber = "AC 7463",
   //                 TrackingClassOfService = "J"
   //             },
   //             new Passenger{
   //                 TrackingRecordID = 1,
   //                 FullName = "NAVJIT MATHARU",
   //                 BarcodeString = "M1MATHARU/NAVJIT      EPRCC4A LGAYYZAC 0723 008J001D0035A1B1>2181WA5009BAC              29014214321378832AC AC 0739335198E      3PC*20600000P0922ACG    AC 23EUO          5                                                 0F          NNN  ",
   //                 ToAirport = "Toronto",
   //                 FromAirport = "NEW YORK LGA",
   //                 ArrivalDate = System.DateTime.Now.Date,
   //                 ArrivalTime = System.DateTime.Now.AddHours(2),
   //                 DepartureDate = System.DateTime.Now.Date,
   //                 DepartureTime = System.DateTime.Now.AddMinutes(55),
   //                 BoardingTime = System.DateTime.Now.AddMinutes(35),
   //                 FFN = "0739335198E",
   //                 FlightCarrier = "AC",
   //                 FlightNumber = "AC 723",
   //                 TrackingClassOfService = "J"
   //             },
   //         };
   //         Passengers = Passengers.OrderByDescending(x=>x.DepartureTime).ThenBy(x=>x.FullName).ToList();
   //         #endregion

   //         #region Filling Cards
   //         Cards = new List<Card>{
   //             new Card{
   //                 ID = 1,
   //                 Name = "The test Card 1",
   //                 PictureResourceID = 2130837544
   //             },
   //             new Card{
   //                 ID = 2,
   //                 Name = "The test Card 2",
   //                 PictureResourceID = 2130837545
   //             },
   //             new Card{
   //                 ID = 3,
   //                 Name = "The test Card 3",
   //                 PictureResourceID = 2130837546
   //             },
   //             new Card{
   //                 ID = 4,
   //                 Name = "The test Card 4",
   //                 PictureResourceID = 2130837547
   //             },
   //             new Card{
   //                 ID = 5,
   //                 Name = "The test Card 5",
   //                 PictureResourceID = 2130837548
   //             },
   //             new Card{
   //                 ID = 6,
   //                 Name = "The test Card 6",
   //                 PictureResourceID = 2130837549
   //             },
   //             new Card{
   //                 ID = 7,
   //                 Name = "The test Card 7",
   //                 PictureResourceID = 2130837550
   //             },
   //             new Card{
   //                 ID = 8,
   //                 Name = "The test Card 8",
   //                 PictureResourceID = 2130837551
   //             },
   //             new Card{
   //                 ID = 9,
   //                 Name = "The test Card 9",
   //                 PictureResourceID = 2130837552
   //             },
   //             new Card{
   //                 ID = 10,
   //                 Name = "The test Card 10",
   //                 PictureResourceID = 2130837553
   //             },
   //             new Card{
   //                 ID = 11,
   //                 Name = "The test Card 11",
   //                 PictureResourceID = 2130837554
   //             },
   //             new Card{
   //                 ID = 12,
   //                 Name = "The test Card 12",
   //                 PictureResourceID = 2130837555
   //             },
   //             new Card{
   //                 ID = 13,
   //                 Name = "The test Card 13",
   //                 PictureResourceID = 2130837544
   //             },
   //             new Card{
   //                 ID = 14,
   //                 Name = "The test Card 14",
   //                 PictureResourceID = 2130837545
   //             },
   //             new Card{
   //                 ID = 15,
   //                 Name = "The test Card 15",
   //                 PictureResourceID = 2130837546
   //             },
   //             new Card{
   //                 ID = 16,
   //                 Name = "The test Card 16",
   //                 PictureResourceID = 2130837547
   //             },
   //             new Card{
   //                 ID = 17,
   //                 Name = "The test Card 17",
   //                 PictureResourceID = 2130837548
   //             },
   //             new Card{
   //                 ID = 18,
   //                 Name = "The test Card 18",
   //                 PictureResourceID = 2130837549
   //             },
   //             new Card{
   //                 ID = 19,
   //                 Name = "The test Card 19",
   //                 PictureResourceID = 2130837550
   //             },
   //             new Card{
   //                 ID = 20,
   //                 Name = "The test Card 20",
   //                 PictureResourceID = 2130837551
   //             },
   //             new Card{
   //                 ID = 21,
   //                 Name = "The test Card 21",
   //                 PictureResourceID = 2130837552
   //             },
   //             new Card{
   //                 ID = 22,
   //                 Name = "The test Card 22",
   //                 PictureResourceID = 2130837553
   //             },
   //             new Card{
   //                 ID = 23,
   //                 Name = "The test Card 23",
   //                 PictureResourceID = 2130837554
   //             },
   //             new Card{
   //                 ID = 24,
   //                 Name = "The test Card 24",
   //                 PictureResourceID = 2130837555
   //             }
   //         };
   //         #endregion

   //         #region Filling Messages
   //         Messages = new List<Message>() {
   //             new Message {
   //                 Id = 1,
   //                 From = "John Smith",
   //                 Time = DateTime.Now.Subtract(new TimeSpan(0,0,30,0)),
   //                 Title = "Gate openned"
   //             },
   //             new Message {
   //                 Id = 1,
   //                 From = "Administrator",
   //                 Time = DateTime.Now.Subtract(new TimeSpan(0,1,30,0)),
   //                 Title = "Black list updated"
   //             },
   //             new Message {
   //                 Id = 1,
   //                 From = "IEG Support",
   //                 Time = DateTime.Now.Subtract(new TimeSpan(0,2,0,0)),
   //                 Title = "Backup required"
   //             },
   //             new Message {
   //                 Id = 1,
   //                 From = "John Smith",
   //                 Time = DateTime.Now.Subtract(new TimeSpan(2,0,0,0)),
   //                 Title = "Gate openned"
   //             }
   //         };
   //         Messages = Messages.OrderByDescending(x => x.Time).ToList();
   //         #endregion
   //     }

   //     public List<Workstation> Lounges
   //     {
   //         get;
   //         set;
   //     }

   //     public List<Airline> Airlines
   //     {
   //         get;
   //         set;
   //     }

   //     public List<Passenger> Passengers
   //     {
   //         get;
   //         set;
   //     }
        
   //     public List<Models.PassengerService> Services
   //     {
   //         get;
   //         set;
   //     }

   //     public List<Card> Cards
   //     {
   //         get;
   //         set;
   //     }

   //     public List<AIMS.DAL.Message> Messages
   //     {
   //         get;
   //         set;
   //     }
   // }
}