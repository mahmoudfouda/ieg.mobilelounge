﻿
namespace AIMS.Models
{
    public class Airline : IAdorner
    {
        protected DAL.Airline _airline { get; set; }

        public Airline()
        {
            this._airline = new DAL.Airline();
        }

        private void CheckImage()
        {
            if (!string.IsNullOrWhiteSpace(this._airline.ImageHandle) && (AirlineLogoBytes == null || AirlineLogoBytes.Length == 0))
            {
                var img = DAL.ImageDBAdapter.Current.GetImage(this._airline.ImageHandle);
                if (img != null && img.PictureBytes != null && img.PictureBytes.Length != 0)
                    AirlineLogoBytes = img.PictureBytes;
            }
        }
        
        //public void SaveImage(byte[] imageBytes)
        //{
        //    this.AirlineLogoBytes = imageBytes;
        //    if (imageBytes == null || imageBytes.Length == 0) return;//TODO: maybe image has to be deleted!!!
            
        //    AirlinesRepository.SaveAirlineImage(this);
        //}
        
        public decimal ID
        {
            get { return this._airline.ID; }
            set { this._airline.ID = value; }
        }

        public string Code
        {
            get { return this._airline.Code; }
            set { this._airline.Code = value; }
        }
        
        public string Name
        {
            get { return this._airline.Name; }
            set { this._airline.Name = value; }
        }
        
        public string DisplayName
        {
            get { return this._airline.DisplayName; }
        }

        public string ImageHandle {
            get {
                CheckImage();
                return this._airline.ImageHandle;
            }
            set {
                this._airline.ImageHandle = value;
                CheckImage();
            }
        }

        public byte[] AirlineLogoBytes
        {
            get;// { return this._airline.AirlineLogoBytes; }
            protected set;// { this._airline.AirlineLogoBytes = value; }
        }

        public string Description
        {
            get { return this._airline.Description; }
            set { this._airline.Description = value; }
        }

        public int LogoResourceID
        {
            get { return this._airline.LogoResourceID; }
            set { this._airline.LogoResourceID = value; }
        }

        public object DALObject
        {
            get
            {
                return _airline;
            }
        }
    }
}
