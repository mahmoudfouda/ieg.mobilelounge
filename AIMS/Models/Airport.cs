﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class Airport : IAdorner
    {
        protected DAL.Airport _airport { get; set; }

        public Airport()
        {
            this._airport = new DAL.Airport();
        }
        
        public long Id {
            get { return this._airport.Id; }
            set { this._airport.Id = value; }
        }
        
        public string IATA_Code
        {
            get { return this._airport.IATA_Code; }
            set { this._airport.IATA_Code = value; }
        }

        public string Name
        {
            get { return this._airport.Name; }
            set { this._airport.Name = value; }
        }

        public string Continent
        {
            get { return this._airport.Continent; }
            set { this._airport.Continent = value; }
        }

        public string Country
        {
            get { return this._airport.Country; }
            set { this._airport.Country = value; }
        }

        public string Municipality
        {
            get { return this._airport.Municipality; }
            set { this._airport.Municipality = value; }
        }

        public double Latitude
        {
            get { return this._airport.Latitude; }
            set { this._airport.Latitude = value; }
        }

        public double Longitude
        {
            get { return this._airport.Longitude; }
            set { this._airport.Longitude = value; }
        }

        public object DALObject
        {
            get
            {
                return _airport;
            }
        }
    }
}
