﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public enum OccupancyValueType : int
    {
        Primary = 0,
        Guest = 1,
        ReEntry = 2,
        LoungeHopping = 3,
        //TODO: Other types of PAX
    }

    public class OccupancyElapsedData
    {
        public DateTime UtcTime { get; set; }

        public DateTime LocalTime { get; set; }

        public Dictionary<int, int> Values { get; set; }
    }
}
