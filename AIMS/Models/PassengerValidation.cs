﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class PassengerValidation
    {
        public PassengerValidation() : this(0)
        {
            PassengerNames = new List<string>();
        }

        public PassengerValidation(string validationMetaData)// : this(byte.Parse(validationMetaData))
        {
            PassengerNames = new List<string>();
            byte byteMetaData = 0;
            if (!string.IsNullOrEmpty(validationMetaData))
            {
                if (byte.TryParse(validationMetaData, out byteMetaData)) InitiateValues(byteMetaData);
                else
                {
                    if (validationMetaData.ToLower().Contains("error"))
                    {
                        var returnedError = validationMetaData;
                    }
                    else
                    {
                        //PassengerNames = validationMetaData;
                        PassengerNames = validationMetaData.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    }
                }
            }
        }

        public PassengerValidation(byte validationMetaData)
        {
            InitiateValues(validationMetaData);
        }

        private void InitiateValues(byte validationMetaData)
        {
            #region the AIMS way
            
            IsNameRequired = ((int)validationMetaData & 1) == 1;
            IsFFNRequired = ((int)validationMetaData & 2) == 2;
            IsFlightInfoRequired = ((int)validationMetaData & 4) == 4;
            IsNotesRequired = ((int)validationMetaData & 8) == 8;
            IsPNRRequired = ((int)validationMetaData & 16) == 16;
            IsDestinationOriginRequired = ((int)validationMetaData & 32) == 32;
            
            #endregion
            
            //TODO: fill IsPNRRequired and IsDestinationOriginRequired
            //IsNameRequired = (validationMetaData & 1) != 0;
            //IsFFNRequired = (validationMetaData & 2) != 0;
            //IsFlightInfoRequired = (validationMetaData & 4) != 0;
            //IsNotesRequired = (validationMetaData & 8) != 0;
        }

        public bool IsNameRequired { get; private set; }

        public bool IsFFNRequired { get; private set; }

        public bool IsPNRRequired { get; private set; }

        public bool IsDestinationOriginRequired { get; private set; }

        public bool IsFlightInfoRequired { get; private set; }

        public bool IsNotesRequired { get; private set; }

        public List<string> PassengerNames { get; set; }
    }
}
