﻿using AIMS.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMS.Models
{
    public class Passenger: IAdorner
    {
        protected DAL.Passenger passenger { get; set; }

        public Passenger()
        {
            this.passenger = new DAL.Passenger();
        }
        
        public int Id {
            get { return this.passenger.Id; }
            set { this.passenger.Id = value; }
        }

        public decimal TrackingRecordID
        {
            get { return this.passenger.TrackingRecordID; }
            set { this.passenger.TrackingRecordID = value; }
        }

        public decimal CardID
        {
            get { return this.passenger.CardID; }
            set { this.passenger.CardID = value; }
        }

        public string CardName
        {
            get { return this.passenger.CardName; }
            set { this.passenger.CardName = value; }
        }

        public decimal OtherCardID
        {
            get { return this.passenger.OtherCardID; }
            set { this.passenger.OtherCardID = value; }
        }

        public string FFN
        {
            get { return this.passenger.FFN; }
            set { this.passenger.FFN = value; }
        }

        public string FlightCarrier
        {
            get { return this.passenger.FlightCarrier; }
            set { this.passenger.FlightCarrier = value; }
        }

        public string FlightNumber
        {
            get { return this.passenger.FlightNumber; }
            set { this.passenger.FlightNumber = value; }
        }

        public string Terminal
        {
            get { return this.passenger.Terminal; }
            set { this.passenger.Terminal = value; }
        }

        public string GateNumber
        {
            get { return this.passenger.GateNumber; }
            set { this.passenger.GateNumber = value; }
        }

        public string BoardingPassFlightCarrier
        {
            get { return this.passenger.BoardingPassFlightCarrier; }
            set { this.passenger.BoardingPassFlightCarrier = value; }
        }

        public string BoardingPassFlightNumber
        {
            get { return this.passenger.BoardingPassFlightNumber; }
            set { this.passenger.BoardingPassFlightNumber = value; }
        }

        public string BoardingPassCompCode
        {
            get { return this.passenger.BoardingPassCompCode; }
            set { this.passenger.BoardingPassCompCode = value; }
        }

        public string BoardingPassFFAirline
        {
            get { return this.passenger.BoardingPassFFAirline; }
            set { this.passenger.BoardingPassFFAirline = value; }
        }

        public string BoardingPassFFNumber
        {
            get { return this.passenger.BoardingPassFFNumber; }
            set { this.passenger.BoardingPassFFNumber = value; }
        }

        public string BoardingPassPaxDesc
        {
            get { return this.passenger.BoardingPassPaxDesc; }
            set { this.passenger.BoardingPassPaxDesc = value; }
        }

        public string BoardingPassSeq
        {
            get { return this.passenger.BoardingPassSeq; }
            set { this.passenger.BoardingPassSeq = value; }
        }

        public string BoardingPassEIndicator
        {
            get { return this.passenger.BoardingPassEIndicator; }
            set { this.passenger.BoardingPassEIndicator = value; }
        }

        public DateTime BoardingPassFlightDate
        {
            get { return this.passenger.BoardingPassFlightDate; }
            set { this.passenger.BoardingPassFlightDate = value; }
        }

        public string BoardingPassName
        {
            get { return this.passenger.BoardingPassName; }
            set { this.passenger.BoardingPassName = value; }
        }

        public string Title
        {
            get { return this.passenger.Title; }
            set { this.passenger.Title = value; }
        }
        
        public string FullName
        {
            get { return this.passenger.FullName; }
            set { this.passenger.FullName = value; }
        }

        public string HostName
        {
            get { return this.passenger.HostName; }
            set { this.passenger.HostName = value; }
        }

        public string FromAirport
        {
            get { return this.passenger.FromAirport; }
            set { this.passenger.FromAirport = value; }
        }

        public string ToAirport
        {
            get { return this.passenger.ToAirport; }
            set { this.passenger.ToAirport = value; }
        }
        
        public string FromCity
        {
            get { return this.passenger.FromCity; }
            set { this.passenger.FromCity = value; }
        }

        public string ToCity
        {
            get { return this.passenger.ToCity; }
            set { this.passenger.ToCity = value; }
        }

        public DateTime DepartureDate
        {
            get { return this.passenger.DepartureDate; }
            set { this.passenger.DepartureDate = value; }
        }

        public DateTime BoardingTime
        {
            get { return this.passenger.BoardingTime; }
            set { this.passenger.BoardingTime = value; }
        }

        public DateTime DepartureTime
        {
            get { return this.passenger.DepartureTime; }
            set { this.passenger.DepartureTime = value; }
        }

        public DateTime ArrivalDate
        {
            get { return this.passenger.ArrivalDate; }
            set { this.passenger.ArrivalDate = value; }
        }

        public DateTime ArrivalTime
        {
            get { return this.passenger.ArrivalTime; }
            set { this.passenger.ArrivalTime = value; }
        }

        public string PNR
        {
            get { return this.passenger.PNR; }
            set { this.passenger.PNR = value; }
        }

        public string PassengerStatus
        {
            get { return this.passenger.PassengerStatus; }
            set { this.passenger.PassengerStatus = value; }
        }

        public string TrackingClassOfService
        {
            get { return this.passenger.TrackingClassOfService; }
            set { this.passenger.TrackingClassOfService = value; }
        }

        public string TrackingPassCombinedFlightNumber
        {
            get { return this.passenger.TrackingPassCombinedFlightNumber; }
            set { this.passenger.TrackingPassCombinedFlightNumber = value; }
        }

        public string TrackingSystemInfo
        {
            get { return this.passenger.TrackingSystemInfo; }
            set { this.passenger.TrackingSystemInfo = value; }
        }

        public int TrackingStatus
        {
            get { return this.passenger.TrackingStatus; }
            set { this.passenger.TrackingStatus = value; }
        }

        public string BarcodeString
        {
            get { return this.passenger.BarcodeString; }
            set { this.passenger.BarcodeString = value; }
        }

        public bool IsValid
        {
            get { return this.passenger.IsValid; }
            set { this.passenger.IsValid = value; }
        }

        public decimal AirlineId
        {
            get { return this.passenger.AirlineId; }
            set { this.passenger.AirlineId = value; }
        }

        public decimal OtherAirlineId
        {
            get { return this.passenger.OtherAirlineId; }
            set { this.passenger.OtherAirlineId = value; }
        }

        public string Notes
        {
            get { return this.passenger.Notes; }
            set { this.passenger.Notes = value; }
        }

        public string SeatNumber
        {
            get { return this.passenger.SeatNumber; }
            set { this.passenger.SeatNumber = value; }
        }

        public decimal GuestOf
        {
            get { return this.passenger.GuestOf; }
            set { this.passenger.GuestOf = value; }
        }

        public bool IsGuest { get { return GuestOf > 0; } }

        public decimal WorkstationId
        {
            get { return this.passenger.WorkstationId; }
            set { this.passenger.WorkstationId = value; }
        }

        public decimal LoungeId
        {
            get { return this.passenger.LoungeId; }
            set { this.passenger.LoungeId = value; }
        }

        public string LoungeName
        {
            get { return this.passenger.LoungeName; }
            set { this.passenger.LoungeName = value; }
        }

        public DateTime TrackingTimestamp
        {
            get { return this.passenger.TrackingTimestamp; }
            set { this.passenger.TrackingTimestamp = value; }
        }

        public DateTime TimestampUtc
        {
            get { return this.passenger.TimestampUtc; }
            set { this.passenger.TimestampUtc = value; }
        }

        public string FailedReason
        {
            get { return this.passenger.FailedReason; }
            set { this.passenger.FailedReason = value; }
        }

        public string GUID
        {
            get { return this.passenger.GUID; }
            set { this.passenger.GUID = value; }
        }

        public string AirlineDesignatorBoardingPassIssuer
        {
            get { return this.passenger.AirlineDesignatorBoardingPassIssuer; }
            set { this.passenger.AirlineDesignatorBoardingPassIssuer = value; }
        }

        public bool HasPnrHistory{
            get { return this.passenger.HasPnrHistory; }
            set { this.passenger.HasPnrHistory = value; }
        }

        public decimal Type
        {
            get { return this.passenger.Type; }
            set { this.passenger.Type = value; }
        }

        public decimal SpecialHandling
        {
            get { return this.passenger.SpecialHandling; }
            set { this.passenger.SpecialHandling = value; }
        }

        private Dictionary<PassengerService, List<Passenger>> _passengerServices;
        public Dictionary<PassengerService, List<Passenger>> PassengerServices
        {
            get {
                if (_passengerServices == null)
                    _passengerServices = new Dictionary<PassengerService, List<Passenger>>();
                return _passengerServices;
            }
            set
            {
                if (value == null) _passengerServices = new Dictionary<PassengerService, List<Passenger>>();
                else _passengerServices = value;
            }
        }

        public string NotificationColorCode
        {
            get
            {
                if(IsReEntry)
                {
                    var c = WebApiServiceContext.Configurations.Configurations.FirstOrDefault(x => x.Name == "RentryColorCode");
                    if (c != null)
                    {
                        return c.Value;
                    }
                }
                else if(IsLoungeHopping)
                {
                    var c = WebApiServiceContext.Configurations.Configurations.FirstOrDefault(x => x.Name == "LoungeHoppingColorCode");
                    if (c != null)
                    {
                        return c.Value;
                    }
                }

                return null;
            }
        }

        public bool IsReEntry { get; set; }
        public bool IsLoungeHopping { get; set; }

        public object DALObject
        {
            get
            {
                return this.passenger;
            }
        }

    }
}
