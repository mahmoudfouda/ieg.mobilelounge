﻿using Ieg.Mobile.DataContracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class ReadOnlyPosition
    {
        public ReadOnlyPosition(Position position)
        {
            if (position != null)
            {
                Id = position.Id;
                Altitude = position.Altitude;
                FenceId = position.FenceId;
                Latitude = position.Latitude;
                Longitude = position.Longitude;
                Radius = position.MaxGpsFence;
            }
        }

        public long Id { get; private set; }
        public long? FenceId { get; private set; }
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
        public double? Altitude { get; private set; }
        public double Radius { get; private set; }

        //public static explicit operator ReadOnlyPosition(Contracts.Position position)
        //{
        //    return new ReadOnlyPosition
        //    {
        //        Id = position.Id,
        //        FenceId = position.FenceId,
        //        Accuracy = position.Accuracy,
        //        Altitude = position.Altitude,
        //        Latitude = position.Latitude,
        //        Longitude = position.Longitude,
        //        MaxGpsFence = position.MaxGpsFence,
        //    };
        //}

        //public static explicit operator Contracts.Position(ReadOnlyPosition position)
        //{
        //    return new Contracts.Position
        //    {
        //        Id = position.Id,
        //        FenceId = position.FenceId,
        //        Accuracy = position.Accuracy,
        //        Altitude = position.Altitude,
        //        Latitude = position.Latitude,
        //        Longitude = position.Longitude,
        //        MaxGpsFence = position.MaxGpsFence,
        //    };
        //}
    }
}
