﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class Message
    {
        private DAL.Message message { get; set; }

        public Message()
        {
            this.message = new DAL.Message();
        }

        public int Id
        {
            get { return this.message.Id; }
            set { this.message.Id = value; }
        }

        public decimal BulletinId
        {
            get { return this.message.BulletinId; }
            set { this.message.BulletinId = value; }
        }

        public DateTime Time
        {
            get { return this.message.Time; }
            set { this.message.Time = value; }
        }

        public string Title
        {
            get { return this.message.Title; }
            set { this.message.Title = value; }
        }

        public string From
        {
            get { return this.message.From; }
            set { this.message.From = value; }
        }

        public decimal Type
        {
            get { return this.message.Type; }
            set { this.message.Type = value; }
        }

        public string Body
        {
            get { return this.message.Body; }
            set { this.message.Body = value; }
        }

        public string Handle
        {
            get { return this.message.Handle; }
            set { this.message.Handle = value; }
        }

        public string Attachment
        {
            get { return this.message.Attachment; }
            set { this.message.Attachment = value; }
        }
    }
}
