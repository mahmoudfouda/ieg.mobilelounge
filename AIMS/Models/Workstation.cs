﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class Workstation
    {
        private DAL.Workstation workstation;

        public Workstation()
        {
            this.workstation = new DAL.Workstation();
            this.LastStatisticsUpdate = DateTime.MinValue;
        }

        public DateTime LastStatisticsUpdate { get; set; }

        public int LastPercentage { get; set; }

        public LoungeOccupancy LastLoungeOccupancy { get; set; }

        public decimal LoungeID
        {
            get { return this.workstation.LoungeID; }
            set { this.workstation.LoungeID = value; }
        }

        public decimal DefaultAirlineId
        {
            get { return this.workstation.DefaultAirlineId; }
            set { this.workstation.DefaultAirlineId = value; }
        }
        
        public decimal WorkstationID
        {
            get { return this.workstation.WorkstationID; }
            set { this.workstation.WorkstationID = value; }
        }
        
        public int PAXAvgStayMinutes
        {
            get { return this.workstation.PAXAvgStayMinutes; }
            set { this.workstation.PAXAvgStayMinutes = value; }
        }

        public int Capacity
        {
            get { return this.workstation.Capacity; }
            set { this.workstation.Capacity = value; }
        }

        public string AirportCode
        {
            get { return this.workstation.AirportCode; }
            set { this.workstation.AirportCode = value; }
        }
        
        public string LoungeName
        {
            get { return this.workstation.LoungeName; }
            set { this.workstation.LoungeName = value; }
        }
        
        public string WorkstationName
        {
            get { return this.workstation.WorkstationName; }
            set { this.workstation.WorkstationName = value; }
        }

        public string Description
        {
            get { return this.workstation.Description; }
            set { this.workstation.Description = value; }
        }
    }
}
