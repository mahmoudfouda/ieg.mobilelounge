﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class LoungeOccupancy// : IAdorner
    {
        //public object DALObject => throw new NotImplementedException();//Not necessary for now
        public DateTimeOffset Time { get; set; }

        public decimal Percentage { get; set; }

        public decimal OccupancyTotal { get; set; }

        public decimal OccupancyTypePassenger { get; set; }

        public decimal? OccupancyCapacity { get; set; }

        public decimal LoungeId { get; set; }
    }
}
