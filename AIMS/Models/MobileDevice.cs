﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class MobileDevice
    {
        public DeviceType Type { get; set; }
        public string DeviceUniqueId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceVersion { get; set; }
        public string OwnerSpecificName { get; set; }
        public string Description { get; set; }

        private AIMS.Contracts.DataContracts.MobileDevice Device { get; set; }

        public static implicit operator AIMS.Contracts.DataContracts.MobileDevice(AIMS.Models.MobileDevice device)
        {
            if (device == null)
                return null;
            if(device.Device != null)
                return device.Device;
            return 
                new Contracts.DataContracts.MobileDevice {
                    Description = device.Description,
                    DeviceModel = device.DeviceModel,
                    DeviceName = device.DeviceName,
                    DeviceUniqueId = device.DeviceUniqueId,
                    DeviceVersion = device.DeviceVersion,
                    OwnerSpecificName = device.OwnerSpecificName
                };
        }

        public static implicit operator AIMS.Models.MobileDevice(AIMS.Contracts.DataContracts.MobileDevice device)
        {
            if (device == null)
                return null;

            return new Models.MobileDevice
            {
                Device = device,
                DeviceModel = device.DeviceModel,
                Description = device.Description,
                DeviceName = device.DeviceName,
                DeviceUniqueId = device.DeviceUniqueId,
                OwnerSpecificName = device.OwnerSpecificName,
                DeviceVersion = device.DeviceVersion
            };
        }
    }
}
