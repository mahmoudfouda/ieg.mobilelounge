﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class Log
    {
        protected DAL.Log _log { get; set; }

        public Log()
        {
            var position = DefaultMobileApplication.Current.Position == null ? new Ieg.Mobile.DataContracts.Models.Position() : DefaultMobileApplication.Current.Position;
            this._log = new DAL.Log()
            {
                Username = (MembershipProvider.Current != null && MembershipProvider.Current.LoggedinUser != null) ? MembershipProvider.Current.LoggedinUser.Username : "Unknown",
                MobileKey = (MembershipProvider.Current != null && MembershipProvider.Current.LoggedinUser != null) ? MembershipProvider.Current.LoggedinUser.MobileKey : "",
                DeviceId = DefaultMobileApplication.Current.CurrentDevice.DeviceUniqueId,
                Latitude = position.Latitude,
                Longitude = position.Longitude,
                Altitude = position.Altitude.HasValue ? position.Altitude.Value : 0,
                Accuracy = position.Accuracy.HasValue ? position.Accuracy.Value : 0
            };
        }
        public int Id
        {
            get { return this._log.Id; }
            set { this._log.Id = value; }
        }

        public DateTime Time
        {
            get { return this._log.Time; }
            set { this._log.Time = value; }
        }

        public DateTime LocalTime
        {
            get { return this._log.LocalTime; }
            set { this._log.LocalTime = value; }
        }

        public string Username
        {
            get { return this._log.Username; }
            set { this._log.Username = value; }
        }

        public string MobileKey
        {
            get { return this._log.MobileKey; }
            set { this._log.MobileKey = value; }
        }

        public string Title
        {
            get { return this._log.Title; }
            set { this._log.Title = value; }
        }

        public LogType LogType
        {
            get { return (LogType)this._log.LogType; }
            set { this._log.LogType = (int)value; }
        }

        public LogLevel LogLevel
        {
            get { return (LogLevel)this._log.LogLevel; }
            set { this._log.LogLevel = (int)value; }
        }

        public string DeviceId
        {
            get { return this._log.DeviceId; }
            set { this._log.DeviceId = value; }
        }

        public string Section
        {
            get { return this._log.Section; }
            set { this._log.Section = value; }
        }

        public string Description
        {
            get { return this._log.Description; }
            set { this._log.Description = value; }
        }

        public override string ToString()
        {
            return this._log.ToString();
        }
    }
}
