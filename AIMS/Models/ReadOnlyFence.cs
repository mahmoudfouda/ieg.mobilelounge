﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AIMS.Models
{
    public class ReadOnlyFence
    {
        public ReadOnlyFence(Contracts.DataContracts.Fence fence = null)
        {
            if (fence != null)
            {
                Id = fence.Id;
                Name = fence.Name;
                Description = fence.Description;

                try
                {
                    if (fence.Positions != null)
                        Positions = new System.Collections.ObjectModel.ReadOnlyCollection<ReadOnlyPosition>(fence.Positions.Select(x => new ReadOnlyPosition(x.FromContractsPosition())).ToList());
                }
                catch (Exception ex)
                {
                    var e = new Exception($"Exception in casting Positions in the fence ({Id}:{Name})", ex);
                    throw e;
                }
            }
        }

        public long Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }

        public System.Collections.ObjectModel.ReadOnlyCollection<ReadOnlyPosition> Positions { get; private set; }
    }
}