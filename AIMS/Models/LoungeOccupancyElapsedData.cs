﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class LoungeOccupancyElapsedData
    {
        public decimal ClientId { get; set; }
        public decimal LoungeId { get; set; }
        public string LoungeName { get; set; }
        public string WorkstationName { get; set; }
        public int Capacity { get; set; }
        public List<OccupancyElapsedData> OccupancyRecords { get; set; }
    }
}
