﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class User
    {
        private DAL.User user;

        public User()
        {
            this.user = new DAL.User();
        }

        public decimal AirlineId
        {
            get { return this.user.AirlineId; }
            set { this.user.AirlineId = value; }
        }

        public string MobileKey
        {
            get { return this.user.MobileKey; }
            set { this.user.MobileKey = value; }
        }

        public string AirportName
        {
            get { return this.user.AirportName; }
            set { this.user.AirportName = value; }
        }

        public string Username
        {
            get { return this.user.Username; }
            set { this.user.Username = value; }
        }

        public string Password
        {
            get { return this.user.Password; }
            set { this.user.Password = value; }
        }

        public string FirstName
        {
            get { return this.user.FirstName; }
            set { this.user.FirstName = value; }
        }

        public string LastName
        {
            get { return this.user.LastName; }
            set { this.user.LastName = value; }
        }

        public byte[] ProfileImage
        {
            get { return this.user.ProfileImage; }
            set { this.user.ProfileImage = value; }
        }

        public DateTime LastLoginDate
        {
            get { return this.user.LastLoginDate; }
            set { this.user.LastLoginDate = value; }
        }

        public override string ToString()
        {
            return this.user.ToString();
        }

    }
}
