﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class PassengerService
    {
        public PassengerService()
        {
        }

        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string DisplayText
        {
            get
            {
                return string.Format("{0} {1}", Quantity, Name);
            }
        }

        public string DisplayDescription
        {
            get
            {
                return string.Format("{0}$ ({1})", TotalPrice, (UnitPrice == 0 ? "Free service" : string.Format("{0}$/u", UnitPrice)));
            }
        }

        public int Quantity
        {
            get;
            set;
        }

        public int MaximumQuantity
        {
            get;
            set;
        }

        public float UnitPrice
        {
            get;
            set;
        }

        public float TotalPrice
        {
            get
            {
                return (int)(UnitPrice * Quantity);
            }
        }

        public string Description
        {
            get;
            set;
        }

    }
}
