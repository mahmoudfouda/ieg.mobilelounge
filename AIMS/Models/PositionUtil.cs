﻿using Ieg.Mobile.DataContracts.Models;

namespace AIMS.Models
{
    public static class PositionUtil
    {
        public static Position FromContractsPosition(this Contracts.Position position)
        {
            return new Position
            {
                Id = position.Id,
                FenceId = position.FenceId,
                Accuracy = position.Accuracy,
                Altitude = position.Altitude,
                Latitude = position.Latitude,
                Longitude = position.Longitude,
                MaxGpsFence = position.MaxGpsFence,
            };
        }

        public static Contracts.Position ToContractPosition(this Position position)
        {
            return new Contracts.Position
            {
                Id = position.Id,
                FenceId = position.FenceId,
                Accuracy = position.Accuracy,
                Altitude = position.Altitude,
                Latitude = position.Latitude,
                Longitude = position.Longitude,
                MaxGpsFence = position.MaxGpsFence,
            };
        }
    }
}
