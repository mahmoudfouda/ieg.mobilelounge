﻿using System;

namespace AIMS.Models
{
    public class BarcodeScannerSerialNumber
    {
        public const string SERIALIZER_IDENTIFIER = "BarcodeScannerSN";

        public Guid Id { get; set; }

        public PlatformType OS { get; set; }

        public int CodeMaskType { get; set; }

        public string UserName { get; set; }

        public string SerialNumber { get; set; }
    }
}
