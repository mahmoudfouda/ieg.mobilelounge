﻿using System.Collections.Generic;

namespace AIMS.Models
{
    public class Card : IAdorner
    {
        protected DAL.Card _card { get; set; }

        public Card()
        {
            _card = new DAL.Card();
        }

        private void CheckImage()
        {
            if (!string.IsNullOrWhiteSpace(this._card.ImageHandle) && (CardPictureBytes == null || CardPictureBytes.Length == 0))
            {
                var img = DAL.ImageDBAdapter.Current.GetImage(this._card.ImageHandle);
                if (img != null && img.PictureBytes != null && img.PictureBytes.Length != 0)
                    CardPictureBytes = img.PictureBytes;
            }
        }

        //public void SaveImage(byte[] imageBytes)
        //{
        //    this._card.CardPictureBytes = imageBytes;
        //    if (imageBytes == null || imageBytes.Length == 0) return;//TODO: maybe image has to be deleted!!!

        //    CardsRepository.SaveCardImage(this);
        //}

        public decimal ID
        {
            get
            {
                return this._card.ID;
            }
            set
            {
                this._card.ID = value;
            }
        }

        public string Name
        {
            get
            {
                return this._card.Name;
            }
            set
            {
                this._card.Name = value;
            }
        }

        public string DisplayName
        {
            get {
                return this._card.DisplayName;
            }
        }

        public string Currency
        {
            get { return this._card.Currency; }
            set { this._card.Currency = value; }
        }

        public string AcceptedClass
        {
            get { return this._card.AcceptedClass; }
            set { this._card.AcceptedClass = value; }
        }
        
        public decimal AirlineID
        {
            get { return this._card.AirlineID; }
            set { this._card.AirlineID = value; }
        }

        public byte[] CardPictureBytes
        {
            get;// { return this._card.CardPictureBytes; }
            protected set;// { this._card.CardPictureBytes = value; }
        }

        public string ImageHandle {
            get {
                CheckImage();
                return this._card.ImageHandle;
            }
            set {
                this._card.ImageHandle = value;
                CheckImage();
            }
        }

        public string Description
        {
            get { return this._card.Description; }
            set { this._card.Description = value; }
        }

        public int PictureResourceID
        {
            get { return this._card.PictureResourceID; }
            set { this._card.PictureResourceID = value; }
        }

        private List<PassengerService> _allowedServices;
        public List<PassengerService> AllowedServices
        {
            get {
                if (_allowedServices == null)
                    _allowedServices = new List<PassengerService>();
                return _allowedServices;
            }
            set {
                if (value == null) _allowedServices = new List<PassengerService>();
                else
                {
                    _allowedServices = value;
                }
            }
        }

        public int ComplementaryGuests
        {
            get { return this._card.ComplimentaryGuests; }
            set { this._card.ComplimentaryGuests = value; }
        }

        public int FamilyGuests
        {
            get { return this._card.FamilyGuests; }
            set { this._card.FamilyGuests = value; }
        }

        public int PaidGuests
        {
            get { return this._card.PaidGuests; }
            set { this._card.PaidGuests = value; }
        }

        public object DALObject
        {
            get
            {
                return _card;
            }
        }
    }
}
