﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Models
{
    public class OperationResult<T>
    {
        public OperationResult()
        {
            GeneratedDate = DateTime.Now;
        }
        
        public DateTime GeneratedDate { get; set; }
        
        public bool IsSucceeded { get; set; }
        
        public string Message { get; set; }
        
        public T ReturnParam { get; set; }
        
        public int? ErrorCode { get; set; }
    }
}
