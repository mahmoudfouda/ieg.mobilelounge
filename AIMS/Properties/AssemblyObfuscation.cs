﻿//using System.Reflection;

////Obfuscation settings defining encryption and other obfuscation mechanisms.
//[assembly: Obfuscation(Feature = "code control flow obfuscation", Exclude = false)]
//[assembly: Obfuscation(Feature = "type renaming pattern 'b'.*", Exclude = false)]
//[assembly: Obfuscation(Feature = "string encryption", Exclude = false)]
//[assembly: Obfuscation(Feature = "debug [secure]", Exclude = false)]
//[assembly: Obfuscation(Feature = "encrypt resources", Exclude = false)]
//[assembly: Obfuscation(Feature = "encrypt symbol names with password InformationEngineeringGroup", Exclude = false)]