﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS
{
    public delegate void ChangeNeededHandler(bool isNeeded);
    public interface INotifyChangeNeeded
    {
        //event ChangeNeededHandler OnChangeNeeded;

        void NotifyChangeNeeded(bool isNeeded);

        void CheckIfSynchronizationNeeded();
    }
}
