﻿using System;
using System.Collections.Generic;

namespace AIMS
{
    public delegate void ReporitoryChangedHandler(object repository);
    public delegate void ReporitoryChangedHandler<T>(T repository);

    public interface INotifyRepositoryChanged
    {
        //event ReporitoryChangedHandler OnRepositoryChanged;

        void NotifyRepositoryChanged(object objects);
    }
}

