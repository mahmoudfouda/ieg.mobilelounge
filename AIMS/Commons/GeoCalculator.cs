﻿using System;

namespace AIMS.Commons
{
    public static class GeoCalculator
    {
        public const double EarthRadiusInMiles = 3956.0;
        public const double EarthRadiusInKilometers = 6367.0;

        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        public static double CalculateDistance(double lat1, double lng1, double lat2, double lng2)//default is Miles
        {
            return CalculateDistance(lat1, lng1, lat2, lng2, GeoCalculatorMeasurementTypes.Miles);
        }

        public static double CalculateDistance(double lat1, double lng1, double lat2, double lng2, GeoCalculatorMeasurementTypes measurementType)
        {
            double radius = GeoCalculator.EarthRadiusInMiles;

            if (measurementType == GeoCalculatorMeasurementTypes.Kilometers) { radius = GeoCalculator.EarthRadiusInKilometers; }
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
    }

    public enum GeoCalculatorMeasurementTypes : int
    {
        Miles = 0,
        Kilometers = 1
    }
}
