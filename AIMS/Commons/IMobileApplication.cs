﻿using System;
using Ieg.Mobile.DataContracts.MobileLounge.ServiceModel;
using Ieg.Mobile.Localization;

namespace AIMS
{
    public interface IMobileApplication
    {
        string StartupFolder { get; }
        bool ContinueScanning { get; set; }
        CameraType? ActiveCamera { get; set; }
        Models.MobileDevice CurrentDevice { get; }
        bool IsDeveloperModeEnabled { get; set; }
        LanguageRepository TextProvider { get; }
        
        event EventHandler ApplicationStart;
        event EventHandler ApplicationInit;
        event EventHandler ApplicationStop;

        /// <summary>
        /// Must fill up the Device SerialNumber and StartupFolder in order to work with AIMS core project
        /// </summary>
        /// <param name="gpsAdapter"></param>
        /// <param name="deviceSerialNumber"></param>
        /// <param name="applicationStartupFolder"></param>
        /// <param name="deviceType"></param>
        void TriggerApplicationInit(string applicationStartupFolder, IGpsAdapter gpsAdapter, Models.MobileDevice device);
        void TriggerApplicationStart();
    }
}

