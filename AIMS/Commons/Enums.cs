﻿namespace AIMS
{
    public enum LogType : int
    {
        Info = 1,
        Warning = 2,
        Error = 3
    }

    public enum LogLevel : int
    {
        Developer = 0,
        Client = 1
    }

    public enum ErrorCode : int
    {
        #region Tracking Errors         (0 ~ 100)
        NoError = 0,
        DocumentWithoutPrivilege = 1,
        ClassNotAccepted = 2,
        CardExpired = 3,
        ConflictBetweenTwoCardConfigurations = 4,
        MembershipDeclined = 5,
        BoardingPassExpired = 6,
        ArrivalMismatch = 7,
        DepartureMismatch = 8,
        AirlineMismatch = 9,
        CardNotvalidated = 10,
        PurchaseCanceled = 11,
        WrongReservationDate = 12,
        InvoiceNotFound = 13,
        InvoiceRefunded = 14,
        NeedToSwipeBoardingPass = 15,
        PassengerCannotBeUpdated = 16,
        CustomerNotFound = 17,
        AncillaryNetworkIssue = 18,
        AncillaryEntryRequired = 19,
        FutureReservation = 20,
        ExpiredReservation = 21,
        SaberInvokeFailed = 22,
        SaberFlightNotFound = 23,
        SaberResultNotValid = 24,
        UnknownFailure = 25,
        OriginMismatch = 26,
        ExternalValidationFailed = 27,
        #endregion

        #region Tracking Errors New     (101 ~ 200)
        GroupBelonging = 110, //“Must fly with agreed partners”
        ClassOfTravel = 111, //“Class of travel not accepted”
        FlightNumber = 112, //“Flight number not accepted”
        DestinationOrigin = 113, //“Destination/Origin mismatch”
        Time = 114, //“Time of the day not accepted”
        MarketingCarrier = 115, //“Marketing carrier not accepted”
        DaysAccepted = 116, //« Document expired »
        PassengerNameMismatch = 117, //name on the card is different than the one on BP
        GuestLimitationsReached = 118,
        UnauthorizedAccess = 401,// When external token is invalid
        #endregion

        #region User Validation Errors  (1001 ~ 1050)
        IncompleteUserInfo = 1001,
        UserValidationError = 1002,
        UserRetrievalError = 1003,
        WorkstationsError = 1004,
        NoLoungesForUser = 1005,
        UserValidationException = 1006,
        UserRetrievalException = 1007,
        WorkstationRetrievalException = 1008,
        LocationRetrievalException = 1009,
        LocationValidationException = 1010,
        InternetConnectionError = 1011,
        SessionExpiredError = 1012,
        ServicePlanExpiredError = 1013,
        ServicePlanIsFullError = 1014,
        BlackListed = 1015,
        #endregion

        #region Generic Errors          (1051 ~ 1100)
        UnknownException = 1051,
        CommunicationError = 1052,
        TimeoutException = 1053,
        AuthorizationError = 1054,
        TaskAlreadyPerformedException = 1055,
        #endregion

        #region Data Retrieval Error        (1101 ~ 1150)
        AirlinesRetrievalError = 1101,
        CardsRetrievalError = 1102,
        PassengersRetrievalError = 1103,
        #endregion
    }

    public enum DeviceType
    {
        Unspecified = 0,
        Phone = 1,
        Tablet = 2,
        SmartTV = 3
    }

    public enum PlatformType
    {
        Unknown = 0,
        UWP = 1,
        Android = 2,
        iOS = 3
    }

    public enum CameraType
    {
        BackCamera = 0,
        FrontCamera = 1,
        ExternalCamera = 2
    }

    public enum SyncScope
    {
        /// <summary>
        /// Meaning all the database items
        /// </summary>
        All = 0,
        /// <summary>
        /// Meaning access document items including (Airlines + Cards + Images)
        /// </summary>
        AccessDocuments = 1,
        /// <summary>
        /// The Airlines list only
        /// </summary>
        Airlines = 2,
        /// <summary>
        /// The Cards only
        /// </summary>
        Cards = 3,
        /// <summary>
        /// All the Airline and Card Images
        /// </summary>
        Images = 4,
        /// <summary>
        /// The list of Airports only
        /// </summary>
        Airports = 5,
        /// <summary>
        /// All the language 
        /// </summary>
        LanguageTexts = 6
    }

    public enum PassengerServiceType : int// TODO: Keep it updated
    {
        None = 0,
        GuestComplimentary = 1,
        GuestFamily = 2,
        GuestPaid = 3,

        AncillaryShower = 100,
        AncillaryDrink = 101,
        AncillaryShoeShining = 102,
    }
}
