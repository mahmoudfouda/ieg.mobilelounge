﻿using AIMS.Contracts;
using System.Threading.Tasks;
using AIMS.Contracts.Validators;

namespace AIMS.Validators
{
    internal class DefaultValidator : IMobileValidator
    {
        public TokenResponse ExternalToken { get; private set; } = null;

        public bool IsTokenExpired
        {
            get
            {
                return false;
            }
        }

        public Task<ResultPack<TokenResponse>> Authenticate(string username, string password, string mobileKey, IWebApiServiceContext proxy, Position position)
        {
            return new Task<ResultPack<TokenResponse>>(() =>
            {
                return new ResultPack<TokenResponse>
                {
                    IsSucceeded = true,
                    ReturnParam = null
                };
            });
        }

        public void Authenticate(string username, string password, string mobileKey, IWebApiServiceContext proxy, Position position, TokenCallback callback)
        {
            var tokenRes = new ResultPack<TokenResponse>
            {
                IsSucceeded = true,
                ReturnParam = null
            };
            if (callback != null)
                callback.Invoke(tokenRes);
        }

        public Task<ResultPack<TokenResponse>> RefreshToken(string username, string mobileKey, Position position)
        {
            return new Task<ResultPack<TokenResponse>>(() =>
            {
                return new ResultPack<TokenResponse>
                {
                    IsSucceeded = true,
                    ReturnParam = null
                };
            });
        }

        public void RefreshToken(string username, string mobileKey, Position position, TokenCallback callback)
        {
            var tokenRes = new ResultPack<TokenResponse>
            {
                IsSucceeded = true,
                ReturnParam = null
            };
            if (callback != null)
                callback.Invoke(tokenRes);
        }
    }
}