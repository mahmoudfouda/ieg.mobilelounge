﻿using AIMS.Contracts;
using AIMS.Contracts.DataContracts;
using AIMS.Contracts.Validators;
using System;
using System.Threading.Tasks;

namespace AIMS.Validators
{
    internal class DualTokenValidator : IMobileValidator
    {
        private DateTime _tokenExpirationTime;
        private IWebApiServiceContext _proxy;

        public TokenResponse ExternalToken
        {
            get; private set;
        }

        public bool IsTokenExpired
        {
            get
            {
                if (_tokenExpirationTime == default(DateTime)) return false;
                if (_tokenExpirationTime.CompareTo(DateTime.Now) <= 0) return true;
                return false;
            }
        }

        private void setRefreshTokenTime(string expiresIn)
        {
            try {
                if(expiresIn.Contains(".")) {
                    expiresIn = expiresIn.Split ('.')[0];
                }
            }
            catch {
                
            }


            int expIn = 0;
            float realExpIn = 0;
            float.TryParse(expiresIn, out realExpIn);
            expIn = (int)Math.Floor(realExpIn);

            if (expIn > 60)
                _tokenExpirationTime = DateTime.Now.AddSeconds(expIn - 10);
            else
                _tokenExpirationTime = DateTime.Now.AddSeconds(expIn - Math.Ceiling((float)expIn / 5));
        }

        public void Authenticate(string username, string password, string mobileKey, IWebApiServiceContext proxy, Contracts.Position position, TokenCallback callback)
        {
            //var props = new System.Collections.Generic.Dictionary<string, object>();
            //props.Add("mobilekey", mobileKey);
            //if (proxy == null) return;
            //_proxy = proxy;

            ////proxy.GetJson<ResultPack<TokenResponse>>(string.Format("externalauthentication/{0}/{1}/{2}", username, password, mobileKey), (Contracts.Position)DefaultMobileApplication.Position, (extAuthResponse) =>
            //_proxy.SendJson<ResultPack<TokenResponse>>("externaltoken",
            //    new UserValidationInput { Username = username, Password = password, Properties = props },
            //    position,//(Contracts.Position)DefaultMobileApplication.Position,
            //    (extAuthResponse) =>
            //    {
            //        var res = (ResultPack<TokenResponse>)extAuthResponse;
            //        if (res.IsSucceeded)
            //        {
            //            ExternalToken = res.ReturnParam;
            //            if (ExternalToken != null)
            //                setRefreshTokenTime(ExternalToken.ExpiresIn);
            //            else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
            //        }
            //        else ExternalToken = null;

            //        if (callback != null)
            //            callback.Invoke(res);
            //    },
            //    (extAuthResponse) =>
            //    {
            //        if (callback != null)
            //            callback.Invoke(new ResultPack<TokenResponse>
            //            {
            //                IsSucceeded = false,
            //                Message = extAuthResponse != null && extAuthResponse.ReturnParam != null ? extAuthResponse.ReturnParam.Message : "Unknown error in DualTokenValidator.Authenticate()"
            //            });
            //    });

            var otherTokens = WebApiServiceContext.Configurations?.OtherTokens;
            var res = new ResultPack<TokenResponse> {
                IsSucceeded = false
            };
            if (otherTokens != null && otherTokens.Count > 0)
            {
                ExternalToken = otherTokens[0].Token;
                res.IsSucceeded = true;
                res.ReturnParam = ExternalToken;
                if (ExternalToken != null)
                    setRefreshTokenTime(ExternalToken.ExpiresIn);
                else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
            }
            else
            {
                ExternalToken = null;
                res.Message = "External token could not be retrieved.";
            }

            if (callback != null)
                callback.Invoke(res);
        }

        public void RefreshToken(string username, string mobileKey, Contracts.Position position, TokenCallback callback)
        {
            var props = new System.Collections.Generic.Dictionary<string, object>();
            props.Add("mobilekey", mobileKey);
            //proxy.GetJson<ResultPack<TokenResponse>>(string.Format("refreshexternaltoken/{0}/{1}", username, mobileKey), (Contracts.Position)DefaultMobileApplication.Current.Position, (extAuthResponse) =>
            _proxy.SendJson<ResultPack<TokenResponse>>("externaltokenrefresh",
                new UserValidationInput { Username = username, Properties = props },
                position,//(Contracts.Position)DefaultMobileApplication.Current.Position,
                (extAuthResponse, log) =>
                {
                    LogsRepository.TrackEvents(log);

                    var res = (ResultPack<TokenResponse>)extAuthResponse;
                    if (res.IsSucceeded)
                    {
                        ExternalToken = res.ReturnParam;
                        if (ExternalToken != null)
                            setRefreshTokenTime(ExternalToken.ExpiresIn);
                        else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
                    }
                    else ExternalToken = null;//Maybe we can try again

                    res.IsSucceeded = !IsTokenExpired;
                    if (callback != null)
                        callback.Invoke(res);
                },
                (extAuthResponse, log) =>
                {
                    LogsRepository.TrackEvents (log);

                    if (callback != null)
                        callback.Invoke(new ResultPack<TokenResponse>
                        {
                            IsSucceeded = false,
                            Message = extAuthResponse.ReturnParam.Message
                        });
                });
        }

        public Task<ResultPack<TokenResponse>> Authenticate(string username, string password, string mobileKey, IWebApiServiceContext proxy, Contracts.Position position)
        {
            //var props = new System.Collections.Generic.Dictionary<string, object>();
            //props.Add("mobilekey", mobileKey);
            //if (proxy == null)
            //    return new ResultPack<TokenResponse>
            //    {
            //        IsSucceeded = false,
            //        Message = "The proxy is null"
            //    };
            //_proxy = proxy;

            //try
            //{
            //    var res = await _proxy.SendJsonAsync<ResultPack<TokenResponse>>("externaltoken",
            //        new UserValidationInput { Username = username, Password = password, Properties = props },
            //        position//(Contracts.Position)DefaultMobileApplication.Position
            //        ).ConfigureAwait(false);

            //    if (res.IsSucceeded)
            //    {
            //        ExternalToken = res.ReturnParam;
            //        if (ExternalToken != null)
            //            setRefreshTokenTime(ExternalToken.ExpiresIn);
            //        else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
            //    }
            //    else ExternalToken = null;

            //    return res;
            //}
            //catch (Exception ex)
            //{
            //    ExternalToken = null;
            //    LogsRepository.AddError("Exception in DualTokenValidator.Authenticate()", ex);
            //    return new ResultPack<TokenResponse>
            //    {
            //        IsSucceeded = false,
            //        Message = "Exception in retrieving external token"
            //    };
            //}
            return new Task<ResultPack<TokenResponse>>(()=> {
                var otherTokens = WebApiServiceContext.Configurations?.OtherTokens;
                var res = new ResultPack<TokenResponse>
                {
                    IsSucceeded = false
                };
                if (otherTokens != null && otherTokens.Count > 0)
                {
                    ExternalToken = otherTokens[0].Token;
                    res.IsSucceeded = true;
                    res.ReturnParam = ExternalToken;
                    if (ExternalToken != null)
                        setRefreshTokenTime(ExternalToken.ExpiresIn);
                    else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
                }
                else
                {
                    ExternalToken = null;
                    res.Message = "External token could not be retrieved.";
                }

                return res;
            });
        }

        public async Task<ResultPack<TokenResponse>> RefreshToken(string username, string mobileKey, Contracts.Position position)
        {
            var props = new System.Collections.Generic.Dictionary<string, object>();
            props.Add("mobilekey", mobileKey);

            try
            {
                var res = await _proxy.SendJsonAsync<ResultPack<TokenResponse>>("externaltokenrefresh",
                    new UserValidationInput { Username = username, Properties = props },
                    position//(Contracts.Position)DefaultMobileApplication.Current.Position
                    ).ConfigureAwait(false);

                if (res.IsSucceeded)
                {
                    ExternalToken = res.ReturnParam;
                    if (ExternalToken != null)
                        setRefreshTokenTime(ExternalToken.ExpiresIn);
                    else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
                }
                else ExternalToken = null;//Maybe we can try again

                res.IsSucceeded = !IsTokenExpired;
                return res;
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Exception in DualTokenValidator.RefreshToken()", ex);
                return new ResultPack<TokenResponse>
                {
                    IsSucceeded = false,
                    Message = "Exception in refreshing external token"
                };
            }
        }
    }
}