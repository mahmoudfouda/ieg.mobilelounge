﻿using AIMS.Contracts.Validators;
using System;
using System.Linq;

namespace AIMS.Validators
{
    internal class SecondValidatorFactory
    {
        //private const string DualTokenMobileKeysString = "UA";//TODO: get it from claims
        public static IMobileValidator CreateValidator(string key, string dualTokenMobileKeysString)
        {
            if (string.IsNullOrWhiteSpace(key))
                key = "";
            else key = key.Trim().ToUpper();//Allways uppercase client names (MOBILE_KEY)

            if (string.IsNullOrEmpty(dualTokenMobileKeysString))
                return new DefaultValidator();

            var dualTokenMobileKeys = dualTokenMobileKeysString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.ToUpper()).ToList();

            if (dualTokenMobileKeys.Any(x => key.StartsWith(x)))
                return new DualTokenValidator();

            return new DefaultValidator();
        }
    }
}
