﻿using System;
using System.Collections.Generic;
using System.Linq;
using AIMS.DAL;
//using Ieg.Aims.Business.Contracts.Data_Contracts;
//using AIMS.ServiceProxy;
using AIMS.Contracts.AIMSClient;

namespace AIMS
{
    public class LoungeRepository : RepositoryBase
    {
        private IEnumerable<WORKSTATION> GetAllWorkstationsForUser(string username)//must resolve the username usage TODO
        {
            //TODO: create server method ServiceFactory.Current.GetAllWorkstationsForUser(string username);
            //return new List<WORKSTATION>
            //{
            //    new WORKSTATION
            //    {
            //        WORKSTATION_ID = 282,
            //        WORKSTATION_NAME = "TEST WKS",
            //        LOUNGE_ID = 111,
            //        LOUNGE_DETAIL = new LOUNGE
            //        {
            //            LOUNGE_ID = 111,
            //            LOUNGE_NAME = "TEST LOUNGE",
            //        },
            //        AIRPORT_CODE = "YUL",
            //    },
            //    new WORKSTATION
            //    {
            //        WORKSTATION_ID = 283,
            //        WORKSTATION_NAME = "TEST WKS 2",
            //        LOUNGE_ID = 222,
            //        LOUNGE_DETAIL = new LOUNGE
            //        {
            //            LOUNGE_ID = 222,
            //            LOUNGE_NAME = "TEST LOUNGE 2",
            //        },
            //        AIRPORT_CODE = "YUL",
            //    },
            //};
            return new List<WORKSTATION>
            {
                new WORKSTATION
                {
                    WORKSTATION_ID = 322,
                    WORKSTATION_NAME = "BUSINESS",
                    LOUNGE_ID = 111,
                    LOUNGE_DETAIL = new LOUNGE
                    {
                        LOUNGE_ID = 111,
                        LOUNGE_NAME = "LX BUSINESS LOUNGE",
                    },
                    AIRPORT_CODE = "YUL",
                },
                new WORKSTATION
                {
                    WORKSTATION_ID = 323,
                    WORKSTATION_NAME = "SENATOR",
                    LOUNGE_ID = 222,
                    LOUNGE_DETAIL = new LOUNGE
                    {
                        LOUNGE_ID = 222,
                        LOUNGE_NAME = "LX SENATOR LOUNGE 2",
                    },
                    AIRPORT_CODE = "YUL",
                },
            };
        }

        public void ReloadLounges(bool forceGet = false){
            try{
                Lounges = WorkstationDBAdapter.Current.GetWorkstations();
                //Lounges = new List<Lounge>();


                if(Lounges == null || Lounges.Count() == 0 || forceGet)
                {
                    Lounges = GetAllWorkstationsForUser(MembershipProvider.Current.LoggedinUser.Username).Select(x=> new Workstation{
                        AirportCode = x.AIRPORT_CODE,
                        LoungeID = x.LOUNGE_DETAIL.LOUNGE_ID,
                        LoungeName = x.LOUNGE_DETAIL.LOUNGE_NAME,
                        WorkstationID = x.WORKSTATION_ID,
                        WorkstationName = x.WORKSTATION_NAME
                    }).ToList();

                    if(Lounges != null || Lounges.Count() > 0)
                    {
                        foreach (var lounge in Lounges) {
                            WorkstationDBAdapter.Current.SaveWorkstation(lounge);
                        }
                        NotifyRepositoryChanged(Lounges);
                    }
                }
            }
            catch (Exception ex){
                Logger.Current.AddError ("Error in loading lounges", ex);
            }
        }

        private LoungeRepository()
        {
            ReloadLounges();
        }

        private static LoungeRepository _current;
        public static LoungeRepository Current {
            get{
                return _current ?? (_current = new LoungeRepository());
            }
            private set{
                _current = value;
            }
        }

        public IEnumerable<Workstation> Lounges
        {
            get;
            set;
        }
    }
}