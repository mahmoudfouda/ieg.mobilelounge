﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Support.V4.View;
using Android.Support.V4.App;
using AIMS.Models;
using Android.Graphics;
using Android.Views;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity (Label = "AIMS Passenger Info", Icon = "@drawable/Icon", Theme = "@style/Theme.PageIndicatorDefaults"/*, WindowSoftInputMode = SoftInput.AdjustResize*/)]
	[IntentFilter (new[]{Intent.ActionMain}, Categories = new[]{ "com.iegamerica.aims" })]
	public class ManualInputActivity : BaseTabActivity
	{
        #region Fields
        
        #endregion

        #region Elements
        TableLayout pagerButtonsPanel;
	    ImageButton btnConfirm, btnCancel;
        Button btnScan;
        TextView lblMainPageTitle;//, lblMIPageTime;
        ImageView imgMainPageTitle;
        //Timer clockTimer;
        #endregion

        #region Properties
        //public bool IsNameRequired { get; private set; }

        //public bool IsFFNRequired { get; private set; }

        //public bool IsFlightInfoRequired { get; private set; }

        //public bool IsNotesRequired { get; private set; }

        public PassengerValidation ValidationResult { get; private set; }

        #endregion

        #region Methods
        private void RefreshButtons()
        {
            if (btnConfirm == null || btnCancel == null || mPager == null) return;

            var pos = mPager.CurrentItem;

            if (pos == mPager.Adapter.Count - 1)//if last tab
                btnConfirm.SetImageResource(Resource.Drawable.ic_check_white_48dp);// "Done";
            else
                btnConfirm.SetImageResource(Resource.Drawable.ic_chevron_right_white_48dp);// "Next";

            if (pos == 0)//if first tab
                btnCancel.SetImageResource(Resource.Drawable.ic_close_white_48dp);// "Cancel";
            else
                btnCancel.SetImageResource(Resource.Drawable.ic_chevron_left_white_48dp);

            //btnConfirm.Text = App.TextProvider.GetText(2031);// "Done";
            //btnCancel.Text = App.TextProvider.GetText(2004);// "Cancel";
            //btnConfirm.Text = App.TextProvider.GetText(2029);// "Next";
            //btnCancel.Text = App.TextProvider.GetText(2030);// "Back";
        }

	    public void Close()
	    {
            Finish();
        }

        private void Cancel(object sender, EventArgs e)
        {
            var pos = mPager.CurrentItem;

            if (pos > 0)
                mPager.SetCurrentItem(pos - 1, true);
            else//first Item
                Finish();
        }

        public void Confirm(object sender, EventArgs e)
        {
            //Toast.MakeText(this,"Confirmed", ToastLength.Short).Show();
            var pos = mPager.CurrentItem;

            if (pos == mPager.Adapter.Count - 1)//last Item
            {
                //TODO: Validate the passenger info
                //((MIMainInformationFragment)FRAGMENTS[0]).SavePassenger();
                //((MIDetailedInformationFragment)FRAGMENTS[1]).SavePassenger();

                var resultActivity = new Intent(this, typeof(ResultActivity));
                resultActivity.PutExtra("barcode", string.Empty);
                resultActivity.PutExtra("isSecondScan", (false).ToString());
                //resultActivity.PutExtra("errorCode", ((int)errorCode).ToString());
                StartActivity(resultActivity);
                Finish();
            }
            else
            {
                mPager.SetCurrentItem(pos + 1, true);
            }
        }

        private void Scan(object sender, EventArgs e)
        {
            //Finish();
            App.MainForm.Scan(true);
        }
        
        //void RefreshTime()
        //{
        //    lblMIPageTime.Text = string.Format("[{0}]", DateTime.Now.ToShortTimeString());
        //}

        public void RefreshTabs()
        {
            ((PAXDetailAdapter)mAdapter).TabCaptions = AIMSApp.IsDemoVersion
                ? new string[]
                {
                    LanguageRepository.Current.GetText(2022) /*"Passenger"*/,
                    LanguageRepository.Current.GetText(2015) /*"Flight"*/,
                    LanguageRepository.Current.GetText(2043) /*"Services"*/
                }
                : new string[]
                {
                    LanguageRepository.Current.GetText(2022) /*"Passenger"*/,
                    LanguageRepository.Current.GetText(2015) /*"Flight"*/ /*, "Services"*/
                };

            mIndicator.SetViewPager(mPager);
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
            
            RefreshTabs();
            RefreshButtons();
        }
        #endregion

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.ManualInput);
            //App.ManualInputForm = this;
            ValidationResult = new PassengerValidation();


            //if (MembershipProvider.Current.LoggedinUser == null)
            //	OnBackPressed ();

            lblMainPageTitle = FindViewById<TextView>(Resource.Id.lblMIPageTitle);
            //lblMIPageTime = FindViewById<TextView>(Resource.Id.lblMIPageTime);
            imgMainPageTitle = FindViewById<ImageView>(Resource.Id.imgMIPageTitle);

            pagerButtonsPanel = FindViewById<TableLayout>(Resource.Id.pagerButtons);
            btnConfirm = (ImageButton)pagerButtonsPanel.FindViewWithTag("MIConfirmButton");
            btnCancel = (ImageButton)pagerButtonsPanel.FindViewWithTag("MICancelButton");
            btnScan = (Button)pagerButtonsPanel.FindViewWithTag("MIScanButton");
            //btnConfirm.SetBackgroundResource (Resource.Drawable.defaultbutton);
            //btnCancel.SetBackgroundResource (Resource.Drawable.defaultbutton);

            if (MembershipProvider.Current == null) {
                Finish();
                return;//TODO: Logout
            }
            
            #region Loading Airline Image
            Title = lblMainPageTitle.Text = MembershipProvider.Current.UserHeaderText;
            var airline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
            if (airline != null)
            {
                if (airline.AirlineLogoBytes != null && airline.AirlineLogoBytes.Length > 0)
                {
                    //imgMainPageTitle.SetImageResource(Resource.Drawable.STR);//Star Alliance client
                    var bm = BitmapFactory.DecodeByteArray(airline.AirlineLogoBytes, 0, airline.AirlineLogoBytes.Length);
                    if (bm != null)
                    {
                        bm = ImageAdapter.HighlightImage(bm, 6);
                        imgMainPageTitle.SetImageBitmap(bm);
                    }
                }
                else
                {
                    if (airline.LogoResourceID != 0)
                        imgMainPageTitle.SetImageResource(airline.LogoResourceID);
                    else
                        ImageAdapter.LoadImage(airline.ImageHandle, (res) =>
                        {
                            var bm = BitmapFactory.DecodeByteArray(res, 0, res.Length);
                            if (bm != null)
                            {
                                bm = ImageAdapter.HighlightImage(bm);
                                imgMainPageTitle.SetImageBitmap(bm);
                            }
                        });
                }
            }
            #endregion

            #region LocalTime
             //RefreshTime();
             //clockTimer = new Timer(30000);
             //clockTimer.Elapsed += (object sender, ElapsedEventArgs e) =>
             //{
             //    RunOnUiThread(() => {
             //        RefreshTime();
             //    });
             //};
             //clockTimer.Start();
            #endregion

            #region Pager and indicator initiations

		    mAdapter = new PAXDetailAdapter(SupportFragmentManager,
		        AIMSApp.IsDemoVersion
		            ? new Android.Support.V4.App.Fragment[]
		            {
		                new MIMainInformationFragment(),
		                new MIDetailedInformationFragment(),
		                new MITravelServicesFragment()
		            }
		            : new Android.Support.V4.App.Fragment[]
		            {
		                new MIMainInformationFragment(),
		                new MIDetailedInformationFragment(),
		                //new MITravelServicesFragment()
		            },
		        AIMSApp.IsDemoVersion
		            ? new string[]
		            {
		                LanguageRepository.Current.GetText(2022) /*"Passenger"*/,
                        LanguageRepository.Current.GetText(2015) /*"Flight"*/,
                        LanguageRepository.Current.GetText(2043) /*"Services"*/
		            }
		            : new string[]
		            {
		                LanguageRepository.Current.GetText(2022) /*"Passenger"*/,
                        LanguageRepository.Current.GetText(2015) /*"Flight"*/ /*, "Services"*/
		            },
		        AIMSApp.IsDemoVersion
		            ? new int[] {0, 0, 0}
		            : new int[] {0, 0 /*, 0*/});

            mPager = FindViewById<ViewPager> (Resource.Id.pager);
			mPager.Adapter = mAdapter;

			mIndicator = FindViewById<TabPageIndicator> (Resource.Id.indicator);
			//mIndicator.SetViewPager (mPager);
            mPager.PageSelected += (sender, e) => {
                RefreshButtons();
            };
            #endregion
            
            btnConfirm.Click += Confirm;
            btnCancel.Click += Cancel;
            btnScan.Click += Scan;

            try
            {
                var missingDataStr = Intent.GetStringExtra("missingValues");
                if (!string.IsNullOrEmpty(missingDataStr))
                {
                    ValidationResult = new PassengerValidation(missingDataStr);
                }

            }
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }

            LoadUITexts(App.TextProvider.SelectedLanguage);

		}

        public class PAXDetailAdapter : FragmentPagerAdapter, TitleProvider
        {
            private readonly Android.Support.V4.App.Fragment[] _fragments;
            private readonly int[] _imageResourceIds;

            public string[] TabCaptions { get; set; }

            public PAXDetailAdapter(Android.Support.V4.App.FragmentManager fm, Android.Support.V4.App.Fragment[] tabFragments, string[] tabCaptions, int[] imageResourceIds) : base(fm)
            {
                _fragments = tabFragments;
                _imageResourceIds = imageResourceIds;
                TabCaptions = tabCaptions;
            }

            public override Android.Support.V4.App.Fragment GetItem(int position)
            {
                //TODO create all the MI fragments here
                return _fragments[position % _fragments.Length];
            }

            public override int Count => _fragments.Length;

            public string GetTitle(int position)
            {
                return TabCaptions[position % TabCaptions.Length];
            }

            public int GetImageResourceId(int position)
            {
                return _imageResourceIds[position % _imageResourceIds.Length];
            }

            public int GetSecondStateImageResourceId(int position)
            {
                return _imageResourceIds[position % _imageResourceIds.Length];
            }
        }
    }
}

