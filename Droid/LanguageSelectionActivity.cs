using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Content.PM;
using Ieg.Mobile.Localization;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS | Languages", Icon = "@drawable/Icon", NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class LanguageSelectionActivity : BaseActivity
    {
        #region Elements
        ListView lstLanguages;
        #endregion

        #region Methods
        public override void LoadUITexts(Language? selectedLanguage)
        {
            //Really nothing here!!
        }
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LanguageSelection);


            Title = "AIMS | Languages";

            lstLanguages = FindViewById<ListView>(Resource.Id.lstLanguages);

            //App.intentMain = new Intent(this, typeof(MainActivity));
            //App.intentSummary = new Intent(this, typeof(SummaryActivity));

            lstLanguages.Adapter = new LanguageAdapter(this);
            lstLanguages.ItemClick += delegate (object sender, AdapterView.ItemClickEventArgs args)
            {
                ShowLoading("");//TODO: Add "Setting your language.." in the language excel file
                var selectedLanguage = ((JavaLanguage)lstLanguages.Adapter.GetItem(args.Position));

                App.TextProvider.SelectedLanguage = (Language)selectedLanguage.Id;
                LogsRepository.AddInfo("User selected a language", string.Format("Language detail: {0}", selectedLanguage.Name));

                Analytics.TrackEvent("LanguageSelectionActivity", new Dictionary<string, string>
                {
                    { "User selected a language", string.Format("Language detail: {0}", selectedLanguage.Name) },
                });


                HideLoading();
                StartActivity(new Intent(Application.Context, typeof(LoginActivity)));
                Finish();
            };
        }
    }
}