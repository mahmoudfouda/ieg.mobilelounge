﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;
using Microsoft.AppCenter.Analytics;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS", Icon = "@drawable/Icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, Theme = "@android:style/Theme.Dialog")]
    public class PopupActivity : BaseActivity
    {
        private TextView lblCustomers;
        private TextView lblUpdatedTime;
        private ImageView imgTitle;
        private string stringCustomer = "Current Customers: {0}";
        private string stringTime = "Last Update: {0}";

        public override void LoadUITexts(Language? selectedLanguage)
        {

        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.RequestFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LoungeTrafficPopup);
            Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));
            SetFinishOnTouchOutside(true);

            lblCustomers = FindViewById<TextView>(Resource.Id.lblCustomers);
            lblUpdatedTime = FindViewById<TextView>(Resource.Id.lblUpdatedTime);
            imgTitle = FindViewById<ImageView>(Resource.Id.imgTitle);

            if (MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy != null)
            {
                lblCustomers.Text = string.Format(stringCustomer, MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.OccupancyTotal);
                lblUpdatedTime.Text = string.Format(stringTime, MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.Time.ToString("hh:mm tt"));
            }
            else
            {
                lblCustomers.Text = string.Format(stringCustomer, 0);
                lblUpdatedTime.Text = string.Format(stringTime, DateTime.Now.ToString("hh:mm tt"));
            }

            var airline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
            if (airline != null)
            {
                if (AIMSApp.LoungeBitmapImage != null)
                {
                    imgTitle.SetImageBitmap(AIMSApp.LoungeBitmapImage);
                }
                else
                {
                    if (airline.AirlineLogoBytes != null && airline.AirlineLogoBytes.Length > 0)
                    {
                        var bm = BitmapFactory.DecodeByteArray(airline.AirlineLogoBytes, 0, airline.AirlineLogoBytes.Length);
                        if (bm != null)
                        {
                            imgTitle.SetImageBitmap(bm);
                        }
                    }
                    else
                    {
                        //(new JavaAirline(airline)).LoadImage(imgAgentAirline, this.Context, App.ImageRepository);//Passing new object of ImageRepository
                        var item = (new JavaAirline(airline));
                        /*ImageRepository.Current*/
                        App.ImageRepository.LoadImage(item.ImageHandle, (imageResult) =>
                        {
                            if (imageResult == null)
                            {
                                LogsRepository.AddError("Error in AgentProfileActivity in loading airline image", "result is null");

                                Analytics.TrackEvent("LanguageSelectionActivity", new Dictionary<string, string>
                                {
                                { "Error in AgentProfileActivity in loading airline image", "result is null" },
                                });


                                return;
                            }

                            if (imageResult.IsSucceeded)
                            {
                                var bytes = imageResult.ReturnParam;

                                //item.SaveImage(bytes);//Removed IImageItem and it is automatically saved in the ImageRepository

                                if (bytes == null || bytes.Length == 0) return;

                                try
                                {
                                    var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                                    if (bm != null)
                                    {
                                        this.RunOnUiThread(() =>
                                        {
                                            imgTitle.SetImageBitmap(bm);
                                        });
                                        //bm.Recycle();
                                        //bm = null;
                                    }
                                }
                                catch (System.Exception eex)
                                {
                                    Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                                }
                            }
                        });
                    }
                }
            }
        }
    }
}