using Android.Content;
using Android.Widget;
using Android.Util;
using Android.Views.InputMethods;

namespace AIMS.Droid
{
    public class ActionEditText : EditText
    {
        public ActionEditText(Context context) : base(context)
        {
        }

        public ActionEditText(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public ActionEditText(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
        }

        public override IInputConnection OnCreateInputConnection(EditorInfo outAttrs)
        {
            IInputConnection conn = base.OnCreateInputConnection(outAttrs);
            outAttrs.ImeOptions &= ~ImeFlags.NoEnterAction;
            return conn;
            //IInputConnection connection = base.OnCreateInputConnection(outAttrs);

            //int imeActions = (int)((int)outAttrs.ImeOptions & (int)ImeAction.ImeMaskAction);// EditorInfo.IME_MASK_ACTION;
            //if ((imeActions & (int)ImeAction.Done/*EditorInfo.IME_ACTION_DONE*/) != 0)
            //{
            //    // clear the existing action
            //    outAttrs.ImeOptions = (ImeFlags)((int)outAttrs.ImeOptions ^ imeActions);
            //    // set the DONE action
            //    outAttrs.ImeOptions = (ImeFlags)((int)outAttrs.ImeOptions | (int)ImeAction.Done);// EditorInfo.IME_ACTION_DONE;
            //}
            //if ((outAttrs.ImeOptions &  ImeFlags.NoEnterAction/* EditorInfo.IME_FLAG_NO_ENTER_ACTION*/) != 0)
            //{
            //    outAttrs.ImeOptions &= ~ImeFlags.NoEnterAction;// EditorInfo.IME_FLAG_NO_ENTER_ACTION;
            //}
            //return connection;
        }
    }
}