using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AIMS.Droid
{
	/**
	 * A TitleProvider provides the title to display according to a view.
	 */
	public interface TitleProvider
	{
		/**
	     * Returns the title of the view at position
	     * @param position
	     * @return
	     */
		String GetTitle (int position);

		/**
	     * Returns the image resource id of the view at position
	     * @param position
	     * @return
	     */
		int GetImageResourceId (int position);

        /**
	     * Returns the second state image resource id of the view at position
	     * @param position
	     * @return
	     */
        int GetSecondStateImageResourceId(int position);
    }
}

