using System;

using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Android.Locations;
using AIMS.Models;
using Ieg.Mobile.DataContracts.MobileLounge.ServiceModel;
using Ieg.Mobile.DataContracts.Models;

namespace AIMS.Droid
{
    public class GPSAdapter : Activity, ILocationListener, IGpsAdapter
    {
        #region Fields
        LocationManager _locationManager;
        string _locationProvider;
        Activity _ownerActivity;
        #endregion

        #region Constructors
        public GPSAdapter(Activity activity)
        {
            if (activity == null) throw new Exception("The owner activity is not accessible!");

            _ownerActivity = activity;
        }
        #endregion

        #region Events
        public event EventHandler<Position> GpsAdapter_OnLocationChanged;
        public event EventHandler<Exception> OnLog;

        public void OnLocationChanged(Location location)
        {
            if (GpsAdapter_OnLocationChanged != null)
                GpsAdapter_OnLocationChanged.Invoke(this, new Position
                {
                    Altitude = location.Altitude,
                    Latitude = location.Latitude,
                    Longitude = location.Longitude,
                    Accuracy = location.Accuracy,
                });
        }

        public void OnProviderDisabled(string provider)
        {
            if (_ownerActivity != null)
                RunOnUiThread(() =>
                {
                    Toast.MakeText(_ownerActivity, "GPS is off!", ToastLength.Short).Show();
                });
            if (GpsAdapter_OnLocationChanged != null)
                GpsAdapter_OnLocationChanged.Invoke(this, null);

            Stop();
            //Start();
        }

        public void OnProviderEnabled(string provider)
        {
            if (_ownerActivity != null)
                RunOnUiThread(() =>
                {
                    Toast.MakeText(_ownerActivity, "GPS is on!", ToastLength.Short).Show();
                });
            Start();
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
        }
        #endregion

        #region Methods
        public void Start()
        {
            try
            {
                if (_ownerActivity == null) return;

                _locationManager = (LocationManager)_ownerActivity.GetSystemService(LocationService);

                Criteria locationCriteria = new Criteria();

                locationCriteria.Accuracy = Accuracy.Coarse;
                locationCriteria.PowerRequirement = Power.Medium;
                locationCriteria.AltitudeRequired = true;
                locationCriteria.BearingRequired = false;
                locationCriteria.SpeedRequired = false;

                _locationProvider = _locationManager.GetBestProvider(locationCriteria, true);


                if (_locationProvider != null)/* && _locationManager.IsProviderEnabled(_locationProvider)*/
                {
                    if (_locationProvider.Equals("gps") && ((AIMSApp)_ownerActivity.Application).CurrentDeviceType == DeviceType.Tablet)
                        _locationProvider = "network";

                    if (_locationProvider.Equals("network") || _locationProvider.Equals("gps"))
                        _locationManager.RequestLocationUpdates(_locationProvider, 10, (float)0.1, this);
                    else//_locationProvider.Equals("passive")//Must be
                    {
                        LogsRepository.AddError("GPS error",
                            string.Format("{0} is not available.\nLocation provider is '{1}'", locationCriteria, _locationProvider));

                        throw new Exception("GPS Error", new Exception(string.Format("{0} is not available or disabled.", locationCriteria)));
                    }
                    //CurrentLocation = _locationManager.GetLastKnownLocation(LocationManager.PassiveProvider);//for security reasons (do not get unreal locations)
                }
                else
                {
                    LogsRepository.AddError("GPS error", string.Format("{0} is not available.", locationCriteria));
                    //Toast.MakeText(_ownerActivity, 
                    //    string.Format("{0} is not available. Does the device have location services enabled?", locationCriteria), ToastLength.Short).Show();//TODO: use language provider
                    throw new Exception("GPS Error", new Exception(string.Format("{0} is not available or disabled.", locationCriteria)));
                }
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("GPS Start exception", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                throw new Exception("GPS Error", ex);
            }
        }

        public void Stop()
        {
            if (_locationManager != null)
                _locationManager.RemoveUpdates(this);
        }
        #endregion
    }
}