﻿using System;

namespace AIMS.Droid
{
	public class JavaObjectBase<T> : Java.Lang.Object
	{
		public virtual T Item { get; set; }

		public JavaObjectBase()
			: this(default(T)){}

		public JavaObjectBase(T item)
		{
			this.Item = item;
		}
	}
}

