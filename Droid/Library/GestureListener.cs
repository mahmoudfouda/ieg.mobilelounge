﻿using System;
using Android.Views;

namespace AIMS.Droid
{
	class GestureListener: Java.Lang.Object, GestureDetector.IOnGestureListener
	{
        #region Actions
        public event Action OnLeftToRight;
		public event Action OnRightToLeft;
		public event Action OnTopToBottom;
		public event Action OnBottomToTop;
		public event Action OnSingleTap;
        #endregion

        #region Constants
        static int SWIPE_MAX_OFF_PATH = 250;//250
		static int SWIPE_MIN_DISTANCE = 20;//100
		static int SWIPE_THRESHOLD_VELOCITY = 100;//200
        #endregion

        #region Fields
        private BaseTabActivity hostActivity;
        #endregion

        #region Constructor
        public GestureListener(BaseTabActivity activity)
		{
			hostActivity = activity;
        }
        #endregion

        #region Events
        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			try
			{
				if ( Math.Abs ( e1.GetY () - e2.GetY () ) > SWIPE_MAX_OFF_PATH && Math.Abs ( e1.GetX () - e2.GetX () ) < SWIPE_MAX_OFF_PATH )
				{
					if (OnBottomToTop != null && e1.GetY () - e2.GetY () > SWIPE_MIN_DISTANCE && Math.Abs ( velocityY ) > SWIPE_THRESHOLD_VELOCITY)
					{
						//var bottomMaxDistance = (hostActivity.IntDisplayHeight/8);
						var bottomMaxDistance = hostActivity.ConvertDpToPixels(66);
						if(e1.GetY() >= (hostActivity.IntDisplayHeight - bottomMaxDistance))//<100
							OnBottomToTop ();
					}
					else if (OnTopToBottom != null && e2.GetY () - e1.GetY () > SWIPE_MIN_DISTANCE && Math.Abs ( velocityY ) > SWIPE_THRESHOLD_VELOCITY )
					{
						var topMaxDistance = hostActivity.ConvertDpToPixels(128);
						if(e1.GetY() < topMaxDistance)
							OnTopToBottom ();
					}
				}
				else if ( Math.Abs ( e1.GetY () - e2.GetY () ) < SWIPE_MAX_OFF_PATH && Math.Abs ( e1.GetX () - e2.GetX () ) > SWIPE_MAX_OFF_PATH ){
					if (OnRightToLeft != null && e1.GetX () - e2.GetX () > SWIPE_MIN_DISTANCE && Math.Abs ( velocityX ) > SWIPE_THRESHOLD_VELOCITY )
					{
						OnRightToLeft ();
					}
					else if (OnLeftToRight != null && e2.GetX () - e1.GetX () > SWIPE_MIN_DISTANCE && Math.Abs ( velocityX ) > SWIPE_THRESHOLD_VELOCITY)
					{
						//if(e1.GetX()<300)//<100 (2/3 of width)
						OnLeftToRight ();
					}
				}
			}
			//catch// ( Exception e )
			//{
			//	//Console.WriteLine ( "Failed to work" +e.Message);
			//}
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }

            return false;
		}

		public bool OnDown(MotionEvent e)
		{
			return true;
		}
		public void OnLongPress(MotionEvent e) {}
		public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			return true;
		}
		public void OnShowPress(MotionEvent e)
		{

		}
		public bool OnSingleTapUp(MotionEvent e)
		{
			if(OnSingleTap != null)
				OnSingleTap();
			return true;
        }
        #endregion
    }
}

