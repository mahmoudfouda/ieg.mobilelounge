﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIMS.Models;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace AIMS.Droid
{
    public class CenterLockHorizontalScrollview : HorizontalScrollView
    {
        private Context context;
        private int prevIndex = 0;
        private ViewGroup parent;
        private ServicesAdapter _mAdapter;

        public event EventHandler<PassengerService> OnItemSelected;

        public CenterLockHorizontalScrollview(Context context, IAttributeSet attrs) : base(context,attrs)
        {
            this.context = context;
            this.SmoothScrollingEnabled = (true);
        }

        public void SetAdapter(Context context, ServicesAdapter mAdapter)
        {

            try
            {
                _mAdapter = mAdapter;
                FillViewWithAdapter(mAdapter);
            }
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }
        }

        private void FillViewWithAdapter(ServicesAdapter mAdapter)
        {
            if (ChildCount == 0)
            {
                throw new Exception(
                        "CenterLockHorizontalScrollView must have one child");
            }
            if (ChildCount == 0 || mAdapter == null)
                return;

            parent = (ViewGroup)GetChildAt(0);

            parent.RemoveAllViews();

            for (int i = 0; i < mAdapter.Count; i++)
            {
                var itemView = mAdapter.GetView(i, null, parent);
                parent.AddView(itemView);

                itemView.Click += OnItemViewClick;
            }
        }

        private void OnItemViewClick(object sender, EventArgs e)
        {
            try
            {
                var position = parent.IndexOfChild(sender as View);

                if (OnItemSelected != null)
                    OnItemSelected.Invoke(position, (JavaPassengerService)_mAdapter.GetItem(position));
            }
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }

        }
    }
}