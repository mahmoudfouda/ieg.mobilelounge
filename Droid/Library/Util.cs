﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AIMS.Droid.Library
{
    public static class Util
    {
        public static string Capitalize(string text)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(text[0]) + text.Substring(1);
        }

        public static Android.Graphics.Color GetProgressColor(int percentage)
        {
            return percentage >= 90 ? Android.Graphics.Color.ParseColor("#eb5656") :
                   percentage >= 50 ? Android.Graphics.Color.ParseColor("#f2c84b") :
                                      Android.Graphics.Color.ParseColor("#6ecf97");
        }
    }
}