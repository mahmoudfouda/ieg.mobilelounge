using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

namespace AIMS.Droid
{
    public class AIMSMessageDialogFragment : Android.Support.V4.App.DialogFragment
    {
        #region Elements
        TextView lblTitle, lblText;
        Button btnOk;
        #endregion
        
        #region Fields
        string title, text;
        #endregion

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Android 3.x+ still wants to show title: disable
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            
            // CHANGE TO YOUR DIALOG LAYOUT or VIEW CREATION CODE
            var view = inflater.Inflate(Resource.Layout.AIMSMessage, container, true);

            this.lblTitle = view.FindViewById<TextView>(Resource.Id.lblAIMSMessageTitle);
            this.lblText = view.FindViewById<TextView>(Resource.Id.lblAIMSMessageText);
            this.btnOk = view.FindViewById<Button>(Resource.Id.btnAIMSMessageButton);

            this.lblText.Text = text;
            this.lblTitle.Text = title;
            this.btnOk.Click += (sender, e) => {
                Dismiss();
            };

            return view;
        }

        public override void OnResume()
        {
            // Auto size the dialog based on it's contents
            Dialog.Window.SetLayout(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);

            // Make sure there is no background behind our view
            Dialog.Window.SetBackgroundDrawable(new ColorDrawable(Android.Graphics.Color.Transparent));

            // Disable standard dialog styling/frame/theme: our custom view should create full UI
            SetStyle(Android.Support.V4.App.DialogFragment.StyleNoFrame, Android.Resource.Style.Theme);

            base.OnResume();
        }
    }
}