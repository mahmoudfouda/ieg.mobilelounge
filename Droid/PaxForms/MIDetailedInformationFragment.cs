﻿using Android.OS;
using Android.Views;
using Android.Widget;
using AIMS.Models;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    public class MIDetailedInformationFragment : BaseFragment
	{
		#region Elements
		View view;

        TextView lblAirportFromLabel, lblAirportToLabel, lblSeatNumberLabel, lblPNRLabel, lblPassengerStatusLabel;
        EditText txtAirportFrom, txtAirportTo, txtSeatNumber, txtPNR, txtPassengerStatus;
        #endregion

        #region Methods
        private void LoadPassenger()
        {
            txtAirportFrom.Text = MembershipProvider.Current.SelectedLounge.AirportCode;
            if (MembershipProvider.Current.SelectedPassenger != null)
            {
                ManualInputActivity parent = (ManualInputActivity)Activity;

                //txtAirportFrom.Text = MembershipProvider.Current.SelectedPassenger.FromAirport;//TODO: doublecheck with mike to stay commented
                txtAirportTo.Text = MembershipProvider.Current.SelectedPassenger.ToAirport;
                txtSeatNumber.Text = MembershipProvider.Current.SelectedPassenger.SeatNumber;

                if(parent.ValidationResult.IsPNRRequired)
                    txtPNR.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                txtPNR.Text = MembershipProvider.Current.SelectedPassenger.PNR;
                txtPassengerStatus.Text = MembershipProvider.Current.SelectedPassenger.PassengerStatus;
            }
        }

        public void SavePassenger()
        {
            if (MembershipProvider.Current == null) return;//in case of session timeout (which logs the user out)

            if (MembershipProvider.Current.SelectedPassenger == null)
                MembershipProvider.Current.SelectedPassenger = new Passenger();

            MembershipProvider.Current.SelectedPassenger.FromAirport = txtAirportFrom.Text;
            MembershipProvider.Current.SelectedPassenger.ToAirport = txtAirportTo.Text;
            MembershipProvider.Current.SelectedPassenger.SeatNumber = txtSeatNumber.Text;

            MembershipProvider.Current.SelectedPassenger.PNR = txtPNR.Text;
            MembershipProvider.Current.SelectedPassenger.PassengerStatus = txtPassengerStatus.Text;
            MembershipProvider.Current.SelectedPassenger.TrackingPassCombinedFlightNumber = "";

        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
            lblAirportFromLabel.Text = App.TextProvider.GetText(2032);//From (Airport)
            lblAirportToLabel.Text = App.TextProvider.GetText(2033);//To (Airport)
            txtAirportFrom.Hint = App.TextProvider.GetText(2018);//Departure
            txtAirportTo.Hint = App.TextProvider.GetText(2019);//Arrival

            lblSeatNumberLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2034));//Seat Number:
            lblPNRLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2044));//PNR: 
            lblPassengerStatusLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2035));//Passenger Status: 
        }
        #endregion

        #region Events
        public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
            #region Initializing and getting controls
            view = inflater.Inflate(Resource.Layout.MIDetailedInformation, container, false);
			txtAirportFrom = view.FindViewById<EditText>(Resource.Id.txtAirportFrom);
			txtAirportTo = view.FindViewById<EditText>(Resource.Id.txtAirportTo);
            txtSeatNumber = view.FindViewById<EditText>(Resource.Id.txtSeatNumber);
            txtPNR = view.FindViewById<EditText>(Resource.Id.txtPNR);
			txtPassengerStatus = view.FindViewById<EditText>(Resource.Id.txtPassengerStatus);

            lblAirportFromLabel = view.FindViewById<TextView>(Resource.Id.lblAirportFromLabel);
            lblAirportToLabel = view.FindViewById<TextView>(Resource.Id.lblAirportToLabel);
            lblSeatNumberLabel = view.FindViewById<TextView>(Resource.Id.lblSeatNumberLabel);
            lblPNRLabel = view.FindViewById<TextView>(Resource.Id.lblPNRLabel);
            lblPassengerStatusLabel = view.FindViewById<TextView>(Resource.Id.lblPassengerStatusLabel);
            #endregion

            this.txtAirportTo.EditorAction += (_, e) =>
            {
                if (e.ActionId == Android.Views.InputMethods.ImeAction.Done)
                {
                    ((ManualInputActivity)this.Activity).Confirm(_, e);
                }
            };

            LoadUITexts(App.TextProvider.SelectedLanguage);
            LoadPassenger();

            return view;
        }
        
        public override void OnPause()
        {
            base.OnPause();
            SavePassenger();
        }
        
        #endregion
    }
}

