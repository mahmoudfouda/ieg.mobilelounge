﻿using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using AIMS.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    public class MIMainInformationFragment : BaseFragment
    {
        #region Elements
        View view;
        TableLayout pnlPassengerName;

        ImageView imgMIAirline, imgMISelectedCard;
        TextView lblMIAirlineName, lblMISelectedCard, /*lblMIAirlineNameLabel,*/ lblMIFullnameLabel,
            lblNotesLabel, lblFFNLabel, lblCarrierFNumberClasLabel;
        EditText txtFirstName, txtLastName, txtFullName, txtFFN, txtNotes, txtCarrier, txtFlightNumber, txtClassOfTravel;
        //Spinner drpMICard;

        //DropDownCardsAdapter dropDownCardsAdapter;
        #endregion

        #region Methods
        private void LoadPassenger()
        {
            #region Load Airline and Card pictures
            lblMIAirlineName.Text = MembershipProvider.Current.SelectedAirline.Name;
            if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes != null && MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length > 0)
            {
                var bm = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedAirline.AirlineLogoBytes, 0, MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length);
                if (bm != null)
                {
                    bm = ImageAdapter.HighlightImage(bm, 6);
                    imgMIAirline.SetImageBitmap(bm);
                    //bm.Recycle();
                    //bm = null;
                }
            }
            else
            {
                if (MembershipProvider.Current.SelectedAirline.LogoResourceID != 0)
                    imgMIAirline.SetImageResource(MembershipProvider.Current.SelectedAirline.LogoResourceID);
                else
                    ImageAdapter.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (res) =>
                    {
                        var bm = BitmapFactory.DecodeByteArray(res, 0, res.Length);
                        if (bm != null)
                        {
                            bm = ImageAdapter.HighlightImage(bm);
                            imgMIAirline.SetImageBitmap(bm);
                        }
                    });
            }

            lblMISelectedCard.Text = MembershipProvider.Current.SelectedCard.Name;
            if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
            {
                var bm = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedCard.CardPictureBytes, 0, MembershipProvider.Current.SelectedCard.CardPictureBytes.Length);
                if (bm != null)
                {
                    imgMISelectedCard.SetImageBitmap(bm);
                    //bm.Recycle();
                    //bm = null;
                }
            }
            else
            {
                if (MembershipProvider.Current.SelectedCard.PictureResourceID != 0)
                    imgMISelectedCard.SetImageResource(MembershipProvider.Current.SelectedCard.PictureResourceID);
                else
                    ImageAdapter.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (res) =>
                    {
                        var bm = BitmapFactory.DecodeByteArray(res, 0, res.Length);
                        if (bm != null)
                        {
                            this.Activity.RunOnUiThread(() =>
                            {
                                imgMISelectedCard.SetImageBitmap(bm);
                            });
                        }
                    });
            }
            #endregion

            if (MembershipProvider.Current.SelectedPassenger == null) return;

            ManualInputActivity parent = (ManualInputActivity)Activity;

            #region Name(s)
            var names = new List<string>();
            if (parent.ValidationResult.PassengerNames.Count > 0)
            {
                names = parent.ValidationResult.PassengerNames;
            }
            else if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.FullName))
                names.Add(MembershipProvider.Current.SelectedPassenger.FullName);

            txtFullName.Text = "";
            var fullNameSB = new StringBuilder();//TODO: refactor and clean
            txtFirstName.Text = "";
            //var firstNameSB = new StringBuilder();
            txtLastName.Text = "";
            //var lastNameSB = new StringBuilder();

            //bool hasSlashSeperator = names.Count == 0 || (names.Count > 0 && names.Any(x => x.Contains("/")));

            if (names.Count > 0)
            {
                //string[] nameParts;
                //if (hasSlashSeperator)
                //    nameParts = names.Last().Split(new char[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);
                //else nameParts = names.Last().Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries).ToArray();

                //lastNameSB.Append(nameParts.Length > 0 ? nameParts[0] : "");

                //firstNameSB.Append(nameParts.Length > 1 ? nameParts[1] : "");

                fullNameSB.Append(names.Last());//using the name on the last document 
            }

            //Keeping this part commented for further changes in minds!!!
            //foreach (var fullName in names)//Showing both names
            //{
            //    if (hasSlashSeperator)
            //    { //filling last name / first name
            //        if (!string.IsNullOrWhiteSpace(fullName))
            //        {
            //            var nameParts = fullName.Split(new char[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);

            //            if (lastNameSB.Length > 0) lastNameSB.Append(";");
            //            lastNameSB.Append(nameParts.Length > 0 ? nameParts[0] : "");

            //            if (firstNameSB.Length > 0) firstNameSB.Append(";");
            //            firstNameSB.Append(nameParts.Length > 1 ? nameParts[1] : "");
            //        }
            //    }
            //    else
            //    { //filling full name
            //        if (fullNameSB.Length > 0) fullNameSB.Append(";");
            //        fullNameSB.Append(fullName);
            //    }
            //}

            //if (hasSlashSeperator)
            //{//Showing last name / first name
            //    pnlPassengerName.Visibility = ViewStates.Visible;
            //    txtFullName.Visibility = ViewStates.Gone;

            //    txtLastName.Text = lastNameSB.ToString();
            //    txtFirstName.Text = firstNameSB.ToString();

            //    if (parent.ValidationResult.IsNameRequired || !string.IsNullOrEmpty(parent.ValidationResult.PassengerNames))
            //    {
            //        txtFirstName.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
            //        txtLastName.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
            //    }
            //}
            //else
            //{//Showing full name
            pnlPassengerName.Visibility = ViewStates.Gone;
            txtFullName.Visibility = ViewStates.Visible;

            txtFullName.Text = fullNameSB.ToString();

            if (parent.ValidationResult.IsNameRequired || names.Count == 0)
            {
                txtFullName.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
            }
            //}
            #endregion

            #region FFN
            if (parent.ValidationResult.IsFFNRequired)
                txtFFN.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
            txtFFN.Text = MembershipProvider.Current.SelectedPassenger.FFN;
            #endregion

            #region Flight Info
            if (parent.ValidationResult.IsFlightInfoRequired)
            {
                txtCarrier.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                txtClassOfTravel.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                txtFlightNumber.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
            }

            txtFlightNumber.Text = MembershipProvider.Current.SelectedPassenger.FlightNumber;
            txtClassOfTravel.Text = MembershipProvider.Current.SelectedPassenger.TrackingClassOfService;

            if (string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedPassenger.FlightCarrier))
            {
                if (MembershipProvider.Current.SelectedAirline != null)
                {
                    txtCarrier.Text = MembershipProvider.Current.SelectedAirline.Code;
                }
            }
            else
            {
                txtCarrier.Text = MembershipProvider.Current.SelectedPassenger.FlightCarrier;
            }
            #endregion
            
            #region Notes
            if (parent.ValidationResult.IsNotesRequired)
                txtNotes.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
            txtNotes.Text = MembershipProvider.Current.SelectedPassenger.Notes; 
            #endregion
        }

        public void SavePassenger()
        {
            if (MembershipProvider.Current == null) return;//in case of session timeout (which logs the user out)

            if (MembershipProvider.Current.SelectedPassenger == null)
                MembershipProvider.Current.SelectedPassenger = new Passenger();

            //MembershipProvider.Current.SelectedAirline.Name = lblMIAirlineName.Text;//TODO: set passengers info
            //MembershipProvider.Current.SelectedCard.Name = lblMISelectedCard.Text;//TODO: set passengers info
            if (txtFullName.Visibility == ViewStates.Visible)//for now, we are always going into this if
                MembershipProvider.Current.SelectedPassenger.FullName = txtFullName.Text;
            else
            {
                if (string.IsNullOrWhiteSpace(txtLastName.Text) || string.IsNullOrWhiteSpace(txtFirstName.Text))
                {
                    MembershipProvider.Current.SelectedPassenger.FullName = txtLastName.Text.Trim() + txtFirstName.Text.Trim();
                }
                else
                {
                    MembershipProvider.Current.SelectedPassenger.FullName = txtLastName.Text.Trim() + "/" + txtFirstName.Text.Trim();
                }
            }
            MembershipProvider.Current.SelectedPassenger.FFN = txtFFN.Text;
            MembershipProvider.Current.SelectedPassenger.Notes = txtNotes.Text;

            MembershipProvider.Current.SelectedPassenger.FlightCarrier = txtCarrier.Text;
            MembershipProvider.Current.SelectedPassenger.FlightNumber = txtFlightNumber.Text;
            MembershipProvider.Current.SelectedPassenger.TrackingClassOfService = txtClassOfTravel.Text;
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
            //lblMIAirlineNameLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2023));//Airline: 
            lblMIFullnameLabel.Text = App.TextProvider.GetText(2024);// string.Format("{0}: ", App.TextProvider.GetText(2024));//Full Name
            txtFirstName.Hint = App.TextProvider.GetText(2025);//First
            txtLastName.Hint = App.TextProvider.GetText(2026);//Last
            txtFullName.Hint = App.TextProvider.GetText(2024);//Full Name
            lblFFNLabel.Text = App.TextProvider.GetText(2027);// string.Format("{0}: ", App.TextProvider.GetText(2027));//Frequent Flyer Number
            lblNotesLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2028));//Notes: 
            lblCarrierFNumberClasLabel.Text = string.Format("{0} - {1} - {2}",
                App.TextProvider.GetText(2036),
                App.TextProvider.GetText(2016),
                App.TextProvider.GetText(2014)); //Carrier - Flight Number - Class
        }
        #endregion

        #region Events
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            #region Initializing and getting controls
            view = inflater.Inflate(Resource.Layout.MIMainInformation, container, false);
            pnlPassengerName = view.FindViewById<TableLayout>(Resource.Id.pnlPassengerName);

            txtFullName = view.FindViewById<EditText>(Resource.Id.txtFullName);
            txtFirstName = view.FindViewById<EditText>(Resource.Id.txtFirstName);
            txtLastName = view.FindViewById<EditText>(Resource.Id.txtLastName);
            txtFFN = view.FindViewById<EditText>(Resource.Id.txtFFN);
            txtCarrier = view.FindViewById<EditText>(Resource.Id.txtCarrier);
            txtFlightNumber = view.FindViewById<EditText>(Resource.Id.txtFlightNumber);
            txtClassOfTravel = view.FindViewById<EditText>(Resource.Id.txtClassOfTravel);
            txtNotes = view.FindViewById<EditText>(Resource.Id.txtNotes);
            
            lblMIFullnameLabel = view.FindViewById<TextView>(Resource.Id.lblMIFullnameLabel);
            lblFFNLabel = view.FindViewById<TextView>(Resource.Id.lblFFNLabel);
            lblNotesLabel = view.FindViewById<TextView>(Resource.Id.lblNotesLabel);
            lblMIAirlineName = view.FindViewById<TextView>(Resource.Id.lblMIAirlineName);
            //lblMIAirlineNameLabel = view.FindViewById<TextView>(Resource.Id.lblMIAirlineNameLabel);
            lblMISelectedCard = view.FindViewById<TextView>(Resource.Id.lblMISelectedCard);
            lblCarrierFNumberClasLabel = view.FindViewById<TextView>(Resource.Id.lblCarrierFNumberClasLabel);

            imgMISelectedCard = view.FindViewById<ImageView>(Resource.Id.imgMISelectedCard);
            imgMIAirline = view.FindViewById<ImageView>(Resource.Id.imgMIAirline);

            //drpMICard = (Spinner)view.FindViewWithTag("drpMISelectedCard");
            //dropDownCardsAdapter = new DropDownCardsAdapter(this.Context);//TODO: Load it manually
            //dropDownCardsAdapter.AdapterChanged += (notUsed)=>{
            //    this.Activity.RunOnUiThread(()=> {
            //        drpMICard.Adapter = dropDownCardsAdapter;
            //        drpMICard.SetSelection(dropDownCardsAdapter.GetCardPosition(MembershipProvider.Current.SelectedCard));
            //    });
            //};//TODO: Make this adapter integrated (singleton)

            //txtNotes.SetHorizontallyScrolling(false);
            //txtNotes.SetMaxLines(int.MaxValue);
            this.txtNotes.EditorAction += (_, e) =>
            {
                if (e.ActionId == Android.Views.InputMethods.ImeAction.Next)
                {
                    ((ManualInputActivity)this.Activity).Confirm(_, e);
                }
                else
                    e.Handled = false;
            };

            #endregion
            LoadUITexts(App.TextProvider.SelectedLanguage);
            LoadPassenger();

            return view;
        }

        public override void OnPause()
        {
            base.OnPause();
            SavePassenger();
        }
        #endregion
    }
}