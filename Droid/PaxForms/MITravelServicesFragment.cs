﻿using Android.OS;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    public class MITravelServicesFragment : BaseFragment
	{
		#region Elements
		View view;
        ListView lstPassengerServices;
        #endregion

        public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			view = inflater.Inflate(Resource.Layout.MITravelServices, container, false);

            lstPassengerServices = view.FindViewById<ListView>(Resource.Id.lstPassengerServices);
            lstPassengerServices.Adapter = new ServicesAdapter(this.Context);//TODO: must load services based on selected info (Card | Airline)
            lstPassengerServices.ItemClick += delegate (object sender, AdapterView.ItemClickEventArgs args)
            {
                var selectedService = ((JavaPassengerService)lstPassengerServices.Adapter.GetItem(args.Position));
                //TODO: customize and calculate passenger services' prices
            };
            return view;
		}

        #region Methods
        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
        }
        #endregion
    }
}

