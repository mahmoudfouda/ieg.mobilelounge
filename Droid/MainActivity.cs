﻿using System;

using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.OS;
//using ZXing.Mobile;
//using ZXing;
using Android.Views.Animations;
using System.Timers;
using System.Linq;
using Android.Support.V4.View;
using Android.Support.V4.App;
using Android.Graphics;
using MWBarcodeScanner;
using Android.Media;
using Ieg.Mobile.Localization;
using Android.Support.V4.Content;
using Android;
using Android.Content.PM;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Android.Animation;
using AIMS.Droid.Library;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS", Icon = "@drawable/Icon", Theme = "@style/Theme.PageIndicatorDefaults")]
    [IntentFilter(new[] { Intent.ActionMain }, Categories = new[] { "com.iegamerica.aims" })]
    //[Activity (Label = "AIMS", Icon = "@drawable/icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : BaseTabActivity//global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        #region Constants
        const int scanTimeout = 20000;
        const int focusInterval = 800;
        #endregion

        #region Elements
        GridView grdAirlines, grdAirlines2;
        View separatorLine;
        ImageButton btnScan;
        AutoCompleteTextView txtSearch;
        TextView lblMainPageTitle;//, lblMainPageTime;
        ImageView imgMainPageTitle;
        ImageButton btnSearchToggle, btnMenuLeft, btnMenuRight, btnCloseSearch, btnSearch;
        //MobileBarcodeScanner scanner;
        //TabHost tabHost;
        LinearLayout swipeMenu, pnlGrdAirlines;
        RelativeLayout pnlSearch;//, pnlMainPageTitle;

        Timer scanTimer;//, clockTimer;//, focusTimer;
        GestureDetector gestureDetector;
        GestureListener gestureListener;
        #endregion

        #region Fields
        //TODO: Demo condition
        public static Android.Support.V4.App.Fragment[] FRAGMENTS;
        // = AIMSApp.IsDemoVersion ? new Android.Support.V4.App.Fragment[]{
        //    new AgentProfileActivity(),
        //    new AgentLogsActivity(),
        //    new AgentMessagesActivity(),
        //    new AgentPassengersActivity()//,
        //    //new AgentStatsActivity()
        //} : new Android.Support.V4.App.Fragment[]{
        //    new AgentProfileActivity(),
        //    //new AgentLogsActivity(),
        //    //new AgentMessagesActivity(),
        //    new AgentPassengersActivity()//,
        //    //new AgentStatsActivity()
        //};

        //TODO: Demo condition
        public static string[] CONTENT;// = AIMSApp.IsDemoVersion ? new string[] { LanguageProvider.Current.GetText(2009)/*"Home"*/, LanguageProvider.Current.GetText(2040)/*"Logs"*/, LanguageProvider.Current.GetText(2041)/*"Messages"*/, LanguageProvider.Current.GetText(2010) /*"Last Hour"*//*, "Stats"*/ } :
                                       //new string[] { LanguageProvider.Current.GetText(2009)/*"Home"*/,/* "Logs", "Messages",*/ LanguageProvider.Current.GetText(2010)/*"Last Hour"*//*, "Stats"*/ };


        //TODO: Demo condition//<!--Theme change-->
        public static int[] ImageResourceIds;// = AIMSApp.IsDemoVersion ? new int[] { Resource.Drawable.tab_home_off_light, Resource.Drawable.tab_logs_off_light, Resource.Drawable.tab_message_off_light, Resource.Drawable.tab_passengers_off_light, Resource.Drawable.tab_stats_off_light } :
                                             //new int[] { Resource.Drawable.tab_home_off_light, /*Resource.Drawable.tab_logs_off, Resource.Drawable.tab_message_off,*/ Resource.Drawable.tab_passengers_off_light, Resource.Drawable.tab_stats_off_light };//TODO: Add passengers tab image
                                             //public static int[] ImageResourceIds = AIMSApp.IsDemoVersion ? new int[] { Resource.Drawable.tab_home_off, Resource.Drawable.tab_logs_off, Resource.Drawable.tab_message_off, Resource.Drawable.tab_passengers_off, Resource.Drawable.tab_stats_off } :
                                             //new int[] { Resource.Drawable.tab_home_off, /*Resource.Drawable.tab_logs_off, Resource.Drawable.tab_message_off,*/ Resource.Drawable.tab_passengers_off, Resource.Drawable.tab_stats_off };//TODO: Add passengers tab image

        //TODO: Demo condition//<!--Theme change-->
        public static int[] ImageResourceIds_On;//= AIMSApp.IsDemoVersion ? new int[] { Resource.Drawable.tab_home_off, Resource.Drawable.tab_logs_off, Resource.Drawable.tab_message_off, Resource.Drawable.tab_passengers_off, Resource.Drawable.tab_stats_off } :
                                                //new int[] { Resource.Drawable.tab_home_off, /*Resource.Drawable.tab_logs_on, Resource.Drawable.tab_message_on,*/ Resource.Drawable.tab_passengers_off, Resource.Drawable.tab_stats_off };//TODO: Add passengers tab image (selected state)
                                                //public static int[] ImageResourceIds_On = AIMSApp.IsDemoVersion ? new int[] { Resource.Drawable.tab_home_on, Resource.Drawable.tab_logs_on, Resource.Drawable.tab_message_on, Resource.Drawable.tab_passengers_on, Resource.Drawable.tab_stats_on } :
                                                //                                                                    new int[] { Resource.Drawable.tab_home_on, /*Resource.Drawable.tab_logs_on, Resource.Drawable.tab_message_on,*/ Resource.Drawable.tab_passengers_on, Resource.Drawable.tab_stats_on };//TODO: Add passengers tab image (selected state)


        bool searchPanelVisible = false;

        //public int TabsCount {
        //	get;
        //	set;
        //}
        AirlinesAdapter airlinesAdapter;
        ArrayAdapter autoCompleteAdapter;
        Vibrator vibrator;
        ToneGenerator toneGenerator;
        int searchPanelMeasuredHeight;
        private ProgressBar occupancyProgress;
        private TextView progressBarinsideText;

        //AIMSApp app;
        #endregion

        #region Properties
        public bool IsAgentMenuOpen { get { return swipeMenu.IsShown; } }
        #endregion

        #region Methods
        public async void Scan(bool isSecondScan = false)
        {
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) != Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new System.String[] { Manifest.Permission.Camera, Manifest.Permission.Flashlight }, 0);
                return;
            }

            //RunOnUiThread(() =>
            //{

            //});

            App.ContinueScanning = true;
            App.IsScanningCancelled = false;

            #region Manatee Barcode Scanner
            var scanner = new Scanner(this);
            scanner.initDecoder();


            scanTimer = new Timer(scanTimeout);
            scanTimer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                App.ContinueScanning = false;
                scanTimer.Stop();
                try
                {
                    if (scanner != null)
                        scanner.closeScanner();
                }
                catch (System.Exception eex)
                {
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                }
                scanner = null;
            };
            scanTimer.Start();


            var result = await scanner.CustomScan(App.ActiveCamera == CameraType.FrontCamera ? Android.Hardware.CameraFacing.Front : Android.Hardware.CameraFacing.Back);
            scanTimer.Stop();

            try
            {
                if (scanner != null)
                    scanner.closeScanner();
            }
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }
            scanner = null;
            #endregion

            #region ZXing Barcode Scanner
            //ZXing.Result result;

            ////TODO: Uncomment here to have real scan
            //#region Real scan
            //var scanner = new MobileBarcodeScanner
            //{
            //    UseCustomOverlay = false,//We can customize the top and bottom text of the default overlay
            //    TopText = App.TextProvider.GetText(2514), //"Hold the camera up to the barcode\nabout 6 inches away";
            //    BottomText = App.TextProvider.GetText(2515), //"Wait for the barcode to automatically scan!";
            //};

            ////focusTimer = new Timer(focusInterval);
            ////focusTimer.Elapsed += (object sender, ElapsedEventArgs e) =>
            ////{
            ////    scanner.AutoFocus();
            ////};

            //scanTimer = new Timer(scanTimeout);
            //scanTimer.Elapsed += (object sender, ElapsedEventArgs e) =>
            //{
            //    App.ContinueScanning = false;
            //    scanTimer.Stop();
            //    //focusTimer.Stop();
            //    scanner.Cancel();
            //};
            //scanTimer.Start();
            ////focusTimer.Start();


            ////TODO: We need High-Resolution cameras on both sides. (Very Important!)
            ////TODO: Back camera must have higher resolution than 8MP with auto-focus feature
            ////TODO: Front camera must have higher resolution than 5MP (without supporting 320p preview - Like Sony XPeria)
            ////Start scanning
            //scanner.AutoFocus();
            //result = await scanner.Scan(new MobileBarcodeScanningOptions
            //{
            //    PossibleFormats = new System.Collections.Generic.List<BarcodeFormat>{
            //        BarcodeFormat.AZTEC,
            //        BarcodeFormat.PDF_417,
            //        BarcodeFormat.All_1D
            //    },
            //    TryHarder = false,
            //    InitialDelayBeforeAnalyzingFrames = 0,
            //    DelayBetweenAnalyzingFrames = 0,
            //    DelayBetweenContinuousScans = 0,
            //    PureBarcode = true,
            //    AutoRotate = true,// (for front cam) it does nothing
            //    TryInverted = true,// it does nothing
            //    UseFrontCameraIfAvailable = App.UseFrontCamera,
            //    UseNativeScanning = true,
            //    CameraResolutionSelector = availableResolutions =>
            //    {
            //        var sorted = availableResolutions.OrderBy(x => x.Width).ThenBy(y => y.Height).ToList();
            //        if (App.UseFrontCamera) return sorted[sorted.Count - 1]; //sorted.Count - (sorted.Count/3)];//Best Resolution
            //        return sorted[sorted.Count / 3];//[0];//TODO: Test it with the business target mobiles // mid-low resolution
            //    }
            //});
            //scanTimer.Stop();
            //try
            //{
            //    scanner.Cancel();
            //}
            //catch { }
            //scanner = null;
            ////focusTimer.Stop();
            //#endregion
            #endregion

            #region Mock Scan
            //result = new ZXing.Result("abc",
            //                          new byte[] { 65, 66, 67 },
            //                          new ResultPoint[] { },
            //                          BarcodeFormat.PDF_417);
            //System.Threading.Thread.Sleep(2000);
            #endregion

            HandleScanResult(result, isSecondScan);
        }

        private void HandleScanResult(ScannerResult result, bool wasSecondScan = false)
        {
            if (result != null && !string.IsNullOrEmpty(result.code))
            {
                try
                {
                    if (toneGenerator != null)
                        toneGenerator.StartTone(Tone.PropAck);
                    if (vibrator != null)
                        vibrator.Vibrate(300);
                }
                catch (System.Exception eex)
                {
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                }

                //App.ContinueScanning = false;
                //App.IsScanningCancelled = true;


                if (App.ManualInputForm != null)
                {
                    try
                    {
                        App.ManualInputForm.Close();
                        //App.ManualInputForm.Dispose();
                    }
                    catch (System.Exception eex)
                    {
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                    }
                    App.ManualInputForm = null;
                }


                var resultActivity = new Intent(this, typeof(ResultActivity));
                resultActivity.PutExtra("barcode", result.code);
                resultActivity.PutExtra("isSecondScan", wasSecondScan.ToString());
                StartActivity(resultActivity);
            }
            else
            {
                App.ContinueScanning = false;
                App.IsScanningCancelled = true;
            }
        }

        #region ZXing handler
        //private void HandleScanResult(ZXing.Result result)
        //{
        //    if (result != null && !string.IsNullOrEmpty(result.Text))
        //    {
        //        try
        //        {
        //            if (toneGenerator != null)
        //                toneGenerator.StartTone(Tone.PropAck);
        //            if (vibrator != null)
        //                vibrator.Vibrate(300);
        //        }
        //        catch { }

        //        var resultActivity = new Intent(this, typeof(ResultActivity));
        //        resultActivity.PutExtra("barcode", result.Text);
        //        StartActivity(resultActivity);
        //    }
        //    else
        //    {
        //        App.ContinueScanning = false;
        //    }
        //}
        #endregion

        private void BeginSearch()
        {
            ShowLoading(App.TextProvider.GetText(2508));
            airlinesAdapter.SearchAirlines(this.txtSearch.Text);
        }

        private void setTopMargin(View view, int topMarginInDips)
        {
            ViewGroup.MarginLayoutParams layoutParams =
                (ViewGroup.MarginLayoutParams)view.LayoutParameters;
            layoutParams.TopMargin = topMarginInDips;//base.ConvertDpToPixels(topMarginInDips);
            view.RequestLayout();
        }

        private void setBottomMargin(View view, int bottomMarginInDips)
        {
            ViewGroup.MarginLayoutParams layoutParams =
                (ViewGroup.MarginLayoutParams)view.LayoutParameters;
            layoutParams.BottomMargin = bottomMarginInDips;//base.ConvertDpToPixels(topMarginInDips);
            view.RequestLayout();
        }

        private TranslateAnimation makeMarginAnimation(int fromMargin, int toMargin)
        {
            TranslateAnimation animation =
                new TranslateAnimation(0, 0, 0, toMargin - fromMargin);
            animation.Duration = 500;
            animation.AnimationEnd += (object sender, Animation.AnimationEndEventArgs e) =>
            {
                pnlGrdAirlines.ClearAnimation();
                setTopMargin(pnlGrdAirlines, toMargin);
            };
            return animation;
        }

        void AgentSwipeMenuToggle()
        {
            try
            {
                if (swipeMenu.IsShown)
                {
                    swipeMenu.Visibility = ViewStates.Gone;
                    swipeMenu.Animation = new TranslateAnimation(0f, 0f, 0f, swipeMenu.MeasuredHeight);
                    swipeMenu.Animation.Duration = 300;
                    btnMenuLeft.SetImageResource(Resource.Drawable.btn_menu_open2);
                    btnMenuRight.SetImageResource(Resource.Drawable.btn_menu_open2);
                    //				btnMenuLeft.Text = "Menu";
                    //				btnMenuRight.Text = "Menu";
                    //btnDescExpander.SetImageResource (Resource.Drawable.airline_a3); 
                }
                else
                {
                    swipeMenu.Visibility = ViewStates.Visible;
                    swipeMenu.RequestFocus();
                    swipeMenu.Animation = new TranslateAnimation(0f, 0f, swipeMenu.MeasuredHeight, 0f);
                    swipeMenu.Animation.Duration = 300;
                    btnMenuLeft.SetImageResource(Resource.Drawable.btn_menu_close2);
                    btnMenuRight.SetImageResource(Resource.Drawable.btn_menu_close2);
                    //				btnMenuLeft.Text = "Close";
                    //				btnMenuRight.Text = "Close";
                    //btnDescExpander.SetImageResource (Resource.Drawable.airline_ac);
                }
            }
            catch (Exception ex)
            {
                AIMSMessage("Error opening agent menu", ex.Message);

                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }
        }

        void SearchPanelToggle()
        {
            if (searchPanelVisible)
            {//Hide
                searchPanelVisible = false;

                //pnlSearch.Visibility = ViewStates.Gone; 
                //btnSearchToggle.Visibility = ViewStates.Visible;
                pnlGrdAirlines.Animation = makeMarginAnimation(searchPanelMeasuredHeight, 0);
                setBottomMargin(pnlGrdAirlines, 0);// -searchPanelMeasuredHeight);
                pnlSearch.Animation = AnimationUtils.LoadAnimation(this, Resource.Animation.search_fadeout);
                pnlSearch.Animation.AnimationEnd += (object sender, Animation.AnimationEndEventArgs e) =>
                {
                    pnlSearch.Visibility = ViewStates.Gone;
                    //pnlMainPageTitle.SetBackgroundResource(Resource.Drawable.bg_trans_grad_top);
                    ShowLoading(App.TextProvider.GetText(2504));
                    airlinesAdapter.LoadAirlines();//resetting airlines
                    this.txtSearch.Text = "";
                };

                //pnlSearch.Animation = new TranslateAnimation (0f, 0f, 0f, -pnlSearch.MeasuredHeight);
                //pnlSearch.Animation.Duration = 350;

                btnSearchToggle.Visibility = ViewStates.Visible;
                btnSearchToggle.Animation = AnimationUtils.LoadAnimation(this, Resource.Animation.login_fadein);

                //				grdAirlines.Animation = new TranslateAnimation (0f, 0f, 0f, -pnlSearch.MeasuredHeight);
                //				grdAirlines.Animation.Duration = 500;

            }
            else
            {//Show
                searchPanelVisible = true;

                //pnlMainPageTitle.SetBackgroundResource(Resource.Color.bg_dark_trans);

                //pnlSearch.Visibility = ViewStates.Visible;
                //btnSearchToggle.Visibility = ViewStates.Gone;
                pnlSearch.Visibility = ViewStates.Visible;
                pnlSearch.RequestFocus();
                pnlSearch.Animation = AnimationUtils.LoadAnimation(this, Resource.Animation.search_fadein);
                pnlSearch.Animation.AnimationEnd += (object sender, Animation.AnimationEndEventArgs ev) =>
                {
                    searchPanelMeasuredHeight = pnlSearch.MeasuredHeight;
                    pnlGrdAirlines.Animation = makeMarginAnimation(0, searchPanelMeasuredHeight);
                    pnlGrdAirlines.Animation.AnimationEnd += (object s, Animation.AnimationEndEventArgs e) =>
                    {
                        setBottomMargin(pnlGrdAirlines, 0);
                    };
                };

                //pnlSearch.Animation = new TranslateAnimation (0f, 0f, -pnlSearch.MeasuredHeight, 0f);
                //pnlSearch.Animation.Duration = 350;

                btnSearchToggle.Animation = AnimationUtils.LoadAnimation(this, Resource.Animation.login_fadeout);
                btnSearchToggle.Animation.AnimationEnd += (object sender, Animation.AnimationEndEventArgs e) =>
                {
                    btnSearchToggle.Visibility = ViewStates.Gone;
                };

                //				grdAirlines.Animation = new TranslateAnimation (0f, 0f, -pnlSearch.MeasuredHeight, 0f);
                //				grdAirlines.Animation.Duration = 500;

            }
        }

        Animation inFromRightAnimation()
        {
            Animation inFromRight = new TranslateAnimation(
                Android.Views.Animations.Dimension.RelativeToParent, +1.0f,
                Android.Views.Animations.Dimension.RelativeToParent, 0.0f,
                Android.Views.Animations.Dimension.RelativeToParent, 0.0f,
                Android.Views.Animations.Dimension.RelativeToParent, 0.0f);
            inFromRight.Duration = 240;
            //inFromRight.SetInterpolator(new AccelerateInterpolator());
            return inFromRight;
        }

        Animation outToRightAnimation()
        {
            Animation outtoLeft = new TranslateAnimation(
                Android.Views.Animations.Dimension.RelativeToParent, -1.0f,
                Android.Views.Animations.Dimension.RelativeToParent, 0.0f,
                Android.Views.Animations.Dimension.RelativeToParent, 0.0f,
                Android.Views.Animations.Dimension.RelativeToParent, 0.0f);
            outtoLeft.Duration = 240;
            //outtoLeft.SetInterpolator(new AccelerateInterpolator());
            return outtoLeft;
        }

        //void GoToNextTab(){
        //	if (tabHost.CurrentTab < TabsCount)
        //		tabHost.CurrentTab++;
        //	else
        //		tabHost.CurrentTab = 0;
        //}

        //void GoToPreviousTab(){
        //	if (tabHost.CurrentTab > 0)
        //		tabHost.CurrentTab--;
        //	else
        //		tabHost.CurrentTab = TabsCount - 1;
        //}

        //void CreateTab(Type activityType, string tag, string label, int drawableId )
        //{
        //	var intent = new Intent(this, activityType);
        //	intent.AddFlags(ActivityFlags.NewTask);

        //	var spec = tabHost.NewTabSpec(tag);
        //	var drawableIcon = Resources.GetDrawable(drawableId);
        //	spec.SetIndicator(label, drawableIcon);
        //	spec.SetContent(intent);

        //	tabHost.AddTab(spec);
        //	TabsCount++;
        //}

        //void RefreshTime()
        //{
        //    lblMainPageTime.Text = string.Format("[{0}]", DateTime.Now.ToShortTimeString());
        //}

        void InitTabsArray()
        {
            if (App.IsDeveloperModeEnabled)
            {
                FRAGMENTS = AIMSApp.IsDemoVersion ? new Android.Support.V4.App.Fragment[]{
                    new AgentProfileActivity(),
                    new AgentMessagesActivity(),
                    new AgentPassengersActivity(),//TODO: Add failed passengers tab and image
                    new AgentLogsActivity(),
                    new AgentDevLogsActivity(),
                    new AgentStatisticsActivity(),
                    new AdvancedSettingsActivity(),
                } : new Android.Support.V4.App.Fragment[]{
                    new AgentProfileActivity(),
                    new AgentMessagesActivity(),
                    new AgentPassengersActivity(),
                    new AgentLogsActivity(),
                    new AgentDevLogsActivity(),
                    new AgentStatisticsActivity(),
                    new AdvancedSettingsActivity(),
                };

                CONTENT = AIMSApp.IsDemoVersion ? new string[] {
                    LanguageRepository.Current.GetText(2009)/*"Home"*/,
                    LanguageRepository.Current.GetText(2041)/*"Messages"*/,
                    LanguageRepository.Current.GetText(2008)/*"Last Hour"*/, //2010
                    LanguageRepository.Current.GetText(2040)/*"Logs"*/,
                    "Dev Logs",
                    LanguageRepository.Current.GetText(2011), /*, "Stats"*/
                    LanguageRepository.Current.GetText(2050)/*"Settings"*/,
                } :
                    new string[] {
                        LanguageRepository.Current.GetText(2009)/*"Home"*/,
                        LanguageRepository.Current.GetText(2041)/*"Messages"*/,
                        LanguageRepository.Current.GetText(2008)/*"Last Hour"*/, //2010
                        LanguageRepository.Current.GetText(2040)/*"Logs"*/,
                        "Dev Logs",
                        LanguageRepository.Current.GetText(2011), /*, "Stats"*/
                        LanguageRepository.Current.GetText(2050)/*"Settings"*/, };

                ImageResourceIds = AIMSApp.IsDemoVersion ? new int[] {
                    Resource.Drawable.tab_home_off_light,
                    Resource.Drawable.tab_message_off_light,
                    Resource.Drawable.tab_passengers_off_light,
                    Resource.Drawable.tab_logs_off_light,
                    Resource.Drawable.tab_logs_off_light,//TODO: Create picture for developer logs
                    Resource.Drawable.tab_stats_off_light,
                    Resource.Drawable.tab_settings_off_light,
                } :
                    new int[] {
                        Resource.Drawable.tab_home_off_light,
                        Resource.Drawable.tab_message_off_light,
                        Resource.Drawable.tab_passengers_off_light,
                        Resource.Drawable.tab_logs_off_light,
                        Resource.Drawable.tab_logs_off_light,
                        Resource.Drawable.tab_stats_off_light,
                        Resource.Drawable.tab_settings_off_light,
                    };

                ImageResourceIds_On = AIMSApp.IsDemoVersion ? new int[] {
                    Resource.Drawable.tab_home_off,
                    Resource.Drawable.tab_message_off,
                    Resource.Drawable.tab_passengers_off,
                    Resource.Drawable.tab_logs_off,
                    Resource.Drawable.tab_logs_off,//TODO: Create picture for developer logs
                    Resource.Drawable.tab_stats_off,
                    Resource.Drawable.tab_settings_off,
                } :
                    new int[] {
                        Resource.Drawable.tab_home_off,
                        Resource.Drawable.tab_message_off,
                        Resource.Drawable.tab_passengers_off,
                        Resource.Drawable.tab_logs_off,
                        Resource.Drawable.tab_logs_off,
                        Resource.Drawable.tab_stats_off,
                        Resource.Drawable.tab_settings_off,
                    };
            }
            else
            {
                FRAGMENTS = AIMSApp.IsDemoVersion ? new Android.Support.V4.App.Fragment[]{
                    new AgentProfileActivity(),
                    new AgentMessagesActivity(),
                    new AgentPassengersActivity(),//TODO: Add failed passengers tab and image
                    new AgentStatisticsActivity(),
                    new AgentLogsActivity(),
                    //new AgentDevLogsActivity(),
                    //new AgentStatsActivity()//,
                    //new AdvancedSettingsActivity(),
                } : new Android.Support.V4.App.Fragment[]{
                    new AgentProfileActivity(),
                    new AgentMessagesActivity(),
                    new AgentPassengersActivity(),
                    new AgentStatisticsActivity(),
                    new AgentLogsActivity(),
                    //new AgentDevLogsActivity(),
                    //new AgentStatsActivity()//,
                    //new AdvancedSettingsActivity(),
		        };

                CONTENT = AIMSApp.IsDemoVersion ? new string[] {
                    LanguageRepository.Current.GetText(2009)/*"Home"*/,
                    LanguageRepository.Current.GetText(2041)/*"Messages"*/,
                    LanguageRepository.Current.GetText(2008)/*"Last Hour"*/,
                    LanguageRepository.Current.GetText(2011)/*"Statistics"*/,
                    LanguageRepository.Current.GetText(2040)/*"Logs"*/,
                    //"Dev Logs",
                    /*, "Stats"*/
                    //LanguageProvider.Current.GetText(2046)/*"Advanced Settings"*/,
                } :
                    new string[] {
                        LanguageRepository.Current.GetText(2009)/*"Home"*/,
                        LanguageRepository.Current.GetText(2041)/*"Messages"*/,
                        LanguageRepository.Current.GetText(2008)/*"Last Hour"*/,
                        LanguageRepository.Current.GetText(2011)/*"Statistics"*/,
                        LanguageRepository.Current.GetText(2040)/*"Logs"*/,
                        //"Dev Logs",
                        /*, "Stats"*/
                        //LanguageProvider.Current.GetText(2046)/*"Advanced Settings"*/,
                    };

                ImageResourceIds = AIMSApp.IsDemoVersion ? new int[] {
                    Resource.Drawable.tab_home_off_light,
                    Resource.Drawable.tab_message_off_light,
                    Resource.Drawable.tab_passengers_off_light,
                    Resource.Drawable.tab_stats_off_light,
                    Resource.Drawable.tab_logs_off_light,
                    //Resource.Drawable.tab_logs_off_light,//TODO: Create picture for developer logs
                    //Resource.Drawable.tab_stats_off_light,
                    //Resource.Drawable.tab_settings_off_light,
                } :
                    new int[] {
                        Resource.Drawable.tab_home_off_light,
                        Resource.Drawable.tab_message_off_light,
                        Resource.Drawable.tab_passengers_off_light,
                        Resource.Drawable.tab_stats_off_light,
                        Resource.Drawable.tab_logs_off_light,
                        //Resource.Drawable.tab_logs_off_light,
                        //Resource.Drawable.tab_stats_off_light,
                        //Resource.Drawable.tab_settings_off_light,
                    };

                ImageResourceIds_On = AIMSApp.IsDemoVersion ? new int[] {
                    Resource.Drawable.tab_home_off,
                    Resource.Drawable.tab_message_off,
                    Resource.Drawable.tab_passengers_off,
                    Resource.Drawable.tab_stats_off_light,
                    Resource.Drawable.tab_logs_off,
                    //Resource.Drawable.tab_logs_off,//TODO: Create picture for developer logs
                    //Resource.Drawable.tab_stats_off,
                    //Resource.Drawable.tab_settings_off,
                } :
                    new int[] {
                        Resource.Drawable.tab_home_off,
                        Resource.Drawable.tab_message_off,
                        Resource.Drawable.tab_passengers_off,
                        Resource.Drawable.tab_stats_off_light,
                        Resource.Drawable.tab_logs_off,
                        //Resource.Drawable.tab_logs_off,
                        //Resource.Drawable.tab_stats_off,
                        //Resource.Drawable.tab_settings_off,
                    };
            }
            if (mPager != null && mPager.Adapter != null)
                mPager.Adapter.NotifyDataSetChanged();
        }

        public void RefreshTabs()
        {
            InitTabsArray();

            mIndicator.SetViewPager(mPager);
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
            txtSearch.Hint = string.Format("{0}...", App.TextProvider.GetText(2008));//Search...
            //if (!btnScan.Text.Equals("License Error"))
            //    btnScan.Text = App.TextProvider.GetText(2007);//Scan

            RefreshTabs();
        }
        #endregion

        #region Events
        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                #region Initials
                base.OnCreate(bundle);
                ShowLoading(App.TextProvider.GetText(2504));//"Retrieving list of airlines"
                SetContentView(Resource.Layout.Main);

                if (MembershipProvider.Current.LoggedinUser == null)
                    OnBackPressed();

                Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjM0OTQ4QDMxMzgyZTMxMmUzMGIxak5ycnNJQldGYW8yeWRhYXRoQjY5VzQ3MUVyQ3dJOGlpaElVYjhXQnM9");

                //pnlMainPageTitle = FindViewById<RelativeLayout>(Resource.Id.pnlMainPageTitle);
                lblMainPageTitle = FindViewById<TextView>(Resource.Id.lblMainPageTitle);
                imgMainPageTitle = FindViewById<ImageView>(Resource.Id.imgMainPageTitle);
                //lblMainPageTime = FindViewById<TextView>(Resource.Id.lblMainPageTime);

                Title = lblMainPageTitle.Text = MembershipProvider.Current.UserHeaderText;

                #endregion

                //global::Xamarin.Forms.Forms.Init (this, bundle);
                //LoadApplication (new App ());

                App.MainForm = this;

                #region LocalTime
                //RefreshTime();
                //clockTimer = new Timer(30000);
                //clockTimer.Elapsed += (object sender, ElapsedEventArgs e) =>
                //{
                //    RunOnUiThread(() =>
                //    {
                //        RefreshTime();
                //    });
                //};
                //clockTimer.Start();
                #endregion

                #region Gesture Settings
                gestureListener = new GestureListener(this);
                //gestureListener.OnLeftToRight += GestureLeft;
                //gestureListener.OnRightToLeft += GestureRight; 
                gestureListener.OnTopToBottom += GestureTop;
                gestureListener.OnBottomToTop += GestureBottom;
                //gestureListener.OnSingleTap += SingleTap;  
                gestureDetector = new GestureDetector(this, gestureListener);
                #endregion

                #region Search Panel
                pnlSearch = FindViewById<RelativeLayout>(Resource.Id.pnlSearch);
                txtSearch = FindViewById<AutoCompleteTextView>(Resource.Id.txtSearch);
                btnSearch = FindViewById<ImageButton>(Resource.Id.btnSearch);

                btnSearch.Click += delegate (object sender, EventArgs e)
                {
                    BeginSearch();
                };

                btnCloseSearch = FindViewById<ImageButton>(Resource.Id.btnCloseSearch);
                btnCloseSearch.Click += delegate (object sender, EventArgs e)
                {
                    SearchPanelToggle();
                };
                btnSearchToggle = FindViewById<ImageButton>(Resource.Id.btnSearchToggle);
                btnSearchToggle.Click += delegate (object sender, EventArgs e)
                {
                    SearchPanelToggle();
                };
                #endregion

                #region Agent's Swipe Menu
                InitTabsArray();

                swipeMenu = FindViewById<LinearLayout>(Resource.Id.swipeMenu);

                mAdapter = new AgentDetailAdapter(SupportFragmentManager);

                mPager = FindViewById<ViewPager>(Resource.Id.agentPager);

                mPager.Adapter = mAdapter;


                mIndicator = FindViewById<TabPageIndicator>(Resource.Id.agentIndicator);
                //mIndicator.SetViewPager(mPager);//it is done in LoadUITexts()

                btnMenuLeft = FindViewById<ImageButton>(Resource.Id.btnSwipeMenuLeft);
                btnMenuLeft.Click += delegate (object sender, EventArgs e)
                {
                    AgentSwipeMenuToggle();
                };

                btnMenuRight = FindViewById<ImageButton>(Resource.Id.btnSwipeMenuRight);
                btnMenuRight.Click += delegate (object sender, EventArgs e)
                {
                    AgentSwipeMenuToggle();
                };
                #endregion

                #region Sound and Vibration
                try
                {
                    vibrator = (Vibrator)GetSystemService(VibratorService);
                    toneGenerator = new ToneGenerator(Stream.Notification, 100);
                }
                catch { }
                #endregion

                #region Scan Button
                //ZXing
                //MobileBarcodeScanner.Initialize(Application);
                btnScan = FindViewById<ImageButton>(Resource.Id.btnScan);

                try
                {
                    //Manatee
                    //The dlls are loaded from (C:\Users\Siavash\Documents\Project Samples\Manatee - Xamarin-Universal\Android\MWBarcodeScanner)

                    if (MembershipProvider.Current.BarcodeScannerAPIKeys != null)
                    {
                        var androidSerialNumbers = MembershipProvider.Current.BarcodeScannerAPIKeys.Where(x => x.OS == PlatformType.Android);
                        foreach (var sn in androidSerialNumbers)
                        {
                            BarcodeConfig.MWB_registerCode(sn.CodeMaskType, sn.UserName, sn.SerialNumber);
                        }
                    }
                    BarcodeConfig.initDecoder();

                    //Create a new instance of our Scanner
                    btnScan.Click += delegate
                    {
                        HideLoading();

                        App.ContinueScanning = true;//When agent wants to start the continuous scanning
                        Scan();
                    };
                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Exception in initiating the Barcode scanner", ex);
                    AIMSMessage("Error in Barcode scanner", ex.Message);
                    //HideLoading();
                    // btnScan.Text = "License Error";

                    Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                }
                #endregion

                #region Airlines and Cards + imageRepository
                //App.ImageRepository = new ImageRepository(new WebApiServiceContext());//TODO: it killed me well

                //TODO: change every where
                pnlGrdAirlines = FindViewById<LinearLayout>(Resource.Id.pnlGrdAirlines);
                grdAirlines = FindViewById<GridView>(Resource.Id.grdAirlines);
                grdAirlines.ItemClick += GrdAirlines_ItemClick;
                grdAirlines2 = FindViewById<GridView>(Resource.Id.grdAirlines2);

                separatorLine = FindViewById<View>(Resource.Id.separatorLine);

                grdAirlines2.ItemClick += GrdAirlines2_ItemClick;

                airlinesAdapter = new AirlinesAdapter(this);


                airlinesAdapter.AdapterChanged += (notUsed) =>
                {
                    if (MembershipProvider.Current == null) return;
                    RunOnUiThread(new Action(() =>
                    {
                        grdAirlines.Adapter = airlinesAdapter;
                        grdAirlines2.Adapter = airlinesAdapter.GetSecondSectionAdapter();

                        if (grdAirlines2.Adapter.Count > 0)
                            separatorLine.Visibility = ViewStates.Visible;
                        else
                            separatorLine.Visibility = ViewStates.Gone;

                        if (autoCompleteAdapter == null)
                        {//do it once
                            autoCompleteAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleDropDownItem1Line, airlinesAdapter.GetAutoCompleteOptions());
                            txtSearch.Adapter = autoCompleteAdapter;
                            txtSearch.ItemClick += txtSearch_ItemClicked;
                        }

                        LoadLoungeImage();

                    }));

                    HideLoading();
                };

                airlinesAdapter.LoadAirlines(true);

                #endregion

                occupancyProgress = FindViewById<ProgressBar>(Resource.Id.occupancyProgressBar);
                occupancyProgress.SetBackgroundColor(Android.Graphics.Color.LightGray);
                progressBarinsideText = FindViewById<TextView>(Resource.Id.progressBarinsideText);
                App.OccupancyPercentageChanged += OccupancyPercentageChanged;
                App.OnRefreshEvent(null, null);

                //occupancyProgress.Touch -= OccupancyProgress_Touch;
                //occupancyProgress.Touch += OccupancyProgress_Touch;

                occupancyProgress.Click += OccupancyProgress_Click;


                LoadUITexts(App.TextProvider.SelectedLanguage);
            }
            catch (Exception ex)
            {
                AIMSMessage("Error", ex.Message);
                HideLoading();
                LogsRepository.AddError("Exception in loading main screen", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }
        }

        private void OccupancyProgress_Click(object sender, EventArgs e)
        {
            StartActivity(new Intent(this, typeof(PopupActivity)));
        }

        private void OccupancyProgress_Touch(object sender, View.TouchEventArgs e)
        {
            StartActivity(new Intent(this, typeof(PopupActivity)));
        }

        private void LoadLoungeImage()
        {
            string pathLoungeImage = string.Format(AIMSApp.AIMS_LOUNGE_IMAGE_NAME, MembershipProvider.Current.SelectedLounge.LoungeID);
            App.ImageRepository.LoadImage(pathLoungeImage, (imageResult) =>
            {
                if (imageResult == null || imageResult.ReturnParam == null || imageResult.ReturnParam.Length == 0)
                {
                    var airline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
                    if (airline != null)
                    {
                        if (airline.AirlineLogoBytes != null && airline.AirlineLogoBytes.Length > 0)
                        {
                            try
                            {
                                var bm = BitmapFactory.DecodeByteArray(airline.AirlineLogoBytes, 0, airline.AirlineLogoBytes.Length);
                                if (bm != null)
                                {
                                    bm = ImageAdapter.HighlightImage(bm);

                                    AIMSApp.LoungeBitmapImage = bm;

                                    imgMainPageTitle.SetImageBitmap(bm);
                                }
                            }
                            catch (System.Exception eex)
                            {
                                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                            }
                        }
                        else
                        {
                            var item = (new JavaAirline(airline));
                            App.ImageRepository.LoadImage(item.ImageHandle, (imageResultImg) =>
                            {
                                if (imageResultImg == null)
                                {
                                    LogsRepository.AddError("Error in MainActivity in loading airline image", "result is null");

                                    Analytics.TrackEvent("MainActivity", new Dictionary<string, string>
                                    {
                                        { "Error in MainActivity in loading airline image", "result is nul" }
                                    });

                                    return;
                                }

                                if (imageResultImg.IsSucceeded)
                                {
                                    var bytes = imageResultImg.ReturnParam;

                                    if (bytes == null || bytes.Length == 0) return;

                                    try
                                    {
                                        var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                                        if (bm != null)
                                        {
                                            bm = ImageAdapter.HighlightImage(bm);
                                            AIMSApp.LoungeBitmapImage = bm;

                                            this.RunOnUiThread(() =>
                                            {
                                                imgMainPageTitle.SetImageBitmap(bm);
                                            });
                                        }
                                    }
                                    catch (System.Exception eex)
                                    {
                                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                                    }
                                }
                            });
                        }
                    }
                }

                else if (imageResult.IsSucceeded)
                {
                    var bytes = imageResult.ReturnParam;

                    if (bytes == null || bytes.Length == 0) return;

                    try
                    {
                        var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                        if (bm != null)
                        {
                            bm = ImageAdapter.HighlightImage(bm);
                            AIMSApp.LoungeBitmapImage = bm;

                            this.RunOnUiThread(() =>
                            {
                                imgMainPageTitle.SetImageBitmap(bm);
                            });
                        }
                    }
                    catch (System.Exception eex)
                    {
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                    }
                }
            }, true);
        }

        private void OccupancyPercentageChanged()
        {
            RunOnUiThread(() =>
            {
                //occupancyProgress.SetProgress(MembershipProvider.Current.SelectedLounge.LastPercentage, true);

#if DEBUG
                MembershipProvider.Current.SelectedLounge.LastPercentage = DateTime.Now.Minute;
#endif


                progressBarinsideText.Text = $"{ MembershipProvider.Current.SelectedLounge.LastPercentage.ToString("N0")}%";

                occupancyProgress.ProgressDrawable.SetColorFilter(Util.GetProgressColor(MembershipProvider.Current.SelectedLounge.LastPercentage),
                                                                  Android.Graphics.PorterDuff.Mode.SrcIn);

                ObjectAnimator animation = ObjectAnimator.OfInt(occupancyProgress, "progress", occupancyProgress.Progress,
                                           MembershipProvider.Current.SelectedLounge.LastPercentage);
                animation.SetDuration(1000);
                animation.SetInterpolator(new DecelerateInterpolator());
                animation.Start();
            });
        }

        private void txtSearch_ItemClicked(object sender, AdapterView.ItemClickEventArgs e)
        {
            ShowLoading(App.TextProvider.GetText(2508));// "Searching the airlines");//TODO: Check translations and add to EXCEL file
            var airline = autoCompleteAdapter.GetItem(e.Position);
            if (airline == null) return;
            airlinesAdapter.SearchAirlines(airline.ToString());
        }

        private void GrdAirlines_ItemClick(object sender, AdapterView.ItemClickEventArgs args)
        {
            var airline = (JavaAirline)grdAirlines.Adapter.GetItem(args.Position);
            if (airline != null)
            {
                MembershipProvider.Current.SelectedAirline = airline;
                StartActivity(new Intent(this, typeof(CardSelectionActivity)));
            }
        }

        private void GrdAirlines2_ItemClick(object sender, AdapterView.ItemClickEventArgs args)
        {
            var airline = (JavaAirline)grdAirlines2.Adapter.GetItem(args.Position);
            if (airline != null)
            {
                MembershipProvider.Current.SelectedAirline = airline;
                StartActivity(new Intent(this, typeof(CardSelectionActivity)));
            }
        }

        public override void OnBackPressed()
        {
            if (IsAgentMenuOpen)
                AgentSwipeMenuToggle();
            else if (MembershipProvider.Current.UserLounges.Count() == 1)
                Process.KillProcess(Process.MyPid());
            else
                base.OnBackPressed();
        }

        public override bool DispatchTouchEvent(MotionEvent ev)
        {
            gestureDetector.OnTouchEvent(ev);
            return base.DispatchTouchEvent(ev);
        }

        void GestureBottom()
        {
            if (!swipeMenu.IsShown)
                AgentSwipeMenuToggle();
            //isSingleTapFired = false; 
        }

        void GestureTop()
        {
            if (swipeMenu.IsShown)
                AgentSwipeMenuToggle();
            //isSingleTapFired = false; 
        }

        protected override void OnResume()
        {
            #region Sia - Testing [TODO: remove this]
            //MembershipProvider.Current.PushUserLogs(new System.Collections.Generic.List<DAL.Log> {
            //    new DAL.Log {
            //        LogLevel = DAL.LogLevel.Client,
            //        LogType = DAL.LogType.Error,
            //        Time = DateTime.Now,
            //        Title = "Title 1",
            //        Description= "Desc 1",
            //    },
            //    new DAL.Log {
            //        LogLevel = DAL.LogLevel.Developer,
            //        LogType = DAL.LogType.Warning,
            //        Time = DateTime.Now,
            //        Title = "Title 2",
            //        Description= "Desc 2",
            //    }
            //}, (sender, e) =>
            //{
            //    if (e)
            //        LongToast("Done");
            //    else
            //        LongToast("Failed");
            //});
            #endregion

            base.OnResume();
            if (App.ContinueScanning)
            {
                //App.ContinueScanning = false;
                Scan();
            }
        }

        #endregion

        #region Agent Menu TabAdapter
        public class AgentDetailAdapter : FragmentPagerAdapter, TitleProvider
        {
            public AgentDetailAdapter(Android.Support.V4.App.FragmentManager fm) : base(fm)
            {

            }

            public override Android.Support.V4.App.Fragment GetItem(int position)
            {
                return MainActivity.FRAGMENTS[position % MainActivity.FRAGMENTS.Length];
            }

            public override int Count
            {
                get
                {
                    return MainActivity.FRAGMENTS.Length;
                }
            }

            public string GetTitle(int position)
            {
                return MainActivity.CONTENT[position % MainActivity.CONTENT.Length];
            }

            public int GetImageResourceId(int position)
            {
                return MainActivity.ImageResourceIds[position % MainActivity.ImageResourceIds.Length];
            }

            public int GetSecondStateImageResourceId(int position)
            {
                return MainActivity.ImageResourceIds_On[position % MainActivity.ImageResourceIds_On.Length];
            }
        }
        #endregion
    }
}

