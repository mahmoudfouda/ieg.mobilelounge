using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    public abstract class BaseFragment : Android.Support.V4.App.Fragment, ILoadable
    {
        #region Properties
        //protected LanguageProvider TextProvider { get; private set; }
        public AIMSApp App { get; private set; }
        //protected GPSAdapter GPS { get; private set; }
        #endregion

        #region Fields
        ProgressDialog progressDialog;
        #endregion

        #region Constructors
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            App = (AIMSApp)this.Activity.Application;
            //if (App.GPS == null)//it happens in splash screen loading time//TODO: remove if nothing happened
            //    App.GPS = new GPSAdapter(this.Activity);

            //TextProvider = LanguageProvider.Current;//it happens in splash screen loading time
            App.TextProvider.OnLanguageChanged += LoadUITexts;//TODO: Check if it happens once!
        }
        #endregion

        #region Methods
        public void LongToast(string message)
        {
            this.Activity.RunOnUiThread(() => Toast.MakeText(this.Activity, message, ToastLength.Long).Show());
        }

        public void ShortToast(string message)
        {
            this.Activity.RunOnUiThread(() => Toast.MakeText(this.Activity, message, ToastLength.Short).Show());
        }

        public void AIMSMessage(string title, string text)
        {
            this.Activity.RunOnUiThread(() =>
            {
                var intent = new Intent(this.Activity, typeof(AIMSMessageActivity));
                intent.PutExtra("title", title);
                intent.PutExtra("text", text);
                StartActivity(intent);
            });
        }

        public void AIMSConfirm(string title, string text, EventHandler okCallback = null, EventHandler cancelCallback = null)
        {
            this.Activity.RunOnUiThread(() => {
                var intent = new Intent(this.Activity, typeof(AIMSConfirmActivity));
                intent.PutExtra("title", title);
                intent.PutExtra("text", text);
                AIMSConfirmActivity.OkEvent = okCallback;
                AIMSConfirmActivity.CancelEvent = cancelCallback;
                StartActivity(intent);
            });
        }

        public void Dialog(string title, string text, EventHandler cancelCallback)
        {
            this.Activity.RunOnUiThread(() => { var progressDialog = ProgressDialog.Show(this.Activity, title, text, true, true, cancelCallback); });
        }

        public void DialogLock(string title, string text)
        {
            this.Activity.RunOnUiThread(() => { progressDialog = ProgressDialog.Show(this.Activity, title, text, true); });
        }

        public void HideDialog()
        {
            //if(progressDialog != null)
            //    this.Activity.RunOnUiThread(() => progressDialog.Hide());

            Acr.UserDialogs.UserDialogs.Instance.HideLoading();
        }

        public void HideLoading()
        {
            HideDialog();
        }

        public void ShowLoading(string text)
        {
            Acr.UserDialogs.UserDialogs.Instance.ShowLoading(text);

            //DialogLock(App.TextProvider.GetText(2001)/*"Loading..."*/, text);
        }

        public abstract void LoadUITexts(Language? selectedLanguage);
        #endregion
    }
}