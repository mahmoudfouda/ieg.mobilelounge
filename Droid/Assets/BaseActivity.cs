using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    public abstract class BaseActivity : Activity, ILoadable
    {
        #region Properties
        //protected LanguageProvider TextProvider { get; private set; }
        public AIMSApp App { get; private set; }
        //protected GPSAdapter GPS { get; private set; }
        #endregion

        #region Fields
        ProgressDialog progressDialog;
        #endregion

        #region Constructors
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            App = (AIMSApp)Application;
            //if (App.GPS == null)//it happens in splash screen loading time//TODO: remove if nothing happened
            //    App.GPS = new GPSAdapter(this);

            //App.TextProvider = LanguageProvider.Current;//it happens in splash screen loading time
            App.TextProvider.OnLanguageChanged += LoadUITexts;//TODO: Check if it happens once!
        }
        #endregion
        
        #region Methods
        public void LongToast(string message)
        {
            RunOnUiThread(() => Toast.MakeText(this,message,ToastLength.Long).Show());
        }

        public void ShortToast(string message)
        {
            RunOnUiThread(() => Toast.MakeText(this, message, ToastLength.Short).Show());
        }

        public void AIMSMessage(string title, string text)
        {
            RunOnUiThread(() =>
            {
                var intent = new Intent(this, typeof(AIMSMessageActivity));
                intent.PutExtra("title", title);
                intent.PutExtra("text", text);
                StartActivity(intent);
            });
        }

        public void AIMSConfirm(string title, string text, EventHandler okCallback = null, EventHandler cancelCallback = null)
        {
            RunOnUiThread(() => {
                var intent = new Intent(this, typeof(AIMSConfirmActivity));
                intent.PutExtra("title", title);
                intent.PutExtra("text", text);
                AIMSConfirmActivity.OkEvent = okCallback;
                AIMSConfirmActivity.CancelEvent = cancelCallback;
                StartActivity(intent);
            });
        }

        public void Dialog(string title, string text, EventHandler cancelCallback)
        {
            RunOnUiThread(() => { var progressDialog = ProgressDialog.Show(this, title, text, true, true, cancelCallback); });
        }

        public void DialogLock(string title, string text)
        {
            RunOnUiThread(() => { progressDialog = ProgressDialog.Show(this, title, text, true); });
        }

        public void HideDialog()
        {
            //if (progressDialog != null)
            //    RunOnUiThread(() => progressDialog.Hide());

            RunOnUiThread(() => Acr.UserDialogs.UserDialogs.Instance.HideLoading());
        }

        public void HideLoading()
        {
            HideDialog();
        }

        public void ShowLoading(string text)
        {
            RunOnUiThread(() => Acr.UserDialogs.UserDialogs.Instance.ShowLoading(text));

            //DialogLock(App.TextProvider.GetText(2001), text);
            //RunOnUiThread(() => { progressDialog = ProgressDialog.Show(this, App.TextProvider.GetText(2001)/*"Loading..."*/, text, true); });
        }

        public abstract void LoadUITexts(Language? selectedLanguage);
        #endregion
    }
}