﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Graphics;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    //[Activity (Label = "AIMS Passenger Info", Icon = "@drawable/icon")]			
    public abstract class BaseTabActivity : FragmentActivity, ILoadable
	{
        #region Fields
        private Random RANDOM = new Random ();
		public FragmentPagerAdapter mAdapter;
		public ViewPager mPager;
		public PageIndicator mIndicator;
        private ProgressDialog progressDialog;
        #endregion

        #region Properties
        //protected LanguageProvider TextProvider { get; private set; }
        protected AIMSApp App { get; private set; }
        //protected GPSAdapter GPS { get; private set; }

        public int ScreenWidthInDP
        {
            get;
            private set;
        }
        public int ScreenHeightInDP
        {
            get;
            private set;
        }

        public int IntDisplayWidth
        {
            get;
            private set;
        }
        public int IntDisplayHeight
        {
            get;
            private set;
        }
        #endregion

        #region Methods
        public int ConvertPixelsToDp(float pixelValue)
        {
            var dp = (int)((pixelValue) / Resources.DisplayMetrics.Density);
            return dp;
        }

        public int ConvertDpToPixels(float dpValue)
        {
            var pixels = (int)(dpValue * Resources.DisplayMetrics.Density);
            return pixels;
        }

        public void LongToast(string message)
        {
            RunOnUiThread(() => Toast.MakeText(this, message, ToastLength.Long).Show());
        }

        public void ShortToast(string message)
        {
            RunOnUiThread(() => Toast.MakeText(this, message, ToastLength.Short).Show());
        }

        public void AIMSMessage(string title, string text)
        {
            RunOnUiThread(() =>
            {
                var intent = new Intent(this, typeof(AIMSMessageActivity));
                intent.PutExtra("title", title);
                intent.PutExtra("text", text);
                StartActivity(intent);
            });
        }

        public void AIMSConfirm(string title, string text, EventHandler okCallback = null, EventHandler cancelCallback = null)
        {
            RunOnUiThread(() => {
                var intent = new Intent(this, typeof(AIMSConfirmActivity));
                intent.PutExtra("title", title);
                intent.PutExtra("text", text);
                AIMSConfirmActivity.OkEvent = okCallback;
                AIMSConfirmActivity.CancelEvent = cancelCallback;
                StartActivity(intent);
            });
        }

        public void Dialog(string title, string text, EventHandler cancelCallback)
        {
            RunOnUiThread(() => { var progressDialog = ProgressDialog.Show(this, title, text, true, true, cancelCallback); });
        }

        public void DialogLock(string title, string text)
        {
            RunOnUiThread(() => { progressDialog = ProgressDialog.Show(this, title, text, true); });
        }

        public void HideDialog()
        {
            //if (progressDialog != null)
            //    RunOnUiThread(() => progressDialog.Hide());

            RunOnUiThread(() => Acr.UserDialogs.UserDialogs.Instance.HideLoading());

        }

        public void HideLoading()
        {
            HideDialog();
        }

        public void ShowLoading(string text)
        {
            //DialogLock(App.TextProvider.GetText(2001)/*"Loading..."*/, text);

            Acr.UserDialogs.UserDialogs.Instance.ShowLoading(text);

        }
        
        #endregion

        #region Events

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            App = (AIMSApp)Application;
            //if(App.GPS == null)//it happens in splash screen loading time//TODO: remove if nothing happened
            //    App.GPS = new GPSAdapter(this);
            //TextProvider = LanguageProvider.Current;//it happens in splash screen loading time
            App.TextProvider.OnLanguageChanged += LoadUITexts;//TODO: Check if it happens once!

            var metrics = Resources.DisplayMetrics;
            ScreenWidthInDP = ConvertPixelsToDp(metrics.WidthPixels);
            ScreenHeightInDP = ConvertPixelsToDp(metrics.HeightPixels);

            Display display = this.WindowManager.DefaultDisplay;
            var point = new Point();
            display.GetSize(point);
            IntDisplayWidth = point.X;
            IntDisplayHeight = point.Y;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            //TODO : Can have menu here
            //MenuInflater.Inflate (Resource.Menu.menu, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            //TODO : Can have menu functionalities here
            //			switch (item.ItemId) {
            //			case Resource.Id.random:
            //				int page = RANDOM.Next (mAdapter.Count);
            //				Toast.MakeText (this, "Changing to page " + page, ToastLength.Short);
            //				mPager.CurrentItem = page;
            //				return true;
            //
            //			case Resource.Id.add_page:
            //				Console.WriteLine ("Adapter count " + mAdapter.Count);
            //				if (mAdapter.Count < 10) {
            //					mAdapter.SetCount (mAdapter.Count + 1);
            //					mIndicator.NotifyDataSetChanged ();
            //				}
            //				return true;
            //
            //			case Resource.Id.remove_page:
            //
            //				Console.WriteLine ("Remove page " + mAdapter.Count);
            //				if (mAdapter.Count > 1) {
            //					mAdapter.SetCount (mAdapter.Count - 1);
            //					mIndicator.NotifyDataSetChanged ();
            //				}
            //				return true;
            //			}
            return base.OnOptionsItemSelected(item);
        }

        public abstract void LoadUITexts(Language? selectedLanguage);
        #endregion

    }
}

