﻿using System;
using Android.App;
using Android.Runtime;
using Android.Content;
using Android.OS;
using Ieg.Mobile.Localization;
using AIMS.Models;
using Ieg.Mobile.DataContracts.MobileLounge.ServiceModel;
using System.Collections.Generic;
using System.Timers;
using Android.Graphics;
using System.Linq;

namespace AIMS.Droid
{
    [Application]
    public class AIMSApp : Application, IMobileApplication, Application.IActivityLifecycleCallbacks
    {
        private static readonly DefaultMobileApplication UnderlyingApplication = DefaultMobileApplication.Create();
        private bool isInitialized;
        private Timer refresherTimer;

        public const string AIMS_LOUNGE_IMAGE_NAME = "AIMS_Lounge_{0}.png";

        const int OccupancyCheckerInterval = 180000;

        public delegate void RepositoryChangedHandler(object itemsList);

        public delegate void OccupancyPercentageChangedHandler();

        public event OccupancyPercentageChangedHandler OccupancyPercentageChanged;

        #region Constants
#if DEBUG
        public const bool IsDemoVersion = true;//TODO: resolve it (shouldn't be as a boolean value)
#else
        public const bool IsDemoVersion = false;//TODO: resolve it (shouldn't be as a boolean value)
#endif
        //public const bool UseFrontCamera = true;//TODO: enable front camera (an option for stand/fixed tablets)
        #endregion

        #region Activities and Intents
        public Intent intentMain
        {
            get;
            set;
        }

        public Intent GuestsIntent
        {
            get;
            set;
        }

        public Intent ManualEntryGuest
        {
            get;
            set;
        }

        public Intent SummaryIntent
        {
            get;
            set;
        }

        public Intent ValidationResultIntent
        {
            get;
            set;
        }

        public Intent BoardingPassIntent
        {
            get;
            set;
        }

        public LoungeSelectionActivity LoungeSelectionForm
        {
            get;
            set;
        }

        public MainActivity MainForm
        {
            get;
            set;
        }

        public CardSelectionActivity CardSelectionForm
        {
            get;
            set;
        }

        public ManualEntryActivity ManualInputForm
        {
            get;
            set;
        }

        public ManualEntryActivity PassengerSummaryForm
        {
            get;
            set;
        }

        public ValidationResultActivity ValidationForm
        {
            get;
            set;
        }

        public BoardingPassActivity BoardingPassForm
        {
            get;
            set;
        }

        public GuestsActivity GuestsForm
        {
            get;
            set;
        }

        #endregion

        #region Properties
        //public string SerialNumber
        //{
        //    get { return UnderlyingApplication.SerialNumber; }
        //    //private set { UnderlyingApplication.SerialNumber = value; }
        //}

        public bool IsGuestMode { get;  set; }

        public string StartupFolder
        {
            get { return UnderlyingApplication.StartupFolder; }
            //private set { UnderlyingApplication.StartupFolder = value; }
        }

        public bool ContinueScanning
        {
            get { return UnderlyingApplication.ContinueScanning; }
            set { UnderlyingApplication.ContinueScanning = value; }
        }

        public MobileDevice CurrentDevice { get; set; }

        public DeviceType CurrentDeviceType
        {
            get { return UnderlyingApplication.CurrentDevice.Type; }
            //set { UnderlyingApplication.IsTablet = value; }
        }

        public CameraType? ActiveCamera
        {
            get { return UnderlyingApplication.ActiveCamera; }
            set { UnderlyingApplication.ActiveCamera = value; }
        }

        public static int SessionTimeoutMinutes
        {
            get { return DefaultMobileApplication.SessionTimeoutMinutes; }
            set { DefaultMobileApplication.SessionTimeoutMinutes = value; }
        }

        public bool IsDeveloperModeEnabled
        {
            get { return UnderlyingApplication.IsDeveloperModeEnabled; }
            set { UnderlyingApplication.IsDeveloperModeEnabled = value; }
        }

        public int HoursOccupancyHistory
        {
            get { return UnderlyingApplication.HoursOccupancyHistory; }
            set { UnderlyingApplication.HoursOccupancyHistory = value; }
        }

        public Activity CurrentActivity { get; private set; }

        //private GPSAdapter _gps;
        //public GPSAdapter GPS {
        //    get { return _gps; }
        //    set {
        //        if(_gps == null && value != null) { 
        //            _gps = value;
        //        }
        //    }
        //}
        public LanguageRepository TextProvider
        {
            get { return UnderlyingApplication.TextProvider; }
        }

        //public GpsValidationProvider GpsValidationProvider { get; set; }

        //private ImageRepository imageRepository;//TODO: Here is a critical part in image loading process
        public ImageRepository ImageRepository
        {
            get
            {
                //if (imageRepository == null)
                //    imageRepository = new ImageRepository(new WebApiServiceContext());
                //return imageRepository;
                return new ImageRepository();
            }
        }

        public ResultActivity ResultForm { get; set; }
        public bool IsScanningCancelled { get; set; }
        public bool FromGuestActivity { get; internal set; }
        public bool CloseManualEntryPage { get; internal set; }
        public static Bitmap LoungeBitmapImage { get; internal set; }

        #endregion

        #region Methods
        protected AIMSApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            UnderlyingApplication.ApplicationInit += BootstrapApplication;
            LogsRepository.OnSavedLog += OnSavedLogRepository;
            LogsRepository.OnTrackEvents += LogsRepository_OnTrackEvents;

            try
            {
                DefaultMobileApplication.Current.CheckAccuracy = false;
            }
            catch { }
        }

        private void LogsRepository_OnTrackEvents(Dictionary<string, Dictionary<string, string>> log)
        {
            try
            {
                if (log == null)
                    return;

                var key = log.Keys.ToList().First();
                var value = log.Values.ToList().First();

                if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedLounge != null)
                    key = MembershipProvider.Current.SelectedLounge.LoungeName + " - " + key;

               /* var wifiMgr = (Android.Net.Wifi.WifiManager)GetSystemService(WifiService);
                var wifiList = wifiMgr.ScanResults;
                foreach (var item in wifiList)
                {
                    if (value.ContainsKey($"Wifi SSID: {item.Ssid}"))
                        continue;

                    var wifiLevel = Android.Net.Wifi.WifiManager.CalculateSignalLevel(item.Level, 100);
                    value.Add($"Wifi SSID: {item.Ssid}", $"Strengh: {wifiLevel}");
                } */

                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(key, value);
            }
            catch (Exception ef)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ef);
            }
        }

        private void OnSavedLogRepository(object model)
        {
            try
            {
                if (model == null)
                    return;

                Log log = (Log)model;

                if (log != null)
                {
                    if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedLounge != null)
                        log.Title += " - " + MembershipProvider.Current.SelectedLounge.LoungeName;

                    if (log.LogType == LogType.Info || log.LogType == LogType.Warning)
                    {
                        Microsoft.AppCenter.Analytics.Analytics.TrackEvent(log.LogType.ToString(), new Dictionary<string, string>
                    {
                        { log.Title, string.IsNullOrEmpty(log.Description) ? log.Title :  log.Description.Replace("\t"," ").Replace("\n", " ") }
                    });
                    }
                    else
                    {
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(new Exception(log.Description), new Dictionary<string, string>
                        {
                            { log.LogType.ToString(),  log.Title }
                        });
                    }

                    //Microsoft.AppCenter.Analytics.Analytics.TrackEvent(log.LogType.ToString(), new Dictionary<string, string>
                    //{
                    //    { log.Title, string.IsNullOrEmpty(log.Description) ? log.Title :  log.Description.Replace("\t"," ").Replace("\n", " ") }
                    //});
                }
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex, new Dictionary<string, string> {
                        { "FunctionName", "AIMSApp.OnSavedLogRepository" }});
            }
        }

        public override void OnCreate()
        {
            base.OnCreate();
            RegisterActivityLifecycleCallbacks(this);
            InitializeTimer();
        }

        private void InitializeTimer()
        {
            if (!isInitialized)
            {
                isInitialized = true;
                refresherTimer = new Timer(OccupancyCheckerInterval);
                refresherTimer.Elapsed += OnRefreshEvent;
                refresherTimer.Enabled = true;
            }
        }

        public void OnRefreshEvent(object sender, ElapsedEventArgs e)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine("{0} - OnRefreshEvent was called.", DateTime.Now);
            return;
#endif

            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedLounge == null)
                return;

            GetLoungeOccupancy(MembershipProvider.Current.SelectedLounge, (data) =>
            {
                if (data == null)
                    return;

                MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy = (LoungeOccupancy)data;
                MembershipProvider.Current.SelectedLounge.LastPercentage = (int)System.Math.Ceiling(MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.Percentage);

                if (OccupancyPercentageChanged != null)
                    OccupancyPercentageChanged();

            });
        }

        public void TriggerApplicationInit(string applicationStartupFolder, IGpsAdapter gpsAdapter, Models.MobileDevice device)//(IGpsAdapter gpsAdapter, string deviceSerialNumber, string applicationStartupFolder, DeviceType deviceType)
        {
            UnderlyingApplication.TriggerApplicationInit(applicationStartupFolder, gpsAdapter, device);//, deviceSerialNumber, applicationStartupFolder, deviceType);
        }

        public void TriggerApplicationStart()
        {
            UnderlyingApplication.TriggerApplicationStart();
        }

        public override void OnTerminate()
        {
            base.OnTerminate();
            UnregisterActivityLifecycleCallbacks(this);
        }

        private void BootstrapApplication(object sender, EventArgs e)
        {
            //StartupFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            //UnderlyingApplication.SetDBCurrentPath(StartupFolder);

            //app init ...
            //_mainForm = new MainActivity ();
            //_summaryForm = new SummaryActivity ();
        }

        public void GetLoungeOccupancyWhithQueue(Workstation selectedMobileWorkstation, RepositoryChangedHandler callback = null)
        {
            if (callback == null || selectedMobileWorkstation == null) return;

            var repo = new OccupancyRepository();

            repo.LoadOccupancyWithQueue(selectedMobileWorkstation, (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancy occupancy) =>
            {
                if (isSucceeded)
                    callback.Invoke(occupancy);
                //else
                //    callback.Invoke(null);
#if DEBUG
                if (!isSucceeded)
                    System.Diagnostics.Debug.WriteLine($"Error in OccupancyRepository.GetLoungeOccupancy():{message}\n{errorMetadata}");
#endif
            });
        }

        public void GetRetrieveCachedLoungeOccupancies(decimal loungeID, Workstation selectedMobileWorkstation, DateTime fromLocalDate, DateTime toLocalDate, int intervalInSeconds, RepositoryChangedHandler callback = null)
        {
            if (callback == null || loungeID == 0)
                return;

            var repo = new OccupancyRepository();
            repo.OnLoungeOccupancyCachdReceived += (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancyElapsedData occupancyList) =>
            {
                if (callback != null && isSucceeded)
                    callback.Invoke(occupancyList);
                else
                    callback.Invoke(null);
            };

            repo.RetrieveCachedLoungeOccupancies(loungeID, selectedMobileWorkstation.WorkstationID, selectedMobileWorkstation.AirportCode, fromLocalDate, toLocalDate, intervalInSeconds);
        }


        public void GetDwellLoungeOccupancies(decimal loungeID, Workstation selectedMobileWorkstation, DateTime fromLocalDate, DateTime toLocalDate, int intervalInSeconds, RepositoryChangedHandler callback = null)
        {
            if (callback == null || loungeID == 0)
                return;

            var repo = new OccupancyRepository();
            repo.OnLoungeOccupancyCachdReceived += (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancyElapsedData occupancyList) =>
            {
                if (callback != null && isSucceeded)
                    callback.Invoke(occupancyList);
            };

            repo.GetDwellLoungeOccupancies(loungeID, selectedMobileWorkstation.WorkstationID, selectedMobileWorkstation.AirportCode, fromLocalDate, toLocalDate, intervalInSeconds);
        }


        public static void GetLoungeOccupancy(Workstation selectedMobileWorkstation, RepositoryChangedHandler callback = null)
        {
            if (callback == null || selectedMobileWorkstation == null) return;

            var repo = new OccupancyRepository();
            repo.OnLoungeOccupancyReceived += (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancy occupancy) =>
            {
                if (isSucceeded)
                    callback.Invoke(occupancy);
                else
                    callback.Invoke(null);
#if DEBUG
                if (!isSucceeded)
                    System.Diagnostics.Debug.WriteLine($"Error in OccupancyRepository.GetLoungeOccupancy():{message}\n{errorMetadata}");
#endif
            };
            repo.GetLoungeOccupancy(selectedMobileWorkstation);
        }



        #region Activity Lifecycle Callbacks
        public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
        {
            CurrentActivity = activity;
            //throw new NotImplementedException();
        }

        public void OnActivityDestroyed(Activity activity)
        {
            //throw new NotImplementedException();
        }

        public void OnActivityPaused(Activity activity)
        {
            //throw new NotImplementedException();
        }

        public void OnActivityResumed(Activity activity)
        {
            CurrentActivity = activity;
            //throw new NotImplementedException();
        }

        public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
        {
            //throw new NotImplementedException();
        }

        public void OnActivityStarted(Activity activity)
        {
            CurrentActivity = activity;
        }

        public void OnActivityStopped(Activity activity)
        {
            //throw new NotImplementedException();
        }
        #endregion
        #endregion

        #region IMobileApplication implementation

        public event EventHandler ApplicationStart { add { UnderlyingApplication.ApplicationStart += value; } remove { UnderlyingApplication.ApplicationStart -= value; } }

        public event EventHandler ApplicationInit { add { UnderlyingApplication.ApplicationInit += value; } remove { UnderlyingApplication.ApplicationInit -= value; } }

        public event EventHandler ApplicationStop { add { UnderlyingApplication.ApplicationStop += value; } remove { UnderlyingApplication.ApplicationStop -= value; } }

        public event EventHandler<ErrorCode> OnValidationFailed { add { DefaultMobileApplication.OnValidationFailed += value; } remove { DefaultMobileApplication.OnValidationFailed -= value; } }

        #endregion
    }
}