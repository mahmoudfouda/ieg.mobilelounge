using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Github.Aakira.Expandablelayout;
using Ieg.Mobile.Localization;
using Java.Security;
using Syncfusion.Android.Buttons;

namespace AIMS.Droid
{
    [Activity(Label = "Last Hour Passengers", WindowSoftInputMode = SoftInput.AdjustResize | SoftInput.StateAlwaysHidden)]
    public class AgentPassengersActivity : BaseFragment
    {
        #region Fields
        private EditText txtDate;
        private Spinner spinnerDates;
         private ImageButton btnSearchFilter;
        private int previousTotal = 0;

        PassengersAdapter passengersAdapter;
        bool isBusy = false;
        #endregion

        #region Elements
        View view;
        RelativeLayout pnlPassengerSearch;

        EditText txtPassengerSearch;
        //Button btnPassengerSearch;
        ImageButton btnFailedPassengers, btnPassengerResetSearch, btnPassengerSearch;
        ListView lstPassengers;
        private ExpandableRelativeLayout expandableLayout;
        AIMSApp app;
        private DateTime currentDateFilter;
        private int totalItemCount = 0;
        private int currentPage = 1;
        private int previousTotalItemCount;
        private int startingPageIndex;
        private bool loading;
        private SfSegmentedControl segmentedControl;
        private ObservableCollection<string> textCollection = new ObservableCollection<String>();
        private const int PAGE_SIZE = 20;
        #endregion

        #region Methods
        private void LoadPassengers(bool silent = false)
        {
            if (!isBusy)
            {
                if (!silent)
                    ShowLoading(App.TextProvider.GetText(2509));// "Loading last hour passengers..");//TODO: Check translations and add to EXCEL file
                isBusy = true;
                passengersAdapter.LoadLastHourPassengers();
            }
        }

        private void LoadFailedPassengers(bool silent = false)
        {
            if (!isBusy)
            {
                if (!silent)
                    ShowLoading("");
                isBusy = true;
                passengersAdapter.LoadFailedPassengers();
            }
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
            txtPassengerSearch.Hint = string.Format("{0}...", App.TextProvider.GetText(2008));

            //Load by labels
            List<String> arrayList = new List<string>();
            arrayList.Add(App.TextProvider.GetText(2184));
            arrayList.Add(App.TextProvider.GetText(2185));
            arrayList.Add(App.TextProvider.GetText(2186));
            arrayList.Add(App.TextProvider.GetText(2187));
            ArrayAdapter adapter = new ArrayAdapter(this.Context, Resource.Drawable.spinner_item, arrayList);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinnerDates.Adapter = adapter;

            if (textCollection == null)
                textCollection = new ObservableCollection<string>();
            else if (textCollection.Count > 0)
            {
                try
                {
                    textCollection.Clear();
                }
                catch { }
            }

            textCollection.Add(App.TextProvider.GetText(2188));
            textCollection.Add(App.TextProvider.GetText(2189));

        }
        #endregion

        #region Events
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.AgentPassengers, container, false);
            app = (AIMSApp)this.Activity.Application;

            #region Initialization
            pnlPassengerSearch = view.FindViewById<RelativeLayout>(Resource.Id.pnlPassengerSearch);
            txtPassengerSearch = view.FindViewById<EditText>(Resource.Id.txtPassengerSearch);
            btnPassengerSearch = view.FindViewById<ImageButton>(Resource.Id.btnPassengerSearch);
            btnPassengerResetSearch = view.FindViewById<ImageButton>(Resource.Id.btnPassengerResetSearch);
            btnFailedPassengers = view.FindViewById<ImageButton>(Resource.Id.btnFailedPassengers);
            lstPassengers = view.FindViewById<ListView>(Resource.Id.lstPassengers);
            expandableLayout = (ExpandableRelativeLayout)view.FindViewById(Resource.Id.expandableLayout);
            expandableLayout.Toggle();
            txtDate = view.FindViewById<EditText>(Resource.Id.txtDate);
            txtDate.Click += TxtDate_Click;
            currentDateFilter = DateTime.Now;
            txtDate.Text = currentDateFilter.Date.ToString("MM/dd/yyyy");

            spinnerDates = view.FindViewById<Spinner>(Resource.Id.spinnerDates);
            var adapter = ArrayAdapter.CreateFromResource(this.Context, Resource.Array.filterDates, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinnerDates.Adapter = adapter;

     
            btnSearchFilter = view.FindViewById<ImageButton>(Resource.Id.btnSearchFilter);
            btnSearchFilter.Click += BtnSearchFilter_Click;

            var segmentMain = view.FindViewById<LinearLayout>(Resource.Id.segmentMain);
            segmentedControl = new SfSegmentedControl(view.Context);


            segmentedControl.ItemsSource = textCollection;
            segmentedControl.CornerRadius = 10;
            segmentedControl.SegmentHeight = 40;
            segmentedControl.VisibleSegmentsCount = 2;
            segmentedControl.SelectedIndex = 1;
            segmentedControl.BackColor = Color.Rgb(0, 207, 246);
            segmentedControl.BorderColor = Color.Rgb(0, 207, 246);
            segmentedControl.BorderThickness = 1;
            segmentedControl.SelectionTextColor = Color.Rgb(2, 160, 174);
            segmentedControl.FontColor = Color.DarkGray;
            segmentedControl.SelectionIndicatorSettings = new SelectionIndicatorSettings
            {
                //CornerRadius = 10,
                Position= SelectionIndicatorPosition.Fill,
                Color = Color.White
            };
            segmentedControl.SetGravity(GravityFlags.Center);
            

            segmentMain.AddView(segmentedControl);


            #endregion

            passengersAdapter = new PassengersAdapter(this.Context, App);
            passengersAdapter.AdapterChanged += (notUsed) =>
            {
                HideLoading();
                isBusy = false;

                if (passengersAdapter.IsSearchByFilter)
                    totalItemCount += PAGE_SIZE;

                loading = false;
                Activity.RunOnUiThread(new Action(() =>
                {
                    lstPassengers.Adapter = passengersAdapter;

                    try
                    {
                        lstPassengers.SetSelection(previousTotalItemCount);
                    }
                    catch { }

                }));
            };

            LoadPassengers(true);

            lstPassengers.ItemClick += delegate (object sender, AdapterView.ItemClickEventArgs args)
            {
                var passenger = ((JavaPassenger)lstPassengers.Adapter.GetItem(args.Position));

                lstPassengers.ClearChoices();
                lstPassengers.RequestLayout();

                if (passengersAdapter.IsLastHourLoaded)
                {
                    MembershipProvider.Current.SelectedPassenger = passenger;

                    //app.SummaryIntent.SetFlags(ActivityFlags.NewTask);
                    //app.MainForm.StartActivity(app.SummaryIntent);//great idea

                    app.ValidationResultIntent = new Intent(this.Context, typeof(ValidationResultActivity));
                    app.ValidationResultIntent.SetFlags(ActivityFlags.NewTask);
                    app.MainForm.StartActivity(app.ValidationResultIntent);


                    //app.BoardingPassIntent.SetFlags(ActivityFlags.NewTask);//TODO: New boarding pass design
                    //app.MainForm.StartActivity(app.BoardingPassIntent);//great idea
                }
                else
                {
                    string title = "", text = "";
                    if (string.IsNullOrEmpty(passenger.FailedReason))
                    {
                        title = App.TextProvider.GetText(2045);
                        text = passenger.BarcodeString;
                    }
                    else
                    {
                        var newLineIndex = passenger.FailedReason.IndexOf("\n");
                        if (newLineIndex == -1)
                        {
                            title = passenger.FailedReason;
                            text = passenger.BarcodeString;
                        }
                        else
                        {
                            title = passenger.FailedReason.Substring(0, newLineIndex);
                            text = string.Format("{0}\n\n{1}\n", passenger.FailedReason.Substring(newLineIndex + 1),
                            !string.IsNullOrWhiteSpace(passenger.BarcodeString) ?
                            string.Format("Barcode: {0}", passenger.BarcodeString) :
                            string.Format("Manually entered\nFull Name: {0}\nFlight: [{1}-{2}-{3}]\nFFN: {4}",
                            passenger.FullName, passenger.FlightCarrier,
                            passenger.FlightNumber, passenger.TrackingClassOfService,
                            passenger.FFN));
                        }
                    }
                    AIMSMessage(title, text);
                }
            };
            btnPassengerSearch.Click += BtnPassengerSearch_Click;
            btnPassengerResetSearch.Click += delegate (object sender, EventArgs e)
            {

                if (expandableLayout.Expanded)
                {
                    expandableLayout.Collapse();
                    this.txtPassengerSearch.Text = "";
                    LoadPassengers();
                }
                else
                {
                    this.passengersAdapter.ClearList();
                    expandableLayout.Expand();
                }


            };
            btnFailedPassengers.Click += delegate (object sender, EventArgs e)
            {
                this.txtPassengerSearch.Text = "";
                LoadFailedPassengers();
            };

            lstPassengers.Scroll += LstPassengers_Scroll;

            LoadUITexts(App.TextProvider.SelectedLanguage);
            return view;
        }

        private void LstPassengers_Scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            if (!passengersAdapter.IsSearchByFilter)
                return;


            // If the total item count is zero and the previous isn't, assume the
            // list is invalidated and should be reset back to initial state
            if (totalItemCount < previousTotalItemCount)
            {
                this.currentPage = this.startingPageIndex;
                this.previousTotalItemCount = totalItemCount;
                if (totalItemCount == 0) { this.loading = true; }
            }
            // If it's still loading, we check to see if the dataset count has
            // changed, if so we conclude it has finished loading and update the current page
            // number and total item count.
            if (loading && (totalItemCount > previousTotalItemCount))
            {
                //loading = false;
                previousTotalItemCount = totalItemCount;
            }

            // If it isn't currently loading, we check to see if we have breached
            // the visibleThreshold and need to reload more data.
            // If we do need to reload some more data, we execute onLoadMore to fetch the data.
            if (!loading && (e.FirstVisibleItem + e.VisibleItemCount) >= totalItemCount)
            {
                LoadMore(currentPage + 1);
            }
        }

        private void LoadMore(int nextPage)
        {
            if (string.IsNullOrWhiteSpace(txtPassengerSearch.Text) || txtPassengerSearch.Text.Length < 3) return;
            if (!isBusy)
            {
                TimeSpan currentTimeSpan = CalculateTimeToSearch();

                ShowLoading(App.TextProvider.GetText(2510));
                isBusy = true;
                loading = true;
                passengersAdapter.LoadMorePassengersByFilter(txtPassengerSearch.Text, segmentedControl.SelectedIndex == 0,
                                                             currentDateFilter.Subtract(currentTimeSpan), nextPage);
                currentPage = nextPage;
                expandableLayout.Collapse();
            }
        }

        private void BtnSearchFilter_Click(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(txtPassengerSearch.Text) || txtPassengerSearch.Text.Length < 3) return;
            if (!isBusy)
            {
                TimeSpan currentTimeSpan = CalculateTimeToSearch();

                var date = currentDateFilter;

                if (currentTimeSpan.Hours == 0)
                    date = date.Date;
                else
                    date = currentDateFilter.Subtract(currentTimeSpan);

                currentPage = 1;
                totalItemCount = 0;
                ShowLoading(App.TextProvider.GetText(2510));// "Searching last hour passengers..");//TODO: Check translations and add to EXCEL file
                isBusy = true;
                loading = true;
                passengersAdapter.SearchPassengersByFilter(txtPassengerSearch.Text, segmentedControl.SelectedIndex == 0,
                                                           date, currentPage);
                expandableLayout.Collapse();
            }
        }

        private TimeSpan CalculateTimeToSearch()
        {
            var currentTimeSpan = TimeSpan.FromSeconds(0);

            switch (spinnerDates.SelectedItemPosition)
            {
                case 1:
                    currentTimeSpan = TimeSpan.FromHours(4);
                    break;

                case 2:
                    currentTimeSpan = TimeSpan.FromHours(8);
                    break;

                case 3:
                    currentTimeSpan = TimeSpan.FromHours(24);
                    break;

                default:
                    break;
            }

            return currentTimeSpan;
        }


        private void TxtDate_Click(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today;
            DatePickerDialog dialog = new DatePickerDialog(this.Context, OnDateSet, today.Year, today.Month - 1, today.Day);
            dialog.DatePicker.MinDate = today.Millisecond;
            dialog.Show();
        }

        void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            currentDateFilter = e.Date;
            txtDate.Text = e.Date.ToString("MM/dd/yyyy");
        }

        private void BtnPassengerSearch_Click(object sender, EventArgs e)
        {
            this.txtPassengerSearch.Text = "";
            LoadPassengers();
        }

        public override void OnResume()
        {
            base.OnResume();

            btnFailedPassengers.Visibility = App.IsDeveloperModeEnabled ? ViewStates.Visible : ViewStates.Gone;

            //if (!isBusy)
            //{
            //    isBusy = true;
            //    passengersAdapter.LoadPassengers();
            //}
        }

        #endregion
    }

}