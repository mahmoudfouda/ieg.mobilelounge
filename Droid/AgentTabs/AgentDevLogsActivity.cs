using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity(Label = "Advanced Logs")]
    public class AgentDevLogsActivity : BaseFragment
    {
        #region Fields
        LogsAdapter adapter;
        #endregion

        #region Elements
        View view;

        ListView lstDevLogs;
        #endregion

        #region Methods
        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
        }

        //public void RefreshLogs()
        //{
        //    if (adapter != null)
        //    {
        //        if (App.IsDeveloperModeEnabled)
        //            adapter.LoadDeveloperOnlyLogs();
        //        else adapter.LoadClientOnlyLogs();
        //    }
        //}
        #endregion

        #region Events
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.AgentDevLogs, container, false);

            lstDevLogs = view.FindViewById<ListView>(Resource.Id.lstDevLogs);

            adapter = new LogsAdapter(this.Context);
            //adapter.LoadDeveloperOnlyLogs();

            lstDevLogs.Adapter = adapter;
            
            lstDevLogs.ItemClick += delegate (object sender, AdapterView.ItemClickEventArgs args) {
                var selectedLog = ((JavaLog)lstDevLogs.Adapter.GetItem(args.Position));
                AIMSMessage(selectedLog.Title, string.Format("[{0} on {1}]\n{2}",selectedLog.Time.ToShortDateString(), selectedLog.Time.ToLongTimeString(), selectedLog.Description));
            };

            return view;
        }
        
        public override void OnResume()
        {
            base.OnResume();
            adapter.LoadDeveloperOnlyLogs();
        }
        #endregion
    }
}