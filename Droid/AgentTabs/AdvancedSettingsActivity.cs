using System;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity(Label = "Advanced Settings")]
    public class AdvancedSettingsActivity : BaseFragment
    {
        #region Fields
        LogsRepository repository;
        #endregion

        #region Elements
        View view;
        TextView lblFrontCamera, lblPushLogMessages;

        ImageButton btnFrontCameraSwitch;
        Button btnPushLogMessages, btnClearLogs;
        #endregion

        #region Methods

        public override void LoadUITexts(Language? selectedLanguage)
        {
            lblFrontCamera.Text = "Scanner";//TODO: Add to languages
            lblPushLogMessages.Text = string.Format("{0}:", App.TextProvider.GetText(2040));

            btnPushLogMessages.Text = App.TextProvider.GetText(2054);
            btnClearLogs.Text = App.TextProvider.GetText(2055);
        }

        private void RefreshCameraSwitchButton()
        {
            if (App.ActiveCamera == CameraType.FrontCamera)
                btnFrontCameraSwitch.SetImageResource(Resource.Drawable.camera_back03);
            else btnFrontCameraSwitch.SetImageResource(Resource.Drawable.camera_front03);
        }
        #endregion

        #region Events

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.AdvancedSettings, container, false);
            repository = new LogsRepository();

            #region Initializing Components
            lblFrontCamera = view.FindViewById<TextView>(Resource.Id.lblFrontCamera);
            lblPushLogMessages = view.FindViewById<TextView>(Resource.Id.lblPushLogMessages);

            btnPushLogMessages = view.FindViewById<Button>(Resource.Id.btnPushLogMessages);
            btnPushLogMessages.Click += (sender,e) => {
                repository.UploadLogs((ss,ee)=> {
                    LongToast(App.TextProvider.GetText(2031));
                    //ShortToast("Logs have been sent to server.");//TODO: Give a Reference-Number to the client
                });
            };

            btnClearLogs = view.FindViewById<Button>(Resource.Id.btnClearLogs);
            btnClearLogs.Click += (sender, e) => {
                AIMSConfirm(App.TextProvider.GetText(2040), App.TextProvider.GetText(2518), (ss, ee) => {
                    repository.DeleteAllLogs();
                    LongToast(App.TextProvider.GetText(2031));
                });
            };
            
            btnFrontCameraSwitch = view.FindViewById<ImageButton>(Resource.Id.btnFrontCameraSwitch);
            RefreshCameraSwitchButton();
            btnFrontCameraSwitch.Click += (sender, e) =>
            {
                if(App.ActiveCamera == CameraType.FrontCamera)
                    App.ActiveCamera = CameraType.BackCamera;
                else App.ActiveCamera = CameraType.FrontCamera;

                RefreshCameraSwitchButton();
            };
            #endregion

            LoadUITexts(App.TextProvider.SelectedLanguage);

            return view;
        }
        
        #endregion
    }
}