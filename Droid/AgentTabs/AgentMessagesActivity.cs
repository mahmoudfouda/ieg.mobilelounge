using System;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity(Label = "Messages")]
    public class AgentMessagesActivity : BaseFragment
    {
        #region Fields
        MessagesAdapter adapter;
        #endregion

        #region Elements
        View view;
        ListView lstAgentMessages;
        #endregion

        #region Methods
        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
        }
        #endregion

        #region Events
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.AgentMessages, container, false);
            lstAgentMessages = view.FindViewById<ListView>(Resource.Id.lstAgentMessages);

            lstAgentMessages.Adapter = adapter = new MessagesAdapter(this.Context);
            adapter.AdapterChanged += (messages) => {
                //HideLoading();
                Activity.RunOnUiThread(new Action(() => {
                    lstAgentMessages.Adapter = adapter;
                }));
            };

            lstAgentMessages.ItemClick += delegate (object sender, AdapterView.ItemClickEventArgs args) {
                //var selectedMessage = ((JavaMessage)lstAgentMessages.Adapter.GetItem(args.Position));
                ShowLoading("");
                ((MessagesAdapter)lstAgentMessages.Adapter).LoadFullMessage(args.Position, 
                    (selectedMessage) => {
                        HideLoading();
                        AIMSMessage(selectedMessage.Title,
                            string.Format("From:{0}\nDate:{1}\n-------\n{2}",
                                selectedMessage.From,
                                selectedMessage.Time,
                                selectedMessage.Body));
                    });
            };

            //ShowLoading("");//silent
            adapter.LoadMessages();

            return view;
        }

        //public override void OnResume()
        //{
        //    base.OnResume();
        //}
        
        #endregion
    }
}