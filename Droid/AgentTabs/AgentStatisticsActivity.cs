﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AIMS.Models;
using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.Content;
using Android.Support.V4.Content.Res;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using Com.Syncfusion.Charts;
using Ieg.Mobile.Localization;
using Java.Sql;
using Microcharts;
using Microcharts.Droid;
using SkiaSharp;
using static Android.Views.View;
using Orientation = Android.Widget.Orientation;

namespace AIMS.Droid
{
    [Activity(Label = "Agent Statistics")]
    public class AgentStatisticsActivity : BaseFragment
    {
        #region Fields
        private const int MinRoundMinutes = 5;
        private const int MaxRoundMinutes = 60;

        private bool dwellSelected;
        private bool graphSelected;
        private int roundMinutes = 5;
        private int primaryPassengers;
        private int guestPassengers;
        private DateTime toLocalDate, currentFromLocalDate;
        private List<Workstation> workstations;
        private LoungeOccupancyElapsedData listData;
        //private JavaLounge selectedLounge;
        private SeekbarListener seekbarListener;

        private string formatedGuestText = "{0}";
        private string formatedPrimaryText = "{0}";
        private string formatedLoungeText = "{0}";
        private string formatedCustomersText = "{0}";
        private string titleOfPage = "Lounge Traffic - ";
        private string titleOfPieChart = "Current data - Last update: {0}";
        private System.Threading.CancellationTokenSource throttleCts = new System.Threading.CancellationTokenSource();

        #endregion

        #region Elements
        private View view;
        private SwipeRefreshLayout swipeRefreshLayout;

        //private SwipeRefreshLayout swipeRefreshLayout;
        private TextView lblTitle, lblSubTitle, lblAgentStatisticsCapacityTitle, lblAgentStatisticsCapacityAmount, lblAgentStatisticsCurrentCustomersTitle,
            lblAgentStatisticsCurrentCustomersAmount, lblAgentStatisticsPrimaryTitle, lblAgentStatisticsPrimaryAmount, lblAgentStatisticsGuestsTitle,
            lblAgentStatisticsGuestsAmount;
        private JavaLounge currentLounge;
        private ListView lstLounges;
        private SeekBar seekBar;

        private Button trafficButton;
        private Button dwellButton;
        private Button refreshButton;
        private SfChart chartView;
        private SfChart pieChartView;
        private DateTimeAxis primaryAxis;
        private NumericalAxis secondaryAxis;
        private ColumnSeries ChartColumnBarSeriesActual;
        private ColumnSeries ChartColumnBarSeriesWeekly;
        private CustomSFChartTrackballBehavior trackballBehavior;
        private DateTimeAxis primaryBarAxis;
        private NumericalAxis secondaryBarAxis;

        public ChartZoomPanBehavior ZoomPan { get; private set; }
        public AreaSeries ChartSeriesPax { get; private set; }
        public AreaSeries ChartSeriesGuest { get; private set; }
        public AreaSeries ChartSeriesReentry { get; private set; }
        public AreaSeries ChartSeriesUnknown { get; private set; }
        public PieSeries PieSeries { get; private set; }


        #endregion

        #region Methods
        public override void LoadUITexts(Language? selectedLanguage)
        {
            titleOfPage = App.TextProvider.GetText(2145);
            titleOfPieChart = App.TextProvider.GetText(2146);

            lblAgentStatisticsCapacityTitle.Text = App.TextProvider.GetText(2147).Replace(" {0}","");
            lblAgentStatisticsCurrentCustomersTitle.Text = App.TextProvider.GetText(2148).Replace(" {0}", "");
            lblAgentStatisticsPrimaryTitle.Text = App.TextProvider.GetText(2149).Replace(" {0}", "");
            lblAgentStatisticsGuestsTitle.Text = App.TextProvider.GetText(2150).Replace(" {0}", "");

            secondaryAxis.Title.Text = (App.TextProvider.GetText(2151));
            secondaryBarAxis.Title.Text = (App.TextProvider.GetText(2151));

            primaryAxis.Title.Text = (App.TextProvider.GetText(2152));
            primaryBarAxis.Title.Text = (App.TextProvider.GetText(2152));

            ChartSeriesPax.Label = (App.TextProvider.GetText(2153));
            ChartSeriesGuest.Label = (App.TextProvider.GetText(2154));

            ChartColumnBarSeriesActual.Label = (App.TextProvider.GetText(2155));
            ChartColumnBarSeriesWeekly.Label = (App.TextProvider.GetText(2156));

            chartView.ReloadChart();

            //ChartColumnBar.Title.Text = DateTime.Now.ToLongDateString() + App.TextProvider.GetText(2157);
        }
        #endregion

        #region Events
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            workstations = MembershipProvider.Current.UserLounges.ToList();
        }

        public override void OnResume()
        {
            base.OnResume();

        }



        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.AgentStatistics, container, false);

            swipeRefreshLayout = view.FindViewById<SwipeRefreshLayout>(Resource.Id.swipeToRefresh);
            var refreshListener = new RefreshListener();
            swipeRefreshLayout.SetOnRefreshListener(refreshListener);
            refreshListener.Refreshed += RefreshListener_Refreshed;
            ConfigureLabels();

            ConfigureSeekBar();

            ConfigureListView();

            chartView = view.FindViewById<SfChart>(Resource.Id.chartView);
            pieChartView = view.FindViewById<SfChart>(Resource.Id.pieChartView);

            ConfigureButtons();

            ChartSettings();

            setListViewHeightBasedOnChildren(lstLounges);

            LoadUITexts(null);
            TrafficButton_Click(null, null);

            return view;
        }

        private void ConfigureListView()
        {
            currentLounge = new JavaLounge(MembershipProvider.Current.SelectedLounge);
            lstLounges = view.FindViewById<ListView>(Resource.Id.lstAgentStatisticsLounges);
            lstLounges.Adapter = new LoungeAdapter(this, currentLounge);
            lstLounges.ItemClick += LstLounges_ItemClick;
        }

        private void ConfigureButtons()
        {
            Typeface font = Typeface.CreateFromAsset(Context.Assets, "Font Awesome 5 Free-Solid-900.otf");
            trafficButton = view.FindViewById<Button>(Resource.Id.lblAgentStatisticsTrafficGraphButton);
            dwellButton = view.FindViewById<Button>(Resource.Id.lblAgentStatisticsDwellGraphButton);
            trafficButton.SetTypeface(font, TypefaceStyle.Normal);
            dwellButton.SetTypeface(font, TypefaceStyle.Normal);
            trafficButton.Click += TrafficButton_Click;
            dwellButton.Click += DwellButton_Click;
        }

        private void ConfigureLabels()
        {
            lblTitle = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsTitle);
            lblTitle.Text = $"{App.TextProvider.GetText(2145)} {MembershipProvider.Current.SelectedLounge.WorkstationName}";
            lblTitle.SetTypeface(null, TypefaceStyle.Bold);

            lblSubTitle = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsSubTitle);
            lblSubTitle.Text = $"{MembershipProvider.Current.SelectedLounge.AirportCode} {MembershipProvider.Current.SelectedLounge.LoungeName}";

            lblAgentStatisticsCapacityTitle = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsCapacityTitle);
            lblAgentStatisticsCapacityTitle.Text = "Capacity:"; // TODO use localization

            lblAgentStatisticsCapacityAmount = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsCapacityAmount);

            lblAgentStatisticsCurrentCustomersTitle = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsCurrentCustomersTitle);
            lblAgentStatisticsCurrentCustomersTitle.Text = "Current Customers:";

            lblAgentStatisticsCurrentCustomersAmount = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsCurrentCustomersAmount);

            lblAgentStatisticsPrimaryTitle = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsPrimaryTitle);
            lblAgentStatisticsPrimaryTitle.Text = "Primary:"; // TODO use localization

            lblAgentStatisticsPrimaryAmount = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsPrimaryAmount);

            lblAgentStatisticsGuestsTitle = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsGuestsTitle);
            lblAgentStatisticsGuestsTitle.Text = "Guests:"; // TODO use localization

            lblAgentStatisticsGuestsAmount = view.FindViewById<TextView>(Resource.Id.lblAgentStatisticsGuestsAmount);
        }

        private void ConfigureSeekBar()
        {
            seekBar = view.FindViewById<SeekBar>(Resource.Id.seekBar);
            seekBar.Max = App.HoursOccupancyHistory;
            seekBar.Progress = 0;
            seekBar.Min = 0;
            SeekbarListener seekbarListener = new SeekbarListener();
            seekbarListener.ProgressChanged += SeekbarListener_ProgressChanged;
            seekBar.SetOnSeekBarChangeListener(seekbarListener);
        }

        private void SeekbarListener_ProgressChanged(object sender, EventArgs e)
        {
            if (listData != null && listData.OccupancyRecords.Count > 0)
                ZoomPan.ZoomByRange((DateTimeAxis)chartView.PrimaryAxis, listData.OccupancyRecords.First().LocalTime.AddHours(seekBar.Progress), DateTime.Now.ToLocalTime());
        }

        private void RefreshListener_Refreshed(object sender, EventArgs e)
        {
            LoadData();
        }

        private void ChartSettings()
        {
            ZoomPan = new ChartZoomPanBehavior();
            ZoomPan.ZoomMode = ZoomMode.X;
            chartView.Behaviors.Add(ZoomPan);

            trackballBehavior = new CustomSFChartTrackballBehavior();
            trackballBehavior.LineStyle.StrokeWidth = (1);
            trackballBehavior.LineStyle.StrokeColor = Color.Blue;
            trackballBehavior.LabelDisplayMode = TrackballLabelDisplayMode.FloatAllPoints;
            trackballBehavior.TrackBallChanged += TrackballBehavior_TrackBallChanged;
            chartView.Behaviors.Add(trackballBehavior);

            primaryAxis = new DateTimeAxis();
            primaryAxis.Title.Text = "Time";
            primaryAxis.LabelStyle.LabelFormat = "HH:mm";
            chartView.PrimaryAxis = primaryAxis;

            //Adding Secondary Axis for the Chart.
            secondaryAxis = new NumericalAxis();
            secondaryAxis.Title.Text = ("Total Customers");
            secondaryAxis.Interval = 10;
            secondaryAxis.Minimum = 0;
            chartView.SecondaryAxis = secondaryAxis;

            #region Creating Series of main chart
            ChartSeriesPax = new AreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Primary",
                //Color = UIColor.Clear.FromHexString("#0e79d8"),
                ShowTrackballInfo = true,
                //Alpha = 0.5f,
                EnableAnimation = true
            };
            ChartSeriesPax.ColorModel.ColorPalette = ChartColorPalette.Custom;
            ChartGradientColor gradientColorPax = new ChartGradientColor() { StartPoint = new PointF(0.5f, 1), EndPoint = new PointF(0.5f, 0) };

            ChartGradientStop stopP1 = new ChartGradientStop() { Color = Color.ParseColor("#660e79d8"), Offset = 0 };
            ChartGradientStop stopP2 = new ChartGradientStop() { Color = Color.ParseColor("#0e79d8"), Offset = 1 };

            gradientColorPax.GradientStops.Add(stopP1);
            gradientColorPax.GradientStops.Add(stopP2);


            ChartGradientColorCollection gradientColorsPax = new ChartGradientColorCollection()
            {
                gradientColorPax//,
                //gradientColorPax2
            };
            ChartSeriesPax.ColorModel.CustomGradientColors = gradientColorsPax;

            ChartSeriesGuest = new AreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Guest",
                ShowTrackballInfo = true,
                EnableAnimation = true
            };
            ChartSeriesGuest.ColorModel.ColorPalette = ChartColorPalette.Custom;
            ChartGradientColor gradientColorGuest = new ChartGradientColor() { StartPoint = new PointF(0.5f, 1), EndPoint = new PointF(0.5f, 0) };
            ChartGradientStop stopG1 = new ChartGradientStop() { Color = Color.ParseColor("#669370db"), Offset = 0 };
            ChartGradientStop stopG2 = new ChartGradientStop() { Color = Color.ParseColor("#9370db"), Offset = 1 };
            gradientColorGuest.GradientStops.Add(stopG1);
            gradientColorGuest.GradientStops.Add(stopG2);
            ChartGradientColorCollection gradientColorsGuest = new ChartGradientColorCollection()
            {
                gradientColorGuest
            };
            ChartSeriesGuest.ColorModel.CustomGradientColors = gradientColorsGuest;

            ChartSeriesReentry = new AreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Reentry",
                ShowTrackballInfo = true,
                Alpha = 0.5f,
                VisibilityOnLegend = Visibility.Gone
            };

            ChartSeriesUnknown = new AreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Unknown",
                VisibilityOnLegend = Visibility.Gone,
                Color = Color.Gray,
                Alpha = 0.5f
            };

            chartView.Series.Add(ChartSeriesPax);
            chartView.Series.Add(ChartSeriesGuest);
            chartView.Series.Add(ChartSeriesReentry);
            chartView.Legend.Visibility = Visibility.Visible;
            #endregion


            #region Settings for Pie Chart
            PieSeries = new PieSeries()
            {
                XBindingPath = "TypePassenger",
                YBindingPath = "Total",
                CircularCoefficient = 0.85,
                DataMarkerPosition = CircularSeriesDataMarkerPosition.OutsideExtended,
                EnableAnimation = true
            };

            pieChartView.Series.Add(PieSeries);
            PieSeries.DataMarker.LabelContent = LabelContent.Percentage;
            PieSeries.DataMarker.ShowLabel = true;

            PieSeries.ColorModel.ColorPalette = ChartColorPalette.Custom;

            Color[] nsArray = new Color[] { Color.ParseColor("#0e79d8"), Color.ParseColor("#9370db"), Color.LightGray };
            PieSeries.ColorModel.CustomColors = nsArray;
            #endregion

            bool isTabletSize = Resources.GetBoolean(Resource.Boolean.isTablet);
            if (isTabletSize)
            {
                pieChartView.Title.TextSize = 15;
                chartView.Title.TextSize = 19;
                lblTitle.TextSize = 22;
                lblSubTitle.TextSize = 17;

                lblAgentStatisticsCapacityAmount.TextSize = lblAgentStatisticsCapacityTitle.TextSize = 15;
                lblAgentStatisticsCurrentCustomersAmount.TextSize = lblAgentStatisticsCurrentCustomersTitle.TextSize = 15;
                lblAgentStatisticsGuestsAmount.TextSize = lblAgentStatisticsGuestsTitle.TextSize = 15;
                lblAgentStatisticsPrimaryAmount.TextSize = lblAgentStatisticsPrimaryTitle.TextSize = 15;
            }
            else
            {
               // pieChartView.LayoutParameters.Height = (int)(pieChartView.LayoutParameters.Height * 0.7);
                pieChartView.Title.TextSize = 12;
                chartView.Title.TextSize = 16;
                chartView.Legend.Title.TextSize = 12;
                lblTitle.TextSize = 19;
                lblSubTitle.TextSize = 14;
                lblAgentStatisticsCapacityAmount.TextSize = lblAgentStatisticsCapacityTitle.TextSize = 14;
                lblAgentStatisticsCurrentCustomersAmount.TextSize = lblAgentStatisticsCurrentCustomersTitle.TextSize = 14;
                lblAgentStatisticsGuestsAmount.TextSize = lblAgentStatisticsGuestsTitle.TextSize = 14;
                lblAgentStatisticsPrimaryAmount.TextSize = lblAgentStatisticsPrimaryTitle.TextSize = 14;
            }

            ChartSeriesPax = new AreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = App.TextProvider.GetText(2153),
                ShowTrackballInfo = true,
                EnableAnimation = true
            };

            ChartSeriesGuest = new AreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = App.TextProvider.GetText(2154),
                ShowTrackballInfo = true,
                EnableAnimation = true
            };

            ChartSeriesReentry = new AreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Reentry",
                ShowTrackballInfo = true,
                Alpha = 0.5f,
                VisibilityOnLegend = Visibility.Gone
            };

            ChartSeriesUnknown = new AreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Unknown",
                VisibilityOnLegend = Visibility.Gone,
                Color = Color.Gray,
                Alpha = 0.5f
            };

            primaryBarAxis = new DateTimeAxis();
            primaryBarAxis.Title.Text = "Time";
            secondaryBarAxis = new NumericalAxis();

            ChartColumnBarSeriesActual = new ColumnSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Actual",
                Color = Color.Orange,
                ShowTrackballInfo = true,
            };

            ChartColumnBarSeriesWeekly = new ColumnSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Color = Color.LightGray,
                Label = "Today's Projections",
                ShowTrackballInfo = true,
                Alpha = 0.4f,
            };
        }

        private void TrackballBehavior_TrackBallChanged(List<ChartPointInfo> pointsInfo)
        {
            try
            {
                System.Threading.Interlocked.Exchange(ref this.throttleCts, new System.Threading.CancellationTokenSource()).Cancel();

                Task.Delay(TimeSpan.FromMilliseconds(750), this.throttleCts.Token)
                    .ContinueWith(delegate
                    {
                        LoadPieChartFromTouch(pointsInfo);
                    },
                    System.Threading.CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.FromCurrentSynchronizationContext());
            }
            catch { }
        }

        private void LoadPieChartFromTouch(List<ChartPointInfo> pointsInfo)
        {
            try
            {
                var listData = pointsInfo.Select(c => c.ChartDataPoint as GraphDataCell);

                var actualTotal = (float)listData.Sum(c => c.Total);
                var customersTotal = (float)listData.Where(c => c.PassengerType == 0).Sum(c => c.Total);
                var guestsTotal = (float)listData.Where(c => c.PassengerType == 1).Sum(c => c.Total);

                if (actualTotal > 0)
                {
                    lblAgentStatisticsCurrentCustomersAmount.Text = string.Format(formatedCustomersText, actualTotal);
                    lblAgentStatisticsPrimaryAmount.Text = string.Format(formatedPrimaryText, customersTotal);
                    lblAgentStatisticsGuestsAmount.Text = string.Format(formatedGuestText, guestsTotal);

                    PieSeries.ItemsSource = pointsInfo.Select(x => new
                    {
                        Total = (x.ChartDataPoint as GraphDataCell).Total,
                        TypePassenger = GetTypePassenger((x.ChartDataPoint as GraphDataCell).PassengerType)
                    });
                }
                else
                {
                    NoDataPieChart();
                }

                pieChartView.Title.Text = string.Empty;
                pieChartView.ReloadChart();
            }
            catch { }
        }

        public static void setListViewHeightBasedOnChildren(ListView listView)
        {
            if (listView.Adapter == null)
            {
                // pre-condition
                return;
            }

            int totalHeight = listView.PaddingTop + listView.PaddingBottom;
            for (int i = 0; i < listView.Count; i++)
            {
                View listItem = listView.Adapter.GetView(i, null, listView);
                if (listItem.GetType() == typeof(ViewGroup))
                {
                    listItem.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
                }
                listItem.Measure(0, 0);
                totalHeight += listItem.MeasuredHeight;
            }

            listView.LayoutParameters.Height = totalHeight + (listView.DividerHeight * (listView.Count - 1));
        }

        private void TrafficButton_Click(object sender, EventArgs e)
        {
            dwellSelected = false;
            graphSelected = true;
            seekBar.Visibility = ViewStates.Visible;

            chartView.Axes.Clear();
            chartView.PrimaryAxis = null;
            chartView.SecondaryAxis = null;
            chartView.Series.Clear();
            chartView.Behaviors.Clear();

            lblAgentStatisticsCapacityAmount.Visibility = lblAgentStatisticsCapacityTitle.Visibility = ViewStates.Visible;
            lblAgentStatisticsCurrentCustomersAmount.Visibility = lblAgentStatisticsCurrentCustomersTitle.Visibility = ViewStates.Visible;
            lblAgentStatisticsGuestsAmount.Visibility = lblAgentStatisticsGuestsTitle.Visibility = ViewStates.Visible;
            lblAgentStatisticsPrimaryAmount.Visibility = lblAgentStatisticsPrimaryTitle.Visibility = ViewStates.Visible;

            chartView.Behaviors.Add(trackballBehavior);
            primaryAxis.LabelStyle.LabelFormat = "HH:mm";
            chartView.PrimaryAxis = primaryAxis;


            //Adding Secondary Axis for the Chart.
            secondaryAxis.Interval = 10;
            secondaryAxis.Minimum = 0;
            chartView.SecondaryAxis = secondaryAxis;

            #region Creating Series of main chart
            ChartSeriesPax.ColorModel.ColorPalette = ChartColorPalette.Custom;
            ChartGradientColor gradientColorPax = new ChartGradientColor() { StartPoint = new PointF(0.5f, 1), EndPoint = new PointF(0.5f, 0) };

            ChartGradientStop stopP1 = new ChartGradientStop() { Color = Color.ParseColor("#660e79d8"), Offset = 0 };
            ChartGradientStop stopP2 = new ChartGradientStop() { Color = Color.ParseColor("#0e79d8"), Offset = 1 };

            gradientColorPax.GradientStops.Add(stopP1);
            gradientColorPax.GradientStops.Add(stopP2);


            ChartGradientColorCollection gradientColorsPax = new ChartGradientColorCollection()
            {
                gradientColorPax//,
                //gradientColorPax2
            };
            ChartSeriesPax.ColorModel.CustomGradientColors = gradientColorsPax;
            ChartSeriesGuest.ColorModel.ColorPalette = ChartColorPalette.Custom;
            ChartGradientColor gradientColorGuest = new ChartGradientColor() { StartPoint = new PointF(0.5f, 1), EndPoint = new PointF(0.5f, 0) };
            ChartGradientStop stopG1 = new ChartGradientStop() { Color = Color.ParseColor("#669370db"), Offset = 0 };
            ChartGradientStop stopG2 = new ChartGradientStop() { Color = Color.ParseColor("#9370db"), Offset = 1 };
            gradientColorGuest.GradientStops.Add(stopG1);
            gradientColorGuest.GradientStops.Add(stopG2);
            ChartGradientColorCollection gradientColorsGuest = new ChartGradientColorCollection()
            {
                gradientColorGuest
            };
            ChartSeriesGuest.ColorModel.CustomGradientColors = gradientColorsGuest;

            chartView.Series.Add(ChartSeriesPax);
            chartView.Series.Add(ChartSeriesGuest);
            chartView.Series.Add(ChartSeriesReentry);
            chartView.Title.Visibility = ViewStates.Invisible;
            chartView.Legend.Visibility = Visibility.Visible;
            #endregion

            LoadData();
        }

        private async void DwellButton_Click(object sender, EventArgs e)
        {
            dwellSelected = true;
            graphSelected = false;
            seekBar.Visibility = ViewStates.Invisible;

            chartView.PrimaryAxis = null;
            chartView.SecondaryAxis = null;
            chartView.Series.Clear();
            chartView.Behaviors.Clear();

            lblAgentStatisticsCapacityAmount.Visibility = lblAgentStatisticsCapacityTitle.Visibility = ViewStates.Invisible;
            lblAgentStatisticsCurrentCustomersAmount.Visibility = lblAgentStatisticsCurrentCustomersTitle.Visibility = ViewStates.Invisible;
            lblAgentStatisticsGuestsAmount.Visibility = lblAgentStatisticsGuestsTitle.Visibility = ViewStates.Invisible;
            lblAgentStatisticsPrimaryAmount.Visibility = lblAgentStatisticsPrimaryTitle.Visibility = ViewStates.Invisible;

            try
            {
                #region Settings for ColumnBar Chart
                 
                primaryBarAxis.LabelStyle.LabelFormat = "HH:mm";
                secondaryBarAxis.Interval = (10);
                secondaryBarAxis.Minimum = (0);

                chartView.PrimaryAxis = primaryBarAxis;
                chartView.SecondaryAxis = secondaryBarAxis;
                chartView.SideBySideSeriesPlacement = false;


                var trackballBehaviorBar = new CustomSFChartTrackballBehavior();
                trackballBehaviorBar.LineStyle.StrokeWidth = (1);
                trackballBehaviorBar.LineStyle.StrokeColor = Color.Blue;
                trackballBehaviorBar.LabelDisplayMode = TrackballLabelDisplayMode.FloatAllPoints;
                chartView.Behaviors.Add(trackballBehaviorBar);

                chartView.Series.Add(ChartColumnBarSeriesActual);
                chartView.Series.Add(ChartColumnBarSeriesWeekly);
                chartView.Legend.Visibility = Visibility.Visible;
                chartView.Title.Text = DateTime.Now.ToLongDateString() + App.TextProvider.GetText(2157);
                #endregion

                LoadData();
            }
            catch (Exception ef)
            {

            }

        }



        private void LstLounges_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            currentLounge = ((JavaLounge)lstLounges.Adapter.GetItem(e.Position));
            (lstLounges.Adapter as LoungeAdapter).RefreshData(currentLounge);

            LoadData();
        }

        private void SelectLounge(int index)
        {
            currentLounge = ((JavaLounge)lstLounges.Adapter.GetItem(index));
            (lstLounges.Adapter as LoungeAdapter).RefreshData(currentLounge);

            LoadData();
        }

        private async void LoadData()
        {
            swipeRefreshLayout.Refreshing = true;

            await Task.Run(() =>
            {
                lblTitle.Text = $"{App.TextProvider.GetText(2145)} {currentLounge.WorkstationName}";
                lblSubTitle.Text = $"{currentLounge.AirportCode} {currentLounge.LoungeName}";

                toLocalDate = DateTime.Now;
                currentFromLocalDate = toLocalDate.Subtract(new TimeSpan(12, 0, 0));
                int intervalInSeconds = (int)Math.Ceiling(toLocalDate.Subtract(currentFromLocalDate).TotalSeconds / 30f);

                if (!dwellSelected)
                    App.GetRetrieveCachedLoungeOccupancies(currentLounge.LoungeID, currentLounge, currentFromLocalDate, toLocalDate, intervalInSeconds, OnLoungeChanged);
                else
                {
                    var dtNow = DateTime.Now;
                    toLocalDate = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 23, 0, 0, DateTimeKind.Local);
                    currentFromLocalDate = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 0, 0, 0, DateTimeKind.Local);

                    App.GetDwellLoungeOccupancies(currentLounge.LoungeID, currentLounge, currentFromLocalDate, toLocalDate, intervalInSeconds, OnLoungeChanged);
                }
            });
        }

        private string GetTypePassenger(decimal type)
        {
            switch (type)
            {
                case 0:
                    return "Pax";
                case 1:
                case 2:
                    return "Guest";
                case 3:
                    return "Reentry";
                case 5:
                    return "Refuse";
                default:
                    return "Unknown";
            }
        }

        public async Task RefreshLineChart(LoungeOccupancyElapsedData listData)
        {
            if (ChartSeriesPax == null) return;

            try
            {
                pieChartView.Title.Text = "";
                try
                {
                    var primaryIndex = (int)OccupancyValueType.Primary;
                    var guestIndex = (int)OccupancyValueType.Guest;

                    decimal maxSum = 0, totalOfPrimary = 0, totalOfGuests = 0;
                    if (chartView != null && listData.OccupancyRecords.Count > 0)
                    {
                        maxSum = listData.OccupancyRecords.Max(x => x.Values[primaryIndex] + x.Values[guestIndex]);
                        totalOfPrimary = listData.OccupancyRecords.Last().Values[primaryIndex];
                        totalOfGuests = listData.OccupancyRecords.Last().Values[guestIndex];

                        //total = totalOfPrimary + totalOfGuests;

                        if (maxSum > 0)
                        {
                            var graphMax = (int)Math.Ceiling((float)maxSum * 1.1);
                            ((NumericalAxis)chartView.SecondaryAxis).Maximum = graphMax;

                            var interval = (float)graphMax / 10;
                            if (interval < 1)
                                chartView.SecondaryAxis.Interval = (1);
                            else chartView.SecondaryAxis.Interval = (Math.Floor(interval));
                        }

                        //((SFDateTimeAxis)Chart.PrimaryAxis).Interval = //If we need to change the time interval on horizontal axis
                    }
                    ChartSeriesPax.ItemsSource = listData.OccupancyRecords.Select(x => new GraphDataCell()
                    {
                        Time = x.LocalTime,
                        Total = x.Values[primaryIndex],
                        PassengerType = primaryIndex
                    }).ToList();

                    ChartSeriesGuest.ItemsSource = listData.OccupancyRecords.Select(x => new GraphDataCell()
                    {
                        Time = x.LocalTime,
                        Total = x.Values[guestIndex],
                        PassengerType = guestIndex
                    }).ToList();

                    if (maxSum > 0)
                    {
                        pieChartView.Visibility = ViewStates.Visible;

                        var pieDataSource = new[] {
                                new {
                                    Total = totalOfPrimary,
                                    TypePassenger = GetTypePassenger(primaryIndex)},
                                new {
                                    Total = totalOfGuests,
                                    TypePassenger = GetTypePassenger(guestIndex)}
                            }.ToList();


                        PieSeries.ItemsSource = pieDataSource;

                        if (!pieDataSource.Max(c => c.Total > 0))
                        {
                            NoDataPieChart();
                            pieChartView.Title.Text = string.Format(titleOfPieChart, DateTime.Now.ToString("HH:mm"));
                        }

                        /*  lblAgentStatisticsCapacityAmount.Visibility = ViewStates.Invisible;
                          lblAgentStatisticsPrimaryAmount.Visibility = ViewStates.Invisible;
                          lblAgentStatisticsCurrentCustomersAmount.Visibility = ViewStates.Invisible;
                          lblAgentStatisticsGuestsTitle.Visibility = ViewStates.Invisible;*/

                        lblAgentStatisticsCapacityAmount.Text = string.Format(formatedLoungeText, listData.Capacity);
                        lblAgentStatisticsCurrentCustomersAmount.Text = string.Format(formatedPrimaryText, pieDataSource.Sum(c => c.Total));
                        lblAgentStatisticsPrimaryAmount.Text = string.Format(formatedCustomersText, totalOfPrimary);
                        lblAgentStatisticsGuestsAmount.Text = string.Format(formatedGuestText, totalOfGuests);
                    }
                    else
                    {
                        lblAgentStatisticsCapacityAmount.Text = string.Format(formatedLoungeText, listData.Capacity);
                        lblAgentStatisticsPrimaryAmount.Text = string.Format(formatedPrimaryText, 0f);
                        lblAgentStatisticsCurrentCustomersAmount.Text = string.Format(formatedCustomersText, 0f);
                        lblAgentStatisticsGuestsAmount.Text = string.Format(formatedGuestText, 0f);

                        NoDataPieChart();
                    }

                    pieChartView.Title.Text = string.Format(titleOfPieChart, DateTime.Now.ToString("HH:mm"));

                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Get Lounge Occupancy List call error.", ex);
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                }
                finally
                {
                    await Task.Delay(new TimeSpan(0, 0, (int)ChartSeriesGuest.AnimationDuration));
                }
            }
            catch
            {
            }
        }

        private void NoDataPieChart()
        {
            var list = new[]
            {
                        new { Total = 0, TypePassenger = GetTypePassenger(0) },
                        new { Total = 0, TypePassenger = GetTypePassenger(1) },
                        new { Total = 1, TypePassenger = GetTypePassenger(6) },
                    }.ToList();

            PieSeries.ItemsSource = list;
        }

        public async Task RefreshBarChart(LoungeOccupancyElapsedData data)
        {
            #region Random values
            var listData = new List<LoungeOccupancy>();
            Random randon = new Random();
            var dtNow = DateTime.Now;


            /*for (int i = 0; i < 24; i++)
            {
                if (i <= dtNow.Hour)
                {
                    listData.Add(new LoungeOccupancy()
                    {
                        Time = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, i, 0, 0),
                        OccupancyTotal = randon.Next(3, 25),
                        OccupancyTypePassenger = 0
                    });
                }

                listData.Add(new LoungeOccupancy()
                {
                    Time = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, i, 0, 0),
                    OccupancyTotal = randon.Next(3, 25),
                    OccupancyTypePassenger = 1
                });
            }*/

            foreach (var item in data.OccupancyRecords)
            {
                if (item.UtcTime.Day == dtNow.Day)
                {
                    listData.Add(new LoungeOccupancy()
                    {
                        Time = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, item.LocalTime.Hour, 0, 0),
                        OccupancyTotal = item.Values[0],
                        OccupancyTypePassenger = 0
                    });
                }
                else
                {
                    listData.Add(new LoungeOccupancy()
                    {
                        Time = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, item.LocalTime.Hour, 0, 0),
                        OccupancyTotal = item.Values[0],
                        OccupancyTypePassenger = 1
                    });
                }
            }


            #endregion


            try
            {
                decimal total = 0;
                if (chartView != null && listData.Count > 0)
                {
                    if ((total = listData.Max(x => x.OccupancyTotal)) > 0)
                    {
                        var graphMax = (int)Math.Ceiling((float)total * 1.1);
                        ((NumericalAxis)chartView.SecondaryAxis).Maximum = (graphMax);

                        var interval = (float)graphMax / 10;
                        if (interval < 1)
                            ((NumericalAxis)chartView.SecondaryAxis).Interval = (1);
                        else ((NumericalAxis)chartView.SecondaryAxis).Interval = (Math.Floor(interval));
                    }

                     ((DateTimeAxis)chartView.PrimaryAxis).IntervalType = DateTimeIntervalType.Hours;
                    ((DateTimeAxis)chartView.PrimaryAxis).Interval = (1);
                }

                ChartColumnBarSeriesActual.ItemsSource = listData.Where(c => c.OccupancyTypePassenger == 0).Select(x => new GraphDataCell()
                {
                    Time = x.Time.DateTime,
                    Total = (double)x.OccupancyTotal,
                    PassengerType = 0
                }).ToList();

                ChartColumnBarSeriesWeekly.ItemsSource = listData.Where(c => c.OccupancyTypePassenger == 1).Select(x => new GraphDataCell()
                {
                    Time = x.Time.DateTime,
                    Total = (double)x.OccupancyTotal,
                    PassengerType = 1
                }).ToList();

            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Get Lounge Occupancy List call error.", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }
            finally
            {
                await Task.Delay(new TimeSpan(0, 0, (int)ChartSeriesGuest.AnimationDuration));
            }
        }


        private void OnLoungeChanged(object data)
        {
            Activity.RunOnUiThread(async () =>
            {
                if(data == null)
                {
                    swipeRefreshLayout.Refreshing = false;
                    return;
                }

                if (data is LoungeOccupancyElapsedData == false)
                {
                    swipeRefreshLayout.Refreshing = false;
                    return;
                }
       
                listData = (LoungeOccupancyElapsedData)data;

                if (dwellSelected)
                    await RefreshBarChart(listData);
                else
                    await RefreshLineChart(listData);

                swipeRefreshLayout.Refreshing = false;
            });
        }

        private void ShowDwell(IEnumerable<ChartEntry> entries)
        {
            Activity.RunOnUiThread(() =>
            {
                var chart = new LineChart() { Entries = entries };
                trafficButton.SetTextColor(Color.Black);
                dwellButton.SetTextColor(Color.Blue);
            });
        }

        private void ShowTraffic(IEnumerable<ChartEntry> entries)
        {
            Activity.RunOnUiThread(() =>
            {
                var chart = new BarChart() { Entries = entries };

                trafficButton.SetTextColor(Color.Blue);
                dwellButton.SetTextColor(Color.Black);
            });
        }

        private DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks, dt.Kind);
        }

        private DateTime RoundMinute(DateTime dt)
        {
            if (roundMinutes == 1)
            {
                return dt;
            }

            int lowerMinute = ((int)(dt.Minute / roundMinutes)) * roundMinutes;
            int higherMinute = lowerMinute + roundMinutes;

            if (higherMinute - dt.Minute > dt.Minute - lowerMinute)
            {
                return dt.AddMinutes(-(dt.Minute - lowerMinute));
            }
            else
            {
                return dt.AddMinutes(higherMinute - dt.Minute);
            }
        }
        #endregion
    }

    public class RefreshListener : Java.Lang.Object, SwipeRefreshLayout.IOnRefreshListener
    {
        public event EventHandler Refreshed;

        public void OnRefresh()
        {
            Refreshed?.Invoke(null, EventArgs.Empty);
        }
    }

    public class SeekbarListener : Java.Lang.Object, SeekBar.IOnSeekBarChangeListener
    {
        public event EventHandler ProgressChanged;
        public event EventHandler StartTrackingTouch;
        public event EventHandler StopTrackingTouch;

        public void OnProgressChanged(SeekBar seekBar, int progress, bool fromUser)
        {
            ProgressChanged?.Invoke(this, EventArgs.Empty);
        }

        public void OnStartTrackingTouch(SeekBar seekBar)
        {
            StartTrackingTouch?.Invoke(this, EventArgs.Empty);
        }

        public void OnStopTrackingTouch(SeekBar seekBar)
        {
            StopTrackingTouch?.Invoke(this, EventArgs.Empty);
        }
    }

    public class GraphDataCell
    {
        public DateTime Time { get; internal set; }
        public double Total { get; internal set; }
        public int PassengerType { get; internal set; }
    }

    public class CustomSFChartTrackballBehavior : ChartTrackballBehavior
    {
        public event TrackedBallHandler TrackBallChanged;

        protected override View GetView(ChartSeries series, object data, int index)
        {
            LinearLayout layout = new LinearLayout(Chart.Context);
            layout.Orientation = Orientation.Vertical;

            TextView text = new TextView(Chart.Context);
            text.SetTextColor(Color.White);
            text.Text = (data as GraphDataCell).Total.ToString();
            layout.AddView(text);

            return layout;
        }

        public async override void Hide()
        {
            await Task.Delay(1000);

            base.Hide();
        }

        protected override void OnLabelsGenerated(List<ChartPointInfo> chartPointsInfo)
        {
            base.OnLabelsGenerated(chartPointsInfo);

            TrackBallChanged?.Invoke(chartPointsInfo);
        }
    }


    public delegate void TrackedBallHandler(List<ChartPointInfo> pointsInfo);
}