﻿
using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System.Timers;
using Android.Views.Animations;
using Android.Animation;
using Android.Graphics;
using Ieg.Mobile.Localization;
using System.Collections.Generic;
using Microsoft.AppCenter.Analytics;
using AIMS.Droid.Library;

namespace AIMS.Droid
{
    [Activity (Label = "Agent Profile")]			
	public class AgentProfileActivity : BaseFragment
    {
        #region Elements
        View view;
        LinearLayout pnlAdvanced;

        TextView lblLanguage, lblAgentFullName, lblAgentRole, lblAgentLoungeName, lblAgentAirportName, lblOccupancy, lblAdvancedSettings,
            btnTerms, btnPrivacy;
        ImageView imgAgentProfilePic, imgAgentAirline, imgAgentMap;
        ProgressBar occupancyProgressBar;
        //ToggleButton btnFrontCamera;
        Spinner drpLanguageSelector;
        ImageButton btnAdvancedSettings;
        //Button btnTerms, btnPrivacy;
        #endregion

        #region Fields
        Timer refresherTimer;
        bool isInitialized = false;
        private TextView progressBarinsideText;
        const int OccupancyCheckerInterval = 4000;
        #endregion

        #region Properties

        #endregion

        #region Methods
        //private void InitializeTimer()
        //{
        //    if (!isInitialized)
        //    {
        //        isInitialized = true;
        //        refresherTimer = new Timer(OccupancyCheckerInterval);
        //        refresherTimer.Elapsed += OnRefreshEvent;
        //        refresherTimer.Enabled = true;//TODO: testing
        //    }
        //}

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Terms and Conditions + Privacy buttons + label
            lblAgentRole.Text = App.TextProvider.GetText(2012);//TODO: role name from server
            lblOccupancy.Text = App.TextProvider.GetText(2039);//Occupancy
            lblLanguage.Text = App.TextProvider.GetText(2038);//Language
            lblAdvancedSettings.Text = App.TextProvider.GetText(2046);//Advanced Settings
        }
        
        #endregion

        #region Events
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.AgentProfile, container, false);

            #region Initialize Controls and Elements
            lblAgentFullName = view.FindViewById<TextView>(Resource.Id.lblAgentFullName);
            lblAgentRole = view.FindViewById<TextView>(Resource.Id.lblAgentRole);
            lblAgentLoungeName = view.FindViewById<TextView>(Resource.Id.lblAgentLoungeName);
            lblAgentAirportName = view.FindViewById<TextView>(Resource.Id.lblAgentAirportName);
            lblOccupancy = view.FindViewById<TextView>(Resource.Id.lblOccupancy);
            lblLanguage = view.FindViewById<TextView>(Resource.Id.lblLanguage);
            lblAdvancedSettings = view.FindViewById<TextView>(Resource.Id.lblAdvancedSettings);
            pnlAdvanced = view.FindViewById<LinearLayout>(Resource.Id.pnlAdvanced);

            btnTerms = view.FindViewById<TextView>(Resource.Id.btnTerms);
            btnTerms.Click += (sender, e) =>
            {
                var webViewIntent = new Intent(this.Context, typeof(WebViewerActivity));
                webViewIntent.PutExtra("title", "Mobile Lounge | Terms and Conditions");
                webViewIntent.PutExtra("url", "https://www.google.ca/intl/en/policies/terms/regional.html");
                //webViewIntent.PutExtra("url", "http://AIMSLounge.com/Mobile/TermsAndConditions/");//TODO: must be uncommented and tested (after uploads)
                StartActivity(webViewIntent);
            };
            btnPrivacy = view.FindViewById<TextView>(Resource.Id.btnPrivacy);
            btnPrivacy.Click += (sender, e) =>
            {
                var webViewIntent = new Intent(this.Context, typeof(WebViewerActivity));
                webViewIntent.PutExtra("title", "Mobile Lounge | Privacy Policy");
                webViewIntent.PutExtra("url", "https://www.google.ca/intl/en/policies/privacy/");
                //webViewIntent.PutExtra("url", "http://AIMSLounge.com/Mobile/PrivacyPolicy/");//TODO: must be uncommented and tested (after uploads)
                StartActivity(webViewIntent);
            };



            occupancyProgressBar = (ProgressBar)view.FindViewById<ProgressBar>(Resource.Id.occupancyProgressBar);
            occupancyProgressBar.SetBackgroundColor(Android.Graphics.Color.LightGray);
            progressBarinsideText = view.FindViewById<TextView>(Resource.Id.progressBarinsideText);
            App.OccupancyPercentageChanged += OccupancyPercentageChanged;
            App.OnRefreshEvent(null, null);

            drpLanguageSelector = view.FindViewById<Spinner>(Resource.Id.drpLanguageSelector);
            var languageAdapter = new DropDownLanguageAdapter(this.Context);
            drpLanguageSelector.Adapter = languageAdapter;
            drpLanguageSelector.SetSelection(languageAdapter.GetLanguagePosition(App.TextProvider.SelectedLanguage));
            drpLanguageSelector.ItemSelected += (sender, e) =>
            {
                App.TextProvider.SelectedLanguage = languageAdapter.GetLanguage(drpLanguageSelector.SelectedItemPosition);
            };

            btnAdvancedSettings = view.FindViewById<ImageButton>(Resource.Id.btnAdvancedSettings);
            btnAdvancedSettings.Click += (sender, e) =>
            {
                if (!App.IsDeveloperModeEnabled)
                {
                    StartActivity(new Intent(this.Context, typeof(LoginDialogActivity)));
                }
            };

            //btnFrontCamera = view.FindViewById<ToggleButton>(Resource.Id.btnFrontCamera);
            //btnFrontCamera.Checked = App.UseFrontCamera;
            //btnFrontCamera.CheckedChange += (sender, e) =>
            //{
            //    App.UseFrontCamera = btnFrontCamera.Checked;
            //};

            imgAgentProfilePic = view.FindViewById<ImageView>(Resource.Id.imgAgentProfilePic);
            imgAgentAirline = view.FindViewById<ImageView>(Resource.Id.imgAgentAirline);
            imgAgentMap = view.FindViewById<ImageView>(Resource.Id.imgAgentMap);
            #endregion

            if (AIMSApp.IsDemoVersion)//TODO: Demo condition
            {
                // lblOccupancy.Visibility = ViewStates.Visible;
                imgAgentMap.Visibility = ViewStates.Gone;
                // occupancyProgressBar.Visibility = ViewStates.Visible;
            }
            else
            {
                // lblOccupancy.Visibility = ViewStates.Gone;
                imgAgentMap.Visibility = ViewStates.Gone;
                //occupancyProgressBar.Visibility = ViewStates.Gone;
            }

            LoadUITexts(App.TextProvider.SelectedLanguage);

            lblAgentAirportName.Text = MembershipProvider.Current.LoggedinUser.AirportName;
            lblAgentFullName.Text = string.Format("{0} {1}", MembershipProvider.Current.LoggedinUser.FirstName, MembershipProvider.Current.LoggedinUser.LastName);
            lblAgentLoungeName.Text = MembershipProvider.Current.SelectedLounge.LoungeName;

            //imgAgentAirline.SetImageResource();
            //imgAgentProfilePic.SetImageBitmap();
            var airline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
            if (airline != null)
            {
                if (AIMSApp.LoungeBitmapImage != null)
                {
                    imgAgentAirline.SetImageBitmap(AIMSApp.LoungeBitmapImage);
                }
                else
                {
                    if (airline.AirlineLogoBytes != null && airline.AirlineLogoBytes.Length > 0)
                    {
                        var bm = BitmapFactory.DecodeByteArray(airline.AirlineLogoBytes, 0, airline.AirlineLogoBytes.Length);
                        if (bm != null)
                        {
                            imgAgentAirline.SetImageBitmap(bm);
                        }
                    }
                    else
                    {
                        //(new JavaAirline(airline)).LoadImage(imgAgentAirline, this.Context, App.ImageRepository);//Passing new object of ImageRepository
                        var item = (new JavaAirline(airline));
                        /*ImageRepository.Current*/
                        App.ImageRepository.LoadImage(item.ImageHandle, (imageResult) =>
                        {
                            if (imageResult == null)
                            {
                                LogsRepository.AddError("Error in AgentProfileActivity in loading airline image", "result is null");

                                Analytics.TrackEvent("LanguageSelectionActivity", new Dictionary<string, string>
                                {
                                { "Error in AgentProfileActivity in loading airline image", "result is null" },
                                });


                                return;
                            }

                            if (imageResult.IsSucceeded)
                            {
                                var bytes = imageResult.ReturnParam;

                                //item.SaveImage(bytes);//Removed IImageItem and it is automatically saved in the ImageRepository

                                if (bytes == null || bytes.Length == 0) return;

                                try
                                {
                                    var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                                    if (bm != null)
                                    {
                                        this.Activity.RunOnUiThread(() =>
                                        {
                                            imgAgentAirline.SetImageBitmap(bm);
                                        });
                                        //bm.Recycle();
                                        //bm = null;
                                    }
                                }
                                catch (System.Exception eex)
                                {
                                    Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                                }
                            }
                        });
                    }
                }
            }
         
            // InitializeTimer();

            return view;
        }

        private void OccupancyPercentageChanged()
        {
            if (this.Activity != null)
            {
                ((MainActivity)this.Activity).RunOnUiThread(() =>
                {
                    progressBarinsideText.Text = $"{ MembershipProvider.Current.SelectedLounge.LastPercentage.ToString("N0")}%";

                    occupancyProgressBar.ProgressDrawable.SetColorFilter(Util.GetProgressColor(MembershipProvider.Current.SelectedLounge.LastPercentage),
                                                                      Android.Graphics.PorterDuff.Mode.SrcIn);

                    ObjectAnimator animation = ObjectAnimator.OfInt(occupancyProgressBar, "progress", occupancyProgressBar.Progress, MembershipProvider.Current.SelectedLounge.LastPercentage);
                    animation.SetDuration(1000);
                    animation.SetInterpolator(new DecelerateInterpolator());
                    animation.Start();
                });
            }
        }

        public override void OnResume()
        {
            base.OnResume();
            if (App.IsDeveloperModeEnabled)
            {
                pnlAdvanced.Visibility = ViewStates.Gone;
            }
            else
            {
                pnlAdvanced.Visibility = ViewStates.Visible;
            }
            ((MainActivity)this.Activity).RefreshTabs();//TODO: it must be where the developer login ends successfully
        }

        //private void OnRefreshEvent(object sender, ElapsedEventArgs e)
        //{
        //    //TODO: call getLoungeOccupancy from service and move the callback bellow
        //    #region getOccupancy callback
        //    Activity.RunOnUiThread(() => {
        //        int percentage = (new Random()).Next(60, 80);//TODO: the real percentage result (<= 100)

        //        ObjectAnimator animation = ObjectAnimator.OfInt(occupancyProgressBar, "progress", occupancyProgressBar.Progress, percentage);
        //        animation.SetDuration(1200);
        //        animation.SetInterpolator(new DecelerateInterpolator());
        //        animation.Start();
        //        lblOccupancy.Text = string.Format("Occupancy [{0}%]", percentage);
        //    });
        //    #endregion
        //}

        //protected override void OnCreate (Bundle savedInstanceState)
        //{
        //	base.OnCreate (savedInstanceState);
        //	SetContentView (Resource.Layout.AgentProfile);
        //  // Create your application here
        //  app = (AIMSApp)Application;
        //}

        //public override void OnBackPressed()
        //{
        //    if (app.MainForm.IsAgentMenuOpen)
        //        app.MainForm.AgentProfileWindowToggle();
        //    //base.OnBackPressed();
        //}
        #endregion
    }
}