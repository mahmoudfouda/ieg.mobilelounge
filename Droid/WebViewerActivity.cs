using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Content.PM;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS | IEG-America Service Statements", Icon = "@drawable/Icon", WindowSoftInputMode = Android.Views.SoftInput.AdjustResize, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, Theme = "@android:style/Theme.Dialog")]
    public class WebViewerActivity : BaseActivity
    {
        #region Fields
        string currentUrl = "";
        #endregion

        #region Elements
        WebView webView;
        TextView lblWebTitle, lblWebLoading;
        ProgressBar webProgressBar;
        #endregion

        #region Methods

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //No other languages for [Terms of Service] and [License Agreement] and [Privacy Policy]
            //TODO: Otherwise, we need to refresh the page for other langs
            lblWebLoading.Text = App.TextProvider.GetText(2001);
        }

        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.RequestFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.WebViewer);
            Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            #region Initialize Components
            webProgressBar = FindViewById<ProgressBar>(Resource.Id.pB1);
            lblWebLoading = FindViewById<TextView>(Resource.Id.tV1);
            lblWebTitle = FindViewById<TextView>(Resource.Id.lblWebTitle);
            webView = FindViewById<WebView>(Resource.Id.webview);
            webView.Settings.JavaScriptEnabled = true;
            webView.Settings.CacheMode = CacheModes.NoCache;
            var chromeClient = new MyChromeClient
            {
                ProgressChanged = (sender, progress) =>
                {
                    if (progress < 100 && webProgressBar.Visibility == ViewStates.Gone)
                    {
                        webProgressBar.Visibility = ViewStates.Visible;
                        lblWebLoading.Visibility = ViewStates.Visible;
                    }

                    webProgressBar.Progress = progress;
                    if (progress == 100)
                    {
                        webProgressBar.Visibility = ViewStates.Gone;
                        lblWebLoading.Visibility = ViewStates.Gone;
                    }
                }
            };

            webView.SetWebChromeClient(chromeClient);
            webView.SetWebViewClient(new MyWebViewClient());
            #endregion

            LoadUITexts(null);

            currentUrl = Intent.GetStringExtra("url") ?? "about:blank";
            lblWebTitle.Text = Intent.GetStringExtra("title") ?? "IEG-America | Statements of Services";
        }

        protected override void OnResume()
        {
            base.OnResume();

            webView.LoadUrl(currentUrl);
        }

        protected override void OnPause()
        {
            base.OnPause();

            webView.StopLoading();
        }

        public override bool OnKeyDown(Android.Views.Keycode keyCode, Android.Views.KeyEvent e)
        {
            if (keyCode == Keycode.Back && webView.CanGoBack())
            {
                webView.GoBack();
                return true;
            }

            return base.OnKeyDown(keyCode, e);
        }
    }
    public class MyChromeClient : WebChromeClient
    {
        public EventHandler<int> ProgressChanged;
        public override void OnProgressChanged(WebView view, int progress)
        {
            if (ProgressChanged != null)
                ProgressChanged.Invoke(this, progress);
        }
    }

    public class MyWebViewClient : WebViewClient
    {
        public override bool ShouldOverrideUrlLoading(WebView view, string url)
        {
            view.LoadUrl(url);
            return true;
        }
    }
}