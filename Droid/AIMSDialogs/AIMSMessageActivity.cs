using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.Graphics;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS", Icon = "@drawable/Icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, Theme = "@android:style/Theme.Dialog")]
    public class AIMSMessageActivity : Activity
    {
        #region Elements
        TextView lblTitle, lblText;
        Button btnOk;
        #endregion

        #region Events
        EventHandler cancelEvent;
        #endregion

        #region Fields
        //string title, text;
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.RequestFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AIMSMessage);
            Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            this.lblTitle = FindViewById<TextView>(Resource.Id.lblAIMSMessageTitle);
            this.lblText = FindViewById<TextView>(Resource.Id.lblAIMSMessageText);
            this.btnOk = FindViewById<Button>(Resource.Id.btnAIMSMessageButton);

            this.lblTitle.Text = Intent.GetStringExtra("title") ?? "";
            this.lblText.Text = Intent.GetStringExtra("text") ?? "";

            this.btnOk.Click += (sender, e) => {
                Finish();
                if (cancelEvent != null)
                    cancelEvent.Invoke(this,null);
            };
        }

        //public void Show(string title, string text)
        //{
        //    Dialog dialog = new Dialog(this);
        //    dialog.Window.RequestFeature(WindowFeatures.NoTitle);
        //    dialog.Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
        //    // layout to display
        //    dialog.SetContentView(Resource.Layout.AIMSMessage);

        //    // set color transpartent
        //    dialog.Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

        //    dialog.Show();
        //}

        //public AIMSMessageActivity(string title, string text, EventHandler cancelCallback)
        //{
        //    cancelEvent = cancelCallback;
        //    this.title = title;
        //    this.text = text;
        //}
    }
}