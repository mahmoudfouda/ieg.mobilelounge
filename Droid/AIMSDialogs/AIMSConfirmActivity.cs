using System;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.Graphics;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS", Icon = "@drawable/Icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, Theme = "@android:style/Theme.Dialog")]
    public class AIMSConfirmActivity : Activity
    {
        #region Elements
        TextView lblTitle, lblText;
        Button btnOk, btnCancel;
        #endregion

        #region Events
        public static EventHandler OkEvent, CancelEvent;
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.RequestFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AIMSConfirm);
            Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            this.lblTitle = FindViewById<TextView>(Resource.Id.lblAIMSConfirmTitle);
            this.lblText = FindViewById<TextView>(Resource.Id.lblAIMSConfirmText);
            this.btnOk = FindViewById<Button>(Resource.Id.btnAIMSConfirmOk);
            this.btnCancel = FindViewById<Button>(Resource.Id.btnAIMSConfirmCancel);

            this.lblTitle.Text = Intent.GetStringExtra("title") ?? "";
            this.lblText.Text = Intent.GetStringExtra("text") ?? "";

            this.btnOk.Click += (sender, e) => {
                Finish();
                if (OkEvent != null)
                    OkEvent.Invoke(this, null);
            };
            this.btnCancel.Click += (sender, e) => {
                Finish();
                if (CancelEvent != null)
                    CancelEvent.Invoke(this, null);
            };
        }

    }
}