﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;
using Android.Graphics.Drawables;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
	[Activity (Label = "AIMS", Icon = "@drawable/Icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, Theme = "@android:style/Theme.Dialog")]
	public class CardSelectionActivity : BaseActivity
    {
        #region Constants
        const int minimumCardsForSearch = 12;
        #endregion

        #region Fields
        ArrayAdapter autoCompleteAdapter;
        #endregion

        #region Elements
        RelativeLayout pnlCardsSearch;
        LinearLayout pnlCardsTitle;

        TextView lblCardsTitle;
        AutoCompleteTextView txtCardsSearch;
        ImageView imgCardsTitle;
        //Button btnCardSearch;
        ImageButton btnCardResetSearch, btnCardSearch;
        GridView grdCards;
        CardsAdapter cardsAdapter;
        #endregion

        #region Methods
        public void BeginSearch()
        {
            ShowLoading(App.TextProvider.GetText(2505));//"Retrieving list of cards"
            cardsAdapter.SearchCards(this.txtCardsSearch.Text);
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
            txtCardsSearch.Hint = string.Format("{0}...", App.TextProvider.GetText(2008));//Search...
        }
        #endregion

        #region Events
        protected override void OnCreate (Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.RequestFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            base.OnCreate (savedInstanceState);
            SetContentView (Resource.Layout.CardSelection);
            Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));
            App.CardSelectionForm = this;
            //ActionBar.Hide();

            if (MembershipProvider.Current.SelectedAirline != null)
			{
                pnlCardsSearch = FindViewById<RelativeLayout>(Resource.Id.pnlCardSearch);
                txtCardsSearch= FindViewById<AutoCompleteTextView>(Resource.Id.txtCardSearch);
                btnCardSearch = FindViewById<ImageButton>(Resource.Id.btnCardSearch);
                btnCardSearch.Click += delegate (object sender, EventArgs e) { BeginSearch(); };
                btnCardResetSearch = FindViewById<ImageButton>(Resource.Id.btnCardResetSearch);
                btnCardResetSearch.Click += delegate (object sender, EventArgs e) {
                    txtCardsSearch.Text = "";
                    ShowLoading(App.TextProvider.GetText(2505));
                    cardsAdapter.LoadCards();
                };

                pnlCardsTitle = FindViewById<LinearLayout>(Resource.Id.pnlCardsTitle);
                imgCardsTitle = FindViewById<ImageView>(Resource.Id.imgCardsTitle);
                lblCardsTitle = FindViewById<TextView>(Resource.Id.lblCardsTitle);

                grdCards = FindViewById<GridView> (Resource.Id.grdCards);
                grdCards.ItemClick += GrdCards_ItemClick;

                cardsAdapter = new CardsAdapter(this);
                
                cardsAdapter.AdapterChanged += (notUsed) => {
                    RunOnUiThread(new Action(() => {
                        grdCards.Adapter = cardsAdapter;
                        if(autoCompleteAdapter == null) { 
                            autoCompleteAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleDropDownItem1Line, cardsAdapter.GetAutoCompleteOptions());
                            txtCardsSearch.Adapter = autoCompleteAdapter;
                            txtCardsSearch.ItemClick += TxtCardsSearch_ItemClick;
                            if (cardsAdapter.Count < minimumCardsForSearch)
                                pnlCardsSearch.Visibility = ViewStates.Gone;
                        }

                    }));
                    HideLoading();
                };
                LoadUITexts(App.TextProvider.SelectedLanguage);
                ShowLoading(App.TextProvider.GetText(2505));//"Retrieving list of cards"
                cardsAdapter.LoadCards();

                
                lblCardsTitle.Text = Title = MembershipProvider.Current.SelectedAirline.Name;
                if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes == null || MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length == 0)
                {
                    if(MembershipProvider.Current.SelectedAirline.LogoResourceID != 0)
                        imgCardsTitle.SetImageResource(MembershipProvider.Current.SelectedAirline.LogoResourceID);
                    else
                        ImageAdapter.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (res)=> {
                            if (res != null)
                            {
                                var bm = BitmapFactory.DecodeByteArray(res, 0, res.Length);
                                if (bm != null)
                                {
                                    bm = ImageAdapter.HighlightImage(bm);
                                    imgCardsTitle.SetImageBitmap(bm);
                                }
                            }
                        });
                }
                else
                {
                    var bm = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedAirline.AirlineLogoBytes, 0, MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length);
                    if (bm != null)
                    {
                        bm = ImageAdapter.HighlightImage(bm);
                        imgCardsTitle.SetImageBitmap(bm);
                        //bm.Recycle();
                        //bm = null;
                    }
                }
            }
		}

        private void TxtCardsSearch_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            //ShowLoading("Searching the cards");//TODO: recommended to add this text
            ShowLoading(App.TextProvider.GetText(2505));//"Retrieving list of cards"

            var card = autoCompleteAdapter.GetItem(e.Position);
            if (card == null) return;
            cardsAdapter.SearchCards(card.ToString());
        }

        private void GrdCards_ItemClick(object sender, AdapterView.ItemClickEventArgs args)
        {
            var card = (JavaCard)grdCards.Adapter.GetItem(args.Position);
            if (card != null)
            {
                MembershipProvider.Current.SelectedPassenger = new Models.Passenger {
                    CardID = card.ID,
                    AirlineId = card.AirlineID
                };
                //MembershipProvider.Current.SelectedCard = card;

                App.ContinueScanning = false;//When agent wants to input without barcode
//                                             App.MainForm.StartActivity(new Intent(App.MainForm, typeof(ManualInputActivity)));//TODO: CardDetailsActivity

                App.MainForm.StartActivity(new Intent(App.MainForm, typeof(ManualEntryActivity)));//TODO: CardDetailsActivity


            }
            //else
            //{
            //    Toast.MakeText(this, "No Card has been selected..", ToastLength.Short).Show();
            //}
        }
        #endregion
    }
}

