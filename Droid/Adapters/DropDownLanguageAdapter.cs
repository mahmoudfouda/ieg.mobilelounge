using Android.Content;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    public class DropDownLanguageAdapter : LanguageAdapter
    {
        public DropDownLanguageAdapter(Context c) : base(c)
        {
        }

        public Language? GetLanguage(int position)
        {
            try
            {
                return (Language)languages[position].Id;
            }
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }
            return null;
        }

        public int GetLanguagePosition(Language? language)
        {
            if (languages == null || !language.HasValue) return -1;
            var langId = (int)language;
            return languages.FindIndex(x => x.Id == langId);
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View layout;

            if (convertView == null)
            {
                LayoutInflater inflater = LayoutInflater.FromContext(context);
                layout = inflater.Inflate(Resource.Drawable.languageitem_dropdown, null);
            }
            else
            {
                layout = (RelativeLayout)convertView;
            }

            var lblDrpLanguageName = layout.FindViewById<TextView>(Resource.Id.lblDrpLanguageName);
            var imgDrpLanguageImage = layout.FindViewById<ImageView>(Resource.Id.imgDrpLanguageImage);

            lblDrpLanguageName.Text = languages[position].Name;
            imgDrpLanguageImage.SetImageResource(languages[position].ImageResourceId);

            return layout;
        }
    }
}