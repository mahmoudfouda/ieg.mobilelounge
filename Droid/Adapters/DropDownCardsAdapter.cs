﻿using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Graphics;
//using AIMS.DAL;

namespace AIMS.Droid
{
	public class DropDownCardsAdapter : CardsAdapter
	{
		public DropDownCardsAdapter (Context c) : base(c)
		{
		}
        
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View gridView;
			ImageView imageView;
            TextView lblName;
			if (convertView == null) { 
				LayoutInflater inflater = LayoutInflater.FromContext (context);
				gridView = inflater.Inflate(Resource.Drawable.carditem_dropdown, null);//changed
			} else {
				gridView = (LinearLayout)convertView;
			}
			imageView =  gridView.FindViewById<ImageView>(Resource.Id.card_item_image);
            lblName = gridView.FindViewById<TextView>(Resource.Id.lblCardName);
            lblName.Text = cards[position].DisplayName;

            if (cards[position].CardPictureBytes != null && cards[position].CardPictureBytes.Length > 0)
            {
                try
                {
                    var bm = BitmapFactory.DecodeByteArray(cards[position].CardPictureBytes, 0, cards[position].CardPictureBytes.Length);
                    if (bm != null)
                    {
                        imageView.SetImageBitmap(bm);
                        //bm.Recycle();
                        //bm = null;
                    }
                }
                catch (System.Exception eex)
                {
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                }
            }
            else if (cards[position].PictureResourceID == 0)
            {
                imageView.SetImageResource(Resource.Drawable.not_available_pic_off);

                //Triggering image load...
                //cards[position].LoadImage(imageView, context, app.ImageRepository);
                LoadImage(imageView, cards[position].ImageHandle);

                //lblName.Visibility = ViewStates.Visible;
                //imageView.Visibility = ViewStates.Gone;
            }
            else
                imageView.SetImageResource(cards[position].PictureResourceID);
            

            return gridView;
		}
	}
}

