using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using AIMS.Models;

namespace AIMS.Droid
{
    public class JavaMessage : Message
    {
        public JavaMessage()
            : this(new Message())
        { }

        public JavaMessage(Message item)
        {
            Id = item.Id;
            Time = item.Time;
            Title = item.Title;
            Body = item.Body;
            Handle = item.Handle;
            From = item.From;
            Attachment = item.Attachment;
            BulletinId = item.BulletinId;
            Type = item.Type;
        }

        public static implicit operator Java.Lang.Object(JavaMessage item)
        {
            return new JavaObjectBase<JavaMessage>(item);
        }

        public static explicit operator JavaMessage(Java.Lang.Object item)
        {
            return (item as JavaObjectBase<JavaMessage>).Item;
        }
    }
    class MessagesAdapter : BaseAdapter
    {
        Context context;
        List<JavaMessage> messages = new List<JavaMessage>();
        MessageRepository repository;

        public event ReporitoryChangedHandler AdapterChanged;

        public MessagesAdapter(Context c)
        {
            context = c;
            repository = new MessageRepository();
            repository.OnRepositoryChanged += Messages_OnRepositoryChanged;
            //messages =  MockRepository.Current.Messages.Select(x => new JavaMessage(x)).ToList();
        }

        private void Messages_OnRepositoryChanged(object repository)
        {
            if (repository == null) return;
            else if (repository is IEnumerable<Message>)
            {
                messages = ((IEnumerable<Message>)repository).Select(x => new JavaMessage(x)).ToList();
            }
            else return;

            //NotifyDataSetChanged();//TODO: why not calling this?

            if (AdapterChanged != null)
                AdapterChanged.Invoke(messages);
        }

        public void LoadMessages()
        {
            repository.LoadAllMessages();
        }

        public override int Count
        {
            get { return messages.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return messages[position];
        }

        public override long GetItemId(int position)
        {
            return messages[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View linearLayout;

            if (convertView == null)
            {
                LayoutInflater inflater = LayoutInflater.FromContext(context);
                linearLayout = inflater.Inflate(Resource.Drawable.messageitem, null);
            }
            else {
                linearLayout = (LinearLayout)convertView;
            }
            var lblMessageTitle = linearLayout.FindViewById<TextView>(Resource.Id.lblMessageTitle);
            lblMessageTitle.Text = messages[position].Title;
            var lblMessageFrom = linearLayout.FindViewById<TextView>(Resource.Id.lblMessageFrom);
            lblMessageFrom.Text = messages[position].From;
            var lblMessageTime = linearLayout.FindViewById<TextView>(Resource.Id.lblMessageTime);
            lblMessageTime.Text = string.Format("{0:yyyy/MM/dd} - {1}", messages[position].Time, messages[position].Time.ToShortTimeString());
            
            return linearLayout;
        }

        public void LoadFullMessage(int position, ReporitoryChangedHandler<Message> callback)
        {
            if (callback == null || repository == null) return;
            if (string.IsNullOrEmpty(messages[position].Body) && !string.IsNullOrEmpty(messages[position].Handle))
            {
                repository.LoadMessageBody(messages[position].Handle, (messageBody) => {
                    messages[position].Body = messageBody;
                    messages[position].Handle = string.Empty;//making it not to load twice (if doesn't have body)
                    callback.Invoke(messages[position]);
                });
            }
            else callback.Invoke(messages[position]);
        }
    }
}