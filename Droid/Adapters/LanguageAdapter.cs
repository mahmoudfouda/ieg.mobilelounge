using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    public class JavaLanguage
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ImageResourceId { get; set; }

        public JavaLanguage()
            : this(-1, "Not selected", Resource.Drawable.not_available_pic_off) { }

        public JavaLanguage(int languageId, string languageName, int imageResourceId)
        {
            Id = languageId;
            Name = languageName;
            ImageResourceId = imageResourceId;
        }

        public static implicit operator Java.Lang.Object(JavaLanguage item)
        {
            return new JavaObjectBase<JavaLanguage>(item);
        }

        public static explicit operator JavaLanguage(Java.Lang.Object item)
        {
            return (item as JavaObjectBase<JavaLanguage>).Item;
        }
    }

    public class LanguageAdapter : BaseAdapter
    {
        protected Context context;
        protected List<JavaLanguage> languages;
        
        public LanguageAdapter(Context c)
        {
            context = c;
            languages = LanguageRepository.Current.Languages.Select(x=> new JavaLanguage {
                Id = (int)x.Key,
                Name = x.Value,
                ImageResourceId = GetImageResourceId((int)x.Key)
            }).ToList();
        }

        protected int GetImageResourceId(int id)
        {
            switch (id)//TODO: Correct it (Bad practise)
            {
                case 0:
                    return Resource.Drawable.Flag_0;
                case 1:
                    return Resource.Drawable.Flag_1;
                case 2:
                    return Resource.Drawable.Flag_2;
                default:
                    return Resource.Drawable.not_available_pic_off;
            }
        }

        public override int Count
        {
            get { return languages.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return languages[position];
        }

        public override long GetItemId(int position)
        {
            return languages[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View layout;

            if (convertView == null)
            {
                LayoutInflater inflater = LayoutInflater.FromContext(context);
                layout = inflater.Inflate(Resource.Drawable.languageitem, null);
            }
            else
            {
                layout = (RelativeLayout)convertView;
            }

            var lblLanguageName = layout.FindViewById<TextView>(Resource.Id.lblLanguageName);
            var imgLanguageImage = layout.FindViewById<ImageView>(Resource.Id.imgLanguageImage);

            lblLanguageName.Text = languages[position].Name;
            imgLanguageImage.SetImageResource(languages[position].ImageResourceId);
            
            return layout;
        }
    }
}