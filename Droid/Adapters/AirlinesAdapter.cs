﻿using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using System.Linq;
using Android.Graphics;
using System.Threading.Tasks;
using Android.Content.Res;
using AIMS.Models;
using Microsoft.AppCenter.Crashes;
using Android.App;

namespace AIMS.Droid
{
    public class JavaAirline : Airline
    {
        #region Fields
        //private ImageRepository repository;
        #endregion
        public JavaAirline()
            : this(new Airline()) { }

        public JavaAirline(Airline item)
        {
            AirlineLogoBytes = item.AirlineLogoBytes;//TODO: Check if it is necessary for updated pictures
            Code = item.Code;
            Description = item.Description;
            ID = item.ID;
            LogoResourceID = item.LogoResourceID;
            Name = item.Name;
            ImageHandle = item.ImageHandle;

            //repository = new ImageRepository(new WebApiServiceContext());
            //repository = new ImageRepository(ImageHandle, new WebApiServiceContext());
            //repository.OnRepositoryChanged += OnImageLoaded;
        }


        public static implicit operator Java.Lang.Object(JavaAirline item)
        {
            return new JavaObjectBase<JavaAirline>(item);
        }

        public static explicit operator JavaAirline(Java.Lang.Object item)
        {
            return (item as JavaObjectBase<JavaAirline>).Item;
        }
    }

    public class AirlinesAdapter : ImageAdapter
    {
        //Context context;
        //AIMSApp app;
        List<JavaAirline> fullAirlines = new List<JavaAirline>();
        List<JavaAirline> airlines = new List<JavaAirline>();
        AirlinesRepository repository;
        private MemoryLimitedLruCache _memoryCache;


        //public ArrayAdapter AutoCompleteAdapter { get; private set; }

        public event ReporitoryChangedHandler AdapterChanged;

        /// <summary>
        /// Loads all the Airlines from repository
        /// </summary>
        /// <param name="c">The Context of the activity</param>
        public AirlinesAdapter(Context c) : base(c)
        {
            repository = new AirlinesRepository();
            repository.OnRepositoryChanged += Airlines_OnRepositoryChanged;
            //airlines = repository.Airlines.Select(x => new JavaAirline(x)).ToList();

            // Get max available VM memory, exceeding this amount will throw an OutOfMemory exception.
            // Stored in kilobytes as LruCache takes an int in its constructor.
            var maxMemory = (int)(Java.Lang.Runtime.GetRuntime().MaxMemory() / 1024);
            // Use 1/8th of the available memory for this memory cache.
            int cacheSize = maxMemory / 8;
            _memoryCache = new MemoryLimitedLruCache(cacheSize);
        }

        private AirlinesAdapter(Context c, List<JavaAirline> airlines) : this(c)
        {
            this.airlines = airlines;
            //repository = new AirlinesRepository();
            //repository.OnRepositoryChanged += Airlines_OnRepositoryChanged;
            //airlines = repository.Airlines.Select(x => new JavaAirline(x)).ToList();
        }

        private int getSeparatorIndex(List<JavaAirline> items)
        {
            if (!items.Any(x => x.Name.Equals("######") && x.Code.Equals("######")))
                return -1;

            var separatorIndex = 0;
            while (!items[separatorIndex].Name.Equals("######") || !items[separatorIndex].Code.Equals("######"))
            {
                separatorIndex++;
            }
            return separatorIndex;
        }

        public void LoadAirlines(bool forceGet = false)
        {
            repository.LoadAirlines(forceGet);
        }

        public void SearchAirlines(string keyword)
        {
            repository.SearchAirlines(keyword);
        }

        private void Airlines_OnRepositoryChanged(object repository)
        {
            if (repository == null) return;
            else if (repository is IEnumerable<Airline>)
            {
                fullAirlines = ((IEnumerable<Airline>)repository).Select(x => new JavaAirline((Airline)x)).ToList();
                var separatorIndex = getSeparatorIndex(fullAirlines);

                if (separatorIndex != -1)
                    airlines = fullAirlines.Take(separatorIndex).ToList();
                else airlines = fullAirlines;
            }
            else return;

            if (AdapterChanged != null)
                AdapterChanged.Invoke(airlines);
        }

        public List<string> GetAutoCompleteOptions()
        {
            var keywords = fullAirlines.Select(x => x.Name).ToList();
            keywords.AddRange(fullAirlines.Select(x => x.Code));
            keywords = keywords.OrderBy(x => x).ToList();
            return keywords.Distinct().ToList();
        }

        public override int Count
        {
            get { return airlines.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return airlines[position];
        }

        public override long GetItemId(int position)
        {
            if (airlines != null && position >= 0 && position < Count)
            {
                return (long)airlines[position].ID;
            }
            return 0;
        }

        // lazy loading should not be here!!! (Reason: frequent stopping and lagging in scroll)
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view;
            TextView lblAirlineName;
            ImageView imageView;
            if (convertView == null)
            {  // if it's not recycled, initialize some attributes
                LayoutInflater inflater = LayoutInflater.FromContext(context);
                view = inflater.Inflate(Resource.Drawable.airlineitem, null);
            }
            else
            {
                view = (LinearLayout)convertView;
            }
            lblAirlineName = view.FindViewById<TextView>(Resource.Id.lblAirlineName);
            imageView = view.FindViewById<ImageView>(Resource.Id.airline_item_image);
            //if(airlines[position].Code.Equals("######") && airlines[position].Name.Equals("######"))//was adding separator (Bad practise!)
            //{
            //    imageView.SetImageResource(Resource.Drawable.not_available_pic_off);
            //    lblAirlineName.Text = "";
            //    var ll = (LinearLayout)imageView.Parent;
            //    ll.LayoutParameters.Width = 720;
            //    ll.LayoutParameters.Height = 0;
            //    ll.RequestLayout();
            //}
            //else { 

            if (airlines[position].AirlineLogoBytes != null && airlines[position].AirlineLogoBytes.Length > 0)
            {
                Bitmap bmp = null;

                try
                {
                    bmp = (Bitmap)_memoryCache.Get(airlines[position].ImageHandle);

                    if (bmp != null)
                    {
                        imageView.SetImageBitmap(bmp);
                    }
                    else
                    {
                        bmp = BitmapFactory.DecodeByteArray(airlines[position].AirlineLogoBytes, 0, airlines[position].AirlineLogoBytes.Length);
                        if (bmp != null)
                        {
                            _memoryCache.Put(airlines[position].ImageHandle, bmp);
                            imageView.SetImageBitmap(bmp);
                        }
                    }
                }
                catch (Exception eex)
                {
                    Crashes.TrackError(eex);

                    bmp = BitmapFactory.DecodeByteArray(airlines[position].AirlineLogoBytes, 0, airlines[position].AirlineLogoBytes.Length);
                    if (bmp != null)
                    {
                        _memoryCache.Put(airlines[position].ImageHandle, bmp);
                        imageView.SetImageBitmap(bmp);
                    }
                }
            }
            else if (airlines[position].LogoResourceID == 0)
            {
                imageView.SetImageResource(Resource.Drawable.not_available_pic_off);

                //Triggering image load...
                LoadImage(imageView, airlines[position].ImageHandle);
                //airlines[position].LoadImage(imageView, context, app.ImageRepository);
            }
            else
                imageView.SetImageResource(airlines[position].LogoResourceID);

            lblAirlineName.Text = airlines[position].DisplayName;
            //}
            return view;
        }

        public AirlinesAdapter GetSecondSectionAdapter()
        {
            var separatorIndex = getSeparatorIndex(fullAirlines);
            if (separatorIndex != -1)//Not necessary
                return new AirlinesAdapter(base.context, this.fullAirlines.Skip(separatorIndex + 1).ToList());
            return new AirlinesAdapter(base.context, new List<JavaAirline>());
        }
    }


    public class MemoryLimitedLruCache : Android.Support.V4.Util.LruCache
    {
        public MemoryLimitedLruCache(int size) : base(size) { }

        protected override int SizeOf(Java.Lang.Object key, Java.Lang.Object value)
        {
            // android.graphics.Bitmap.getByteCount() method isn't currently implemented in Xamarin. Invoke Java method.
            IntPtr classRef = Android.Runtime.JNIEnv.FindClass("android/graphics/Bitmap");
            var getBytesMethodHandle = Android.Runtime.JNIEnv.GetMethodID(classRef, "getByteCount", "()I");
            var byteCount = Android.Runtime.JNIEnv.CallIntMethod(value.Handle, getBytesMethodHandle);

            return byteCount / 1024;
        }
    }
}