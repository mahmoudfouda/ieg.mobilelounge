using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using AIMS.Models;
using Android.App;
using System;

namespace AIMS.Droid
{
    public class JavaPassenger : Passenger
    {
        public JavaPassenger()
            : this(new Passenger()) { }

        public JavaPassenger(Passenger item)
        {
            CardID = item.CardID;
            OtherCardID = item.OtherCardID;
            AirlineId = item.AirlineId;
            TrackingRecordID = item.TrackingRecordID;
            FullName = item.FullName;
            TrackingTimestamp = item.TrackingTimestamp;
            TimestampUtc = item.TimestampUtc;
            //FirstName = item.FirstName;
            //LastName = item.LastName;
            FromAirport = item.FromAirport;
            ToAirport = item.ToAirport;
            Title = item.Title;
            TrackingClassOfService = item.TrackingClassOfService;
            PassengerStatus = item.PassengerStatus;
            PNR = item.PNR;
            FFN = item.FFN;
            FlightCarrier = item.FlightCarrier;
            FlightNumber = item.FlightNumber;
            BoardingPassFlightCarrier = item.BoardingPassFlightCarrier;
            BoardingPassFlightNumber = item.BoardingPassFlightNumber;
            BoardingPassCompCode = item.BoardingPassCompCode;
            BoardingPassFFAirline = item.BoardingPassFFAirline;
            BoardingPassFFNumber = item.BoardingPassFFNumber;
            BoardingPassPaxDesc = item.BoardingPassPaxDesc;
            BoardingPassSeq = item.BoardingPassSeq;
            BoardingPassEIndicator = item.BoardingPassEIndicator;
            BoardingPassFlightDate = item.BoardingPassFlightDate;
            BoardingPassName = item.BoardingPassName;
            ArrivalDate = item.ArrivalDate;
            ArrivalTime = item.ArrivalTime;
            BoardingTime = item.BoardingTime;
            DepartureDate = item.DepartureDate;
            DepartureTime = item.DepartureTime;
            BarcodeString = item.BarcodeString;
            IsValid = item.IsValid;
            WorkstationId = item.WorkstationId;
            TrackingStatus = item.TrackingStatus;
            TrackingPassCombinedFlightNumber = item.TrackingPassCombinedFlightNumber;
            TrackingSystemInfo = item.TrackingSystemInfo;
            FailedReason = item.FailedReason;
            GuestOf = item.GuestOf;
            HostName = item.HostName;

        }

        public static implicit operator Java.Lang.Object(JavaPassenger item)
        {
            return new JavaObjectBase<JavaPassenger>(item);
        }

        public static explicit operator JavaPassenger(Java.Lang.Object item)
        {
            return (item as JavaObjectBase<JavaPassenger>).Item;
        }
    }
    public class PassengersAdapter : BaseAdapter
    {
        private AIMSApp _app;
        Context context;
        List<JavaPassenger> passengers = new List<JavaPassenger>();
        PassengersRepository repository;// = new PassengersRepository();
        private Button btnLastHourShowHost;

        public event ReporitoryChangedHandler AdapterChanged;

        public bool IsLastHourLoaded { get; private set; }

        /// <summary>
        /// Loads all the Passengers from repository
        /// </summary>
        /// <param name="c">The Context of the activity</param>
        public PassengersAdapter(Context c, AIMSApp app)
        {
            _app = app;
            context = c;
            repository = new PassengersRepository();
            repository.OnRepositoryChanged += Passengers_OnRepositoryChanged;
            //passengers = MockRepository.Current.Passengers.Select(x => new JavaPassenger(x)).ToList();
        }

        public void ClearList()
        {
            passengers.Clear();
        }

        public void LoadLastHourPassengers()
        {
            repository.LoadLastHourPassengers();
            IsLastHourLoaded = true;
        }

        public void LoadFailedPassengers()
        {
            repository.LoadFailedPassengers();
            IsLastHourLoaded = false;
        }

        public void SearchPassengersByFilter(string keyWord, bool airportSearch, DateTime dateTime, int currentPage)
        {
            repository.SearchPassengers(keyWord, airportSearch, dateTime, currentPage);
            IsSearchByFilter = true;
        }

        public void SearchPassengers(string keyword)
        {
            repository.SearchPassengers(keyword);
            IsLastHourLoaded = true;
        }

        private void Passengers_OnRepositoryChanged(object repository)
        {
            var pass = PassengersRepository.GroupPassengers((IEnumerable<Passenger>)repository);

            passengers = pass.Select(x => new JavaPassenger((Passenger)x)).ToList();
            
            if (AdapterChanged != null)
                AdapterChanged.Invoke(passengers);
        }

        public override int Count
        {
            get { return passengers.Count; }
        }

        public bool IsSearchByFilter { get; private set; }

        public override Java.Lang.Object GetItem(int position)
        {
            return passengers[position];
        }

        public override long GetItemId(int position)
        {
            if (passengers != null && position >= 0 && position < Count)
            {
                return (long)passengers[position].TrackingRecordID;
            }
            return 0;
        }
        
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            ImageView imageView;
            TextView lblPassengerName, lblPassengerItemDescription, lblPassengerItemTime;
            if (convertView == null)
            {
                LayoutInflater inflater = LayoutInflater.FromContext(context);
                view = inflater.Inflate(Resource.Drawable.passengeritem, null);
            }
            else
            {
                view = (LinearLayout)convertView;
            }

            imageView = view.FindViewById<ImageView>(Resource.Id.imgPassengerItem);
            
            lblPassengerName = view.FindViewById<TextView>(Resource.Id.lblPassengerItemName);
            lblPassengerItemTime = view.FindViewById<TextView>(Resource.Id.lblPassengerItemTime);
            lblPassengerItemDescription = view.FindViewById<TextView>(Resource.Id.lblPassengerItemDescription);
            btnLastHourShowHost = view.FindViewById<Button>(Resource.Id.btnLastHourShowHost);

            var passenger = passengers[position];


            if (IsLastHourLoaded)
            {
                imageView.SetImageResource(Resource.Drawable.passenger_medium_green);
                lblPassengerName.Text = passenger.FullName;
            }
            else
            {
                imageView.SetImageResource(Resource.Drawable.passenger_medium_red);
                if (string.IsNullOrEmpty(passenger.FailedReason))
                    lblPassengerName.Text = passenger.FullName;
                else
                {
                    if (passenger.FailedReason.Contains("\n"))
                    {
                        lblPassengerName.Text = passenger.FailedReason.Substring(0, passenger.FailedReason.IndexOf("\n"));
                        lblPassengerItemDescription.Text = passenger.FailedReason.Substring(passenger.FailedReason.IndexOf("\n") + 1);
                    }
                    else
                        lblPassengerName.Text = passenger.FailedReason;
                }
            }

            if (!IsLastHourLoaded)
            {
                imageView.SetImageResource(Resource.Drawable.passenger_red);
                btnLastHourShowHost.Visibility = ViewStates.Gone;
            }
            else if (passenger.GuestOf > 0)
            {
                btnLastHourShowHost.Visibility = ViewStates.Visible;
                imageView.SetImageResource(Resource.Drawable.newguest);
                btnLastHourShowHost.Text = string.IsNullOrWhiteSpace(passenger.HostName) ? "Main PAX" : passenger.HostName;

                btnLastHourShowHost.Click -= OnMainPaxClicked;
                btnLastHourShowHost.Click += OnMainPaxClicked;
                btnLastHourShowHost.Tag = position;
            }
            else
            {
                imageView.SetImageResource(Resource.Drawable.passenger_blue);
                btnLastHourShowHost.Visibility = ViewStates.Gone;
            }

            lblPassengerItemTime.Text = passenger.TrackingTimestamp.ToShortTimeString();

            return view;
        }

        private void OnMainPaxClicked(object sender, System.EventArgs e)
        {
            try
            {
                if ((sender as Button).Tag != null)
                {
                    int position = int.Parse((sender as Button).Tag.ToString());

                    var pr = new PassengersRepository();
                    pr.OnPassengerChecked += (isSucceeded, errorCode, errorMetadata, message, passenger) =>
                    {
                        if (!isSucceeded)
                        {
                            //TODO: show message
                            return;
                        }

                        MembershipProvider.Current.SelectedPassenger = passenger;

                        _app.ValidationResultIntent = new Intent(this.context, typeof(ValidationResultActivity));
                        _app.ValidationResultIntent.SetFlags(ActivityFlags.NewTask);
                        _app.MainForm.StartActivity(_app.ValidationResultIntent);
                    };
                    pr.RetrieveMainPassenger(passengers[position], MembershipProvider.Current.SelectedLounge);
                }

            }
            catch(System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }

        }

        internal void LoadMorePassengersByFilter(string keyWord, bool airportSearch, DateTime dateTime, int nextPage)
        {
            repository.SearchPassengers(keyWord, airportSearch, dateTime, nextPage, true);
            IsSearchByFilter = true;
        }
    }
}