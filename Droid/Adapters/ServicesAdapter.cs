﻿using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using System.Linq;
using AIMS.Models;
using System.Collections.Generic;
using Android.Content.Res;
using Android.Graphics;

namespace AIMS.Droid
{
    //public class JavaPassengerService : Java.Lang.Object{
    //	public PassengerService PassengerService {
    //		get;
    //		set;
    //	}
    //}
    public class JavaPassengerService : PassengerService
    {
        public JavaPassengerService()
            : this(new PassengerService()) { }

        public JavaPassengerService(PassengerService item)
        {
            Id = item.Id;
            Name = item.Name;
            Quantity = item.Quantity;
            MaximumQuantity = item.MaximumQuantity;
            UnitPrice = item.UnitPrice;
            Description = item.Description;
        }

        public static implicit operator Java.Lang.Object(JavaPassengerService item)
        {
            return new JavaObjectBase<JavaPassengerService>(item);
        }

        public static explicit operator JavaPassengerService(Java.Lang.Object item)
        {
            return (item as JavaObjectBase<JavaPassengerService>).Item;
        }
    }
    public class ServicesAdapter : BaseAdapter
	{
		Context context;
		System.Collections.Generic.List<JavaPassengerService> services;

		public ServicesAdapter (Context c)
		{
			context = c;
            services = new System.Collections.Generic.List<JavaPassengerService>(); //MockRepository.Current.Services.Select(x=>new JavaPassengerService(x)).ToList();

            if (MembershipProvider.Current.SelectedPassenger == null)
                return;


            if (MembershipProvider.Current.SelectedPassenger.PassengerServices == null)//TODO: move this into Client Core Library inside (MembershipProvider.Current.SelectedPassenger.Set{} very last line)
                MembershipProvider.Current.SelectedPassenger.PassengerServices = new Dictionary<PassengerService, List<Passenger>>();

            foreach (var guestCatKeyVal in MembershipProvider.Current.SelectedPassenger.PassengerServices)
            {
                services.Add(new JavaPassengerService(guestCatKeyVal.Key));

                var passengerService = guestCatKeyVal.Key;
                passengerService.Quantity = guestCatKeyVal.Value.Count;
            }
        }

        public Dictionary<PassengerService, List<Passenger>> PassengerServices
        {
            get
            {
                return MembershipProvider.Current.SelectedPassenger.PassengerServices;
            }
        }


        public ServicesAdapter(Context c, Passenger passenger)
        {
            context = c;
            if(passenger != null)
                services = passenger.PassengerServices.Select(x => new JavaPassengerService(x.Key)).ToList();
        }

        public override int Count {
			get { return services.Count; }
		}

        public float PriceSum
        {
            get
            {
                if (services == null || services.Count == 0) return 0;
                return services.Sum(x => x.TotalPrice);
            }
        }

        public override Java.Lang.Object GetItem (int position)
		{
			return services[position];
		}

		public override long GetItemId (int position)
		{
			return services[position].Id;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View linearLayout;

			if (convertView == null) { 
				LayoutInflater inflater = LayoutInflater.FromContext (context);		
				linearLayout = inflater.Inflate(Resource.Drawable.serviceitem, null);
			} 
			else {
				linearLayout = (RelativeLayout)convertView; 
			}

            var passengerService = PassengerServices.Keys.ToList()[position];

            var imgServiceItem = linearLayout.FindViewById<ImageView>(Resource.Id.imgServiceItem);
            var lblPSQuantity = linearLayout.FindViewById<TextView>(Resource.Id.lblPSQuantity);
            var lblPSMaxQuantity = linearLayout.FindViewById<TextView>(Resource.Id.lblPSMaxQuantity);

            switch (passengerService.Id)
            {
                case 0:
                    //none
                    imgServiceItem.SetImageResource((Resource.Drawable.tab_passengers_off_big));
                    break;
                case 1:
                    //complementary
                    imgServiceItem.SetImageResource((Resource.Drawable.GuestOff));
                    break;
                case 2:
                    //family
                    imgServiceItem.SetImageResource((Resource.Drawable.FamilyGuestOff));
                    break;
                case 3:
                    //paid
                    imgServiceItem.SetImageResource((Resource.Drawable.PaidGuestOff));
                    break;
                default:
                    //defaultimage
                    imgServiceItem.SetImageResource((Resource.Drawable.tab_passengers_off_big)); 
                    break;
            }

            lblPSQuantity.Text = PassengerServices[passengerService].Count.ToString(); //passengerService.Quantity.ToString();
            lblPSMaxQuantity.Text = string.Format("/ {0}", passengerService.MaximumQuantity);

            //var lblLoungeName = linearLayout.FindViewById<TextView> (Resource.Id.lblServiceName);
            //lblLoungeName.Text = services[position].DisplayText;
            //var lblLoungeDescription = linearLayout.FindViewById<TextView> (Resource.Id.lblServiceDescription);
            //lblLoungeDescription.Text = services[position].DisplayDescription;
            //var btnEditService = linearLayout.FindViewById<Button> (Resource.Id.btnEditService);
            //btnEditService.Click += delegate {
            //	btnEditService.Enabled = false;
            //};
            return linearLayout;
		}

        public  Bitmap GetBitmapFromResources(int resImage)
        {
            BitmapFactory.Options options = new BitmapFactory.Options
            {
                InJustDecodeBounds = false,
                InDither = false,
                InSampleSize = 1,
                InScaled = false,
                InPreferredConfig = Bitmap.Config.Argb8888
            };

            return BitmapFactory.DecodeResource(this.context.Resources, resImage, options);
        }

    }
}

