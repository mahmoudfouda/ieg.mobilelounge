﻿using Android.Content;
using Android.Widget;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using AIMS.Models;
using System;
using Android.OS;
using AIMS.Droid.Library;

namespace AIMS.Droid
{
    //	public class JavaLounge : Java.Lang.Object{
    //		public Lounge Lounge {
    //			get;
    //			set;
    //		}
    //	}

    public class JavaLounge : Workstation
    {
        public JavaLounge()
            : this(new Workstation()) { }

        public JavaLounge(Workstation item)
        {
            AirportCode = item.AirportCode;
            Description = item.Description;
            LoungeName = item.LoungeName;
            LoungeID = item.LoungeID;
            WorkstationID = item.WorkstationID;
            WorkstationName = item.WorkstationName;
            DefaultAirlineId = item.DefaultAirlineId;
            Capacity = item.Capacity;
            PAXAvgStayMinutes = item.PAXAvgStayMinutes;
            LastPercentage = item.LastPercentage;
        }

        public static implicit operator Java.Lang.Object(JavaLounge item)
        {
            return new JavaObjectBase<JavaLounge>(item);
        }

        public static explicit operator JavaLounge(Java.Lang.Object item)
        {
            return (item as JavaObjectBase<JavaLounge>).Item;
        }
    }

    public class LoungeAdapter : BaseAdapter
    {
        Context context;
        BaseFragment baseFragment;
        List<JavaLounge> lounges;

        public LoungeAdapter(Context c)
        {
            context = c;
            //			lounges = MockRepository.Current.Lounges.Select(x=>new JavaLounge{
            //				Lounge = x
            //			}).ToList();
            if (MembershipProvider.Current != null)
                lounges = MembershipProvider.Current.UserLounges.Select(x => new JavaLounge(x)).ToList();
        }

        public LoungeAdapter(BaseFragment bf)
        {
            baseFragment = bf;
            
            if (MembershipProvider.Current != null)
                lounges = MembershipProvider.Current.UserLounges.Select(x => new JavaLounge(x)).ToList();
        }

        public LoungeAdapter(BaseFragment bf, JavaLounge currentLounge)
        {
            baseFragment = bf;

   
            if (MembershipProvider.Current != null)
                lounges = MembershipProvider.Current.UserLounges.Where(c=> c.LoungeID != currentLounge.LoungeID).Select(x => new JavaLounge(x)).ToList();
        }

        public void RefreshData(JavaLounge currentLounge)
        {
            lounges.Clear();
            var list = MembershipProvider.Current.UserLounges.Where(c => c.LoungeID != currentLounge.LoungeID).Select(x => new JavaLounge(x)).ToList();

            foreach (var item in list)
            {
                lounges.Add(item);
            }

            NotifyDataSetChanged();
        }

        public override int Count
        {
            get { return lounges.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return lounges[position];
        }

        public override long GetItemId(int position)
        {
            return (long)lounges[position].WorkstationID;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View layout;

            if (convertView == null)
            {
                LayoutInflater inflater = LayoutInflater.FromContext(context ?? baseFragment.Context);
                layout = inflater.Inflate(Resource.Drawable.loungeitem, null);
            }
            else
            {
                layout = (RelativeLayout)convertView;
            }
            var lblLoungeName = layout.FindViewById<TextView>(Resource.Id.lblLoungeName);
            lblLoungeName.Text = lounges[position].WorkstationName;
            var lblLoungeDescription = layout.FindViewById<TextView>(Resource.Id.lblLoungeDescription);
            lblLoungeDescription.Text = string.Format("{0} | {1}", lounges[position].AirportCode, lounges[position].LoungeName);



#if DEBUG
            lounges[position].LastPercentage = DateTime.Now.Minute;
#endif


            var progressBar = layout.FindViewById<ProgressBar>(Resource.Id.downloadProgressBar);
            progressBar.SetProgress(lounges[position].LastPercentage, true);
            progressBar.SetBackgroundColor(Android.Graphics.Color.LightGray);
            progressBar.ProgressDrawable.SetColorFilter(Util.GetProgressColor(lounges[position].LastPercentage), Android.Graphics.PorterDuff.Mode.SrcIn);

            var lblPerc = layout.FindViewById<TextView>(Resource.Id.progressBarinsideText);
            lblPerc.Text = $"{ lounges[position].LastPercentage.ToString("N0")}%";

            if (context != null)
            {
                (context as BaseActivity).App.GetLoungeOccupancyWhithQueue(lounges[position], (data) =>
                {
                    (context as BaseActivity).RunOnUiThread(() =>
                    {
                        if (data == null)
                            return;

                        var loungeOccupancy = (LoungeOccupancy)data;
                        lounges[position].LastPercentage = (int)System.Math.Ceiling(loungeOccupancy.Percentage);
                        NotifyDataSetChanged();
                    });
                });
            }
            else
            {
                baseFragment.App.GetLoungeOccupancyWhithQueue(lounges[position], (data) =>
                {
                    baseFragment.Activity.RunOnUiThread(() =>
                    {
                        if(position == 0)
                        {
                            layout.Selected = true;
                        }

                        if (data == null)
                            return;

                        var loungeOccupancy = (LoungeOccupancy)data;
                        lounges[position].LastPercentage = (int)System.Math.Ceiling(loungeOccupancy.Percentage);
                        NotifyDataSetChanged();
                    });
                });
            }

            return layout;
        }
    }
}



