﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIMS.Models;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AIMS.Droid.Adapters
{
    public class ExpandableListAdapter : BaseExpandableListAdapter
    {
        private Activity _context;
        private List<string> _listDataHeader = new List<string>(); // header titles
                                                                   // child data in format of header title, child title
        private Dictionary<string, List<string>> _listDataChild = new Dictionary<string, List<string>>();

        private Dictionary<PassengerService, List<Passenger>> _services;

        public AIMSApp App
        {
            get
            {
                return (AIMSApp)_context.Application;
            }
        }

        public Dictionary<PassengerService, List<Passenger>> Services => _services;


        public ExpandableListAdapter(Activity context, Dictionary<PassengerService, List<Passenger>>  services)
        {
            _context = context;
            _services = services;

            foreach (var guestCatKeyVal in services)
            {
                _listDataHeader.Add(guestCatKeyVal.Key.Name);

                if (guestCatKeyVal.Value != null)
                    _listDataChild.Add(guestCatKeyVal.Key.Name, guestCatKeyVal.Value.Select(c => c.FullName).ToList());

                if (guestCatKeyVal.Key.MaximumQuantity > guestCatKeyVal.Value.Count)
                    _listDataChild[guestCatKeyVal.Key.Name].Add(App.TextProvider.GetText(2164));
            }
        }
        //for cchild item view
        public override Java.Lang.Object GetChild(int groupPosition, int childPosition)
        {
            return _listDataChild[_listDataHeader[groupPosition]][childPosition];
        }
        public override long GetChildId(int groupPosition, int childPosition)
        {
            return childPosition;
        }

        public override View GetChildView(int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent)
        {
            string childText = (string)GetChild(groupPosition, childPosition);
            if (convertView == null)
            {
                convertView = _context.LayoutInflater.Inflate(Resource.Layout.ListItemServiceLayout, null);
            }

            TextView txtListChild = (TextView)convertView.FindViewById(Resource.Id.lblListItem);
            txtListChild.Text = childText;

            var imgAddButton  = (ImageView)convertView.FindViewById(Resource.Id.imgAddButton);

            var passengerService = _services.Keys.ToList()[groupPosition];


            imgAddButton.Visibility = _services[passengerService].Count > childPosition ? ViewStates.Gone : ViewStates.Visible;

            //imgAddButton.Click += OnAddButtonClick;


            return convertView;
        }

        private void OnAddButtonClick(object sender, EventArgs e)
        {
            App.IsGuestMode = true;

            MembershipProvider.Current.SelectedGuest = new Passenger
            {
                CardID = MembershipProvider.Current.SelectedCard.ID,
                AirlineId = MembershipProvider.Current.SelectedPassenger.AirlineId
            };

            App.ManualEntryGuest = new Intent(_context, typeof(ManualEntryActivity));
            App.ManualEntryGuest.SetFlags(ActivityFlags.NewTask);
            App.MainForm.StartActivity(App.ManualEntryGuest);
        }

        public override int GetChildrenCount(int groupPosition)
        {
            return _listDataChild[_listDataHeader[groupPosition]].Count;
        }
        //For header view
        public override Java.Lang.Object GetGroup(int groupPosition)
        {
            return _listDataHeader[groupPosition];
        }
        public override int GroupCount
        {
            get
            {
                return _listDataHeader.Count;
            }
        }
        public override long GetGroupId(int groupPosition)
        {
            return groupPosition;
        }
        public override View GetGroupView(int groupPosition, bool isExpanded, View convertView, ViewGroup parent)
        {
            string headerTitle = (string)GetGroup(groupPosition);

            convertView = convertView ?? _context.LayoutInflater.Inflate(Resource.Layout.HeaderServiceLayout, null);
            var lblListHeader = (TextView)convertView.FindViewById(Resource.Id.lblListHeader);
            lblListHeader.Text = headerTitle;

            var imgServiceItem = (ImageView)convertView.FindViewById(Resource.Id.imgServiceItem);

            var passengerService = _services.Keys.ToList()[groupPosition];
            LoadImageItemService(imgServiceItem, passengerService);
            var lblPSQuantity = convertView.FindViewById<TextView>(Resource.Id.lblPSQuantity);
            var lblPSMaxQuantity = convertView.FindViewById<TextView>(Resource.Id.lblPSMaxQuantity);

            lblPSQuantity.Text = _services[passengerService].Count.ToString(); //  passengerService.Quantity.ToString();
            lblPSMaxQuantity.Text = string.Format("/ {0}", passengerService.MaximumQuantity);

            return convertView;
        }

        private void LoadImageItemService(ImageView imgServiceItem, PassengerService passengerService)
        {
            switch (passengerService.Id)
            {
                case 0:
                    //none
                    imgServiceItem.SetImageResource(Resource.Drawable.tab_passengers_on_big);
                    break;
                case 1:
                    //complementary
                    imgServiceItem.SetImageResource(Resource.Drawable.GuestOn);
                    break;
                case 2:
                    //family
                    imgServiceItem.SetImageResource(Resource.Drawable.FamilyGuestOn);
                    break;
                case 3:
                    //paid
                    imgServiceItem.SetImageResource(Resource.Drawable.PaidGuestOn);
                    break;
                default:
                    //defaultimage
                    imgServiceItem.SetImageResource(Resource.Drawable.tab_passengers_on_big);
                    break;
            }
        }

        public override bool HasStableIds
        {
            get
            {
                return false;
            }
        }
        public override bool IsChildSelectable(int groupPosition, int childPosition)
        {
            return true;
        }
    }
}