﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIMS.Models;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Util.Concurrent.Locks;
using Microsoft.AppCenter.Crashes;
using String = System.String;

namespace AIMS.Droid.Adapters
{
    public class AirportListAdapter : ArrayAdapter
    {
        private List<Airport> airports = new List<Airport>();
        private Context mContext;
        private int itemLayout;

        public AirportListAdapter(Context context, int resource, List<Airport> storeDataLst) : base(context, resource, storeDataLst)
        {
            airports = storeDataLst;
            mContext = context;
            itemLayout = resource;
            this.SetNotifyOnChange(true);
        }

        public List<Airport> Airports
        {
            get
            {
                return airports;
            }
            set
            {
                airports = value;
            }
        }

        public override int Count => airports.Count;

        public override Java.Lang.Object GetItem(int position)
        {
            return new JavaObjectBase<Airport>(Airports[position]);
        }

        public override Filter Filter
        {
            get
            {
                return new AiportFilter(this);
            }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inflater = LayoutInflater.FromContext(mContext);
            var view = convertView ?? inflater.Inflate(Resource.Drawable.airportItem, null);

            try
            {
                var apName = view.FindViewById<TextView>(Resource.Id.lblAirportName);
                var apCode = view.FindViewById<TextView>(Resource.Id.lblAirportCode);

                apName.Text = Airports[position].Name;
                apCode.Text = Airports[position].IATA_Code;
            }
            catch { }

            return view;
        }
    }

    class AiportFilter : Filter
    {
        protected object mLock = new object();
        private AirportListAdapter _adapter;
        private FilterResults results;
        private Java.Lang.Object lockk;
        private bool isUpdateNeeded = true;

        public AiportFilter(AirportListAdapter adapter)
        {
            _adapter = adapter;
            lockk = this;
        }

        protected override FilterResults PerformFiltering(ICharSequence prefix)
        {
            results = new FilterResults();

            if (prefix == null || prefix.Length() == 0)
            {
                lock (mLock)
                {
                    JavaList<Airport> list = new JavaList<Airport>();
                    results.Values = list;
                    results.Count = list.Count;
                    isUpdateNeeded = false;
                }
            }
            else
            {
                using (var ar = new AirportsRepository())
                {
                    ar.OnRepositoryChanged += (data) =>
                     {
                         JavaList<Airport> itemsJava = new JavaList<Airport>();

                         _adapter.Airports = (List<Airport>)data;
                         if (_adapter.Airports != null && _adapter.Airports.Count > 0)
                         {
                             try
                             {
                                 foreach (var item in _adapter.Airports)
                                 {
                                     itemsJava.Add(item);
                                 }

                                 results.Values = itemsJava;
                                 results.Count = itemsJava.Count();
                                 _adapter.NotifyDataSetChanged();

                                 isUpdateNeeded = false;
                                 lockk.NotifyAll();

                             }
                             catch { }
                         }
                     };

                    ar.SearchAirports(prefix.ToString(), true);

                }
            }

            if (prefix != null && prefix.Length() > 1)
            {
                lock (lockk)
                {
                    while (isUpdateNeeded == true)
                    {
                        try
                        {
                            lockk.Wait();
                        }
                        catch { }
                    }
                    isUpdateNeeded = true;
                }
            }

            return results;
        }

        protected override void PublishResults(ICharSequence constraint, FilterResults results)
        {
            var mObjects = results.Values;
            if (results.Count > 0)
            {
                _adapter.NotifyDataSetChanged();
            }
            else
            {
                _adapter.NotifyDataSetInvalidated();
            }
        }
    }
}