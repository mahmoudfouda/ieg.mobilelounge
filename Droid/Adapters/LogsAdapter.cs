﻿using Android.Widget;
using Android.Content;
using System.Linq;
using Android.Views;
using System.Collections.Generic;
using AIMS.Models;

namespace AIMS.Droid
{
    //	public class JavaLog : Java.Lang.Object{
    //		public Log Log {
    //			get;
    //			set;
    //		}
    //	}

    public class JavaLog: Log
	{
		public JavaLog()
			:this(new Log()){}

		public JavaLog(Log item)
		{
			Id = item.Id;
			Time = item.Time;
            LocalTime = item.LocalTime;
			Title = item.Title;
			LogType = item.LogType;
			LogLevel = item.LogLevel;
			Description = item.Description;
		}

		public static implicit operator Java.Lang.Object (JavaLog item)
		{
			return new JavaObjectBase<JavaLog> (item);
		}

		public static explicit operator JavaLog (Java.Lang.Object item)
		{
			return (item as JavaObjectBase<JavaLog>).Item;
		}
	}

	public class LogsAdapter : BaseAdapter
	{
		Context context;
		List<JavaLog> logs = new List<JavaLog>();
        LogsRepository repository;

        public LogsAdapter (Context c)
		{
			context = c;

            repository = new LogsRepository();
            repository.OnRepositoryChanged += Logs_OnRepositoryChanged;
        }

        private void Logs_OnRepositoryChanged(object repository)
        {
            if (repository == null) return;
            
            logs = ((IEnumerable<Log>)repository).Select(x => new JavaLog(x)).ToList();

            //if (AdapterChanged != null)
            //    AdapterChanged.Invoke(airlines);

            NotifyDataSetChanged();//TODO: Make sure it is necessary (or it will be called twice)
        }

        public void LoadClientOnlyLogs()
        {
            repository.LoadClientOnlyLogs();
        }

        public void LoadDeveloperOnlyLogs()
        {
            repository.LoadDeveloperOnlyLogs();
        }

        public void LoadAllLogs()
        {
            repository.LoadAllLogs();
        }

        public override int Count {
			get { return logs.Count; }
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return logs[position];
		}

		public override long GetItemId (int position)
		{
			return logs[position].Id;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View linearLayout;

			if (convertView == null) { 
				LayoutInflater inflater = LayoutInflater.FromContext (context);		
				linearLayout = inflater.Inflate(Resource.Drawable.logitem, null);
			} 
			else {
				linearLayout = (LinearLayout)convertView; 
			}
			var lblLogName = linearLayout.FindViewById<TextView> (Resource.Id.lblLogTitle);
			lblLogName.Text = logs[position].Title;
			var lblLogDescription = linearLayout.FindViewById<TextView> (Resource.Id.lblLogDescription);
			lblLogDescription.Text = logs[position].Description;
			var lblLogTime = linearLayout.FindViewById<TextView> (Resource.Id.lblLogTime);
			lblLogTime.Text = string.Format("{0:yyyy/MM/dd - HH:mm:ss (FFF)}", logs[position].LocalTime);

			//var imgLog = linearLayout.FindViewById<ImageView> (Resource.Id.imgLog);

			return linearLayout;
		}
        
	}
}

