﻿using Android.Widget;
using Android.Content;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Android.Graphics;
using AIMS.Models;
using Microsoft.AppCenter.Crashes;

namespace AIMS.Droid
{
    public class JavaCard : Card
    {
        #region Fields
        //private ImageRepository repository;
        //private ImageView _imageView;
        //private Activity _activity;
        #endregion
        
        public JavaCard()
            : this(new Card()) { }

        public JavaCard(Card item)
        {
            AirlineID = item.AirlineID;
            Description = item.Description;
            ID = item.ID;
            CardPictureBytes = item.CardPictureBytes;//TODO: Check if it is necessary for updated pictures
            Currency = item.Currency;
            AcceptedClass = item.AcceptedClass;
            Name = item.Name;
            ImageHandle = item.ImageHandle;

            //repository = new ImageRepository(ImageHandle, new WebApiServiceContext());
            //repository.OnRepositoryChanged += OnImageLoaded;
        }
        
        public static implicit operator Java.Lang.Object(JavaCard item)
        {
            return new JavaObjectBase<JavaCard>(item);
        }

        public static explicit operator JavaCard(Java.Lang.Object item)
        {
            return (item as JavaObjectBase<JavaCard>).Item;
        }
    }

    public class CardsAdapter : ImageAdapter
    {
		protected List<JavaCard> cards = new List<JavaCard>();//it is a parent class
        CardsRepository repository;

        public event ReporitoryChangedHandler AdapterChanged;

        public List<string> GetAutoCompleteOptions()
        {
            return cards.Select(x => x.Name).Distinct().ToList();
        }

        /// <summary>
        /// Loads all the Cards and Airlines from repository
        /// </summary>
        /// <param name="c">The Context of the activity</param>
        public CardsAdapter (Context c) : base(c)
		{
            repository = new CardsRepository();
            repository.OnRepositoryChanged += Cards_OnRepositoryChanged;
            //cards = repository.GetAirlineCards().Select(x=>new JavaCard(x)).ToList();
        }

        public void LoadCards()
        {
            repository.LoadCards();
        }

        public void SearchCards(string keyword)
        {
            repository.SearchCards(keyword);
        }

        private void Cards_OnRepositoryChanged(object repository)
        {
            //if (repository is IEnumerable<Airline>) return;//It's airline refresh!

            //cards = this.repository.GetAirlineCards().Select(x => new JavaCard(x)).ToList();

            if (repository != null) {
                if (repository is IEnumerable<Card>)
                    cards = ((IEnumerable<Card>)repository).Select(x => new JavaCard((Card)x)).ToList();
                else return;//It's airline refresh!
            }

            if (AdapterChanged != null)
                AdapterChanged.Invoke(cards);
        }

        public override int Count {
			get { return cards.Count; }
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return cards[position];
		}

		public override long GetItemId (int position)
		{
			if (cards != null && position >= 0 && position < Count) {
				return (long)cards[position].ID;
			}
			return 0;
		}

        public int GetCardPosition(Card card)
        {
            if (cards == null || card == null) return -1;

            return cards.FindIndex(x => x.ID == card.ID);
        }

        // lazy loading should not be here!!! (Reason: frequent stopping and lagging in scroll)
        public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View gridView;
			TextView lblName;
			ImageView imageView;
			if (convertView == null) {  // if it's not recycled, initialize some attributes
				LayoutInflater inflater = LayoutInflater.FromContext (context);	
				gridView = inflater.Inflate(Resource.Drawable.carditem, null);
			} else {
				gridView = (LinearLayout)convertView;
			}
			imageView =  gridView.FindViewById<ImageView>(Resource.Id.card_item_image);


            if (cards[position].CardPictureBytes != null && cards[position].CardPictureBytes.Length > 0)
            {
                try
                {
                    var bm = BitmapFactory.DecodeByteArray(cards[position].CardPictureBytes, 0, cards[position].CardPictureBytes.Length);
                    if (bm != null)
                    {
                        imageView.SetImageBitmap(bm);
                        //bm.Recycle();
                        //bm = null;
                    }
                }
                catch (System.Exception eex)
                {
                    Crashes.TrackError(eex);
                }
            }
            else if (cards[position].PictureResourceID == 0)
            {
                imageView.SetImageResource(Resource.Drawable.not_available_pic_off);

                //Triggering image load...
                LoadImage(imageView, cards[position].ImageHandle);
                //cards[position].LoadImage(imageView, context, app.ImageRepository);
            }
            else
                imageView.SetImageResource(cards[position].PictureResourceID);	
            
			lblName = gridView.FindViewById<TextView> (Resource.Id.lblCardName);
			lblName.Text = cards [position].DisplayName;
			return gridView;
		}
	}
}

