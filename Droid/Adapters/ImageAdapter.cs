using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Widget;
using Android.Graphics;
using static Android.Graphics.BlurMaskFilter;
using AIMS.Models;
using Microsoft.AppCenter.Analytics;

namespace AIMS.Droid
{
    public abstract class ImageAdapter : BaseAdapter
    {
        protected Context context;
        protected AIMSApp app;
        //private static Dictionary<string, DateTime> loadRequests = new Dictionary<string, DateTime>();

        //private const int imageLoadTimeout = 30;
        
        public ImageAdapter(Context c)
        {
            if (c == null) throw new Exception("ImageLoaderAdapter exception", new Exception("The context object is null"));
            context = c;
            app = (AIMSApp)c.ApplicationContext;
        }

        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            float height = options.OutHeight;
            float width = options.OutWidth;
            double inSampleSize = 1D;

            if (height > reqHeight || width > reqWidth)
            {
                int halfHeight = (int)(height / 2);
                int halfWidth = (int)(width / 2);

                // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
                while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
                {
                    inSampleSize *= 2;
                }
            }

            return (int)inSampleSize;
        }

        public static Bitmap HighlightImage(Bitmap src, int blurRadius = 10)
        {
            // create new bitmap, which will be painted and becomes result image
            //Bitmap bmOut = Bitmap.CreateBitmap(src.Width + 96, src.Height + 96, Bitmap.Config.Argb8888);
            Bitmap bmOut = Bitmap.CreateBitmap(src.Width + (blurRadius*2), src.Height + (blurRadius * 2), Bitmap.Config.Argb8888);
            // setup canvas for painting
            Canvas canvas = new Canvas(bmOut);
            // setup default color
            canvas.DrawColor(new Color(0), PorterDuff.Mode.Clear);
            // create a blur paint for capturing alpha
            Paint ptBlur = new Paint();
            //ptBlur.SetMaskFilter(new BlurMaskFilter(15, Blur.Outer));
            ptBlur.SetMaskFilter(new BlurMaskFilter(blurRadius, Blur.Outer));
            int[] offsetXY = new int[2];
            // capture alpha into a bitmap
            Bitmap bmAlpha = src.ExtractAlpha(ptBlur, offsetXY);
            // create a color paint
            Paint ptAlphaColor = new Paint();
            ptAlphaColor.Color = Color.White;// new Color(int.MaxValue);
            // paint color for captured alpha region (bitmap)
            //canvas.DrawBitmap(bmAlpha, offsetXY[0], offsetXY[1], ptAlphaColor);
            canvas.DrawBitmap(bmAlpha, 0, 0, ptAlphaColor);
            // free memory
            bmAlpha.Recycle();

            // paint the image source
            //canvas.DrawBitmap(src, 0, 0, null);
            canvas.DrawBitmap(src, Math.Abs(offsetXY[0]), Math.Abs(offsetXY[1]), null);

            // return out final image
            return bmOut;
        }

        public void LoadImage(ImageView imageView, string imageHandle, bool forceGet = false)
        {
            if (imageView == null) return;

            //if (loadRequests.Any(x => x.Key.Equals(imageHandle)))//preventing duplicate calls after less than 30 (imageLoadTimeout) seconds
            //{
            //    if (loadRequests[imageHandle].AddSeconds(imageLoadTimeout).CompareTo(DateTime.Now) == -1)
            //        loadRequests[imageHandle] = DateTime.Now;
            //    else return;
            //}
            //else loadRequests.Add(imageHandle, DateTime.Now);

            /*ImageRepository.Current*/
            app.ImageRepository.LoadImage(imageHandle, (imageResult) => {
                if (imageResult == null)
                {
                    LogsRepository.AddError("Error in ImageAdapter.LoadImage()", "result is null");

                    Analytics.TrackEvent("ImageAdapter", new Dictionary<string, string>
                    {
                        { "Error in ImageAdapter.LoadImage()", "result is null" },
                    });

                    return;
                }

                if (imageResult.IsSucceeded)
                {
                    var bytes = imageResult.ReturnParam;
                    
                    if (bytes == null || bytes.Length == 0) return;

                    try
                    {
                        var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                        if (bm != null)
                        {
                            ((Activity)context).RunOnUiThread(() => {
                                imageView.SetImageBitmap(bm);
                            });
                        }
                    }
                    catch (Exception eex)
                    {
                        var m = eex.Message;
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                    }
                }
            }, forceGet);
        }

        public static void LoadImage(string imageHandle, ReporitoryChangedHandler<byte[]> callback, bool forceGet = false)
        {
            if (callback == null) return;

            var ir = new ImageRepository();

            //if (loadRequests.Any(x => x.Key.Equals(imageHandle)))//preventing duplicate calls after less than 30 (imageLoadTimeout) seconds
            //{
            //    if (loadRequests[imageHandle].AddSeconds(imageLoadTimeout).CompareTo(DateTime.Now) == -1)
            //        loadRequests[imageHandle] = DateTime.Now;
            //    else return;
            //}
            //else loadRequests.Add(imageHandle, DateTime.Now);
            
            ir.LoadImage(imageHandle, (imageResult) => {
                if (imageResult == null)
                {
                    LogsRepository.AddError("Error in ImageAdapter.LoadImage()", "result is null");

                    Analytics.TrackEvent("ImageAdapter", new Dictionary<string, string>
                    {
                        { "Error in ImageAdapter.LoadImage()", "result is null" },
                    });


                    ir.Dispose();
                    return;
                }

                if (imageResult.IsSucceeded)
                {
                    var bytes = imageResult.ReturnParam;
                    if (bytes !=null && bytes.Length == 0)
                    {
                        ir.Dispose();
                        return;
                    }

                    try
                    {
                        callback.Invoke(bytes);
                    }
                    catch (Exception eex)
                    {
                        //TODO: Log here
                        var m = eex.Message;
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                    }
                    finally
                    {
                        ir.Dispose();
                    }
                }
            }, forceGet);
        }
    }
}