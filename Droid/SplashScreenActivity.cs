using System;

using Android.App;
using Android.Content;
using Android.OS;
using System.Threading.Tasks;
using Android.Widget;
using Android.Telephony;
using Android.Views;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.PM;
using Ieg.Mobile.Localization;
using AIMS.Models;
using AIMS.Droid.Library;
using Android.Bluetooth;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Support.V4.App;
using Android;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Acr.UserDialogs;
using System.Collections.Generic;

namespace AIMS.Droid
{
    [Activity(Label = "Mobile Lounge", Icon = "@drawable/Icon", MainLauncher = true, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, Theme = "@android:style/Theme.Dialog")]
    public class SplashScreenActivity : Activity
    {
        bool applicationMustClose = false;
        private TextView splashText;
        private static string LogTag = "IEGMobileLounge";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.RequestFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SplashScreen);//TODO: it must be light
            Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            splashText = FindViewById<TextView>(Resource.Id.splashText);
          

            //TODO: Prompt for enabling the GPS
            /*
            
            lertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChatSDK.getSDKInstance().activity);
        alertDialogBuilder
                .setMessage(
                        "GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",

                new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                ChatSDK.getSDKInstance().activity.startActivity(callGPSSettingIntent);
                            }
                        });
//      alertDialogBuilder.setNegativeButton("Cancel",
//              new DialogInterface.OnClickListener() {
//                  public void onClick(DialogInterface dialog, int id) {
//                      dialog.cancel();
//                  }
//              });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

             */

            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.AccessCoarseLocation) != Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new System.String[] { Manifest.Permission.AccessCoarseLocation, Manifest.Permission.AccessFineLocation, Manifest.Permission.Camera, Manifest.Permission.Flashlight, Manifest.Permission.AccessWifiState }, 0);
            }


            UserDialogs.Init(this);
        }

        protected override void OnResume()
        {
            base.OnResume();

            Task startupWork = new Task(() =>
            {
                AIMSApp app = (AIMSApp)Application;
                //TODO: preload more objects and perform some startup work that takes a bit of time...
                RunOnUiThread(() =>
                {
                    #region GPS initiation and Device-Type detection

                    try
                    {
                        BluetoothAdapter myDevice = BluetoothAdapter.DefaultAdapter;

                        var model = Build.Model.StartsWith(Build.Manufacturer) ? Util.Capitalize(Build.Model) : string.Format("{0} {1}", Util.Capitalize(Build.Manufacturer), Build.Model);// UIDevice.CurrentDevice.Model;
                        var name =
                            myDevice == null ?
                                model : //Also using the model if (BT is not available)
                                myDevice.Name;// Using the bluetooth assigned name

                        var localizedModel = Build.Manufacturer;// UIDevice.CurrentDevice.LocalizedModel;
                        var systemName = "Android";//Why not!!! // UIDevice.CurrentDevice.SystemName;
                        var systemVersion = string.Format("{0} Api({1})", Build.VERSION.Release, Build.VERSION.Sdk); //UIDevice.CurrentDevice.SystemVersion;
                        //var zone = UIDevice.CurrentDevice.Zone;
                        var description = string.Format(
                            "{0} device (named: \"{1}\") running {2} ver. {3}, registered and activated by {4} ver. {5}.",
                            model,
                            name,
                            systemName,
                            systemVersion,
                            this.ApplicationContext.ApplicationInfo.ProcessName,
                            this.ApplicationContext.PackageManager.GetPackageInfo(this.ApplicationContext.PackageName, 0).VersionCode);



                        // Initialization of device identifications.
#if DEBUG
                        string serialNumber = "d79d0451";
                        //string serialNumber = "abd5465d4b";//Uncomment this for the developer serial number
                        //string serialNumber = "FakeDeviceUIDFakeDeviceUIDFakeDeviceUIDFakeDeviceUIDFakeDeviceUID";//Uncomment this for the registeration test
                        //string serialNumber = "LGH812a752ba55";//Uncomment this for Olivier's serial number
#else
                        string serialNumber = Android.OS.Build.Serial;
#endif

                        var currentDevice = new MobileDevice
                        {
                            DeviceModel = model,
                            DeviceName = localizedModel,
                            DeviceVersion = string.Format("{0} {1}", systemName, systemVersion),
                            DeviceUniqueId = serialNumber,
                            OwnerSpecificName = name,
                            Description = description
                        };


                        TelephonyManager manager = this.GetSystemService(TelephonyService) as TelephonyManager;
                        bool isTabletSize = Resources.GetBoolean(Resource.Boolean.isTablet);
                        if (manager.PhoneType == PhoneType.None || isTabletSize)
                        {
                            currentDevice.Type = DeviceType.Tablet;
                            //currentDevice.Type = CameraType.FrontCamera;// failed because of bad front camera qualities
                        }
                        else
                        {
                            currentDevice.Type = DeviceType.Phone;
                            //currentDevice.Type = CameraType.BackCamera;
                        }

                        //For the initialization of the logger.
                        var startupFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

                        //Application initiation
                        app.TriggerApplicationInit(startupFolder, new GPSAdapter(this), currentDevice);
                        LogsRepository.AddClientInfo("Application started");

                        Analytics.TrackEvent("SplashScreenActivity", new Dictionary<string, string>
                        {
                            { "Application started", "Application started" }
                        });


                        splashText.Text = ((AIMSApp)Application).TextProvider.GetText(2001);
                    }
                    catch (Exception ex)//TODO: Can be multiple exceptions cather!!
                    {
                        if (ex.Message.Contains("GPS Error"))
                        {
                            LogsRepository.AddError("Error in initiating GPS.", ex);

                            //var progressDialog = Android.App.ProgressDialog.Show(this, "GPS Error", app.TextProvider.GetText((int)ErrorCode.LocationRetrievalException), true, true,
                            //    (sender, e) =>
                            //    {
                            //        applicationMustClose = true;
                            //    });
                        }
                        else
                        {
                            //var progressDialog = Android.App.ProgressDialog.Show(this, "Startup Error", app.TextProvider.GetText((int)ErrorCode.UnknownException), true, true,
                            //    (sender, e) =>
                            //    {
                            //        applicationMustClose = true;
                            //    });
                        }

                        Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                    }


                    app.OnValidationFailed += (sender, code) =>
                    {
                        RunOnUiThread(() =>
                        {
                            Toast.MakeText(this,
                                    app.TextProvider.GetText((int)code),
                                    ToastLength.Long).Show();

                            try
                            {
                                Analytics.TrackEvent("OnValidationFailed", new Dictionary<string, string>
                                {
                                    { "OnValidationFailed", app.TextProvider.GetText((int)code) },
                                    { "Lat", DefaultMobileApplication.Current?.Position?.Latitude.ToString() },
                                    { "Lon", DefaultMobileApplication.Current?.Position?.Longitude.ToString() },
                                    { "Accuracy", DefaultMobileApplication.Current?.Position?.Accuracy.ToString() }
                                });
                            }
                            catch { }


                            if (app.CurrentActivity == null || app.CurrentActivity is LoginActivity) return;

                            //var lastActivity = app.CurrentActivity;

                            try
                            {
                                //TODO: it can be a better design by tracing Activity.Parent.Parent.. 
                                //      or having an active collection of activities as a history

                                if (app.PassengerSummaryForm != null)
                                    app.PassengerSummaryForm.Finish();

                                if (app.BoardingPassForm != null)
                                    app.BoardingPassForm.Finish();

                                if (app.ManualInputForm != null)
                                    app.ManualInputForm.Finish();

                                if (app.CardSelectionForm != null)
                                    app.CardSelectionForm.Finish();

                                if (app.MainForm != null)
                                    app.MainForm.Finish();

                                if (app.LoungeSelectionForm != null)
                                    app.LoungeSelectionForm.Finish();

                                //if (app.CurrentActivity != null && !(app.CurrentActivity is MainActivity) &&
                                //!(app.CurrentActivity is ManualInputActivity) && !(app.CurrentActivity is SummaryActivity))
                                app.CurrentActivity.Finish();
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Error in logging out due to session expire.", ex);

                                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex, new Dictionary<string, string> {
                                { "FunctionName", "OnValidationFailed" }
                            });
                            }

                            StartActivity(new Intent(this, typeof(LoginActivity)));
                        });
                    };

                    RunOnUiThread(() =>
                    {
                        app.intentMain = new Intent(this, typeof(MainActivity));
                        app.SummaryIntent = new Intent(this, typeof(ValidationResultActivity));//TODO: New boarding pass design //TODO: remove it when new design tested
                        //app.BoardingPassIntent = new Intent(this, typeof(BoardingPassActivity));
                    });
                    #endregion

#if DEBUG
                    #region The test codes without requiring login can be placed here

                    #endregion
#else
                Java.Lang.Thread.Sleep(1000);//Task.Delay(2000);  //TODO: remove this line if you don't need to see splash screen (just simulate a bit of startup work.)
#endif
                });

                while (DefaultMobileApplication.Current.Position == null && !applicationMustClose)//waiting for the GPS position detection
                {
                    Java.Lang.Thread.Sleep(10);//Task.Delay(10);
                }
            });

            startupWork.ContinueWith(t =>
            {
                if (applicationMustClose)
                {
                    Process.KillProcess(Process.MyPid());
                }
                else
                {
                    if (t.IsFaulted == true || t.Status != TaskStatus.RanToCompletion)
                    {
                        RunOnUiThread(() =>
                        {
                            Toast.MakeText(this, LanguageRepository.Current.GetText(1051), ToastLength.Long).Show();

                            Analytics.TrackEvent("startupWork.ContinueWith", new Dictionary<string, string>
                            {
                                                { "startupWork.ContinueWith", LanguageRepository.Current.GetText(1051) }
                            });
                        });
                        //return;
                    }
                    else
                    {
                        if (LanguageRepository.Current.SelectedLanguage.HasValue)
                            StartActivity(new Intent(Application.Context, typeof(LoginActivity)));
                        else
                            StartActivity(new Intent(Application.Context, typeof(LanguageSelectionActivity)));
                    }
                    Finish();
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());

            startupWork.Start();
        }
    }
}