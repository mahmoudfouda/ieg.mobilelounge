﻿using System;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Widget;
using Android.OS;
using Android.Views.Animations;
using System.Threading.Tasks;
using Ieg.Mobile.Localization;
using AIMS.Droid.Library;
using System.Net;
using System.Collections.Generic;
using Android.Runtime;
using Acr.UserDialogs;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS", Icon = "@drawable/Icon", WindowSoftInputMode = Android.Views.SoftInput.AdjustPan,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]//, Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class LoginActivity : BaseActivity
    {
        #region Elements
        protected Button btnLogin, btnCancel;
        protected EditText txtUName, txtPassword;
        protected LinearLayout frmLogin;
        protected TextView btnWebSiteLink;
        private Animation fadeIn;
        #endregion

        #region Fields
        //Location currentLocation;

        //public TokenResponse m_TokenResponse { get; private set; }
        //public static TokenResponse _clientToken { get; private set; }
        #endregion

        #region Methods

        public void InitializeControls()
        {
            SetContentView(Resource.Layout.Login);

            frmLogin = FindViewById<LinearLayout>(Resource.Id.frmLogin);

            btnWebSiteLink = FindViewById<TextView>(Resource.Id.btnWebSiteLink);

            btnLogin = FindViewById<Button>(Resource.Id.btnSignIn);
            btnCancel = FindViewById<Button>(Resource.Id.btnCancelSignIn);

            txtUName = FindViewById<EditText>(Resource.Id.txtUName);
            txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);

            try
            {
                var lblVersion = FindViewById<TextView>(Resource.Id.lblVersion);
                lblVersion.Text = this.ApplicationContext.PackageManager.GetPackageInfo(PackageName, 0).VersionName;
            }
            catch { }

            LoadUITexts(App.TextProvider.SelectedLanguage);

#if DEBUG
            //var dialog = new AIMSMessageDialogFragment();
            //dialog.Show(new Android.Support.V4.App.FragmentManager(), "dialog");

            //var dialog = new AIMSMessageActivity();
            //dialog.Show("","");

            //AIMSMessage("Test title", "Test message with the full body to appear.\nWith new line supporting.");
            //AIMSConfirm("Test title", "Test question with the full body to appear.\nWith new line supporting.");


            //#region GPS initiation
            //App.GPS.LocationChanged += (object sender, Location e) =>
            //{
            //    if (e == null) return;
            //    RunOnUiThread(() =>
            //    {
            //        Toast.MakeText(this,
            //            string.Format("New loc: {0} | {1} ({2})", e.Latitude, e.Longitude, e.Accuracy),
            //            ToastLength.Short).Show();//TODO: remove this
            //    });
            //};
            //#endregion

            //txtUName.Text = "testlx";// LX test
            //txtPassword.Text = "m1%AIMS1024";// LX test and ZH test
            //txtUName.Text = "testos";// ZH test
            //txtPassword.Text = "Mobile001";// LX test and ZH test

            txtUName.Text = "mobile";// IEG test
            txtPassword.Text = "Password001";// IEG test

            //txtUName.Text = "mobileac";
            //txtPassword.Text = "QamC704!l%";

             txtUName.Text = "V746361";// UA test
             txtPassword.Text = "engine3ring4";// UA test


            //txtUName.Text = "mobiletester";// DEMO test
            //txtPassword.Text = "m2+IEG1131";// DEMO test


#endif

            btnWebSiteLink.Click += (sender, e) =>
            {
                var webViewIntent = new Intent(this, typeof(WebViewerActivity));
                webViewIntent.PutExtra("title", "IEG Inc.");
                webViewIntent.PutExtra("url", "http://www.ieg-america.com");
                StartActivity(webViewIntent);
            };

            //MembershipProvider.Current.OnLogin += Current_OnLogin;//TODO: constructor..

            btnLogin.Click += /*async*/ (sender, args) =>
            {
                //var token = await GetTokenAsync();
                //var webApiResult = /*await*/ CallWebApiAsync();
                //var res = AuthenticateUser(txtUName.Text, txtPassword.Text, App.SerialNumber);
                //GetJson<ResultPack<Contracts.Login.ADUSRS>>("user/"+ txtUName.Text, null,null,null);
                App.ContinueScanning = false;
                App.IsScanningCancelled = false;

                DoLogin();
            };
            txtPassword.EditorAction += (_, e) =>
            {
                if (e.ActionId == Android.Views.InputMethods.ImeAction.Go)
                {
                    DoLogin();
                }
            };

            btnCancel.Click += delegate
            {
                DoCancel();
            };

        }

        private IWebProxy GetProxy()
        {
            //Android.Net.Proxy
            //var proxyURI = new System.Uri(string.Format("{0}:{1}", "your specific proxy host", 80));
            //ICredentials credentials = new System.Net.NetworkCredential("username", "password");
            //WebProxy proxy = new System.Net.WebProxy(proxyURI, true, null, credentials);
            //return proxy;

            HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");

            // Obtain the 'Proxy' of the  Default browser.  
            IWebProxy proxy = myWebRequest.Proxy;
            return proxy;
        }

        protected void DoLogin()
        {
            ShowLoading(App.TextProvider.GetText(2502)/*"Validating username and password"*/);
            var loginTask = new Task(new Action(() =>
            {
                MembershipProvider.CreateMembershipProvider(txtUName.Text, txtPassword.Text, GetProxy());//, App.SerialNumber);
                HideLoading();
                ShowLoading(App.TextProvider.GetText(2511)/*"Authenticating"*/);
                MembershipProvider.Current.Login(txtUName.Text, txtPassword.Text/*, App.SerialNumber, DefaultMobileApplication.Position*/, Current_OnLogin);//TODO: check position working
            }));
            loginTask.Start();
        }

        //private void Gps_OnValidationFailed(object ss, EventArgs ee)
        //{
        //    RunOnUiThread(() =>
        //    {
        //        if (App.CurrentActivity == null || App.CurrentActivity is LoginActivity) return;

        //        Toast.MakeText(this,
        //                App.TextProvider.GetText((int)ErrorCode.LocationValidationException),
        //                ToastLength.Long).Show();
        //        var lastActivity = App.CurrentActivity;
        //        StartActivity(new Intent(this, typeof(LoginActivity)));//TODO: testing
        //        lastActivity.Finish();
        //    });
        //}

        private void Current_OnLogin(object sender, Tuple<bool, ErrorCode> e)
        {
            if (e.Item1)
            {
                LogsRepository.AddClientInfo("User logged in", MembershipProvider.Current.LoggedinUser.ToString());

                Analytics.TrackEvent("LoginActivity", new Dictionary<string, string>
                {
                    { "User logged in", MembershipProvider.Current.LoggedinUser.ToString() },
                });



                if (MembershipProvider.Current.UserLounges.Count() > 0)
                {
                    //The class (WebApiServiceContext) has to be instantiated after a successfull login (new WebApiServiceContext(txtUName.Text, txtPassword.Text, App.SerialNumber))
                    //if (App.GpsValidationProvider == null)
                    //{
                    //    App.GpsValidationProvider = new GpsValidationProvider(new WebApiServiceContext());
                    //    App.GpsValidationProvider.OnGpsValidationFailed += Gps_OnValidationFailed;
                    //}
                    try
                    {
                        App.TriggerApplicationStart();//TODO: Make it automatic
                        if (MembershipProvider.Current.SelectedLounge == null)
                            StartActivity(new Intent(this, typeof(LoungeSelectionActivity)));
                        else//go to airlines activity
                        {
                            LogsRepository.AddClientInfo("Lounge selected", string.Format("The user {0} selected the lounge", MembershipProvider.Current.LoggedinUser.Username));
                            LogsRepository.AddInfo("Lounge selected", string.Format("Lounge detail: {0} | WSID: {1}", MembershipProvider.Current.SelectedLounge.LoungeName, MembershipProvider.Current.SelectedLounge.WorkstationID));

                            Analytics.TrackEvent("LoginActivity", new Dictionary<string, string>
                            {
                                { "Lounge selected", string.Format("The user {0} selected the lounge", MembershipProvider.Current.LoggedinUser.Username) },
                                { "Lounge selected Detail", string.Format("Lounge detail: {0} | WSID: {1}", MembershipProvider.Current.SelectedLounge.LoungeName, MembershipProvider.Current.SelectedLounge.WorkstationID) }
                            });

                            StartActivity(new Intent(this, typeof(MainActivity)));
                        }
                        Finish();
                    }
                    catch (Exception ex)
                    {
                        LogsRepository.AddError("Error in staring activities", ex);
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                    }
                }
                else
                {
                    LogsRepository.AddClientWarning("No lounge is available", string.Format("There were no lounge for the user {0}", MembershipProvider.Current.LoggedinUser.Username));

                    Analytics.TrackEvent("LoginActivity", new Dictionary<string, string>
                    {
                        { "No lounge is available", string.Format("There were no lounge for the user {0}", MembershipProvider.Current.LoggedinUser.Username) }
                    });



                    AIMSMessage(App.TextProvider.GetText(2049), App.TextProvider.GetText(1005));
                    //RunOnUiThread(() =>
                    //{
                    //    Toast.MakeText(this, App.TextProvider.GetText(1005)/*"No lounges are assigned to this user"*/, ToastLength.Short).Show();
                    //});
                }
            }
            else
            {
                string errorMessage = App.TextProvider.GetText((int)e.Item2);
                LogsRepository.AddError("User login failed", string.Format("A user with username '{0}' failed to login into AIMS,\nError Code: {1}\nError Message: {2}", txtUName.Text, e.Item2, errorMessage));

                Analytics.TrackEvent("LoginDialogActivity", new Dictionary<string, string>
                {
                    { "User login failed", string.Format("A user with username '{0}' failed to login into AIMS,\nError Code: {1}\nError Message: {2}", txtUName.Text, e.Item2, errorMessage) }
                });


                AIMSMessage(App.TextProvider.GetText(2047), errorMessage);
                //RunOnUiThread(() =>
                //{
                //    Toast.MakeText(this, errorMessage, ToastLength.Long).Show();
                //});
                //(new AlertDialog.Builder(this)).SetTitle("Error").SetMessage(res.Message).SetIcon(Android.Resource.Drawable.StatNotifyError).Show();
            }
            HideLoading();
        }

        protected void DoCancel()
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            Title = "Agents entrance";

            txtUName.Hint = App.TextProvider.GetText(2002);
            txtPassword.Hint = App.TextProvider.GetText(2003);
            btnLogin.Text = App.TextProvider.GetText(2005);
            btnCancel.Text = App.TextProvider.GetText(2004);
        }

        #endregion

        #region Events
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            ShowLoading(App.TextProvider.GetText(2501)/*"Application is starting"*/);
            InitializeControls();

            fadeIn = AnimationUtils.LoadAnimation(this, Resource.Animation.login_fadein);
            HideLoading();
            frmLogin.StartAnimation(fadeIn);

            Microsoft.AppCenter.AppCenter.LogLevel = Microsoft.AppCenter.LogLevel.Verbose;
            Crashes.ShouldProcessErrorReport = ShouldProcess;
            Crashes.NotifyUserConfirmation(UserConfirmation.AlwaysSend);
            Crashes.ShouldAwaitUserConfirmation = ConfirmationHandler;

            Microsoft.AppCenter.AppCenter.Start("1898c67a-e3cd-4bdc-a681-8dd899e30757", typeof(Analytics), typeof(Crashes));

            Microsoft.AppCenter.AppCenter.GetInstallIdAsync().ContinueWith(installId =>
            {
                AppCenterLog.Info(LogTag, "AppCenter.InstallId=" + installId.Result);
            });

            Crashes.HasCrashedInLastSessionAsync().ContinueWith(hasCrashed =>
            {
                AppCenterLog.Info(LogTag, "Crashes.HasCrashedInLastSession=" + hasCrashed.Result);
            });

            Crashes.GetLastSessionCrashReportAsync().ContinueWith(report =>
            {
                AppCenterLog.Info(LogTag, "Crashes.LastSessionCrashReport.Exception=" + report.Result?.Exception);
            });

        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            string uName = "", pass = "";
            if (txtUName != null && txtPassword != null)
            {
                uName = txtUName.Text;
                pass = txtPassword.Text;
            }

            InitializeControls();

            this.txtUName.Text = uName;
            this.txtPassword.Text = pass;
        }
        public override void OnBackPressed()
        {
            DoCancel();
            base.OnBackPressed();
        }
        #endregion


        #region AppCenter

        static LoginActivity()
        {
            Crashes.SendingErrorReport += LoginActivity.SendingErrorReportHandler;
            Crashes.SentErrorReport += LoginActivity.SentErrorReportHandler;
            Crashes.FailedToSendErrorReport += LoginActivity.FailedToSendErrorReportHandler;
        }


        private static string LogTag = "IEGMobileLounge";

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public static void SendingErrorReportHandler(object sender, SendingErrorReportEventArgs e)
        {
            AppCenterLog.Info(LogTag, "Sending error report");
            var args = e as SendingErrorReportEventArgs;
            ErrorReport report = args.Report;

            //test some values
            if (report.Exception != null)
            {
                AppCenterLog.Info(LogTag, report.Exception.ToString());
            }
            else if (report.AndroidDetails != null)
            {
                AppCenterLog.Info(LogTag, report.AndroidDetails.ThreadName);
            }
        }

        public static void SentErrorReportHandler(object sender, SentErrorReportEventArgs e)
        {
            AppCenterLog.Info(LogTag, "Sent error report");
            var args = e as SentErrorReportEventArgs;
            ErrorReport report = args.Report;

            //test some values
            if (report.Exception != null)
            {
                AppCenterLog.Info(LogTag, report.Exception.ToString());
            }
            else
            {
                AppCenterLog.Info(LogTag, "No system exception was found");
            }

            if (report.AndroidDetails != null)
            {
                AppCenterLog.Info(LogTag, report.AndroidDetails.ThreadName);
            }
        }

        public static void FailedToSendErrorReportHandler(object sender, FailedToSendErrorReportEventArgs e)
        {
            AppCenterLog.Info(LogTag, "Failed to send error report");
            var args = e as FailedToSendErrorReportEventArgs;
            ErrorReport report = args.Report;

            //test some values
            if (report.Exception != null)
            {
                AppCenterLog.Info(LogTag, report.Exception.ToString());
            }
            else if (report.AndroidDetails != null)
            {
                AppCenterLog.Info(LogTag, report.AndroidDetails.ThreadName);
            }

            if (e.Exception != null)
            {
                AppCenterLog.Info(LogTag, "There is an exception associated with the failure");
            }
        }

        public static bool ShouldProcess(ErrorReport report)
        {
            AppCenterLog.Info(LogTag, "Determining whether to process error report");
            return true;
        }

        public bool ConfirmationHandler()
        {
            RunOnUiThread(() =>
            {
                var result = UserDialogs.Instance.ActionSheetAsync("Crash detected. Send anonymous crash report?", "Cancel", "Sair",
                    null, new string[] { "Send", "Always Send", "Don't Send" }).ContinueWith((arg) =>
                    {
                        var answer = arg.Result;
                        UserConfirmation userConfirmationSelection;
                        if (answer == "Send")
                        {
                            userConfirmationSelection = UserConfirmation.Send;
                        }

                        else if (answer == "Always Send")
                        {
                            userConfirmationSelection = UserConfirmation.AlwaysSend;
                        }
                        else
                        {
                            userConfirmationSelection = UserConfirmation.DontSend;
                        }

                        AppCenterLog.Debug(LogTag, "User selected confirmation option: \"" + answer + "\"");

                        Crashes.NotifyUserConfirmation(userConfirmationSelection);

                    });
            });

            return true;
        }

        #endregion
    }
}