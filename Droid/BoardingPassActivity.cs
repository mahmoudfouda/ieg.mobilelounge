using System;

using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using System.Timers;
using System.Globalization;
using Android.Graphics;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS", Icon = "@drawable/Icon", Theme = "@android:style/Theme.NoTitleBar.OverlayActionModes", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class BoardingPassActivity : BaseActivity
    {
        #region Constants
        const int refreshInterval = 4000;
        #endregion

        #region Elements
        ScrollView rootScrollView;
        //TableLayout tblDeparture, tblArrival;
        LinearLayout boardingPassContentHolder/*, boardingPassTopSection, boardingPassMiddleSection*/;
        RelativeLayout pnlServices;
        View barcodeSeparatorBefore, servicesSeparatorBefore, servicesSeparatorAfter;

        ListView lstServices;
        ImageView imgDepartureTime, /*imgDepartureGate,  */imgRemainingDepartureTime, imgArrivalWeather, imgSummarySelectedCard, imgSummarySelectedCardOther;
        TextView lblPassengerName, lblFFN,/* lblClass,*/ lblFlightNumber, /*lblAirlineLabel, lblCardLabel, */lblFFNLabel,
            /* lblClassLabel,*/ lblSeat, lblTerminal, lblSeatLabel, lblTerminalLabel,/* lblArrivalGate,*/ lblSummaryAirlineName, lblSummaryAirlineNameLabel, 
            lblRemainingDepartureTime, lblDepartureTime, lblDepartureDate, lblBoardingTime, lblBoardingTimeLabel, lblPriceSum, lblDepartureGateTop, lblDepartureGate, 
            lblDepartureGateLabelTop, lblDepartureGateLabel, /*lblDepartureGateComment, lblArrivalTime, lblArrivalTimeComment, lblDepartureComment,*/lblFlightNumberLabel, 
            lblServicesLabel, lblArrivalWeatherTemperature/*, lblArrivalWeatherComment, lblArrivalWeatherLocation*/, lblArrivalAirportLabel, lblArrivalAirport, 
            lblDepartureAirportLabel, lblDepartureAirport;
        Button btnBarcodeDetails;
        #endregion

        #region Fields
        Timer refresherTimer;
        bool isInitialized = false;
        DateTime? departureTime, arrivalTime;

        string delayedText, onTimeText;
        #endregion
        
        #region Properties
        private string BarcodeString { get; set; }
        #endregion

        private void RefreshPassenger()
        {
            //TODO: do not use text provider here (session will be kept alive!!)

            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            RunOnUiThread(new Action(() => {

                if (MembershipProvider.Current.SelectedCard != null)
                {
                    if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                    {
                        var bm = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedCard.CardPictureBytes, 0, MembershipProvider.Current.SelectedCard.CardPictureBytes.Length);
                        if (bm != null)
                        {
                            imgSummarySelectedCard.SetImageBitmap(bm);//TODO: add it in the AXML file
                            //bm.Recycle();
                            //bm = null;
                        }
                    }
                    else
                    {
                        //(new JavaCard(MembershipProvider.Current.SelectedCard)).LoadImage(imgSummarySelectedCard, this, App.ImageRepository);
                        var item = (new JavaCard(MembershipProvider.Current.SelectedCard));
                        /*ImageRepository.Current*/
                        App.ImageRepository.LoadImage(item.ImageHandle, (imageResult) =>
                        {
                            if (imageResult == null)
                            {
                                LogsRepository.AddError("Error in BoardingPassActivity in loading card image", "result is null");
                                return;
                            }

                            if (imageResult.IsSucceeded)
                            {
                                var bytes = imageResult.ReturnParam;

                                //item.SaveImage(bytes);//Removed IImageItem and it is automatically saved in the ImageRepository

                                if (bytes.Length == 0) return;

                                try
                                {
                                    var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                                    if (bm != null)
                                    {
                                        this.RunOnUiThread(() =>
                                        {
                                            imgSummarySelectedCard.SetImageBitmap(bm);//TODO: add it in the AXML file
                                        });
                                        //bm.Recycle();
                                        //bm = null;
                                    }
                                }
                                catch (System.Exception eex)
                                {
                                    Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                                }
                            }
                        });
                    }
                }

                if (MembershipProvider.Current.SelectedPassenger.OtherCardID != 0)
                {
                    var otherCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedPassenger.OtherCardID);
                    if (otherCard != null)
                    {
                        if (otherCard.CardPictureBytes != null && otherCard.CardPictureBytes.Length > 0)
                        {
                            var bm = BitmapFactory.DecodeByteArray(otherCard.CardPictureBytes, 0, otherCard.CardPictureBytes.Length);
                            if (bm != null)
                            {
                                imgSummarySelectedCardOther.SetImageBitmap(bm);//TODO: add it in the AXML file
                                //bm.Recycle();
                                //bm = null;
                            }
                        }
                        else
                        {
                            //(new JavaCard(otherCard)).LoadImage(imgSummarySelectedCardOther, this, App.ImageRepository);
                            var item = (new JavaCard(otherCard));
                            /*ImageRepository.Current*/
                            App.ImageRepository.LoadImage(item.ImageHandle, (imageResult) =>
                            {
                            if (imageResult == null)
                            {
                                LogsRepository.AddError("Error in BoardingPassActivity in loading othercard image", "result is null");
                                return;
                            }

                                if (imageResult.IsSucceeded)
                                {
                                    var bytes = imageResult.ReturnParam;

                                    //item.SaveImage(bytes);//Removed IImageItem and it is automatically saved in the ImageRepository

                                    if (bytes.Length == 0) return;

                                    try { 
                                        var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                                        if (bm != null)
                                        {
                                            this.RunOnUiThread(() =>
                                            {
                                                imgSummarySelectedCardOther.SetImageBitmap(bm);//TODO: add it in the AXML file
                                            });
                                            //bm.Recycle();
                                            //bm = null;
                                        }
                                    }
                                    catch (System.Exception eex)
                                    {
                                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                                    }
                                }
                            });
                        }
                    }
                    else imgSummarySelectedCardOther.Visibility = ViewStates.Gone;
                }
                else imgSummarySelectedCardOther.Visibility = ViewStates.Gone;


                lblPassengerName.Text = MembershipProvider.Current.SelectedPassenger.FullName;
                lblFFN.Text = MembershipProvider.Current.SelectedPassenger.FFN;
                //lblClass.Text = MembershipProvider.Current.SelectedPassenger.TrackingClassOfService;
                lblFlightNumber.Text = string.Format("{0} {1}", MembershipProvider.Current.SelectedPassenger.FlightCarrier, MembershipProvider.Current.SelectedPassenger.FlightNumber);
                lblSummaryAirlineName.Text = MembershipProvider.Current.SelectedAirline.Name;
                if(!lblDepartureAirportLabel.Text.EndsWith(MembershipProvider.Current.SelectedPassenger.FromAirport))
                    lblDepartureAirportLabel.Text = string.Format("{0} {1}", lblDepartureAirportLabel.Text, MembershipProvider.Current.SelectedPassenger.FromAirport);
                lblDepartureAirport.Text = MembershipProvider.Current.SelectedPassenger.FromCity;
                
                if (!lblArrivalAirportLabel.Text.EndsWith(MembershipProvider.Current.SelectedPassenger.ToAirport))
                    lblArrivalAirportLabel.Text = string.Format("{0} {1}", lblArrivalAirportLabel.Text, MembershipProvider.Current.SelectedPassenger.ToAirport);
                lblArrivalAirport.Text = MembershipProvider.Current.SelectedPassenger.ToCity;
                
                //lblDepartureGate.Text = "N/A";//TODO: from flightstat (get the gate number)
                //lblDepartureGateComment.Text = "";//TODO: get the gate comments

                if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.BarcodeString))
                    BarcodeString = MembershipProvider.Current.SelectedPassenger.BarcodeString;
                else btnBarcodeDetails.Visibility = ViewStates.Gone;
            }));
        }

        private void RefreshTimes()
        {
            //TODO: do not use text provider here (session will be kept alive!!)

            if (MembershipProvider.Current != null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            RunOnUiThread(new Action(() => {
                //TODO: Compute the diff between server time and client time

                //TODO: uncomment these below
                var currentTime = DateTime.Now;//TODO: it must be the server current given time
                //var departureTime = MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate;
                //var departureTime = MembershipProvider.Current.SelectedPassenger.DepartureTime;
                if (!departureTime.HasValue)
                {
                    departureTime = currentTime.AddMinutes(45).AddSeconds(5);

                    //var arrivalTime = MembershipProvider.Current.SelectedPassenger.ArrivalTime;
                    arrivalTime = departureTime.Value.AddHours(16).AddMinutes(35);
                }


                var remainingTime = departureTime.Value.Subtract(currentTime);

                lblRemainingDepartureTime.SetTextColor(new Android.Graphics.Color(0, 233, 0));//green
                imgDepartureTime.SetImageResource(Resource.Drawable.departure_time_medium);
                //imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate);
                if (remainingTime.Hours > 0)
                {
                    lblRemainingDepartureTime.Text = string.Format("+{0}h {1}m", remainingTime.Hours, remainingTime.Minutes);
                }
                else if (remainingTime.Minutes > 0)
                {
                    lblRemainingDepartureTime.Text = string.Format("+{0}m", remainingTime.Minutes);
                    if (remainingTime.Minutes < 45)
                    {
                        //imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate_green);//TODO: get gate status
                        lblRemainingDepartureTime.SetTextColor(new Android.Graphics.Color(255, 180, 43));//orange
                        imgDepartureTime.SetImageResource(Resource.Drawable.departure_time_medium_green);
                    }
                    else if (remainingTime.Minutes <= 15)
                    {
                        //imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate_green);//TODO: get gate status
                        lblRemainingDepartureTime.SetTextColor(new Android.Graphics.Color(255, 43, 43));//red
                        imgDepartureTime.SetImageResource(Resource.Drawable.departure_time_medium_red);
                    }
                }
                else
                {
                    //imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate_red);//TODO: get gate status
                    lblRemainingDepartureTime.Text = "";
                }

                //lblDepartureTime.Text = string.Format("{0:h:m tt}", Passenger.DepartureTime);
                //lblDepartureTime.Text = MembershipProvider.Current.SelectedPassenger.DepartureTime.ToString("hh:mm:ss tt", CultureInfo.InvariantCulture);
                lblDepartureDate.Text = departureTime.Value.ToString("dd MMMM yyyy", CultureInfo.InvariantCulture);
                lblDepartureTime.Text = departureTime.Value.ToString("hh:mm tt", CultureInfo.InvariantCulture);
                //lblDepartureComment.Text = onTimeText;//TODO: get delay or ontime status (On time)
                //lblDepartureComment.SetTextColor(new Android.Graphics.Color(0, 233, 0));//green
                //lblArrivalTime.Text = arrivalTime.Value.ToString("MMMM dd, yyyy\nhh:mm tt", CultureInfo.InvariantCulture);
                //lblArrivalTimeComment.Text = delayedText;//TODO: get delay or ontime status (Delayed)
                //lblArrivalTimeComment.SetTextColor(new Android.Graphics.Color(255, 180, 43));//orange
                //lblArrivalGate.Text = "";//TODO: get the passenger arrival gate

                //imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate_***);//TODO: get gate status
                //imgArrivalTime.SetImageResource(Resource.Drawable.arrival_time_medium);//TODO: Ask if it must be changed

            }));
        }

        private void RefreshWeather()
        {
            //TODO: do not use text provider here (session will be kept alive!!)
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            RunOnUiThread(new Action(() => {
                //TODO: update all the weather lables and images on the screen..

                //lblArrivalWeatherTemperature
                //lblArrivalWeatherComment
                //lblArrivalWeatherLocation

                //imgArrivalWeather
            }));
        }

        private void DisposeTimers()//TODO: dispose all the timers here
        {
            if (MembershipProvider.Current != null)
                MembershipProvider.Current.SelectedPassenger = null;//passenger is cleared
            if (refresherTimer == null) return;
            refresherTimer.Enabled = false;
            refresherTimer.Dispose();
            refresherTimer = null;
        }

        ~BoardingPassActivity()
        {
            DisposeTimers();
        }

        public void StartTimers()//TODO: create all the timers here
        {
            if (refresherTimer != null) return;
            refresherTimer = new Timer(refreshInterval);
            refresherTimer.Elapsed += OnRefreshEvent;
            refresherTimer.Enabled = true;
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            onTimeText = App.TextProvider.GetText(2021);
            delayedText = App.TextProvider.GetText(2020);
            //lblSummaryTitle.Text = App.TextProvider.GetText(2006);//Welcome
            //lblCardLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2013));//Card: 
            //lblClassLabel.Text = App.TextProvider.GetText(2014);//Class
            //lblFlightNumberLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2017));//Flight Info: 
            lblFlightNumberLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2015).ToUpper());//FLIGHT
            lblDepartureGateLabelTop.Text = string.Format("{0}: ", App.TextProvider.GetText(2058).ToUpper());//GATE
            lblDepartureAirportLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2059).ToUpper());//FROM
            lblArrivalAirportLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2060).ToUpper());//TO
            lblBoardingTimeLabel.Text = App.TextProvider.GetText(2061).ToUpper();//BOARDING AT
            lblTerminalLabel.Text = App.TextProvider.GetText(2062).ToUpper();//TERMINAL
            lblSeatLabel.Text = App.TextProvider.GetText(2063).ToUpper();//SEAT
            //lblDepartureAirportLabel.Text = App.TextProvider.GetText(2018);// string.Format("{0}: ",App.TextProvider.GetText(2018));//Departure:  
            //lblArrivalAirportLabel.Text = App.TextProvider.GetText(2019);// string.Format("{0}: ", App.TextProvider.GetText(2019));//Arrival: 
            lblFFNLabel.Text = App.TextProvider.GetText(2027);//FFN
            lblServicesLabel.Text = App.TextProvider.GetText(2043);//"Services";
            btnBarcodeDetails.Text = App.TextProvider.GetText(2045);//"Details"
            //lblAirlineLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2023));//"Airline: "
        }


        #region Events
        public override void OnActionModeFinished(ActionMode mode)
        {
            DisposeTimers();
            base.OnActionModeFinished(mode);
        }

        protected override void OnPause()
        {
            DisposeTimers();
            base.OnPause();
        }

        protected override void OnStop()
        {
            DisposeTimers();
            base.OnStop();
        }

        protected override void OnStart()
        {
            StartTimers();
            base.OnStart();
        }

        protected override void OnResume()
        {
            StartTimers();
            base.OnResume();
        }

        public override void OnActionModeStarted(ActionMode mode)
        {
            StartTimers();
            base.OnActionModeStarted(mode);
        }

        private void OnRefreshEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            RefreshTimes();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.BoardingPass);
            App.BoardingPassForm = this;

            Title = MembershipProvider.Current.UserHeaderText;


            #region Initializing and getting controls

            pnlServices = FindViewById<RelativeLayout>(Resource.Id.pnlServices);
            boardingPassContentHolder = FindViewById<LinearLayout>(Resource.Id.boardingPassContentHolder);

            btnBarcodeDetails = FindViewById<Button>(Resource.Id.btnBarcodeDetails);

            servicesSeparatorBefore = FindViewById<View>(Resource.Id.servicesSeparatorBefore);
            servicesSeparatorAfter = FindViewById<View>(Resource.Id.servicesSeparatorAfter);

            rootScrollView = FindViewById<ScrollView>(Resource.Id.rootScrollView);
            lstServices = FindViewById<ListView>(Resource.Id.lstServices);

            //lblSummaryTitle = FindViewById<TextView>(Resource.Id.lblSummaryTitle);
            lblPassengerName = FindViewById<TextView>(Resource.Id.lblPassengerName);
            lblSummaryAirlineNameLabel = FindViewById<TextView>(Resource.Id.lblSummaryAirlineNameLabel);
            lblSummaryAirlineName = FindViewById<TextView>(Resource.Id.lblSummaryAirlineName);
            lblFFNLabel = FindViewById<TextView>(Resource.Id.lblFFNLabel);
            //lblClassLabel = FindViewById<TextView>(Resource.Id.lblClassLabel);
            lblFlightNumberLabel = FindViewById<TextView>(Resource.Id.lblFlightNumberLabel);
            lblDepartureAirportLabel = FindViewById<TextView>(Resource.Id.lblDepartureAirportLabel);
            lblArrivalAirportLabel = FindViewById<TextView>(Resource.Id.lblArrivalAirportLabel);
            //lblArrivalGate = FindViewById<TextView>(Resource.Id.lblArrivalGate);
            lblServicesLabel = FindViewById<TextView>(Resource.Id.lblServicesLabel);
            //lblTierStatus = FindViewById<TextView>(Resource.Id.lblTierStatus);
            lblFFN = FindViewById<TextView>(Resource.Id.lblFFN);
            lblSeat = FindViewById<TextView>(Resource.Id.lblSeat);
            lblSeatLabel = FindViewById<TextView>(Resource.Id.lblSeatLabel);
            lblTerminal = FindViewById<TextView>(Resource.Id.lblTerminal);
            lblTerminalLabel = FindViewById<TextView>(Resource.Id.lblTerminalLabel);
            //lblClass = FindViewById<TextView>(Resource.Id.lblClass);
            lblFlightNumber = FindViewById<TextView>(Resource.Id.lblFlightNumber);
            lblRemainingDepartureTime = FindViewById<TextView>(Resource.Id.lblRemainingDepartureTime);
            lblDepartureAirport = FindViewById<TextView>(Resource.Id.lblDepartureAirport);
            lblDepartureTime = FindViewById<TextView>(Resource.Id.lblDepartureTime);
            lblDepartureDate = FindViewById<TextView>(Resource.Id.lblDepartureDate);
            lblDepartureGateTop = FindViewById<TextView>(Resource.Id.lblDepartureGateTop);
            lblDepartureGateLabelTop = FindViewById<TextView>(Resource.Id.lblDepartureGateLabelTop);
            lblBoardingTime = FindViewById<TextView>(Resource.Id.lblBoardingTime);
            lblBoardingTimeLabel = FindViewById<TextView>(Resource.Id.lblBoardingTimeLabel);
            lblPriceSum = FindViewById<TextView>(Resource.Id.lblPriceSum);
            lblDepartureGate = FindViewById<TextView>(Resource.Id.lblDepartureGate);
            lblDepartureGateLabel = FindViewById<TextView>(Resource.Id.lblDepartureGateLabel);
            lblArrivalAirport = FindViewById<TextView>(Resource.Id.lblArrivalAirport);
            //lblArrivalTime = FindViewById<TextView>(Resource.Id.lblArrivalTime);
            //lblArrivalTimeComment = FindViewById<TextView>(Resource.Id.lblArrivalTimeComment);
            lblArrivalWeatherTemperature = FindViewById<TextView>(Resource.Id.lblArrivalWeatherTemperature);
            //lblArrivalWeatherComment = FindViewById<TextView>(Resource.Id.lblArrivalWeatherComment);
            //lblArrivalWeatherLocation = FindViewById<TextView>(Resource.Id.lblArrivalWeatherLocation);

            imgDepartureTime = FindViewById<ImageView>(Resource.Id.imgDepartureTime);
            //imgDepartureGate = FindViewById<ImageView>(Resource.Id.imgDepartureGate);
            //imgArrivalTime = FindViewById<ImageView>(Resource.Id.imgArrivalTime);
            imgArrivalWeather = FindViewById<ImageView>(Resource.Id.imgArrivalWeather);
            imgRemainingDepartureTime = FindViewById<ImageView>(Resource.Id.imgRemainingDepartureTime);
            imgSummarySelectedCard = FindViewById<ImageView>(Resource.Id.imgSummarySelectedCard);
            imgSummarySelectedCardOther = FindViewById<ImageView>(Resource.Id.imgSummarySelectedCardOther);
            isInitialized = true;

            if (AIMSApp.IsDemoVersion)
            {
                lblRemainingDepartureTime.Visibility = ViewStates.Visible;
                //lblArrivalGate.Visibility = ViewStates.Visible;
                //tblDeparture.Visibility = ViewStates.Visible;
                //tblArrival.Visibility = ViewStates.Visible;
                servicesSeparatorBefore.Visibility = ViewStates.Visible;
                servicesSeparatorAfter.Visibility = ViewStates.Visible;
                pnlServices.Visibility = ViewStates.Visible;
                lstServices.Visibility = ViewStates.Visible;
            }
            else
            {
                lblRemainingDepartureTime.Visibility = ViewStates.Gone;
                //lblArrivalGate.Visibility = ViewStates.Gone;
                //tblDeparture.Visibility = ViewStates.Gone;
                //tblArrival.Visibility = ViewStates.Gone;
                servicesSeparatorBefore.Visibility = ViewStates.Gone;
                servicesSeparatorAfter.Visibility = ViewStates.Gone;
                pnlServices.Visibility = ViewStates.Gone;
                lstServices.Visibility = ViewStates.Gone;
            }
            #endregion


            btnBarcodeDetails.Click += (sender, e) =>
            {
                AIMSMessage(btnBarcodeDetails.Text, BarcodeString);
            };

            LoadUITexts(App.TextProvider.SelectedLanguage);


            RefreshPassenger();
            RefreshTimes();

            StartTimers();
        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            
            if (newConfig.Orientation == Android.Content.Res.Orientation.Portrait)
            {
                boardingPassContentHolder.Orientation = Orientation.Vertical;
                boardingPassContentHolder.WeightSum = 1;
            }
            else if (newConfig.Orientation == Android.Content.Res.Orientation.Landscape)
            {
                boardingPassContentHolder.Orientation = Orientation.Horizontal;
                boardingPassContentHolder.WeightSum = 2;
            }
        }
        #endregion
    }
}