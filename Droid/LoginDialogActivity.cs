using System;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.Graphics;
using Ieg.Mobile.Localization;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS | Developer Authentication", Icon = "@drawable/Icon", WindowSoftInputMode = Android.Views.SoftInput.AdjustResize, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, Theme = "@android:style/Theme.Dialog")]
    public class LoginDialogActivity : BaseActivity
    {
        #region Elements
        protected Button btnDevSignIn, btnDevCancelSignIn;
        protected EditText txtDevUName, txtDevPassword;
        protected TextView lblDevLoginTitle;
        #endregion

        #region Methods
        public void DoLogin()
        {
            if (string.IsNullOrWhiteSpace(this.txtDevUName.Text) || string.IsNullOrEmpty(this.txtDevPassword.Text))
                AIMSMessage(App.TextProvider.GetText(2047), App.TextProvider.GetText(1001));
            else
            {
                ShowLoading(App.TextProvider.GetText(2502));//TODO: use Membership Provider
                MembershipProvider.Current.ValidateDeveloper(this.txtDevUName.Text, this.txtDevPassword.Text, DefaultMobileApplication.Current.Position, (sender, result) => {
                    if(result.Item1)
                    {
                        App.IsDeveloperModeEnabled = true;
                        Finish();
                    }
                    else
                    {
                        string errorMessage = App.TextProvider.GetText((int)result.Item2);
                        LogsRepository.AddWarning("Developer login failed", 
                            string.Format("A user with username '{0}' failed to authenticate as developer with username '{1}',\nError Code: {2}\nError Message: {3}", 
                            MembershipProvider.Current.LoggedinUser.Username, txtDevUName.Text, result.Item2, errorMessage));

                        Analytics.TrackEvent("LoginDialogActivity", new Dictionary<string, string>
                        {
                            { "Developer login failed", string.Format("A user with username '{0}' failed to authenticate as developer with username '{1}',\nError Code: {2}\nError Message: {3}", 
                                MembershipProvider.Current.LoggedinUser.Username, txtDevUName.Text, result.Item2, errorMessage) }
                        });


                        AIMSMessage(App.TextProvider.GetText(2047), errorMessage);
                        //RunOnUiThread(() =>
                        //{
                        //    Toast.MakeText(this, errorMessage, ToastLength.Long).Show();
                        //});
                    }
                    HideLoading();
                });
            }
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            txtDevUName.Hint = App.TextProvider.GetText(2002);
            txtDevPassword.Hint = App.TextProvider.GetText(2003);
            btnDevSignIn.Text = App.TextProvider.GetText(2005);
            btnDevCancelSignIn.Text = App.TextProvider.GetText(2004);
        }

        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.RequestFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LoginDialog);
            Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));
            

            #region Initialize Components
            lblDevLoginTitle = FindViewById<TextView>(Resource.Id.lblDevLoginTitle);
            txtDevUName = FindViewById<EditText>(Resource.Id.txtDevUName);
            txtDevPassword = FindViewById<EditText>(Resource.Id.txtDevPassword);
            btnDevSignIn = FindViewById<Button>(Resource.Id.btnDevSignIn);
            btnDevCancelSignIn = FindViewById<Button>(Resource.Id.btnDevCancelSignIn);
            #endregion


#if DEBUG
            txtDevUName.Text = "dev";
            var date = string.Format("{0}{1}{2}dev", DateTime.UtcNow.Month,DateTime.UtcNow.Day, DateTime.UtcNow.Year);
            date = date.Replace("/", "").Replace("-", "").Replace("0", "").Replace(" ", "");//2016/07/15-> 715216
            txtDevPassword.Text = date;
#endif
            

            btnDevSignIn.Click += (sender, e) => {
                DoLogin();
            };
            txtDevPassword.EditorAction += (_, e) =>
            {
                if (e.ActionId == Android.Views.InputMethods.ImeAction.Go)
                {
                    DoLogin();
                }
            };

            btnDevCancelSignIn.Click += (sender, e) => {
                Finish();
            };
            
            //int width = WindowManager.DefaultDisplay.Width;
            //int widthdp = (int)(width / Resources.DisplayMetrics.Density);
            //int height = WindowManager.DefaultDisplay.Height;
            //int heightdp = (int)(height / Resources.DisplayMetrics.Density);
            //LongToast(string.Format("Dialog box width:{0}\nDialog box height:{1}\n---------------\nDialog box width in dp:{2}\nDialog box height in dp:{3}", width, height, widthdp, heightdp));
        }
        
    }
}