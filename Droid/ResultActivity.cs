﻿
using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using System.Timers;
using AIMS.Models;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity (Label = "AIMS", Icon = "@drawable/Icon"/*, MainLauncher = true*/, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class ResultActivity : BaseActivity
	{
        #region Constants
        const int successWaitTimeout = 800; //1000
        const int errorWaitTimeout = 4500;
        #endregion

        #region Elements
        ImageView imgResult;
		TextView lblResultTitle, lblResultComments;
	    Button btnResultReject, btnResultAccept;
        //Intent summaryIntent;
        #endregion

        #region Fields
        //ResultPack<Passenger> passengerCheckResult = null;
        bool isSucceeded = false;
	    bool isSecondScan = false;
        ErrorCode? errorCode = 0;
        string errorMetadata = string.Empty;
        Timer formTimer;
        #endregion

        #region Methods
        private void Error(string text, int? errorCode = null){
            RunOnUiThread(() => {
                this.imgResult.SetImageResource (Resource.Drawable.failed);
                this.imgResult.Visibility = ViewStates.Visible;
                Vibrator vibrator = (Vibrator) GetSystemService(Context.VibratorService);
			    vibrator.Vibrate(400);

                if (string.IsNullOrWhiteSpace(text))
                {
                    this.lblResultTitle.Text = App.TextProvider.GetText(1104);//"Validation failed!";
                    this.lblResultComments.Text = App.TextProvider.GetText(1051);//"Unknown error";
                }
                else
                {
                    if (errorCode.HasValue && errorCode.Value == (int)ErrorCode.ExternalValidationFailed)
                    {
                        this.lblResultTitle.Text = App.TextProvider.GetText(errorCode.Value);
                        this.lblResultComments.Text = text;
                    }
                    else
                    {
                        if (text.Contains("\n"))
                        {
                            this.lblResultTitle.Text = text.Substring(0, text.IndexOf("\n")).Trim();
                            this.lblResultComments.Text = text.Substring(text.IndexOf("\n") + 1).Trim();
                        }
                        else
                        {
                            this.lblResultTitle.Text = App.TextProvider.GetText(1104);//"Validation failed!";
                            this.lblResultComments.Text = text;
                        }
                    }
                }
                
                this.lblResultComments.Visibility = ViewStates.Visible;
                this.lblResultTitle.Visibility = ViewStates.Visible;
            });
        }

		private void Confirmed(string text){
            RunOnUiThread(new Action(()=>{
                this.imgResult.SetImageResource(Resource.Drawable.done);
                this.imgResult.Visibility = ViewStates.Visible;
                this.lblResultTitle.Text = string.Format("{0}!", App.TextProvider.GetText(2006)); //"Welcome!";
                this.lblResultTitle.Visibility = ViewStates.Visible;
                if (string.IsNullOrWhiteSpace(text))
                    this.lblResultComments.Text = ""; //this.lblResultComments.Text = "Your boarding pass is confirmed.";//TODO: need to create a translation for this
                else this.lblResultComments.Text = text;
                this.lblResultComments.Visibility = ViewStates.Visible;
            }));
		}

	    private void Question(string text, int? errorCode = null)
	    {
            RunOnUiThread(new Action(() => {
                this.imgResult.SetImageResource(Resource.Drawable.failed);
                this.imgResult.Visibility = ViewStates.Visible;
                Vibrator vibrator = (Vibrator)GetSystemService(Context.VibratorService);
                vibrator.Vibrate(400);

                if (string.IsNullOrWhiteSpace(text))
                {
                    this.lblResultTitle.Text = App.TextProvider.GetText(1104);//"Validation failed!";
                    this.lblResultComments.Text = App.TextProvider.GetText(1051);//"Unknown error";
                }
                else
                {
                    if (errorCode.HasValue && errorCode.Value == (int)ErrorCode.ExternalValidationFailed)
                    {
                        this.lblResultTitle.Text = App.TextProvider.GetText(errorCode.Value);
                        this.lblResultComments.Text = text;
                    }
                    else
                    {
                        if (text.Contains("\n"))
                        {
                            this.lblResultTitle.Text = text.Substring(0, text.IndexOf("\n")).Trim();
                            this.lblResultComments.Text = text.Substring(text.IndexOf("\n") + 1).Trim();
                        }
                        else
                        {
                            this.lblResultTitle.Text = App.TextProvider.GetText(1104);//"Validation failed!";
                            this.lblResultComments.Text = text;
                        }
                    }
                }

                this.lblResultComments.Visibility = ViewStates.Visible;
                this.lblResultTitle.Visibility = ViewStates.Visible;
                this.btnResultAccept.Visibility = ViewStates.Visible;
                this.btnResultReject.Visibility = ViewStates.Visible;

            }));
        }
		
        private void HandleResult()
        {
            formTimer.Stop();
            //if (App.ManualInputForm != null)
            //{
            //    try
            //    {
            //        App.ManualInputForm.Close();
            //        //App.ManualInputForm.Dispose();
            //    }
            //    catch
            //    {

            //    }
            //    App.ManualInputForm = null;
            //}

            //App.ContinueScanning = true;//TODO: uncomment if you want to continue scanning

            if (isSucceeded)// passengerCheckResult.IsSucceeded)
            {
                //StartActivity(summaryIntent);
                StartActivity(App.SummaryIntent);//TODO: New boarding pass design
                //StartActivity(App.BoardingPassIntent);//TODO: loading the same intent (test it)

                App.ContinueScanning = true;//TODO: uncomment if you want to continue scanning after success scan
                Finish();
            }
            else
            {
                if (errorCode.HasValue)
                {
                    switch (errorCode.Value)
                    {
                        case ErrorCode.TaskAlreadyPerformedException:
                            Finish();
                            break;
                        //case ErrorCode.PassengerNameMismatch:
                        //    break;
                        case ErrorCode.NeedToSwipeBoardingPass://This is an incomplete passenger!!!
                            //var manualInputIntent = new Intent(App.MainForm, typeof(ManualInputActivity));//

                            var manualInputIntent = new Intent(App.MainForm, typeof(ManualEntryActivity));

                            //if(manualInputIntent.HasExtra("errorCode")) manualInputIntent.RemoveExtra("errorCode");
                            manualInputIntent.PutExtra("errorCode", ((int)errorCode.Value).ToString());

                            //if (manualInputIntent.HasExtra("missingValues")) manualInputIntent.RemoveExtra("missingValues");
                            manualInputIntent.PutExtra("missingValues", errorMetadata);

                            App.MainForm.StartActivity(manualInputIntent);
                            //app.ContinueScanning = true;//TODO: uncomment if you want to continue scanning after incomplete passenger
                            Finish();
                            break;
                        default:
                            Finish();
                            break;
                    }
                }
                else
                    Finish();
                //else app.ContinueScanning = true;//TODO: uncomment if you want to continue scanning after failed scan but complete passenger
            }
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
        }
        #endregion

        #region Events
        private void Image_OnClick(object sender, EventArgs e)
        {
            HandleResult();
        }

        private void PassengerResultTimedOut(object sender, ElapsedEventArgs e)
        {
            HandleResult();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            try
            {

                ShowLoading(App.TextProvider.GetText(2507)/*"Checking passenger information with server"*/);
                SetContentView(Resource.Layout.Result);

                this.lblResultTitle = FindViewById<TextView>(Resource.Id.lblResultTitle);
                this.lblResultTitle.Visibility = ViewStates.Gone;
                this.lblResultComments = FindViewById<TextView>(Resource.Id.lblResultComments);
                this.lblResultComments.Visibility = ViewStates.Gone;
                this.imgResult = FindViewById<ImageView>(Resource.Id.imgResult);
                this.imgResult.Visibility = ViewStates.Gone;
                this.imgResult.Click += Image_OnClick;

                this.btnResultAccept = FindViewById<Button>(Resource.Id.btnResultAccept);
                this.btnResultAccept.Visibility = ViewStates.Gone;
                this.btnResultAccept.Click += Accept_OnClick;
                this.btnResultReject = FindViewById<Button>(Resource.Id.btnResultReject);
                this.btnResultReject.Visibility = ViewStates.Gone;
                this.btnResultReject.Click += Reject_OnClick;

                App.ResultForm = this;
                //summaryIntent = new Intent(this, typeof(SummaryActivity));

                string barcode = Intent.GetStringExtra("barcode") ?? "";

                isSecondScan = false;
                if (Intent.HasExtra("isSecondScan"))
                {
                    string secondScanStr = Intent.GetStringExtra("isSecondScan") ?? "";
                    try
                    {
                        isSecondScan = bool.Parse(secondScanStr);
                    }
                    catch (System.Exception eex)
                    {
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                    }
                }

                //ErrorCode? parentErrorCode = null;
                //if (Intent.HasExtra("errorCode"))
                //{
                //    var eCode = Intent.GetStringExtra("errorCode");
                //    try
                //    {
                //        parentErrorCode = (ErrorCode?)int.Parse(eCode);
                //    }
                //    catch
                //    {
                //    }
                //}

                var pr = new PassengersRepository();
                pr.OnPassengerChecked += OnPassengerChecked;

                formTimer = new Timer
                {
                    Enabled = false,
                    Interval = successWaitTimeout
                };
                formTimer.Elapsed += PassengerResultTimedOut;

                if (isSecondScan)
                {
                    if (App.IsGuestMode)
                        pr.GetGuestInfo(MembershipProvider.Current.SelectedPassenger, barcode,
                                       (PassengerServiceType)MembershipProvider.Current.SelectedPassengerService.Id,
                                       MembershipProvider.Current.SelectedLounge);
                    else
                        pr.ValidatePassengerWithCard(MembershipProvider.Current.SelectedLounge, barcode);
                }
                else if (!string.IsNullOrWhiteSpace(barcode))
                {
                    if (App.IsGuestMode)
                        pr.GetGuestInfo(MembershipProvider.Current.SelectedPassenger, barcode,
                                       (PassengerServiceType)MembershipProvider.Current.SelectedPassengerService.Id,
                                       MembershipProvider.Current.SelectedLounge);
                    else
                        pr.GetPassengerInfo(barcode, MembershipProvider.Current.SelectedLounge);
                }
                else
                {
                    if (App.IsGuestMode)
                        pr.ValidateGuestInfo(MembershipProvider.Current.SelectedPassenger,
                                             MembershipProvider.Current.SelectedGuest, (PassengerServiceType)MembershipProvider.Current.SelectedPassengerService.Id,
                                             MembershipProvider.Current.SelectedLounge);
                    else
                        pr.ValidatePassengerInfo(MembershipProvider.Current.SelectedLounge);
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void Reject_OnClick(object sender, EventArgs e)
        {
            //TODO: rejected
            using (var pr = new PassengersRepository())
            {
                PassengersRepository.AddFailedPassenger(MembershipProvider.Current.SelectedPassenger, "Passenger name was mismatch and rejected by agent");//TODO: ugly
                pr.AgentRejectPassenger(MembershipProvider.Current.SelectedLounge);
                
                try
                {
                    if (App.ManualInputForm != null)
                        App.ManualInputForm.Close();//this is a customised close method I implemented
                }
                catch (System.Exception eex)
                {
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                }
            }
            Finish();
        }

        private void Accept_OnClick(object sender, EventArgs e)
        {
            if (App.ManualInputForm != null)
            {
                try
                {
                    App.ManualInputForm.Close();
                    //App.ManualInputForm.Dispose();
                }
                catch (System.Exception eex)
                {
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                }
                App.ManualInputForm = null;
            }

            var manualInput = new Intent(App.MainForm, typeof(ManualInputActivity));

            //if (manualInput.HasExtra("errorCode")) manualInput.RemoveExtra("errorCode");
            manualInput.PutExtra("errorCode", ((int)errorCode.Value).ToString());

            //if (manualInput.HasExtra("missingValues")) manualInput.RemoveExtra("missingValues");
            manualInput.PutExtra("missingValues", errorMetadata);

            App.MainForm.StartActivity(manualInput);
            //app.ContinueScanning = true;//TODO: uncomment if you want to continue scanning after incomplete passenger
            Finish();
        }

        private void OnPassengerChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            if (isSucceeded)
            {
                if (App.IsGuestMode)
                    MembershipProvider.Current.SelectedGuest = passenger;
                else
                    MembershipProvider.Current.SelectedPassenger = passenger;
            }

            //success = passengerCheckResult.IsSucceeded;
            //this.passengerCheckResult = passengerCheckResult;
            this.isSucceeded = isSucceeded;
            this.errorCode = (ErrorCode?)errorCode;
            this.errorMetadata = errorMetadata;
#if DEBUG//TODO: Remove the whole (DEBUG) section below
            //if (passenger.CardID == 0)
            //    passenger.CardID = 1303;
            //if (passenger.AirlineId == 0)
            //    passenger.AirlineId = 3;
#endif

            //MembershipProvider.Current.SelectedCard = CardDBAdapter.Current.GetCard(MembershipProvider.Current.SelectedPassenger.CardID);
            //MembershipProvider.Current.SelectedAirline = AirlineDBAdapter.Current.GetAirline(MembershipProvider.Current.SelectedPassenger.AirlineId);
            //SummaryActivity.Passenger = passengerCheckResult.ReturnParam;//the manual input form will rely on this passenger variable

            if (this.isSucceeded)
            {
                if (this.errorCode == ErrorCode.PassengerNameMismatch)
                {
                    //Assembling the customized error message for NameMismatch case in UA validation!
                    Question(string.Format("{0}{1}",
                        (this.errorCode.HasValue ?
                            string.Format("{0}\n", LanguageRepository.Current.GetText((int)this.errorCode)) : ""),
                            message),
                        errorCode);
                    HideLoading();
                    return;
                }
                formTimer.Interval = successWaitTimeout;
                Confirmed(message);
                //formTimer.Start();
            }
            else
            {
                if (this.errorCode == ErrorCode.TaskAlreadyPerformedException)
                {
                    HideLoading();
                    HandleResult();
                    return;
                }
                else
                {
                    formTimer.Interval = errorWaitTimeout;
                    PassengersRepository.AddFailedPassenger(passenger, message);
                    Error(message, errorCode);
                    //formTimer.Start();
                }
            }

            HideLoading();
            formTimer.Start();
        }

        public override void OnBackPressed()
        {
            formTimer.Stop();
            HandleResult();

//            base.OnBackPressed();
        }
        #endregion
    }
}

