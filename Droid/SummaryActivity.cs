﻿using System;

using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using System.Timers;
using System.Globalization;
using Android.Graphics;
using Ieg.Mobile.Localization;
using AIMS.Droid.Library;
using System.Collections.Generic;
using AIMS.Models;
using Android.Content;
using Microsoft.AppCenter.Analytics;

namespace AIMS.Droid
{
    [Activity (Label = "AIMS", Icon = "@drawable/Icon", Theme = "@android:style/Theme.NoTitleBar.OverlayActionModes" /*, MainLauncher = true*/, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class _SummaryActivity : BaseActivity
    {
        #region Constants
        const int refreshInterval = 4000;
        #endregion

        #region Elements
        ScrollView rootScrollView;
        TableLayout tblDeparture, tblArrival;
        RelativeLayout pnlServices;
        View barcodeSeparatorBefore, servicesSeparatorBefore, servicesSeparatorAfter;

        ListView lstServices;
        ImageView imgDepartureTime, imgDepartureGate, imgArrivalTime, imgArrivalWeather, imgSummarySelectedCard, imgSummarySelectedCardOther;
        private CenterLockHorizontalScrollview horizontalScrollViewServices;
        private LinearLayout itemlayoutServices;
        TextView lblSummaryTitle, lblPassengerName, lblFFN, lblClass, lblFlightNumber, lblDepartureAirport, lblAirlineLabel, lblDepartureAirportLabel,
            lblCardLabel, lblFFNLabel, lblClassLabel, lblArrivalAirportLabel, lblArrivalGate, lblSummaryAirlineName, lblRemainingDepartureTime, lblDepartureTime, lblDepartureComment, lblPriceSum,
            lblDepartureGate, lblDepartureGateComment, lblArrivalTime, lblArrivalTimeComment, lblFlightNumberLabel, lblServicesLabel,
            lblArrivalWeatherTemperature, lblArrivalWeatherComment, lblArrivalWeatherLocation, lblArrivalAirport;
        Button btnBarcodeDetails;
        #endregion

        #region Fields
        Timer refresherTimer;
        bool isInitialized = false;
        DateTime? departureTime, arrivalTime;

        string delayedText, onTimeText;
        #endregion

        #region Properties
        //public static Passenger Passenger { get; set; }//Clean it after closing this activity (Bad practice)
        private string BarcodeString { get; set; }
        #endregion

        #region Methods
        private void GetListViewSize(ListView myListView) {
			IListAdapter myListAdapter = myListView.Adapter;
			if (myListAdapter == null) {
				return;
			}
			//set listAdapter in loop for getting final size
			try{
				int totalHeight = 0;
				for (int size = 0; size < myListAdapter.Count; size++) {
					//View listItem = myListAdapter.GetView(size, null, myListView);
					RelativeLayout listItem = (RelativeLayout)myListAdapter.GetView(size, null, myListView);
					listItem.Measure(0, 0);
					totalHeight += listItem.MeasuredHeight;
				}
				//setting listview item in adapter
				ViewGroup.LayoutParams parameters = myListView.LayoutParameters;
				parameters.Height = totalHeight + (myListView.DividerHeight * (myListAdapter.Count - 1));
				myListView.LayoutParameters = parameters;
				rootScrollView.Fling (0);
			}
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }
        }

        private void RefreshPassenger()
        {
            //TODO: do not use text provider here (session will be kept alive!!)

            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            PassengersRepository passengersRepository = new PassengersRepository();

            if (MembershipProvider.Current.SelectedGuest != null)
            {
                SetContents();
            }
            else if (!MembershipProvider.Current.SelectedPassenger.IsGuest)//TODO: Avoid loading the host when validation happened (only when selected from list)
            {
                passengersRepository.OnPassengerChecked += OnPassengerChecked;
                passengersRepository.RetrieveMainPassenger(MembershipProvider.Current.SelectedPassenger, MembershipProvider.Current.SelectedLounge);
            }
            else 
            {
                passengersRepository.OnPassengerChecked += OnPassengerChecked;
                passengersRepository.RetrieveGuestPassenger(MembershipProvider.Current.SelectedPassenger, MembershipProvider.Current.SelectedLounge);
            }

        }

        private void OnPassengerChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            MembershipProvider.Current.SelectedPassenger = passenger;
            SetContents();
        }

        private void SetContents()
        {
            RunOnUiThread(new Action(() =>
            {
                imgSummarySelectedCard.SetImageBitmap(null);
                lblPassengerName.Text = string.Empty;
                lblFFN.Text = string.Empty;
                lblClass.Text = string.Empty;
                lblFlightNumber.Text = string.Empty;
                lblSummaryAirlineName.Text = string.Empty;
                lblDepartureAirport.Text = string.Empty;
                lblArrivalAirport.Text = string.Empty;

                if (!App.IsGuestMode)
                {
                    if (MembershipProvider.Current.SelectedCard != null)
                    {
                        if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                        {
                            var bm = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedCard.CardPictureBytes, 0, MembershipProvider.Current.SelectedCard.CardPictureBytes.Length);
                            if (bm != null)
                            {
                                imgSummarySelectedCard.SetImageBitmap(bm);
                                //bm.Recycle();
                                //bm = null;
                            }
                        }
                        else
                        {
                            //(new JavaCard(MembershipProvider.Current.SelectedCard)).LoadImage(imgSummarySelectedCard, this, App.ImageRepository);
                            var item = (new JavaCard(MembershipProvider.Current.SelectedCard));
                            /*ImageRepository.Current*/
                            App.ImageRepository.LoadImage(item.ImageHandle, (imageResult) =>
                            {
                                if (imageResult == null)
                                {
                                    LogsRepository.AddError("Error in SummaryActivity in loading card image", "result is null");

                                    Analytics.TrackEvent("ValidationResultActivity", new Dictionary<string, string>
                                        {
                                            { "Error in SummaryActivity in loading card image", "result is null" }
                                        });

                                    return;
                                }

                                if (imageResult.IsSucceeded)
                                {
                                    var bytes = imageResult.ReturnParam;

                                    //item.SaveImage(bytes);//Removed IImageItem and it is automatically saved in the ImageRepository

                                    if (bytes == null || bytes.Length == 0) return;

                                    try
                                    {
                                        var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                                        if (bm != null)
                                        {
                                            this.RunOnUiThread(() =>
                                            {
                                                imgSummarySelectedCard.SetImageBitmap(bm);
                                            });
                                            //bm.Recycle();
                                            //bm = null;
                                        }
                                    }
                                    catch (System.Exception eex)
                                    {
                                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                                    }
                                }
                            });
                        }
                    }

                    imgSummarySelectedCardOther.SetImageBitmap(null);
                    if (MembershipProvider.Current.SelectedPassenger.OtherCardID != 0)
                    {
                        var otherCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedPassenger.OtherCardID);
                        if (otherCard != null)
                        {
                            if (otherCard.CardPictureBytes != null && otherCard.CardPictureBytes.Length > 0)
                            {
                                var bm = BitmapFactory.DecodeByteArray(otherCard.CardPictureBytes, 0, otherCard.CardPictureBytes.Length);
                                if (bm != null)
                                {
                                    imgSummarySelectedCardOther.SetImageBitmap(bm);
                                    //bm.Recycle();
                                    //bm = null;
                                }
                            }
                            else
                            {
                                //(new JavaCard(otherCard)).LoadImage(imgSummarySelectedCardOther, this, App.ImageRepository);
                                var item = (new JavaCard(otherCard));
                                /*ImageRepository.Current*/
                                App.ImageRepository.LoadImage(item.ImageHandle, (imageResult) =>
                                {
                                    if (imageResult == null)
                                    {
                                        LogsRepository.AddError("Error in SummaryActivity in loading card image", "result is null");

                                        Analytics.TrackEvent("ValidationResultActivity", new Dictionary<string, string>
                                        {
                                            { "Error in SummaryActivity in loading card image", "result is null" }
                                        });

                                        return;
                                    }

                                    if (imageResult.IsSucceeded)
                                    {
                                        var bytes = imageResult.ReturnParam;

                                        //item.SaveImage(bytes);//Removed IImageItem and it is automatically saved in the ImageRepository

                                        if (bytes == null || bytes.Length == 0) return;

                                        try
                                        {
                                            var bm = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                                            if (bm != null)
                                            {
                                                this.RunOnUiThread(() =>
                                                {
                                                    imgSummarySelectedCardOther.SetImageBitmap(bm);
                                                });
                                                //bm.Recycle();
                                                //bm = null;
                                            }
                                        }
                                        catch (System.Exception eex)
                                        {
                                            Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                                        }
                                    }
                                });
                            }
                        }
                        else imgSummarySelectedCardOther.Visibility = ViewStates.Gone;
                    }
                    else imgSummarySelectedCardOther.Visibility = ViewStates.Gone;


                    lblPassengerName.Text = MembershipProvider.Current.SelectedPassenger.FullName;
                    lblFFN.Text = MembershipProvider.Current.SelectedPassenger.FFN;
                    lblClass.Text = MembershipProvider.Current.SelectedPassenger.TrackingClassOfService;
                    lblFlightNumber.Text = string.Format("{0} {1} {2}",
                                           MembershipProvider.Current.SelectedPassenger.FlightCarrier,
                                           string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.FlightNumber) ? string.Empty : MembershipProvider.Current.SelectedPassenger.FlightNumber.TrimStart('0'),
                                           MembershipProvider.Current.SelectedPassenger.TrackingClassOfService);


                    lblSummaryAirlineName.Text = MembershipProvider.Current.SelectedAirline != null ? MembershipProvider.Current.SelectedAirline.Name : string.Empty;
                    lblDepartureAirport.Text = MembershipProvider.Current.SelectedPassenger.FromAirport;
                    lblArrivalAirport.Text = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedPassenger.ToAirport) ? "YYT" : MembershipProvider.Current.SelectedPassenger.ToAirport;// MembershipProvider.Current.SelectedPassenger.ToAirport;//TODO: Get ToAirport
                                                                                                                                                                                                //lblDepartureGate.Text = "N/A";//TODO: get the gate number
                                                                                                                                                                                                //lblDepartureGateComment.Text = "";//TODO: get the gate comments

                    if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.BarcodeString))
                        BarcodeString = MembershipProvider.Current.SelectedPassenger.BarcodeString;
                    else btnBarcodeDetails.Visibility = ViewStates.Gone;

                    horizontalScrollViewServices.SetAdapter(this, new ServicesAdapter(this));
                }
                else if (MembershipProvider.Current.SelectedGuest != null)
                {
                    imgSummarySelectedCardOther.SetImageBitmap(null);

                    lblPassengerName.Text = MembershipProvider.Current.SelectedGuest.FullName;
                    lblFFN.Text = MembershipProvider.Current.SelectedGuest.FFN;
                    lblClass.Text = MembershipProvider.Current.SelectedGuest.TrackingClassOfService;
                    lblFlightNumber.Text = string.Format("{0} {1} {2}",
                                           MembershipProvider.Current.SelectedGuest.FlightCarrier,
                                           string.IsNullOrEmpty(MembershipProvider.Current.SelectedGuest.FlightNumber) ? string.Empty : MembershipProvider.Current.SelectedGuest.FlightNumber.TrimStart('0'),
                                           MembershipProvider.Current.SelectedGuest.TrackingClassOfService);


                    lblSummaryAirlineName.Text = MembershipProvider.Current.SelectedAirline != null ? MembershipProvider.Current.SelectedAirline.Name : string.Empty;
                    lblDepartureAirport.Text = MembershipProvider.Current.SelectedGuest.FromAirport;
                    lblArrivalAirport.Text = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedGuest.ToAirport) ? "YYT" : MembershipProvider.Current.SelectedGuest.ToAirport;// MembershipProvider.Current.SelectedPassenger.ToAirport;//TODO: Get ToAirport
                                                                                                                                                                                                //lblDepartureGate.Text = "N/A";//TODO: get the gate number
                                                                                                                                                                                                //lblDepartureGateComment.Text = "";//TODO: get the gate comments

                    if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedGuest.BarcodeString))
                        BarcodeString = MembershipProvider.Current.SelectedGuest.BarcodeString;
                    else btnBarcodeDetails.Visibility = ViewStates.Gone;


                }

                horizontalScrollViewServices.Visibility = MembershipProvider.Current.SelectedPassenger.IsGuest ? ViewStates.Gone : ViewStates.Visible;

            }));
        }

        private void RefreshTimes()
        {
            //TODO: do not use text provider here (session will be kept alive!!)

            if (MembershipProvider.Current != null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            RunOnUiThread(new Action(() => {
                //TODO: Compute the diff between server time and client time

                //TODO: uncomment these below
                var currentTime = DateTime.Now;//TODO: it must be the server current given time
                //var departureTime = MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate;
                //var departureTime = MembershipProvider.Current.SelectedPassenger.DepartureTime;
                if (!departureTime.HasValue)
                {
                    departureTime = currentTime.AddMinutes(45).AddSeconds(5);

                    //var arrivalTime = MembershipProvider.Current.SelectedPassenger.ArrivalTime;
                    arrivalTime = departureTime.Value.AddHours(16).AddMinutes(35);
                }


                var remainingTime = departureTime.Value.Subtract(currentTime);

                lblRemainingDepartureTime.SetTextColor(new Android.Graphics.Color(0, 233, 0));//green
                imgDepartureTime.SetImageResource(Resource.Drawable.departure_time_medium);
                imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate);
                if (remainingTime.Hours > 0)
                {
                    lblRemainingDepartureTime.Text = string.Format("[{0}h {1}m remaining]", remainingTime.Hours, remainingTime.Minutes);
                }
                else if (remainingTime.Minutes > 0)
                {
                    lblRemainingDepartureTime.Text = string.Format("[{0}m remaining]", remainingTime.Minutes);
                    if (remainingTime.Minutes < 45)
                    {
                        imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate_green);//TODO: get gate status
                        lblRemainingDepartureTime.SetTextColor(new Android.Graphics.Color(255, 180, 43));//orange
                        imgDepartureTime.SetImageResource(Resource.Drawable.departure_time_medium_green);
                    }
                    else if (remainingTime.Minutes <= 15)
                    {
                        imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate_green);//TODO: get gate status
                        lblRemainingDepartureTime.SetTextColor(new Android.Graphics.Color(255, 43, 43));//red
                        imgDepartureTime.SetImageResource(Resource.Drawable.departure_time_medium_red);
                    }
                }
                else
                {
                    imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate_red);//TODO: get gate status
                    lblRemainingDepartureTime.Text = "";
                }

                //lblDepartureTime.Text = string.Format("{0:h:m tt}", Passenger.DepartureTime);
                //lblDepartureTime.Text = MembershipProvider.Current.SelectedPassenger.DepartureTime.ToString("hh:mm:ss tt", CultureInfo.InvariantCulture);
                lblDepartureTime.Text = departureTime.Value.ToString("MMMM dd, yyyy\nhh:mm tt", CultureInfo.InvariantCulture);
                lblDepartureComment.Text = onTimeText;//TODO: get delay or ontime status (On time)
                lblDepartureComment.SetTextColor(new Android.Graphics.Color(0, 233, 0));//green
                lblArrivalTime.Text = arrivalTime.Value.ToString("MMMM dd, yyyy\nhh:mm tt", CultureInfo.InvariantCulture);
                lblArrivalTimeComment.Text = delayedText;//TODO: get delay or ontime status (Delayed)
                lblArrivalTimeComment.SetTextColor(new Android.Graphics.Color(255, 180, 43));//orange
                //lblArrivalGate.Text = "";//TODO: get the passenger arrival gate

                //imgDepartureGate.SetImageResource(Resource.Drawable.departure_gate_***);//TODO: get gate status
                imgArrivalTime.SetImageResource(Resource.Drawable.arrival_time_medium);//TODO: Ask if it must be changed

            }));
        }

        private void RefreshWeather()
        {
            //TODO: do not use text provider here (session will be kept alive!!)
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            RunOnUiThread(new Action(() => {
                //TODO: update all the weather lables and images on the screen..

                //lblArrivalWeatherTemperature
                //lblArrivalWeatherComment
                //lblArrivalWeatherLocation

                //imgArrivalWeather
            }));
        }

        private void DisposeTimers()//TODO: dispose all the timers here
        {
            //TODO: Check it 
            //if(MembershipProvider.Current != null)
            //    MembershipProvider.Current.SelectedPassenger = null;//passenger is cleared


            if (refresherTimer == null) return;
            refresherTimer.Enabled = false;
            refresherTimer.Dispose();
            refresherTimer = null;
        }

        //~SummaryActivity()
        //{
        //    DisposeTimers();
        //}

        public new void Dispose()
        {
            DisposeTimers();
            base.Dispose();
        }

        public void StartTimers()//TODO: create all the timers here
        {
            if (refresherTimer != null) return;
            refresherTimer = new Timer(refreshInterval);
            refresherTimer.Elapsed += OnRefreshEvent;
            refresherTimer.Enabled = true;
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            onTimeText = App.TextProvider.GetText(2021);
            delayedText = App.TextProvider.GetText(2020);
            lblSummaryTitle.Text = App.TextProvider.GetText(2006);//Welcome
            lblCardLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2013));//Card: 
            lblClassLabel.Text = App.TextProvider.GetText(2014);//Class
            lblFlightNumberLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2017));//Flight Info: 
            lblDepartureAirportLabel.Text = App.TextProvider.GetText(2018);// string.Format("{0}: ",App.TextProvider.GetText(2018));//Departure:  
            lblArrivalAirportLabel.Text = App.TextProvider.GetText(2019);// string.Format("{0}: ", App.TextProvider.GetText(2019));//Arrival: 
            lblFFNLabel.Text = App.TextProvider.GetText(2027);//FFN
            //lblServicesLabel.Text = App.TextProvider.GetText(2043);//"Services";
            btnBarcodeDetails.Text = App.TextProvider.GetText(2045);//"Details"
            lblAirlineLabel.Text = string.Format("{0}: ", App.TextProvider.GetText(2023));//"Airline: "
        }
        #endregion

        #region Events

        public override void OnActionModeFinished(ActionMode mode)
        {
            DisposeTimers();
            base.OnActionModeFinished(mode);
        }

        protected override void OnPause()
        {
            DisposeTimers();
            base.OnPause();
        }

        protected override void OnStop()
        {
            DisposeTimers();
            base.OnStop();
        }

        protected override void OnStart()
        {
            StartTimers();
            base.OnStart();
        }

        protected override void OnResume()
        {
            StartTimers();
            base.OnResume();
        }

        public override void OnActionModeStarted(ActionMode mode)
        {
            StartTimers();
            base.OnActionModeStarted(mode);
        }

        private void OnRefreshEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            RefreshTimes();
        }

        protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.Summary);
            //App.PassengerSummaryForm = this;

            Title = MembershipProvider.Current.UserHeaderText;

            #region Initializing and getting controls
            tblDeparture = FindViewById<TableLayout>(Resource.Id.tblDeparture);
            tblArrival = FindViewById<TableLayout>(Resource.Id.tblArrival);
            pnlServices = FindViewById<RelativeLayout>(Resource.Id.pnlServices);

            btnBarcodeDetails = FindViewById<Button>(Resource.Id.btnBarcodeDetails);

            servicesSeparatorBefore = FindViewById<View>(Resource.Id.servicesSeparatorBefore);
            servicesSeparatorAfter = FindViewById<View>(Resource.Id.servicesSeparatorAfter);

            rootScrollView = FindViewById<ScrollView>(Resource.Id.rootScrollView);
            lstServices = FindViewById<ListView>(Resource.Id.lstServices);

            lblSummaryTitle = FindViewById<TextView>(Resource.Id.lblSummaryTitle);
            lblPassengerName = FindViewById<TextView>(Resource.Id.lblPassengerName);
            lblAirlineLabel = FindViewById<TextView>(Resource.Id.lblAirlineLabel);
            lblSummaryAirlineName = FindViewById<TextView>(Resource.Id.lblSummaryAirlineName);
            lblCardLabel = FindViewById<TextView>(Resource.Id.lblCardLabel);
            lblFFNLabel = FindViewById<TextView>(Resource.Id.lblFFNLabel);
            lblClassLabel = FindViewById<TextView>(Resource.Id.lblClassLabel);
            lblFlightNumberLabel = FindViewById<TextView>(Resource.Id.lblFlightNumberLabel);
            lblDepartureAirportLabel = FindViewById<TextView>(Resource.Id.lblDepartureAirportLabel);
            lblArrivalAirportLabel = FindViewById<TextView>(Resource.Id.lblArrivalAirportLabel);
            lblArrivalGate = FindViewById<TextView>(Resource.Id.lblArrivalGate);
            lblServicesLabel = FindViewById<TextView>(Resource.Id.lblServicesLabel);
            //lblTierStatus = FindViewById<TextView>(Resource.Id.lblTierStatus);
            lblFFN = FindViewById<TextView>(Resource.Id.lblFFN);
            lblClass = FindViewById<TextView>(Resource.Id.lblClass);
            lblFlightNumber = FindViewById<TextView>(Resource.Id.lblFlightNumber);
            lblRemainingDepartureTime = FindViewById<TextView>(Resource.Id.lblRemainingDepartureTime);
            lblDepartureAirport = FindViewById<TextView>(Resource.Id.lblDepartureAirport);
            lblDepartureTime = FindViewById<TextView>(Resource.Id.lblDepartureTime);
            lblDepartureComment = FindViewById<TextView>(Resource.Id.lblDepartureComment);
            lblPriceSum = FindViewById<TextView>(Resource.Id.lblPriceSum);
            lblDepartureGate = FindViewById<TextView>(Resource.Id.lblDepartureGate);
            lblDepartureGateComment = FindViewById<TextView>(Resource.Id.lblDepartureGateComment);
            lblArrivalAirport = FindViewById<TextView>(Resource.Id.lblArrivalAirport);
            lblArrivalTime = FindViewById<TextView>(Resource.Id.lblArrivalTime);
            lblArrivalTimeComment = FindViewById<TextView>(Resource.Id.lblArrivalTimeComment);
            lblArrivalWeatherTemperature = FindViewById<TextView>(Resource.Id.lblArrivalWeatherTemperature);
            lblArrivalWeatherComment = FindViewById<TextView>(Resource.Id.lblArrivalWeatherComment);
            lblArrivalWeatherLocation = FindViewById<TextView>(Resource.Id.lblArrivalWeatherLocation);

            imgDepartureTime = FindViewById<ImageView>(Resource.Id.imgDepartureTime);
            imgDepartureGate = FindViewById<ImageView>(Resource.Id.imgDepartureGate);
            imgArrivalTime = FindViewById<ImageView>(Resource.Id.imgArrivalTime);
            imgArrivalWeather = FindViewById<ImageView>(Resource.Id.imgArrivalWeather);
            imgSummarySelectedCard = FindViewById<ImageView>(Resource.Id.imgSummarySelectedCard);
            imgSummarySelectedCardOther = FindViewById<ImageView>(Resource.Id.imgSummarySelectedCardOther);

            horizontalScrollViewServices = FindViewById<CenterLockHorizontalScrollview>(Resource.Id.horizontalScrollView1);
            itemlayoutServices = FindViewById<LinearLayout>(Resource.Id.itemServices);
            horizontalScrollViewServices.OnItemSelected += OnPassengerSelected;


            isInitialized = true;

            if(AIMSApp.IsDemoVersion)
            {
                lblRemainingDepartureTime.Visibility = ViewStates.Visible;
                lblArrivalGate.Visibility = ViewStates.Visible;
                tblDeparture.Visibility = ViewStates.Visible;
                tblArrival.Visibility = ViewStates.Visible;
                servicesSeparatorBefore.Visibility = ViewStates.Visible;
                servicesSeparatorAfter.Visibility = ViewStates.Visible;
                pnlServices.Visibility = ViewStates.Visible;
                lstServices.Visibility = ViewStates.Visible;
            }
            else
            {
                lblRemainingDepartureTime.Visibility = ViewStates.Gone;
                lblArrivalGate.Visibility = ViewStates.Gone;
                tblDeparture.Visibility = ViewStates.Gone;
                tblArrival.Visibility = ViewStates.Gone;
                servicesSeparatorBefore.Visibility = ViewStates.Gone;
                servicesSeparatorAfter.Visibility = ViewStates.Gone;
                pnlServices.Visibility = ViewStates.Gone;
                lstServices.Visibility = ViewStates.Gone;
            }
            #endregion

            btnBarcodeDetails.Click += (sender, e) =>
            {
                AIMSMessage(btnBarcodeDetails.Text, BarcodeString);
            };

            LoadUITexts(App.TextProvider.SelectedLanguage);

            StartTimers();
            
            RefreshPassenger();
            RefreshTimes();
        }

        private void OnPassengerSelected(object sender, PassengerService e)
        {
            MembershipProvider.Current.SelectedPassengerService = e;

            App.GuestsIntent = new Intent(this, typeof(GuestsActivity));
            App.GuestsIntent.SetFlags(ActivityFlags.NewTask);
            App.MainForm.StartActivity(App.GuestsIntent);
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();

            App.IsGuestMode = false;
        }

        #endregion
    }
}