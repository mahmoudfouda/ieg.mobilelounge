﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AIMS.Droid.Adapters;
using AIMS.Models;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS Passenger Info", Icon = "@drawable/Icon", Theme = "@style/Theme.PageIndicatorDefaults", WindowSoftInputMode = SoftInput.AdjustPan)]
    [IntentFilter(new[] { Intent.ActionMain }, Categories = new[] { "com.iegamerica.aims" })]
    public class ManualEntryActivity : BaseActivity
    {
        private PassengerValidation ValidationResult;
        private ImageView imgAirline;
        private ImageView imgMISelectedCard;
        private List<Airport> items = new List<Airport>();
        private AirportListAdapter adapterFrom;
        private AirportListAdapter adapterTo;
        private AutoCompleteTextView txtAirportFrom;
        private AutoCompleteTextView txtAirportTo;
        private EditText txtFullName;
        private EditText txtFFN;
        private EditText txtCarrier;
        private EditText txtFlightNumber;
        private EditText txtClassOfTravel;
        private EditText txtSeat;
        private EditText txtNotes;
        private EditText txtPNR;
        private LinearLayout selectedAirlineViewSPMI2;
        private LinearLayout layoutImage;
        private LinearLayout layoutSelectedGuestType;
        private TextView lblCardNameSPMI2;
        private TextView lblAgentAirlineSPMI2;
        private TextView lblAgentNameSPMI2;
        private ImageView imgAgentAirlineSPMI2;
        private TextView lblFromFullSPMI2;
        private TextView lblToFullSPMI2;
        private TextView lblSelectedGuestType;
        private TableLayout pagerButtonsPanel;
        private ImageButton btnConfirm;
        private ImageButton btnCancel;
        private ImageButton btnScan;
        private Bitmap airlineImageBMP;
        private Bitmap cardImageBMP;
        private Bitmap agentAirlineBMP;

        public Passenger SelectedPassenger
        {
            get
            {
                if (MembershipProvider.Current != null)
                {
                    if (((AIMSApp)Application).IsGuestMode)
                        return MembershipProvider.Current.SelectedGuest;
                    else return MembershipProvider.Current.SelectedPassenger;
                }
                return null;
            }
            set
            {
                if (MembershipProvider.Current != null)
                {
                    if (((AIMSApp)Application).IsGuestMode)
                        MembershipProvider.Current.SelectedGuest = value;
                    else MembershipProvider.Current.SelectedPassenger = value;
                }
            }
        }

        public bool IsSecondScan { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ManualPassengerEntry);
            ((AIMSApp)Application).ManualInputForm = this;
            ValidationResult = new PassengerValidation();
            App.CloseManualEntryPage = false;

            if (MembershipProvider.Current == null)
            {
                Finish();
                return;//TODO: Logout
            }

            imgAirline = FindViewById<ImageView>(Resource.Id.imgAirline);

            imgMISelectedCard = FindViewById<ImageView>(Resource.Id.imgCard);

            adapterFrom = new AirportListAdapter(this, Android.Resource.Layout.TwoLineListItem, items);

            adapterTo = new AirportListAdapter(this, Android.Resource.Layout.TwoLineListItem, items);

            txtAirportFrom = FindViewById<AutoCompleteTextView>(Resource.Id.txtAirportFrom);
            txtAirportFrom.Threshold = 2;
            txtAirportFrom.Adapter = adapterFrom;

            txtAirportTo = FindViewById<AutoCompleteTextView>(Resource.Id.txtAirportTo);
            txtAirportTo.Threshold = 2;
            txtAirportTo.Adapter = adapterTo;

            txtAirportFrom.ItemClick += OnSelectedAirport;
            txtAirportTo.ItemClick += OnSelectedAirport;

            txtAirportTo.FocusChange += OnAiportFocusChange;
            txtAirportFrom.FocusChange += OnAiportFocusChange;


            #region Initializing and getting controls
            txtFullName = FindViewById<EditText>(Resource.Id.txtFullName);
            txtFFN = FindViewById<EditText>(Resource.Id.txtFFN);
            txtCarrier = FindViewById<EditText>(Resource.Id.txtCarrier);
            txtFlightNumber = FindViewById<EditText>(Resource.Id.txtFlightNumber);
            txtClassOfTravel = FindViewById<EditText>(Resource.Id.txtClassOfTravel);
            txtSeat = FindViewById<EditText>(Resource.Id.txtSeatNumber);
            txtNotes = FindViewById<EditText>(Resource.Id.txtNotes);
            txtPNR = FindViewById<EditText>(Resource.Id.txtPNR);
            selectedAirlineViewSPMI2 = FindViewById<LinearLayout>(Resource.Id.selectedAirlineViewSPMI2);
            layoutImage = FindViewById<LinearLayout>(Resource.Id.layoutImage);
            layoutSelectedGuestType = FindViewById<LinearLayout>(Resource.Id.layoutSelectedGuestType); 

            lblCardNameSPMI2 = FindViewById<TextView>(Resource.Id.lblCardNameSPMI2);
            lblAgentAirlineSPMI2 = FindViewById<TextView>(Resource.Id.lblAgentAirlineSPMI2);
            lblAgentNameSPMI2 = FindViewById<TextView>(Resource.Id.lblAgentNameSPMI2);
            imgAgentAirlineSPMI2 = FindViewById<ImageView>(Resource.Id.imgAgentAirlineSPMI2);

            lblFromFullSPMI2 = FindViewById<TextView>(Resource.Id.lblFromFullSPMI2);
            lblToFullSPMI2 = FindViewById<TextView>(Resource.Id.lblToFullSPMI2);
            lblSelectedGuestType = FindViewById<TextView>(Resource.Id.lblSelectedGuestType);


            txtFullName.InputType = txtFullName.InputType | Android.Text.InputTypes.TextFlagCapCharacters;
            txtAirportFrom.InputType = txtFullName.InputType | Android.Text.InputTypes.TextFlagCapCharacters;
            txtAirportTo.InputType = txtFullName.InputType | Android.Text.InputTypes.TextFlagCapCharacters;
            txtFFN.InputType = txtFullName.InputType | Android.Text.InputTypes.TextFlagCapCharacters;
            txtCarrier.InputType = txtFullName.InputType | Android.Text.InputTypes.TextFlagCapCharacters;
            txtClassOfTravel.InputType = txtFullName.InputType | Android.Text.InputTypes.TextFlagCapCharacters;
            txtSeat.InputType = txtFullName.InputType | Android.Text.InputTypes.TextFlagCapCharacters;
            txtNotes.InputType = txtFullName.InputType | Android.Text.InputTypes.TextFlagCapCharacters;


            this.txtSeat.EditorAction += (_, e) =>
            {
                if (e.ActionId == Android.Views.InputMethods.ImeAction.Next)
                {

                }
                else
                    e.Handled = false;
            };

            txtFlightNumber.RequestFocus();

            #endregion

            pagerButtonsPanel = FindViewById<TableLayout>(Resource.Id.pagerButtons);
            btnConfirm = (ImageButton)pagerButtonsPanel.FindViewWithTag("MIConfirmButton");
            btnCancel = (ImageButton)pagerButtonsPanel.FindViewWithTag("MICancelButton");
            btnScan = (ImageButton)pagerButtonsPanel.FindViewWithTag("MIScanButton");

            try
            {
                if (App.CurrentDeviceType == DeviceType.Phone)
                {
                    DisplayMetrics metrics = new DisplayMetrics();
                    Window.WindowManager.DefaultDisplay.GetMetrics(metrics);

                    txtAirportTo.DropDownWidth = (int)(320 * metrics.Density);
                    txtAirportFrom.DropDownWidth = (int)(320 * metrics.Density);
                }
            }
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }

            btnConfirm.Click += Confirm;
            btnCancel.Click += Cancel;
            btnScan.Click += Scan;

            txtSeat.EditorAction += OnDoneSeat;

            try
            {
                var missingDataStr = Intent.GetStringExtra("missingValues");
                if (!string.IsNullOrEmpty(missingDataStr))
                {
                    ValidationResult = new PassengerValidation(missingDataStr);
                }

            }
            catch (System.Exception eex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
            }

            //  LoadUITexts(App.TextProvider.SelectedLanguage);

            lblAgentAirlineSPMI2.Text = MembershipProvider.Current.SelectedLounge.LoungeName;
            lblAgentNameSPMI2.Text = MembershipProvider.Current.UserFullName;

            LoadAirlineImage();

            LoadCardImage();


            if (!App.IsGuestMode)
                IsSecondScan = true;

            LoadPassenger();

            LoadUITexts(null);
        }

        private void OnDoneSeat(object sender, TextView.EditorActionEventArgs e)
        {
            if(e.ActionId == Android.Views.InputMethods.ImeAction.Done)
            {
                Confirm(null,null);
            }
        }

        private void OnAiportFocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if(!e.HasFocus)
            {
                var editText = (AutoCompleteTextView)sender;
                TextView textView = editText == txtAirportFrom ? lblFromFullSPMI2 : lblToFullSPMI2;
                LoadFullAirportName(editText.Text, textView);
            }
        }

        private void Cancel(object sender, EventArgs e)
        {
            Finish();
        }

        public void Confirm(object sender, EventArgs e)
        {
            if (MembershipProvider.Current == null)
                return;

            SavePassenger();

            var resultActivity = new Intent(this, typeof(ResultActivity));
            resultActivity.PutExtra("barcode", string.Empty);
            resultActivity.PutExtra("isSecondScan", (false).ToString());
            StartActivity(resultActivity);
            Finish();
        }

        private void Scan(object sender, EventArgs e)
        {
            ((AIMSApp)Application).MainForm.Scan(true);
        }

        private void OnSelectedAirport(object sender, AdapterView.ItemClickEventArgs e)
        {
            var adapter = ((AutoCompleteTextView)sender).Adapter as AirportListAdapter;

            var airport = (adapter.GetItem(e.Position) as JavaObjectBase<Airport>).Item;

            if (airport != null)
                ((AutoCompleteTextView)sender).Text = airport.IATA_Code;

            if (sender == txtAirportFrom)
                lblFromFullSPMI2.Text = airport.Name;
            else
                lblToFullSPMI2.Text = airport.Name;
        }

        private void LoadAirlineImage()
        {
            if (MembershipProvider.Current.SelectedAirline != null)
            {
                #region Filling the Airline
                //passengerTabAirlineNameLabel.Text = MembershipProvider.Current.SelectedAirline.Name.ToUpper();
                if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes != null && MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length > 0)
                {
                    airlineImageBMP = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedAirline.AirlineLogoBytes, 0, MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length);
                    if (airlineImageBMP != null)
                    {
                        airlineImageBMP = ImageAdapter.HighlightImage(airlineImageBMP, 6);
                        imgAirline.SetImageBitmap(airlineImageBMP);
                    }
                }
                else
                {
                    ImageAdapter.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (imageBytes) =>
                    {
                        if (imageBytes != null)
                        {
                            airlineImageBMP = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                            if (airlineImageBMP != null)
                            {
                                airlineImageBMP = ImageAdapter.HighlightImage(airlineImageBMP);

                                this.RunOnUiThread(() =>
                                {
                                    imgAirline.SetImageBitmap(airlineImageBMP);
                                });
                            }
                        }
                    });
                }
                #endregion
            }


            #region Loading Airline Image
            //Title = imgAirline.Text = MembershipProvider.Current.UserHeaderText;
            var airline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
            if (airline != null)
            {
                if (airline.AirlineLogoBytes != null && airline.AirlineLogoBytes.Length > 0)
                {
                    //imgMainPageTitle.SetImageResource(Resource.Drawable.STR);//Star Alliance client
                    agentAirlineBMP = BitmapFactory.DecodeByteArray(airline.AirlineLogoBytes, 0, airline.AirlineLogoBytes.Length);
                    if (agentAirlineBMP != null)
                    {
                        agentAirlineBMP = ImageAdapter.HighlightImage(agentAirlineBMP, 6);
                        imgAgentAirlineSPMI2.SetImageBitmap(agentAirlineBMP);
                    }
                }
                else
                {
                    if (airline.LogoResourceID != 0)
                        imgAirline.SetImageResource(airline.LogoResourceID);
                    else
                        ImageAdapter.LoadImage(airline.ImageHandle, (res) =>
                        {
                            if (res == null)
                                return;

                            agentAirlineBMP = BitmapFactory.DecodeByteArray(res, 0, res.Length);
                            if (agentAirlineBMP != null)
                            {
                                agentAirlineBMP = ImageAdapter.HighlightImage(agentAirlineBMP);

                                this.RunOnUiThread(() =>
                                {
                                    imgAgentAirlineSPMI2.SetImageBitmap(agentAirlineBMP);
                                });
                            }
                        });
                }
            }
            #endregion
        }

        private void LoadCardImage()
        {
            if (MembershipProvider.Current.SelectedCard != null)
            {
                lblCardNameSPMI2.Text = MembershipProvider.Current.SelectedCard.Name;

                if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                {
                    cardImageBMP = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedCard.CardPictureBytes, 0, MembershipProvider.Current.SelectedCard.CardPictureBytes.Length);
                    if (cardImageBMP != null)
                    {
                        imgMISelectedCard.SetImageBitmap(cardImageBMP);
                        //bm.Recycle();
                        //bm = null;
                    }
                }
                else
                {
                    if (MembershipProvider.Current.SelectedCard.PictureResourceID != 0)
                        imgMISelectedCard.SetImageResource(MembershipProvider.Current.SelectedCard.PictureResourceID);
                    else
                        ImageAdapter.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (res) =>
                        {
                            if (res == null)
                                return;



                            cardImageBMP = BitmapFactory.DecodeByteArray(res, 0, res.Length);
                            if (cardImageBMP != null)
                            {
                                this.RunOnUiThread(() =>
                                {
                                    imgMISelectedCard.SetImageBitmap(cardImageBMP);
                                });
                            }
                        });
                }
            }
        }

        private void LoadPassenger()
        {
            RunOnUiThread(() =>
            {

                if (SelectedPassenger == null) return;

                #region Name(s)
                var names = new List<string>();
                if (ValidationResult.PassengerNames.Count > 0)
                {
                    names = ValidationResult.PassengerNames;
                }
                else if (!string.IsNullOrEmpty(SelectedPassenger.FullName))
                    names.Add(SelectedPassenger.FullName);

                txtFullName.Text = "";
                var fullNameSB = new StringBuilder();//TODO: refactor and clean

                if (names.Count > 0)
                {
                    fullNameSB.Append(names.Last());//using the name on the last document 
                }

                txtFullName.Text = fullNameSB.ToString();

                if (ValidationResult.IsNameRequired || names.Count == 0)
                {
                    txtFullName.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                }
                #endregion

                #region FFN
                if (ValidationResult.IsFFNRequired)
                    txtFFN.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);

                txtFFN.Text = SelectedPassenger.FFN;
                #endregion

                #region Flight Info
                if (ValidationResult.IsFlightInfoRequired)
                {
                    txtCarrier.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                    txtClassOfTravel.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                    txtFlightNumber.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                }

                txtFlightNumber.Text = SelectedPassenger.FlightNumber;
                txtClassOfTravel.Text = SelectedPassenger.TrackingClassOfService;

                if (string.IsNullOrWhiteSpace(SelectedPassenger.FlightCarrier))
                {
                    if (MembershipProvider.Current.SelectedAirline != null)
                    {
                        txtCarrier.Text = MembershipProvider.Current.SelectedAirline.Code;
                    }
                }
                else
                {
                    txtCarrier.Text = SelectedPassenger.FlightCarrier;
                }
                #endregion

                #region Notes
                if (ValidationResult.IsNotesRequired)
                    txtNotes.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);

                txtNotes.Text = SelectedPassenger.Notes;
                #endregion

                txtSeat.Text = SelectedPassenger.SeatNumber;

                #region Filling the PNR
                txtPNR.Text = SelectedPassenger.PNR;
                //flightTabStatusText.Text = SelectedPassenger.PassengerStatus;

                if (ValidationResult.IsPNRRequired)
                    txtPNR.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                #endregion

                #region Filling the Origin Destination
                txtAirportFrom.Text = MembershipProvider.Current.SelectedLounge.AirportCode;
                LoadFullAirportName(txtAirportFrom.Text, lblFromFullSPMI2);
                txtAirportTo.Text = SelectedPassenger.ToAirport;

                if (ValidationResult.IsDestinationOriginRequired)
                {
                    txtAirportTo.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                    txtAirportFrom.SetBackgroundResource(Resource.Drawable.textbox_style_highlighted);
                }
                #endregion


                lblFromFullSPMI2.Text = this.txtAirportFrom.Text;
                lblToFullSPMI2.Text = this.txtAirportTo.Text;


                lblSelectedGuestType.Visibility = ((AIMSApp)Application).IsGuestMode ? ViewStates.Visible : ViewStates.Gone;
                if (((AIMSApp)Application).IsGuestMode)
                {
                    selectedAirlineViewSPMI2.SetBackgroundColor(Android.Graphics.Color.ParseColor("#9370db"));
                    layoutImage.SetBackgroundColor(Android.Graphics.Color.ParseColor("#9370db"));
                    layoutSelectedGuestType.SetBackgroundColor(Android.Graphics.Color.ParseColor("#9370db"));

                    lblSelectedGuestType.Text = $"{MembershipProvider.Current.SelectedPassengerService.Name} {((AIMSApp)Application).TextProvider.GetText(2154)}";
                }
                else
                {
                    layoutSelectedGuestType.SetBackgroundColor(Android.Graphics.Color.ParseColor("#0e79d8"));
                    selectedAirlineViewSPMI2.SetBackgroundColor(Android.Graphics.Color.ParseColor("#0e79d8"));
                    layoutImage.SetBackgroundColor(Android.Graphics.Color.ParseColor("#0e79d8"));
                }

                if (IsSecondScan && !((AIMSApp)Application).IsScanningCancelled)
                {
                    SavePassenger();
                    ((AIMSApp)Application).MainForm.Scan(true);
                }
                else if (App.FromGuestActivity)
                {
                    App.FromGuestActivity = false;
                    SavePassenger();
                    ((AIMSApp)Application).MainForm.Scan(true);
                }

            });
        }

        private void LoadFullAirportName(string IATA_Code, TextView label)
        {
            RunOnUiThread(() =>
            {
                label.Text = "...";
            });
            using (var ar = new AirportsRepository())
            {
                ar.OnRepositoryChanged += (data) =>
                {
                    var airport = (Airport)data;
                    if (airport != null)
                        RunOnUiThread(() =>
                        {
                            label.Text = airport.Name;
                        });
                };
                ar.GetAirport(IATA_Code, true);
            }
        }

        public void SavePassenger()
        {
            if (MembershipProvider.Current == null) return;//in case of session timeout (which logs the user out)

            if (SelectedPassenger == null)//Keeping the passenger object allways instanciated and ready...
                SelectedPassenger = new Passenger();

            SelectedPassenger.FullName = txtFullName.Text;

            SelectedPassenger.FFN = txtFFN.Text;
            SelectedPassenger.Notes = txtNotes.Text;

            SelectedPassenger.FlightCarrier = txtCarrier.Text;
            SelectedPassenger.FlightNumber = txtFlightNumber.Text;
            SelectedPassenger.TrackingClassOfService = txtClassOfTravel.Text;

            SelectedPassenger.FromAirport = txtAirportFrom.Text.ToUpper();
            SelectedPassenger.ToAirport = txtAirportTo.Text.ToUpper();
            SelectedPassenger.SeatNumber = txtSeat.Text.ToUpper();

            SelectedPassenger.PNR = txtPNR.Text.ToUpper();
            SelectedPassenger.TrackingPassCombinedFlightNumber = "";
        }

        public override void Finish()
        {
            //if (IsSecondScan && !((AIMSApp)Application).IsScanningCancelled)
            //    ((AIMSApp)Application).MainForm.Scan(true);
            //else App.IsGuestMode = false;

            try
            {
                if (App.ContinueScanning && !App.IsGuestMode)
                    App.MainForm.Scan(true);
                else
                {
                    if (imgAgentAirlineSPMI2 != null)
                        imgAgentAirlineSPMI2.Dispose();

                    if (imgAirline != null)
                        imgAirline.Dispose();

                    if (imgMISelectedCard != null)
                        imgMISelectedCard.Dispose();

                    if (airlineImageBMP != null)
                    {
                        airlineImageBMP.Recycle();
                        airlineImageBMP.Dispose();
                        airlineImageBMP = null;
                    }

                    if (cardImageBMP != null)
                    {
                        cardImageBMP.Recycle();
                        cardImageBMP.Dispose();
                        cardImageBMP = null;
                    }

                    if (agentAirlineBMP != null)
                    {
                        agentAirlineBMP.Recycle();
                        agentAirlineBMP.Dispose();
                        agentAirlineBMP = null;
                    }
                }
            }
            catch(Exception ex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex, new Dictionary<string, string> {
                                { "FunctionName", "ManualEntryActivity.Finish" }
                });
            }

            base.Finish();
        }

        public void Close()
        {
            Finish();
        }

        public override void LoadUITexts(Language? selectedLanguage)
        {
            txtFlightNumber.Hint = App.TextProvider.GetText(2015);//Flight
            txtPNR.Hint = App.TextProvider.GetText(2044);//PNR
            txtFullName.Hint = App.TextProvider.GetText(2024);//Full Name
            txtFFN.Hint = App.TextProvider.GetText(2027);//Loyalty Number

            var fromText = App.TextProvider.GetText(2032);//From (Airport)
            if (fromText.IndexOf(" ") >= 0)
                fromText = fromText.Substring(0, fromText.IndexOf(" "));//From
            txtAirportFrom.Hint = fromText;

            var toText = App.TextProvider.GetText(2033);//To (Airport)
            if (toText.IndexOf(" ") >= 0)
                toText = toText.Substring(0, toText.IndexOf(" "));//To
            txtAirportTo.Hint = toText;

            txtNotes.Hint = App.TextProvider.GetText(2028);//Notes
            txtSeat.Hint = App.TextProvider.GetText(2063);//Seat

            if (App.IsGuestMode)
                lblSelectedGuestType.Text = $"{MembershipProvider.Current.SelectedPassengerService.Name} {App.TextProvider.GetText(2154)}";
        }

        protected override void OnResume()
        {
            base.OnResume();

            if(App.CloseManualEntryPage)
            {
                App.CloseManualEntryPage = false;
                Finish();
            }
        }
    }
}