﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIMS.Models;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;
using Microsoft.AppCenter.Analytics;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS", Icon = "@drawable/Icon", Theme = "@android:style/Theme.NoTitleBar.OverlayActionModes" /*, MainLauncher = true*/, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class ValidationResultActivity : BaseActivity
    {
        private TextView lblFlightLabelVR2;
        private TextView lblPNRLabelVR2;
        private TextView lblFFNLabelVR2;
        private TextView lblBoardingTimeLabelVR2;
        private TextView lblTerminalLabelVR2;
        private TextView lblGateLabelVR2;
        private TextView lblReentry;
        private RelativeLayout reentryView;
        private TextView lblSeatLabelVR2;
        private Button btnBarcodeDetails;
        private Button btnConfirm;
        private TextView lblFFNVR2;
        private TextView lblFlightVR2;
        private TextView lblPNRVR2;
        private TextView lblFullNameVR2;
        private TextView lblBoardingDateVR2;
        private TextView lblFromVR2;
        private TextView lblFromFullAirportNameVR2;
        private TextView lblToVR2;
        private TextView lblToFullAirportNameVR2;
        private TextView lblBoardingTimeVR2;
        private TextView lblTerminalVR2;
        private TextView lblGateVR2;
        private TextView lblSeatVR2;
        private TextView lblTimeVR2;
        private TextView lblWeatherVR2;
        private TextView lblCardNameVR2;
        private ImageView imgSelectedCardVR2;
        private ImageView imgSelectedAirlineVR2;
        private ImageView imgSelectedCardOtherVR2;
        private TextView lblCardOtherNameVR2;
        private LinearLayout selectedAirlineViewSPMI2;
        private LinearLayout layoutImage;
        private LinearLayout layoutInf;
        private CenterLockHorizontalScrollview horizontalScrollViewServices;
        private LinearLayout itemlayoutServices;
        private string onTimeText;
        private string delayedText;
        private Bitmap otherCardImageBMP;
        private Bitmap airlineImageBMP;
        private Bitmap guestAirlineImageBMP;
        private Bitmap cardImageBMP;
        private View emptyView;

        public string BarcodeString { get; private set; }

        public override void LoadUITexts(Language? selectedLanguage = null)
        {
            RunOnUiThread(() =>
            {
                onTimeText = App.TextProvider.GetText(2021);
                delayedText = App.TextProvider.GetText(2020);

                lblFlightLabelVR2.Text = string.Format("{0}:", App.TextProvider.GetText(2015).ToUpper());//FLIGHT
                lblPNRLabelVR2.Text = string.Format("{0}:", App.TextProvider.GetText(2044).ToUpper());//PNR
                lblFFNLabelVR2.Text = string.Format("{0}:", App.TextProvider.GetText(2141).ToUpper());//CARD

                lblBoardingTimeLabelVR2.Text = App.TextProvider.GetText(2061).ToUpper();//BOARDING AT
                lblTerminalLabelVR2.Text = App.TextProvider.GetText(2061).ToUpper();//TERMINAL
                lblGateLabelVR2.Text = App.TextProvider.GetText(2058).ToUpper();//GATE
                lblSeatLabelVR2.Text = App.TextProvider.GetText(2063).ToUpper();//SEAT

                btnBarcodeDetails.Text = (App.TextProvider.GetText(2045));//"Details"
            });
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ValidationResult);
            App.ValidationForm = this;

            lblFlightLabelVR2 = FindViewById<TextView>(Resource.Id.lblFlightLabelVR2);
            lblPNRLabelVR2 = FindViewById<TextView>(Resource.Id.lblPNRLabelVR2);
            lblFFNLabelVR2 = FindViewById<TextView>(Resource.Id.lblFFNLabelVR2);
            lblBoardingTimeLabelVR2 = FindViewById<TextView>(Resource.Id.lblBoardingTimeLabelVR2);
            lblTerminalLabelVR2 = FindViewById<TextView>(Resource.Id.lblTerminalLabelVR2);
            lblGateLabelVR2 = FindViewById<TextView>(Resource.Id.lblGateLabelVR2);
            lblReentry = FindViewById<TextView>(Resource.Id.lblreentry);
            reentryView = FindViewById<RelativeLayout>(Resource.Id.reentryView);
            lblSeatLabelVR2 = FindViewById<TextView>(Resource.Id.lblSeatLabelVR2);
            btnBarcodeDetails = FindViewById<Button>(Resource.Id.btnBarcodeDetails);
            btnConfirm = FindViewById<Button>(Resource.Id.btnConfirm);

            lblFFNVR2 = FindViewById<TextView>(Resource.Id.lblFFNVR2);
            lblFlightVR2 = FindViewById<TextView>(Resource.Id.lblFlightVR2);
            lblPNRVR2 = FindViewById<TextView>(Resource.Id.lblPNRVR2);
            lblFullNameVR2 = FindViewById<TextView>(Resource.Id.lblFullNameVR2);
            //lblCardNameVR2 = FindViewById<TextView>(Resource.Id.lblCardNameVR2);
            //lblCardOtherNameVR2 = FindViewById<TextView>(Resource.Id.lblCardOtherNameVR2);
            lblBoardingDateVR2 = FindViewById<TextView>(Resource.Id.lblBoardingDateVR2);
            lblFromVR2 = FindViewById<TextView>(Resource.Id.lblFromVR2);
            lblFromFullAirportNameVR2 = FindViewById<TextView>(Resource.Id.lblFromFullAirportNameVR2);
            lblToVR2 = FindViewById<TextView>(Resource.Id.lblToVR2);
            lblToFullAirportNameVR2 = FindViewById<TextView>(Resource.Id.lblToFullAirportNameVR2);
            lblBoardingTimeVR2 = FindViewById<TextView>(Resource.Id.lblBoardingTimeVR2);
            lblTerminalVR2 = FindViewById<TextView>(Resource.Id.lblTerminalVR2);
            lblGateVR2 = FindViewById<TextView>(Resource.Id.lblGateVR2);
            lblSeatVR2 = FindViewById<TextView>(Resource.Id.lblSeatVR2);
            lblTimeVR2 = FindViewById<TextView>(Resource.Id.lblTimeVR2);
            lblWeatherVR2 = FindViewById<TextView>(Resource.Id.lblWeatherVR2);
            lblCardNameVR2 = FindViewById<TextView>(Resource.Id.lblCardNameVR2);

            imgSelectedCardVR2 = FindViewById<ImageView>(Resource.Id.imgSelectedCardVR2);
            imgSelectedAirlineVR2 = FindViewById<ImageView>(Resource.Id.imgSelectedAirlineVR2);

            imgSelectedCardOtherVR2 = FindViewById<ImageView>(Resource.Id.imgSelectedCardOtherVR2);
            lblCardOtherNameVR2 = FindViewById<TextView>(Resource.Id.lblCardOtherNameVR2);

            selectedAirlineViewSPMI2 = FindViewById<LinearLayout>(Resource.Id.selectedAirlineViewSPMI2);
            layoutImage = FindViewById<LinearLayout>(Resource.Id.layoutImage);
            layoutInf = FindViewById<LinearLayout>(Resource.Id.layoutInf);


            horizontalScrollViewServices = FindViewById<CenterLockHorizontalScrollview>(Resource.Id.horizontalScrollView1);
            itemlayoutServices = FindViewById<LinearLayout>(Resource.Id.itemServices);
            horizontalScrollViewServices.OnItemSelected += OnPassengerSelected;

            emptyView = FindViewById<View>(Resource.Id.emptyView);

            btnConfirm.Click += (sender, e) =>
            {
                Finish();
            };

            btnBarcodeDetails.Click += (sender, e) =>
            {
                AIMSMessage(btnBarcodeDetails.Text, BarcodeString);
            };

            ChangeSizeofImages();

            LoadUITexts();

            RefreshPassenger();

            CheckReentry();

            AddSpaceForReentry();
        }

        private void ChangeSizeofImages()
        {
            if (App.CurrentDeviceType == DeviceType.Phone)
            {
                Point sizeScreen = new Point();
                Window.WindowManager.DefaultDisplay.GetSize(sizeScreen);

                float infWeight = 5.0f;
                float imgWeight = 2.0f;

                if (sizeScreen.X > sizeScreen.Y)
                {
                    infWeight = 6.0f;
                    imgWeight = 1.0f;
                }

                LinearLayout.LayoutParams layoutInfParam = new LinearLayout.LayoutParams(0, WindowManagerLayoutParams.WrapContent)
                {
                    Weight = infWeight
                };
                layoutInf.LayoutParameters = layoutInfParam;

                LinearLayout.LayoutParams layouImageParam = new LinearLayout.LayoutParams(0, WindowManagerLayoutParams.WrapContent)
                {
                    Weight = imgWeight
                };
                layoutImage.LayoutParameters = layouImageParam;
            }
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            ChangeSizeofImages();

            AddSpaceForReentry();
        }

        private void AddSpaceForReentry()
        {
            if (reentryView.Visibility == ViewStates.Visible)
            {
                Point sizeScreen = new Point();
                Window.WindowManager.DefaultDisplay.GetSize(sizeScreen);
                if (sizeScreen.X > sizeScreen.Y)
                {
                    emptyView.Visibility = ViewStates.Visible;
                }
                else
                {
                    emptyView.Visibility = ViewStates.Gone;
                }
            }
        }

        private void OnPassengerSelected(object sender, PassengerService e)
        {
            MembershipProvider.Current.SelectedPassengerService = e;

            App.GuestsIntent = new Intent(this, typeof(GuestsActivity));
            App.GuestsIntent.PutExtra("group", (int)sender);
            App.GuestsIntent.SetFlags(ActivityFlags.NewTask);
            App.MainForm.StartActivity(App.GuestsIntent);
        }

        private void CheckReentry()
        {
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null)
                return;

            if (MembershipProvider.Current.SelectedPassenger.IsReEntry)
            {
                reentryView.Visibility = ViewStates.Visible;

                string colorCode = MembershipProvider.Current.SelectedPassenger.NotificationColorCode;

                if (!string.IsNullOrEmpty(colorCode))
                {
                    reentryView.SetBackgroundColor(Color.ParseColor(colorCode));
                }

                lblReentry.Text = App.TextProvider.GetText(2182).ToUpper();
            }
            else if (MembershipProvider.Current.SelectedPassenger.IsLoungeHopping)
            {
                reentryView.Visibility = ViewStates.Visible;

                string colorCode = MembershipProvider.Current.SelectedPassenger.NotificationColorCode;

                if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.NotificationColorCode))
                {
                    reentryView.SetBackgroundColor(Color.ParseColor(colorCode));
                }

                lblReentry.Text = App.TextProvider.GetText(2183).ToUpper();
            }
            else
            {
                reentryView.Visibility = ViewStates.Gone;
                reentryView.SetBackgroundColor(Color.Transparent);
                lblReentry.Text = string.Empty;
            }
        }

        private void RefreshPassenger()
        {
            ClearContents();

            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null)
                return;

            RefreshColors();

            PassengersRepository passengersRepository = new PassengersRepository();

            if (MembershipProvider.Current.SelectedGuest != null)
            {
                SetContents();
            }
            else if (!MembershipProvider.Current.SelectedPassenger.IsGuest)
            {
                passengersRepository.OnPassengerChecked += OnPassengerChecked;
                passengersRepository.RetrieveMainPassenger(MembershipProvider.Current.SelectedPassenger, MembershipProvider.Current.SelectedLounge);
            }
            else
            {
                passengersRepository.OnPassengerChecked += OnPassengerChecked;
                passengersRepository.RetrieveGuestPassenger(MembershipProvider.Current.SelectedPassenger, MembershipProvider.Current.SelectedLounge);
            }
        }

        private void RefreshColors()
        {
            if ((MembershipProvider.Current.SelectedGuest != null && MembershipProvider.Current.SelectedGuest.TrackingRecordID > 0) ||
                MembershipProvider.Current.SelectedPassenger.IsGuest)
            {
                selectedAirlineViewSPMI2.SetBackgroundColor(Android.Graphics.Color.ParseColor("#9370db"));
                layoutImage.SetBackgroundColor(Android.Graphics.Color.ParseColor("#9370db"));
                lblCardNameVR2.Text = string.Empty;
                imgSelectedCardVR2.SetImageBitmap(null);

                lblCardNameVR2.Visibility = ViewStates.Gone;
                imgSelectedCardVR2.Visibility = ViewStates.Gone;
                //lblCardOtherNameVR2.Visibility = ViewStates.Gone;
                //imgSelectedCardOtherVR2.Visibility = ViewStates.Gone;
                horizontalScrollViewServices.Visibility = ViewStates.Gone;
            }
            else
            {
                selectedAirlineViewSPMI2.SetBackgroundColor(Android.Graphics.Color.ParseColor("#0e79d8"));
                layoutImage.SetBackgroundColor(Android.Graphics.Color.ParseColor("#0e79d8"));

                lblCardNameVR2.Visibility = ViewStates.Visible;
                imgSelectedCardVR2.Visibility = ViewStates.Visible;

                horizontalScrollViewServices.Visibility = ViewStates.Visible;
            }
        }

        private void ClearContents()
        {
            RunOnUiThread(() =>
            {
                //servicesCollectionViewVR2.Hidden = true;
                imgSelectedCardVR2.SetImageBitmap(null);
                //imgSelectedCardOtherVR2.Image = null;
                imgSelectedAirlineVR2.SetImageBitmap(null);

                // lblFFNVR2.Text = lblFlightVR2.Text = lblFullNameVR2.Text = lblBoardingDateVR2.Text = "";

                lblFFNVR2.Text = lblFlightVR2.Text = lblPNRVR2.Text = lblFullNameVR2.Text =
                lblCardNameVR2.Text = lblCardOtherNameVR2.Text = lblBoardingDateVR2.Text = "";

                lblFromVR2.Text = lblFromFullAirportNameVR2.Text = lblToVR2.Text = lblToFullAirportNameVR2.Text =
                lblBoardingTimeVR2.Text = lblTerminalVR2.Text = lblGateVR2.Text = lblSeatVR2.Text =
                lblTimeVR2.Text = lblWeatherVR2.Text = "...";

                btnBarcodeDetails.Visibility = ViewStates.Gone;
            });
        }

        private void OnPassengerChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            MembershipProvider.Current.SelectedPassenger = passenger;
            SetContents();
        }

        private void SetContents()
        {
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null)
                return;

            RunOnUiThread(new Action(() =>
            {
                try
                {

                    imgSelectedCardOtherVR2.SetImageBitmap(null);
                    lblCardNameVR2.Text = "";
                    lblCardOtherNameVR2.Text = "";
                    lblBoardingTimeVR2.Text = "...";
                    lblTerminalVR2.Text = "...";
                    lblGateVR2.Text = "...";
                    lblSeatVR2.Text = "...";
                    lblTimeVR2.Text = "...";
                    lblWeatherVR2.Text = "...";
                    btnBarcodeDetails.Visibility = ViewStates.Gone;
                    imgSelectedCardVR2.SetImageBitmap(null);

                    if (!App.IsGuestMode)
                    {
                        if (MembershipProvider.Current.SelectedCard != null)
                        {
                            lblCardNameVR2.Text = MembershipProvider.Current.SelectedCard.Name;
                            if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                            {
                                cardImageBMP = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedCard.CardPictureBytes, 0, MembershipProvider.Current.SelectedCard.CardPictureBytes.Length);
                                if (cardImageBMP != null)
                                {
                                    imgSelectedCardVR2.SetImageBitmap(cardImageBMP);
                                }
                            }
                            else
                            {
                                var item = (new JavaCard(MembershipProvider.Current.SelectedCard));
                                App.ImageRepository.LoadImage(item.ImageHandle, (imageResult) =>
                                {
                                    if (imageResult == null)
                                    {
                                        LogsRepository.AddError("Error in SummaryActivity in loading card image", "result is null");

                                        Analytics.TrackEvent("ValidationResultActivity", new Dictionary<string, string>
                                        {
                                            { "Error in SummaryActivity in loading card image", "result is null" }
                                        });

                                        return;
                                    }

                                    if (imageResult.IsSucceeded)
                                    {
                                        var bytes = imageResult.ReturnParam;

                                        //item.SaveImage(bytes);//Removed IImageItem and it is automatically saved in the ImageRepository

                                        if (bytes == null || bytes.Length == 0) return;

                                        try
                                        {
                                            cardImageBMP = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                                            if (cardImageBMP != null)
                                            {
                                                this.RunOnUiThread(() =>
                                                {
                                                    imgSelectedCardVR2.SetImageBitmap(cardImageBMP);
                                                });
                                                //bm.Recycle();
                                                //bm = null;
                                            }
                                        }
                                        catch (Exception eex)
                                        {
                                            Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                                        }
                                    }
                                });
                            }
                        }

                        LoadOtherCardImage();

                        LoadAirlineImage();

                        lblFromVR2.Text = MembershipProvider.Current.SelectedPassenger.FromAirport;
                        LoadFullAirportName(lblFromVR2.Text, lblFromFullAirportNameVR2);
                        lblToVR2.Text = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedPassenger.ToAirport) ? "..." : MembershipProvider.Current.SelectedPassenger.ToAirport;
                        LoadFullAirportName(lblToVR2.Text, lblToFullAirportNameVR2);

                        lblFullNameVR2.Text = MembershipProvider.Current.SelectedPassenger.FullName;
                        lblFFNVR2.Text = MembershipProvider.Current.SelectedPassenger.FFN;
                        lblFlightVR2.Text = string.Format("{0} {1} {2}",
                            MembershipProvider.Current.SelectedPassenger.FlightCarrier,
                            string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.FlightNumber) ? string.Empty : MembershipProvider.Current.SelectedPassenger.FlightNumber.TrimStart('0'),
                            MembershipProvider.Current.SelectedPassenger.TrackingClassOfService);
                        lblPNRVR2.Text = MembershipProvider.Current.SelectedPassenger.PNR;
                        var bpValdationDate =
                            MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate != null && MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate != DateTime.MinValue ?
                            MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate : MembershipProvider.Current.SelectedPassenger.TrackingTimestamp;
                        lblBoardingDateVR2.Text = bpValdationDate.ToString("d MMM yyyy");

                        if (MembershipProvider.Current.SelectedPassenger.PassengerServices == null)//TODO: move this into Client Core Library inside (MembershipProvider.Current.SelectedPassenger.Set{} very last line)
                            MembershipProvider.Current.SelectedPassenger.PassengerServices = new Dictionary<PassengerService, List<Passenger>>();

                        horizontalScrollViewServices.SetAdapter(this, new ServicesAdapter(this));
                    }
                    else if (MembershipProvider.Current.SelectedGuest != null)
                    {
                        imgSelectedCardVR2.SetImageBitmap(null);
                        lblCardNameVR2.Text = "";

                        lblFullNameVR2.Text = MembershipProvider.Current.SelectedGuest.FullName;
                        lblFFNVR2.Text = MembershipProvider.Current.SelectedGuest.FFN;
                        lblFlightVR2.Text = string.Format("{0} {1} {2}",
                            MembershipProvider.Current.SelectedGuest.FlightCarrier,
                            MembershipProvider.Current.SelectedGuest.FlightNumber,
                            MembershipProvider.Current.SelectedGuest.TrackingClassOfService);
                        lblPNRVR2.Text = MembershipProvider.Current.SelectedGuest.PNR;
                        var bpValdationDate =
                            MembershipProvider.Current.SelectedGuest.BoardingPassFlightDate != null && MembershipProvider.Current.SelectedGuest.BoardingPassFlightDate != DateTime.MinValue ?
                            MembershipProvider.Current.SelectedGuest.BoardingPassFlightDate : MembershipProvider.Current.SelectedGuest.TrackingTimestamp;
                        lblBoardingDateVR2.Text = bpValdationDate.ToString("d MMM yyyy");

                        lblFromVR2.Text = MembershipProvider.Current.SelectedGuest.FromAirport;
                        LoadFullAirportName(lblFromVR2.Text, lblFromFullAirportNameVR2);
                        lblToVR2.Text = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedGuest.ToAirport) ? "..." : MembershipProvider.Current.SelectedGuest.ToAirport;
                        LoadFullAirportName(lblToVR2.Text, lblToFullAirportNameVR2);

                        LoadGuestAirlineImage();
                    }

                    lblPNRLabelVR2.Visibility = string.IsNullOrWhiteSpace(lblPNRVR2.Text) ? ViewStates.Gone : ViewStates.Visible;

                    btnBarcodeDetails.Visibility = ViewStates.Gone;
#if DEBUG
                    if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.BarcodeString))
                    {
                        BarcodeString = MembershipProvider.Current.SelectedPassenger.BarcodeString;
                        btnBarcodeDetails.Visibility = ViewStates.Visible;
                    }
                    else if (MembershipProvider.Current.SelectedGuest != null && !string.IsNullOrEmpty(MembershipProvider.Current.SelectedGuest.BarcodeString))
                    {
                        BarcodeString = MembershipProvider.Current.SelectedGuest.BarcodeString;
                        btnBarcodeDetails.Visibility = ViewStates.Visible;
                    }
#endif

#if DEBUG//TODO: remove in case the service has been implemented
                    lblBoardingTimeVR2.Text = "08:45AM";
                    lblTerminalVR2.Text = "3";
                    lblGateVR2.Text = "16";
                    lblSeatVR2.Text = "56E";
                    lblTimeVR2.Text = "+10hrs";
                    lblWeatherVR2.Text = "23 C";
#endif

                    RefreshColors();
                }

                catch (Exception ex)
                {
                    LogsRepository.AddError("Error in SetContents()", ex);

                    Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                }
            }));
        }

        private void LoadOtherCardImage()
        {
            if (MembershipProvider.Current.SelectedPassenger.OtherCardID > 0)
            {
                lblCardOtherNameVR2.Text = string.Format("Card Id: {0}", MembershipProvider.Current.SelectedPassenger.OtherCardID);

                var otherCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedPassenger.OtherCardID);
                if (otherCard != null)
                {
                    lblCardOtherNameVR2.Visibility = ViewStates.Visible;
                    imgSelectedCardOtherVR2.Visibility = ViewStates.Visible;


                    lblCardOtherNameVR2.Text = otherCard.Name;
                    if (otherCard.CardPictureBytes != null && otherCard.CardPictureBytes.Length > 0)
                    {
                        otherCardImageBMP = BitmapFactory.DecodeByteArray(otherCard.CardPictureBytes, 0, otherCard.CardPictureBytes.Length);
                        if (otherCardImageBMP != null)
                        {
                            imgSelectedCardOtherVR2.SetImageBitmap(otherCardImageBMP);
                        }
                    }
                    else
                    {
                        ImageAdapter.LoadImage(otherCard.ImageHandle, (imageBytes) =>
                        {
                            otherCardImageBMP = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                            RunOnUiThread(() =>
                            {
                                imgSelectedCardOtherVR2.SetImageBitmap(otherCardImageBMP);
                            });
                        });
                    }
                }
            }
            else
            {
                imgSelectedCardOtherVR2.Visibility = ViewStates.Gone;
                lblCardOtherNameVR2.Visibility = ViewStates.Gone;
            }
        }

        private void LoadAirlineImage()
        {
            if (MembershipProvider.Current.SelectedAirline != null)
            {
                #region Filling the Airline
                if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes != null && MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length > 0)
                {
                    airlineImageBMP = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedAirline.AirlineLogoBytes, 0, MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length);
                    if (airlineImageBMP != null)
                    {
                        airlineImageBMP = ImageAdapter.HighlightImage(airlineImageBMP, 6);
                        imgSelectedAirlineVR2.SetImageBitmap(airlineImageBMP);
                    }
                }
                else
                {
                    ImageAdapter.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (imageBytes) =>
                    {
                        if (imageBytes == null)
                            return;

                        airlineImageBMP = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                        if (airlineImageBMP != null)
                        {
                            airlineImageBMP = ImageAdapter.HighlightImage(airlineImageBMP);

                            this.RunOnUiThread(() =>
                            {
                                imgSelectedAirlineVR2.SetImageBitmap(airlineImageBMP);
                            });
                        }
                    });
                }
                #endregion
            }
        }

        private void LoadGuestAirlineImage()
        {
            var guestAirline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.SelectedGuest.AirlineId);
            if (guestAirline != null)
            {
                guestAirlineImageBMP = guestAirline.AirlineLogoBytes == null ? null : BitmapFactory.DecodeByteArray(guestAirline.AirlineLogoBytes, 0, guestAirline.AirlineLogoBytes.Length);
                if (guestAirlineImageBMP != null)
                {
                    guestAirlineImageBMP = ImageAdapter.HighlightImage(guestAirlineImageBMP, 6);
                    imgSelectedAirlineVR2.SetImageBitmap(guestAirlineImageBMP);
                }
                else
                {
                    ImageAdapter.LoadImage(guestAirline.ImageHandle, (imageBytes) =>
                    {
                        var bmp = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                        if (bmp != null)
                        {
                            bmp = ImageAdapter.HighlightImage(bmp);
                            this.RunOnUiThread(() =>
                            {
                                imgSelectedAirlineVR2.SetImageBitmap(bmp);
                            });
                        }
                    });
                }
            }
        }

        private void LoadFullAirportName(string IATA_Code, TextView label)
        {
            RunOnUiThread(() =>
            {
                label.Text = "...";
            });
            using (var ar = new AirportsRepository())
            {
                ar.OnRepositoryChanged += (data) =>
                {
                    var airport = (Airport)data;
                    if (airport != null)
                        RunOnUiThread(() =>
                        {
                            label.Text = airport.Name;
                        });
                };
                ar.GetAirport(IATA_Code, true);
            }
        }

        public override void Finish()
        {
            base.Finish();

            CleanMemoryOfImages();

            if (!App.IsGuestMode)
                MembershipProvider.Current.SelectedPassenger = null;

            MembershipProvider.Current.SelectedGuest = null;

            App.CloseManualEntryPage = true;

            if (App.ContinueScanning && !App.IsGuestMode)
                App.MainForm.Scan();
            else
                App.IsGuestMode = false;
        }

        private void CleanMemoryOfImages()
        {
            try
            {
                if (imgSelectedAirlineVR2 != null)
                    imgSelectedAirlineVR2.Dispose();

                if (imgSelectedCardOtherVR2 != null)
                    imgSelectedCardOtherVR2.Dispose();

                if (imgSelectedCardVR2 != null)
                    imgSelectedCardVR2.Dispose();


                if (airlineImageBMP != null)
                {
                    airlineImageBMP.Recycle();
                    airlineImageBMP.Dispose();
                    airlineImageBMP = null;
                }

                if (guestAirlineImageBMP != null)
                {
                    guestAirlineImageBMP.Recycle();
                    guestAirlineImageBMP.Dispose();
                    guestAirlineImageBMP = null;
                }

                if (cardImageBMP != null)
                {
                    cardImageBMP.Recycle();
                    cardImageBMP.Dispose();
                    cardImageBMP = null;
                }

                if (otherCardImageBMP != null)
                {
                    otherCardImageBMP.Recycle();
                    otherCardImageBMP.Dispose();
                    otherCardImageBMP = null;
                }

            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex, new Dictionary<string, string> {
                                { "FunctionName", "ValidationResultActivity.Finish" }
                });
            }
        }

        protected override void OnResume()
        {
            base.OnResume();

            if (horizontalScrollViewServices.Visibility == ViewStates.Visible)
            {
                var adapter = new ServicesAdapter(this);
                horizontalScrollViewServices.SetAdapter(this, adapter);
                adapter.NotifyDataSetChanged();
            }
        }



    }
}