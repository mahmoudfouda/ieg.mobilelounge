﻿
using System;
using System.Collections.Generic;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Ieg.Mobile.Localization;
using Microsoft.AppCenter.Analytics;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS | Lounges", Icon = "@drawable/Icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class LoungeSelectionActivity : BaseActivity
    {

        #region Elements
        ListView lstLounges;
        private Button logoutButton;
        TextView lblTitle, lblLoungeSelectionSubTitle;
        #endregion

        #region Methods
        public override void LoadUITexts(Language? selectedLanguage)
        {
            //TODO: Load all texts and UI labels here
            Title = App.TextProvider.GetText(2037);// "Available lounges";

            lblTitle.Text = string.Format("{0} {1}!", App.TextProvider.GetText(2006)/*"Welcome "*/, MembershipProvider.Current.LoggedinUser.FirstName);
            lblLoungeSelectionSubTitle.Text = App.TextProvider.GetText(2503);//Please select the lounge

            logoutButton.Text = App.TextProvider.GetText(2056);
        }
        #endregion

        #region Events
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LoungeSelection);

            App.LoungeSelectionForm = this;

            lblTitle = FindViewById<TextView>(Resource.Id.lblLoungeSelectionTitle);
            lblLoungeSelectionSubTitle = FindViewById<TextView>(Resource.Id.lblLoungeSelectionSubTitle);
            lstLounges = FindViewById<ListView>(Resource.Id.lstLounges);
            logoutButton = FindViewById<Button>(Resource.Id.logoutButton);

            LoadUITexts(App.TextProvider.SelectedLanguage);

            lstLounges.Adapter = new LoungeAdapter(this);
            lstLounges.ItemClick += delegate (object sender, AdapterView.ItemClickEventArgs args)
            {
                try
                {
                    ShowLoading("");
                    var selectedLounge = ((JavaLounge)lstLounges.Adapter.GetItem(args.Position));
                    MembershipProvider.Current.SelectedLounge = selectedLounge;

                    try
                    {
                        MembershipProvider.Current.SelectedLounge.LastPercentage = selectedLounge.LastPercentage;
                    }
                    catch { }

                    LogsRepository.AddClientInfo("User selected a lounge", string.Format("Lounge detail: {0} | WSID: {1}", MembershipProvider.Current.SelectedLounge.LoungeName, MembershipProvider.Current.SelectedLounge.WorkstationID));

                    Analytics.TrackEvent("LoungeSelectionActivity", new Dictionary<string, string>
                    {
                        { "User selected a lounge", string.Format("Lounge detail: {0} | WSID: {1}", MembershipProvider.Current.SelectedLounge.LoungeName, MembershipProvider.Current.SelectedLounge.WorkstationID) }
                    });



                    HideLoading();
                    StartActivity(App.intentMain);
                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Exception in selecting lounge", ex);
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                }
            };

            logoutButton.Click += OnLogoutClick;
        }

        private void OnLogoutClick(object sender, EventArgs e)
        {
            //OnBackPressed();

            StartActivity(new Android.Content.Intent(Application.Context, typeof(LoginActivity)));
            Finish();
        }

        public override void OnBackPressed()
        {
            Process.KillProcess(Process.MyPid());
        }
        #endregion
    }
}

