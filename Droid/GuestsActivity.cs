﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIMS.Droid.Adapters;
using AIMS.Models;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.Localization;

namespace AIMS.Droid
{
    [Activity(Label = "AIMS Passenger Info", Icon = "@drawable/Icon", Theme = "@style/Theme.PageIndicatorDefaults"/*, WindowSoftInputMode = SoftInput.AdjustResize*/)]
    [IntentFilter(new[] { Intent.ActionMain }, Categories = new[] { "com.iegamerica.aims" })]
    public class GuestsActivity : BaseActivity
    {
        private ExpandableListView expListView;
        private ExpandableListAdapter expandableListAdapter;
        private int groupId;

        public override void LoadUITexts(Language? selectedLanguage)
        {
            
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Guests);

            expListView = FindViewById<ExpandableListView>(Resource.Id.lvExp);
            expandableListAdapter = new ExpandableListAdapter(this, MembershipProvider.Current.SelectedPassenger.PassengerServices);
            expListView.SetAdapter(expandableListAdapter);

            expListView.ChildClick += ExpListView_ChildClick;

            var lblHostName = FindViewById<TextView>(Resource.Id.lblHostName);
            lblHostName.Text = MembershipProvider.Current.SelectedPassenger.FullName;

            groupId = this.Intent.GetIntExtra("group", 0);


            LoadCardImage();
        }

        protected override void OnResume()
        {
            base.OnResume();

            if (MembershipProvider.Current.SelectedPassenger != null)
            {
                expandableListAdapter = new ExpandableListAdapter(this, MembershipProvider.Current.SelectedPassenger.PassengerServices);
                expListView.SetAdapter(expandableListAdapter);

                if (groupId >= 0)
                    expListView.ExpandGroup(groupId, true);

                expandableListAdapter.NotifyDataSetChanged();
            }
        }

        private void ExpListView_ChildClick(object sender, ExpandableListView.ChildClickEventArgs e)
        {
            if (e.ClickedView != null)
                e.ClickedView.SetBackgroundColor(Color.ParseColor("#30808080"));

            var child = expandableListAdapter.GetChild(e.GroupPosition, e.ChildPosition);

            var passengerService = expandableListAdapter.Services.Keys.ToList()[e.GroupPosition];

            if (passengerService != null)
                MembershipProvider.Current.SelectedPassengerService = passengerService;
            else
                MembershipProvider.Current.SelectedPassengerService = null;

            var passenger = expandableListAdapter.Services[passengerService].Count > e.ChildPosition ? expandableListAdapter.Services[passengerService][e.ChildPosition] : null;

            if(passenger != null)
            {
                MembershipProvider.Current.SelectedGuest = passenger;
                OpenGuestValidator();
            }
            else
            {
                App.IsGuestMode = true;

                MembershipProvider.Current.SelectedGuest = new Passenger
                {
                    CardID = MembershipProvider.Current.SelectedCard.ID,
                    AirlineId = MembershipProvider.Current.SelectedPassenger.AirlineId
                };

                App.FromGuestActivity = true;
                App.IsScanningCancelled = false;
                App.ManualEntryGuest = new Intent(this, typeof(ManualEntryActivity));
                App.ManualEntryGuest.SetFlags(ActivityFlags.NewTask);
                App.MainForm.StartActivity(App.ManualEntryGuest);
            }
        }

        public void OpenGuestValidator()
        {
            this.RunOnUiThread(() =>
            {
                App.IsGuestMode = true;
                App.SummaryIntent = new Intent(this, typeof(ValidationResultActivity));
                App.SummaryIntent.SetFlags(ActivityFlags.NewTask);
                App.MainForm.StartActivity(App.SummaryIntent);
            });
        }

        private void LoadCardImage()
        {
            var imgMISelectedCard = FindViewById<ImageView>(Resource.Id.imgMISelectedCard);

            if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
            {
                var bm = BitmapFactory.DecodeByteArray(MembershipProvider.Current.SelectedCard.CardPictureBytes, 0, MembershipProvider.Current.SelectedCard.CardPictureBytes.Length);
                if (bm != null)
                {
                    imgMISelectedCard.SetImageBitmap(bm);
                    //bm.Recycle();
                    //bm = null;
                }
            }
            else
            {
                if (MembershipProvider.Current.SelectedCard.PictureResourceID != 0)
                    imgMISelectedCard.SetImageResource(MembershipProvider.Current.SelectedCard.PictureResourceID);
                else
                    ImageAdapter.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (res) =>
                    {
                        if (res != null)
                        {
                            var bm = BitmapFactory.DecodeByteArray(res, 0, res.Length);
                            if (bm != null)
                            {
                                this.RunOnUiThread(() =>
                                {
                                    imgMISelectedCard.SetImageBitmap(bm);
                                });
                            }
                        }
                    });
            }
        }
    }
}