using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class LoungeTableView : UITableView
    {
        public UIEdgeInsets BorderWidth { get; set; }
        public UIColor BorderColor { get; set; }

        public LoungeTableView(IntPtr handle) : base(handle)
		{
            BorderColor = new UIColor(new CGColor(0, 0, 0));
            BorderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);
        }

		public override void Draw(CGRect rect)
		{
			base.Draw(rect);

			var context = UIGraphics.GetCurrentContext();

			UIDrawBorder.DrawLeftBorder(context, rect, this.Frame, BorderColor, BorderWidth);
			UIDrawBorder.DrawRightBorder(context, rect, this.Frame, BorderColor, BorderWidth);
		}
    }
}