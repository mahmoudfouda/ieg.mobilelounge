﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class AgentDevSettingTabLogsView : UIView
    {

        public UIEdgeInsets borderWidth = new UIEdgeInsets(0.5f, 0.5f, 0.5f, 0.5f);

        public UIColor borderColor = UIColor.LightGray;
        public AgentDevSettingTabLogsView (IntPtr handle) : base (handle)
        {
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, borderColor, borderWidth);
        }
    }
}