﻿using CoreGraphics;
using System;
using UIKit;

namespace AIMS_IOS
{
    public partial class AgentDevSettingTabThresholdView : UIView
    {
        public UIEdgeInsets borderWidth = new UIEdgeInsets(0.5f, 0.5f, 0.5f, 0.5f);

        public UIColor borderColor = UIColor.LightGray;

        public AgentDevSettingTabThresholdView(IntPtr handle) : base(handle)
        {
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, borderColor, borderWidth);
        }
    }
}