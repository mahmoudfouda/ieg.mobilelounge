﻿using System;
using System.Drawing;

using CoreGraphics;
using Foundation;
using UIKit;

namespace AIMS_IOS
{
    [Register("AgentDevSettingTabUsersView")]
    public class AgentDevSettingTabUsersView : UIView
    {
        public UIEdgeInsets borderWidth = new UIEdgeInsets(0.5f, 0.5f, 0.5f, 0.5f);

        public UIColor borderColor = UIColor.LightGray;
        public AgentDevSettingTabUsersView(IntPtr handle) : base(handle)
        {
        }
        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, borderColor, borderWidth);
        }
    }
}