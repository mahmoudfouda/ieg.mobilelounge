﻿using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace AIMS_IOS
{
    public partial class AgentTabBottomView : UIView
    {
        public UIEdgeInsets borderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);
        public UIColor borderColor = UIColor.Black;

        public AgentTabBottomView(IntPtr handle) : base(handle)
        {
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            var context = UIGraphics.GetCurrentContext();
            UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, borderColor, borderWidth);
        }
    }
}