using Foundation;
using System;
using UIKit;
using CoreGraphics;
using System.Linq;

namespace AIMS_IOS
{
    public partial class AgentHomeTabLanguageView : UIView
	{
		public UIEdgeInsets borderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

		public UIColor borderColor = UIColor.Black;

        public AgentHomeTabLanguageView (IntPtr handle) : base (handle)
        {
        }
        
        /*
        public override UIView HitTest(CGPoint point, UIEvent uievent)
        {
            //return base.HitTest(point, uievent);
            if(!this.ClipsToBounds && !this.Hidden && this.Alpha > 0f)
            {
                UIView[] reversedArray = new UIView[this.Subviews.Length];

                for(int i = 0; i < this.Subviews.Length; i++)
                {
                    reversedArray[i] = this.Subviews[this.Subviews.Length - 1];
                }

                foreach(UIView subview in reversedArray)
                {
                    CGPoint subPoint = subview.ConvertPointFromView(point, this);
                    UIView result = subview.HitTest(subPoint, uievent);
                    if(result != null)
                    {
                        return result;
                    }
                }
            }
            return null;
        }*/


        public override void Draw(CGRect rect)
		{
			base.Draw(rect);

			var context = UIGraphics.GetCurrentContext();

			//UIDrawBorder.DrawLeftBorder(context, rect, this.Frame, borderColor, borderWidth);
			//UIDrawBorder.DrawRightBorder(context, rect, this.Frame, borderColor, borderWidth);
			//UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, borderColor, borderWidth);
		}
	}
    
}