using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class AgentHomeTabLoungeView : UIView
    {
		public UIEdgeInsets borderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

		public UIColor borderColor = UIColor.Black;

        public AgentHomeTabLoungeView (IntPtr handle) : base (handle)
        {
        }

		public override void Draw(CGRect rect)
		{
			base.Draw(rect);

			var context = UIGraphics.GetCurrentContext();

            //UIDrawBorder.DrawLeftBorder(context, rect, this.Frame, borderColor, borderWidth);
            //UIDrawBorder.DrawRightBorder(context, rect, this.Frame, borderColor, borderWidth);
            //UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, borderColor, borderWidth);
            //UIDrawBorder.DrawTopSeparator(context, rect, UIScreen.MainScreen.Bounds.Width, borderColor, borderWidth);
        }
    }
}