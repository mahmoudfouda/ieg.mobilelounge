using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class DevLogTextView : UITextView
    {
        public DevLogTextView (IntPtr handle) : base (handle)
        {
        }

		public override bool PointInside(CGPoint point, UIEvent uievent)
		{
			return false;
		}
    }
}