﻿using System;
using System.Drawing;
using CoreFoundation;
using UIKit;
using Foundation;
using CoreGraphics;
using MapKit;
using AIMS;
using CoreLocation;
using AIMS_IOS.View_designer.Template;

namespace AIMS_IOS
{
    public partial class MapViewController : UIViewController
    {
        private MapViewDelegate mapViewDelegate;
        private UISlider slider;
        private CLLocationCoordinate2D mapCenter;
        private MKCoordinateRegion mapRegion;

        public MapViewController()
        {
        }

        public MapViewController(IntPtr handle) : base(handle)
        {
        }

        public MKMapView Map { get; private set; }

        public bool IsShown { get; private set; } = false;
        public nfloat ScaleFactor { get; private set; }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            this.IsShown = true;
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            this.IsShown = false;
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Map = new MKMapView();
            //Map.Frame = new CGRect(this.View.Frame.X, 20f, this.View.Frame.Width, this.View.Frame.Height / 1.5);
            Map.MapType = MKMapType.Standard;
            Map.ZoomEnabled = true;

            this.View.AddSubview(Map);

            slider = new UISlider();
            slider.MaxValue = 0.99f;
            slider.Value = 0;

            slider.ValueChanged += Slider_ValueChanged;
            this.View.AddSubview(slider);

            Map.Layer.BorderColor = UIColor.Gray.CGColor;
            Map.Layer.BorderWidth = 2.0f;

            UpdateLayout();

            AddMapTypes();

            MapSetting();

            slider.Value = 0.5f;

            backButton.TouchUpInside += (ss, ee) => { Close(); };
            backButtonTile.TouchUpInside += (ss, ee) => { Close(); };
        }

        public void Close()
        {
            if (AppDelegate.MapViewController.IsShown)
            {
                this.DismissViewController(true, null);
            }
        }

        private void AddMapTypes()
        {
            int typesWidth = (int)(ScaleFactor * 260);
            int typesHeight = (int)(ScaleFactor * 30);
            int distanceFromBottom = (int)(ScaleFactor * 5);

            var mapTypes = new UISegmentedControl(new CGRect((Map.Bounds.Width - typesWidth) / 2, Map.Bounds.Height - distanceFromBottom, typesWidth, typesHeight));
            mapTypes.BackgroundColor = UIColor.White;
            mapTypes.Layer.CornerRadius = 5;
            mapTypes.ClipsToBounds = true;
            mapTypes.InsertSegment("Road", 0, false);
            mapTypes.InsertSegment("Satellite", 1, false);
            mapTypes.InsertSegment("Hybrid", 2, false);
            mapTypes.SelectedSegment = 0; // Road is the default
            mapTypes.AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin;
            mapTypes.ValueChanged += (s, e) =>
            {
                switch (mapTypes.SelectedSegment)
                {
                    case 0:
                        Map.MapType = MKMapType.Standard;
                        break;
                    case 1:
                        Map.MapType = MKMapType.Satellite;
                        break;
                    case 2:
                        Map.MapType = MKMapType.Hybrid;
                        break;
                }
            };

            View.AddSubview(mapTypes);
        }

        public void UpdateLayout()
        {
            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            nfloat statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            ScaleFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                ScaleFactor = height / (667f * (4f / 3f));

            CGRect frame = UIScreen.MainScreen.Bounds;

            topView.Frame = new CGRect(0f, statusBarH, frame.Width, 44f * ScaleFactor);

            nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.6f;

            string regularFontName = "Helvetica Neue";
            nfloat xxLargeTexts = 26f * ScaleFactor, xLargeTexts = 24f * ScaleFactor, largeTexts = 22f * ScaleFactor, medium = 17f * ScaleFactor, smallTexts = 15f * ScaleFactor, xSmallTexts = 13f * ScaleFactor;
            var mediumFont = UIFont.FromName(regularFontName, medium);

            backButton.Font = backButtonTile.Font = mediumFont;


            backButton.Frame = new CGRect((8f * ScaleFactor), 0f, 10f * ScaleFactor, topView.Frame.Height);
            backButtonTile.ContentMode = UIViewContentMode.ScaleAspectFit;
            backButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            backButtonTile.Frame = new CGRect(backButton.Frame.Right + (5f * ScaleFactor), 0f,
                40f * ScaleFactor, topView.Frame.Height);


            Map.Frame = new CGRect(this.View.Frame.X, (this.topView.Frame.Height + 20 * ScaleFactor), this.View.Frame.Width, this.View.Frame.Height / 1.5);
            slider.Frame = new CGRect(this.View.Frame.X, Map.Frame.Height + this.topView.Frame.Height + (20f * ScaleFactor), this.View.Frame.Width, (ScaleFactor * 50f));

        }

        private void Slider_ValueChanged(object sender, EventArgs e)
        {
            var delta = (1-slider.Value) / 69.0;
            var currentRegion = Map.Region;
            currentRegion.Span = new MKCoordinateSpan(delta, delta);

            CLLocation newLocation = new CLLocation(mapCenter.Latitude + currentRegion.Span.LatitudeDelta, mapCenter.Longitude + currentRegion.Span.LongitudeDelta);
            CLLocation centerLocation = new CLLocation(mapCenter.Latitude, mapCenter.Longitude);
            var distance = centerLocation.DistanceFrom(newLocation);

            mapRegion = MKCoordinateRegion.FromDistance(mapCenter, distance, distance);
            Map.CenterCoordinate = mapCenter;
            Map.Region = mapRegion;
        }


        private void MapSetting()
        {
            if (DefaultMobileApplication.Current == null)
                return;

            var positions = new System.Collections.Generic.List<AIMS.Models.ReadOnlyPosition>();
            #region Filling up positions with white fences
            var fenceList = DefaultMobileApplication.Current.Fences;
            if (fenceList != null)
            {
                foreach (var fence in fenceList)
                {
                    if (fence.Positions != null)
                        positions.AddRange(fence.Positions);
                }

            }
            #endregion


            double lat = DefaultMobileApplication.Current.Position.Latitude;
            double lon = DefaultMobileApplication.Current.Position.Latitude;

            #region IEG Office position (TODO: Remove)
#if DEBUG
            lat = 45.511021;
            lon = -73.668443; 
#endif
            #endregion

            mapCenter = new CLLocationCoordinate2D(lat, lon);
            mapRegion = MKCoordinateRegion.FromDistance(mapCenter, 2000, 2000);
            Map.CenterCoordinate = mapCenter;
            Map.Region = mapRegion;

            // add an annotation
            Map.AddAnnotation(new MKPointAnnotation
            {
                Title = "MyAnnotation",
                Coordinate = new CLLocationCoordinate2D(lat, lon)
            });


            // set the map delegate
            mapViewDelegate = new MapViewDelegate();
            Map.Delegate = mapViewDelegate;

            //show these positions in circles positions[i].(Latitude, Longitude, Radius)
            foreach (var position in positions)
            {
                Map.AddOverlay(MKCircle.Circle(new CLLocationCoordinate2D(position.Latitude, position.Longitude), position.Radius));
            }

        }
    }

    public class CustomAnnotation : MKAnnotation
    {
        private CLLocationCoordinate2D coordinate;

        private readonly string title;

        public CustomAnnotation(string title, CLLocationCoordinate2D coordinate)
        {
            this.title = title;
            this.coordinate = coordinate;
        }

        public override string Title => title;

        public override CLLocationCoordinate2D Coordinate => coordinate;
    }
}