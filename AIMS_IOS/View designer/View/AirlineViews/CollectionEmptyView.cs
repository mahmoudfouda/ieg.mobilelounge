﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using CoreGraphics;
using Foundation;

namespace AIMS_IOS
{
    class CollectionEmptyView : UICollectionReusableView
    {
        [Export("initWithFrame:")]
        public CollectionEmptyView(System.Drawing.RectangleF frame) : base(frame)
        {
            this.BackgroundColor = UIColor.Clear;
            this.Frame = new CGRect(0f, 0f, 0f, 0f);
        }
    }
}
