﻿using Foundation;
using System;
using UIKit;
using System.Threading.Tasks;

namespace AIMS_IOS
{
    public partial class AirlineCollectionView : UICollectionView
    {


        public AirlineCollectionView (IntPtr handle) : base (handle)
        {
        }

        public AirlineCollectionView (CoreGraphics.CGRect rect, UICollectionViewFlowLayout flow) : base(rect, flow)
        { }


    }
}