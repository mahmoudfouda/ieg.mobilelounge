﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using CoreGraphics;
using Foundation;

namespace AIMS_IOS
{
    class CollectionSectionView : UICollectionReusableView
    {

        [Export("initWithFrame:")]
        public CollectionSectionView(System.Drawing.RectangleF frame) : base(frame)
        {
            this.BackgroundColor = UIColor.White;
        }

    }
}
