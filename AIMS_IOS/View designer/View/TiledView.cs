﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class TiledView : UIView
    {
        public static nfloat ScaleFactor { get; set; } = 1f;

        UIImage bgImage;
        UIView bgView;
        public TiledView (IntPtr handle) : base (handle)
        {
            //bgImage = UIImage.FromFile("tile_gray.gif");
            bgImage = UIImage.FromFile("tile_gray_transparent.png");
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            this.AddSubview(this.bgView);
            this.SendSubviewToBack(this.bgView);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            if (bgView == null)
            {
                BackgroundColor = UIColor.Clear;
                bgView = new UIView();
                bgView.BackgroundColor = UIColor.FromPatternImage(bgImage);
                Layer.BorderColor = bgView.Layer.BorderColor = new CGColor(0,0);
                Layer.BorderWidth = bgView.Layer.BorderWidth = 0f;
                Layer.CornerRadius = bgView.Layer.CornerRadius = 3f * ScaleFactor;
            }

            bgView.Frame = this.Bounds;
        }
    }
}