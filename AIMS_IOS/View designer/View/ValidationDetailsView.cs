﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class ValidationDetailsView : UIView
    {
        public UIEdgeInsets BorderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

        public UIColor BorderColor = UIColor.Black;

        public ValidationDetailsView (IntPtr handle) : base (handle)
        {
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            UIDrawBorder.DrawBorders(context, rect, this.Frame, BorderColor, BorderWidth);
           
        }
    }
}