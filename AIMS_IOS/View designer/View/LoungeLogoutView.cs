﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class LoungeLogoutView : UIView
    {
        public UIEdgeInsets BorderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

        public UIColor BorderColor;// = new UIColor(CoreImage.CIColor.BlackColor);


        public LoungeLogoutView (IntPtr handle) : base (handle)
        {
            BorderColor = new UIColor(new CoreGraphics.CGColor(0, 0, 0));
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            UIDrawBorder.DrawBorders(context, rect, this.Frame, BorderColor, BorderWidth);
        }
    }
}