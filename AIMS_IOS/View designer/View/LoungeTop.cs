using Foundation;
using System;
using UIKit;
using CoreGraphics;


namespace AIMS_IOS
{
    public partial class LoungeTop : UIView
    {
		public nfloat BorderWidthAll { get; set; }

		public nfloat BorderColorAll { get; set; }

		public UIEdgeInsets BorderWidth { get; set; }

		public UIColor BorderColor { get; set; }

        public LoungeTop (IntPtr handle) : base (handle)
        {
        }

		public override void Draw(CGRect rect)
		{
			base.Draw(rect);

			BorderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

			//BorderColor = new UIColor(CoreImage.CIColor.BlackColor);
            BorderColor = new UIColor(new CoreGraphics.CGColor(0, 0, 0));

            var context = UIGraphics.GetCurrentContext();

			UIDrawBorder.DrawBorders(context, rect, this.Frame, BorderColor, BorderWidth);
		}
    }
}