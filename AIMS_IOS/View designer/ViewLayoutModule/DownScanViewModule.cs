﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public static class DownScanViewModule
    {
        public static void UpdateScanViewLayout(CGRect controllerFrame, UIView scanView, UIButton leftDown, UIButton rightDown, UIButton scanButton, UIImageView gradiant, float leftMargin)
        {
            nfloat screenHeight = controllerFrame.Width > controllerFrame.Height ? controllerFrame.Height : controllerFrame.Width;

            nfloat height = screenHeight * 0.075f;

            nfloat height13 = 0;
            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                height13 = scanView.SafeAreaInsets.Bottom/2;

            //minimum
            if (height < 50f)
                height = 50f;

            scanView.Frame = new CGRect(leftMargin, controllerFrame.Height - height - height13,
                controllerFrame.Width - leftMargin, height);

            leftDown.Frame = new CGRect(5f, 5f, height - 10f, height - 10f);
            rightDown.Frame = new CGRect(scanView.Frame.Width - leftDown.Frame.Width - 5f, 5f,
                leftDown.Frame.Width, leftDown.Frame.Height);

            scanButton.Frame = new CGRect(leftDown.Frame.X + leftDown.Frame.Width + 10f, 5f,
                scanView.Frame.Width - (leftDown.Frame.X + leftDown.Frame.Width + 10f)*2f ,
                leftDown.Frame.Height);
            scanButton.ContentMode = UIViewContentMode.ScaleAspectFit;
            scanButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

            if (gradiant != null)
                gradiant.Frame = new CGRect(0f, 0f, scanView.Frame.Width, scanView.Frame.Height); ;

            scanView.AutoresizingMask = UIViewAutoresizing.All;
        }

        
    }
}
