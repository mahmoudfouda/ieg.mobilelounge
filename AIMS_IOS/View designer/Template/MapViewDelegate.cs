﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using MapKit;
using UIKit;

namespace AIMS_IOS.View_designer.Template
{
    public class MapViewDelegate : MKMapViewDelegate
    {
        private const string PinId = "PinAnnotation";

        public override MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView result = null;

            if (annotation is MKUserLocation)
                return result;

            var annotationView = mapView.DequeueReusableAnnotation(PinId) as MKPinAnnotationView ?? new MKPinAnnotationView(annotation, PinId);


            if (annotation is CustomAnnotation)
                annotationView.PinTintColor = UIColor.Red;
            else
                annotationView.PinTintColor = UIColor.Blue;

            annotationView.CanShowCallout = true;
            result = annotationView;

            return result;
        }

        public override void CalloutAccessoryControlTapped(MKMapView mapView, MKAnnotationView view, UIControl control)
        {
            if (view.Annotation is CustomAnnotation monkeyAnnotation)
            {
                var alert = UIAlertController.Create("My Annotation", monkeyAnnotation.Title, UIAlertControllerStyle.Alert);
                alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                UIApplication.SharedApplication.Windows[0].RootViewController.PresentViewController(alert, true, null);
            }
        }

        public override MKOverlayRenderer OverlayRenderer(MKMapView mapView, IMKOverlay overlay)
        {
            return new MKCircleRenderer(overlay as MKCircle)
            {
                FillColor = UIColor.Green,
                Alpha = 0.4f
            };
        }
    }
}