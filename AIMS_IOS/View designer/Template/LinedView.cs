using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public class LinedView : UIView
    {
        public nfloat[] LinesY { get; set; }

        private UIEdgeInsets borderWidth = new UIEdgeInsets(.5f, .5f, .5f, .5f);

        private UIColor borderColor;

        public LinedView() : base()
        {
            this.LinesY = new nfloat[0];
            borderColor = UIColor.Black;
            BackgroundColor = UIColor.Clear;

            UserInteractionEnabled = false;
        }

        public LinedView(UIColor separatorLinesColor) : base()
        {
            this.LinesY = new nfloat[0];
            borderColor = separatorLinesColor;
            BackgroundColor = UIColor.Clear;

            UserInteractionEnabled = false;
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            for(int i = 0; i < LinesY.Length; i++)
            {
                context.SetFillColor(borderColor.CGColor);
                context.FillRect(new CGRect(0f, LinesY[i], UIScreen.MainScreen.Bounds.Width,
                    borderWidth.Top));
            }
        }

    }
}