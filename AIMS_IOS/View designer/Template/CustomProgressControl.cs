﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading.Tasks;
using AIMS;
using AIMS.Models;
using CoreGraphics;
using Foundation;
using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace AIMS_IOS
{
    [Register("CustomProgressControl")]
    [DesignTimeVisible(true)]
    [Category("New Controls")]
    public class CustomProgressControl : UIView
    {
        private UILabel lblPerc;
        private UIImageView viewImageWithe;
        private UIImageView viewImage;
        private nfloat imgArrowSize;
        private UIImageView viewImageArrow;
        private nfloat scaleFactor;
        private nfloat lblHeight;
        private UILabel lblprocessing;
        private double _lastPercentage;
        private UIProgressView progressView;

        public CustomProgressControl(CGRect frame) : base(frame)
        {
        }

        protected CustomProgressControl(NSObjectFlag t) : base(t)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
        }

        public CustomProgressControl(IntPtr handle) : base(handle)
        {
        }

        public override CGRect Frame
        {
            get => base.Frame;
            set
            {
                base.Frame = value;
                Initialize();
            }
        }

        public static nfloat MinSize
        {
            get
            {
                var img = new UIImage("BarArrow_white.png");

                nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

                var scaleFactor = height / 667f;
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                    scaleFactor *= .75f;

                var imgSize = (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad ? img.Size.Height * 0.15f : img.Size.Height * 0.25f) * scaleFactor;

                imgSize += (5 * scaleFactor);

                return imgSize;
            }
        }

        public bool IsAutoUpdateProgress { get; set; } = false;

        public bool IsParentDark { get; set; } = false;

        public void Initialize()
        {
            this.BackgroundColor = UIColor.Clear;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
               UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            scaleFactor = height / 667f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                scaleFactor *= .75f;

            height = 44f / 650f * height;
            height = height < 44f ? 44f : height;

            nfloat actualHeight = GetRealHeight();

            progressView = new UIProgressView(new CGRect(0, this.Frame.Height / 2, this.Frame.Width, height));

            var X = 1.0f;
            var Y = 10.0f; // This changes the height

            if (UIDevice.CurrentDevice.CheckSystemVersion(14, 0))
                Y = (float)(4 * scaleFactor);


            CGAffineTransform transform = CGAffineTransform.MakeScale(X, Y);
            progressView.Transform = transform;

            lblHeight = 15f * scaleFactor;

            //lblprocessing = new UILabel(new CGRect((progressView.Frame.Width/2) - (14f* scaleFactor), (this.progressView.Frame.Height / 2) - (lblHeight / 2) + 5f, 28f * scaleFactor, lblHeight));
            lblprocessing = new UILabel(new CGRect((progressView.Frame.Width / 2) - (14f * scaleFactor), 0, 28f * scaleFactor, this.Frame.Height));
            lblprocessing.Text = "0%";
            lblprocessing.TextColor = UIColor.White;
            lblprocessing.Lines = 0;
            lblprocessing.TextAlignment = UITextAlignment.Center;
            

            progressView.BackgroundColor = Xamarin.Forms.Color.FromHex("#CCCCCC").ToUIColor();


            /* 
             lblPerc = new UILabel(new CGRect(0f, (this.Frame.Height / 2) - (lblHeight / 2), 28f * scaleFactor, lblHeight))
             {
                 Text = "0%",
             };
             lblPerc.TextColor = IsParentDark ? UIColor.White : UIColor.Gray;
             lblPerc.Hidden = true;
             lblPerc.TextAlignment = UITextAlignment.Center;

             var image = new UIImage("progressBarBG_flat.png");
             image = Resize(image, new CGSize(this.Frame.Width - lblPerc.Frame.Right, actualHeight));


             viewImage = new UIImageView(new CGRect(lblPerc.Frame.Right, this.Frame.Height / 2, 0, actualHeight))
             {
                 //Image = new UIImage("progressBarBG_flat.png")   //progressBar_3DSample.jpg
             };
             viewImage.Image = image;
             viewImage.Layer.BorderColor = UIColor.Clear.CGColor;
             viewImage.ContentMode = UIViewContentMode.ScaleAspectFill;
             viewImage.ClipsToBounds = true;


             //viewImageWithe = new UIImageView(new CGRect(this.Frame.Width, 0f, 0f, 25f));
             viewImageWithe = new UIImageView(new CGRect(lblPerc.Frame.Right, this.Frame.Height / 2, this.Frame.Width - lblPerc.Frame.Right, actualHeight))
             {
                 Image = new UIImage("progressBarNoFrame_WithBG.png") //progressBarFrame_WithBG
             };
             viewImageWithe.Layer.BorderColor = UIColor.Clear.CGColor;
             viewImageWithe.Alpha = 0.6f;

             var imgArrow = IsParentDark ? new UIImage("BarArrow_white.png") : new UIImage("BarArrow.png");

             imgArrowSize = (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad ? imgArrow.Size.Height * 0.15f : imgArrow.Size.Height * 0.25f) * scaleFactor;

             viewImageArrow = new UIImageView(new CGRect(lblPerc.Frame.Right - (7 * scaleFactor), viewImage.Frame.Top - imgArrowSize, imgArrowSize, imgArrowSize));
             //{
             //    Image = IsParentDark ? new UIImage("BarArrow_white.png") : new UIImage("BarArrow.png"),
             //};
             viewImageArrow.Image = imgArrow;
             viewImageArrow.Hidden = true;
             viewImageArrow.Alpha = 0.6f; */

            SetFonts();

            foreach (var view in Subviews)
            {
                view.RemoveFromSuperview();
            }

            AddSubview(progressView);
            AddSubview(lblprocessing);

            //AddSubview(viewImage);
            //AddSubview(viewImageWithe);
            //AddSubview(viewImageArrow);
            //AddSubview(lblPerc);

            if (IsAutoUpdateProgress)
            {
                AppDelegate.OnOccupancyUpdateTick -= AppOccupancyUpdateTick;
                AppDelegate.OnOccupancyUpdateTick += AppOccupancyUpdateTick;
            }
        }

        private UIImage Resize(UIImage source, CGSize newSize)
        {
            try
            {
                UIGraphics.BeginImageContext(newSize);
                CGContext context = UIGraphics.GetCurrentContext();
                context.InterpolationQuality = CGInterpolationQuality.None;


                context.TranslateCTM(0, newSize.Height);
                context.ScaleCTM(1f, -1f);

                context.DrawImage(new RectangleF(0, 0, (float)newSize.Width, (float)newSize.Height), source.CGImage);

                var scaledImage = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();

                return scaledImage;
            }
            catch
            {
                return source;
            }
        }

        private nfloat GetRealHeight()
        {
            return this.Frame.Height > 5 ? 5 : this.Frame.Height;
        }

        public void SetFonts()
        {
            UIFont smallFont;
            string regularFontName = "Helvetica Neue";
            string boldFontName = "Helvetica-Bold";

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                smallFont = UIFont.FromName(boldFontName, 10f * scaleFactor);
            else
                smallFont = UIFont.FromName(boldFontName, 12f * scaleFactor);

            lblprocessing.Font = smallFont;
            //lblPerc.Font = smallFont;
        }

        private void AppOccupancyUpdateTick(object sender, EventArgs e)
        {
            //LoadProgressData();

            BeginInvokeOnMainThread(() =>
            {
                try
                {
                    UpdateProgress(MembershipProvider.Current.SelectedLounge.LastPercentage);
                }
                catch { }
            });
        }

        internal void LoadProgressData()
        {
          //  if (this.Hidden)
          //      return;

            if (MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy != null)
            {
                if ((double)MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.Percentage == _lastPercentage)
                    UpdateProgress(_lastPercentage);
            }

            try
            {
                var selectedLounge = MembershipProvider.Current.SelectedLounge;

                AppManager.GetLoungeOccupancy(selectedLounge, (data) =>
                {
                    BeginInvokeOnMainThread(() =>
                    {
                        if (data == null)
                            return;

                        MembershipProvider.Current.SelectedLounge.LastPercentage = (int)Math.Ceiling(((LoungeOccupancy)data).Percentage);

                        UpdateProgress((double)((LoungeOccupancy)data).Percentage);
                    });
                });
            }
            catch
            {
            }
        }

        public void UpdateProgress(double percentage)
        {
#if DEBUG
           // percentage = DateTime.Now.Minute;
#endif


            this._lastPercentage = percentage;
            UIView.Animate(1, 0, UIViewAnimationOptions.CurveEaseIn, () =>
            {
                var width = this.Frame.Width;

                var height = GetRealHeight();

                //progressView.Progress = (float)(percentage/100);

                UIView.Animate(2500, () =>
                {
                    progressView.SetProgress((float)(percentage / 100), true);
                });

                lblprocessing.Text = percentage.ToString("N0") + "%";

                progressView.ProgressTintColor =
                   percentage >= 90 ? Xamarin.Forms.Color.FromHex("#eb5656").ToUIColor() :
                   percentage >= 50 ? Xamarin.Forms.Color.FromHex("#f2c84b").ToUIColor() :
                                      Xamarin.Forms.Color.FromHex("#6ecf97").ToUIColor();


                /*lblPerc.Frame = new CGRect(0f, (this.Frame.Height / 2) - (lblHeight / 2), 28f * scaleFactor, lblHeight);

                width -= 28f * scaleFactor;

                double progress = percentage;
                double valueprogress = (width * progress / 100);
                lblPerc.Text = progress.ToString("N0") + "%";

                viewImage.Frame = new CGRect(lblPerc.Frame.Right, this.Frame.Height / 2, valueprogress, height);

                viewImageWithe.Frame = new CGRect(lblPerc.Frame.Right + valueprogress, this.Frame.Height / 2, (width - valueprogress), height);

                viewImageArrow.Frame = new CGRect(lblPerc.Frame.Right + valueprogress - 7f, viewImage.Frame.Top - imgArrowSize, imgArrowSize, imgArrowSize);
                viewImageArrow.Alpha = 0.6f;

                if (viewImageArrow.Hidden)
                    viewImageArrow.Hidden = false;

                if (lblPerc.Hidden)
                    lblPerc.Hidden = false; */

                this.SetNeedsLayout();

            }, null);

        }

        public void RefreshProgress()
        {
            if (MembershipProvider.Current?.SelectedLounge?.LastLoungeOccupancy != null)
                UpdateProgress((double)MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.Percentage);
            else
                UpdateProgress(_lastPercentage);
        }
    }
}