using Foundation;
using System;
using UIKit;
using CoreGraphics;


namespace AIMS_IOS
{
    public class UIBorderView : UIView
    {

        public UIEdgeInsets borderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

        public UIColor borderColor = UIColor.Black;

        public UIBorderView(IntPtr handle) : base (handle)
        {
        }

        public UIBorderView(CGRect rect) : base(rect)
        {
            this.Frame = rect;

        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            UIDrawBorder.DrawBorders(context, rect, this.Frame, borderColor, borderWidth);
        }

    }
}