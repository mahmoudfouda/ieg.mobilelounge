﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using UIKit;

namespace AIMS_IOS
{
    public class TableSourceButtonsStatistics : UICollectionViewSource
    {
        private List<string> tableItems;
        protected NSString cellIdentifier = (NSString)"StatisticButtonCell";
        private UIColor selectedColor;
        public event TableItemSelectedHandler TableItemSelected;
        private nfloat scaleFactor;

        public TableSourceButtonsStatistics(List<string> items, UIColor selectedColor, nfloat scaleFactor)
        {
            tableItems = items;
            this.selectedColor = selectedColor;
            this.scaleFactor = scaleFactor;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return tableItems.Count;
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.DequeueReusableCell(cellIdentifier, indexPath) as StatisticButtonCell;

            cell.SetSelectedColor(selectedColor);
            cell.UpdateLayout(indexPath.Row, scaleFactor);

            return cell;
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            TableItemSelected?.Invoke(collectionView, indexPath);
        }

        public delegate void TableItemSelectedHandler(UICollectionView collectionView, NSIndexPath indexPath);

    }
}