﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using AIMS.Models;
using AIMS;
using System.Threading;
using System.Threading.Tasks;

namespace AIMS_IOS
{
    public delegate void AirportSelectionHandler(Airport airport);
    public delegate void AirportsListHeightChangedHandler(nfloat newHeight);

    public class AirportsTableSource : UITableViewSource
    {
        public const int MAX_SHOWING_AUTOCOMPLETE_ROWS = 8;
        public const int AUTOCOMPLETE_ROW_HEIGHT = 44;

        List<Airport> items = new List<Airport>();
        NSString cellIdentifier = (NSString)"airportCell";
        AIMSViewController parentViewController;
        UITableView tableView;
        nfloat scaleFactor = 1f;
        CancellationTokenSource throttleCts = new CancellationTokenSource();

        public event AirportSelectionHandler OnAirportSelected;
        public event AirportsListHeightChangedHandler OnAirportsListHeightChanged;

        public void SearchAirports(string keyword)
        {
            Interlocked.Exchange(ref throttleCts, new CancellationTokenSource()).Cancel();

            Task.Delay(TimeSpan.FromMilliseconds(1000), this.throttleCts.Token)
                .ContinueWith(delegate {
                    this.PerformSearch(keyword);
                },
                CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void PerformSearch(string keyword)
        {
            if (tableView == null) return;

            InvokeOnMainThread(() =>
            {
                tableView.Hidden = true;
            });
            using (var ar = new AirportsRepository())
            {
                ar.OnRepositoryChanged += (data) =>
                {
                    InvokeOnMainThread(() =>
                    {
                        items.Clear();
                        items = (List<Airport>)data;
                        if (items != null && items.Count > 0)
                        {
                            tableView.ReloadData();
                            tableView.Hidden = false;

                            try
                            {
                                int maxRows = Math.Min(items.Count, MAX_SHOWING_AUTOCOMPLETE_ROWS);
                                nfloat height = (AUTOCOMPLETE_ROW_HEIGHT * maxRows) * parentViewController.ScaleFactor;
                                if (items.Count >= MAX_SHOWING_AUTOCOMPLETE_ROWS)
                                    height -= ((AUTOCOMPLETE_ROW_HEIGHT * parentViewController.ScaleFactor) / 2f);
                                tableView.Frame = new CoreGraphics.CGRect(tableView.Frame.Location, new CoreGraphics.CGSize(tableView.Frame.Width, height));

                                OnAirportsListHeightChanged?.Invoke(height);
                            }
                            catch { }
                        }
                        else tableView.Hidden = true;
                    });
                };
                ar.SearchAirports(keyword, true);
            }
        }

        public AirportsTableSource(AIMSViewController owner, UITableView tableView) : base()
        {
            items = new List<Airport>();
            parentViewController = owner;
            this.tableView = tableView;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(cellIdentifier) as AirportCell;

            cell.UpdateLayout(tableView.RowHeight, parentViewController);
            cell.UpdateCell(items[indexPath.Row].IATA_Code, items[indexPath.Row].Name);

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return items.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            if (items == null || items.Count == 0 || items.Count <= indexPath.Row)
            {
                tableView.Hidden = true;
                return;
            }

            OnAirportSelected?.Invoke(items[indexPath.Row]);
            
            tableView.Hidden = true;
            
            tableView.DeselectRow(indexPath, true);
        }
    }
}