﻿using System;
using System.Collections.Generic;

using Foundation;
using UIKit;
using AIMS_IOS.Utility;
using AIMS;
using AIMS.Models;

namespace AIMS_IOS
{
    public class CardSource : UICollectionViewSource
    {
        protected List<CardCellData> items;
        protected string cellIdentifier = "cardsCell";

        AirLineViewController airLineViewController;

        public CardSource(List<CardCellData> items, AirLineViewController airLineViewController)
        {
            this.items = items;
            this.airLineViewController = airLineViewController;

        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)//TODO: GetView(int position, View convertView, ViewGroup parent)
        {
            #region Old Codes
            //var cell = (CardsCell)collectionView.DequeueReusableCell(cellIdentifier, indexPath);

            ////cell.img.Image = items[indexPath.Row].image;
            ////cell.footerLabel.Text = items[indexPath.Row].footer;

            //if (items[indexPath.Row].image == null)
            //{
            //    items[indexPath.Row].image = UIImage.FromFile("Error/not_available_pic_off");
            //    ImageAdapter.LoadImage(items[indexPath.Row].Card.ImageHandle, (imageBytes) =>
            //    {
            //        items[indexPath.Row].Card.CardPictureBytes = imageBytes;
            //        CardsRepository.SaveCardImage(items[indexPath.Row].Card);//Saved to local DB

            //        InvokeOnMainThread(() =>
            //        {
            //            cell.updateCell(imageBytes.ToImage(), items[indexPath.Row].footer, airLineViewController.cardCellSize);
            //        });
            //    });
            //}

            //cell.updateCell(items[indexPath.Row].image, items[indexPath.Row].footer, airLineViewController.cardCellSize);

            //return cell;
            #endregion

            var cell = (CardsCell)collectionView.DequeueReusableCell(cellIdentifier, indexPath);

            if (items[indexPath.Row].image == null)
            {
                cell.updateCell(UIImage.FromFile("Error/not_available_pic_off"), items[indexPath.Row].footer, airLineViewController.cardCellSize, false);
                ImageAdapter.LoadCardImage(items[indexPath.Row].Card.ImageHandle, cell, items[indexPath.Row], airLineViewController.cardCellSize, (imageBytes, theCell, theItem, theFrameSize) =>
                {
                    var uiImage = imageBytes.ToImage();
                    InvokeOnMainThread(() =>
                    {
                        //theItem.Card.CardPictureBytes = imageBytes;
                        //The problem is whenever we are accessing the [SQLite db] from threads other than the 
                        //main UIKit thread it was throwing exceptions!!!
                        //CardsRepository.SaveCardImage(theItem.Card);//Saved to local DB (in UI thread) already done
                        theItem.image = uiImage;
                        theCell.updateCell(uiImage, theItem.footer, theFrameSize);
                    });
                });
            }
            else cell.updateCell(items[indexPath.Row].image, items[indexPath.Row].footer, airLineViewController.cardCellSize);

            return cell;//We are not returning the true cell!!! (TODO: Siavash)

        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return items.Count;
        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            MembershipProvider.Current.SelectedCard = items[indexPath.Row].Card;
            AppData.selectedCard = items[indexPath.Row];
            AppData.ValidationResult = new PassengerValidation();

            MembershipProvider.Current.SelectedPassenger = new Passenger
            {
                CardID = items[indexPath.Row].Card.ID,
                AirlineId = items[indexPath.Row].Card.AirlineID
            };

            //airLineViewController.displayCardView(indexPath.Row);
            collectionView.DeselectItem(indexPath, true);

            AppDelegate.IsScanningCancelled = true;

            airLineViewController.PresentSinglePageMIForm();

            airLineViewController.disMisKeyB();
        }
    }

    public class CardCellData
    {
        public Card Card { get; private set; }
        public UIImage image;
        public string footer;

        public CardCellData(Card card)
        {
            this.Card = card;
            this.image = this.Card.CardPictureBytes.ToImage();
            this.footer = card.Name;
        }
    }
}