﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace AIMS_IOS
{
    public class TableSourceStatistics : UITableViewSource
    {
        private List<LoungeCellData> TableItems;
        protected NSString cellIdentifier = (NSString)"LoungeCellStatistics";

        public event TableRowSelectedHandler TableRowSelected;

        public TableSourceStatistics(List<LoungeCellData> items)
        {
            TableItems = items;
        }

        public void UpdateItems(List<LoungeCellData> items)
        {
            if (items != null)
                TableItems = items;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return TableItems.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(cellIdentifier) as LoungeCellStatistics;

            cell.Frame = new CoreGraphics.CGRect(0, 0, tableView.Frame.Size.Width, tableView.Frame.Size.Height);

            cell.UpdateLayout();
            cell.UpdateCell(TableItems[indexPath.Row].name, TableItems[indexPath.Row].description, TableItems[indexPath.Row].Progress);

            cell.PreservesSuperviewLayoutMargins = false;
            cell.SeparatorInset = UIEdgeInsets.Zero;

            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            TableRowSelected?.Invoke(TableItems[indexPath.Row].Workstation);
        }
    }

    public delegate void TableRowSelectedHandler(AIMS.Models.Workstation workstation);
}