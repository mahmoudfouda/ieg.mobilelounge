﻿using System;
using System.Collections.Generic;

using Foundation;
using UIKit;
using AIMS.Models;
using System.Drawing;

namespace AIMS_IOS
{
    public class PassengerServiceCollectionViewSource : UICollectionViewSource
    {
        public event EventHandler<PassengerService> OnItemSelected;
        public nfloat ScaleFactor { get; set; } = 1;
        public PassengerServiceCollectionViewSource()
        {
        }

        public List<PassengerService> Rows { get; set; }

        public Single FontSize { get; set; }

        public SizeF ImageViewSize { get; set; }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return Rows.Count;
        }

        public override Boolean ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return true;
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            if (Rows.Count > indexPath.Row)
                if (OnItemSelected != null)
                    OnItemSelected.Invoke(this, Rows[indexPath.Row]);
        }

        public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (PassengerServiceCell)collectionView.CellForItem(indexPath);
            cell.ImageView.Alpha = 0.5f;
        }

        public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (PassengerServiceCell)collectionView.CellForItem(indexPath);
            cell.ImageView.Alpha = 1;

            PassengerService row = Rows[indexPath.Row];
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var theCell = (PassengerServiceCell)collectionView.DequeueReusableCell(PassengerServiceCell.ReusableCellId, indexPath);

            PassengerService row = Rows[indexPath.Row];

            theCell.UpdateCell(row, ScaleFactor);//, FontSize, ImageViewSize);

            return theCell;
        }
    }
}