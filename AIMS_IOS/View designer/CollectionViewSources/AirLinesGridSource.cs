﻿using System;
using System.Collections.Generic;

using Foundation;
using UIKit;
using AIMS;
using CoreGraphics;
using AIMS_IOS.Utility;
using AIMS.Models;

namespace AIMS_IOS
{
    public class AirLinesGridSource : UICollectionViewSource
    {
        protected List<AirlineCellData> itemsS1;
        protected List<AirlineCellData> itemsS2;
        protected NSString cellIdentifier = (NSString)"cell_AirlinesGrid";
        AirLineViewController airLineViewController;
        UICollectionView airLineCollectionView;
        NSString sectionType = new NSString("UICollectionElementKindSectionFooter");
        protected CGPoint cellSize;

        public AirLinesGridSource(List<AirlineCellData> items1, List<AirlineCellData> items2, AirLineViewController airLineViewController, UICollectionView collectionView)
        {
            this.itemsS1 = items2;
            this.itemsS2 = items1;
            this.airLineViewController = airLineViewController;
            this.airLineCollectionView = collectionView;
            cellSize = airLineViewController.cellSize;
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            #region Old Codes
            //var cell = collectionView.DequeueReusableCell(cellIdentifier, indexPath) as AirlineCell;

            //cell.updateLayout(cellSize);

            //if (indexPath.Section == 1)
            //{
            //    if (itemsS1[indexPath.Row].image == null)
            //    {
            //        itemsS1[indexPath.Row].image = UIImage.FromFile("Error/not_available_pic_off");
            //        ImageAdapter.LoadImage(itemsS1[indexPath.Row].Airline.ImageHandle, (imageBytes) =>
            //        {
            //            itemsS1[indexPath.Row].Airline.AirlineLogoBytes = imageBytes;
            //            AirlinesRepository.SaveAirlineImage(itemsS1[indexPath.Row].Airline);//Saved to local DB

            //            InvokeOnMainThread(() =>
            //            {
            //                cell.updateCell(imageBytes.ToImage(), itemsS1[indexPath.Row].name);
            //            });
            //        });
            //    }

            //    cell.updateCell(itemsS1[indexPath.Row].image, itemsS1[indexPath.Row].name);
            //}
            //else
            //{
            //    if (itemsS2[indexPath.Row].image == null)
            //    {
            //        itemsS2[indexPath.Row].image = UIImage.FromFile("Error/not_available_pic_off");
            //        ImageAdapter.LoadImage(itemsS2[indexPath.Row].Airline.ImageHandle, (imageBytes) =>
            //        {
            //            itemsS2[indexPath.Row].Airline.AirlineLogoBytes = imageBytes;
            //            AirlinesRepository.SaveAirlineImage(itemsS2[indexPath.Row].Airline);//Saved to local DB

            //            InvokeOnMainThread(() =>
            //            {
            //                cell.updateCell(imageBytes.ToImage(), itemsS2[indexPath.Row].name);
            //            });
            //        });
            //    }

            //    cell.updateCell(itemsS2[indexPath.Row].image, itemsS2[indexPath.Row].name);
            //}

            //return cell;
            #endregion

            var cell = collectionView.DequeueReusableCell(cellIdentifier, indexPath) as AirlineCell;

            if (indexPath.Section == 1)
            {
                if (itemsS1[indexPath.Row].image == null)
                {
                    cell.updateCell(UIImage.FromFile("Error/not_available_pic_off"), itemsS1[indexPath.Row].name, cellSize, false);
                    ImageAdapter.LoadAirlineImage(
                        itemsS1[indexPath.Row].Airline.ImageHandle,
                        cell,
                        itemsS1[indexPath.Row],
                        (imageBytes, theCell, theItem) =>
                        {
                            var uiImage = imageBytes.ToImage();
                            //theItem.Airline.AirlineLogoBytes = imageBytes;
                            theItem.image = uiImage;

                            InvokeOnMainThread(() =>
                            {
                                //AirlinesRepository.SaveAirlineImage(theItem.Airline);//Saved to local DB : Already done

                                theCell.updateCell(uiImage, theItem.name, cellSize);
                            });
                        }, AppData.usersAirLine.Airline.ID == itemsS1[indexPath.Row].Airline.ID);
                }
                else cell.updateCell(itemsS1[indexPath.Row].image, itemsS1[indexPath.Row].name, cellSize);
            }
            else
            {
                if (itemsS2[indexPath.Row].image == null)
                {
                    cell.updateCell(UIImage.FromFile("Error/not_available_pic_off"), itemsS2[indexPath.Row].name, cellSize, false);
                    ImageAdapter.LoadAirlineImage(
                        itemsS2[indexPath.Row].Airline.ImageHandle,
                        cell,
                        itemsS2[indexPath.Row],
                        (imageBytes, theCell, theItem) =>
                        {
                            var uiImage = imageBytes.ToImage();
                            //theItem.Airline.AirlineLogoBytes = imageBytes;
                            theItem.image = uiImage;

                            InvokeOnMainThread(() =>
                            {
                                //AirlinesRepository.SaveAirlineImage(theItem.Airline);//Saved to local DB//Already done

                                theCell.updateCell(uiImage, theItem.name, cellSize);
                            });
                        }, AppData.usersAirLine.Airline.ID == itemsS2[indexPath.Row].Airline.ID);
                }
                else cell.updateCell(itemsS2[indexPath.Row].image, itemsS2[indexPath.Row].name, cellSize);

            }

            return cell;
        }

        public override void Scrolled(UIScrollView scrollView)
        {
            //var indexPathOfVisibleItems = airLineCollectionView.IndexPathsForVisibleItems;
            //var visibleCells = airLineCollectionView.VisibleCells;

            //for(int i = 0; i < visibleCells.Length; i++)
            //{
            //    var cell = airLineCollectionView.DequeueReusableCell(cellIdentifier,
            //        indexPathOfVisibleItems[i]) as AirlineCell;

            //    cell.updateLayout(cellSize);

            //    if (indexPathOfVisibleItems[i].Section == 1)
            //    {
            //        if (itemsS1[indexPathOfVisibleItems[i].Row].image == null)
            //        {
            //            cell.updateCell(UIImage.FromFile("Error/not_available_pic_off"), itemsS1[indexPathOfVisibleItems[i].Row].name, false);
            //            ImageAdapter.LoadAirlineImage(itemsS1[indexPathOfVisibleItems[i].Row].Airline.ImageHandle, cell, itemsS1[indexPathOfVisibleItems[i].Row], (imageBytes, theCell, theItem) =>
            //            {
            //                InvokeOnMainThread(() =>
            //                {
            //                    var uiImage = imageBytes.ToImage();
            //                    theItem.Airline.AirlineLogoBytes = imageBytes;
            //                    AirlinesRepository.SaveAirlineImage(theItem.Airline);//Saved to local DB
            //                    theItem.image = uiImage;

            //                    theCell.updateCell(uiImage, theItem.name);
            //                });
            //            });
            //        }
            //        else cell.updateCell(itemsS1[indexPathOfVisibleItems[i].Row].image, itemsS1[indexPathOfVisibleItems[i].Row].name);
            //    }
            //    else
            //    {
            //        if (itemsS2[indexPathOfVisibleItems[i].Row].image == null)
            //        {
            //            cell.updateCell(UIImage.FromFile("Error/not_available_pic_off"), itemsS2[indexPathOfVisibleItems[i].Row].name, false);
            //            ImageAdapter.LoadAirlineImage(itemsS2[indexPathOfVisibleItems[i].Row].Airline.ImageHandle, cell, itemsS2[indexPathOfVisibleItems[i].Row], (imageBytes, theCell, theItem) =>
            //            {
            //                InvokeOnMainThread(() =>
            //                {
            //                    var uiImage = imageBytes.ToImage();
            //                    theItem.Airline.AirlineLogoBytes = imageBytes;
            //                    AirlinesRepository.SaveAirlineImage(theItem.Airline);//Saved to local DB
            //                    theItem.image = uiImage;

            //                    theCell.updateCell(uiImage, theItem.name);
            //                });
            //            });
            //        }
            //        else cell.updateCell(itemsS2[indexPathOfVisibleItems[i].Row].image, itemsS2[indexPathOfVisibleItems[i].Row].name);

            //    }

            //}
        }


        public override nint NumberOfSections(UICollectionView collectionView)
        {
            //TODO Section 1 must be the default result
            //Section 2 must only have element with a research
            //the line between the section is the header of section, thus currently the
            //line will always be there
            /*if (itemsS2.Count == 0)
                return 1;
            else
                return 2;*/
            return airLineViewController.calculateSection();

        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return section == 1 ? itemsS1.Count : itemsS2.Count;
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            MembershipProvider.Current.SelectedAirline = indexPath.Section == 1 ? itemsS1[indexPath.Row].Airline : itemsS2[indexPath.Row].Airline;
            AppData.selectedAirLine = indexPath.Section == 1 ? itemsS1[indexPath.Row] : itemsS2[indexPath.Row];

            var cell = collectionView.DequeueReusableCell(cellIdentifier, indexPath) as AirlineCell;
            var absoluteFrame = cell.ContentView.Superview.ConvertPointToView(new CGPoint(cell.Frame.X, cell.Frame.Y), airLineViewController.MasterView);

            airLineViewController.ShowLoading(AppDelegate.TextProvider.GetText(2505));
            AppManager.getCards(airLineViewController.CardsListChanged);
            //airLineViewController.CardsListChanged(cards);
            //airLineViewController.displayCardView(indexPath.Row, absoluteFrame, AppData.selectedAirLine);

            //airLineViewController.disMisKeyB();

            //collectionView.DeselectItem(indexPath, true);
        }

        public override UICollectionReusableView GetViewForSupplementaryElement(UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
        {
            if (indexPath.Section == 1)
            {

                var lineView = collectionView.DequeueReusableSupplementaryView(elementKind, "line", indexPath) as CollectionSectionView;

                return lineView;
            }
            else
            {
                var lineView = collectionView.DequeueReusableSupplementaryView(elementKind, "empty", indexPath) as CollectionEmptyView;

                return lineView;

            }
        }
        
    }

    public class AirlineCellData
    {
        public Airline Airline { get; private set; }
        public UIImage image;
        public string name;
        public string code;

        public AirlineCellData(Airline airline)
        {
            this.Airline = airline;
            this.image = airline.AirlineLogoBytes.ToImage();
            this.name = this.Airline.Name;
            this.code = this.Airline.Code;
        }

    }
}