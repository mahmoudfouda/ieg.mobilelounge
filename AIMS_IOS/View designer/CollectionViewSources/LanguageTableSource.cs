﻿using System;
using System.Collections.Generic;

using Foundation;
using UIKit;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public class LanguageTableSource : UITableViewSource
    {

        List<LanguageCellData> items;
        protected NSString cellIdentifier = (NSString)"langCell";
        private languageSelectionViewController vc;
        private UITableView tableView;

        public LanguageTableSource(List<LanguageCellData> items, languageSelectionViewController vc, UITableView tableView)
        {
            this.items = items;
            this.vc = vc;
            this.tableView = tableView;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                var cell = tableView.DequeueReusableCell(cellIdentifier) as LangCell;

                cell.updateLayout(tableView.RowHeight);

                if (indexPath.Row < items.Count - 1)
                    cell.updateCell(items[indexPath.Row], false);
                else
                    cell.updateCell(items[indexPath.Row], true);

                return cell;
            }
            catch
            {
                return null;
            }
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return items.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            AppManager.SetSelectedLanguage(items[indexPath.Row]);

            tableView.DeselectRow(indexPath, true);

            vc.onRowSelected();
        }
    }

    public class LanguageCellData
    {
        private UIImage GetLanguageImage(int languageKey)
        {
            return UIImage.FromFile(string.Format("Flag/Flag_{0}.png", languageKey));
        }

        public Language language;
        public string name;
        public UIImage flag;

        public LanguageCellData(string name, Language language)
        {
            this.name = name;
            this.language = language;
            this.flag = GetLanguageImage((int)language);
        }
    }
}