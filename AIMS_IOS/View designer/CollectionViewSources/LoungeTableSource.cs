﻿using System;
using System.Collections.Generic;

using Foundation;
using UIKit;
using AIMS.Models;
using AIMS;

namespace AIMS_IOS
{
    public class LoungeTableSource : UITableViewSource
    {
        List<LoungeCellData> items;
        protected NSString cellIdentifier = (NSString)"loungeCell";
        LoungeTableViewController parentViewController;
        UITableView tableView;

        public LoungeTableSource(List<LoungeCellData> items, LoungeTableViewController viewController, UITableView tableView)
        {
            this.items = items;
            parentViewController = viewController;
            this.tableView = tableView;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(cellIdentifier) as LoungeCell;

            cell.Frame = new CoreGraphics.CGRect(0, 0, this.tableView.Frame.Size.Width, this.tableView.Frame.Size.Height);

            //cell.updateLayout(tableView.RowHeight, parentViewController);
            cell.UpdateLayout();
            cell.updateCell(items[indexPath.Row].name, items[indexPath.Row].description, items[indexPath.Row].Progress);

            return cell;
        }


        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return items.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            MembershipProvider.Current.SelectedLounge = items[indexPath.Row].Workstation;
            AppData.SelectedLounge = items[indexPath.Row];
            tableView.DeselectRow(indexPath, true);
            parentViewController.onRowSelected();
        }

    }
}