﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIMS.Models;
using Foundation;
using UIKit;

namespace AIMS_IOS
{
    public class LoungeCellData
    {
        public string name;
        public string description;
        public string code;
        public Workstation Workstation { get; private set; }

        static TimeSpan refreshInterval = TimeSpan.FromMinutes(AppDelegate.RefreshIntervalMinutes);

        public double Progress { get; set; }

        public bool IsUpdated
        {
            get
            {
                if (Workstation == null) return false;

                return Workstation.LastStatisticsUpdate >= DateTime.UtcNow.Subtract(refreshInterval);
            }
            set
            {
                if (Workstation != null)
                {
                    if (value)
                        Workstation.LastStatisticsUpdate = DateTime.UtcNow;
                    else Workstation.LastStatisticsUpdate = DateTime.MinValue;
                }
            }
        }

        public LoungeCellData(Workstation workstation)
        {
            this.Workstation = workstation;
            this.name = workstation.WorkstationName;
            this.code = workstation.AirportCode;
            this.description = string.Format("{0} | {1}", workstation.AirportCode, workstation.LoungeName);
        }
    }
}