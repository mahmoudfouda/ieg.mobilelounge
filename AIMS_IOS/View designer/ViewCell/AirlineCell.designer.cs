﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("AirlineCell")]
    partial class AirlineCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView AirLineImageCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView cellBackground { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView decoration { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Footer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAirlineName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AirLineImageCell != null) {
                AirLineImageCell.Dispose ();
                AirLineImageCell = null;
            }

            if (cellBackground != null) {
                cellBackground.Dispose ();
                cellBackground = null;
            }

            if (decoration != null) {
                decoration.Dispose ();
                decoration = null;
            }

            if (Footer != null) {
                Footer.Dispose ();
                Footer = null;
            }

            if (lblAirlineName != null) {
                lblAirlineName.Dispose ();
                lblAirlineName = null;
            }
        }
    }
}