﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("CardsCell")]
    partial class CardsCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView cardImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel cardLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (cardImage != null) {
                cardImage.Dispose ();
                cardImage = null;
            }

            if (cardLabel != null) {
                cardLabel.Dispose ();
                cardLabel = null;
            }
        }
    }
}