﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using CoreGraphics;
using Foundation;
using Ieg.Mobile.Localization;
using Syncfusion.iOS.Buttons;
using UIKit;

namespace AIMS_IOS
{
    [Register("LastHourFilterCell")]
    public class LastHourFilterCell : UITableViewCell
    {
        public event EventHandler SearchCliked;


        private UIDatePicker datePicker;
        private UITextField txtdate, txtPicker;
        private UIDeviceOrientation currentOrientation;
        private UIStackView stackViewAiport, stackViewLounge, stackViewDate, stackViewGeneral;
        private NSDateFormatter dateFormatter = new NSDateFormatter();
        private UIPickerView pickerView;
        private SfSegmentedControl segmentedControl;
        private List<String> arrayList = new List<string>();
        private ObservableCollection<SfSegmentItem> textCollection = new ObservableCollection<SfSegmentItem>();
        private UIStackView stackView;
        private UIButton btnSearch;

        public DateTime SelectedDate
        {
            get
            {
                DateTime reference = new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var utcDateTime = reference.AddSeconds(datePicker.Date.SecondsSinceReferenceDate);

                var date =  utcDateTime.ToLocalTime().Subtract(SelectedTimeSpan);

                if (SelectedTimeSpan.TotalMinutes == 0)
                    date = date.Date;

                return date;

                //return utcDateTime.ToLocalTime().Date;
            }
        }

        public TimeSpan SelectedTimeSpan
        {
            get
            {
                switch (((PickerDataSource)pickerView.Model).SelectedIndex)
                {
                    case 1:
                        return TimeSpan.FromHours(4);

                    case 2:
                        return TimeSpan.FromHours(8);

                    case 3:
                        return TimeSpan.FromHours(24);

                    default:
                        return TimeSpan.FromSeconds(0);
                }
            }
        }

        public bool AirportSearch
        {
            get
            {
                return segmentedControl.SelectedIndex == 0;
            }
        }


        public LastHourFilterCell(IntPtr handle) : base(handle)
        {
            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;
            LoadTexts(null);

            this.SelectionStyle = UITableViewCellSelectionStyle.None;
        }

        private void LoadTexts(Language? language)
        {
            InvokeOnMainThread(() =>
            {

                arrayList.Clear();
                arrayList.Add(AppDelegate.TextProvider.GetText(2184));
                arrayList.Add(AppDelegate.TextProvider.GetText(2185));
                arrayList.Add(AppDelegate.TextProvider.GetText(2186));
                arrayList.Add(AppDelegate.TextProvider.GetText(2187));

                textCollection.Clear();
                textCollection.Add(new SfSegmentItem() { Text = AppDelegate.TextProvider.GetText(2188) });
                textCollection.Add(new SfSegmentItem() { Text = AppDelegate.TextProvider.GetText(2189) });

            });
        }

        [Export("initWithFrame:")]
        public LastHourFilterCell(CGRect frame) : base(frame)
        {

        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            if (currentOrientation != UIDevice.CurrentDevice.Orientation)
                UpdateLayout();
        }

        public void SetFonts(nfloat scaleFactor)
        {
            UIFont smallFont, largeFont;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                smallFont = UIFont.SystemFontOfSize(14f * (scaleFactor * 3f / 4f));
                largeFont = UIFont.SystemFontOfSize(20f * (scaleFactor * 3f / 4f));
            }
            else
            {
                smallFont = UIFont.SystemFontOfSize(14f * scaleFactor);
                largeFont = UIFont.SystemFontOfSize(20f * scaleFactor);
            }

            segmentedControl.Font = largeFont;
            txtdate.Font = smallFont;
            txtPicker.Font = smallFont;

        }


        public void UpdateLayout()
        {
            var screenWidth = UIScreen.MainScreen.Bounds.Width;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                            UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            currentOrientation = UIDevice.CurrentDevice.Orientation;

            var scaleFactor = height / 667f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                scaleFactor *= .75f;

            var isLandscape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
            var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            if (!isPad && !isLandscape && scaleFactor > 1f)
                scaleFactor = 1f;

            RemoveControlsFromSuperView();

            if (segmentedControl == null)
            {
                CreateSegmentControl(screenWidth);

                var lineView1 = new UIView(new CGRect(0, stackView.Frame.Bottom + 20, screenWidth, 1));
                lineView1.Layer.BorderWidth = 0.5f;
                lineView1.Layer.BorderColor = UIColor.Black.CGColor;


                CreateDateControl(screenWidth, lineView1);

                CreatePickerControl(screenWidth);

                CreateSearchButton(screenWidth);

                this.ContentView.AddSubview(stackView);
                this.ContentView.AddSubview(lineView1);
                this.ContentView.AddSubview(txtdate);
                this.ContentView.AddSubview(txtPicker);
                this.ContentView.AddSubview(btnSearch);
            }

            SetFonts(scaleFactor);

        }

        private void CreateSearchButton(nfloat screenWidth)
        {
            btnSearch = new UIButton();
            btnSearch.SetImage(UIImage.FromFile("btn_search1.png"), UIControlState.Normal);
            btnSearch.Frame = new CGRect(screenWidth - 64, txtPicker.Frame.Bottom + 20, 44f, 44f);


            btnSearch.TouchUpInside -= BtnSearch_TouchUpInside;
            btnSearch.TouchUpInside += BtnSearch_TouchUpInside;

        }

        private void BtnSearch_TouchUpInside(object sender, EventArgs e)
        {
            SearchCliked?.Invoke(this, new EventArgs());
        }

        private void CreatePickerControl(nfloat screenWidth)
        {
            pickerView = new UIPickerView();
            pickerView.Model = new PickerDataSource(arrayList);
            txtPicker = new UITextField(new CGRect(screenWidth / 2 + 5, txtdate.Frame.Top, (screenWidth / 2) - 25, 40));
            txtPicker.Text = arrayList.First();
            txtPicker.Layer.BorderColor = UIColor.LightGray.CGColor;
            txtPicker.Layer.BorderWidth = 1f;
            txtPicker.Layer.CornerRadius = 5;
            txtPicker.BackgroundColor = UIColor.FromRGB(196, 196, 196);

            txtPicker.RightView = new UIImageView(UIImage.FromFile("arrowdown.png"));
            txtPicker.RightViewMode = UITextFieldViewMode.Always;
            txtPicker.InputView = pickerView;
            UIView paddingView = new UIView(new CGRect(0, 0, 15, 20));
            txtPicker.LeftView = paddingView;
            txtPicker.LeftViewMode = UITextFieldViewMode.Always;

            txtPicker.ShouldChangeCharacters += (UITextField textField, NSRange range, string replacementString) =>
            {
                return false;
            };

            ((PickerDataSource)pickerView.Model).ValueChanged += Picker_ValueChanged;
        }

        private void CreateDateControl(nfloat screenWidth, UIView lineView1)
        {
            txtdate = new UITextField(new CGRect(20, lineView1.Frame.Bottom + 12, (screenWidth / 2) - 20, 40));
            txtdate.RightView = new UIImageView(UIImage.FromFile("calendar.png"));
            txtdate.RightViewMode = UITextFieldViewMode.Always;
            datePicker = new UIDatePicker();
            datePicker.Mode = UIDatePickerMode.Date;
            datePicker.ValueChanged += DatePicker_ValueChanged;
            datePicker.Date = NSDate.Now;

            UIView paddingV = new UIView(new CGRect(0, 0, 15, 20));
            txtdate.LeftView = paddingV;
            txtdate.LeftViewMode = UITextFieldViewMode.Always;

            txtdate.InputView = datePicker;

            dateFormatter.DateFormat = "MM/dd/yyyy";
            txtdate.Text = dateFormatter.ToString(NSDate.Now);
            txtdate.ShouldChangeCharacters += (UITextField textField, NSRange range, string replacementString) =>
            {
                return false;
            };

            txtdate.Layer.BorderColor = UIColor.LightGray.CGColor;
            txtdate.Layer.BorderWidth = 1f;
            txtdate.Layer.CornerRadius = 5;
        }

        private void CreateSegmentControl(nfloat screenWidth)
        {
            segmentedControl = new SfSegmentedControl();
            segmentedControl.ItemsSource = textCollection;
            segmentedControl.FontColor = UIColor.FromRGB(255, 255, 255);
            segmentedControl.Font = UIFont.SystemFontOfSize(22);
            segmentedControl.Color = UIColor.FromRGB(0, 207, 246);
            segmentedControl.SegmentHeight = 40;
            segmentedControl.VisibleSegmentsCount = 2;
            segmentedControl.SelectedIndex = 1;
            segmentedControl.DisplayMode = SegmentDisplayMode.Text;
            segmentedControl.BorderThickness = 1;
            segmentedControl.BorderColor = UIColor.Clear;
            segmentedControl.SelectionTextColor = UIColor.FromRGB(4, 142, 172);
            segmentedControl.SelectionIndicatorSettings = new SelectionIndicatorSettings()
            {
                Color = UIColor.White,
                Position = SelectionIndicatorPosition.Fill
            };

            stackView = new UIStackView(new CGRect(20, 25, screenWidth - 40, 40));
            stackView.Axis = UILayoutConstraintAxis.Vertical;
            stackView.AddArrangedSubview(segmentedControl);
        }

        private void RemoveControlsFromSuperView()
        {
            foreach (var view in ContentView.Subviews)
            {
                view.RemoveFromSuperview();
            }

            if (btnSearch != null)
                btnSearch.TouchUpInside -= BtnSearch_TouchUpInside;

            segmentedControl = null;
            stackView = null;
            txtdate = null;
            datePicker = null;
            pickerView = null;
            txtPicker = null;
            btnSearch = null;
        }

        private void Picker_ValueChanged(object sender, EventArgs e)
        {
            txtPicker.Text = ((PickerDataSource)pickerView.Model).SelectedItem;
            txtPicker.EndEditing(true);
        }

        private void DatePicker_ValueChanged(object sender, EventArgs e)
        {
            txtdate.Text = dateFormatter.ToString(datePicker.Date);
        }
    }

    public class PickerDataSource : UIPickerViewModel
    {
        public EventHandler ValueChanged;

        private List<string> _myItems;
        protected int selectedIndex = 0;

        public PickerDataSource(List<string> items)
        {
            _myItems = items;
        }

        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
        }

        public string SelectedItem
        {
            get { return _myItems[selectedIndex]; }
        }

        public override nint GetComponentCount(UIPickerView picker)
        {
            return 1;
        }

        public override nint GetRowsInComponent(UIPickerView picker, nint component)
        {
            return _myItems.Count;
        }

        public override string GetTitle(UIPickerView pickerView, nint row, nint component)
        {
            return _myItems[(int)row];
        }

        public override void Selected(UIPickerView picker, nint row, nint component)
        {
            selectedIndex = (int)row;
            ValueChanged(null, null);
        }

    }

    public class LasHourFilterCellData
    {

    }

    public class AgentTabLastHourFilterSource : UITableViewSource
    {
        public event EventHandler SearchCliked;

        protected List<LasHourFilterCellData> items;
        protected NSString cellIdentifier = (NSString)"lastHourFilterCell";
        private AgentTabLastHour vc;
        private LastHourFilterCell currentCell;

        public List<LasHourFilterCellData> Items
        {
            get
            {
                return items;
            }
        }

        public AgentTabLastHourFilterSource(List<LasHourFilterCellData> items, AgentTabLastHour vc)
        {
            this.items = items;
            this.vc = vc;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var item = items[indexPath.Row];
            int index = indexPath.Row;

            var cell = tableView.DequeueReusableCell(cellIdentifier) as LastHourFilterCell;

            cell.UpdateLayout();
            cell.SetNeedsLayout();
            cell.LayoutIfNeeded();

            cell.SearchCliked -= Cell_SearchCliked;
            cell.SearchCliked += Cell_SearchCliked;


            return cell;
        }

        private void Cell_SearchCliked(object sender, EventArgs e)
        {
            SearchCliked?.Invoke(sender, e);
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return items.Count;
        }
    }
}
