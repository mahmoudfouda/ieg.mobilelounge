using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class LoungeCell : UITableViewCell
    {
        private UIDeviceOrientation currentOrientation;

        public UIEdgeInsets BorderWidth { get; set; }
        public UIColor BorderColor { get; set; }

        public LoungeCell(IntPtr handle) : base(handle)
        {
            BorderColor = new UIColor(new CGColor(0, 0, 0));
            BorderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            if (currentOrientation != UIDevice.CurrentDevice.Orientation)
                UpdateLayout();
        }

        public void updateCell(string title, string description, double progress)
        {
            this.cellTitle.AdjustsFontSizeToFitWidth = false;
            this.cellDescription.AdjustsFontSizeToFitWidth = true;

            this.cellTitle.Text = title;
            this.cellDescription.Text = description;

            customProgress.UpdateProgress(progress);
        }

        public void UpdateLayout()
        {
            var screenWidth = UIScreen.MainScreen.Bounds.Width;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                            UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            currentOrientation = UIDevice.CurrentDevice.Orientation;

            var scaleFactor = height / 667f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                scaleFactor *= .75f;

            var isLandscape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
            var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            if (!isPad && !isLandscape && scaleFactor > 1f)
                scaleFactor = 1f;

            height = 85f / 650f * height; 
            height = height < 85f ? 85f : height;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                height *= .75f;

            nfloat margin = 5f * scaleFactor;
            nfloat bigMargin = 2f * margin;
            nfloat elementHeight = height / 2f - (margin * 2);
            nfloat imgSize = height - margin - margin;

            cellImage.Frame = new CGRect(margin, margin, imgSize, imgSize);

            cellTitle.Frame = new CGRect(cellImage.Frame.Right + bigMargin + (margin/2), cellImage.Frame.Y + (margin / 2f),
                (this.ContentView.Frame.Width * scaleFactor) - (cellImage.Frame.Right + bigMargin + bigMargin),
                17 * scaleFactor);

            cellDescription.Frame = new CGRect(cellTitle.Frame.X, cellTitle.Frame.Bottom,
                (cellTitle.Frame.Width * scaleFactor), cellTitle.Frame.Height - margin);

            var marginCustom = (this.Frame.Width - (this.Frame.Width * 0.80)) / 2;

            // customProgress.Frame = new CGRect(cellTitle.Frame.X, cellDescription.Frame.Bottom + bigMargin, (this.Frame.Width - (2* cellTitle.Frame.X)), CustomProgressControl.MinSize);

            customProgress.Frame = new CGRect(cellTitle.Frame.X, cellDescription.Frame.Bottom + bigMargin, this.Frame.Width - cellTitle.Frame.X - bigMargin, CustomProgressControl.MinSize);


            SetFonts(scaleFactor);
        }

        public void SetFonts(nfloat scaleFactor)
        {
            UIFont smallFont, largeFont;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                smallFont = UIFont.ItalicSystemFontOfSize(12f * (scaleFactor * 3f / 4f));
                largeFont = UIFont.SystemFontOfSize(20f * (scaleFactor * 3f / 4f));
            }
            else
            {
                smallFont = UIFont.ItalicSystemFontOfSize(12f * scaleFactor);
                largeFont = UIFont.SystemFontOfSize(20f * scaleFactor);
            }

            cellTitle.Font = largeFont;
            cellDescription.Font = smallFont;

        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            UIDrawBorder.DrawLeftBorder(context, rect, this.Frame, BorderColor, BorderWidth);
            UIDrawBorder.DrawRightBorder(context, rect, this.Frame, BorderColor, BorderWidth);
            UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, BorderColor, BorderWidth);
        }

         
    }
}