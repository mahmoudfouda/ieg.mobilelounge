﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;

namespace AIMS_IOS
{
    public class LoungeCellStatistics : UITableViewCell
    {
        private UIDeviceOrientation currentOrientation;
        private nfloat scaleFactor;
        private UILabel cellTitle, cellDescription;
        private CustomProgressControl customProgress;
        private nfloat margin;
        private nfloat bigMargin;
        private UIImageView cellImage;

        public LoungeCellStatistics(IntPtr handle) : base(handle)
        {
            BorderColor = UIColor.Gray;
            BorderWidth = new UIEdgeInsets(0.5f, 0.5f, 0.5f, 0.5f);

            cellTitle = new UILabel();
            cellDescription = new UILabel();
            cellImage = new UIImageView(new UIImage("Lounge/sofa.png"));
            customProgress = new CustomProgressControl(new CGRect());

            this.AddSubview(cellImage);
            this.AddSubview(cellTitle);
            this.AddSubview(cellDescription);
            this.AddSubview(customProgress);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            if (currentOrientation != UIDevice.CurrentDevice.Orientation)
            {
                UpdateLayout();

                if (customProgress != null)
                    customProgress.RefreshProgress();
            }
        }

        public void UpdateCell(string title, string description, double progress)
        {
            cellTitle.AdjustsFontSizeToFitWidth = false;
            cellTitle.Text = title;
            cellDescription.Text = description;
            customProgress.UpdateProgress(progress);
        }

        public UIEdgeInsets BorderWidth { get; set; }
        public UIColor BorderColor { get; set; }

        public void UpdateLayout()
        {
            var screenWidth = UIScreen.MainScreen.Bounds.Width;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                            UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            currentOrientation = UIDevice.CurrentDevice.Orientation;

            scaleFactor = height / 667f;

            var isLandscape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
            var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            if (!isPad && !isLandscape && scaleFactor > 1f)
                scaleFactor = 1f;

            //height = 44f / 650f * height;
            //height = height < 44f ? 44f : height;

            height = 60f / 650f * height;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                height *= .75f;
            height = height < 60f ? 60f : height;

            margin = 5f * scaleFactor;
            bigMargin = 2f * margin;
            nfloat elementHeight = height / 2f - (margin * 2);
            nfloat imgSize = height - margin - margin;

            cellImage.Frame = new CGRect(margin, margin, imgSize, imgSize);

            cellTitle.Frame = new CGRect(cellImage.Frame.Right + margin, cellImage.Frame.Y + (margin / 2f),
                (this.Frame.Width * scaleFactor) - (cellImage.Frame.Right + bigMargin + bigMargin),
                17 * scaleFactor);

            cellDescription.Frame = new CGRect(cellTitle.Frame.X, cellTitle.Frame.Bottom,
               (cellTitle.Frame.Width * scaleFactor), cellTitle.Frame.Height - margin);

            //cellTitle.Frame = new CGRect(margin, (margin / 2f),
            //    (this.ContentView.Frame.Width * scaleFactor) - (bigMargin + bigMargin),
            //    imgSize / 2f);

            //cellDescription.Frame = new CGRect(cellTitle.Frame.X, cellTitle.Frame.Bottom,
            //   (cellTitle.Frame.Width * scaleFactor), cellTitle.Frame.Height - margin);

            customProgress.Frame = new CGRect(cellTitle.Frame.X, cellDescription.Frame.Bottom + margin, (this.Frame.Width - cellTitle.Frame.X - margin), CustomProgressControl.MinSize);

            SetFonts(scaleFactor);
        }

        public void SetFonts(nfloat scaleFactor)
        {
            UIFont smallFont, italicSmallFont;
            string regularFontName = "Helvetica Neue";

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                smallFont = UIFont.FromName(regularFontName, 12f * (scaleFactor * 3f / 4f));
                italicSmallFont = UIFont.ItalicSystemFontOfSize(10f * (scaleFactor * 3f / 4f));
            }
            else
            {
                smallFont = UIFont.FromName(regularFontName, 12f * scaleFactor);
                italicSmallFont = UIFont.ItalicSystemFontOfSize(10f * scaleFactor);
            }

            cellTitle.Font = smallFont;
            cellDescription.Font = italicSmallFont;
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();
            context.SetFillColor(BorderColor.CGColor);

            context.FillRect(new CGRect(bigMargin + margin, customProgress.Frame.Bottom + bigMargin - BorderWidth.Bottom,
                             this.Frame.Width - (2*bigMargin) - (3*margin), BorderWidth.Bottom));


        }
    }
}