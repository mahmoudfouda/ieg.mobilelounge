using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace AIMS_IOS
{
    public partial class AirlineCell : UICollectionViewCell
    {
        public nfloat ScaleFactor
        {
            get
            {
                return AppDelegate.AirlinesView.ScaleFactor;
            }
        }

        public AirlineCell (IntPtr handle) : base (handle)
        {
        }
        
		public void updateCell(UIImage image, string footer, CGPoint frameSize, bool realImage = true)
		{
            #region Old Codes

            //         this.Layer.MasksToBounds = true;
            //AirLineImageCell.Image = image;
            //         AirLineImageCell.Layer.MasksToBounds = true;
            //         AirLineImageCell.BackgroundColor = new UIColor(0.9f, 0.4f);
            //         AirLineImageCell.Layer.CornerRadius = 10f;
            //Footer.Text = footer;

            //         //Footer.AdjustsFontSizeToFitWidth = true;
            #endregion

            #region Updating Layout
            //Footer.AdjustsFontSizeToFitWidth = true;
            var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;

            decoration.Frame = new CGRect(0f, 0f, frameSize.X, frameSize.X);
            cellBackground.Frame = decoration.Frame;

            AirLineImageCell.Frame = new CGRect(frameSize.X * 0.06f, frameSize.X * 0.06f, frameSize.X * 0.88f, frameSize.X * 0.88f);
            AirLineImageCell.Layer.CornerRadius = 10f;

            lblAirlineName.Frame = new CGRect(0f, frameSize.X, frameSize.X, frameSize.Y - frameSize.X);

            lblAirlineName.PreferredMaxLayoutWidth = frameSize.X;
            lblAirlineName.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
            #endregion

            //if (isImageSet) return;
            //to prevent setting the item's Image every time on every scroll!!
            this.Layer.MasksToBounds = true;
            AirLineImageCell.Image = image;
            AirLineImageCell.Layer.MasksToBounds = true;
            cellBackground.BackgroundColor = new UIColor(0.9f, 0.4f);
            //AirLineImageCell.Layer.CornerRadius = 10f;
            Footer.Text = footer;
            Footer.Hidden = true;
            lblAirlineName.Text = footer;
            
            setFonts();
        }

        public void setFonts()
        {

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                //Footer.font = UIFont.FromName("font name", 20f);
                //if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                //    lblAirlineName.Font = UIFont.SystemFontOfSize(17f);
                //else lblAirlineName.Font = UIFont.SystemFontOfSize(9f);

                lblAirlineName.Font = UIFont.SystemFontOfSize(8f * ScaleFactor);
                lblAirlineName.LineBreakMode = UILineBreakMode.WordWrap;
                lblAirlineName.TextAlignment = UITextAlignment.Center;
                lblAirlineName.Lines = 2;
            }
            else
            {
                lblAirlineName.Font = UIFont.SystemFontOfSize(14f * ScaleFactor);
                lblAirlineName.LineBreakMode = UILineBreakMode.TailTruncation;
                lblAirlineName.TextAlignment = UITextAlignment.Center;
                lblAirlineName.Lines = 1;
            }
        }
    }
}