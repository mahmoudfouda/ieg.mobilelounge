﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("PassengerServiceCell")]
    partial class PassengerServiceCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgPassengerService { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblPassengerServiceName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblPSMaxQuantity { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblPSQuantity { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView passengerServiceView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (imgPassengerService != null) {
                imgPassengerService.Dispose ();
                imgPassengerService = null;
            }

            if (lblPassengerServiceName != null) {
                lblPassengerServiceName.Dispose ();
                lblPassengerServiceName = null;
            }

            if (lblPSMaxQuantity != null) {
                lblPSMaxQuantity.Dispose ();
                lblPSMaxQuantity = null;
            }

            if (lblPSQuantity != null) {
                lblPSQuantity.Dispose ();
                lblPSQuantity = null;
            }

            if (passengerServiceView != null) {
                passengerServiceView.Dispose ();
                passengerServiceView = null;
            }
        }
    }
}