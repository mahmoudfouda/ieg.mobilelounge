﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("AirportCell")]
    partial class AirportCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAirportCellDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAirportCellTitle { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lblAirportCellDescription != null) {
                lblAirportCellDescription.Dispose ();
                lblAirportCellDescription = null;
            }

            if (lblAirportCellTitle != null) {
                lblAirportCellTitle.Dispose ();
                lblAirportCellTitle = null;
            }
        }
    }
}