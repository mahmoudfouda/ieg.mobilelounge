﻿using AIMS.Models;
using AIMS_IOS.Models;
using Foundation;
using System;
using UIKit;
using AIMS_IOS.Utility;

namespace AIMS_IOS
{
    public partial class GuestCategoryCell : MyCell
    {
        private bool isRowExpanded = false;
        public bool IsRowExpanded
        {
            get {
                return isRowExpanded;
            }
            set
            {
                isRowExpanded = value;
                if (this.imgGuestCategoryExpandCollapse != null)
                    this.imgGuestCategoryExpandCollapse.Image = value ? UIImage.FromFile("Guest/UpArrow") : UIImage.FromFile("Guest/DownArrow");
            }
        }
        
        public GuestCategoryCell (IntPtr handle) : base (handle)
        {
        }

        public override void UpdateRow(Item element, nfloat screenWidth, nfloat scaleFactor)
        {
            if (element == null || !(element is GuestCategoryCellData))
                return;

            imgGuestCategory.Frame = new CoreGraphics.CGRect(5f * scaleFactor, 5f * scaleFactor, 44f * scaleFactor, 44f * scaleFactor);
            imgGuestCategoryExpandCollapse.Frame = new CoreGraphics.CGRect(this.Frame.Width - (35f * scaleFactor), 15f * scaleFactor, 25f * scaleFactor, 25f * scaleFactor);
            lblGuestMaxQty.Frame = new CoreGraphics.CGRect(screenWidth - this.imgGuestCategoryExpandCollapse.Frame.Width - (40f * scaleFactor) - (5f * scaleFactor), 22f * scaleFactor, 37f * scaleFactor, 20f * scaleFactor);
            lblGuestQuantity.Frame = new CoreGraphics.CGRect(this.lblGuestMaxQty.Frame.Left - (35 * scaleFactor), 5f * scaleFactor, 32f * scaleFactor, 44f * scaleFactor);
            lblGuestCategory.Frame = new CoreGraphics.CGRect(this.imgGuestCategory.Frame.Right + (5f * scaleFactor), 2f * scaleFactor, this.lblGuestQuantity.Frame.Left - this.imgGuestCategory.Frame.Right - (10f * scaleFactor), 50f * scaleFactor);


            //BackgroundColor = new UIColor(new CoreGraphics.CGColor(1f, 102f / 255f, 36f / 255f));
            BackgroundColor = UIColor.Clear.FromHexString("#9370db");


            lblGuestCategory.Font = UIFont.SystemFontOfSize(22f * scaleFactor);
            lblGuestCategory.AdjustsLetterSpacingToFitWidth = true;
            lblGuestCategory.AdjustsFontSizeToFitWidth = true;
            lblGuestCategory.MinimumFontSize = 16f * scaleFactor;
            lblGuestCategory.LineBreakMode = UILineBreakMode.WordWrap;
            lblGuestCategory.Lines = 0;

            lblGuestQuantity.Font = UIFont.BoldSystemFontOfSize(24f * scaleFactor);
            lblGuestMaxQty.Font = UIFont.SystemFontOfSize(16f * scaleFactor);
            
            var passengerServiceCellData = element as GuestCategoryCellData;
            var passengerService = passengerServiceCellData.PassengerServiceData;
            if (passengerService == null) return;

            lblGuestCategory.Text = passengerServiceCellData.Name;

            if (passengerService.MaximumQuantity == 0)
            {
                lblGuestMaxQty.Hidden = true;
                lblGuestQuantity.Hidden = true;
            }
            else
            {
                lblGuestMaxQty.Hidden = false;
                lblGuestQuantity.Hidden = false;
                lblGuestQuantity.Text = passengerService.Quantity.ToString();
                lblGuestMaxQty.Text = string.Format("/ {0}", passengerService.MaximumQuantity);
            }
            
            imgGuestCategoryExpandCollapse.Image = UIImage.FromFile("Guest/DownArrow");

            UIImage image = null;
            switch (passengerService.Id)
            {
                case 0:
                    //none
                    image = UIImage.FromFile("Tab/tab_passengers_on_big ");
                    break;
                case 1:
                    //complementary
                    image = UIImage.FromFile("Guest/GuestOn");
                    lblGuestCategory.Text = AppDelegate.TextProvider.GetText(2162);
                    break;
                case 2:
                    //family
                    image = UIImage.FromFile("Guest/FamilyGuestOn");
                    lblGuestCategory.Text = AppDelegate.TextProvider.GetText(2163);
                    break;
                case 3:
                    //paid
                    image = UIImage.FromFile("Guest/PaidGuestOn");
                    break;
                default:
                    //defaultimage
                    image = UIImage.FromFile("Tab/tab_passengers_on_big");
                    break;
            }
            if (image != null)
                this.imgGuestCategory.Image = image;


        }

        public override void SetDeleteButtonAction(UITableView parentTableView, NSIndexPath indexPath, Item item, DeleteItemHandler deleteAction)
        {
            //None for parent rows
        }
    }
}