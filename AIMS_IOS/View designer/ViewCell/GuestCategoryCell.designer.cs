﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("GuestCategoryCell")]
    partial class GuestCategoryCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgGuestCategory { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgGuestCategoryExpandCollapse { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblGuestCategory { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblGuestMaxQty { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblGuestQuantity { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (imgGuestCategory != null) {
                imgGuestCategory.Dispose ();
                imgGuestCategory = null;
            }

            if (imgGuestCategoryExpandCollapse != null) {
                imgGuestCategoryExpandCollapse.Dispose ();
                imgGuestCategoryExpandCollapse = null;
            }

            if (lblGuestCategory != null) {
                lblGuestCategory.Dispose ();
                lblGuestCategory = null;
            }

            if (lblGuestMaxQty != null) {
                lblGuestMaxQty.Dispose ();
                lblGuestMaxQty = null;
            }

            if (lblGuestQuantity != null) {
                lblGuestQuantity.Dispose ();
                lblGuestQuantity = null;
            }
        }
    }
}