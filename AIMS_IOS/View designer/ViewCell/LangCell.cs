using Foundation;
using System;
using UIKit;

namespace AIMS_IOS
{
    public partial class LangCell : UITableViewCell
    {
		public UIEdgeInsets BorderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

        public UIColor BorderColor;// = new UIColor(CoreImage.CIColor.BlackColor);

		private bool last = false;

        public LangCell (IntPtr handle) : base (handle)
        {
            BorderColor = new UIColor(new CoreGraphics.CGColor(0, 0, 0));
        }

		public void updateCell(LanguageCellData SelecLang, bool last)
        {
            langLabel.Text = SelecLang.name;
            cellImage.Image = SelecLang.flag;

			this.last = last;
		}

        public void setFonts()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                //langLabel.font = UIFont.FromName("font name", 20f);
            }
        }

        public void updateLayout(nfloat height)
        {
            setFonts();

            nfloat widthratio = 1.15f;
            nfloat margin = 3f;
            nfloat elementHeight = height - (margin * 2);

            cellImage.Frame = new CoreGraphics.CGRect(margin, margin,
                elementHeight, elementHeight * widthratio);

            langLabel.AdjustsFontSizeToFitWidth = true;
            nfloat labelX = cellImage.Frame.X + cellImage.Frame.Width + (margin * 2f);
            langLabel.Frame = new CoreGraphics.CGRect(labelX, margin,
                ContentView.Frame.Width - labelX - margin, cellImage.Frame.Height);
        }

		public override void Draw(CoreGraphics.CGRect rect)
		{
			base.Draw(rect);

			if (!last)
			{
				BorderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

                BorderColor = new UIColor(new CoreGraphics.CGColor(1, 1, 1));

                var fWidth = this.Frame.Size.Width;
				var fHeight = this.Frame.Size.Height;

				var context = UIGraphics.GetCurrentContext();

				UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, BorderColor, BorderWidth);
			}
		}

    }
}