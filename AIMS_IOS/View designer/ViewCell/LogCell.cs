using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class LogCell : UITableViewCell
    {
        public LogCell (IntPtr handle) : base (handle)
        {
        }
        
		public void updateCell(string title, string descr, string date)
		{
			this.title.Text = title;
			description.Text = descr;
            timeLabel.Text = date;
		}

        public void setFonts(nfloat scaleFactor)
        {
            UIFont smallFont, mediumFont, largeFont;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                smallFont = UIFont.ItalicSystemFontOfSize(12f * (scaleFactor * 3f / 4f));
                mediumFont = UIFont.SystemFontOfSize(14f * (scaleFactor * 3f / 4f));
                largeFont = UIFont.SystemFontOfSize(20f * (scaleFactor * 3f / 4f));
            }
            else
            {
                smallFont = UIFont.ItalicSystemFontOfSize(12f * scaleFactor);
                mediumFont = UIFont.SystemFontOfSize(14f * scaleFactor);
                largeFont = UIFont.SystemFontOfSize(20f * scaleFactor);
            }

            timeLabel.Font = smallFont;
            timeLabel.TextColor = UIColor.FromRGB(100, 100, 100);
            title.Font = largeFont;
            description.Font = mediumFont;
            description.TextColor = UIColor.FromRGB(100, 100, 100);
        }

        public void updateLayout(nfloat height, AgentTabLogs viewController)
        {
            var scaleFactor = viewController.ScaleFactor;
            var screenWidth = UIScreen.MainScreen.Bounds.Width;

            var isLandscape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
            var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            if (!isPad && !isLandscape && scaleFactor > 1f)
                scaleFactor = 1f;

            setFonts(scaleFactor);

            nfloat margin = 5f * scaleFactor;

            
            nfloat imgSize = height - (2f * margin);
            logImage.Frame = new CGRect(margin, margin, imgSize, imgSize);

            timeLabel.Frame = new CGRect(logImage.Frame.Right, 0f, screenWidth - logImage.Frame.Right - (3f *margin), 13f * scaleFactor);

            title.Frame = new CGRect(logImage.Frame.Right + margin, 8f * scaleFactor,
                screenWidth - logImage.Frame.Right - (margin * 2f), 22f * scaleFactor);

            description.Frame = new CGRect(title.Frame.X, title.Frame.Bottom,
                title.Frame.Width, 19f * scaleFactor);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            //updateLayout(this.ContentView.Frame.Height);
        }
    }
}