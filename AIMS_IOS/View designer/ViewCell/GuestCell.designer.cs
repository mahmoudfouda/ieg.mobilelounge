﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("GuestCell")]
    partial class GuestCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnGuestCommand { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel guestNameLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgGuestCommand { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView pnlGuestButtonHolder { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView pnlGuestContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView pnlGuestHeader { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnGuestCommand != null) {
                btnGuestCommand.Dispose ();
                btnGuestCommand = null;
            }

            if (guestNameLabel != null) {
                guestNameLabel.Dispose ();
                guestNameLabel = null;
            }

            if (imgGuestCommand != null) {
                imgGuestCommand.Dispose ();
                imgGuestCommand = null;
            }

            if (pnlGuestButtonHolder != null) {
                pnlGuestButtonHolder.Dispose ();
                pnlGuestButtonHolder = null;
            }

            if (pnlGuestContainer != null) {
                pnlGuestContainer.Dispose ();
                pnlGuestContainer = null;
            }

            if (pnlGuestHeader != null) {
                pnlGuestHeader.Dispose ();
                pnlGuestHeader = null;
            }
        }
    }
}