﻿using AIMS_IOS.Models;
using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace AIMS_IOS
{
    public delegate void DeleteItemHandler(Item item);

    public abstract class MyCell : UITableViewCell
    {
        public MyCell()
        {
        }

        public MyCell(CGRect frame) : base(frame)
        {
        }

        public MyCell(NSCoder coder) : base(coder)
        {
        }

        public MyCell(UITableViewCellStyle style, NSString reuseIdentifier) : base(style, reuseIdentifier)
        {
        }

        public MyCell(UITableViewCellStyle style, string reuseIdentifier) : base(style, reuseIdentifier)
        {
        }

        protected MyCell(NSObjectFlag t) : base(t)
        {
        }

        protected internal MyCell(IntPtr handle) : base(handle)
        {
        }

        public abstract void UpdateRow(Item element, nfloat screenWidth, nfloat scaleFactor);

        public abstract void SetDeleteButtonAction(UITableView parentTableView, NSIndexPath indexPath, Item item, DeleteItemHandler deleteAction);
    }

    public partial class GuestCell : MyCell
    {
        private Item item;
        private UITableView parentTableView;
        private NSIndexPath indexPath;
        private DeleteItemHandler deleteAction;

        public GuestCell (IntPtr handle) : base (handle)
        {
        }
        
        private void initCellLayout(nfloat screenWidth, nfloat scaleFactor)
        {
            this.pnlGuestHeader.Frame = new CGRect(1f * scaleFactor, 1f * scaleFactor, 22f * scaleFactor, 52f * scaleFactor);
            this.pnlGuestHeader.BackgroundColor = UIColor.FromRGB(200, 200, 200);//light gray
            //this.pnlGuestHeader.BackgroundColor = UIColor.FromRGB(220, 76, 24).CGColor;//Dark Orange

            this.pnlGuestContainer.Frame = new CGRect(this.pnlGuestHeader.Frame.Right + (1f * scaleFactor), 1f * scaleFactor,
                screenWidth - this.pnlGuestHeader.Frame.Width - (this.pnlGuestHeader.Frame.Height * (3f / 2f)) - (4f * scaleFactor), this.pnlGuestHeader.Frame.Height);
            this.pnlGuestButtonHolder.Frame = new CGRect(this.pnlGuestContainer.Frame.Right + (1f * scaleFactor), 1f * scaleFactor,
                this.pnlGuestHeader.Frame.Height * (3f / 2f), this.pnlGuestHeader.Frame.Height);
            this.pnlGuestButtonHolder.BackgroundColor = UIColor.FromRGB(255, 255, 255);
            this.pnlGuestButtonHolder.Layer.BorderColor = UIColor.FromRGB(255, 0, 0).CGColor;
            this.pnlGuestButtonHolder.Layer.BorderWidth = 0f;

            this.guestNameLabel.Frame = new CGRect(15f * scaleFactor, 10f * scaleFactor, this.pnlGuestContainer.Frame.Width - (30f * scaleFactor), 30f * scaleFactor);
            this.guestNameLabel.AdjustsFontSizeToFitWidth = true;
            
            this.btnGuestCommand.Font = UIFont.SystemFontOfSize(14f * scaleFactor);
            this.btnGuestCommand.SetTitleColor(UIColor.Red, UIControlState.Normal);

            btnGuestCommand.Hidden = false;
            imgGuestCommand.Hidden = true;
        }

        public void AddingCell(nfloat screenWidth, nfloat scaleFactor)
        {
            initCellLayout(screenWidth, scaleFactor);
            //this.pnlGuestButtonHolder.BackgroundColor = UIColor.FromRGB(255,255,255);
            //this.pnlGuestButtonHolder.Layer.BorderColor = UIColor.FromRGB(220, 76, 24).CGColor;//Dark Orange
            //this.pnlGuestButtonHolder.Layer.BorderColor = UIColor.FromRGB(255, 102, 36).CGColor;
            //this.pnlGuestButtonHolder.Layer.BorderWidth = (2f * scaleFactor);

            guestNameLabel.Text = AppDelegate.TextProvider.GetText(2164);
            guestNameLabel.Font = UIFont.SystemFontOfSize(20f * scaleFactor, UIFontWeight.Semibold);

            btnGuestCommand.Hidden = true;
            imgGuestCommand.Hidden = false;

            var image = UIImage.FromFile("Guest/AddButton");
            imgGuestCommand.Frame = new CGRect((this.pnlGuestButtonHolder.Frame.Width / 2f) - (47f * scaleFactor / 2f) , 3f * scaleFactor, 47f * scaleFactor, 47f * scaleFactor);
            imgGuestCommand.Image = image;
            //btnGuestCommand.SetBackgroundImage(image, UIControlState.Normal);
            //btnGuestCommand.SetTitle("", UIControlState.Normal);
            
        }

        public override void UpdateRow(Item element, nfloat screenWidth, nfloat scaleFactor)
        {
            if(element == null || !(element is GuestCellData) || element.Id == -1)
            {
                AddingCell(screenWidth, scaleFactor);
                return;
            }
            var guest = element as GuestCellData;

            initCellLayout(screenWidth, scaleFactor);

            //this.pnlGuestButtonHolder.BackgroundColor = UIColor.FromRGB(255, 0, 0);//Red

            guestNameLabel.Text = guest.PassengerData.FullName;
            guestNameLabel.Font = UIFont.SystemFontOfSize(20f * scaleFactor, UIFontWeight.Semibold);

            btnGuestCommand.Hidden = false;
            imgGuestCommand.Hidden = true;

            //var image = UIImage.FromFile("Guest/RemoveButton");
            btnGuestCommand.Frame = new CGRect(3f * scaleFactor, 3f * scaleFactor, this.pnlGuestButtonHolder.Frame.Width - (6f * scaleFactor), 47f * scaleFactor);
            //btnGuestCommand.SetImage(null, UIControlState.Normal);
            btnGuestCommand.SetBackgroundImage(null, UIControlState.Normal);
            btnGuestCommand.SetTitle("Delete",UIControlState.Normal);

#if !DEBUG
            btnGuestCommand.Hidden = true;
#endif
        }

        public override void SetDeleteButtonAction(UITableView parentTableView, NSIndexPath indexPath, Item item, DeleteItemHandler deleteAction)
        {
            if (deleteAction == null || btnGuestCommand == null || item == null || item.Id == -1) return;
            
            this.item = item;
            this.parentTableView = parentTableView;
            this.indexPath = indexPath;
            this.deleteAction = deleteAction;

            btnGuestCommand.TouchUpInside -= BtnGuestCommand_TouchUpInside;
            btnGuestCommand.TouchUpInside += BtnGuestCommand_TouchUpInside;
        }

        private void BtnGuestCommand_TouchUpInside(object sender, EventArgs e)
        {
            if (deleteAction != null) deleteAction.Invoke(item);
        }
    }
}