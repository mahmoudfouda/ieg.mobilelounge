﻿using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class AirportCell : UITableViewCell
    {
        public AirportCell(IntPtr handle) :base(handle)
        {

        }

        public void UpdateCell(string title, string description)
        {
            this.lblAirportCellTitle.AdjustsFontSizeToFitWidth = false;
            this.lblAirportCellDescription.AdjustsFontSizeToFitWidth = true;

            this.lblAirportCellTitle.Text = title;
            this.lblAirportCellDescription.Text = description;

        }

        public void UpdateLayout(nfloat height, AIMSViewController viewController)
        {
            var scaleFactor = viewController.ScaleFactor;

            nfloat leftMargin = 8f * scaleFactor;
            nfloat topMargin = 6f * scaleFactor;
            
            lblAirportCellTitle.Frame = new CGRect(leftMargin, topMargin,
                50f * scaleFactor, 16f * scaleFactor);

            lblAirportCellDescription.Frame = new CGRect(lblAirportCellTitle.Frame.X, lblAirportCellTitle.Frame.Bottom,
                260f * scaleFactor, 14f * scaleFactor);


            SetFonts(scaleFactor);
        }
        
        public void SetFonts(nfloat scaleFactor)
        {
            UIFont smallFont = UIFont.ItalicSystemFontOfSize(12f * scaleFactor), 
                largeFont = UIFont.BoldSystemFontOfSize(15f * scaleFactor);

            //if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            //{
            //    smallFont = UIFont.ItalicSystemFontOfSize(12f * (scaleFactor * 3f / 4f));
            //    largeFont = UIFont.SystemFontOfSize(20f * (scaleFactor * 3f / 4f));
            //}
            //else
            //{
            //    smallFont = UIFont.ItalicSystemFontOfSize(12f * scaleFactor);
            //    largeFont = UIFont.SystemFontOfSize(20f * scaleFactor);
            //}

            lblAirportCellTitle.Font = largeFont;
            lblAirportCellDescription.Font = smallFont;

        }
    }
}