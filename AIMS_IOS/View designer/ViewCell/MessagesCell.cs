using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class MessagesCell : UITableViewCell
    {

        public MessagesCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(string title, string description, string time)
		{
			messageTitle.Text = title;
			messageBody.Text = description;
            timeLabel.Text = time;
		}

        public void UpdateLayout(nfloat height, AgentTabMessageView viewController)
        {
            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ? 0 : (float)viewController.View.SafeAreaInsets.Left;

            var scaleFactor = viewController.ScaleFactor;
            var screenWidth = UIScreen.MainScreen.Bounds.Width - leftMargin;

            setFonts(scaleFactor);

            nfloat imgSize = 40f * scaleFactor;
            nfloat margin = 5f * scaleFactor;

            messageImg.Frame = new CGRect(margin, margin, imgSize, imgSize);

            timeLabel.Frame = new CGRect(messageImg.Frame.Right + margin, 0f, 
                screenWidth - messageImg.Frame.Right - (margin * 2f), 13f * scaleFactor);

            messageTitle.Frame = new CGRect(messageImg.Frame.Right + margin, 12f * scaleFactor,
                screenWidth - messageImg.Frame.Right - (margin * 2f), 26f * scaleFactor);

            messageBody.Frame = new CGRect(margin, messageImg.Frame.Bottom,
                screenWidth - (margin * 2f), 30f * scaleFactor);
        }

        private void setFonts(nfloat scaleFactor)
        {
            UIFont smallFont, mediumFont, largeFont;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                smallFont = UIFont.ItalicSystemFontOfSize(12f * (scaleFactor * 3f / 4f));
                mediumFont = UIFont.ItalicSystemFontOfSize(20f * (scaleFactor * 3f / 4f));
                largeFont = UIFont.BoldSystemFontOfSize(22f * (scaleFactor * 3f / 4f));
            }
            else
            {
                smallFont = UIFont.ItalicSystemFontOfSize(12f * scaleFactor);
                mediumFont = UIFont.ItalicSystemFontOfSize(20f * scaleFactor);
                largeFont = UIFont.BoldSystemFontOfSize(22f * scaleFactor);
            }

            timeLabel.Font = smallFont;
            timeLabel.TextColor = UIColor.FromRGB(100, 100, 100);
            messageTitle.Font = largeFont;
            messageBody.Font = mediumFont;
            messageBody.TextColor = UIColor.FromRGB(100, 100, 100);
        }

        //public override void LayoutSubviews()
        //{
        //    base.LayoutSubviews();
        //    UpdateLayout(this.ContentView.Frame.Height);
        //}

    }
}