﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("lastHourCell")]
    partial class lastHourCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnLastHourDelete { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnLastHourShowHost { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView passengerImg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel passengerLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel timeLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnLastHourDelete != null) {
                btnLastHourDelete.Dispose ();
                btnLastHourDelete = null;
            }

            if (btnLastHourShowHost != null) {
                btnLastHourShowHost.Dispose ();
                btnLastHourShowHost = null;
            }

            if (passengerImg != null) {
                passengerImg.Dispose ();
                passengerImg = null;
            }

            if (passengerLabel != null) {
                passengerLabel.Dispose ();
                passengerLabel = null;
            }

            if (timeLabel != null) {
                timeLabel.Dispose ();
                timeLabel = null;
            }
        }
    }
}