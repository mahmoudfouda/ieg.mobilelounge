using Foundation;
using System;
using UIKit;
using CoreGraphics;
using AIMS.Models;

namespace AIMS_IOS
{
    public delegate void PassengerActionHandler(AgentLastHourCellData item);
    public partial class lastHourCell : UITableViewCell
    {
        private AgentLastHourCellData item;
        private bool fail;
        private UITableView parentTableView;
        private NSIndexPath indexPath;
        private PassengerActionHandler deletePassnegerAction, showHostPassengerAction;
        private nfloat scaleFactor = 1f;

        private string failImg = "Passenger/passenger_red.png";
        private string normalImg = "Passenger/passenger_blue.png";
        private string guestImg = "Passenger/guest.png";

        public lastHourCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(AgentLastHourCellData item)
		{
            //passengerLabel.AdjustsFontSizeToFitWidth = true;
            this.item = item;
            fail = item.fail;
            passengerLabel.Text = item.passenger.FullName;
			timeLabel.Text = item.passenger.TrackingTimestamp.ToShortTimeString();
            btnLastHourShowHost.SetTitle(string.IsNullOrWhiteSpace(item.passenger.HostName) ? "Main PAX" : item.passenger.HostName, UIControlState.Normal);

            if (!string.IsNullOrEmpty(item.passenger.FailedReason))
            {
                if (item.passenger.FailedReason.Contains("\n"))
                    passengerLabel.Text = item.passenger.FailedReason.Substring(0, item.passenger.FailedReason.IndexOf("\n"));
                else
                    passengerLabel.Text = item.passenger.FailedReason;
            }

            if (fail)
                passengerImg.Image = UIImage.FromFile(failImg);
            else if (item.passenger.IsGuest)
                passengerImg.Image = UIImage.FromFile(guestImg);
            else
                passengerImg.Image = UIImage.FromFile(normalImg);

            btnLastHourShowHost.Hidden = !item.passenger.IsGuest;
            if (item.passenger.IsGuest)
            {
                //setting the BG to be Guest_Orange (If mike wants)
                //BackgroundColor = new UIColor(new CGColor(1f, 102f / 255f, 36f / 255f));

                //setting the image border to be Guest_Orange
                //passengerImg.Layer.BorderColor = new CGColor(1f, 102f / 255f, 36f / 255f);
                //passengerImg.Layer.BorderWidth = 1f * scaleFactor;

                //Shifting the controls to the right (for shiftingAmount scaled pixels)
                var shiftingAmount = 0f;// (20f * scaleFactor);//The indent of the guests (which was confusing)
                passengerImg.Frame = new CGRect(passengerImg.Frame.X + shiftingAmount, passengerImg.Frame.Y, 
                    passengerImg.Frame.Width, passengerImg.Frame.Height);
                passengerLabel.Frame = new CGRect(passengerImg.Frame.Right + passengerImg.Frame.Y, 8f * scaleFactor,
                    passengerLabel.Frame.Width - shiftingAmount, 22f * scaleFactor);
                timeLabel.Frame = new CGRect(passengerLabel.Frame.X, passengerLabel.Frame.Bottom, 55f * scaleFactor, 19f * scaleFactor);
                btnLastHourShowHost.Frame = new CGRect(timeLabel.Frame.Right, timeLabel.Frame.Top, 110f * scaleFactor, 19f * scaleFactor);
            }
		}

        public void setFonts(nfloat scaleFactor)
        {
            UIFont smallFont, smallRegularFont, deleteButtonFont;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                smallRegularFont = UIFont.SystemFontOfSize(11f * (scaleFactor * 3f / 4f));
                smallFont = UIFont.ItalicSystemFontOfSize(11f * (scaleFactor * 3f / 4f));
                deleteButtonFont = UIFont.SystemFontOfSize(13f * (scaleFactor * 3f / 4f));
                passengerLabel.Font = UIFont.SystemFontOfSize(16f * (scaleFactor * 3f / 4f));
            }
            else
            {
                smallRegularFont = UIFont.SystemFontOfSize(11f * scaleFactor);
                smallFont = UIFont.ItalicSystemFontOfSize(11f * scaleFactor);
                deleteButtonFont = UIFont.SystemFontOfSize(13f * scaleFactor);
                passengerLabel.Font = UIFont.SystemFontOfSize(16f * scaleFactor);
            }

            timeLabel.Font = smallFont;
            btnLastHourShowHost.Font = smallRegularFont;
            timeLabel.TextColor = UIColor.FromRGB(100, 100, 100);

            btnLastHourDelete.Font = deleteButtonFont;
            btnLastHourDelete.SetTitleColor(UIColor.Red, UIControlState.Normal);
        }

        public void UpdateLayout(nfloat height, nfloat screenWidth, nfloat scaleFactor)
        {
            this.scaleFactor = scaleFactor;
            var margin = 5f * scaleFactor;
            var deleteButtonWidth = height * 7f / 5f;
            //TODO: remove debug mode to make the delete button visible and working
#if !DEBUG
            btnLastHourDelete.Hidden = true;
            deleteButtonWidth = 0;
#endif
            btnLastHourDelete.Frame = new CGRect(screenWidth - deleteButtonWidth, 0f, deleteButtonWidth, height);
            
            nfloat imgSize = height - (2f * margin);// height - timeLabel.Frame.Bottom;
            passengerImg.Frame = new CGRect(margin, margin, imgSize, imgSize);

            passengerLabel.Frame = new CGRect(passengerImg.Frame.Right + margin, 8f * scaleFactor, //passengerImg.Frame.Y + (passengerImg.Frame.Height * 0.25f),
                screenWidth - passengerImg.Frame.Right - btnLastHourDelete.Frame.Width - (margin * 2f), 22f * scaleFactor);
            timeLabel.Frame = new CGRect(passengerLabel.Frame.X, passengerLabel.Frame.Bottom, 55f * scaleFactor, 19f * scaleFactor);
            btnLastHourShowHost.Frame = new CGRect(timeLabel.Frame.Right, timeLabel.Frame.Top, 110f * scaleFactor, 19f * scaleFactor);
            btnLastHourShowHost.Layer.CornerRadius = 4f * scaleFactor;

            setFonts(scaleFactor);
        }

        public void SetButtonActions(UITableView parentTableView, NSIndexPath indexPath, AgentLastHourCellData item, PassengerActionHandler deleteAction, PassengerActionHandler showHostAction)
        {
            if (deleteAction == null || btnLastHourDelete == null || item == null || item.passenger == null) return;

            this.item = item;
            fail = item.fail;
            this.parentTableView = parentTableView;
            this.indexPath = indexPath;
            this.deletePassnegerAction = deleteAction;
            this.showHostPassengerAction = showHostAction;

            btnLastHourDelete.TouchUpInside -= DeleteLastHour_TouchUpInside;
            btnLastHourDelete.TouchUpInside += DeleteLastHour_TouchUpInside;

            btnLastHourShowHost.TouchUpInside -= ShowHost_TouchUpInside;
            btnLastHourShowHost.TouchUpInside += ShowHost_TouchUpInside;
        }

        private void DeleteLastHour_TouchUpInside(object sender, EventArgs e)
        {
            if (deletePassnegerAction != null) deletePassnegerAction.Invoke(item);
        }

        private void ShowHost_TouchUpInside(object sender, EventArgs e)
        {
            if (showHostPassengerAction != null) showHostPassengerAction.Invoke(item);
        }

        //public override void LayoutSubviews()
        //{
        //    base.LayoutSubviews();
        //    updateLayout(this.ContentView.Frame.Height);
        //}
    }
}