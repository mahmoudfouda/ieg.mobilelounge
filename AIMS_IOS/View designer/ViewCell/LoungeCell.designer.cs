﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("LoungeCell")]
    partial class LoungeCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel cellDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView cellImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel cellTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.CustomProgressControl customProgress { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (cellDescription != null) {
                cellDescription.Dispose ();
                cellDescription = null;
            }

            if (cellImage != null) {
                cellImage.Dispose ();
                cellImage = null;
            }

            if (cellTitle != null) {
                cellTitle.Dispose ();
                cellTitle = null;
            }

            if (customProgress != null) {
                customProgress.Dispose ();
                customProgress = null;
            }
        }
    }
}