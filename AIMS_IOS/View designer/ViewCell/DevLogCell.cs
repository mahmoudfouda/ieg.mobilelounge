﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class DevLogCell : UITableViewCell
    {

        public DevLogCell(IntPtr handle) : base(handle)
        {
        }

        public void setFonts(nfloat scaleFactor)
        {
            UIFont smallFont, mediumFont, largeFont;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                smallFont = UIFont.ItalicSystemFontOfSize(12f * (scaleFactor * 3f / 4f));
                mediumFont = UIFont.SystemFontOfSize(14f * (scaleFactor * 3f / 4f));
                largeFont = UIFont.SystemFontOfSize(20f * (scaleFactor * 3f / 4f));
            }
            else
            {
                smallFont = UIFont.ItalicSystemFontOfSize(12f * scaleFactor);
                mediumFont = UIFont.SystemFontOfSize(14f * scaleFactor);
                largeFont = UIFont.SystemFontOfSize(20f * scaleFactor);
            }

            dateLabel.Font = smallFont;
            dateLabel.TextColor = UIColor.FromRGB(100, 100, 100);
            titleLabel.Font = largeFont;
            devLogDescription.Font = mediumFont;
            devLogDescription.TextColor = UIColor.FromRGB(100, 100, 100);
        }

        public void UpdateCell(string title, string contents, string date, bool isDeveloperLog)
        {
            titleLabel.Text = title;
            devLogDescription.Text = contents;
            dateLabel.Text = date;

            if (isDeveloperLog)
                logImg.Image = new UIImage("Log/log_dev2_small.png");
            else
                logImg.Image = new UIImage("Log/log_small.png");
        }

        public void UpdateLayout(nfloat height, AgentTabDevLogs viewController)
        {
            var scaleFactor = viewController.ScaleFactor;
            var screenWidth = UIScreen.MainScreen.Bounds.Width;

            setFonts(scaleFactor);

            nfloat margin = 5f * scaleFactor;


            nfloat imgSize = height - (2f * margin);
            logImg.Frame = new CGRect(margin, margin, imgSize, imgSize);

            dateLabel.Frame = new CGRect(logImg.Frame.Right, 0f, screenWidth - logImg.Frame.Right - (3f * margin), 13f * scaleFactor);

            titleLabel.Frame = new CGRect(logImg.Frame.Right + margin, 8f * scaleFactor,
                screenWidth - logImg.Frame.Right - (margin * 2f), 22f * scaleFactor);

            devLogDescription.Frame = new CGRect(titleLabel.Frame.X, titleLabel.Frame.Bottom,
                titleLabel.Frame.Width, 19f * scaleFactor);
            //setFonts();

            // rowHeight = height;

            // nfloat margin = 5f;

            // dateLabel.Frame = new CGRect(margin, 0f, this.ContentView.Frame.Width - (margin * 2f),
            //     height * 0.0714f);
            // dateLabel.AdjustsFontSizeToFitWidth = true;

            // nfloat imgSize = height * 0.25f;

            // logImg.Frame = new CGRect(margin, dateLabel.Frame.Bottom, imgSize, imgSize);


            // titleLabel.Frame = new CGRect(logImg.Frame.Right + margin, logImg.Frame.Y,
            //     this.ContentView.Frame.Width - logImg.Frame.Right - (margin * 2f), logImg.Frame.Height);

            //nfloat messH =  messageLabel.SizeThatFits(new CGSize(this.ContentView.Frame.Width - (margin * 2f), float.MaxValue)).Height;

            // messageLabel.Frame = new CGRect(margin, logImg.Frame.Bottom,
            //     this.ContentView.Frame.Width - (margin * 2f), messH);


            // //messageLabel.Frame = new CGRect(margin, logImg.Frame.Bottom,
            // //    this.ContentView.Frame.Width - (margin * 2f), height - logImg.Frame.Bottom - (margin * 2f));

        }
        
        //public nfloat getHeight()
        //{
        //    return messageLabel.Frame.Bottom + 5f;

        //}

    }
}
