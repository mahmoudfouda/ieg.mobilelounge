﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("LangCell")]
    partial class LangCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView cellImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel langLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (cellImage != null) {
                cellImage.Dispose ();
                cellImage = null;
            }

            if (langLabel != null) {
                langLabel.Dispose ();
                langLabel = null;
            }
        }
    }
}