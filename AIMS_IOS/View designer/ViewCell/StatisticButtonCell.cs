﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;

namespace AIMS_IOS
{
    public class StatisticButtonCell : UICollectionViewCell
    {
        private UIImageView imageView;
        private UILabel labelView;
        private nfloat scaleFactor;
        private UIColor selectedColor;

        [Export("initWithFrame:")]
        public StatisticButtonCell(CGRect frame) : base(frame)
        {
            ContentView.Layer.BorderColor = UIColor.LightGray.CGColor;
            ContentView.Layer.BorderWidth = 0f;
            ContentView.BackgroundColor = UIColor.Clear;
            ContentView.Transform = CGAffineTransform.MakeScale(0.8f, 0.8f);
        }

        public void SetSelectedColor(UIColor selectedColor)
        {
            //SelectedBackgroundView = new UIView {  BackgroundColor = selectedColor,  };
            this.selectedColor = selectedColor;
        }

        public override bool Selected
        {
            get
            {
                return base.Selected;
            }
            set
            {
                if (base.Selected != value)
                    base.Selected = value;

                if (imageView == null || labelView == null)
                    return;

                if (value)
                {
                    imageView.TintColor = selectedColor;
                    labelView.TextColor = selectedColor;
                }
                else
                {
                    imageView.TintColor = UIColor.Black;
                    labelView.TextColor = UIColor.Black;
                }
            }
        }

        public void UpdateLayout(int index, nfloat scaleFactor)
        {
            this.scaleFactor = scaleFactor;

            imageView = new UIImageView(new CGRect(0f, 0f, this.Frame.Width, 20f * scaleFactor))
            {
                Image = index == 0 ? new UIImage("Tab/ntab_stat_graph.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate) : 
                                     new UIImage("Tab/ntab_stat_bar.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate),
                Center = ContentView.Center,
                Transform = CGAffineTransform.MakeScale(0.7f, 0.7f)
            };

            imageView.TintColor = UIColor.Black;

            labelView = new UILabel(new CGRect(0f, imageView.Frame.Bottom, this.Frame.Width, 10f * scaleFactor))
            {
                Text = index == 0 ? AppDelegate.TextProvider.GetText(2158) : AppDelegate.TextProvider.GetText(2159),
                TextAlignment = UITextAlignment.Center,
                LineBreakMode = UILineBreakMode.CharacterWrap
            };

            string regularFontName = "Helvetica Neue";
            nfloat xmicroTexts = 9f * scaleFactor;

            UIFont microFont = UIFont.FromName(regularFontName, xmicroTexts);
            labelView.Font = microFont;

            AddSubview(imageView);
            AddSubview(labelView);
        }
    }
}