﻿using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace AIMS_IOS
{
    public partial class CardsCell : UICollectionViewCell
    {
        public nfloat ScaleFactor
        {
            get
            {
                return AppDelegate.AirlinesView.ScaleFactor;
            }
        }

        public bool HasHolderImage { get; private set; }
        //private bool isImageSet = false;

        public CardsCell (IntPtr handle) : base (handle)
        {
        }

        public void updateCell(UIImage image, string footer, CGPoint frameSize, bool realImage = true)
        {
            #region Old Codes

            //cardImage.Frame = new CGRect(0f, 0f, frameSize.X, frameSize.X * 0.571f);
            //cardLabel.Frame = new CGRect(0f, cardImage.Frame.Bottom, frameSize.X,
            //    frameSize.Y - cardImage.Frame.Bottom);

            //cardImage.Layer.MasksToBounds = true;
            //cardImage.Layer.CornerRadius = 4f;
            //cardImage.Layer.BorderColor = new CGColor(0.5f,1f);
            //cardImage.BackgroundColor = new UIColor(new CGColor(0.5f, 1f));
            //cardImage.Layer.BorderWidth = 2f;
            //cardImage.Image = image;
            //cardLabel.Text = footer;
            //cardLabel.AdjustsFontSizeToFitWidth = true;
            #endregion

            #region Updating Layout
            //cardLabel.AdjustsFontSizeToFitWidth = true;
            var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;

            //if (isImageSet) return;
            //to prevent setting the item's Image every time on every scroll
            cardImage.Frame = new CGRect(0f, 0f, frameSize.X, frameSize.X * 0.571f);
            cardImage.Layer.CornerRadius = 10f;
            cardLabel.Frame = new CGRect(0f, cardImage.Frame.Bottom, frameSize.X,
                frameSize.Y - cardImage.Frame.Bottom); 
            #endregion

            cardImage.BackgroundColor = new UIColor(new CGColor(0.5f, 0f));
            cardImage.Image = image;
            HasHolderImage = !realImage;
            cardLabel.Text = footer;
            
            setFonts();
        }
        
        public void setFonts()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                //cardLabel.font = UIFont.FromName("font name", 20f);
                //if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                //    cardLabel.Font = UIFont.SystemFontOfSize(17f);
                //else cardLabel.Font = UIFont.SystemFontOfSize(9f);

                cardLabel.Font = UIFont.SystemFontOfSize(8f * ScaleFactor);

                cardLabel.LineBreakMode = UILineBreakMode.WordWrap;
                cardLabel.TextAlignment = UITextAlignment.Center;
                cardLabel.Lines = 2;
            }
            else
            {
                cardLabel.Font = UIFont.SystemFontOfSize(14f * ScaleFactor);

                cardLabel.LineBreakMode = UILineBreakMode.TailTruncation;
                cardLabel.TextAlignment = UITextAlignment.Center;
                cardLabel.Lines = 1;
            }
        }
    }
}