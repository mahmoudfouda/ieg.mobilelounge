﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace AIMS_IOS
{
    public class GraphDataCell
    {
        public DateTime Time { get; internal set; }
        public double Total { get; internal set; }
        public int PassengerType { get; internal set; }
    }
}