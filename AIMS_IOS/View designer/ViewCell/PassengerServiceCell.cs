﻿using AIMS.Models;
using Foundation;
using System;
using UIKit;
using AIMS_IOS.Utility;

namespace AIMS_IOS
{
    public partial class PassengerServiceCell : UICollectionViewCell
    {
        public static NSString ReusableCellId { get; } = new NSString("PassengerServiceReusableCell");

        public UIImageView ImageView { get { return this.imgPassengerService; } }

        public PassengerServiceCell (IntPtr handle) : base (handle)
        {
        }

        public void UpdateCell(PassengerService passengerService, nfloat scaleFactor)
        {
            if (passengerService == null) return;

            var cellMargin = 10f * scaleFactor;
            this.imgPassengerService.Frame = new CoreGraphics.CGRect(cellMargin, cellMargin, this.Frame.Width - (2f * cellMargin), this.Frame.Height / 2f);
            this.passengerServiceView.Frame = new CoreGraphics.CGRect(0, imgPassengerService.Frame.Bottom, this.Frame.Width, this.Frame.Height - imgPassengerService.Frame.Height - cellMargin);
            //this.lblPassengerServiceName.Frame = new CoreGraphics.CGRect(5f * scaleFactor, 5f * scaleFactor, passengerServiceView.Frame.Width - cellMargin, passengerServiceView.Frame.Height - cellMargin);//It is hidden
            this.lblPassengerServiceName.Hidden = true;
            this.lblPSQuantity.Frame = new CoreGraphics.CGRect(5f * scaleFactor, 5f * scaleFactor, (passengerServiceView.Frame.Width - cellMargin) / 2f, passengerServiceView.Frame.Height - cellMargin);
            this.lblPSMaxQuantity.Frame = new CoreGraphics.CGRect(this.lblPSQuantity.Frame.Right, 5f * scaleFactor, (passengerServiceView.Frame.Width - cellMargin) / 2f, passengerServiceView.Frame.Height - cellMargin);

            //var fontScaleFactor = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad ? scaleFactor * ((float)2.0f / 3.0f) : scaleFactor;

            this.lblPassengerServiceName.Font = UIFont.FromName("Helvetica Neue", 18f * scaleFactor);
            this.lblPSQuantity.Font = UIFont.FromName("Helvetica Neue", 20f * scaleFactor);
            this.lblPSQuantity.TextColor = UIColor.White;
            this.lblPSMaxQuantity.Font = UIFont.FromName("Helvetica Neue", 12f * scaleFactor);
            this.lblPSMaxQuantity.TextColor = UIColor.White;

            //this.Layer.BorderColor = new CoreGraphics.CGColor(1f, 102f/255f, 36f/255f);
            this.Layer.BorderColor = UIColor.Clear.FromHexString("#9370db").CGColor;
            this.passengerServiceView.BackgroundColor = new UIColor(this.Layer.BorderColor);
            this.Layer.BorderWidth = 1f * scaleFactor;

            UIImage image = null;
            switch (passengerService.Id)
            {
                case 0:
                    //none
                    image = UIImage.FromFile("Guest/tab_passengers_off_big");
                    break;
                case 1:
                    //complementary
                    image = UIImage.FromFile("Guest/GuestOff");
                    break;
                case 2:
                    //family
                    image = UIImage.FromFile("Guest/FamilyGuestOff");
                    break;
                case 3:
                    //paid
                    image = UIImage.FromFile("Guest/PaidGuestOff");
                    break;
                default:
                    //defaultimage
                    image = UIImage.FromFile("Guest/tab_passengers_off_big");
                    break;
            }
            if(image != null)
                this.imgPassengerService.Image = image;

            //this.lblPassengerServiceName.Text = string.Format("{0} / {1}", passengerService.Quantity, passengerService.MaximumQuantity);//It is hidden

            this.lblPSQuantity.Text = passengerService.Quantity.ToString();
            this.lblPSMaxQuantity.Text = string.Format("/ {0}", passengerService.MaximumQuantity);
        }
    }
}