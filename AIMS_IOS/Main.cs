﻿using System;
using System.Reflection;
using UIKit;

namespace AIMS_IOS
{
    public class Application
    {
        // This is the main entry point of the application.
        static void Main(string[] args)
        {
            try
            {
                // if you want to use a different Application Delegate class from "AppDelegate"
                // you can specify it here.
                AppDomain.CurrentDomain.UnhandledException += (s, e) =>
                {
                    throw (e.ExceptionObject as Exception);
                };

                UIApplication.Main(args, null, "AppDelegate");
                // this is a sample comment
            }
            catch (Exception ex)
            {
                try
                {
                    AIMS.LogsRepository.AddError("Error in Application.Main()", ex);
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                }
                catch
                {
                    throw;
                }
                throw;
            }
        }
    }
}
