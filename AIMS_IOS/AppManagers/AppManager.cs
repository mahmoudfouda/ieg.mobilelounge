﻿using System;
using System.Collections.Generic;
using UIKit;
using CoreLocation;
using AIMS;
using System.Linq;
using AIMS.Models;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public delegate void RepositoryChangedHandler(object itemsList);

    public static class AppManager
    {
        public const string AIMS_LOUNGE_IMAGE_NAME = "AIMS_Lounge_{0}.png";


        /// <summary>
        /// Initialisation and setting the default language
        /// </summary>
        public static void initialCurrentLang()
        {
            if (LanguageRepository.Current.SelectedLanguage.HasValue)
            {
                var lang = LanguageRepository.Current.Languages.FirstOrDefault(x => x.Key == LanguageRepository.Current.SelectedLanguage.Value);
                AppData.selectedLanguage = new LanguageCellData(lang.Value, lang.Key);
            }
        }

        public static void getListOfAgentMessages(RepositoryChangedHandler callback = null)
        {
            var messagesRepository = new MessageRepository();
            messagesRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };
            messagesRepository.LoadAllMessages();
        }

        #region TODO: Bad practice (Siavash)
        /// <summary>
        /// Gets the fixed last percentage of lounge occupancy
        /// </summary>
        /// <param name="selectedMobileWorkstation">The instance of the selected workstation</param>
        /// <param name="callback">Callback method with response</param>
        public static void GetLoungeOccupancyWhithQueue(Workstation selectedMobileWorkstation, RepositoryChangedHandler callback = null)
        {
            if (callback == null || selectedMobileWorkstation == null) return;

            var repo = new OccupancyRepository();
            //            repo.OnLoungeOccupancyReceived += (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancy occupancy) =>
            //            {
            //                if (isSucceeded)
            //                    callback.Invoke(occupancy);
            //                //else
            //                //    callback.Invoke(null);
            //#if DEBUG
            //                if(!isSucceeded)
            //                    System.Diagnostics.Debug.WriteLine($"Error in OccupancyRepository.GetLoungeOccupancy():{message}\n{errorMetadata}");
            //#endif
            //            };
            // repo.GetLoungeOccupancy(selectedMobileWorkstation);

            repo.LoadOccupancyWithQueue(selectedMobileWorkstation, (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancy occupancy) =>
            {
                if (isSucceeded)
                    callback.Invoke(occupancy);
                //else
                //    callback.Invoke(null);
#if DEBUG
                if (!isSucceeded)
                    System.Diagnostics.Debug.WriteLine($"Error in OccupancyRepository.GetLoungeOccupancy():{message}\n{errorMetadata}");
#endif
            });
        }

        public static void ClearQueue()
        {
            OccupancyRepository.ClearQueue();
        }

        public static void GetLoungeOccupancy(Workstation selectedMobileWorkstation, RepositoryChangedHandler callback = null)
        {
            if (callback == null || selectedMobileWorkstation == null) return;

            var repo = new OccupancyRepository();
            repo.OnLoungeOccupancyReceived += (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancy occupancy) =>
            {
                if (isSucceeded)
                {
                    MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy = occupancy;
                    callback.Invoke(occupancy);
                }
                else
                    callback.Invoke(null);
#if DEBUG
                if (!isSucceeded)
                    System.Diagnostics.Debug.WriteLine($"Error in OccupancyRepository.GetLoungeOccupancy():{message}\n{errorMetadata}");
#endif
            };
            repo.GetLoungeOccupancy(selectedMobileWorkstation);
        }

        public static void GetRetrieveCachedLoungeOccupancies(decimal loungeID, Workstation selectedMobileWorkstation, DateTime fromLocalDate, DateTime toLocalDate, int intervalInSeconds, RepositoryChangedHandler callback = null)
        {
            if (callback == null || loungeID == 0)
                return;

            var repo = new OccupancyRepository();
            repo.OnLoungeOccupancyCachdReceived += (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancyElapsedData occupancyList) =>
            {
                if (occupancyList == null)
                    occupancyList = new LoungeOccupancyElapsedData();

                if (callback != null)
                    callback.Invoke(occupancyList);

                // if (callback != null && isSucceeded)
                //     callback.Invoke(occupancyList);
            };

            repo.RetrieveCachedLoungeOccupancies(loungeID, selectedMobileWorkstation.WorkstationID, selectedMobileWorkstation.AirportCode, fromLocalDate, toLocalDate, intervalInSeconds);
        }


        public static void getLoungeOccupanciesList(List<decimal> loungeIDs, List<decimal> workstationIDs, List<string> airportCodes, DateTime fromLocalDate, DateTime toLocalDate, int intervalInSeconds, RepositoryChangedHandler callback = null)
        {
            if (callback == null ||
                loungeIDs == null || loungeIDs.Count == 0)
                return;

            var repo = new OccupancyRepository();
            repo.OnLoungeOccupancyListReceived += (bool isSucceeded, int? errorCode, string errorMetadata, string message, List<LoungeOccupancy> occupancyList) =>
            {
                if (callback != null)
                    callback.Invoke(occupancyList);

                //if (callback != null && isSucceeded)
                //    callback.Invoke(occupancyList);
            };

            repo.RetrieveLastOccupanciesList(loungeIDs, workstationIDs, airportCodes, fromLocalDate, toLocalDate, intervalInSeconds);
        }

        public static void getAgentLastHour(RepositoryChangedHandler callback = null)
        {
            var passengerRepository = new PassengersRepository();
            passengerRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };
            passengerRepository.LoadLastHourPassengers();
        }

        public static void searchAgentLastHour(string keyword, RepositoryChangedHandler callback = null)
        {
            if (string.IsNullOrWhiteSpace(keyword) || keyword.Length < 3) return;//This is the rule

            var passengerRepository = new PassengersRepository();
            passengerRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };
            passengerRepository.SearchPassengers(keyword);
        }

        public static void searchAgentLastHour(string keyWord, bool airportSearch, DateTime dateTime, int pageIndex = 1, bool loadMore = false, RepositoryChangedHandler callback = null)
        {
           // if (string.IsNullOrWhiteSpace(keyWord) || keyWord.Length < 3) return;//This is the rule

            var passengerRepository = new PassengersRepository();
            passengerRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };
            passengerRepository.SearchPassengers(keyWord, airportSearch, dateTime, pageIndex, loadMore);
        }



        public static void getAgentFailedLastHour(RepositoryChangedHandler callback = null)
        {
            var passengerRepository = new PassengersRepository();
            passengerRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };
            passengerRepository.LoadFailedPassengers();
        }
        #endregion

        public static void StopTimerOccupancy()
        {
            var repo = new OccupancyRepository();
            repo.StopTimer();
        }

        public static void getAgentTabLogs(RepositoryChangedHandler callback = null)
        {
            var logsRepository = new LogsRepository();
            logsRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };

            logsRepository.LoadClientOnlyLogs();
        }

        public static void getAgentDevTabLogs(RepositoryChangedHandler callback = null)
        {
            var logsRepository = new LogsRepository();
            logsRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };

            logsRepository.LoadDeveloperOnlyLogs();
        }

        public static string getGPSLocation()
        {
            CLLocationManager LocationManager = new CLLocationManager();

            return LocationManager.Location.ToString();
        }

        public static List<LanguageCellData> getListOfLanguges()
        {
            return LanguageRepository.Current.Languages.Select(x => new LanguageCellData(x.Value, x.Key)).ToList(); ;
        }

        public static List<DropDownCell> getListOfLangugeDrpItems()
        {
            return LanguageRepository.Current.Languages.Select(x => new DropDownCell(
                UIImage.FromFile(string.Format("Flag/Flag_{0}.png", (int)x.Key)), x.Value)).ToList();
        }

        internal static void searchAgentLastHour(string text, bool airportSearch, DateTime selectedDate, int currentPage, object loadMore, Action<object> p)
        {
            throw new NotImplementedException();
        }

        public static void SetSelectedLanguage(LanguageCellData selectedLanguageCell)
        {
            AppData.selectedLanguage = selectedLanguageCell;
            if (selectedLanguageCell == null)
                LanguageRepository.Current.SelectedLanguage = null;
            else
                LanguageRepository.Current.SelectedLanguage = selectedLanguageCell.language;
        }

        public static List<LoungeCellData> getListOfLounges()
        {
            return MembershipProvider.Current.UserLounges.Select(x => new LoungeCellData(x)).ToList();
        }

        public static List<decimal> getListOfLoungeIDs()
        {
            return MembershipProvider.Current.UserLounges.Select(c => c.LoungeID).ToList();
        }

        public static List<string> getListOfAirportCodes()
        {
            return MembershipProvider.Current.UserLounges.Select(c => c.AirportCode).ToList();
        }

        public static List<decimal> getListOfWorkstationIDs()
        {
            return MembershipProvider.Current.UserLounges.Select(c => c.WorkstationID).ToList();
        }

        public static void getAirlines(RepositoryChangedHandler callback = null, bool forceGet = false)
        {
            var airlineRepository = new AirlinesRepository();
            airlineRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };

            airlineRepository.LoadAirlines(forceGet);
        }

        public static void searchAirlines(string keyword, RepositoryChangedHandler callback = null)
        {
            var airlineRepository = new AirlinesRepository();
            airlineRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };

            airlineRepository.SearchAirlines(keyword);
        }

        public static void getCards(RepositoryChangedHandler callback = null)
        {
            var cardsRepository = new CardsRepository();
            cardsRepository.OnRepositoryChanged += (items) =>
            {
                if (callback != null)
                    callback.Invoke(items);
            };

            cardsRepository.LoadCards();
        }

        public static void validatePassengerInfo(OnPassengerCheckedHandler callback, string barcode = "")
        {
            try
            {
                var pr = new PassengersRepository();
                pr.OnPassengerChecked += callback;

                if (string.IsNullOrEmpty(barcode))
                    pr.ValidatePassengerInfo(MembershipProvider.Current.SelectedLounge);
                else pr.GetPassengerInfo(barcode, MembershipProvider.Current.SelectedLounge);
            }
            catch (Exception ex)
            {
                AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051));
                LogsRepository.AddError("Passenger Validation call error", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }

        }

        public static void validatePassengerInfoWithSelectedCard(OnPassengerCheckedHandler callback, string cardBarcode = "")
        {
            try
            {
                var pr = new PassengersRepository();
                pr.OnPassengerChecked += callback;

                pr.ValidatePassengerWithCard(MembershipProvider.Current.SelectedLounge, cardBarcode);
            }
            catch (Exception ex)
            {
                AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051));
                LogsRepository.AddError("Passenger Validation call error", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }

        }

        public static void retrieveMainPassengerOf(OnPassengerCheckedHandler callback, Passenger guest = null)
        {
            if (guest == null && MembershipProvider.Current.SelectedPassenger == null) return;
            try
            {
                var pr = new PassengersRepository();
                pr.OnPassengerChecked += callback;
                pr.RetrieveMainPassenger(guest == null ? MembershipProvider.Current.SelectedPassenger : guest, MembershipProvider.Current.SelectedLounge);
            }
            catch (Exception ex)
            {
                AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051));
                LogsRepository.AddError("Passenger Retrieval call error", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }

        }

        public static void retrieveGuestPassenger(OnPassengerCheckedHandler callback, Passenger guest = null)
        {
            if (guest == null && MembershipProvider.Current.SelectedPassenger == null) return;
            try
            {
                var pr = new PassengersRepository();
                pr.OnPassengerChecked += callback;
                pr.RetrieveGuestPassenger(guest == null ? MembershipProvider.Current.SelectedPassenger : guest, MembershipProvider.Current.SelectedLounge);
            }
            catch (Exception ex)
            {
                AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051));
                LogsRepository.AddError("Passenger Retrieval call error", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }

        }

        public static void validateGuestInfo(Passenger guestPassengerInfo, PassengerServiceType guestTypeId, OnPassengerCheckedHandler callback)
        {
            if (guestPassengerInfo == null) return;
            try
            {
                var pr = new PassengersRepository();
                pr.OnPassengerChecked += callback;
                pr.ValidateGuestInfo(MembershipProvider.Current.SelectedPassenger, guestPassengerInfo, guestTypeId, MembershipProvider.Current.SelectedLounge);
            }
            catch (Exception ex)
            {
                AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051));
                LogsRepository.AddError("Guest Validation call error", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }

        }

        public static void validateGuestInfo(string guestBarcode, int guestTypeId, OnPassengerCheckedHandler callback)
        {
            try
            {
                var pr = new PassengersRepository();
                pr.OnPassengerChecked += callback;
                pr.GetGuestInfo(MembershipProvider.Current.SelectedPassenger, guestBarcode, (PassengerServiceType)guestTypeId, MembershipProvider.Current.SelectedLounge);
            }
            catch (Exception ex)
            {
                AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051));
                LogsRepository.AddError("Guest Validation call error", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }

        }

        public static void rejectPassenger()
        {
            try
            {
                using (var pr = new PassengersRepository())
                {
                    pr.AgentRejectPassenger(MembershipProvider.Current.SelectedLounge);
                }
            }
            catch (Exception ex)
            {
                AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051));
                LogsRepository.AddError("Passenger deny call error", ex);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }
        }

        public static event EventHandler OnUpdateProgress;

        public static void UpdateProgress()
        {
            OnUpdateProgress?.Invoke(new Object(), new EventArgs());
        }

        public static void GetDwellLoungeOccupancies(decimal loungeID, Workstation selectedMobileWorkstation, DateTime fromLocalDate, DateTime toLocalDate, int intervalInSeconds, RepositoryChangedHandler callback)
        {
            if (callback == null || loungeID == 0)
                return;

            var repo = new OccupancyRepository();
            repo.OnLoungeOccupancyCachdReceived += (bool isSucceeded, int? errorCode, string errorMetadata, string message, LoungeOccupancyElapsedData occupancyList) =>
            {
                if (occupancyList == null)
                    occupancyList = new LoungeOccupancyElapsedData();

                if (callback != null)
                    callback.Invoke(occupancyList);

             //   if (callback != null && isSucceeded)
             //       callback.Invoke(occupancyList);
            };

            repo.GetDwellLoungeOccupancies(loungeID, selectedMobileWorkstation.WorkstationID, selectedMobileWorkstation.AirportCode, fromLocalDate, toLocalDate, intervalInSeconds);
        }
    }
}
