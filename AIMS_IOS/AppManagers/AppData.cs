﻿using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace AIMS_IOS
{
    public static class AppData//TODO: this class is not necessary (refactor: move these variables to AppDelegate or move all variables here)
    {
        public static LanguageCellData selectedLanguage = null;
        
        public static bool LoungeChanged { get; set; }
        private static LoungeCellData selectedLounge = null;
        public static LoungeCellData SelectedLounge
        {
            get { return selectedLounge; }
            set
            {
                selectedLounge = value;
                LoungeChanged = true;
            }
        }

        public static AirlineCellData usersAirLine;
        public static AirlineCellData selectedAirLine;
        public static CardCellData selectedCard;
        public static string[] name = new string[2];
        public static bool userInitialisationStep = false;
        public static List<LanguageCellData> AppListLang;

        public static bool devMode = false;

        //public static bool FrontCamMode = true;

        public static bool fromAirline = true;

        public static PassengerValidation ValidationResult { get; set; }
        public static UIImage LoungeImage { get; internal set; }
    }
}