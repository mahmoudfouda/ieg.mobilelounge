﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("languageSelectionViewController")]
    partial class languageSelectionViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView langTable { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView logo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView masterView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView titleView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (langTable != null) {
                langTable.Dispose ();
                langTable = null;
            }

            if (logo != null) {
                logo.Dispose ();
                logo = null;
            }

            if (masterView != null) {
                masterView.Dispose ();
                masterView = null;
            }

            if (titleView != null) {
                titleView.Dispose ();
                titleView = null;
            }
        }
    }
}