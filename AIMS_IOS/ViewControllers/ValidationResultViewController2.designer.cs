﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("ValidationResultViewController2")]
    partial class ValidationResultViewController2
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView bgOverlayViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnBGCloseVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton confirmButton2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView curvedViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton detailsButton2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.ValidationDetailsView detailsViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView flightStatsViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView flightViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgFromVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView imgSelectedAirlineViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgSelectedAirlineVR2 { get; set; }

        [Outlet]
        [GeneratedCode("iOS Designer", "1.0")]
        UIKit.UIView reentryView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgSelectedCardOtherVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgSelectedCardVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgTimeVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgToVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgWeatherVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblBoardingDateVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblBoardingTimeLabelVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblBoardingTimeVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblCardNameVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblCardOtherNameVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFFNLabelVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFFNVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFlightLabelVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFlightVR2 { get; set; }

        [Outlet]
        [GeneratedCode("iOS Designer", "1.0")]
        UIKit.UILabel lblreentry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFromFullAirportNameVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFromVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFullNameVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblGateLabelVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblGateVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblPNRLabelVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblPNRVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSeatLabelVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSeatVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblTerminalLabelVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblTerminalVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblTimeVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblToFullAirportNameVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblToVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblWeatherVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView passengerViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView scrollViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView selectedAirlineViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UICollectionView servicesCollectionViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView servicesViewVR2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.TiledView tiledViewBottomCenter { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.TiledView tiledViewBottomLeft { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.TiledView tiledViewBottomRight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.TiledView tiledViewTop { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (bgOverlayViewVR2 != null) {
                bgOverlayViewVR2.Dispose ();
                bgOverlayViewVR2 = null;
            }

            if (btnBGCloseVR2 != null) {
                btnBGCloseVR2.Dispose ();
                btnBGCloseVR2 = null;
            }

            if (confirmButton2 != null) {
                confirmButton2.Dispose ();
                confirmButton2 = null;
            }

            if (curvedViewVR2 != null) {
                curvedViewVR2.Dispose ();
                curvedViewVR2 = null;
            }

            if (detailsButton2 != null) {
                detailsButton2.Dispose ();
                detailsButton2 = null;
            }

            if (detailsViewVR2 != null) {
                detailsViewVR2.Dispose ();
                detailsViewVR2 = null;
            }

            if (flightStatsViewVR2 != null) {
                flightStatsViewVR2.Dispose ();
                flightStatsViewVR2 = null;
            }

            if (flightViewVR2 != null) {
                flightViewVR2.Dispose ();
                flightViewVR2 = null;
            }

            if (imgFromVR2 != null) {
                imgFromVR2.Dispose ();
                imgFromVR2 = null;
            }

            if (imgSelectedAirlineViewVR2 != null) {
                imgSelectedAirlineViewVR2.Dispose ();
                imgSelectedAirlineViewVR2 = null;
            }

            if (imgSelectedAirlineVR2 != null) {
                imgSelectedAirlineVR2.Dispose ();
                imgSelectedAirlineVR2 = null;
            }

            if (imgSelectedCardOtherVR2 != null) {
                imgSelectedCardOtherVR2.Dispose ();
                imgSelectedCardOtherVR2 = null;
            }

            if (imgSelectedCardVR2 != null) {
                imgSelectedCardVR2.Dispose ();
                imgSelectedCardVR2 = null;
            }

            if (imgTimeVR2 != null) {
                imgTimeVR2.Dispose ();
                imgTimeVR2 = null;
            }

            if (imgToVR2 != null) {
                imgToVR2.Dispose ();
                imgToVR2 = null;
            }

            if (imgWeatherVR2 != null) {
                imgWeatherVR2.Dispose ();
                imgWeatherVR2 = null;
            }

            if (lblBoardingDateVR2 != null) {
                lblBoardingDateVR2.Dispose ();
                lblBoardingDateVR2 = null;
            }

            if (lblBoardingTimeLabelVR2 != null) {
                lblBoardingTimeLabelVR2.Dispose ();
                lblBoardingTimeLabelVR2 = null;
            }

            if (lblBoardingTimeVR2 != null) {
                lblBoardingTimeVR2.Dispose ();
                lblBoardingTimeVR2 = null;
            }

            if (lblCardNameVR2 != null) {
                lblCardNameVR2.Dispose ();
                lblCardNameVR2 = null;
            }

            if (lblCardOtherNameVR2 != null) {
                lblCardOtherNameVR2.Dispose ();
                lblCardOtherNameVR2 = null;
            }

            if (lblFFNLabelVR2 != null) {
                lblFFNLabelVR2.Dispose ();
                lblFFNLabelVR2 = null;
            }

            if (lblFFNVR2 != null) {
                lblFFNVR2.Dispose ();
                lblFFNVR2 = null;
            }

            if (lblFlightLabelVR2 != null) {
                lblFlightLabelVR2.Dispose ();
                lblFlightLabelVR2 = null;
            }

            if (lblFlightVR2 != null) {
                lblFlightVR2.Dispose ();
                lblFlightVR2 = null;
            }

            if (lblFromFullAirportNameVR2 != null) {
                lblFromFullAirportNameVR2.Dispose ();
                lblFromFullAirportNameVR2 = null;
            }

            if (lblFromVR2 != null) {
                lblFromVR2.Dispose ();
                lblFromVR2 = null;
            }

            if (lblFullNameVR2 != null) {
                lblFullNameVR2.Dispose ();
                lblFullNameVR2 = null;
            }

            if (lblGateLabelVR2 != null) {
                lblGateLabelVR2.Dispose ();
                lblGateLabelVR2 = null;
            }

            if (lblGateVR2 != null) {
                lblGateVR2.Dispose ();
                lblGateVR2 = null;
            }

            if (lblPNRLabelVR2 != null) {
                lblPNRLabelVR2.Dispose ();
                lblPNRLabelVR2 = null;
            }

            if (lblPNRVR2 != null) {
                lblPNRVR2.Dispose ();
                lblPNRVR2 = null;
            }

            if (lblSeatLabelVR2 != null) {
                lblSeatLabelVR2.Dispose ();
                lblSeatLabelVR2 = null;
            }

            if (lblSeatVR2 != null) {
                lblSeatVR2.Dispose ();
                lblSeatVR2 = null;
            }

            if (lblTerminalLabelVR2 != null) {
                lblTerminalLabelVR2.Dispose ();
                lblTerminalLabelVR2 = null;
            }

            if (lblTerminalVR2 != null) {
                lblTerminalVR2.Dispose ();
                lblTerminalVR2 = null;
            }

            if (lblTimeVR2 != null) {
                lblTimeVR2.Dispose ();
                lblTimeVR2 = null;
            }

            if (lblToFullAirportNameVR2 != null) {
                lblToFullAirportNameVR2.Dispose ();
                lblToFullAirportNameVR2 = null;
            }

            if (lblToVR2 != null) {
                lblToVR2.Dispose ();
                lblToVR2 = null;
            }

            if (lblWeatherVR2 != null) {
                lblWeatherVR2.Dispose ();
                lblWeatherVR2 = null;
            }

            if (mainViewVR2 != null) {
                mainViewVR2.Dispose ();
                mainViewVR2 = null;
            }

            if (passengerViewVR2 != null) {
                passengerViewVR2.Dispose ();
                passengerViewVR2 = null;
            }

            if (scrollViewVR2 != null) {
                scrollViewVR2.Dispose ();
                scrollViewVR2 = null;
            }

            if (selectedAirlineViewVR2 != null) {
                selectedAirlineViewVR2.Dispose ();
                selectedAirlineViewVR2 = null;
            }

            if (servicesCollectionViewVR2 != null) {
                servicesCollectionViewVR2.Dispose ();
                servicesCollectionViewVR2 = null;
            }

            if (servicesViewVR2 != null) {
                servicesViewVR2.Dispose ();
                servicesViewVR2 = null;
            }

            if (tiledViewBottomCenter != null) {
                tiledViewBottomCenter.Dispose ();
                tiledViewBottomCenter = null;
            }

            if (tiledViewBottomLeft != null) {
                tiledViewBottomLeft.Dispose ();
                tiledViewBottomLeft = null;
            }

            if (tiledViewBottomRight != null) {
                tiledViewBottomRight.Dispose ();
                tiledViewBottomRight = null;
            }

            if (tiledViewTop != null) {
                tiledViewTop.Dispose ();
                tiledViewTop = null;
            }
        }
    }
}