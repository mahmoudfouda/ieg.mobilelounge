using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using AudioToolbox;
using AIMS.Models;
using AIMS;
using AIMS_IOS.Utility;
using MWBarcodeScanner;
using ZXing;
using ZXing.Mobile;
using System.Threading.Tasks;
using InfineaSDK.iOS;

namespace AIMS_IOS
{
    public partial class AirLineViewController : AIMSViewController, IScanSuccessCallback
    {
        protected static CameraType lastCameraType;
        protected static ConnStates ipcLastConnState = ConnStates.ConnDisconnected;

        //Scanner scanner;
        SystemSound captureSound;
        //UIImageView bottomBackground;
        UICollectionViewFlowLayout flowLayout;
        UICollectionViewFlowLayout cardFlowLayout;

        public nfloat topPanelHeight = 44f;
        public nfloat topY = 0f;
        public CGPoint cellSize;
        public CGPoint cardCellSize;
        public bool IsSecondScan { get; set; } = false;
        //NSUserDefaults sharedSettings = new NSUserDefaults("Aims.com.ios", NSUserDefaultsType.SuiteName);
        NSUserDefaults sharedSettings = new NSUserDefaults("MobileLounge.iegamerica.com", NSUserDefaultsType.SuiteName);//TODO: is there any reason for the previous name?

        List<AirlineCellData> items = new List<AirlineCellData>();

        List<AirlineCellData> filteredItemsS1 = new List<AirlineCellData>();
        List<AirlineCellData> filteredItemsS2 = new List<AirlineCellData>();

        private bool searchMode = false;


        #region cardViewData

        UIView backgroundView;
        //RoundedRectView creditCardView;
        //UIImageView cardLogo;
        // UILabel cardTitle;
        // List<UIImage> cardImages;
        // List<string> cardLabels;
        // UICollectionView cardView;
        // CGRect cardFrame;
        // UICollectionViewLayout cardViewLayout;
        List<CardCellData> cardCellData;
        List<CardCellData> filteredCardCellData;
        CardSource cardSource;
        // bool cardViewOn = false;

        UIView overCardView;
        UIImageView overIcon;
        UILabel overLabel;
        #endregion

        UIView rotationBackground;
        private UIRefreshControl refreshControl;
        private UIView overCardTrafficView;
        private UIImageView overTrafficIcon;
        private UILabel overTrafficLabel;
        private UIView cardTrafficBackground;
        private UIButton cardTrafficBackgroundButton;

        private CardsView cardTrafficView;
        private UILabel cardTrafficTopLabel;
        private UIView cardTrafficTopView;
        private UIImageView cardTrafficTopImage;
        private UILabel lblCustomers;
        private UILabel lblUpdatedTime;
        private string stringCustomer = "Current Customers: {0}";
        private string stringTime = "Last Update: {0}";

        public UIView MasterView
        {
            get { return masterView; }
        }

        public AirLineViewController(IntPtr handle) : base(handle)
        {

        }

        private void LoadTexts(Ieg.Mobile.Localization.Language? language = null)
        {
            InvokeOnMainThread(() =>
            {
                //scan.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                //scan.SetTitle(IPC.SDK.iOS.BaseDTDevice.SharedDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras
                //scan.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);
                searchBar.AccessibilityHint = string.Format("{0}...", AppDelegate.TextProvider.GetText(2008));

                cardSearchBar.Placeholder = AppDelegate.TextProvider.GetText(2160);
                searchBar.Placeholder = AppDelegate.TextProvider.GetText(2161);
            });
        }

        private int GetSeparatorIndex(List<AirlineCellData> items)
        {
            if (!items.Any(x => x.Airline.Name.Equals("######") && x.Airline.Code.Equals("######")))
                return -1;

            var separatorIndex = 0;
            while (!items[separatorIndex].Airline.Name.Equals("######") || !items[separatorIndex].Airline.Code.Equals("######"))
            {
                separatorIndex++;
            }
            return separatorIndex;
        }

        private void ExternalScannerStatusChanged(ConnectionStateEventArgs e)
        {
            #region IPC EMail (for whitelisting the MobileLounge using the IPC sdk)
            /*
             * 
            You will need to post these PPIDs in the Review Notes section of your app submission. 
            This process usually takes approximately eight business days and so please wait at least eight business days before submitting.

            102789-0027
            102789-0030
            102789-0031
            124918-0035
            124918-0020

             */
            #endregion
            if (e.State == ConnStates.ConnConnected)
            {
                InvokeOnMainThread(() =>
                {
                    scan.Enabled = false;
                });
                AppDelegate.ActiveCamera = CameraType.ExternalCamera;

                #region Enabling multi-scan function (Motion detection mode)

                if (AppDelegate.Current.LineaDevice == null)
                    LogsRepository.AddClientError("External Scanner Error", "The Infinite Peripherals device is not fully detected or functional.\nBaseDTDevice.SharedDevice is null.."); //AIMSMessage("", "BaseDTDevice.SharedDevice is null");
                else
                {
                    InvokeOnMainThread(() =>
                    {
                        scan.SetTitle(AppDelegate.Current.LineaDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras
                    });

                    try
                    {
                        NSError error = null;
                        if (!AppDelegate.Current.LineaDevice.BarcodeSetScanMode(ScanModes.ModeMotionDetect, out error))
                        {
                            if (error != null)
                            {
                                LogsRepository.AddError("Error in BarcodeSetScanMode", error.Description); //AIMSMessage("Error in BarcodeSetScanMode", error.Description);
                                error = null;
                            }
                        }

                        if (!AppDelegate.Current.LineaDevice.BarcodeStartScan(out error))
                        {
                            if (error != null)
                            {
                                LogsRepository.AddError("Error in BarcodeStartScan", error.Description); //AIMSMessage("Error in BarcodeStartScan", error.Description);
                                error = null;
                            }
                        }

                        //It shown: {Unknown error 10/0: channel=0, command=17}
                        //if (!BaseDTDevice.SharedDevice.BarcodeOpticonSetParams("E9Z", false, out error))
                        //{
                        //    if (error != null)
                        //    {
                        //        LogsRepository.AddError("Error in BarcodeOpticonSetInitString", error.Description);
                        //        error = null;
                        //    }
                        //}

                        //Didn't work
                        //if(!BaseDTDevice.SharedDevice.BarcodeOpticonSetInitString("E9Z", out error))
                        //{
                        //    if (error != null)
                        //    {
                        //        LogsRepository.AddError("Error in BarcodeOpticonSetInitString", error.Description);
                        //        error = null;
                        //    }
                        //}
                    }
                    catch (Exception ex)
                    {
                        LogsRepository.AddError("Error in ExternalScannerStatusChanged", ex.StackTrace); //AIMSMessage("Error", ex.StackTrace);
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                    }
                }
                #endregion
            }
            else
            {
                InvokeOnMainThread(() =>
                {
                    try
                    {
                        scan.Enabled = true;
                    }
                    catch { }
                });
                if (AppDelegate.ActiveCamera == CameraType.ExternalCamera)
                    AppDelegate.ActiveCamera = lastCameraType;//Reverting to the user set camera type.
                //AppDelegate.DetermineCurrentDevice();
            }
        }

        private void InitExternalScanner()
        {
            //TODO: refactor and extract this method and move to parent class or AppDelegate
            #region Initiating the INFINEA TAB M 2D barcode scanner hardware

            try
            {
                #region Old Code

                //AppDelegate.Current.LineaDevice = new Device();
                //AppDelegate.Current.LineaDispatcher = AppDelegate.Current.LineaDevice.Dispatcher;


                //AppDelegate.Current.LineaDispatcher.ConnectionStateChanged -= LineaDispatcher_ConnectionStateChanged;
                //AppDelegate.Current.LineaDispatcher.ConnectionStateChanged += LineaDispatcher_ConnectionStateChanged;


                //AppDelegate.Current.LineaDispatcher.BarcodeScanned -= LineaDispatcher_BarcodeScanned;
                //AppDelegate.Current.LineaDispatcher.BarcodeScanned += LineaDispatcher_BarcodeScanned;


                //AppDelegate.Current.LineaDispatcher.MagneticCardSwiped -= LineaDispatcher_MagneticCardSwiped;
                //AppDelegate.Current.LineaDispatcher.MagneticCardSwiped += LineaDispatcher_MagneticCardSwiped;


                //AppDelegate.Current.LineaDispatcher.RfidCardDetected -= LineaDispatcher_RfidCardDetected;
                //AppDelegate.Current.LineaDispatcher.RfidCardDetected += LineaDispatcher_RfidCardDetected;

                //AppDelegate.Current.LineaDevice.Connect();
                #endregion

                AppDelegate.Current.PeripheralEvents.ConnectionState -= LineaDispatcher_ConnectionStateChanged;
                AppDelegate.Current.PeripheralEvents.ConnectionState += LineaDispatcher_ConnectionStateChanged;

                // barcode scans
                AppDelegate.Current.PeripheralEvents.BarcodeNSDataType -= LineaDispatcher_BarcodeScanned;
                AppDelegate.Current.PeripheralEvents.BarcodeNSDataType += LineaDispatcher_BarcodeScanned;

                // MSR card swipes
                AppDelegate.Current.PeripheralEvents.MagneticCardDataTrack2Track3 -= LineaDispatcher_MagneticCardSwiped;
                AppDelegate.Current.PeripheralEvents.MagneticCardDataTrack2Track3 += LineaDispatcher_MagneticCardSwiped;


                // RFID card taps
                AppDelegate.Current.PeripheralEvents.RfCardDetected -= LineaDispatcher_RfidCardDetected;
                AppDelegate.Current.PeripheralEvents.RfCardDetected += LineaDispatcher_RfidCardDetected;

                AppDelegate.Current.PeripheralEvents.MagneticCardEncryptedDataTracksDataTrack1maskedTrack2maskedTrack3 += (object sender, MagneticCardEncryptedDataTracksDataTrack1maskedTrack2maskedTrack3EventArgs e) =>
                {
                    // e contains Data, Encryption, Track1masked, Track2masked, Track3
                    // add handling code here...
                    Console.WriteLine("MagneticCardEncryptedDataTracksDataTrack1maskedTrack2maskedTrack3");
                };

                AppDelegate.Current.PeripheralEvents.MagneticCardReadFailedReason += (object sender, MagneticCardReadFailedReasonEventArgs e) =>
                {
                    // e contains Reason and Source  (values explained in LibraryDemo source code)
                    // add error handling code here... 
                    Console.WriteLine("MagneticCardReadFailedReason");
                };

                AppDelegate.Current.LineaDevice.AddDelegate(AppDelegate.Current.PeripheralEvents);

                AppDelegate.Current.LineaDevice.Connect();
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("InitExternalScanner()", ex);
                AIMSError(AppDelegate.TextProvider.GetText(2528), AppDelegate.TextProvider.GetText(2529));
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }

            try
            {

                if (AppDelegate.Current.LineaDevice != null)
                    AppDelegate.Current.LineaDevice.AwakeFromNib();
            }
            catch (Exception ex)
            {
                LogsRepository.AddError("Error in InitExternalScanner() > AwakeFromNib()", ex.StackTrace); //AIMSMessage("Error", ex.StackTrace);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }
            #endregion
        }

        private void LineaDispatcher_RfidCardDetected(object sender, RfCardDetectedEventArgs e)
        {
            if (IsUserExpired())
                return;

            if (IsLoading || AppDelegate.GuestScanning) return;
#if DEBUG
            string data = "";
            try
            {
                data = e.Info.ToString();
            }
            catch { }

            AIMSMessage(AppDelegate.TextProvider.GetText(2533),
                string.Format(AppDelegate.TextProvider.GetText(2534),
                data, e.CardIndex));
#endif
        }

        private void LineaDispatcher_MagneticCardSwiped(object sender, MagneticCardDataTrack2Track3EventArgs e)
        {
            if (IsUserExpired())
                return;

            if (IsLoading || AppDelegate.GuestScanning) return;
#if DEBUG

            string data = $"{e.Track1 ?? string.Empty}{e.Track2 ?? string.Empty}{e.Track3 ?? string.Empty}";

            AIMSMessage(AppDelegate.TextProvider.GetText(2535),
                string.Format(AppDelegate.TextProvider.GetText(2536), data));
#endif
        }

        private void LineaDispatcher_BarcodeScanned(object sender, BarcodeNSDataTypeEventArgs e)
        {
            if (IsUserExpired())
                return;

            if (IsLoading || AppDelegate.GuestScanning) return;
            try
            {
                ShowLoading(AppDelegate.TextProvider.GetText(2507));
                //AppManager.validatePassengerInfo(OnPassengerChecked, e.BarcodeValue);
                if (IsSecondScan)
                {
                    IsSecondScan = false;//TODO: Test (if it shouldn't be false immediately)
                    AppManager.validatePassengerInfoWithSelectedCard(OnPassengerChecked, e.Barcode.ToString());
                }
                else
                    AppManager.validatePassengerInfo(OnPassengerChecked, e.Barcode.ToString());
            }
            catch { }
        }

        internal void ClearLastValueOnProgress()
        {
            if (progressView != null)
                progressView.UpdateProgress(0);
        }

        private void LineaDispatcher_ConnectionStateChanged(object sender, ConnectionStateEventArgs e)
        {
            try
            {
                if (ipcLastConnState == e.State) return;

                //IPC is calling this event two times !!!! (to prevent the duplicated call)
                ipcLastConnState = e.State;
                ExternalScannerStatusChanged(e);
                AppDelegate.Current.NotifyExternalCameraChanged(sender, e);
            }
            catch { }
        }

        public void InitScanner()
        {
            if (MembershipProvider.Current == null || !MembershipProvider.Current.IsAuthenticated) return;

            if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR) return;


            if (MembershipProvider.Current.IsTrialUser || AppDelegate.BarcodeScannerSerialNembers.Count == 0)
            {
                #region Switching to Xzing
                if (AppDelegate.Current.ZxingOptions == null)
                    AppDelegate.Current.ZxingOptions = new MobileBarcodeScanningOptions
                    {
                        TryHarder = true,
                        CameraResolutionSelector = (resolutions) =>
                        {
                            if (resolutions.Count == 0)
                                return null;
                            return resolutions[resolutions.Count - 1];
                        },
                        PossibleFormats = new List<BarcodeFormat>() {
                                BarcodeFormat.AZTEC,
                                BarcodeFormat.QR_CODE,
                                BarcodeFormat.PDF_417
                        }
                    };

                if (AppDelegate.Current.MainZxingScanner == null)
                {
                    AppDelegate.Current.MainZxingScanner = new MobileBarcodeScanner(this);
                }

                //if (AppDelegate.ZxingScannerView == null)
                //    AppDelegate.ZxingScannerView = new ZXingScannerViewController(AppDelegate.Current.ZxingOptions, AppDelegate.Current.MainZxingScanner);

                //var result = await scanner.Scan(AppDelegate.Current.ZxingOptions);
                //if (result != null)
                //    Console.WriteLine("Scanned Barcode: " + result.Text);

                #endregion
            }
            else
            {
                #region Manatee Barcode Scanner
                if (AppDelegate.IsMWInitiated) return;
                var rectFull_1D = new CGRect(6, 6, 88, 88);
                var rectFull_2D = new CGRect(20, 6, 60, 88);
                var rectDotcode = new CGRect(30, 20, 40, 60);

                foreach (var sn in AppDelegate.BarcodeScannerSerialNembers)
                {
                    var registerResult = BarcodeConfig.MWB_registerSDK(sn.SerialNumber);

                    switch (registerResult)
                    {
                        case BarcodeConfig.MWB_RTREG_OK:
                            scan.SetTitle("", UIControlState.Normal);
                            AppDelegate.IsMWInitiated = true;
                            //Console.WriteLine("Registration OK");
                            break;
                        case BarcodeConfig.MWB_RTREG_INVALID_KEY:
                        //Console.WriteLine("Registration Invalid Key");
                        //break;
                        case BarcodeConfig.MWB_RTREG_INVALID_CHECKSUM:
                        //Console.WriteLine("Registration Invalid Checksum");
                        //break;
                        case BarcodeConfig.MWB_RTREG_INVALID_APPLICATION:
                        //Console.WriteLine("Registration Invalid Application");
                        //break;
                        case BarcodeConfig.MWB_RTREG_INVALID_SDK_VERSION:
                        //Console.WriteLine("Registration Invalid SDK Version");
                        //break;
                        case BarcodeConfig.MWB_RTREG_INVALID_KEY_VERSION:
                        //Console.WriteLine("Registration Invalid Key Version");
                        //break;
                        case BarcodeConfig.MWB_RTREG_INVALID_PLATFORM:
                        //Console.WriteLine("Registration Invalid Platform");
                        //break;
                        case BarcodeConfig.MWB_RTREG_KEY_EXPIRED:
                        //Console.WriteLine("Registration Key Expired");
                        //break;
                        default:
                            scan.SetTitle("License Error", UIControlState.Normal);
                            //scan.Enabled = false;
                            return;
                            //break;
                    }
                }


                BarcodeConfig.MWB_setActiveCodes(
                    BarcodeConfig.MWB_CODE_MASK_AZTEC |
                    BarcodeConfig.MWB_CODE_MASK_PDF |
                    BarcodeConfig.MWB_CODE_MASK_QR
                    );

                BarcodeConfig.MWB_setDirection(BarcodeConfig.MWB_SCANDIRECTION_HORIZONTAL | BarcodeConfig.MWB_SCANDIRECTION_VERTICAL);
                // set the scanning rectangle based on scan direction(format in pct: x, y, width, height)
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_25, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_39, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_93, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_128, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_AZTEC, rectFull_2D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_DM, rectFull_2D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_EANUPC, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_PDF, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_QR, rectFull_2D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_RSS, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_CODABAR, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_DOTCODE, rectDotcode);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_11, rectFull_1D);
                BarcodeConfig.MWBsetScanningRect(BarcodeConfig.MWB_CODE_MASK_MSI, rectFull_1D);

                BarcodeConfig.MWB_setMinLength(BarcodeConfig.MWB_CODE_MASK_25, 5);
                BarcodeConfig.MWB_setMinLength(BarcodeConfig.MWB_CODE_MASK_MSI, 5);
                BarcodeConfig.MWB_setMinLength(BarcodeConfig.MWB_CODE_MASK_39, 5);
                BarcodeConfig.MWB_setMinLength(BarcodeConfig.MWB_CODE_MASK_CODABAR, 5);
                BarcodeConfig.MWB_setMinLength(BarcodeConfig.MWB_CODE_MASK_11, 5);

                MWScannerViewController.setActiveParserMask(BarcodeConfig.MWP_PARSER_MASK_NONE);


                // set decoder effort level (1 - 5)
                // for live scanning scenarios, a setting between 1 to 3 will suffice
                // levels 4 and 5 are typically reserved for batch scanning
                BarcodeConfig.MWB_setLevel(3);

#if DEBUG
                BarcodeConfig.MWBenableZoom(false);
                //BarcodeConfig.MWBenableZoom(true);
                //BarcodeConfig.MWBsetZoomLevels(200,400,0);
                //get and print Library version
                int ver = BarcodeConfig.MWB_getLibVersion();
                int v1 = (ver >> 16);
                int v2 = (ver >> 8) & 0xff;
                int v3 = (ver & 0xff);
                var libVersion = v1.ToString() + "." + v2.ToString() + "." + v3.ToString();
                Console.WriteLine("Manatee Works Barcode Scanner lib version: " + libVersion);
#else
                BarcodeConfig.MWBenableZoom(false);
#endif
                #endregion
            }
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if (IsUserExpired())
                return;

            lastCameraType = (AppDelegate.ActiveCamera.HasValue && AppDelegate.ActiveCamera != CameraType.ExternalCamera) ?
                AppDelegate.ActiveCamera.Value :
                CameraType.BackCamera;

            InitExternalScanner();

            #region Loading Airlines
            if (AppData.LoungeChanged)
            {
                AppData.LoungeChanged = false;
                ClearAirlines();
                ShowLoading(AppDelegate.TextProvider.GetText(2504));
                AppManager.getAirlines(AirlinesListChanged, true);
            }
            #endregion

            title.Text = MembershipProvider.Current.UserHeaderText;
            LoadTexts();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
            {
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;
                searchBar.SearchBarStyle = UISearchBarStyle.Default;
                searchBar.SearchTextField.BackgroundColor = UIColor.White;

                cardSearchBar.SearchBarStyle = UISearchBarStyle.Default;
                cardSearchBar.SearchTextField.BackgroundColor = UIColor.White;
            }

            //View.BackgroundColor = new UIColor(new CGColor(0, 0.098039215686274508f, 0.3215686274509804f, 1));
            View.BackgroundColor = new UIColor(red: 0.79f, green: 0.79f, blue: 0.79f, alpha: 1.0f);


            progressView.IsParentDark = true;
            progressView.IsAutoUpdateProgress = true;

            AppManager.OnUpdateProgress += (sender, e) =>
            {
                if (progressView != null)
                    progressView.LoadProgressData();
            };

            //ScaleFactor = UIScreen.MainScreen.Bounds.Width / 
            //    ((UIDevice.CurrentDevice.UserInterfaceIdiom != UIUserInterfaceIdiom.Pad && 
            //    UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height) ? 
            //    375f : 
            //    667f);

            nfloat longLength = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Width : UIScreen.MainScreen.Bounds.Height;

            ScaleFactor = longLength / 667f;

            var isLandScape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
            var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            if (!isPad && !isLandScape && ScaleFactor > 1f)
                ScaleFactor = 1f;


            rotationBackground = new UIView();
            rotationBackground.BackgroundColor = UIColor.Black;
            this.View.AddSubview(rotationBackground);
            this.View.SendSubviewToBack(rotationBackground);

            InitLoungeTrafficComponents();

            initOverView();

            initOverViewTraffic();

            //beepSound = new SystemSound(1054);//Beep
            captureSound = new SystemSound(1108);//cameraShutter

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;

            scan.TouchUpInside += onScanClick;

            flowLayoutInit();
            setFonts();

            airlineCollectionView.RegisterClassForSupplementaryView(typeof(CollectionSectionView), UICollectionElementKindSection.Header, "line");
            airlineCollectionView.RegisterClassForSupplementaryView(typeof(CollectionEmptyView), UICollectionElementKindSection.Header, "empty");
            //bottomBackground = new UIImageView(UIImage.FromFile("gradiant.png"));
            //bottomView.AddSubview(bottomBackground);
            //bottomView.SendSubviewToBack(bottomBackground);

            loungeBackButton.TouchUpInside += loungeBack;
            loungeBackButtonLabel.TouchUpInside += loungeBack;
            leftUp.TouchUpInside += upButton;
            rightUp.TouchUpInside += upButton;

            if (MembershipProvider.Current == null || MembershipProvider.Current.LoggedinUser == null)//Added for being safe
                return;

            cardCellData = new List<CardCellData>();



            searchBar.TextChanged += searchTextChanged;
            cardSearchBar.TextChanged += cardSearchTextChanged;

            searchBar.SearchButtonClicked += disMisKeyB;
            cardSearchBar.SearchButtonClicked += disMisKeyB;

            UISearchBarUtility.removeBackground(searchBar);
            UISearchBarUtility.removeBackground(cardSearchBar);

            cardSearchBar.Layer.CornerRadius = 5f;
            cardSearchBar.Layer.BorderColor = UIColor.Black.CGColor;
            cardSearchBar.Layer.BorderWidth = 1f;

            //updateLayout();

            SearchView.AutoresizingMask = (UIViewAutoresizing.All);

            var swipeGestureRecognizer = new UISwipeGestureRecognizer();
            swipeGestureRecognizer.Direction = UISwipeGestureRecognizerDirection.Up;
            this.View.AddGestureRecognizer(swipeGestureRecognizer);

            cardBackgroundButton.TouchUpInside += hideCardView;
            cardBackgroundButton.TouchUpInside += disMisKeyB;
            cardBackground.BackgroundColor = new UIColor(0f, 0f, 0f, 0.8f);

            cardTrafficBackgroundButton.TouchUpInside += HideCardTrafficView;
            cardTrafficBackgroundButton.TouchUpInside += disMisKeyB;
            cardTrafficBackground.BackgroundColor = new UIColor(0f, 0f, 0f, 0.8f);

            scan.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            scan.Layer.BorderWidth = 1f;
            title.AdjustsFontSizeToFitWidth = true;

            loungeBackButtonLabel.TitleLabel.AdjustsFontSizeToFitWidth = true;
            cardTopLabel.AdjustsFontSizeToFitWidth = true;
            cardTrafficTopLabel.AdjustsFontSizeToFitWidth = true;

            var tap = new UITapGestureRecognizer((textfiled) =>
            {
                UIView temp = AutoScrollUtility.findFirstResponder(this.View);

                if (temp != null)
                {
                    temp.ResignFirstResponder();
                }
            });

            tap.CancelsTouchesInView = false;
            View.AddGestureRecognizer(tap);

            //InitScanner();

            UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(() =>
            {
                displayCardTrafficView();

                //this.PresentViewController(AppDelegate.LoungeTrafficViewController, true, ()=>
                //{
                //    AppDelegate.LoungeTrafficViewController.View.Superview.Frame = new CGRect(0, 0, 200, 200);
                //    AppDelegate.LoungeTrafficViewController.View.Superview.Center = this.View.Center;
                //});
            });

            progressView.AddGestureRecognizer(tapGesture);

            refreshControl = new UIRefreshControl();
            refreshControl.TintColor = UIColor.White;
            refreshControl.ValueChanged += RefreshControl_ValueChanged;
            airlineCollectionView.RefreshControl = refreshControl;

            progressView.Hidden = false;
        }


        private void InitLoungeTrafficComponents()
        {
            cardTrafficBackground = new UIView();
            cardTrafficBackground.Hidden = true;
            cardTrafficBackgroundButton = new UIButton();
            cardTrafficBackground.AddSubview(cardTrafficBackgroundButton);
            this.View.AddSubview(cardTrafficBackground);

            cardTrafficView = new CardsView();
            cardTrafficView.BackgroundColor = UIColor.White;
            cardTrafficView.Hidden = true;
            cardTrafficTopView = new UIView();
            cardTrafficTopLabel = new UILabel();
            cardTrafficTopImage = new UIImageView();
            cardTrafficTopView.AddSubviews(cardTrafficTopImage, cardTrafficTopLabel);
            cardTrafficView.AddSubview(cardTrafficTopView);

            lblCustomers = new UILabel();
            lblUpdatedTime = new UILabel();
            cardTrafficView.AddSubviews(lblCustomers, lblUpdatedTime);

            string regularFontName = "Helvetica Neue";
            nfloat xxLargeTexts = 26f * ScaleFactor, xLargeTexts = 24f * ScaleFactor, largeTexts = 22f * ScaleFactor, medium = 17f * ScaleFactor, smallTexts = 15f * ScaleFactor, xSmallTexts = 13f * ScaleFactor, microTexts = 10f * ScaleFactor, xmicroTexts = 9f * ScaleFactor;

            lblCustomers.Font = UIFont.FromName(regularFontName, medium);
            lblUpdatedTime.Font = UIFont.FromName(regularFontName, medium);

            if (MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy != null)
            {
                lblCustomers.Text = string.Format(stringCustomer, MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.OccupancyTotal);
                lblUpdatedTime.Text = string.Format(stringTime, MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.Time.ToString("hh:mm tt"));
            }
            else
            {
                lblCustomers.Text = string.Format(stringCustomer, 0);
                lblUpdatedTime.Text = string.Format(stringTime, DateTime.Now.ToString("hh:mm tt"));
            }

            this.View.AddSubview(cardTrafficView);
        }

        private void RefreshControl_ValueChanged(object sender, EventArgs e)
        {
            var task = new Task(async () =>
            {
                await LoadingAirline();


                BeginInvokeOnMainThread(() =>
                {
                    try
                    {
                        if (refreshControl != null)
                            refreshControl.EndRefreshing();
                    }
                    catch
                    {
                    }
                });
            });
            task.Start();
        }

        private async Task LoadingAirline()
        {
            AppData.LoungeChanged = false;
            ClearAirlines();
            ShowLoading(AppDelegate.TextProvider.GetText(2504));
            AppManager.getAirlines(AirlinesListChanged, true);
        }

        public void initOverViewTraffic()
        {
            overCardTrafficView = new UIView();
            overCardTrafficView.BackgroundColor = UIColor.Clear;
            overCardTrafficView.UserInteractionEnabled = false;

            overTrafficIcon = new UIImageView();
            overTrafficIcon.ContentMode = UIViewContentMode.ScaleAspectFit;

            overTrafficLabel = new UILabel();
            overTrafficLabel.Font = UIFont.FromName("Helvetica Neue", 20f * ScaleFactor);
            overTrafficLabel.TextColor = UIColor.White;
            overTrafficLabel.AdjustsFontSizeToFitWidth = true;

            overCardTrafficView.AddSubviews(overTrafficIcon, overTrafficLabel);

            overCardTrafficView.Hidden = true;

            this.View.Add(overCardTrafficView);
        }

        public void initOverView()
        {
            overCardView = new UIView();
            overCardView.BackgroundColor = UIColor.Clear;
            overCardView.UserInteractionEnabled = false;

            overIcon = new UIImageView();
            overIcon.ContentMode = UIViewContentMode.ScaleAspectFit;

            overLabel = new UILabel();
            overLabel.Font = UIFont.FromName("Helvetica Neue", 16f * ScaleFactor);
            overLabel.TextColor = UIColor.White;
            overLabel.AdjustsFontSizeToFitWidth = true;

            overCardView.AddSubviews(overIcon, overLabel);

            overCardView.Hidden = true;

            this.View.Add(overCardView);

        }

        public void setFonts()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                //top view
                loungeBackButtonLabel.Font = UIFont.FromName("Helvetica Neue", 26f);

                title.Font = UIFont.FromName("Helvetica Neue", 20f * ScaleFactor);

                //title.Font = UIFont.FromName("font name", 20f);

                //card top view
                //cardTopLabel.Font = UIFont.FromName("font name", 20f);

                //overLabel.Font = UIFont.FromName("font name", 20f);
            }
            //cardTopLabel.Font = UIFont.SystemFontOfSize(10f * ScaleFactor);
            //cardTopLabel.LineBreakMode = UILineBreakMode.WordWrap;
            ////cardTopLabel.TextAlignment = UITextAlignment.Center;
            //cardTopLabel.Lines = 2;
        }

        public void flowLayoutInit()
        {

            //airlines flow
            nfloat margin = 10f;
            nfloat w = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                w = (w / 6.7f);
            }
            else
            {
                w = (w / 4.7f);
            }

            cellSize = new CGPoint(w, w + w * 0.285f);

            flowLayout = new UICollectionViewFlowLayout()
            {
                ItemSize = new CGSize(cellSize.X, cellSize.Y),
                HeaderReferenceSize = new CGSize(5f, 5f),
                MinimumInteritemSpacing = 10f,
                MinimumLineSpacing = 15f,
                SectionInset = new UIEdgeInsets(5f, 5f, 5f, 5f),
                ScrollDirection = UICollectionViewScrollDirection.Vertical
            };

            airlineCollectionView.CollectionViewLayout = flowLayout;

            updateCardViewLayout();

            UpdateCardViewTrafficLayout();

            //Card flow
            w = (UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width) * 0.9f;

            w /= 3.5f;
            cardCellSize = new CGPoint(w, w * 0.8f);

            cardFlowLayout = new UICollectionViewFlowLayout()
            {
                ItemSize = new CGSize(cardCellSize.X, cardCellSize.Y),
                HeaderReferenceSize = new CGSize(5f, 5f),
                MinimumInteritemSpacing = 5f,
                MinimumLineSpacing = 15f,
                SectionInset = new UIEdgeInsets(5f, 5f, 0f, 5f),
                ScrollDirection = UICollectionViewScrollDirection.Vertical
            };

            cardCollectionView.CollectionViewLayout = cardFlowLayout;

        }

        private void UpdateCardViewTrafficLayout()
        {
            cardTrafficBackground.Frame = new CGRect(0f, 0f, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height);
            cardTrafficBackgroundButton.Frame = cardTrafficBackground.Frame;


            CGRect tempRect = new CGRect(UIScreen.MainScreen.Bounds.Width * 0.05f,
             UIScreen.MainScreen.Bounds.Height * 0.2f,
             UIScreen.MainScreen.Bounds.Width * 0.9f,
             UIScreen.MainScreen.Bounds.Height * 0.65f);

            nfloat overHeight = tempRect.Y * 0.7f;

            overCardTrafficView.Frame = new CGRect(tempRect.X + tempRect.Width * 0.3f,
                        tempRect.Y - overHeight
                        , tempRect.Width * 0.7f, overHeight);

            if (cardCellData != null)
            {
                //calculate view height
                nfloat columns = (nint)(tempRect.Width /
                (cardFlowLayout.ItemSize.Width +
                cardCollectionView.ContentInset.Left + cardFlowLayout.MinimumInteritemSpacing));

                nfloat rows = (nfloat)Math.Ceiling(3 / columns);

                nfloat rowHeight = cardFlowLayout.ItemSize.Height + cardFlowLayout.MinimumInteritemSpacing;
                nfloat yOffset = 40f + cardFlowLayout.HeaderReferenceSize.Height;
                nfloat cardViewHeight = yOffset + (rows * rowHeight);

                //max height
                if (cardViewHeight > UIScreen.MainScreen.Bounds.Height * 0.9f)
                {
                    cardViewHeight = UIScreen.MainScreen.Bounds.Height * 0.9f;
                }
                //center cardView
                nfloat heightRatio = cardViewHeight / UIScreen.MainScreen.Bounds.Height;
                nfloat yPos = UIScreen.MainScreen.Bounds.Height * ((1f - heightRatio) / 2f);

                tempRect = new CGRect(tempRect.X, yPos, tempRect.Width, cardViewHeight);
            }

            cardTrafficView.Frame = tempRect;

            nfloat iconSize = overCardTrafficView.Frame.Height * 0.6f;
            nfloat labelW = overTrafficLabel.IntrinsicContentSize.Width;

            nfloat xOffset = (overCardTrafficView.Frame.Width / 4f) - ((iconSize + labelW + 10f) / 2f);


            var totalPaxH = 20 * ScaleFactor;
            var usedHeight = (10f * ScaleFactor);
            lblCustomers.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
            lblCustomers.Frame = new CGRect(cardTrafficView.Frame.X + (10f * ScaleFactor), usedHeight, cardTrafficView.Frame.Width, totalPaxH);

            usedHeight += lblCustomers.Frame.Height + (20f * ScaleFactor);
            lblUpdatedTime.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
            lblUpdatedTime.Frame = new CGRect(cardTrafficView.Frame.X + (10f * ScaleFactor), usedHeight, cardTrafficView.Frame.Width, totalPaxH);

            nfloat removeTop = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height ?
                                (70f * ScaleFactor) : (100f * ScaleFactor);

            overTrafficIcon.Frame = new CGRect(xOffset, cardTrafficView.Frame.Y - removeTop, iconSize, iconSize);
            overTrafficLabel.Frame = new CGRect(overTrafficIcon.Frame.Right + 5f,
                overTrafficIcon.Frame.Y,
                overCardTrafficView.Frame.Width - overTrafficIcon.Frame.Right - 5f,
                overTrafficIcon.Frame.Height);
            overTrafficLabel.PreferredMaxLayoutWidth = overTrafficLabel.Frame.Width;
            overTrafficLabel.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
            overTrafficLabel.LineBreakMode = UILineBreakMode.WordWrap;
            overTrafficLabel.Lines = 2;
        }

        private void ClearAirlines()
        {
            InvokeOnMainThread(() =>
            {
                filteredItemsS1 = items = new List<AirlineCellData>();
                filteredItemsS2 = new List<AirlineCellData>();
                airlineCollectionView.Source = new AirLinesGridSource(filteredItemsS1, filteredItemsS2, this, airlineCollectionView);
                airlineCollectionView.ReloadData();
            });
        }

        private void AirlinesListChanged(object returnedItems)
        {
            if (MembershipProvider.Current == null)
                return;
            InvokeOnMainThread(() =>
            {
                #region Filling the items and filteredItems(S1/S2)
                if (returnedItems == null)
                {
                    filteredItemsS1 = items = new List<AirlineCellData>();
                    filteredItemsS2 = new List<AirlineCellData>();
                }
                else if (returnedItems is IEnumerable<Airline>)
                {
                    var usersAirline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
                    if (usersAirline != null)
                    {
                        LoadLoungeImage(usersAirline);

                        //AppData.usersAirLine = new AirlineCellData(usersAirline);
                        //if (usersAirline.AirlineLogoBytes != null && usersAirline.AirlineLogoBytes.Length > 0)
                        //    topIcon.Image = usersAirline.AirlineLogoBytes.ToImage();
                        //else
                        //{
                        //    ImageAdapter.LoadImage(
                        //        usersAirline.ImageHandle,
                        //        (imageBytes) =>
                        //        {
                        //            var uiImage = imageBytes.ToImage();
                        //            //usersAirline.AirlineLogoBytes = imageBytes;
                        //            AppData.usersAirLine.image = uiImage;

                        //            InvokeOnMainThread(() =>
                        //            {
                        //                //AirlinesRepository.SaveAirlineImage(usersAirline);//Saved to local DB : Already done

                        //                topIcon.Image = uiImage;
                        //            });
                        //        }, true);
                        //}
                    }

                    var fullItems = ((IEnumerable<Airline>)returnedItems).Select(x => new AirlineCellData(x)/*new AirlineCellData(null, x.DisplayName, x.Code)*/).ToList();

                    //TODO: Siavash make it searchable
                    var separatorIndex = GetSeparatorIndex(fullItems);

                    if (separatorIndex != -1)
                    {
                        filteredItemsS1 = items = fullItems.Take(separatorIndex).ToList();
                        filteredItemsS2 = fullItems.Skip(separatorIndex + 1).ToList();
                    }
                    else
                    {
                        filteredItemsS1 = items = fullItems;
                        filteredItemsS2 = new List<AirlineCellData>();
                    }
                }
                //else// Why it was here???
                //{
                //    filteredItemsS1 = items = new List<AirlineCellData>();
                //    filteredItemsS2 = new List<AirlineCellData>();
                //}
                #endregion
                airlineCollectionView.Source = new AirLinesGridSource(filteredItemsS1, filteredItemsS2, this, airlineCollectionView);
            });
            HideLoading();
        }

        private void LoadLoungeImage(Airline usersAirline)
        {
            AppData.usersAirLine = new AirlineCellData(usersAirline);

            string pathLoungeImage = string.Format(AppManager.AIMS_LOUNGE_IMAGE_NAME, MembershipProvider.Current.SelectedLounge.LoungeID);

            ImageAdapter.LoadImage(pathLoungeImage, (imageBytes) =>
                    {
                        if (imageBytes == null || imageBytes.Length == 0)
                        {
                            if (usersAirline.AirlineLogoBytes != null && usersAirline.AirlineLogoBytes.Length > 0)
                                topIcon.Image = usersAirline.AirlineLogoBytes.ToImage();
                            else
                            {
                                ImageAdapter.LoadImage(
                                    usersAirline.ImageHandle,
                                    (imageResultBytes) =>
                                    {
                                        var uiImage = imageResultBytes.ToImage();
                                        AppData.LoungeImage = uiImage;

                                        AppData.usersAirLine.image = uiImage;

                                        InvokeOnMainThread(() =>
                                        {
                                            topIcon.Image = uiImage;
                                        });
                                    }, true);
                            }
                        }
                        else
                        {
                            var uiImage = imageBytes.ToImage();
                            AppData.usersAirLine.image = uiImage;

                            AppData.LoungeImage = uiImage;

                            InvokeOnMainThread(() =>
                            {
                                topIcon.Image = uiImage;
                            });
                        }
                    }, true);
        }


        public int calculateSection()
        {
            if (filteredItemsS2.Count != 0)
            {
                return 2;
            }

            return 1;

        }

        public void CardsListChanged(object returnedItems)
        {
            InvokeOnMainThread(() =>
            {
                #region Filling the cardCellData*
                if (returnedItems == null) cardCellData = new List<CardCellData>();
                else if (returnedItems is IEnumerable<Card>)
                {
                    cardCellData = ((IEnumerable<Card>)returnedItems).Select(x => new CardCellData(x)).ToList();
                }
                else
                {
                    cardCellData = new List<CardCellData>();
                }
                #endregion
                cardCollectionView.Source = new CardSource(cardCellData, this);

                displayCardView();
                disMisKeyB();

                updateCardViewLayout();
            });
            HideLoading();
        }

        public void disMisKeyB(object sender, EventArgs e)
        {
            searchBar.ResignFirstResponder();
            cardSearchBar.ResignFirstResponder();
        }

        public void disMisKeyB()
        {
            searchBar.ResignFirstResponder();
            cardSearchBar.ResignFirstResponder();
        }

        private void updateCardViewLayout()
        {

            cardBackground.Frame = new CGRect(0f, 0f, UIScreen.MainScreen.Bounds.Width,
                UIScreen.MainScreen.Bounds.Height);
            cardBackgroundButton.Frame = cardBackground.Frame;

            CGRect tempRect = new CGRect(UIScreen.MainScreen.Bounds.Width * 0.05f,
                UIScreen.MainScreen.Bounds.Height * 0.2f,
                UIScreen.MainScreen.Bounds.Width * 0.9f,
                UIScreen.MainScreen.Bounds.Height * 0.65f);

            nfloat overHeight = tempRect.Y * 0.7f;


            if (cardCellData != null && cardCellData.Count < 12)
            {
                //calculate view height
                nfloat columns = (nint)(tempRect.Width /
                    (cardFlowLayout.ItemSize.Width +
                    cardCollectionView.ContentInset.Left + cardFlowLayout.MinimumInteritemSpacing));

                nfloat rows = (nfloat)Math.Ceiling(cardCellData.Count / columns);

                nfloat rowHeight = cardFlowLayout.ItemSize.Height + cardFlowLayout.MinimumInteritemSpacing;
                nfloat yOffset = 40f + cardFlowLayout.HeaderReferenceSize.Height;
                nfloat cardViewHeight = yOffset + (rows * rowHeight);

                //max height
                if (cardViewHeight > UIScreen.MainScreen.Bounds.Height * 0.9f)
                {
                    cardViewHeight = UIScreen.MainScreen.Bounds.Height * 0.9f;
                }
                //center cardView
                nfloat heightRatio = cardViewHeight / UIScreen.MainScreen.Bounds.Height;
                nfloat yPos = UIScreen.MainScreen.Bounds.Height * ((1f - heightRatio) / 2f);

                tempRect = new CGRect(tempRect.X, yPos, tempRect.Width, cardViewHeight);
            }

            cardsView.Frame = tempRect;
            //cardTopView.Frame = new CGRect(0, 0, cardsView.Frame.Width, 40f);
            //cardTopView.BackgroundColor = UIColor.Clear;

            //cardTopImage.Frame = new CGRect(5f, 5f, 30f, 30f);

            //cardTopLabel.Frame = new CGRect(cardTopImage.Frame.Right + 5f, 5f,
            //cardTopView.Frame.Width - cardTopImage.Frame.Right - 5f, 30f);

            float topOffset = 0f;

            if (cardCellData != null && cardCellData.Count > 11)//
            {
                topOffset = 55f;
                cardSearchBar.Hidden = false;
                cardSearchBar.Frame = new CGRect(5f, 2f,
                    tempRect.Width - 14f,
                        44f);
            }
            else
            {
                cardSearchBar.Hidden = true;
            }

            cardCollectionView.Frame = new CGRect(5f,
                cardTopLabel.Frame.Height + topOffset,
                tempRect.Width - 10f,
                tempRect.Height - cardTopLabel.Frame.Height - topOffset - 5f);


            //overview
            overCardView.Frame = new CGRect(tempRect.X + tempRect.Width * 0.3f,
                tempRect.Y - overHeight
                , tempRect.Width * 0.7f, overHeight);


            nfloat iconSize = overCardView.Frame.Height * 0.6f;
            nfloat labelW = overLabel.IntrinsicContentSize.Width;

            nfloat xOffset = (overCardView.Frame.Width / 4f) - ((iconSize + labelW + 10f) / 2f);

            overIcon.Frame = new CGRect(xOffset, overCardView.Frame.Height * 0.2f, iconSize, iconSize);
            overLabel.Frame = new CGRect(overIcon.Frame.Right + 5f,
                overIcon.Frame.Y,
                overCardView.Frame.Width - overIcon.Frame.Right - 5f,
                overIcon.Frame.Height);
            overLabel.PreferredMaxLayoutWidth = overLabel.Frame.Width;
            overLabel.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
            overLabel.LineBreakMode = UILineBreakMode.WordWrap;
            overLabel.Lines = 2;
        }

        private void initCardView()
        {
            backgroundView = new UIView(new CGRect(0f, 0,
                UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
            backgroundView.BackgroundColor = new UIColor(0.3f, 0.3f, 0.3f, 0.3f);

            cardTopLabel.AdjustsFontSizeToFitWidth = true;

        }

        private void removeSearchBarBackground()
        {
            foreach (UIView subView in View.Subviews)
            {
                //if(subView is UISearchBarBackground
            }
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            AppData.fromAirline = true;
            cardsView.UpdateMask();
            cardTrafficView.UpdateMask();
            SearchView.AutoresizingMask = (UIViewAutoresizing.All);

            updateLayout();
            progressView.RefreshProgress();


            if (lblCustomers != null)
            {
                if (MembershipProvider.Current?.SelectedLounge.LastLoungeOccupancy != null)
                {
                    lblCustomers.Text = string.Format(stringCustomer, MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.OccupancyTotal);
                    lblUpdatedTime.Text = string.Format(stringTime, MembershipProvider.Current.SelectedLounge.LastLoungeOccupancy.Time.ToString("hh:mm tt"));
                }
                else
                {
                    lblCustomers.Text = string.Format(stringCustomer, 0);
                    lblUpdatedTime.Text = string.Format(stringTime, DateTime.Now.ToString("hh:mm tt"));
                }
            }
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (progressView != null)
                progressView.LoadProgressData();

            InitScanner();
        }

        partial void SearchButton(UIButton sender)
        {
            searchButton.Hidden = true;
            searchBarAnimation(true);
        }

        partial void cancelSearch(UIButton sender)
        {
            searchBar.Text = "";
            searchBar.ResignFirstResponder();

            ClearAirlines();
            AppManager.getAirlines(AirlinesListChanged);

            ShowSearchButton();
            searchBarAnimation(false);

            hideCardView();
            ShowLoading(AppDelegate.TextProvider.GetText(2504));
        }

        public void searchTextChanged(object sender, UISearchBarTextChangedEventArgs e)
        {
            string text = e.SearchText.Trim();


            ClearAirlines();
            if (text.Length > 0)
            {
                AppManager.searchAirlines(text, AirlinesListChanged);//now its cleaner...
            }
            else
            {
                AppManager.getAirlines(AirlinesListChanged);
            }
        }

        public void cardSearchTextChanged(object sender, UISearchBarTextChangedEventArgs e)
        {
            string text = e.SearchText.Trim();
            filteredCardCellData = (from item in cardCellData
                                    where item.footer.ToUpper().Contains(text.ToUpper())
                                    select item).ToList();

            cardSource = new CardSource(cardCellData, this);

            cardCollectionView.Source = new CardSource(filteredCardCellData, this);

        }

        private void searchBarAnimation(bool search)
        {
            updateCardViewLayout();

            searchMode = search;

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
              0 : (float)View.SafeAreaInsets.Left;

            UIView.Animate(0.5d, 0d, UIViewAnimationOptions.CurveEaseInOut, () =>
            {

                CGRect frame = UIScreen.MainScreen.Bounds;

                var orientation = UIApplication.SharedApplication.StatusBarOrientation;

                if (search)//Search is shown (progress bar is hidden)
                {
                    SearchView.Frame = new CoreGraphics.CGRect(0, topView.Frame.Bottom,
                        frame.Width, 44f);

                    progressView.Frame = new CoreGraphics.CGRect(12f + leftMargin,
                     SearchView.Frame.Bottom + 4f, frame.Width - 36f - leftMargin, 30);

                    //SearchView.Frame = new CoreGraphics.CGRect(0f, progressView.Frame.Bottom + 10f, frame.Width, 44f);

                    airlineCollectionView.Frame = new CoreGraphics.CGRect(leftMargin,
                        progressView.Frame.Bottom,
                        frame.Width,
                        frame.Height - SearchView.Frame.Bottom - bottomView.Frame.Height);

                    progressView.RefreshProgress();
                }
                else//progress bar is shown (Search is hidden)
                {
                    SearchView.Frame = new CoreGraphics.CGRect(0,
                        topView.Frame.Y + (topView.Frame.Height - 44f),
                        frame.Width,
                        44f);

                    progressView.Hidden = false;

                    progressView.Frame = new CoreGraphics.CGRect(12f + leftMargin,
                     SearchView.Frame.Bottom + 4f,
                     frame.Width - 36f - leftMargin, 30);

                    airlineCollectionView.Frame = new CoreGraphics.CGRect(leftMargin,
                            progressView.Frame.Y + progressView.Frame.Height,
                            frame.Width,
                            frame.Height - progressView.Frame.Bottom - bottomView.Frame.Height);
                }
            }, () =>
            {
                if (search)
                    BeginInvokeOnMainThread(() =>
                    {
                        searchBar.BecomeFirstResponder();
                    });
            });
        }

        private void updateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;

            float statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            if (View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                0 : (float)View.SafeAreaInsets.Left;



            //background
            background.Frame = new CGRect(0f, 44f + statusBarH, frame.Width, frame.Height - 44f - statusBarH);

            //-------------card view---------------------
            updateCardViewLayout();

            //-------------traffic lounge view---------------------
            UpdateCardViewTrafficLayout();


            //--------------bottom view----------------
            //bottomView.Frame = new CGRect(0f, frame.Height - 50f, frame.Width, 50f);
            DownScanViewModule.UpdateScanViewLayout(UIScreen.MainScreen.Bounds, bottomView, leftUp, rightUp, scan, null, leftMargin);

            //bottomBackground.Frame = new CoreGraphics.CGRect(0f, 0f,
            //    bottomView.Frame.Width, bottomView.Frame.Height);

            //--------------top view--------------------------
            nfloat realHeight = frame.Width < frame.Height ? frame.Height : frame.Width;

            //adjust top view size by ratio, minimum 44f
            topPanelHeight = 44f / 650f * realHeight;
            topPanelHeight = topPanelHeight < 44f ? 44f : topPanelHeight;

            topView.Frame = new CGRect(0, statusBarH, frame.Width, topPanelHeight);
            topY = topView.Frame.Y;

            nfloat backLabelWidth = loungeBackButtonLabel.TitleLabel.IntrinsicContentSize.Width;

            if (AppDelegate.CurrentDevice.Type == DeviceType.Tablet && sharedSettings.BoolForKey("enabled_preference"))
            {
                loungeBackButton.Frame = new CGRect(8f, 10f, (topPanelHeight - 8f) * 0.7f, topPanelHeight - 24f);
                loungeBackButtonLabel.Hidden = false;
                loungeBackButtonLabel.Frame = new CGRect(loungeBackButton.Frame.Right, 0f, backLabelWidth, topPanelHeight);

            }
            else
            {
                nfloat backButtonHeight = topPanelHeight / 2f;
                loungeBackButton.Frame = new CGRect(2f, topPanelHeight / 4f, (topPanelHeight - 8f) * 0.833f, backButtonHeight);

                loungeBackButtonLabel.Hidden = true;
                loungeBackButtonLabel.Frame = new CGRect(loungeBackButton.Frame.Right, 0f, 0f, topPanelHeight);
            }


            //topIcon.Frame = new CGRect(loungeBackButtonLabel.Frame.Width + loungeBackButton.Frame.Width + 2f, 4f, topH - 8f, topH - 8f);
            nfloat searchButtonMargin = 5f;
            searchButton.Frame = new CGRect(topView.Frame.Width - topPanelHeight - searchButtonMargin,
                searchButtonMargin, topPanelHeight - searchButtonMargin, topPanelHeight - searchButtonMargin);

            nfloat topIconSize = topPanelHeight - 8f;
            nfloat labelSize = title.IntrinsicContentSize.Width;


            if (labelSize > topView.Frame.Width - loungeBackButtonLabel.Frame.Right - searchButton.Frame.Width - topIconSize - 20f)
            {
                labelSize = topView.Frame.Width - loungeBackButtonLabel.Frame.Right - searchButton.Frame.Width - topIconSize - 20f;
            }

            nfloat spaceWidth = topView.Frame.Width - loungeBackButtonLabel.Frame.Right - searchButton.Frame.Width - 20f;
            nfloat centerOffset = (spaceWidth / 2f) - ((topIconSize + labelSize + 10f) / 2f);

            topIcon.Frame = new CGRect(loungeBackButtonLabel.Frame.Right + centerOffset + 5f, 4f, topIconSize, topIconSize);
            title.Frame = new CGRect(topIcon.Frame.Right + 5f, 0f,
                labelSize,
                topPanelHeight);


            //topIcon.Frame = new CGRect(loungeBackButtonLabel.Frame.Right + 2f, 4f, topIconSize, topIconSize);
            //title.Frame = new CGRect(topIcon.Frame.Right + 5f, 0f,
            //    topView.Frame.Width - (topIcon.Frame.Right + 10f + searchButton.Frame.Width + searchButtonMargin),
            //    topH);


            //---------searchView & arline collection----------------

            if (searchMode)
            {
                SearchView.Frame = new CoreGraphics.CGRect(0f, topView.Frame.Bottom,
                    frame.Width, 44f);
            }
            else
            {
                SearchView.Frame = new CoreGraphics.CGRect(0f,
                    topView.Frame.Y + (topView.Frame.Height - 44f),
                    frame.Width,
                    44f);
            }

            //progressView.Hidden = searchButton.Hidden;
            progressView.Hidden = !overCardView.Hidden;

            progressView.Frame = new CoreGraphics.CGRect(12f + leftMargin,
              SearchView.Frame.Bottom + 4f,
              frame.Width - 36f - leftMargin,
              CustomProgressControl.MinSize);

            nfloat height13 = 0;
            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                height13 = View.SafeAreaInsets.Bottom / 2;

            airlineCollectionView.Frame = new CoreGraphics.CGRect(leftMargin,
                    progressView.Frame.Y + progressView.Frame.Height,
                    frame.Width - leftMargin,
                    frame.Height - progressView.Frame.Bottom - bottomView.Frame.Height - height13);

            //--------------searchview content------------------------
            cancelSearchButton.Frame = new CGRect(SearchView.Frame.Width - 44f, 0f,
                44f, 44f);
            searchBar.Frame = new CGRect(0, 0f, SearchView.Frame.Width - 44f - leftMargin, 44f);

            //rotation background
            rotationBackground.Frame = new CGRect(leftMargin, statusBarH,
                UIScreen.MainScreen.Bounds.Width * 2f, UIScreen.MainScreen.Bounds.Height * 2f);


        }

        public void displayCardView()//int itemIndex, CGPoint position, AirlineCellData cellData)
        {
            InvokeOnMainThread(() =>
            {
                cardsView.Hidden = false;
                cardBackground.Hidden = false;
                overCardView.Hidden = false;
                progressView.Hidden = true;
            });


            cardTopLabel.Text = AppData.selectedAirLine.Airline.Name;
            cardTopImage.Image = AppData.selectedAirLine.image;

            overIcon.Image = AppData.selectedAirLine.image;
            overLabel.Text = AppData.selectedAirLine.Airline.Name;
            cardAnimation(true);

        }

        public void hideCardView(object sender, EventArgs e)
        {
            InvokeOnMainThread(() =>
            {
                cardsView.Hidden = true;
                cardBackground.Hidden = true;
                overCardView.Hidden = true;
                progressView.Hidden = !overCardView.Hidden;
            });
            cardAnimation(false);
        }

        public void hideCardView()
        {
            cardsView.Hidden = true;
            cardBackground.Hidden = true;
            overCardView.Hidden = true;
            progressView.Hidden = !overCardView.Hidden;

            cardAnimation(false);
        }

        private void cardAnimation(bool display)
        {
            if (display)
            {
                resetCardLayout();
            }

            var orientation = UIApplication.SharedApplication.StatusBarOrientation;

            UIView.BeginAnimations("cardPop");
            UIView.SetAnimationDuration(0.3d);
            UIView.SetAnimationCurve(UIViewAnimationCurve.EaseIn);

            if (display)
            {
                cardBackground.BackgroundColor = new UIColor(0f, 0f, 0f, 0.9f);
                updateCardViewLayout();
            }
            else
            {
                cardBackground.BackgroundColor = new UIColor(0f, 0f, 0f, 0f);
                resetCardLayout();
            }

            UIView.CommitAnimations();

        }

        private void resetCardLayout()
        {
            cardsView.Frame = new CGRect(UIScreen.MainScreen.Bounds.Width / 2f,
                UIScreen.MainScreen.Bounds.Height / 2f,
                0f, 0f);

            cardTopLabel.Frame = new CGRect(cardTopImage.Frame.X, 0f, 0f, 0f);
            //cardTopLabel.PreferredMaxLayoutWidth = cardsView.Frame.Width - frameSize.X;
            //cardTopLabel.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;

            cardTopImage.Frame = new CGRect(0f, 0f, 0f, 0f);
            cardTopView.Frame = new CGRect(0f, 0f, 0f, 0f);
            cardCollectionView.Frame = new CGRect(0f, 0f, 0f, 0f);
            cardSearchBar.Frame = new CGRect(0f, 0f, 0f, 0f);

        }

        public void upButton(object sender, EventArgs e)
        {
            UIViewController vc = this.Storyboard.InstantiateViewController("agentMenuTabController");
            vc.ProvidesPresentationContextTransitionStyle = true;
            vc.DefinesPresentationContext = true;
            vc.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;

            HideSearchButton();
            this.PresentViewController(vc, true, null);
        }

        public void HideSearchButton()
        {
            UIView.Animate(0.9, () =>
             {
                 searchButton.Alpha = 0;
                 progressView.Alpha = 0;
             }, () =>
             {
                 searchButton.Hidden = true;
                 progressView.Hidden = true;
             });
        }

        public void ShowSearchButton()
        {
            searchButton.Hidden = false;
            progressView.Hidden = false;

            UIView.Animate(0.9, () =>
            {
                searchButton.Alpha = 1;
                progressView.Alpha = 1;
            }, () =>
            {
                searchButton.Hidden = false;
                progressView.Hidden = false;

                // progressView.RefreshProgress();

                if (progressView != null)
                    progressView.LoadProgressData();
            });
        }

        public void PresentTabbedMIForm()//(int row)
        {
            AppDelegate.ManualPassengerView.ProvidesPresentationContextTransitionStyle = true;
            AppDelegate.ManualPassengerView.DefinesPresentationContext = true;
            AppDelegate.ManualPassengerView.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;

            this.PresentViewController(AppDelegate.ManualPassengerView, true, () =>
            {
                hideCardView();
                //reset search here if needed
            });

        }

        public void PresentSinglePageMIForm()
        {
            InvokeOnMainThread(() =>
            {
                if (AppDelegate.SinglePageMIView2.IsShown)//TODO: close it on PCI scanner OnScanned
                {
                    AppDelegate.SinglePageMIView2.DismissViewController(true, null);
                }

                AppDelegate.SinglePageMIView2.ProvidesPresentationContextTransitionStyle = true;
                AppDelegate.SinglePageMIView2.DefinesPresentationContext = true;
                AppDelegate.SinglePageMIView2.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;

                this.PresentViewController(AppDelegate.SinglePageMIView2, true, () =>
                {
                    hideCardView();
                    //reset search here if needed
                });
            });
        }

        public void PresentValidationResult()
        {
            InvokeOnMainThread(() =>
            {
                if (AppDelegate.ActiveCamera == CameraType.ExternalCamera && AppDelegate.ValidationView.IsShown)
                {
                    AppDelegate.ValidationView.DismissViewController(true, null);
                }

                if (AppDelegate.SinglePageMIView2.IsShown)
                {
                    AppDelegate.SinglePageMIView2.DismissViewController(true, null);
                }

                try
                {
                    AppDelegate.ValidationView.ProvidesPresentationContextTransitionStyle = true;
                    AppDelegate.ValidationView.DefinesPresentationContext = true;
                    AppDelegate.ValidationView.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
                    //var parent = AppDelegate.SinglePageMIView2.IsShown ? (UIViewController)AppDelegate.SinglePageMIView2 : (UIViewController)this;
                    //parent.
                    this.PresentViewController(AppDelegate.ValidationView, true, null);
                }
                catch (Exception exx)
                {
                    LogsRepository.AddError("Exception loading ValidationResultView", exx);
                    AIMSError(AppDelegate.TextProvider.GetText(2527), exx.Message);
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(exx);
                }
            });
        }

        public void Scan(bool secondScan = false)
        {
            if (IsUserExpired() || !MembershipProvider.Current.IsAuthenticated || MembershipProvider.Current.IsTokenExpired)
                return;

            this.IsSecondScan = secondScan;
            #region Fake Scanning
#if DEBUG//TODO: remove the whole (DEBUG) section below
            //var fakeScanTask = new Task(() =>
            //{
            //    AIMSConfirm("DEBUG mode detected",
            //        "You have pressed the scan button on DEBUG mode, please...!\nPlease tap on OK to continue with a simulated barcode sample.",
            //        (o, args) =>
            //        {
            //            var barcode = "";
            //            var bID = 0;
            //            switch (bID)
            //            {
            //                case 0://OTP
            //                    barcode = "237479163693939";
            //                    break;
            //                case 1://UA Club Digital
            //                    barcode = "M1XXXXXXX/XXXXXXXXX   XX--XX1 XXXXXXUA 0000 242J008C 136 15C>3180 O3242BUA              0000000000000000 UA UA WHM76423            *000      00  UAG<13>";
            //                    break;
            //                case 2://UA Gold Card
            //                    barcode = "FFPC001UADF494648        LEACH               BILLY WAYNE         UAG0118GLN      ^001";
            //                    break;
            //                default://Lucian's test-case
            //                    barcode = "M1BAHATKALACH/AMALIA  EBS9BDH LAXEWRUA 1997 191J002E0119 15D>5180 K7191BUA 50165821020002A01623316637303 UA                        Y*30601    09        ";
            //                    break;
            //            }
            //            //AppManager.validatePassengerInfo(OnPassengerChecked, barcode);
            //            if (isSecondScan)
            //                AppManager.validatePassengerInfoSecondTime(OnPassengerChecked, barcode);
            //            else
            //                AppManager.validatePassengerInfo(OnPassengerChecked, barcode);
            //        });
            //});
            //fakeScanTask.Start();
            //return;
#endif
            #endregion

            if (AppDelegate.ActiveCamera == CameraType.ExternalCamera) return;
            InvokeOnMainThread(async () =>
            {
                if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR)
                {
                    AIMSMessage(AppDelegate.TextProvider.GetText(2528), AppDelegate.TextProvider.GetText(2532));
                    return;
                }

                #region Camera access permission request
                var camReq = AppDelegate.Current.RequestCameraAccess();
#if DEBUG
                camReq.Wait();// new TimeSpan(0, 0, 10));

                //if(camReq.IsCanceled || camReq.IsFaulted)
                //{
                //    AIMSError("Error", "Failed to request for camera access permission");
                //    return;
                //}
                //if(!camReq.IsCompleted)
                //{
                //    AIMSError("Error", "Failed to request for camera access permission");
                //    return;
                //}
#else
                camReq.Wait();
#endif
                if (!camReq.Result.IsSucceeded)
                {
                    AIMSError(AppDelegate.TextProvider.GetText(2047), camReq.Result.Message);
                    return;
                }
                #endregion

                AppDelegate.ContinueScanning = true;
                AppDelegate.IsScanningCancelled = false;


                if (MembershipProvider.Current.IsTrialUser)
                {
                    #region Zxing Barcode Scanner
                    /**/
                    //Default calling method of Zxing Barcode-Scanner
                    var result = await AppDelegate.Current.MainZxingScanner.Scan(AppDelegate.Current.ZxingOptions);//.ConfigureAwait(false); //scanner.Scan(options).ConfigureAwait(false);
                    if (result != null)
                    {
                        //try
                        //{
                        //    InvokeOnMainThread(() =>
                        //    {
                        //        AppDelegate.ZxingScannerView.DismissViewController(false, null);
                        //    });
                        //}
                        //catch { }//TODO: Completely close and dispose it before going

                        ShowLoading(AppDelegate.TextProvider.GetText(2507));

                        try
                        {
                            if (IsSecondScan)
                            {
                                IsSecondScan = false;
                                AppManager.validatePassengerInfoWithSelectedCard(OnPassengerChecked, result.Text);
                            }
                            else
                                AppManager.validatePassengerInfo(OnPassengerChecked, result.Text);
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Scanner recognition failed", ex);
                            Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                        }
                    }
                    else
                    {
                        AppDelegate.ContinueScanning = false;
                        AppDelegate.IsScanningCancelled = true;
                        if (this.IsSecondScan)
                            handlePassengerFailed((int)ErrorCode.NeedToSwipeBoardingPass);
                    }

                    /*
                    Action<Result> OnResultCallback = null;
                    OnResultCallback = (result) =>
                    {
                        AppDelegate.ZxingScannerView.OnScannedResult -= OnResultCallback;


                        if (result != null)
                        {
                            try
                            {
                                InvokeOnMainThread(() =>
                                {
                                    AppDelegate.ZxingScannerView.DismissViewController(false, null);
                                });
                            }
                            catch { }//TODO: Completely close and dispose it before going

                            ShowLoading(AppDelegate.TextProvider.GetText(2507));


                            try
                            {
                                if (IsSecondScan)
                                {
                                    IsSecondScan = false;
                                    AppManager.validatePassengerInfoWithSelectedCard(OnPassengerChecked, result.Text);
                                }
                                else
                                    AppManager.validatePassengerInfo(OnPassengerChecked, result.Text);
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Scanner recognition failed", ex);
                            }
                        }
                        else AppDelegate.ContinueScanning = false;
                    };

                    AppDelegate.ZxingScannerView.OnScannedResult += OnResultCallback;
                    //AppDelegate.ZxingScannerView. 
                    //ZXingScannerView.
                    AppDelegate.ZxingScannerView.ScanningOptions.UseFrontCameraIfAvailable = (AppDelegate.ActiveCamera == CameraType.FrontCamera);

                    this.PresentViewController(AppDelegate.ZxingScannerView, false, null);
                    */
                    #endregion
                }
                else
                {
                    #region Manatee Barcode Scanner
                    //TODO: (Now we can download the MWBarcodeScanner.dll for iOS from internet somewhere!!!)

                    MWScannerViewController.param_UseFrontCamera = (AppDelegate.ActiveCamera == CameraType.FrontCamera);
                    MWScannerViewController.param_CloseScannerOnSuccess = true;
                    //MWScannerViewController.param_EnableHiRes = true;

                    //MWScannerViewController.enableClose(true);
                    MWScannerViewController.param_EnableClose = true;
                    MWScannerViewController.closeFrame = new CGRect(0, 0, 64, 64);
                    MWScannerViewController.param_PauseScanner = false;

                    MWScannerViewController scanVC = new MWScannerViewController();

                    scanVC.Init();



                    //TODO: Omar we are trying to prevent the camera view rotation! (Couldn't)

                    // var fixecVC = new NoRotationViewController(scanVC);


                    //scanVC.DismissViewController(true, null); close method 1
                    //scanVC.closeScanner(); close method 2 (to test, I do not know the exact behavior)

                    Action<ScannerResult> OnResultCallback = null;
                    OnResultCallback = (obj) =>
                    {
                        if (OnResultCallback != null)
                            scanVC.OnResult -= OnResultCallback;
                        //scanVC.closeScanner();//it disposes

                        if (obj != null)
                        {
                            //TODO: move it to an other place
                            //if (AppDelegate.CurrentDevice == DeviceType.Phone)
                            //    captureSound.PlayAlertSoundAsync();//If it was iPhone
                            //else
                            //    captureSound.PlaySystemSoundAsync();//If it was iPad (no vibrate)

                            ShowLoading(AppDelegate.TextProvider.GetText(2507));
                            //InvokeOnMainThread(() =>
                            //{
                            //    try
                            //    {
                            //        scanVC.DismissViewController(false, null);
                            //        fixecVC.DismissViewController(false, null);
                            //    }
                            //    catch { }
                            //    //this.PresentViewController(AppDelegate.ValidationView,true,null);
                            //});
                            try
                            {
                                //scanVC.closeScanner();//This line is working perfectly 20170309
                                //            fixecVC.DismissViewController(false, null);//This line is also working perfectly 20170309 (Faster)
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Scanner could not be closed", ex);
                                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                                HideLoading();
                            }//TODO: Completely close and dispose it before going
                            try
                            {
                                if (IsSecondScan)
                                {
                                    IsSecondScan = false;
                                    AppManager.validatePassengerInfoWithSelectedCard(OnPassengerChecked, obj.code);
                                }
                                else
                                    AppManager.validatePassengerInfo(OnPassengerChecked, obj.code);
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Scanner recognition failed", ex);
                                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                                HideLoading();
                            }
                        }
                        else
                        {
                            AppDelegate.ContinueScanning = false;
                            AppDelegate.IsScanningCancelled = true;
                            System.Threading.Tasks.Task.Delay(600).ContinueWith((t) =>
                            {
                                InvokeOnMainThread(() =>
                                {
                                    if (this.IsSecondScan)
                                        handlePassengerFailed((int)ErrorCode.NeedToSwipeBoardingPass);
                                });
                            }, System.Threading.CancellationToken.None);
                        }
                    };

                    //MWScannerViewController.successCallback = new ManateeSuccessCallback(OnResultCallback);

                    scanVC.OnResult += OnResultCallback;

                    this.PresentViewController(scanVC, false, null);

                    //LoginViewController.Current.Scan(); 
                    #endregion
                }
            });
        }


        private bool IsUserExpired()
        {
            if (MembershipProvider.Current == null)
            {
                HideLoading();
                AppDelegate.ReturnToLoginView();
                return true;
            }
            return false;
        }

        public void loungeBack(object sender, EventArgs e)
        {
            //Adapters.GPSAdapter.Current.TestFenceValidation();

            var lounges = AppManager.getListOfLounges();
            if (lounges.Count > 1)
                this.DismissModalViewController(true);
            else
            {
                AppDelegate.ReturnToLoginView();
            }
        }

        private void onScanClick(object sender, EventArgs e)
        {
            Scan();
        }

        private void OnPassengerChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            if (IsUserExpired())
                return;

            AppData.ValidationResult = string.IsNullOrWhiteSpace(errorMetadata) ? new PassengerValidation() : new PassengerValidation(errorMetadata);
            MembershipProvider.Current.SelectedPassenger = passenger;
            if (MembershipProvider.Current.SelectedAirline != null)
                AppData.selectedAirLine = new AirlineCellData(MembershipProvider.Current.SelectedAirline);//TODO: It might not be necessary
            if (MembershipProvider.Current.SelectedCard != null)
                AppData.selectedCard = new CardCellData(MembershipProvider.Current.SelectedCard);//TODO: It might not be necessary
            HideLoading();
            InvokeOnMainThread(() =>
            {
                if (AppDelegate.ActiveCamera == CameraType.ExternalCamera && AppDelegate.ValidationView.IsShown)
                {
                    AppDelegate.ValidationView.DismissViewController(true, null);//ToDo: test it (for all scanner types)
                    //AIMSMessage("Dismissed", "the AppDelegate.ValidationView is dismissed");
                }

                var c = AppDelegate.SinglePageMIView2.IsShown;
                if (isSucceeded)
                {
                    if (errorCode.HasValue && (ErrorCode)errorCode.Value == ErrorCode.PassengerNameMismatch)
                    {
                        #region Passenger name mismatch (agent decision)
                        Question(AppDelegate.TextProvider.GetText(1104),
                                    string.Format("{0}{1}",
                                    (errorCode.HasValue ?
                                        string.Format("{0}\n", AppDelegate.TextProvider.GetText((int)errorCode)) : ""),
                                        message),
                                    (ss, ee) =>
                                    {
                                        //this.PresentViewController(AppDelegate.ManualPassengerView, true, null);
                                        PresentSinglePageMIForm();
                                    },
                                    (ss, ee) =>
                                    {
                                        if (passenger != null)
                                        {
                                            PassengersRepository.AddFailedPassenger(passenger, message);
                                        }
                                        AppManager.rejectPassenger();
                                        try
                                        {
                                            AppDelegate.ValidationView.DismissViewController(true, null);
                                            AppDelegate.SinglePageMIView2.DismissViewController(true, null);
                                        }
                                        catch (Exception ex)
                                        {
                                            LogsRepository.AddError("Passenger Reject", ex);
                                            Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                                        }
                                        AIMSMessage(AppDelegate.TextProvider.GetText(2539), AppDelegate.TextProvider.GetText(2540));
                                    });
                        #endregion
                    }
                    else
                    {
                        #region Passenger check success
                        PresentValidationResult();
                        #endregion
                    }
                }
                else
                {
                    #region Passenger check failed
                    if (errorCode.HasValue)
                    {
                        switch ((ErrorCode)errorCode.Value)
                        {
                            case ErrorCode.TaskAlreadyPerformedException:
                                handlePassengerFailed(errorCode);
                                return;
                            default:
                                if (passenger != null)
                                {
                                    PassengersRepository.AddFailedPassenger(passenger, message);
                                }
                                break;
                        }

                        if (!string.IsNullOrEmpty(message))
                        {
                            if (message.Contains("\n"))
                                AIMSError(message.Substring(0, message.IndexOf("\n")).Trim(), message.Substring(message.IndexOf("\n") + 1).Trim(), (sender, e) =>
                                {
                                    handlePassengerFailed(errorCode);
                                });//title is dynamic
                            else
                                AIMSError(AppDelegate.TextProvider.GetText(1104), message, (sender, e) =>
                                {
                                    handlePassengerFailed(errorCode);
                                });//title is "Validation failed!"
                        }
                        else
                            AIMSError(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051), (sender, e) =>
                            {
                                handlePassengerFailed(errorCode);
                            });//title is "Validation failed!"
                    }
                    #endregion
                }
            });
        }

        private void handlePassengerFailed(int? errorCode)
        {
            if (IsUserExpired())
                return;

            if (errorCode.HasValue && (ErrorCode)errorCode.Value == ErrorCode.NeedToSwipeBoardingPass)//This is an incomplete passenger!!!
            {
                PresentSinglePageMIForm();
            }
            else if (AppDelegate.ContinueScanning)
                AppDelegate.AirlinesView.onScanClick(this, EventArgs.Empty);
        }


        public void displayCardTrafficView()//int itemIndex, CGPoint position, AirlineCellData cellData)
        {
            InvokeOnMainThread(() =>
            {
                cardTrafficView.Hidden = false;
                cardTrafficBackground.Hidden = false;
                overCardTrafficView.Hidden = false;
                progressView.Hidden = true;
            });


            cardTrafficTopLabel.Text = "Lounge Traffic";
            cardTrafficTopImage.Image = AppData.LoungeImage;

            overTrafficIcon.Image = AppData.LoungeImage;
            overTrafficLabel.Text = "Lounge Traffic";
            CardTrafficAnimation(true);
        }

        public void HideCardTrafficView(object sender, EventArgs e)
        {
            InvokeOnMainThread(() =>
            {
                cardTrafficView.Hidden = true;
                cardTrafficBackground.Hidden = true;
                overCardTrafficView.Hidden = true;
                progressView.Hidden = !overCardTrafficView.Hidden;
            });
            CardTrafficAnimation(false);
        }

        public void HideCardTrafficView()
        {
            cardTrafficView.Hidden = true;
            cardTrafficBackground.Hidden = true;
            overCardTrafficView.Hidden = true;
            progressView.Hidden = !overCardTrafficView.Hidden;

            CardTrafficAnimation(false);
        }

        private void CardTrafficAnimation(bool display)
        {
            if (display)
            {
                ResetCardTrafficLayout();
            }

            var orientation = UIApplication.SharedApplication.StatusBarOrientation;

            UIView.BeginAnimations("cardTrafficPop");
            UIView.SetAnimationDuration(0.3d);
            UIView.SetAnimationCurve(UIViewAnimationCurve.EaseIn);

            if (display)
            {
                cardTrafficBackground.BackgroundColor = new UIColor(0f, 0f, 0f, 0.9f);
                UpdateCardViewTrafficLayout();
            }
            else
            {
                cardTrafficBackground.BackgroundColor = new UIColor(0f, 0f, 0f, 0f);
                ResetCardTrafficLayout();
            }

            UIView.CommitAnimations();

        }

        private void ResetCardTrafficLayout()
        {
            cardTrafficView.Frame = new CGRect(UIScreen.MainScreen.Bounds.Width / 2f,
                UIScreen.MainScreen.Bounds.Height / 2f,
                0f, 0f);

            cardTrafficTopLabel.Frame = new CGRect(cardTrafficTopImage.Frame.X, 0f, 0f, 0f);
            cardTrafficTopImage.Frame = new CGRect(0f, 0f, 0f, 0f);
            cardTrafficTopView.Frame = new CGRect(0f, 0f, 0f, 0f);
        }

        public void barcodeDetected(MWResult result)
        {

        }
    }

    internal class ManateeSuccessCallback : IScanSuccessCallback
    {
        Action<ScannerResult> _callback = null;
        public ManateeSuccessCallback(Action<ScannerResult> callback)
        {
            _callback = callback;
        }

        public void barcodeDetected(MWResult result)
        {
            if (_callback != null)
            {
                _callback.Invoke(new ScannerResult
                {
                    bytes = result.bytes,
                    code = result.text,
                    isGS1 = result.isGS1,
                    type = result.typeName
                });
            }
        }
    }
}