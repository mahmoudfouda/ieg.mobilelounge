using AIMS;
using Foundation;
using System;
using System.Linq;
using System.Threading.Tasks;
using UIKit;
using CoreGraphics;
using MWBarcodeScanner;
using Ieg.Mobile.Localization;
using System.Net;
using Plugin.Connectivity;

namespace AIMS_IOS
{
    public partial class LoginViewController : AIMSViewController
    {
        // 0, 20	// 0, 20
        // 0, 185	// 0, 176
        // 0, 468	// 0, 881
        public NSObject keyboardShowNotification, keyboardHideNotification;
        public nfloat scrollMaxHeight = 520f;
        public nfloat scaleFactor = 1f;

        public static LoginViewController Current { get; private set; }
        public LinedView linedView;
        private bool isLoggingIn = false;
        private string loadingText =  AppDelegate.TextProvider.GetText(2502);
        private string authenticatingText = AppDelegate.TextProvider.GetText(2511);

        private bool CanLogin
        {
            get
            {
                return (UsernameTextField.Text.Length > 1 && PasswordTextField.Text.Length > 1);
            }
        }

        private void LoadTexts(Language? language)
        {
            if (UsernameTextField == null ||
                PasswordTextField == null ||
                ButtonLogin == null ||
                ButtonCancel == null) return;
            UsernameTextField.Placeholder = AppDelegate.TextProvider.GetText(2002);
            PasswordTextField.Placeholder = AppDelegate.TextProvider.GetText(2003);
            ButtonLogin.SetTitle(AppDelegate.TextProvider.GetText(2005), UIControlState.Normal);
            ButtonCancel.SetTitle(AppDelegate.TextProvider.GetText(2004), UIControlState.Normal);//TODO: the title must be "Reset"

            loadingText = AppDelegate.TextProvider.GetText(2502);
            authenticatingText = AppDelegate.TextProvider.GetText(2511);
        }

        public LoginViewController(IntPtr handle) : base(handle)
        {
        }

        public void initLinedView()
        {
            linedView = new LinedView(UIColor.White);

            this.loginScrollView.AddSubview(linedView);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            initLinedView();

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;//TODO: Should happen on every view controller

            //loading = new LoadingOverlay(new CoreGraphics.CGRect(0f, 0f,
            //    UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height), "Loading");

            this.View.TranslatesAutoresizingMaskIntoConstraints = true;

            ButtonCancel.TitleLabel.AdjustsFontSizeToFitWidth = true;
            ButtonLogin.TitleLabel.AdjustsFontSizeToFitWidth = true;

            UsernameTextField.AdjustsFontSizeToFitWidth = true;
            PasswordTextField.AdjustsFontSizeToFitWidth = true;
            logoLabel.AdjustsFontSizeToFitWidth = true;
            mobileLoungeSolution.AdjustsFontSizeToFitWidth = true;
            providedBy.AdjustsFontSizeToFitWidth = true;
            copyright.AdjustsFontSizeToFitWidth = true;


            //UIApplication.SharedApplication.KeyWindow.RootViewController = navController;

            LoadTexts(null);

            //ButtonLogin.TouchUpInside += displayLoader;
            ButtonLogin.TouchUpInside += onLogin;


            //PasswordTextField.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            //UsernameTextField.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };

            UsernameTextField.AllEditingEvents += onTextChanged;
            UsernameTextField.SpellCheckingType = UITextSpellCheckingType.No;
            UsernameTextField.AutocorrectionType = UITextAutocorrectionType.No;
            UsernameTextField.AutocapitalizationType = UITextAutocapitalizationType.None;
            UsernameTextField.ShouldReturn += (textfiled) =>
            {
                PasswordTextField.BecomeFirstResponder();
                return false;
            };

            PasswordTextField.AllEditingEvents += onTextChanged;
            PasswordTextField.SpellCheckingType = UITextSpellCheckingType.No;
            PasswordTextField.AutocorrectionType = UITextAutocorrectionType.No;
            PasswordTextField.AutocapitalizationType = UITextAutocapitalizationType.None;
            PasswordTextField.ShouldReturn += (textfiled) =>
            {
                //PasswordTextField.ResignFirstResponder();
                onLogin(PasswordTextField, null);
                return true;
            };

            onTextChanged(this, EventArgs.Empty);


            ButtonLogin.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            ButtonLogin.Layer.BorderWidth = 1f;

            ButtonCancel.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            ButtonCancel.Layer.BorderWidth = 1f;

            updateLayout();

            keyboardShowNotification = UIKeyboard.Notifications.ObserveWillShow(keyboardWillShow);
            keyboardHideNotification = UIKeyboard.Notifications.ObserveWillHide(keyboardWillHide);
        }

        public override void ViewWillLayoutSubviews()
        {
            base.ViewWillLayoutSubviews();
            updateLayout();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            HideLoading();

            updateLayout();

            if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR)
            {
                AIMSMessage(AppDelegate.TextProvider.GetText(2531), AppDelegate.TextProvider.GetText(2537));
            }
            else
            {
                var locRes = AppDelegate.Current.RequestLocationAccess();
                if (!locRes.IsSucceeded)
                {
                    AIMSError(AppDelegate.TextProvider.GetText(2047), locRes.Message);
                    return;
                }
            }
            //var camReq = AppDelegate.Current.RequestCameraAccess();
            //camReq.Wait();
            //if (!camReq.Result.IsSucceeded)
            //{
            //    AIMSError("Error", camReq.Result.Message);
            //    return;
            //}
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            Current = this;
#if DEBUG
            UsernameTextField.Text = "mobile";
            PasswordTextField.Text = "Password001";

         
            UsernameTextField.Text = "v746361"; //UA user
            PasswordTextField.Text = "engine3ring4";

            // UsernameTextField.Text = "V772353";// UA test
            // PasswordTextField.Text = "";// UA test

            //UsernameTextField.Text = "mobiletester";// DEMO test
            //PasswordTextField.Text = "m2+IEG1131";// DEMO test

            //UsernameTextField.Text = "mobileac";
            //PasswordTextField.Text = "QamC704!l%";

            //UsernameTextField.Text = "testlx";// LX test
            //PasswordTextField.Text = "m1%AIMS1024";// LX test and ZH test
#else
            UsernameTextField.Text = "";
            PasswordTextField.Text = "";
#endif
            onTextChanged(this, null);

            //TODO: Show version on a label
            versionNo.Text = NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString();
        }

        public void updateLayout()
        {
            CGRect viewFrame = UIScreen.MainScreen.Bounds;

            float mainMargin = 20f;
            //----------------------------Title view----------------------------------
            titleView.Frame = new CGRect(0f, 0f, viewFrame.Width, viewFrame.Height * 0.3f);

            logo.Frame = new CGRect(mainMargin, titleView.Frame.Height * 0.2f,
                viewFrame.Width - (mainMargin * 2), titleView.Frame.Height * 0.5f);

            logoLabel.Frame = new CGRect(logo.Frame.X, logo.Frame.Bottom + titleView.Frame.Height * 0.05f,
                logo.Frame.Width, titleView.Frame.Height * 0.2f);


            //---------------------------bottomView----------------------------------
            float bottomHeight = 80f;
            bottomView.Frame = new CGRect(0, viewFrame.Height - bottomHeight, viewFrame.Width, bottomHeight);

            mobileLoungeSolution.Frame = new CGRect(mainMargin, 0f, viewFrame.Width - (mainMargin * 2), 20f);

            versionNo.Frame = new CGRect(210f, 7f, 70f, 12f);

            providedBy.Frame = new CGRect(mobileLoungeSolution.Frame.X, mobileLoungeSolution.Frame.Height, mobileLoungeSolution.Frame.Width, 20f);

            copyright.Frame = new CGRect(mobileLoungeSolution.Frame.X, bottomView.Frame.Height - 20f - (float)View.SafeAreaInsets.Bottom, mobileLoungeSolution.Frame.Width, 20f);

            //-------------------------login view-------------------------------------
            loginView.Frame = new CGRect(0f, titleView.Frame.Height,
                viewFrame.Width, viewFrame.Height - titleView.Frame.Height - bottomView.Frame.Height);
            //Portrait mode
            float LoginMargin = 20f;
            if (AppDelegate.CurrentDevice.Type == DeviceType.Tablet)
            {
                if (viewFrame.Height > viewFrame.Width)//portrate
                    LoginMargin = 200f;
                else//lanscape
                    LoginMargin = 400f;
            }


            float spacing = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad ? 15f : 10f;



            if (viewFrame.Height > viewFrame.Width || UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                float elementHeight = (float)loginView.Frame.Height / 7f;
                if (AppDelegate.CurrentDevice.Type == DeviceType.Tablet)
                {
                    elementHeight = elementHeight / 1.5f;
                }

                float centerHeightOffset = ((float)loginView.Frame.Height - (elementHeight * 3 + spacing * 2f)) / 3f;
                UsernameTextField.Frame = new CGRect(LoginMargin, centerHeightOffset,
                    viewFrame.Width - (LoginMargin * 2), elementHeight);

                PasswordTextField.Frame = new CGRect(LoginMargin,
                    UsernameTextField.Frame.Y + UsernameTextField.Frame.Height + spacing,
                     UsernameTextField.Frame.Width, elementHeight);

                ButtonCancel.Frame = new CGRect(LoginMargin,
                    PasswordTextField.Frame.Y + PasswordTextField.Frame.Height + spacing,
                    (UsernameTextField.Frame.Width) / 2f - 5f,
                    elementHeight);

                float btnLoginX = (float)ButtonCancel.Frame.X + (float)ButtonCancel.Frame.Width + 10f;
                ButtonLogin.Frame = new CGRect(btnLoginX,
                    ButtonCancel.Frame.Y,
                    ButtonCancel.Frame.Width,
                    elementHeight);

            }//landscape mode
            else
            {
                float elementHeight = 30f;
                float centerHeightOffset = loginView.Frame.Height < 120f ? 0f : 20f;

                float landscapeSpacing = 10f;
                float elementWidth = ((float)loginView.Frame.Width - 40f) / 2f - 5f;

                UsernameTextField.Frame = new CGRect(mainMargin,
                    centerHeightOffset,
                    elementWidth,
                    elementHeight);
                ButtonLogin.Frame = new CGRect(UsernameTextField.Frame.X + UsernameTextField.Frame.Width + +landscapeSpacing,
                    centerHeightOffset,
                    elementWidth,
                    elementHeight);

                PasswordTextField.Frame = new CGRect(mainMargin,
                    centerHeightOffset + 40f,
                    elementWidth,
                    elementHeight);

                ButtonCancel.Frame = new CGRect(ButtonLogin.Frame.X,
                    centerHeightOffset + 40f,
                    elementWidth,
                    elementHeight);

            }

            //-------------scroll view---------------------------------------------
            loginScrollView.Frame = new CGRect(0f, 0f,
                this.View.Frame.Width, this.View.Frame.Height);
            loginScrollView.BackgroundColor = UIColor.White;

            loginScrollView.ContentSize = new CGSize(this.View.Frame.Width, scrollMaxHeight * scaleFactor);

            //lined view
            linedView.Frame = new CGRect(0f, titleView.Frame.Y,
                UIScreen.MainScreen.Bounds.Width, bottomView.Frame.Bottom);

            linedView.LinesY = new nfloat[3] { titleView.Frame.Y, loginView.Frame.Y,
                bottomView.Frame.Y };
        }

        partial void Cancel(UIButton sender)
        {
            PasswordTextField.Text = "";
            UsernameTextField.Text = "";


        }

        public void onTextChanged(object sender, EventArgs e)
        {
            if (CanLogin)
            {
                ButtonLogin.BackgroundColor = UIColor.DarkGray;

            }
            else
            {
                ButtonLogin.BackgroundColor = UIColor.LightGray;
            }

        }

        public async void onLogin(object sender, EventArgs e)
        {
            if (CanLogin && !isLoggingIn)
            {
                isLoggingIn = true;
                ShowLoading(loadingText);

                await Task.Delay(250);

                loginCheck(UsernameTextField.Text, PasswordTextField.Text);
              

              /*  var t = new Task(new Action<object>((credentials) =>
                {
                    var userPassPair = (System.Collections.Generic.KeyValuePair<string, string>)credentials;
                    loginCheck(userPassPair.Key, userPassPair.Value);
                }), new System.Collections.Generic.KeyValuePair<string, string>(UsernameTextField.Text, PasswordTextField.Text));
                t.Start(); */
            }
        }
        
        //Based on tests, actually it does nothing!!!
        private IWebProxy GetProxy()
        {
            HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
            // Obtain the 'Proxy' of the  Default browser.  
            IWebProxy proxy2 = myWebRequest.Proxy;

            IWebProxy proxy = null;
            try
            {
                var ps = CoreFoundation.CFNetwork.GetSystemProxySettings();
                //proxy = CoreFoundation.CFNetwork.GetDefaultProxy();
                if (ps.HTTPEnable)
                    proxy = new WebProxy(ps.HTTPProxy, ps.HTTPPort) {
                        UseDefaultCredentials = true
                    };

            }
            catch
            {
                proxy = null;
            }
            return proxy;
        }

        private void loginCheck(string uName, string password)
        {
            //check if internet is available
            if (!CrossConnectivity.Current.IsConnected)
            {
                string errorMessage = AppDelegate.TextProvider.GetText((int) ErrorCode.InternetConnectionError);
                var errorTitle = AppDelegate.TextProvider.GetText(2047);
                AppDelegate.Current.AIMSMessage(errorTitle, errorMessage);
            }
            MembershipProvider.CreateMembershipProvider(uName, password, GetProxy());//TODO: uncomment and pass the proxy
            //MembershipProvider.CreateMembershipProvider(uName, password, null);
            HideLoading();
            ShowLoading(authenticatingText);
            isLoggingIn = false;
            MembershipProvider.Current.Login(uName, password, (ss, ee) =>
            {
                if (ee.Item1)
                
                
                {
                    LogsRepository.AddClientInfo("User logged in", MembershipProvider.Current.LoggedinUser.ToString());
                    if (MembershipProvider.Current.UserLounges.Count() > 0)
                    {
                        try
                        {
                            AppDelegate.TriggerApplicationStart();
                            //result = false;
                            if (MembershipProvider.Current.SelectedLounge == null)
                            {
                                InvokeOnMainThread(() =>
                                {
                                    UsernameTextField.Text = "";
                                    PasswordTextField.Text = "";


                                    UIViewTransition.displayFromRightToLeftLinear(this, AppDelegate.LoungeSelectionView, 0.3f);
                                    //this.View.Superview.AddSubview(AppDelegate.LoungeSelectionView.View);
                                    //this.View.RemoveFromSuperview();
                                });
                            }
                            else//go to airlines activity
                            {
                                LogsRepository.AddClientInfo("Lounge selected", string.Format("The user {0} selected the lounge", MembershipProvider.Current.LoggedinUser.Username));
                                LogsRepository.AddInfo("Lounge selected", string.Format("Lounge detail: {0} | WSID: {1}", MembershipProvider.Current.SelectedLounge.LoungeName, MembershipProvider.Current.SelectedLounge.WorkstationID));

                                AppData.SelectedLounge = new LoungeCellData(MembershipProvider.Current.SelectedLounge);//Siavash: added in case that only one lounge is available
                                InvokeOnMainThread(() =>
                                {
                                    UsernameTextField.Text = "";
                                    PasswordTextField.Text = "";

                                    //TODO test, AirlinesView must be presented by loungeSelection
                                    //in order to allow dismissing and proper layout
                                    this.View.Superview.AddSubview(AppDelegate.LoungeSelectionView.View);
                                    this.View.RemoveFromSuperview();
                                    AppDelegate.LoungeSelectionView.PresentViewController(AppDelegate.AirlinesView, true, null);
                                    //this.View.Superview.AddSubview(AppDelegate.AirlinesView.View);
                                    //this.View.RemoveFromSuperview();
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            HideLoading();
                            LogsRepository.AddError("Error in presenting view controllers", ex);
                            Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                            //loading.RemoveFromSuperview();
                            //result = false;
                        }
                        finally
                        {
                            HideLoading();
                        }
                    }
                    else
                    {
                        HideLoading();
                        LogsRepository.AddClientWarning("No lounge is available", string.Format("There were no lounge for the user {0}", MembershipProvider.Current.LoggedinUser.Username));
                        AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(2049), AppDelegate.TextProvider.GetText(1005));
                        //loading.RemoveFromSuperview();
                        //result = false;
                    }
                }
                else
                {
                    HideLoading();
                    string errorMessage = AppDelegate.TextProvider.GetText((int)ee.Item2);
                    var errorTitle = AppDelegate.TextProvider.GetText(2047);
                    LogsRepository.AddError("User login failed", string.Format("A user with username '{0}' failed to login into AIMS,\nError Code: {1}\nError Message: {2}", uName, ee.Item2, errorMessage));
                    //result = false;
                    AppDelegate.Current.AIMSMessage(errorTitle, errorMessage);

                }
            });

        }
        
        public void keyboardWillShow(object o, UIKeyboardEventArgs e)
        {
            CGRect keyboardFrame = e.FrameEnd;

            UIView firstResponder = AutoScrollUtility.findFirstResponder(loginScrollView);
            if (firstResponder != null)
            {
                AutoScrollUtility.autoScrollTo(this, loginScrollView,
                    firstResponder.Superview.Frame.Y + ButtonLogin.Frame.Bottom, keyboardFrame.Height);
            }
        }

        public void keyboardWillHide(object o, UIKeyboardEventArgs e)
        {
            nfloat currentY = loginScrollView.ContentOffset.Y;

            //go to top
            if ((scrollMaxHeight * scaleFactor) < loginScrollView.Frame.Height)
            {
                loginScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
                loginScrollView.SetContentOffset(new CGPoint(0f, 0f), true);
            }//stay
            else if (currentY < (scrollMaxHeight * scaleFactor) - loginScrollView.Frame.Height)
            {
                loginScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
            }
            else//go to bottom
            {
                loginScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
                loginScrollView.SetContentOffset(new CGPoint(0f, (scrollMaxHeight * scaleFactor) - loginScrollView.Frame.Height), true);
            }
        }

    }
}