﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("AirLineViewController")]
    partial class AirLineViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AirlineCollectionView airlineCollectionView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView background { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView bottomView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton cancelSearchButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView cardBackground { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton cardBackgroundButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UICollectionView cardCollectionView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISearchBar cardSearchBar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.CardsView cardsView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView cardTopImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel cardTopLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView cardTopView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton leftUp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton loungeBackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton loungeBackButtonLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AirLineMainView masterView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.CustomProgressControl progressView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton rightUp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton scan { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISearchBar searchBar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton searchButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISearchController searchDisplayController { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView SearchView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel title { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView topIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView topView { get; set; }

        [Action ("cancelSearch:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void cancelSearch (UIKit.UIButton sender);

        [Action ("SearchButton:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SearchButton (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (airlineCollectionView != null) {
                airlineCollectionView.Dispose ();
                airlineCollectionView = null;
            }

            if (background != null) {
                background.Dispose ();
                background = null;
            }

            if (bottomView != null) {
                bottomView.Dispose ();
                bottomView = null;
            }

            if (cancelSearchButton != null) {
                cancelSearchButton.Dispose ();
                cancelSearchButton = null;
            }

            if (cardBackground != null) {
                cardBackground.Dispose ();
                cardBackground = null;
            }

            if (cardBackgroundButton != null) {
                cardBackgroundButton.Dispose ();
                cardBackgroundButton = null;
            }

            if (cardCollectionView != null) {
                cardCollectionView.Dispose ();
                cardCollectionView = null;
            }

            if (cardSearchBar != null) {
                cardSearchBar.Dispose ();
                cardSearchBar = null;
            }

            if (cardsView != null) {
                cardsView.Dispose ();
                cardsView = null;
            }

            if (cardTopImage != null) {
                cardTopImage.Dispose ();
                cardTopImage = null;
            }

            if (cardTopLabel != null) {
                cardTopLabel.Dispose ();
                cardTopLabel = null;
            }

            if (cardTopView != null) {
                cardTopView.Dispose ();
                cardTopView = null;
            }

            if (leftUp != null) {
                leftUp.Dispose ();
                leftUp = null;
            }

            if (loungeBackButton != null) {
                loungeBackButton.Dispose ();
                loungeBackButton = null;
            }

            if (loungeBackButtonLabel != null) {
                loungeBackButtonLabel.Dispose ();
                loungeBackButtonLabel = null;
            }

            if (masterView != null) {
                masterView.Dispose ();
                masterView = null;
            }

            if (progressView != null) {
                progressView.Dispose ();
                progressView = null;
            }

            if (rightUp != null) {
                rightUp.Dispose ();
                rightUp = null;
            }

            if (scan != null) {
                scan.Dispose ();
                scan = null;
            }

            if (searchBar != null) {
                searchBar.Dispose ();
                searchBar = null;
            }

            if (searchButton != null) {
                searchButton.Dispose ();
                searchButton = null;
            }

            if (searchDisplayController != null) {
                searchDisplayController.Dispose ();
                searchDisplayController = null;
            }

            if (SearchView != null) {
                SearchView.Dispose ();
                SearchView = null;
            }

            if (title != null) {
                title.Dispose ();
                title = null;
            }

            if (topIcon != null) {
                topIcon.Dispose ();
                topIcon = null;
            }

            if (topView != null) {
                topView.Dispose ();
                topView = null;
            }
        }
    }
}