﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIMS_IOS
{
    public interface IPassengerRefresher
    {
        void RefreshPassenger();
    }
}
