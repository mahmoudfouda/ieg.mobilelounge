using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace AIMS_IOS
{
    public class NoRotationViewController : UIViewController
    {
        public override UIInterfaceOrientation InterfaceOrientation {
            get {
                return UIInterfaceOrientation.Portrait;
            }
        }

        public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
        {
            return false;

        }

        public override bool ShouldAutorotate()
        {
            return false;
        }

        public override bool ShouldAutomaticallyForwardRotationMethods
        {
            get
            {
                return false;
            }
        }
        
        public NoRotationViewController(UIViewController child)
        {
            this.AddChildViewController(child);
            this.View.AddSubview(child.View);
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return UIInterfaceOrientationMask.Portrait;
        }

        public override UIInterfaceOrientation PreferredInterfaceOrientationForPresentation()
        {
            return UIInterfaceOrientation.Portrait;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;
        }
    }
}