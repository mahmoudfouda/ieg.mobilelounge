﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace AIMS_IOS
{
    public class NavigationController : UINavigationController
    {
        UIViewController firstController;

        public NavigationController(UIViewController firstView) : base(firstView)
        {
            firstController = firstView;
        }

        public override void ViewDidLoad()
        {
            firstController = AppDelegate.LoginView;
            this.PushViewController(firstController, true);

            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;

        }

    }
}
