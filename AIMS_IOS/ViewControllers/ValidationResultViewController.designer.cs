﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("ValidationResultViewController")]
    partial class ValidationResultViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel airlineLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel airlineTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView arrivalImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel arrivalLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel arrivalTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView cardImage1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView cardImage2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel cardLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.ResultCardView cardView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel classLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel classTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton confirmButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView departureImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel departureLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel departureTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.ResultDepartureView departureView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton detailsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.ValidationDetailsView detailsView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightInfoLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightInfoTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.ResultFlightInfoView flightInfoView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgFlightPlane { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel loyaltyLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel loyaltyTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel nameLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIProgressView prgBarFlight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView scrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UICollectionView servicesCollectionView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.ResultTitleView titleView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel welcomeLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (airlineLabel != null) {
                airlineLabel.Dispose ();
                airlineLabel = null;
            }

            if (airlineTitleLabel != null) {
                airlineTitleLabel.Dispose ();
                airlineTitleLabel = null;
            }

            if (arrivalImage != null) {
                arrivalImage.Dispose ();
                arrivalImage = null;
            }

            if (arrivalLabel != null) {
                arrivalLabel.Dispose ();
                arrivalLabel = null;
            }

            if (arrivalTitleLabel != null) {
                arrivalTitleLabel.Dispose ();
                arrivalTitleLabel = null;
            }

            if (cardImage1 != null) {
                cardImage1.Dispose ();
                cardImage1 = null;
            }

            if (cardImage2 != null) {
                cardImage2.Dispose ();
                cardImage2 = null;
            }

            if (cardLabel != null) {
                cardLabel.Dispose ();
                cardLabel = null;
            }

            if (cardView != null) {
                cardView.Dispose ();
                cardView = null;
            }

            if (classLabel != null) {
                classLabel.Dispose ();
                classLabel = null;
            }

            if (classTitleLabel != null) {
                classTitleLabel.Dispose ();
                classTitleLabel = null;
            }

            if (confirmButton != null) {
                confirmButton.Dispose ();
                confirmButton = null;
            }

            if (departureImage != null) {
                departureImage.Dispose ();
                departureImage = null;
            }

            if (departureLabel != null) {
                departureLabel.Dispose ();
                departureLabel = null;
            }

            if (departureTitleLabel != null) {
                departureTitleLabel.Dispose ();
                departureTitleLabel = null;
            }

            if (departureView != null) {
                departureView.Dispose ();
                departureView = null;
            }

            if (detailsButton != null) {
                detailsButton.Dispose ();
                detailsButton = null;
            }

            if (detailsView != null) {
                detailsView.Dispose ();
                detailsView = null;
            }

            if (flightInfoLabel != null) {
                flightInfoLabel.Dispose ();
                flightInfoLabel = null;
            }

            if (flightInfoTitleLabel != null) {
                flightInfoTitleLabel.Dispose ();
                flightInfoTitleLabel = null;
            }

            if (flightInfoView != null) {
                flightInfoView.Dispose ();
                flightInfoView = null;
            }

            if (imgFlightPlane != null) {
                imgFlightPlane.Dispose ();
                imgFlightPlane = null;
            }

            if (loyaltyLabel != null) {
                loyaltyLabel.Dispose ();
                loyaltyLabel = null;
            }

            if (loyaltyTitleLabel != null) {
                loyaltyTitleLabel.Dispose ();
                loyaltyTitleLabel = null;
            }

            if (nameLabel != null) {
                nameLabel.Dispose ();
                nameLabel = null;
            }

            if (prgBarFlight != null) {
                prgBarFlight.Dispose ();
                prgBarFlight = null;
            }

            if (scrollView != null) {
                scrollView.Dispose ();
                scrollView = null;
            }

            if (servicesCollectionView != null) {
                servicesCollectionView.Dispose ();
                servicesCollectionView = null;
            }

            if (titleView != null) {
                titleView.Dispose ();
                titleView = null;
            }

            if (welcomeLabel != null) {
                welcomeLabel.Dispose ();
                welcomeLabel = null;
            }
        }
    }
}