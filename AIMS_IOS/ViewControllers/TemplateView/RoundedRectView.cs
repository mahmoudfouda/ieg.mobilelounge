using System;
using Foundation;
using UIKit;
using System.Drawing;
using CoreGraphics;
using CoreAnimation;

namespace AIMS_IOS
{
    class RoundedRectView : UIView
    {
        private float fCornerRadius;
        public float FCornerRadius
        {
            get { return fCornerRadius; }
            set
            {
                fCornerRadius = value;
                this.UpdateMask();
            }

        }


        public static UIRectCorner RoundedTopCorners = UIRectCorner.TopLeft | UIRectCorner.TopRight;
        public static UIRectCorner RoundedBottomCorners = UIRectCorner.BottomLeft | UIRectCorner.BottomRight;
        public static UIRectCorner RoundedLeftCorners = UIRectCorner.TopLeft | UIRectCorner.BottomLeft;
        public static UIRectCorner RoundedRightCorners = UIRectCorner.TopRight | UIRectCorner.BottomRight;

        public UIRectCorner eRoundedCorners;
        public RoundedRectView() : base()
        {
            this.fCornerRadius = 25f;
            this.eRoundedCorners = UIRectCorner.AllCorners;
            this.UpdateMask();
        }

        public RoundedRectView(CGRect rect) : base(rect)
        {
            this.fCornerRadius = 25f;
            this.eRoundedCorners = UIRectCorner.AllCorners;
            this.UpdateMask();

        }

        public RoundedRectView(RectangleF rect, UIColor oBackgroundColor) : base(rect)
        {
            this.fCornerRadius = 25f;
            this.eRoundedCorners = UIRectCorner.AllCorners;
            this.BackgroundColor = oBackgroundColor;
            this.UpdateMask();
        }

        public RoundedRectView(RectangleF rect, UIColor oBackgroundColor, UIRectCorner eCornerFlags) : base(rect)
        {
            this.fCornerRadius = 25f;
            this.eRoundedCorners = eCornerFlags;
            this.BackgroundColor = oBackgroundColor;
            this.UpdateMask();
        }

        private void UpdateMask()
        {
            UIBezierPath oMaskPath = UIBezierPath.FromRoundedRect(this.Bounds,
                this.eRoundedCorners, new SizeF(this.fCornerRadius, this.fCornerRadius));

            CAShapeLayer oMaskLayer = new CAShapeLayer();
            oMaskLayer.Frame = this.Bounds;
            oMaskLayer.Path = oMaskPath.CGPath;
            this.Layer.Mask = oMaskLayer; 

        }

        public override UIViewAutoresizing AutoresizingMask
        {
            get
            {
                return base.AutoresizingMask;
            }
            set
            {
                base.AutoresizingMask = value;
                this.UpdateMask();
            }
        }

        public override CGRect Bounds
        {
            get
            {
                return base.Bounds;
            }
            set
            {
                base.Bounds = value;
                this.UpdateMask();
            }
        }

        public override CGRect Frame
        {
            get
            {
                return base.Frame;
            }
            set
            {
                base.Frame = value;
                this.UpdateMask();
            }
        }
    }
}