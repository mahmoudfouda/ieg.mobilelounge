﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("SinglePageMIViewController")]
    partial class SinglePageMIViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView agentViewSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView bottomViewSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnBackSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnBackTextSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnConfirmSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnScanSPMI1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView flightViewSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgAgentAirlineSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgCardSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgSelectedAirlineSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAgentAirlineSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblClassSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFFNSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFlightSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFromFullSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFromSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblNotesSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblPNRSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSeatSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblToFullSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblToSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainViewSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView passengerViewSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView selectedAirlineViewSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtClassSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFFNSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFlightSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFromSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFullNameSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView txtNotesSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtPNRSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtSeatSPMI { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtToSPMI { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (agentViewSPMI != null) {
                agentViewSPMI.Dispose ();
                agentViewSPMI = null;
            }

            if (bottomViewSPMI != null) {
                bottomViewSPMI.Dispose ();
                bottomViewSPMI = null;
            }

            if (btnBackSPMI != null) {
                btnBackSPMI.Dispose ();
                btnBackSPMI = null;
            }

            if (btnBackTextSPMI != null) {
                btnBackTextSPMI.Dispose ();
                btnBackTextSPMI = null;
            }

            if (btnConfirmSPMI != null) {
                btnConfirmSPMI.Dispose ();
                btnConfirmSPMI = null;
            }

            if (btnScanSPMI1 != null) {
                btnScanSPMI1.Dispose ();
                btnScanSPMI1 = null;
            }

            if (flightViewSPMI != null) {
                flightViewSPMI.Dispose ();
                flightViewSPMI = null;
            }

            if (imgAgentAirlineSPMI != null) {
                imgAgentAirlineSPMI.Dispose ();
                imgAgentAirlineSPMI = null;
            }

            if (imgCardSPMI != null) {
                imgCardSPMI.Dispose ();
                imgCardSPMI = null;
            }

            if (imgSelectedAirlineSPMI != null) {
                imgSelectedAirlineSPMI.Dispose ();
                imgSelectedAirlineSPMI = null;
            }

            if (lblAgentAirlineSPMI != null) {
                lblAgentAirlineSPMI.Dispose ();
                lblAgentAirlineSPMI = null;
            }

            if (lblClassSPMI != null) {
                lblClassSPMI.Dispose ();
                lblClassSPMI = null;
            }

            if (lblFFNSPMI != null) {
                lblFFNSPMI.Dispose ();
                lblFFNSPMI = null;
            }

            if (lblFlightSPMI != null) {
                lblFlightSPMI.Dispose ();
                lblFlightSPMI = null;
            }

            if (lblFromFullSPMI != null) {
                lblFromFullSPMI.Dispose ();
                lblFromFullSPMI = null;
            }

            if (lblFromSPMI != null) {
                lblFromSPMI.Dispose ();
                lblFromSPMI = null;
            }

            if (lblNotesSPMI != null) {
                lblNotesSPMI.Dispose ();
                lblNotesSPMI = null;
            }

            if (lblPNRSPMI != null) {
                lblPNRSPMI.Dispose ();
                lblPNRSPMI = null;
            }

            if (lblSeatSPMI != null) {
                lblSeatSPMI.Dispose ();
                lblSeatSPMI = null;
            }

            if (lblToFullSPMI != null) {
                lblToFullSPMI.Dispose ();
                lblToFullSPMI = null;
            }

            if (lblToSPMI != null) {
                lblToSPMI.Dispose ();
                lblToSPMI = null;
            }

            if (mainViewSPMI != null) {
                mainViewSPMI.Dispose ();
                mainViewSPMI = null;
            }

            if (passengerViewSPMI != null) {
                passengerViewSPMI.Dispose ();
                passengerViewSPMI = null;
            }

            if (selectedAirlineViewSPMI != null) {
                selectedAirlineViewSPMI.Dispose ();
                selectedAirlineViewSPMI = null;
            }

            if (txtClassSPMI != null) {
                txtClassSPMI.Dispose ();
                txtClassSPMI = null;
            }

            if (txtFFNSPMI != null) {
                txtFFNSPMI.Dispose ();
                txtFFNSPMI = null;
            }

            if (txtFlightSPMI != null) {
                txtFlightSPMI.Dispose ();
                txtFlightSPMI = null;
            }

            if (txtFromSPMI != null) {
                txtFromSPMI.Dispose ();
                txtFromSPMI = null;
            }

            if (txtFullNameSPMI != null) {
                txtFullNameSPMI.Dispose ();
                txtFullNameSPMI = null;
            }

            if (txtNotesSPMI != null) {
                txtNotesSPMI.Dispose ();
                txtNotesSPMI = null;
            }

            if (txtPNRSPMI != null) {
                txtPNRSPMI.Dispose ();
                txtPNRSPMI = null;
            }

            if (txtSeatSPMI != null) {
                txtSeatSPMI.Dispose ();
                txtSeatSPMI = null;
            }

            if (txtToSPMI != null) {
                txtToSPMI.Dispose ();
                txtToSPMI = null;
            }
        }
    }
}