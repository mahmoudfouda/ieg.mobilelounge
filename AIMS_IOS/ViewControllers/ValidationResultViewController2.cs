﻿using AIMS;
using AIMS.Models;
using AIMS_IOS.Utility;
using CoreGraphics;
using Foundation;
using Ieg.Mobile.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace AIMS_IOS
{
    public partial class ValidationResultViewController2 : AIMSViewController, IPassengerRefresher
    {
        #region UI Fields and Properties
        string delayedText, onTimeText;
        private bool isInitialized = false, isLandscape = false, isPad = false;

        private string BarcodeString { get; set; }

        private LinedView linedView;

        private PassengerServiceCollectionViewSource ServicesViewSource { get; set; }

        public UIButton ConfirmButton
        {
            get { return confirmButton2; }
        }

        public bool IsGuestMode { get; set; } = false;

        #endregion

        public ValidationResultViewController2(IntPtr handle) : base(handle)
        {
        }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() =>
            {
                onTimeText = AppDelegate.TextProvider.GetText(2021);
                delayedText = AppDelegate.TextProvider.GetText(2020);

                lblFlightLabelVR2.Text = string.Format("{0}:", AppDelegate.TextProvider.GetText(2015).ToUpper());//FLIGHT
                lblPNRLabelVR2.Text = string.Format("{0}:", AppDelegate.TextProvider.GetText(2044).ToUpper());//PNR
                lblFFNLabelVR2.Text = string.Format("{0}:", AppDelegate.TextProvider.GetText(2141).ToUpper());//CARD

                lblBoardingTimeLabelVR2.Text = AppDelegate.TextProvider.GetText(2061).ToUpper();//BOARDING AT
                lblTerminalLabelVR2.Text = AppDelegate.TextProvider.GetText(2061).ToUpper();//TERMINAL
                lblGateLabelVR2.Text = AppDelegate.TextProvider.GetText(2058).ToUpper();//GATE
                lblSeatLabelVR2.Text = AppDelegate.TextProvider.GetText(2063).ToUpper();//SEAT

                detailsButton2.SetTitle(AppDelegate.TextProvider.GetText(2045), UIControlState.Normal);//"Details"
            });
        }

        private void SetFonts()
        {
            var fontScaleFactor = ScaleFactor;

            //if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            //    fontScaleFactor = (float)ScaleFactor * ((float)3 / 4);

            nfloat xxLargeTexts = 30f * fontScaleFactor, xLargeTexts = 22f * fontScaleFactor, largeTexts = 17f * fontScaleFactor, mediumTexts = 13.5f * fontScaleFactor, smallTexts = 12f * fontScaleFactor;

            var titleFont = UIFont.BoldSystemFontOfSize(xxLargeTexts);//Boarding At (Time value)
            var largeFont = UIFont.SystemFontOfSize(xLargeTexts);//Reserved
            var largeBoldFont = UIFont.BoldSystemFontOfSize(xLargeTexts);//PAX Name (Full Name)
            var smallFont = UIFont.SystemFontOfSize(smallTexts);//CARD names
            var mediumFont = UIFont.SystemFontOfSize(mediumTexts, UIFontWeight.Light);//Labels
            var mediumBoldFont = UIFont.SystemFontOfSize(largeTexts);//Values

            lblFlightLabelVR2.Font = lblPNRLabelVR2.Font = lblBoardingDateVR2.Font =
                lblFFNLabelVR2.Font = lblFromVR2.Font = lblToVR2.Font =
                lblWeatherVR2.Font = lblTimeVR2.Font = mediumFont;

            //lblFlightLabelVR2.Frame = new CGRect();
            //lblFlightVR2.Frame = new CGRect();

            //lblPNRLabelVR2.Frame = new CGRect();
            //lblPNRVR2.Frame = new CGRect();

            lblFlightVR2.Font = lblPNRVR2.Font = lblFFNVR2.Font = lblFullNameVR2.Font =
                lblFromFullAirportNameVR2.Font = lblToFullAirportNameVR2.Font = lblTerminalVR2.Font =
                lblGateVR2.Font = lblSeatVR2.Font = mediumBoldFont;

            lblBoardingTimeVR2.Font = largeBoldFont;

            lblCardNameVR2.Font = lblCardOtherNameVR2.Font = lblBoardingTimeLabelVR2.Font = lblTerminalLabelVR2.Font = lblGateLabelVR2.Font =
                lblSeatLabelVR2.Font = smallFont;
            lblCardNameVR2.MinimumFontSize = lblCardOtherNameVR2.MinimumFontSize = 6f * fontScaleFactor;
        }

        private void LoadFullAirportName(string IATA_Code, UILabel label)
        {
            InvokeOnMainThread(() =>
            {
                label.Text = "...";
            });
            using (var ar = new AirportsRepository())
            {
                ar.OnRepositoryChanged += (data) =>
                {
                    var airport = (Airport)data;
                    if (airport != null)
                        InvokeOnMainThread(() =>
                        {
                            label.Text = airport.Name;
                        });
                };
                ar.GetAirport(IATA_Code, true);
            }
        }

        private void ClearContents()
        {
            InvokeOnMainThread(() =>
            {
                servicesCollectionViewVR2.Hidden = true;
                imgSelectedCardVR2.Image = null;
                imgSelectedCardOtherVR2.Image = null;
                imgSelectedAirlineVR2.Image = null;

                lblFFNVR2.Text = lblFlightVR2.Text = lblPNRVR2.Text = lblFullNameVR2.Text =
                lblCardNameVR2.Text = lblCardOtherNameVR2.Text = lblBoardingDateVR2.Text = "";

                lblFromVR2.Text = lblFromFullAirportNameVR2.Text = lblToVR2.Text = lblToFullAirportNameVR2.Text =
                lblBoardingTimeVR2.Text = lblTerminalVR2.Text = lblGateVR2.Text = lblSeatVR2.Text =
                lblTimeVR2.Text = lblWeatherVR2.Text = "...";
            });
        }

        private void SetContents()
        {
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            InvokeOnMainThread(() =>
            {
                try//tired of checkings  >:-|
                {
                    imgSelectedCardOtherVR2.Image = null;
                    lblCardOtherNameVR2.Text = "";
                    lblBoardingTimeVR2.Text = "...";
                    lblTerminalVR2.Text = "...";
                    lblGateVR2.Text = "...";
                    lblSeatVR2.Text = "...";
                    lblTimeVR2.Text = "...";
                    lblWeatherVR2.Text = "...";

                    if (!IsGuestMode)
                    {
                        #region Loading passenger's first card
                        imgSelectedCardVR2.Image = null;
                        lblCardNameVR2.Text = "";
                        if (MembershipProvider.Current.SelectedCard != null)
                        {
                            lblCardNameVR2.Text = MembershipProvider.Current.SelectedCard.Name;
                            if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                            {
                                imgSelectedCardVR2.Image = MembershipProvider.Current.SelectedCard.CardPictureBytes.ToImage();
                            }
                            else
                            {
                                imgSelectedCardVR2.Image = UIImage.FromFile("Error/not_available_pic_off");
                                ImageAdapter.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (imageBytes) =>
                                {
                                    var image = imageBytes.ToImage();
                                    InvokeOnMainThread(() =>
                                    {
                                        imgSelectedCardVR2.Image = image;
                                    });
                                });
                            }
                        }
                        #endregion

                        #region Loading Passenger's second card
                        if (MembershipProvider.Current.SelectedPassenger.OtherCardID > 0)
                        {
                            imgSelectedCardOtherVR2.Hidden = false;
                            imgSelectedCardOtherVR2.Image = UIImage.FromFile("Error/not_available_pic_off");
                            lblCardOtherNameVR2.Text = string.Format("Card Id: {0}", MembershipProvider.Current.SelectedPassenger.OtherCardID);

                            var otherCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedPassenger.OtherCardID);
                            if (otherCard != null)
                            {
                                lblCardOtherNameVR2.Text = otherCard.Name;
                                if (otherCard.CardPictureBytes != null && otherCard.CardPictureBytes.Length > 0)
                                {
                                    imgSelectedCardOtherVR2.Image = otherCard.CardPictureBytes.ToImage();
                                    //imgSelectedCardOtherVR2.Hidden = false;
                                }
                                else
                                {
                                    //imgSelectedCardOtherVR2.Image = UIImage.FromFile("Error/not_available_pic_off");
                                    ImageAdapter.LoadImage(otherCard.ImageHandle, (imageBytes) =>
                                    {
                                        var image = imageBytes.ToImage();
                                        InvokeOnMainThread(() =>
                                        {
                                            imgSelectedCardOtherVR2.Image = image;
                                        //imgSelectedCardOtherVR2.Hidden = false;
                                    });
                                    });
                                }
                            }
                            //else imgSelectedCardOtherVR2.Hidden = true;
                        }
                        else imgSelectedCardOtherVR2.Hidden = true;
                        #endregion

                        #region Filling up the lables
                        lblFullNameVR2.Text = MembershipProvider.Current.SelectedPassenger.FullName;
                        //TODO: Add lable for host name
                        /* string.Format("{0}{1}", MembershipProvider.Current.SelectedPassenger.FullName, 
                            string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedPassenger.HostName) ? "" : string.Format(" (Host: '{0}')", MembershipProvider.Current.SelectedPassenger.HostName));*/
                        lblFFNVR2.Text = MembershipProvider.Current.SelectedPassenger.FFN;
                        lblFlightVR2.Text = string.Format("{0} {1} {2}",
                            MembershipProvider.Current.SelectedPassenger.FlightCarrier,
                            string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.FlightNumber) ? string.Empty: MembershipProvider.Current.SelectedPassenger.FlightNumber.TrimStart('0'),
                            MembershipProvider.Current.SelectedPassenger.TrackingClassOfService);
                        lblPNRVR2.Text = MembershipProvider.Current.SelectedPassenger.PNR;
                        var bpValdationDate =
                            MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate != null && MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate != DateTime.MinValue ?
                            MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate : MembershipProvider.Current.SelectedPassenger.TrackingTimestamp;
                        lblBoardingDateVR2.Text = bpValdationDate.ToString("d MMM yyyy");

                        if (MembershipProvider.Current.SelectedAirline != null)
                        {
                            #region Filling the Airline
                            if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes != null && MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length > 0)
                            {
                                imgSelectedAirlineVR2.Image = MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.ToImage();
                            }
                            else
                            {
                                ImageAdapter.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (imageBytes) =>
                                {
                                    var image = imageBytes.ToImage();
                                    InvokeOnMainThread(() =>
                                    {
                                        imgSelectedAirlineVR2.Image = image;
                                    });
                                });
                            }
                            #endregion
                        }

                        lblFromVR2.Text = MembershipProvider.Current.SelectedPassenger.FromAirport;
                        LoadFullAirportName(lblFromVR2.Text, lblFromFullAirportNameVR2);
                        lblToVR2.Text = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedPassenger.ToAirport) ? "..." : MembershipProvider.Current.SelectedPassenger.ToAirport;
                        LoadFullAirportName(lblToVR2.Text, lblToFullAirportNameVR2);
                        // MembershipProvider.Current.SelectedPassenger.ToAirport;//TODO: Get ToAirport
                        //lblDepartureGate.Text = "N/A";//TODO: get the gate number
                        //lblDepartureGateComment.Text = "";//TODO: get the gate comments 

                        #endregion

                        #region Loading the returned Passenger Services (TODO: extract it to a method - as we need to refresh the quantities later)
                        if (MembershipProvider.Current.SelectedPassenger.PassengerServices == null)//TODO: move this into Client Core Library inside (MembershipProvider.Current.SelectedPassenger.Set{} very last line)
                            MembershipProvider.Current.SelectedPassenger.PassengerServices = new Dictionary<PassengerService, List<Passenger>>();

                        ServicesViewSource.Rows = new List<PassengerService>();
                        foreach (var guestCatKeyVal in MembershipProvider.Current.SelectedPassenger.PassengerServices)
                        {
                            var passengerService = guestCatKeyVal.Key;
                            passengerService.Quantity = guestCatKeyVal.Value.Count;

                            ServicesViewSource.Rows.Add(passengerService);
                        }
                        servicesCollectionViewVR2.Source = ServicesViewSource;
                        servicesCollectionViewVR2.ReloadData();

                        #endregion
                    }
                    else if (MembershipProvider.Current.SelectedGuest != null)
                    {
                        #region Loading guest's only card
                        imgSelectedCardVR2.Image = null;
                        lblCardNameVR2.Text = "";
                        //var guestCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedGuest.CardID);
                        //if (guestCard != null)
                        //{
                        //    lblCardNameVR2.Text = guestCard.Name;
                        //    if (guestCard.CardPictureBytes != null && guestCard.CardPictureBytes.Length > 0)
                        //    {
                        //        imgSelectedCardVR2.Image = guestCard.CardPictureBytes.ToImage();
                        //    }
                        //    else
                        //    {
                        //        imgSelectedCardVR2.Image = UIImage.FromFile("Error/not_available_pic_off");
                        //        ImageAdapter.LoadImage(guestCard.ImageHandle, (imageBytes) =>
                        //        {
                        //            var image = imageBytes.ToImage();
                        //            InvokeOnMainThread(() =>
                        //            {
                        //                imgSelectedCardVR2.Image = image;
                        //            });
                        //        });
                        //    }
                        //}
                        #endregion

                        #region Filling up the lables
                        //TODO: Add lable for host name
                        lblFullNameVR2.Text = MembershipProvider.Current.SelectedGuest.FullName;/*string.Format("{0}{1}", MembershipProvider.Current.SelectedGuest.FullName,
                            string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedGuest.HostName) ? "" : string.Format(" (Host: '{0}')", MembershipProvider.Current.SelectedGuest.HostName));*/
                        lblFFNVR2.Text = MembershipProvider.Current.SelectedGuest.FFN;
                        lblFlightVR2.Text = string.Format("{0} {1} {2}",
                            MembershipProvider.Current.SelectedGuest.FlightCarrier,
                            MembershipProvider.Current.SelectedGuest.FlightNumber,
                            MembershipProvider.Current.SelectedGuest.TrackingClassOfService);
                        lblPNRVR2.Text = MembershipProvider.Current.SelectedGuest.PNR;
                        var bpValdationDate =
                            MembershipProvider.Current.SelectedGuest.BoardingPassFlightDate != null && MembershipProvider.Current.SelectedGuest.BoardingPassFlightDate != DateTime.MinValue ?
                            MembershipProvider.Current.SelectedGuest.BoardingPassFlightDate : MembershipProvider.Current.SelectedGuest.TrackingTimestamp;
                        lblBoardingDateVR2.Text = bpValdationDate.ToString("d MMM yyyy");

                        var guestAirline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.SelectedGuest.AirlineId);
                        if (guestAirline != null)
                        {
                            #region Filling the Guest's Airline
                            if (guestAirline.AirlineLogoBytes != null && guestAirline.AirlineLogoBytes.Length > 0)
                            {
                                imgSelectedAirlineVR2.Image = guestAirline.AirlineLogoBytes.ToImage();
                            }
                            else
                            {
                                ImageAdapter.LoadImage(guestAirline.ImageHandle, (imageBytes) =>
                                {
                                    var image = imageBytes.ToImage();
                                    InvokeOnMainThread(() =>
                                    {
                                        imgSelectedAirlineVR2.Image = image;
                                    });
                                });
                            }
                            #endregion
                        }
                        lblFromVR2.Text = MembershipProvider.Current.SelectedGuest.FromAirport;
                        LoadFullAirportName(lblFromVR2.Text, lblFromFullAirportNameVR2);
                        lblToVR2.Text = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedGuest.ToAirport) ? "..." : MembershipProvider.Current.SelectedGuest.ToAirport;
                        LoadFullAirportName(lblToVR2.Text, lblToFullAirportNameVR2);
                        // MembershipProvider.Current.SelectedGuest.ToAirport;//TODO: Get ToAirport
                        //lblDepartureGate.Text = "N/A";//TODO: get the gate number
                        //lblDepartureGateComment.Text = "";//TODO: get the gate comments 

                        #endregion
                    }
                    lblPNRLabelVR2.Hidden = string.IsNullOrWhiteSpace(lblPNRVR2.Text);

                    #region Loading the Barcode if available
                    detailsButton2.Hidden = true;
#if DEBUG//Clients shouldn't see this button anyways
                    if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.BarcodeString))
                    {
                        BarcodeString = MembershipProvider.Current.SelectedPassenger.BarcodeString;
                        detailsButton2.Hidden = false;
                    }
                    else if (MembershipProvider.Current.SelectedGuest != null && !string.IsNullOrEmpty(MembershipProvider.Current.SelectedGuest.BarcodeString))
                    {
                        BarcodeString = MembershipProvider.Current.SelectedGuest.BarcodeString;
                        detailsButton2.Hidden = false;
                    }
#endif
                    #endregion

                    servicesCollectionViewVR2.Hidden = MembershipProvider.Current.SelectedPassenger.IsGuest;


#if DEBUG//TODO: remove in case the service has been implemented
                    lblBoardingTimeVR2.Text = "08:45AM";
                    lblTerminalVR2.Text = "3";
                    lblGateVR2.Text = "16";
                    lblSeatVR2.Text = "56E";
                    lblTimeVR2.Text = "+10hrs";
                    lblWeatherVR2.Text = "23 C";
#endif

                    RefreshColors();

                }
                catch (Exception ex)
                {
                    LogsRepository.AddError("Error in SetContents()", ex);
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                }
            });
        }

        public void RefreshPassenger()
        {
            ClearContents();
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            RefreshColors();

            if (MembershipProvider.Current.SelectedGuest != null)
            {
                SetContents();
            }
            else if (!MembershipProvider.Current.SelectedPassenger.IsGuest)//TODO: Avoid loading the host when validation happened (only when selected from list)
            {
                AppManager.retrieveMainPassengerOf((isSucceeded, errorCode, errorMetadata, message, passenger) =>
                {
                    MembershipProvider.Current.SelectedPassenger = passenger;
                    SetContents();
                });
            }
            else //SetContents();
            {
                AppManager.retrieveGuestPassenger((isSucceeded, errorCode, errorMetadata, message, passenger) =>
                {
                    MembershipProvider.Current.SelectedPassenger = passenger;
                    SetContents();
                });
            }
        }

        private void RefreshColors()
        {
            //var headerBarColor = new CGColor(14f / 255f, 121f / 255f, 216f / 255f);//Blue
            var headerBarColor = UIColor.Clear.FromHexString("#0e79d8").CGColor;
            var headerTextColorLight = UIColor.FromCGColor(new CGColor(1f, 1f, 1f, 0.8f));//White
            var headerTextColorDark = UIColor.DarkTextColor;

            if ((MembershipProvider.Current.SelectedGuest != null && MembershipProvider.Current.SelectedGuest.TrackingRecordID > 0) || 
                MembershipProvider.Current.SelectedPassenger.IsGuest)
            {
                //headerBarColor = new CGColor(1f, 102f / 255f, 36f / 255f);//Orange
                headerBarColor = UIColor.Clear.FromHexString("#9370db").CGColor;
              //  lblCardNameVR2.Text = "";
              //  imgSelectedCardVR2.Image = null;
                headerTextColorDark = UIColor.White;
            }

            if (isPad)
                bgOverlayViewVR2.BackgroundColor = new UIColor(0f, .75f);//Gray
            else
                bgOverlayViewVR2.BackgroundColor = new UIColor(headerBarColor);

            selectedAirlineViewVR2.BackgroundColor = imgSelectedAirlineVR2.BackgroundColor = UIColor.FromCGColor(headerBarColor);
            imgSelectedAirlineViewVR2.Layer.BorderColor = headerBarColor;

            imgSelectedAirlineVR2.BackgroundColor = imgSelectedAirlineViewVR2.BackgroundColor = detailsViewVR2.BackgroundColor = UIColor.White;
            lblFlightLabelVR2.TextColor = lblPNRLabelVR2.TextColor = //headerTextColorLight;
            lblFlightVR2.TextColor = lblPNRVR2.TextColor = headerTextColorLight; //headerTextColorDark;
        }

        public void RefreshServices()
        {
            servicesCollectionViewVR2.ReloadData();
        }

        private void initLinedView()
        {
            linedView = new LinedView(UIColor.Gray);

            this.scrollViewVR2.AddSubview(linedView);
        }

        private void UpdateLayout()
        {
            #region Preparations
            isLandscape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
            isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            linedView.SetNeedsDisplay();

            nfloat screenWidth = UIScreen.MainScreen.Bounds.Width;
            nfloat screenHeight = UIScreen.MainScreen.Bounds.Height;// (!isLandscape ? UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width);

            //nfloat screenRatio = UIScreen.MainScreen.Bounds.Width / UIScreen.MainScreen.Bounds.Height;
            nfloat screenRatio = 375f / 667f;// isLandscape ? 667f/375f : 375f/667f;

            if (isPad)//Experimental
            {
                //screenWidth = (isLandscape ? screenHeight : screenWidth) * ((float)3.0f / 4.0f);
                //screenWidth *= ((float)2.0f / 3.0f);
                //screenHeight = (float)screenWidth / (float)screenRatio;
                screenHeight *= ((float)3.0f / 4.0f);
                screenWidth = screenHeight * screenRatio;
            }
            //else if (!isLandscape)//Only on iPhone (Portrait mode) it needs
            //    screenHeight -= 20f;

            ScaleFactor = screenHeight / (!isPad && isLandscape ? 375f : 667f);

             if (!isPad && !isLandscape && ScaleFactor > 1f)
                ScaleFactor = 1f;


            nfloat margin = 8f * ScaleFactor;
            nfloat doubleMargin = margin * 2f;

            nfloat xOffset = (UIScreen.MainScreen.Bounds.Width - screenWidth) / 2f;//isPad ? (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f) : 0f; 

            nfloat yOffset = (UIScreen.MainScreen.Bounds.Height - screenHeight) / 2f;
            //nfloat yOffset = isLandscape ? 0f :
            //    (((UIScreen.MainScreen.Bounds.Height - screenHeight) / 2f) +
            //    (!isPad ? 10f : 0f));


            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                0 : (float)View.SafeAreaInsets.Left;

            #endregion

            #region Curved View
            bgOverlayViewVR2.Frame = btnBGCloseVR2.Frame = UIScreen.MainScreen.Bounds;

            //if (completeUpdate && UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            bgOverlayViewVR2.BackgroundColor = new UIColor(0f, .75f);
            btnBGCloseVR2.BackgroundColor = new UIColor(0f, 0f);
            curvedViewVR2.Frame = new CGRect(xOffset, yOffset, screenWidth, screenHeight);
            curvedViewVR2.Layer.CornerRadius = isPad ? 5f * ScaleFactor : 0f;
            #endregion

            #region Bottom View
            detailsViewVR2.Frame = new CGRect(0f, curvedViewVR2.Frame.Height - (50f * ScaleFactor) - (float)View.SafeAreaInsets.Bottom,
                    screenWidth, (50f * ScaleFactor));
            detailsViewVR2.BorderColor = UIColor.Clear;
            detailsViewVR2.BorderWidth = new UIEdgeInsets(0f, 0f, 0f, 0f);


            detailsButton2.Frame = new CGRect(margin, margin,
                100f * ScaleFactor, detailsViewVR2.Frame.Height - doubleMargin);

            //nfloat detailsBW = 70f * ScaleFactor;
            
            nfloat buttonSlice = (detailsViewVR2.Frame.Width - (30f * ScaleFactor)) / 3f;

            confirmButton2.Frame = new CGRect((buttonSlice * 2f) + (20f * ScaleFactor), (detailsViewVR2.Frame.Height * 0.15f),
                buttonSlice, detailsViewVR2.Frame.Height * 0.7f);
            /*new CGRect(detailsViewVR2.Frame.Width - detailsBW - margin, margin,
            detailsBW, confirmButton2.Frame.Height);*/
            confirmButton2.ContentMode = UIViewContentMode.ScaleAspectFit;
            confirmButton2.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

            confirmButton2.Layer.BorderColor = new CGColor(0f, 1f);
            confirmButton2.Layer.BorderWidth = 0f;

            detailsButton2.Layer.BorderColor = new CGColor(0f, 1f);
            detailsButton2.Layer.BorderWidth = 0f;
            #endregion

            #region Selected Airline View

            var statusBarH = (!isPad && !isLandscape) ? 20f : 0f;

            if (isLandscape)
                statusBarH += 10f;
            else if (View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            selectedAirlineViewVR2.Frame = new CGRect(0f, statusBarH,
                screenWidth, 44f * ScaleFactor);//OriginFactor ???

            lblFlightLabelVR2.Frame = new CGRect(margin + leftMargin, 0,
                55f * ScaleFactor, selectedAirlineViewVR2.Frame.Height);

            lblFlightVR2.Frame = new CGRect(lblFlightLabelVR2.Frame.Right, 0,
                94f * ScaleFactor, selectedAirlineViewVR2.Frame.Height);

            imgSelectedAirlineViewVR2.Frame = new CGRect(selectedAirlineViewVR2.Frame.Right - (90f * ScaleFactor) - margin, selectedAirlineViewVR2.Frame.Top,
                90f * ScaleFactor, 90f * ScaleFactor);
            imgSelectedAirlineViewVR2.Layer.CornerRadius = 5f * ScaleFactor;
            imgSelectedAirlineViewVR2.Layer.BorderWidth = 3f * ScaleFactor;
            imgSelectedAirlineVR2.Frame = new CGRect(5f * ScaleFactor, 5f * ScaleFactor,
                imgSelectedAirlineViewVR2.Frame.Width - (10f * ScaleFactor), imgSelectedAirlineViewVR2.Frame.Height - (10f * ScaleFactor));

            lblPNRVR2.Frame = new CGRect(selectedAirlineViewVR2.Frame.Width - ((98f + 76f) * ScaleFactor), 0,
                76f * ScaleFactor, selectedAirlineViewVR2.Frame.Height);

            lblPNRLabelVR2.Frame = new CGRect(lblPNRVR2.Frame.Left - (36f * ScaleFactor), 0,
                36f * ScaleFactor, selectedAirlineViewVR2.Frame.Height);

            #endregion

            #region Passenger View
            passengerViewVR2.Frame = new CGRect(0, 0, screenWidth, 190f * ScaleFactor); //186

            lblBoardingDateVR2.Frame = new CGRect(margin + leftMargin, 40f * ScaleFactor,
                90f * ScaleFactor, 15f * ScaleFactor);
            lblFullNameVR2.Frame = new CGRect((14f * ScaleFactor) + leftMargin, 53f * ScaleFactor,
                254f * ScaleFactor, 24f * ScaleFactor);


            lblFFNLabelVR2.Frame = new CGRect(lblBoardingDateVR2.Frame.Left, 100f * ScaleFactor,
                45f * ScaleFactor, 15f * ScaleFactor);
            lblFFNVR2.Frame = new CGRect(lblFullNameVR2.Frame.Left, 119f * ScaleFactor,
                254f * ScaleFactor, 18f * ScaleFactor);

            var cardsLeft = passengerViewVR2.Frame.Width - (90f * ScaleFactor) - margin;

            imgSelectedCardVR2.Frame = new CGRect(cardsLeft, lblFullNameVR2.Frame.Top, 90f * ScaleFactor, 50f * ScaleFactor);
            lblCardNameVR2.Frame = new CGRect(imgSelectedCardVR2.Frame.Left, imgSelectedCardVR2.Frame.Bottom + (1f * ScaleFactor),
    imgSelectedCardVR2.Frame.Width, doubleMargin);

            imgSelectedCardOtherVR2.Frame = new CGRect(cardsLeft, lblFFNVR2.Frame.Top + lblCardNameVR2.Frame.Height/3, imgSelectedCardVR2.Frame.Width, imgSelectedCardVR2.Frame.Height);

            lblCardOtherNameVR2.Frame = new CGRect(imgSelectedCardOtherVR2.Frame.Left, imgSelectedCardOtherVR2.Frame.Bottom + (1f * ScaleFactor),
                imgSelectedCardOtherVR2.Frame.Width, doubleMargin);
            #endregion

            #region Flight View
            flightViewVR2.Frame = new CGRect(0, passengerViewVR2.Frame.Bottom, screenWidth, 105f * ScaleFactor);

            imgFromVR2.Frame = new CGRect(margin + leftMargin, margin,
                40f * ScaleFactor, 40f * ScaleFactor);
            imgToVR2.Frame = new CGRect(imgFromVR2.Frame.Left, 60f * ScaleFactor,
                imgFromVR2.Frame.Width, imgFromVR2.Frame.Height);

            lblFromVR2.Frame = new CGRect(imgFromVR2.Frame.Right + margin, margin,
                45f * ScaleFactor, 15f * ScaleFactor);
            lblFromFullAirportNameVR2.Frame = new CGRect(lblFromVR2.Frame.Left, lblFromVR2.Frame.Bottom,
                flightViewVR2.Frame.Width - imgToVR2.Frame.Right - doubleMargin, 22f * ScaleFactor);

            lblToVR2.Frame = new CGRect(lblFromVR2.Frame.Left, imgToVR2.Frame.Top,
                45f * ScaleFactor, 15f * ScaleFactor);
            lblToFullAirportNameVR2.Frame = new CGRect(lblToVR2.Frame.Left, lblToVR2.Frame.Bottom,
                lblFromFullAirportNameVR2.Frame.Width, 22f * ScaleFactor);
            #endregion

            #region Flight Stats View
            flightStatsViewVR2.Frame = new CGRect(0, flightViewVR2.Frame.Bottom, screenWidth, 150f * ScaleFactor);

            TiledView.ScaleFactor = ScaleFactor;

            tiledViewTop.Frame = new CGRect(50f * ScaleFactor, 52f * ScaleFactor, flightStatsViewVR2.Frame.Width - (100f * ScaleFactor), 45f * ScaleFactor);

            var bottomBoxWidth = (tiledViewTop.Frame.Width - doubleMargin) / 3f;
            tiledViewBottomLeft.Frame = new CGRect(50f * ScaleFactor, tiledViewTop.Frame.Bottom + margin, bottomBoxWidth, 37f * ScaleFactor);
            tiledViewBottomCenter.Frame = new CGRect(tiledViewBottomLeft.Frame.Right + margin, tiledViewTop.Frame.Bottom + margin, bottomBoxWidth, 37f * ScaleFactor);
            tiledViewBottomRight.Frame = new CGRect(tiledViewBottomCenter.Frame.Right + margin, tiledViewTop.Frame.Bottom + margin, bottomBoxWidth, 37f * ScaleFactor);


            lblBoardingTimeLabelVR2.Frame = new CGRect(8f * ScaleFactor, 6f * ScaleFactor, 100f * ScaleFactor, 11f * ScaleFactor);
            lblBoardingTimeVR2.Frame = new CGRect(8f * ScaleFactor, 21f * ScaleFactor, 257f * ScaleFactor, 18f * ScaleFactor);


            lblTerminalLabelVR2.Frame = new CGRect(5f * ScaleFactor, 3f * ScaleFactor, 65f * ScaleFactor, 11f * ScaleFactor);
            lblTerminalVR2.Frame = new CGRect(5f * ScaleFactor, 16f * ScaleFactor, 65f * ScaleFactor, 17f * ScaleFactor);

            lblGateLabelVR2.Frame = new CGRect(5f * ScaleFactor, 3f * ScaleFactor, 65f * ScaleFactor, 11f * ScaleFactor);
            lblGateVR2.Frame = new CGRect(5f * ScaleFactor, 16f * ScaleFactor, 65f * ScaleFactor, 17f * ScaleFactor);

            lblSeatLabelVR2.Frame = new CGRect(5f * ScaleFactor, 3f * ScaleFactor, 65f * ScaleFactor, 11f * ScaleFactor);
            lblSeatVR2.Frame = new CGRect(5f * ScaleFactor, 16f * ScaleFactor, 65f * ScaleFactor, 17f * ScaleFactor);

            imgWeatherVR2.Frame = new CGRect(50f * ScaleFactor, -15f * ScaleFactor, 50f * ScaleFactor, 70f * ScaleFactor);
            lblWeatherVR2.Frame = new CGRect(96f * ScaleFactor, 19f * ScaleFactor, 35f * ScaleFactor, 13f * ScaleFactor);

            imgTimeVR2.Frame = new CGRect(tiledViewTop.Frame.Left + (tiledViewTop.Frame.Width / 2f), 0f,
                32f * ScaleFactor, 52f * ScaleFactor);
            lblTimeVR2.Frame = new CGRect(imgTimeVR2.Frame.Right + (4f * ScaleFactor), 19f * ScaleFactor, 60f * ScaleFactor, 13f * ScaleFactor);

            #endregion

            #region Services View
            servicesViewVR2.Frame = new CGRect(0, flightStatsViewVR2.Frame.Bottom, screenWidth, 110f * ScaleFactor);
            ServicesViewSource.ScaleFactor = ScaleFactor;
            servicesCollectionViewVR2.Frame = new CGRect(40f * ScaleFactor, 5f * ScaleFactor, 295f * ScaleFactor, 100f * ScaleFactor);

            var serviceCellMargins = 20f * ScaleFactor;
            servicesCollectionViewVR2.CollectionViewLayout = new UICollectionViewFlowLayout()
            {
                ItemSize = new CGSize(60f * ScaleFactor, 80f * ScaleFactor),
                HeaderReferenceSize = new CGSize(0, 0),
                MinimumInteritemSpacing = serviceCellMargins,
                MinimumLineSpacing = margin,
                SectionInset = new UIEdgeInsets(margin, serviceCellMargins, margin, serviceCellMargins),
                ScrollDirection = UICollectionViewScrollDirection.Horizontal
            };
            servicesCollectionViewVR2.ReloadData();
            #endregion

            #region Scroll View
            scrollViewVR2.Frame = new CGRect(selectedAirlineViewVR2.Frame.Left, selectedAirlineViewVR2.Frame.Bottom,
                screenWidth, detailsViewVR2.Frame.Top - selectedAirlineViewVR2.Frame.Bottom);// screenHeight /*- statusBarH*/ - detailsViewVR2.Frame.Height);

            scrollViewVR2.ContentSize = new CGSize(scrollViewVR2.Frame.Width, servicesViewVR2.Frame.Bottom /*scrollViewVR2.Frame.Height*/);//TODO: Test and check the value
            scrollViewVR2.ShowsVerticalScrollIndicator = true;
            scrollViewVR2.ShowsHorizontalScrollIndicator = false;
            #endregion

            #region Reentry View

            if (MembershipProvider.Current.SelectedPassenger.IsReEntry)
            {
                reentryView.Frame = new CGRect(5f, detailsViewVR2.Frame.Top + (7f * ScaleFactor), screenWidth - confirmButton2.Frame.Width - (20f * ScaleFactor), confirmButton2.Frame.Height);

                if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.NotificationColorCode))
                {
                    reentryView.BackgroundColor = UIColor.Black.FromHexString(MembershipProvider.Current.SelectedPassenger.NotificationColorCode);
                }

                reentryView.Hidden = false;
                lblreentry.Text = AppDelegate.TextProvider.GetText(2182).ToUpper();
                lblreentry.SizeToFit();
                lblreentry.CenterFrameInParent();
            }
            else if (MembershipProvider.Current.SelectedPassenger.IsLoungeHopping)
            {
                reentryView.Frame = new CGRect(5f, detailsViewVR2.Frame.Top + (7f * ScaleFactor), screenWidth - confirmButton2.Frame.Width - (20f * ScaleFactor), confirmButton2.Frame.Height);

                if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.NotificationColorCode))
                {
                    reentryView.BackgroundColor = UIColor.Black.FromHexString(MembershipProvider.Current.SelectedPassenger.NotificationColorCode);
                }

                reentryView.Hidden = false;
                lblreentry.Text = AppDelegate.TextProvider.GetText(2183).ToUpper();
                lblreentry.SizeToFit();
                lblreentry.CenterFrameInParent();
            }
            else
            {
                reentryView.Hidden = true;
                lblreentry.Text = null;
            }

            lblreentry.TextColor = UIColor.White; 

            #endregion


            #region Lined View
            linedView.Frame = new CGRect(passengerViewVR2.Frame.Left + margin, passengerViewVR2.Frame.Y,
                    scrollViewVR2.Frame.Width - doubleMargin, scrollViewVR2.ContentSize.Height);

            if (!IsGuestMode)
                linedView.LinesY = new nfloat[3] { /*passengerViewVR2.Frame.Y,*/ flightViewVR2.Frame.Y,
                    flightStatsViewVR2.Frame.Y, servicesViewVR2.Frame.Y/*, servicesViewVR2.Frame.Bottom*/};
            else
                linedView.LinesY = new nfloat[2] { /*passengerViewVR2.Frame.Y,*/ flightViewVR2.Frame.Y,
                    flightStatsViewVR2.Frame.Y/*, servicesViewVR2.Frame.Bottom*/};
            #endregion

            SetFonts();
        }

        public void ConfirmClicked(object sender, EventArgs e)
        {
            servicesCollectionViewVR2.Source = null;

            if (!IsGuestMode)
                MembershipProvider.Current.SelectedPassenger = null;
            else AppDelegate.GuestServicesView.RefreshCategories(MembershipProvider.Current.SelectedPassenger.PassengerServices, true);

            MembershipProvider.Current.SelectedGuest = null;

            //this.DismissModalViewController(true);
            this.DismissViewController(true, null);

            if (AppDelegate.ContinueScanning && !IsGuestMode)
                AppDelegate.AirlinesView.Scan();
            else IsGuestMode = false;
        }

        private void DetailsClicked(object sender, EventArgs e)
        {
            AIMSMessage(AppDelegate.TextProvider.GetText(2530), BarcodeString);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            initLinedView();


            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;


            confirmButton2.TouchUpInside += ConfirmClicked;
            btnBGCloseVR2.TouchUpInside += ConfirmClicked;
            detailsButton2.TouchUpInside += DetailsClicked;

            servicesCollectionViewVR2.ShowsVerticalScrollIndicator = false;
            servicesCollectionViewVR2.ShowsHorizontalScrollIndicator = true;

            #region Passenger's services View Source
            ServicesViewSource = new PassengerServiceCollectionViewSource();
            ServicesViewSource.OnItemSelected += (sender, e) =>
            {
                //AppDelegate.Current.SelectedItem = e;
                //this.PresentViewController(AppDelegate.Current.SecondScreen, true, null);
                MembershipProvider.Current.SelectedPassengerService = e;
                this.PresentViewController(AppDelegate.GuestServicesView, true, null);
                //AIMSMessage("Service Selected", e.Name);
                //if(AppDelegate.Current.GuestServicesView == null)
                //    AppDelegate.Current.GuestServicesView = new GuestsViewController()
            };
            #endregion
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            LoadTexts();
            UpdateLayout();
            isInitialized = true;
            RefreshPassenger();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            UpdateLayout();
            //RefreshPassenger();
        }
    }
}