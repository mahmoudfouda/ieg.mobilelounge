using Foundation;
using System;
using UIKit;
using CoreGraphics;
using AIMS;
using AIMS_IOS.Utility;
using AIMS.Models;
using System.Collections.Generic;
using System.Drawing;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public partial class ValidationResultViewController : AIMSViewController, IPassengerRefresher
    {
        #region UIFields
        public nfloat ScaleFactor { get; private set; }

        string delayedText, onTimeText;
        private bool isInitialized = false;

        private string BarcodeString { get; set; }

        public LinedView linedView;

        public UIButton ConfirmButton
        {
            get { return confirmButton; }
        }
        #endregion

        private PassengerServiceCollectionViewSource ServicesViewSource { get; set; }

        public bool IsGuestMode { get; set; } = false;

        public ValidationResultViewController(IntPtr handle) : base(handle)
        {
        }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() =>
            {
                onTimeText = AppDelegate.TextProvider.GetText(2021);
                delayedText = AppDelegate.TextProvider.GetText(2020);
                welcomeLabel.Text = AppDelegate.TextProvider.GetText(2006);//Welcome
                cardLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2013));//Card: 
                classTitleLabel.Text = AppDelegate.TextProvider.GetText(2014);//Class
                flightInfoTitleLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2017));//Flight Info: 
                departureTitleLabel.Text = AppDelegate.TextProvider.GetText(2018);// string.Format("{0}: ",AppDelegate.TextProvider.GetText(2018));//Departure:  
                arrivalTitleLabel.Text = AppDelegate.TextProvider.GetText(2019);// string.Format("{0}: ", AppDelegate.TextProvider.GetText(2019));//Arrival: 
                loyaltyTitleLabel.Text = AppDelegate.TextProvider.GetText(2027);//FFN
                                                                                //lblServicesLabel.Text = AppDelegate.TextProvider.GetText(2043);//"Services";//Next phase
                detailsButton.SetTitle(AppDelegate.TextProvider.GetText(2045), UIControlState.Normal);//"Details"
                airlineTitleLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2023));//"Airline: "
            });
        }

        public void RefreshPassenger()
        {
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null || !isInitialized)
                return;

            InvokeOnMainThread(() =>
            {
                if (!IsGuestMode)
                {
                    #region Loading passenger's first card
                    cardImage1.Image = null;
                    if (MembershipProvider.Current.SelectedCard != null)
                    {
                        if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                        {
                            cardImage1.Image = MembershipProvider.Current.SelectedCard.CardPictureBytes.ToImage();
                        }
                        else
                        {
                            cardImage1.Image = UIImage.FromFile("Error/not_available_pic_off");
                            ImageAdapter.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (imageBytes) =>
                            {
                                var image = imageBytes.ToImage();
                                InvokeOnMainThread(() =>
                                {
                                    cardImage1.Image = image;
                                });
                            });
                        }
                    }
                    #endregion

                    #region Loading Passenger's second card
                    cardImage2.Image = null;
                    if (MembershipProvider.Current.SelectedPassenger.OtherCardID != 0)
                    {
                        var otherCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedPassenger.OtherCardID);
                        if (otherCard != null)
                        {
                            if (otherCard.CardPictureBytes != null && otherCard.CardPictureBytes.Length > 0)
                            {
                                cardImage2.Image = otherCard.CardPictureBytes.ToImage();
                                cardImage2.Hidden = false;
                            }
                            else
                            {
                                cardImage2.Image = UIImage.FromFile("Error/not_available_pic_off");
                                ImageAdapter.LoadImage(otherCard.ImageHandle, (imageBytes) =>
                                {
                                    var image = imageBytes.ToImage();
                                    InvokeOnMainThread(() =>
                                    {
                                        cardImage2.Image = image;
                                        cardImage2.Hidden = false;
                                    });
                                });
                            }
                        }
                        else cardImage2.Hidden = true;
                    }
                    else cardImage2.Hidden = true;
                    #endregion

                    #region Filling up the lables
                    nameLabel.Text = MembershipProvider.Current.SelectedPassenger.FullName;
                    loyaltyLabel.Text = MembershipProvider.Current.SelectedPassenger.FFN;
                    classLabel.Text = MembershipProvider.Current.SelectedPassenger.TrackingClassOfService;
                    flightInfoLabel.Text = string.Format("{0} {1}", MembershipProvider.Current.SelectedPassenger.FlightCarrier, MembershipProvider.Current.SelectedPassenger.FlightNumber);
                    airlineLabel.Text = MembershipProvider.Current.SelectedAirline.Name;
                    departureLabel.Text = MembershipProvider.Current.SelectedPassenger.FromAirport;
                    arrivalLabel.Text = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedPassenger.ToAirport) ? "..." : MembershipProvider.Current.SelectedPassenger.ToAirport;
                    // MembershipProvider.Current.SelectedPassenger.ToAirport;//TODO: Get ToAirport
                    //lblDepartureGate.Text = "N/A";//TODO: get the gate number
                    //lblDepartureGateComment.Text = "";//TODO: get the gate comments 
                    #endregion

                    #region Loading the Barcode if available
                    if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedPassenger.BarcodeString))
                        BarcodeString = MembershipProvider.Current.SelectedPassenger.BarcodeString;
                    else detailsButton.Hidden = true;
                    #endregion


                    #region Loading the returned Passenger Services (TODO: extract it to a method - as we need to refresh the quantities later)
                    if (MembershipProvider.Current.SelectedPassenger.PassengerServices == null)//TODO: move this into Client Core Library inside (MembershipProvider.Current.SelectedPassenger.Set{} very last line)
                        MembershipProvider.Current.SelectedPassenger.PassengerServices = new Dictionary<PassengerService, List<Passenger>>();
                    
                    ServicesViewSource.Rows = new List<PassengerService>();
                    foreach (var guestCatKeyVal in MembershipProvider.Current.SelectedPassenger.PassengerServices)
                    {
                        var passengerService = guestCatKeyVal.Key;
                        passengerService.Quantity = guestCatKeyVal.Value.Count;

                        ServicesViewSource.Rows.Add(passengerService);
                    }
                    servicesCollectionView.Source = ServicesViewSource;
                    servicesCollectionView.ReloadData();

                    #endregion
                }
                else if (MembershipProvider.Current.SelectedGuest != null)
                {
                    #region Loading guest's only card
                    cardImage1.Image = null;
                    var guestCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedGuest.CardID);
                    if (guestCard != null)
                    {
                        if (guestCard.CardPictureBytes != null && guestCard.CardPictureBytes.Length > 0)
                        {
                            cardImage1.Image = guestCard.CardPictureBytes.ToImage();
                        }
                        else
                        {
                            cardImage1.Image = UIImage.FromFile("Error/not_available_pic_off");
                            ImageAdapter.LoadImage(guestCard.ImageHandle, (imageBytes) =>
                            {
                                var image = imageBytes.ToImage();
                                InvokeOnMainThread(() =>
                                {
                                    cardImage1.Image = image;
                                });
                            });
                        }
                    }
                    #endregion

                    #region Filling up the lables
                    nameLabel.Text = MembershipProvider.Current.SelectedGuest.FullName;
                    loyaltyLabel.Text = MembershipProvider.Current.SelectedGuest.FFN;
                    classLabel.Text = MembershipProvider.Current.SelectedGuest.TrackingClassOfService;
                    flightInfoLabel.Text = string.Format("{0} {1}",
                        MembershipProvider.Current.SelectedGuest.FlightCarrier,
                        MembershipProvider.Current.SelectedGuest.FlightNumber);
                    var guestAirline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.SelectedGuest.AirlineId);
                    if (guestAirline != null)
                        airlineLabel.Text = guestAirline.Name;
                    departureLabel.Text = MembershipProvider.Current.SelectedGuest.FromAirport;
                    arrivalLabel.Text = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedGuest.ToAirport) ? "..." : MembershipProvider.Current.SelectedGuest.ToAirport;
                    // MembershipProvider.Current.SelectedPassenger.ToAirport;//TODO: Get ToAirport
                    //lblDepartureGate.Text = "N/A";//TODO: get the gate number
                    //lblDepartureGateComment.Text = "";//TODO: get the gate comments 
                    #endregion

                    #region Loading the Barcode if available
                    if (!string.IsNullOrEmpty(MembershipProvider.Current.SelectedGuest.BarcodeString))
                        BarcodeString = MembershipProvider.Current.SelectedGuest.BarcodeString;
                    else detailsButton.Hidden = true; 
                    #endregion
                }

                if (MembershipProvider.Current.SelectedPassenger.IsGuest)
                    servicesCollectionView.Hidden = true;
                else servicesCollectionView.Hidden = false;
            });
        }

        public void RefreshServices()
        {
            servicesCollectionView.ReloadData();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            initLinedView();


            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;


            confirmButton.TouchUpInside += confirmButtonTouchUpInside;
            detailsButton.TouchUpInside += DetailsButton_TouchUpInside;


            //nameLabel.AdjustsFontSizeToFitWidth = true;
            //cardLabel.AdjustsFontSizeToFitWidth = true;

            //loyaltyTitleLabel.AdjustsFontSizeToFitWidth = true;
            //classTitleLabel.AdjustsFontSizeToFitWidth = true;

            //loyaltyLabel.AdjustsFontSizeToFitWidth = true;
            //classLabel.AdjustsFontSizeToFitWidth = true;

            //flightInfoTitleLabel.AdjustsFontSizeToFitWidth = true;
            //flightInfoLabel.AdjustsFontSizeToFitWidth = true;

            //airlineTitleLabel.AdjustsFontSizeToFitWidth = true;
            //airlineLabel.AdjustsFontSizeToFitWidth = true;      

            cardImage1.BackgroundColor = UIColor.Clear;

            cardImage2.BackgroundColor = UIColor.Clear;

            imgFlightPlane.BackgroundColor = UIColor.Clear;

            servicesCollectionView.ShowsVerticalScrollIndicator = false;
            servicesCollectionView.ShowsHorizontalScrollIndicator = true;

            #region Passenger's services View Source
            ServicesViewSource = new PassengerServiceCollectionViewSource();
            ServicesViewSource.OnItemSelected += (sender, e) =>
            {
                //AppDelegate.Current.SelectedItem = e;
                //this.PresentViewController(AppDelegate.Current.SecondScreen, true, null);
                MembershipProvider.Current.SelectedPassengerService = e;
                this.PresentViewController(AppDelegate.GuestServicesView, true, null);
                //AIMSMessage("Service Selected", e.Name);
                //if(AppDelegate.Current.GuestServicesView == null)
                //    AppDelegate.Current.GuestServicesView = new GuestsViewController()
            };
            #endregion
        }

        public void initLinedView()
        {
            linedView = new LinedView();

            this.scrollView.AddSubview(linedView);
        }

        public void setFonts()
        {
            //if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            //{
            var fontScaleFactor = ScaleFactor;

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                fontScaleFactor = (float)ScaleFactor * ((float)2 / 3);

            nfloat largerTexts = 30f * fontScaleFactor, largeTexts = 26f * fontScaleFactor, mediumTexts = 18f * fontScaleFactor, smallTexts = 16f * fontScaleFactor;
            string italicFont = "HelveticaNeue-Italic", boldFont = "Helvetica-Bold", regularFont = "Helvetica Neue";

            //top view
            welcomeLabel.Font = UIFont.FromName(regularFont, largerTexts);
            nameLabel.Font = UIFont.FromName(boldFont, largeTexts);

            //card view
            cardLabel.Font = UIFont.FromName(italicFont, smallTexts);

            loyaltyTitleLabel.Font = UIFont.FromName(italicFont, smallTexts);
            loyaltyLabel.Font = UIFont.FromName(boldFont, mediumTexts);

            classTitleLabel.Font = UIFont.FromName(italicFont, smallTexts);
            classLabel.Font = UIFont.FromName(boldFont, mediumTexts);

            flightInfoTitleLabel.Font = UIFont.FromName(italicFont, smallTexts);
            flightInfoLabel.Font = UIFont.FromName(boldFont, mediumTexts);

            airlineTitleLabel.Font = UIFont.FromName(italicFont, smallTexts);
            airlineLabel.Font = UIFont.FromName(boldFont, mediumTexts);

            //departure view
            departureTitleLabel.Font = UIFont.FromName(italicFont, smallTexts);
            departureLabel.Font = UIFont.FromName(boldFont, mediumTexts);

            //arrival view
            arrivalTitleLabel.Font = UIFont.FromName(italicFont, smallTexts);
            arrivalLabel.Font = UIFont.FromName(boldFont, mediumTexts);
            //}
            //else
            //{
            //    //it is already set in story board
            //}
        }

        private void DetailsButton_TouchUpInside(object sender, EventArgs e)
        {
            AIMSMessage(AppDelegate.TextProvider.GetText(2530), BarcodeString);
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            updateLayout();
        }

        private void updateLayout()
        {
            linedView.SetNeedsDisplay();

            nfloat width = UIScreen.MainScreen.Bounds.Width;
            nfloat height = UIScreen.MainScreen.Bounds.Height;// - TabBarHeight;

            nfloat statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            nfloat screenHeight = (UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width);

            ScaleFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                //scaleFactor = (screenHeight - statusBarH - detailsView.Frame.Height) / 667f;
                ScaleFactor = screenHeight / 667f;
            }

            nfloat margin = 5f * ScaleFactor;
            nfloat doubleMargin = margin * 2f;

            nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.66f;

            nfloat childXOffset = (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f);

            //------------------------------details view----------------------------
            detailsView.Frame = new CGRect(0f, height - (50f * ScaleFactor),
                width, (50f * ScaleFactor));
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                detailsView.Frame = new CGRect(0, height - (50f * ScaleFactor),
                                               width, (50f * ScaleFactor));
            }

            confirmButton.Frame = new CGRect(doubleMargin, doubleMargin,
                100f * ScaleFactor, detailsView.Frame.Height - (doubleMargin * 2));

            nfloat detailsBW = 70f * ScaleFactor;
            detailsButton.Frame = new CGRect(width - detailsBW - doubleMargin,
                doubleMargin, detailsBW, confirmButton.Frame.Height);

            confirmButton.Layer.BorderColor = new CGColor(0f, 1f);
            confirmButton.Layer.BorderWidth = 1f;

            detailsButton.Layer.BorderColor = new CGColor(0f, 1f);
            detailsButton.Layer.BorderWidth = 1f;

            //-----------------scroll view---------------------------
            scrollView.Frame = new CGRect(0f, statusBarH, width,
                height - statusBarH - detailsView.Frame.Height);

            //this.scrollView.ContentSize = new CGSize(width, 450f * ScaleFactor);//TODO: what is that
            this.scrollView.ContentSize = new CGSize(width, 530f * ScaleFactor);

            //------------title view------------------
            titleView.Frame = new CGRect(0f, 0f, width, 90f * ScaleFactor);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                titleView.Frame = new CGRect(childXOffset, 0f,
                    childWidth, 90f * ScaleFactor);
            }

            welcomeLabel.Frame = new CGRect(margin, margin,
                titleView.Frame.Width - doubleMargin, 36f * ScaleFactor);
            nameLabel.Frame = new CGRect(welcomeLabel.Frame.X, welcomeLabel.Frame.Bottom + doubleMargin,
                welcomeLabel.Frame.Width, 36f * ScaleFactor);

            //---------------card view--------------------
            cardView.Frame = new CGRect(titleView.Frame.X, titleView.Frame.Bottom, titleView.Frame.Width, 120f * ScaleFactor);

            nfloat cardHeight = 50f * ScaleFactor;//cardView.Frame.Height * 0.3846f - margin;
            nfloat cardWidth = 90f * ScaleFactor;//cardHeight * 1.8f;
            nfloat labelWidth = cardView.Frame.Width - (doubleMargin * 2f + cardWidth * 2f + margin);

            cardImage2.Frame = new CGRect(cardView.Frame.Width - cardWidth - doubleMargin, 7f * ScaleFactor,
                cardWidth, cardHeight);

            cardImage1.Frame = new CGRect(cardImage2.Frame.X - margin - cardImage2.Frame.Width, cardImage2.Frame.Y,
                cardWidth, cardHeight);


            cardLabel.Frame = new CGRect(margin, cardImage1.Frame.Y,
                labelWidth, cardImage1.Frame.Height);

            classTitleLabel.Frame = new CGRect(cardImage1.Frame.X,
                cardLabel.Frame.Bottom + doubleMargin,
                cardView.Frame.Width - margin * 2f - cardImage1.Frame.X, 25f * ScaleFactor);

            loyaltyTitleLabel.Frame = new CGRect(margin, classTitleLabel.Frame.Y,
                cardView.Frame.Width - margin * 2f - classTitleLabel.Frame.X, classTitleLabel.Frame.Height);

            loyaltyLabel.Frame = new CGRect(margin, loyaltyTitleLabel.Frame.Bottom + margin,
                loyaltyTitleLabel.Frame.Width, 17f * ScaleFactor);

            classLabel.Frame = new CGRect(classTitleLabel.Frame.X, loyaltyLabel.Frame.Y,
                classTitleLabel.Frame.Width, loyaltyLabel.Frame.Height);

            //--------------flight info view--------------------------
            flightInfoView.Frame = new CGRect(cardView.Frame.X, cardView.Frame.Bottom,
                titleView.Frame.Width, 60f * ScaleFactor);

            flightInfoLabel.Frame = new CGRect(classLabel.Frame.X, doubleMargin,
                flightInfoView.Frame.Width - classLabel.Frame.X - margin, 20f * ScaleFactor);

            flightInfoTitleLabel.Frame = new CGRect(margin, doubleMargin,
                flightInfoView.Frame.Width - flightInfoLabel.Frame.X - doubleMargin,
                flightInfoLabel.Frame.Height);

            airlineTitleLabel.Frame = new CGRect(flightInfoTitleLabel.Frame.X,
                flightInfoTitleLabel.Frame.Bottom + margin, labelWidth,
                flightInfoTitleLabel.Frame.Height);

            airlineLabel.Frame = new CGRect(flightInfoLabel.Frame.X, airlineTitleLabel.Frame.Y,
                flightInfoView.Frame.Width - classLabel.Frame.X, airlineTitleLabel.Frame.Height);

            //---------------departure----------------------------
            departureView.Frame = new CGRect(flightInfoView.Frame.X, flightInfoView.Frame.Bottom,
                titleView.Frame.Width, 130f * ScaleFactor);

            nfloat imageSize = 70f * ScaleFactor;


            departureTitleLabel.Frame = new CGRect(13f * ScaleFactor, 10f * ScaleFactor,
                75f * ScaleFactor, 18f * ScaleFactor);

            departureImage.Frame = new CGRect(doubleMargin + margin, departureTitleLabel.Frame.Bottom, imageSize, imageSize);

            departureLabel.Frame = new CGRect(doubleMargin, departureImage.Frame.Bottom, 80f * ScaleFactor, 22f * ScaleFactor);


            arrivalTitleLabel.Frame = new CGRect(departureView.Frame.Width - (75f * ScaleFactor), departureTitleLabel.Frame.Y,
                50f * ScaleFactor, departureTitleLabel.Frame.Height);

            arrivalImage.Frame = new CGRect(departureView.Frame.Width - (doubleMargin + margin + imageSize), departureImage.Frame.Top,
                imageSize, imageSize);

            arrivalLabel.Frame = new CGRect(departureView.Frame.Width - (90f * ScaleFactor), arrivalImage.Frame.Bottom,
                departureLabel.Frame.Width, departureLabel.Frame.Height);

            departureLabel.TextAlignment = UITextAlignment.Center;
            arrivalLabel.TextAlignment = UITextAlignment.Center;


            var prgX = departureImage.Frame.Right + doubleMargin + margin;
            var prgY = departureImage.Frame.Top + (imageSize / 2f) - 1f;
            var prgWidth = departureView.Frame.Width - (departureImage.Frame.Right + departureImage.Frame.X + arrivalImage.Frame.Width + (2f * (doubleMargin + margin)));

            prgBarFlight.Frame = new CGRect(prgX, prgY, prgWidth, 2f);

            prgBarFlight.Progress = 0.5f;

            imgFlightPlane.Frame = new CGRect((departureView.Frame.Width / 2f) - (25f * ScaleFactor), prgBarFlight.Frame.Y - (25f * ScaleFactor) + 1f,
                (50f * ScaleFactor), (50f * ScaleFactor));

            if (!IsGuestMode)
            {
                ServicesViewSource.ScaleFactor = ScaleFactor;
                servicesCollectionView.Frame = new CGRect(titleView.Frame.X, departureView.Frame.Bottom,
                    titleView.Frame.Width, 130f * ScaleFactor);

                var serviceCellMargins = 25f * ScaleFactor;
                servicesCollectionView.CollectionViewLayout = new UICollectionViewFlowLayout()
                {
                    ItemSize = new CGSize(60f * ScaleFactor, 80f * ScaleFactor),
                    HeaderReferenceSize = new CGSize(0, 0),
                    MinimumInteritemSpacing = serviceCellMargins,
                    MinimumLineSpacing = serviceCellMargins,
                    SectionInset = new UIEdgeInsets(serviceCellMargins, serviceCellMargins, serviceCellMargins, serviceCellMargins),
                    ScrollDirection = UICollectionViewScrollDirection.Horizontal
                };

                linedView.Frame = new CGRect(0f, titleView.Frame.Y,
                    UIScreen.MainScreen.Bounds.Width, servicesCollectionView.Frame.Bottom);

                linedView.LinesY = new nfloat[5] { cardView.Frame.Y, flightInfoView.Frame.Y,
                departureView.Frame.Y, servicesCollectionView.Frame.Y, servicesCollectionView.Frame.Bottom};
                //TODO: remove debug mode and make the servicesCollectionView visible and working
//#if !DEBUG
//                servicesCollectionView.Hidden = true;
//#endif
            }
            else
            {
                servicesCollectionView.Hidden = true;
                linedView.Frame = new CGRect(0f, titleView.Frame.Y,
                    UIScreen.MainScreen.Bounds.Width, departureView.Frame.Bottom);
                linedView.LinesY = new nfloat[4] {cardView.Frame.Y, flightInfoView.Frame.Y,
                departureView.Frame.Y, departureView.Frame.Bottom};
            }

            setFonts();
        }

        public void confirmButtonTouchUpInside(object sender, EventArgs e)
        {
            servicesCollectionView.Source = null;

            if (!IsGuestMode)
                MembershipProvider.Current.SelectedPassenger = null;
            else AppDelegate.GuestServicesView.RefreshCategories(MembershipProvider.Current.SelectedPassenger.PassengerServices);

            //MembershipProvider.Current.SelectedGuest = null;

            //this.DismissModalViewController(true);
            this.DismissViewController(true, null);

            if (AppDelegate.ContinueScanning && !IsGuestMode)
                AppDelegate.AirlinesView.Scan();
            else IsGuestMode = false;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            LoadTexts();
            updateLayout();
            isInitialized = true;
            RefreshPassenger();
        }
    }
}