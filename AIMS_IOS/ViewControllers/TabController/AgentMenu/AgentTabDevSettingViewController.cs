using System;
using UIKit;
using CoreGraphics;
using AIMS;
using System.Text.RegularExpressions;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public partial class AgentTabDevSettingViewController : AIMSViewController
    {
        //UIImageView bottomBackground;
        LogsRepository repository;

        public nfloat yOffset = 0f;
        public UIButton topButton;

        //public LinedView linedView;

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() =>
            {
                scanButton.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                scanButton.SetTitle(AppDelegate.Current.LineaDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras
                //scanButton.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);

                this.TabBarItem.Title = AppDelegate.TextProvider.GetText(2050);

                advancedSettingsCameraTitle.Text = AppDelegate.TextProvider.GetText(2166);
                scannerLabel.Text = AppDelegate.TextProvider.GetText(2167);

                advancedSettingsLogsTitle.Text = AppDelegate.TextProvider.GetText(2168);
                uploadLogsButton.SetTitle(AppDelegate.TextProvider.GetText(2169), UIControlState.Normal);
                clearLogsButton.SetTitle(AppDelegate.TextProvider.GetText(2170), UIControlState.Normal);


                advancedSettingsPVThresholdLabel.Text = AppDelegate.TextProvider.GetText(2171);
                successScansLabel.Text = AppDelegate.TextProvider.GetText(2172);
                failedScansLabel.Text = AppDelegate.TextProvider.GetText(2173);

                advancedSettingsPVUserLabel.Text = AppDelegate.TextProvider.GetText(2174);
                sessionTimeoutLabel.Text = AppDelegate.TextProvider.GetText(2175);

                advancedSettingsPVStatisticsLabel.Text = AppDelegate.TextProvider.GetText(2011);
                refreshIntervalLabel.Text = AppDelegate.TextProvider.GetText(2176);
                maxHoursIntervalLabel.Text = AppDelegate.TextProvider.GetText(2177);

            });
        }

        public override bool HandlesKeyboardNotifications()
        {
            return true;
        }

        public AgentTabDevSettingViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            //Only do this if required
            if (HandlesKeyboardNotifications())
            {
                RegisterForKeyboardNotifications();
            }

            initLinedView();

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;
            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                scanButton.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
            };

            setFonts();

            repository = new LogsRepository();

            leftDown.TouchUpInside += downButton;
            rightDown.TouchUpInside += downButton;
            scanButton.TouchUpInside += (this.TabBarController as AgentMenuTabController).scanClicked;

            cameraSwitchButton.TouchUpInside += changeCamera;
            clearLogsButton.TouchUpInside += clearLogs;
            uploadLogsButton.TouchUpInside += sendLogs;

            var swipeLeft = new UISwipeGestureRecognizer(HandleLeftSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Left };
            View.AddGestureRecognizer(swipeLeft);

            var swipeRight = new UISwipeGestureRecognizer(HandleRightSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Right };
            View.AddGestureRecognizer(swipeRight);

            //bottomBackground = new UIImageView(UIImage.FromFile("gradiant.png"));
            //bottomView.AddSubview(bottomBackground);
            //bottomView.SendSubviewToBack(bottomBackground);

            scanButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            scanButton.Layer.BorderWidth = 1f;

            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            yOffset = AppDelegate.AirlinesView.topPanelHeight;
            topButton = new UIButton();
            topButton.TitleLabel.Text = "";
            topButton.BackgroundColor = UIColor.Clear;

            topButton.TouchUpInside += downButton;
            this.View.Add(topButton);
            //}

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                AddDoneButton(txtStatisticsRefreshInterval);
                AddDoneButton(txtStatisticsMaxHoursInterval);
                AddDoneButton(txtSuccessScanThreshold);
                AddDoneButton(txtFailedScanThreshold);
                AddDoneButton(txtUsersSessionTimeout);

                txtStatisticsRefreshInterval.ReturnKeyType = UIReturnKeyType.Done;
                txtStatisticsMaxHoursInterval.ReturnKeyType = UIReturnKeyType.Done;
                txtSuccessScanThreshold.ReturnKeyType = UIReturnKeyType.Done;
                txtFailedScanThreshold.ReturnKeyType = UIReturnKeyType.Done;
                txtUsersSessionTimeout.ReturnKeyType = UIReturnKeyType.Done;
            }


            txtSuccessScanThreshold.AllEditingEvents += thresholdTextChanged;
            txtSuccessScanThreshold.EditingDidEnd += (object sender, EventArgs e) =>
            {
                var textfiled = sender as UITextField;
                if (textfiled == null) return;
                textfiled.Text = PassengersRepository.SucceededBarcodeInterval.ToString();
            };
            txtSuccessScanThreshold.ShouldReturn += (textfiled) =>
            {
                if (!string.IsNullOrWhiteSpace(textfiled.Text))
                {
                    try
                    {
                        PassengersRepository.SucceededBarcodeInterval = int.Parse(textfiled.Text);
                    }
                    catch
                    {
                        AIMSError(AppDelegate.TextProvider.GetText(2525), AppDelegate.TextProvider.GetText(2526));
                        return false;
                    }
                    textfiled.ResignFirstResponder();
                    return true;
                }
                return false;
            };


            txtFailedScanThreshold.AllEditingEvents += thresholdTextChanged;
            txtFailedScanThreshold.EditingDidEnd += (object sender, EventArgs e) =>
            {
                var textfiled = sender as UITextField;
                if (textfiled == null) return;
                textfiled.Text = PassengersRepository.FailedBarcodeInterval.ToString();
            };
            txtFailedScanThreshold.ShouldReturn += (textfiled) =>
            {
                if (!string.IsNullOrWhiteSpace(textfiled.Text))
                {
                    try
                    {
                        PassengersRepository.FailedBarcodeInterval = int.Parse(textfiled.Text);
                    }
                    catch
                    {
                        AIMSError(AppDelegate.TextProvider.GetText(2525), AppDelegate.TextProvider.GetText(2526));
                        return false;
                    }
                    textfiled.ResignFirstResponder();
                    return true;
                }
                return false;
            };

            txtUsersSessionTimeout.AllEditingEvents += thresholdTextChanged;
            txtUsersSessionTimeout.EditingDidEnd += (object sender, EventArgs e) =>
            {
                var textfiled = sender as UITextField;
                if (textfiled == null) return;
                textfiled.Text = AppDelegate.SessionTimeoutMinutes.ToString();
            };
            txtUsersSessionTimeout.ShouldReturn += (textfiled) =>
            {
                if (!string.IsNullOrWhiteSpace(textfiled.Text))
                {
                    try
                    {
                        AppDelegate.SessionTimeoutMinutes = int.Parse(textfiled.Text);
                    }
                    catch (Exception ex)
                    {
                        AIMSError(AppDelegate.TextProvider.GetText(2525), ex.Message);
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                        return false;
                    }
                    textfiled.ResignFirstResponder();
                    return true;
                }
                return false;
            };


            txtStatisticsRefreshInterval.AllEditingEvents += thresholdTextChanged;
            txtStatisticsRefreshInterval.EditingDidEnd += (object sender, EventArgs e) =>
            {
                var textfiled = sender as UITextField;
                if (textfiled == null) return;
                textfiled.Text = AppDelegate.RefreshIntervalMinutes.ToString();
            };

            txtStatisticsRefreshInterval.ShouldReturn += (textfield) =>
            {
                if (!string.IsNullOrWhiteSpace(textfield.Text))
                {
                    try
                    {
                        AppDelegate.RefreshIntervalMinutes = int.Parse(textfield.Text);

                        AppDelegate.UpdateOccupancyTimer();
                    }
                    catch (Exception ex)
                    {
                        AIMSError(AppDelegate.TextProvider.GetText(2525), ex.Message);
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                        return false;
                    }
                    textfield.ResignFirstResponder();
                    return true;
                }
                return false;
            };

            txtStatisticsMaxHoursInterval.AllEditingEvents += thresholdTextChanged;
            txtStatisticsMaxHoursInterval.EditingDidEnd += (object sender, EventArgs e) =>
            {
                var textfiled = sender as UITextField;
                if (textfiled == null) return;
                textfiled.Text = AppDelegate.HoursOccupancyHistory.ToString();
            };

            txtStatisticsMaxHoursInterval.ShouldReturn += (textfield) =>
            {
                if (!string.IsNullOrWhiteSpace(textfield.Text))
                {
                    try
                    {
                        AppDelegate.HoursOccupancyHistory = int.Parse(textfield.Text);
                    }
                    catch (Exception ex)
                    {
                        AIMSError(AppDelegate.TextProvider.GetText(2525), ex.Message);
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                        return false;
                    }

                    textfield.ResignFirstResponder();
                    return true;
                }
                return false;
            };


            //scannerLabel.AdjustsFontSizeToFitWidth = true;

            LoadTexts();

            var gesture = new UITapGestureRecognizer(() => View.EndEditing(true));
            gesture.CancelsTouchesInView = false; //for iOS5
            View.AddGestureRecognizer(gesture);
        }

        protected void AddDoneButton(UITextField textField)
        {
            var toolbar = new UIToolbar(new CGRect(0.0f, 0.0f, 50.0f, 44.0f));

            var doneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate
            {
                textField.ShouldReturn.Invoke(textField);
            });

            toolbar.Items = new UIBarButtonItem[] {
                new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
                doneButton
            };

            textField.InputAccessoryView = toolbar;
        }

        public void initLinedView()
        {
            //linedView = new LinedView(UIColor.Gray);

            //this.devScrollView.AddSubview(linedView);
        }

        public void setFonts()
        {
           // if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                    UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

                nfloat scaleFactor = (height / 667f) * 0.6f;

                var isLandscape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
                var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
                if (!isPad && !isLandscape && scaleFactor > 1f)
                    scaleFactor = 1f;

                //Commenting to support iPhone 5 and 5s
                //if (scaleFactor < 1f)
                //{
                //    scaleFactor = 1f;
                //}

                nfloat largeTexts = 28f * scaleFactor, smallTexts = 18f * scaleFactor;
                string italicFontName = "HelveticaNeue-Italic", boldFontName = "Helvetica-Bold", regularFontName = "Helvetica Neue";

                var largeFont = UIFont.FromName(boldFontName, largeTexts);
                var smallFont = UIFont.FromName(italicFontName, smallTexts);
                var regularFont = UIFont.FromName(regularFontName, smallTexts);


                //scan view
                advancedSettingsCameraTitle.Font = largeFont;
                scannerLabel.Font = smallFont;

                //logs view
                advancedSettingsLogsTitle.Font = largeFont;
                logsLabel.Font = smallFont;
                uploadLogsButton.Font = regularFont;
                clearLogsButton.Font = regularFont;

                //Threshold View
                advancedSettingsPVThresholdLabel.Font = largeFont;
                successScansLabel.Font = smallFont;
                failedScansLabel.Font = smallFont;
                txtSuccessScanThreshold.Font = txtFailedScanThreshold.Font = UIFont.FromName(boldFontName, smallTexts);

                //Users View
                advancedSettingsPVUserLabel.Font = largeFont;
                sessionTimeoutLabel.Font = smallFont;
                txtUsersSessionTimeout.Font = UIFont.FromName(boldFontName, smallTexts);

                //Statistics View
                advancedSettingsPVStatisticsLabel.Font = largeFont;
                refreshIntervalLabel.Font = smallFont;
                txtStatisticsRefreshInterval.Font = UIFont.FromName(boldFontName, smallTexts);
                maxHoursIntervalLabel.Font = smallFont;
                txtStatisticsMaxHoursInterval.Font = UIFont.FromName(boldFontName, smallTexts);
            }
        }

        protected void HandleLeftSwipe(UISwipeGestureRecognizer recognizer)
        {
            UITabSwipe.HandleLeftSwipe(/*recognizer,*/ this);
        }

        protected void HandleRightSwipe(UISwipeGestureRecognizer recognizer)
        {
            UITabSwipe.HandleRightSwipe(/*recognizer,*/ this);
        }

        public void changeCamera(object sende, EventArgs e)
        {
            if (AppDelegate.ActiveCamera == CameraType.ExternalCamera) return;
            if (AppDelegate.ActiveCamera == CameraType.FrontCamera)
            {
                AppDelegate.ActiveCamera = CameraType.BackCamera;
            }
            else
            {
                AppDelegate.ActiveCamera = CameraType.FrontCamera;
            }
            refreshControlsState();
        }

        private void clearLogs(object sender, EventArgs e)
        {
            AIMSConfirm(AppDelegate.TextProvider.GetText(2040), AppDelegate.TextProvider.GetText(2518), (ss, ee) =>
            {
                repository.DeleteAllLogs();
                AIMSMessage("", AppDelegate.TextProvider.GetText(2031));
            });
        }

        private void sendLogs(object sender, EventArgs e)
        {
            repository.UploadLogs((ss, ee) =>
            {
                AIMSMessage("", AppDelegate.TextProvider.GetText(2031));//TODO: Give a Reference-Number to the client
            });
        }

        private void thresholdTextChanged(object sender, EventArgs e)
        {
            var txt = sender as UITextField;
            if (txt == null) return;
            txt.Text = Regex.Replace(txt.Text, @"[^0-9]+", "");
            txt.Text = Regex.Replace(txt.Text, " ", "");

            if (txt.Text.Length > 4)
            {
                txt.Text = txt.Text.Substring(0, 4);
            }

        }

        public void downButton(object send, EventArgs e)
        {
            AgentMenuTabController temp = this.TabBarController as AgentMenuTabController;

            temp.goToAirLineViewController();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            updateLayout();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (this.NavigationController != null)
                this.NavigationController.NavigationBarHidden = true;

            updateLayout();
        }

        private void updateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;
            float statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;


            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ? UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
   0 : (float)View.SafeAreaInsets.Left;


            DownScanViewModule.UpdateScanViewLayout(this.View.Frame, bottomView, leftDown,
                rightDown, scanButton, null, leftMargin);


            nfloat scaleFactor = (height / 667f) * 0.8f;
            //Commenting to support iPhone 5 and 5s
            //if (scaleFactor < 1f)
            //{
            //    scaleFactor = 1f;
            //}

            nfloat margin = 10f * scaleFactor;

            if (View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            if (topButton != null)
                topButton.Frame = new CGRect(0f, statusBarH, this.View.Frame.Width, yOffset);

            //------------scroll view--------------
            devScrollView.Frame = new CGRect(0f, statusBarH + yOffset, frame.Width,
                frame.Height - bottomView.Frame.Height - statusBarH);

            devScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, 400d * scaleFactor + yOffset);

            nfloat widthButton = 110f * scaleFactor;
            nfloat heightButton = 40f * scaleFactor;
            nfloat xButton = frame.Width - widthButton - margin;

            //-------------- scan view-----------------------------
            scanView.Frame = new CGRect(0f, 0f, frame.Width, 95f * scaleFactor);

            advancedSettingsCameraTitle.Frame = new CGRect(margin + leftMargin, margin,
                scanView.Frame.Width - (2f * margin) - leftMargin, 30f * scaleFactor);

            cameraSwitchButton.Frame = new CGRect(xButton, (45f * scaleFactor),
                widthButton, heightButton);

            nfloat camImageSize = cameraSwitchButton.Frame.Height - 2f;
            cameraImage.Frame = new CGRect(
                cameraSwitchButton.Frame.X + (cameraSwitchButton.Frame.Width / 2f) - camImageSize / 2f,
                cameraSwitchButton.Frame.Y + 2f,
                camImageSize,
                camImageSize);

            scannerLabel.Frame = new CGRect(margin + leftMargin, cameraSwitchButton.Frame.Y + margin,
                scanView.Frame.Width - cameraSwitchButton.Frame.Width - (2f * margin) - leftMargin, cameraSwitchButton.Frame.Height - margin);

            //---------------------logs view----------------------------
            logsView.Frame = new CGRect(0f, scanView.Frame.Bottom, frame.Width, 145f * scaleFactor);

            advancedSettingsLogsTitle.Frame = new CGRect(margin + leftMargin, margin,
                logsView.Frame.Width - (2f * margin) - leftMargin, 30f * scaleFactor);

            uploadLogsButton.Frame = new CGRect(xButton, (45f * scaleFactor),
                widthButton, heightButton);

            clearLogsButton.Frame = new CGRect(uploadLogsButton.Frame.X, uploadLogsButton.Frame.Bottom + margin,
                widthButton, heightButton);

            logsLabel.Frame = new CGRect(margin + leftMargin, uploadLogsButton.Frame.Y + margin,
                logsView.Frame.Width - (2f * margin) - uploadLogsButton.Frame.Width - leftMargin, uploadLogsButton.Frame.Height - margin);


            //-------------------Threshold view-------------------------
            thresholdView.Frame = new CGRect(0f, logsView.Frame.Bottom, frame.Width, 128f * scaleFactor);

            advancedSettingsPVThresholdLabel.Frame = new CGRect(margin + leftMargin, margin,
                thresholdView.Frame.Width - (2f * margin) - leftMargin, 30f * scaleFactor);

            txtSuccessScanThreshold.Frame = new CGRect(xButton, advancedSettingsPVThresholdLabel.Frame.Bottom + margin,
                widthButton, 30f * scaleFactor);

            txtFailedScanThreshold.Frame = new CGRect(xButton, txtSuccessScanThreshold.Frame.Bottom + margin,
                widthButton, 30f * scaleFactor);

            successScansLabel.Frame = new CGRect(margin + leftMargin, txtSuccessScanThreshold.Frame.Y + margin,
                thresholdView.Frame.Width - (2f * margin) - txtSuccessScanThreshold.Frame.Width - leftMargin, txtSuccessScanThreshold.Frame.Height - margin);

            failedScansLabel.Frame = new CGRect(margin + leftMargin, txtFailedScanThreshold.Frame.Y + margin,
                thresholdView.Frame.Width - (2f * margin) - txtFailedScanThreshold.Frame.Width - leftMargin, txtFailedScanThreshold.Frame.Height - margin);


            //---------------------Users view---------------------------
            userSettingsView.Frame = new CGRect(0f, thresholdView.Frame.Bottom, frame.Width, 88f * scaleFactor);

            advancedSettingsPVUserLabel.Frame = new CGRect(margin + leftMargin, margin,
                userSettingsView.Frame.Width - (2f * margin) - leftMargin, 30f * scaleFactor);

            txtUsersSessionTimeout.Frame = new CGRect(xButton, advancedSettingsPVUserLabel.Frame.Bottom + margin,
                widthButton, 30f * scaleFactor);

            sessionTimeoutLabel.Frame = new CGRect(margin + leftMargin, txtUsersSessionTimeout.Frame.Y + margin,
                userSettingsView.Frame.Width - (2f * margin) - txtUsersSessionTimeout.Frame.Width - leftMargin, txtUsersSessionTimeout.Frame.Height - margin);

            //---------------------Statistics view---------------------------
            statisticsView.Frame = new CGRect(0f, userSettingsView.Frame.Bottom, frame.Width, 128f * scaleFactor);

            advancedSettingsPVStatisticsLabel.Frame = new CGRect(margin + leftMargin, margin,
                statisticsView.Frame.Width - (2f * margin) - leftMargin, 30f * scaleFactor);

            txtStatisticsRefreshInterval.Frame = new CGRect(xButton, advancedSettingsPVStatisticsLabel.Frame.Bottom + margin,
                widthButton, 30f * scaleFactor);

            txtStatisticsMaxHoursInterval.Frame = new CGRect(xButton, txtStatisticsRefreshInterval.Frame.Bottom + margin,
                widthButton, 30f * scaleFactor);

            refreshIntervalLabel.Frame = new CGRect(margin + leftMargin, txtStatisticsRefreshInterval.Frame.Y + margin,
                statisticsView.Frame.Width - (2f * margin) - txtStatisticsRefreshInterval.Frame.Width - leftMargin, txtStatisticsRefreshInterval.Frame.Height - margin);

            maxHoursIntervalLabel.Frame = new CGRect(margin + leftMargin, txtStatisticsMaxHoursInterval.Frame.Y + margin,
                statisticsView.Frame.Width - (2f * margin) - txtStatisticsMaxHoursInterval.Frame.Width - leftMargin, txtStatisticsMaxHoursInterval.Frame.Height - margin);


            devScrollView.ContentSize = new CoreGraphics.CGSize(frame.Width, scanView.Frame.Height + logsView.Frame.Height +
                                        thresholdView.Frame.Height + userSettingsView.Frame.Height + statisticsView.Frame.Height +
                                        (50f * scaleFactor) + yOffset);


            //cameraSwitchButton.Frame = new CGRect(xButton, margin, widthButton,
            //    scanView.Frame.Height - (margin * 2f));

            //lined view
            //linedView.Frame = new CGRect(margin, scanView.Frame.Y,
            //    UIScreen.MainScreen.Bounds.Width - (2f * margin), statisticsView.Frame.Bottom);

            //linedView.LinesY = new nfloat[5] { scanView.Frame.Y, logsView.Frame.Y, thresholdView.Frame.Y, userSettingsView.Frame.Bottom, statisticsView.Frame.Bottom };


            refreshControlsState();
        }

        private void refreshControlsState()
        {
            if (!AppDelegate.ActiveCamera.HasValue || AppDelegate.ActiveCamera == CameraType.BackCamera)
            {
                scannerLabel.Text = AppDelegate.TextProvider.GetText(2167);
                cameraImage.Image = UIImage.FromFile("Camera/camera_front.png");
            }
            else if (AppDelegate.ActiveCamera == CameraType.FrontCamera)
            {
                scannerLabel.Text = AppDelegate.TextProvider.GetText(2178);
                cameraImage.Image = UIImage.FromFile("Camera/camera_back.png");
            }
            else
            {
                scannerLabel.Text = "Camera: (External Device)";
                cameraImage.Image = null;//UIImage.FromFile("Camera/camera_front.png");
            }

            txtSuccessScanThreshold.Text = PassengersRepository.SucceededBarcodeInterval.ToString();
            txtFailedScanThreshold.Text = PassengersRepository.FailedBarcodeInterval.ToString();
            txtUsersSessionTimeout.Text = AppDelegate.SessionTimeoutMinutes.ToString();
            txtStatisticsMaxHoursInterval.Text = AppDelegate.HoursOccupancyHistory.ToString();
            txtStatisticsRefreshInterval.Text = AppDelegate.RefreshIntervalMinutes.ToString();
        }

    }
}