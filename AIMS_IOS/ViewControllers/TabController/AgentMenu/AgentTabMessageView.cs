using AIMS;
using AIMS.Models;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using CoreGraphics;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public partial class AgentTabMessageView : UIViewController
    {
        //UIImageView bottomBackground;

        List<Message> items = new List<Message>();

        nfloat yOffset = 0f;
        public UIButton topButton;
        public UIView whiteBackground;

        public nfloat ScaleFactor { get; private set; }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() => {
                scanButton.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                scanButton.SetTitle(AppDelegate.Current.LineaDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras

                this.TabBarItem.Title = AppDelegate.TextProvider.GetText(2041);

                //scanButton.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);
            });
        }

        public AgentTabMessageView (IntPtr handle) : base (handle)
        {
        }

        private void LoadMessages()
        {
            AppManager.getListOfAgentMessages((repository)=> {
                InvokeOnMainThread(()=> {
                    if (repository == null) return;
                    else if (repository is IEnumerable<Message>)
                    {
                        items = ((IEnumerable<Message>)repository).ToList();
                        #region Mock Message
#if DEBUG
                        items.Add(new Message
                        {
                            Time = DateTime.Now,
                            Title = "Today's Long and biiiiiiiiiiiig titled Announcement",
                            Body = "Last descriptions, and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts"
                        });
                        items.Add(new Message
                        {
                            Time = DateTime.Now.AddMinutes(-30),
                            Title = "Today's Announcement",
                            Body = "Again descriptions, and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts"
                        });
                        items.Add(new Message {
                            Time = DateTime.Now.AddDays(-1),
                            Title = "Yesterday's Announcement",
                            Body = "Descriptions, and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts"
                        });
#endif
                        #endregion
                        messageTableView.Source = new AgentTabMessageViewSource(items, this);
                        updateTableLayout();
                    }
                });
            });
        }

        public override void ViewDidLoad()
		{
			base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            whiteBackground = new UIView();
            whiteBackground.BackgroundColor = UIColor.White;
            this.View.AddSubview(whiteBackground);
            this.View.SendSubviewToBack(whiteBackground);

            //if (true)//???
            //{
                yOffset = AppDelegate.AirlinesView.topPanelHeight;
                topButton = new UIButton();
                topButton.TitleLabel.Text = "";
                topButton.BackgroundColor = UIColor.Clear;

                topButton.TouchUpInside += downButton;

                this.View.Add(topButton);
            //}

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;
            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                scanButton.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
            };

            //updateTableLayout();

            //bottomBackground = new UIImageView(UIImage.FromFile("gradiant.png"));
            //bottomView.AddSubview(bottomBackground);
            //bottomView.SendSubviewToBack(bottomBackground);


            leftDown.TouchUpInside += downButton;
            rightDown.TouchUpInside += downButton;
            scanButton.TouchUpInside += (this.TabBarController as AgentMenuTabController).scanClicked;

            var swipeLeft = new UISwipeGestureRecognizer(HandleLeftSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Left };
            View.AddGestureRecognizer(swipeLeft);

            var swipeRight = new UISwipeGestureRecognizer(HandleRightSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Right };
            View.AddGestureRecognizer(swipeRight);

            scanButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            scanButton.Layer.BorderWidth = 1f;

            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            LoadTexts();
        }
        
        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            updateLayout();
            LoadMessages();

        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            updateLayout();
            LoadMessages();
        }

        public void updateTableLayout()
        {
            //nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
            //    UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ////ratio based on iphone 6 minimum of 28f
            //nfloat estimatedHeight = 80f / 650f * height;
            nfloat estimatedHeight = 80f * ScaleFactor;
            estimatedHeight = estimatedHeight < 90f ? 90f : estimatedHeight;

            messageTableView.EstimatedRowHeight = estimatedHeight;
            messageTableView.RowHeight = estimatedHeight;
        }

        public void updateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;
            float statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            if (View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ScaleFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                //ScaleFactor = (frame.Height - statusBarH - detailsView.Frame.Height);
                ScaleFactor = height / (667f * (4f / 3f));
            }

            var isLandscape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
            var isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            if (!isPad && !isLandscape && ScaleFactor > 1f)
                ScaleFactor = 1f;

            if (topButton != null)
            {
                topButton.Frame = new CGRect(0f, statusBarH, this.View.Frame.Width, yOffset);

                whiteBackground.Frame = new CGRect(0f, topButton.Frame.Bottom, frame.Width, frame.Height);
            }

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ? 0 : (float)View.SafeAreaInsets.Left;

            DownScanViewModule.UpdateScanViewLayout(this.View.Frame, bottomView, leftDown,
                rightDown, scanButton, null, leftMargin);


            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.66f;

                nfloat childXOffset = (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f);

                messageTableView.Frame = new CGRect(childXOffset, statusBarH + yOffset,
                childWidth,
                this.View.Frame.Height - statusBarH - bottomView.Frame.Height - yOffset);
            }
            {
                messageTableView.Frame = new CGRect(0f, statusBarH + yOffset + 10f,
                this.View.Frame.Width,
                this.View.Frame.Height - statusBarH - bottomView.Frame.Height - yOffset);
            }

            


        }

        public void downButton(object send, EventArgs e)
        {
            AgentMenuTabController temp = this.TabBarController as AgentMenuTabController;

            temp.goToAirLineViewController();
        }

        protected void HandleLeftSwipe(UISwipeGestureRecognizer recognizer)
        {
            //(this.TabBarController as AgentMenuTabController).fromSwipe = true;
            UITabSwipe.HandleLeftSwipe(/*recognizer,*/ this);
        }

        protected void HandleRightSwipe(UISwipeGestureRecognizer recognizer)
        {
            //(this.TabBarController as AgentMenuTabController).fromSwipe = true;
            UITabSwipe.HandleRightSwipe(/*recognizer,*/ this);
        }

        public void handleTabBarClicked(object sender, EventArgs e)
        {
            //(this.TabBarController as AgentMenuTabController).fromSwipe = false;

        }
    }


	public partial class AgentTabMessageViewSource : UITableViewSource
	{
		List<Message> items;
        AgentTabMessageView parentViewController;
		protected NSString cellIdentifier = (NSString)"MessagesCell";
        
		public AgentTabMessageViewSource(List<Message> items, AgentTabMessageView viewController)
		{
			this.items = items;
            this.parentViewController = viewController;
            viewController.TabBarController.TabBar.Items[1].BadgeValue = items.Count.ToString();
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell(cellIdentifier) as MessagesCell;

            cell.UpdateLayout(tableView.RowHeight, parentViewController);

            var item = items[indexPath.Row];
            cell.UpdateCell(item.Title, item.Body, string.Format("{0:yyyy/MM/dd} - {1}", item.Time, item.Time.ToShortTimeString()));

			return cell;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return items.Count;
		}

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            var selectedMessage = items[indexPath.Row];
            if (selectedMessage != null)
            {
                (new MessageRepository()).LoadMessageBody(selectedMessage.Handle, (message) => {
                    InvokeOnMainThread(()=> {// not sure this is necessary
                        new UIAlertView(selectedMessage.Title,
                        string.Format("From:{0}\nDate:{1}{3}\n-------\n{2}",
                                    selectedMessage.From,
                                    selectedMessage.Time,
                                    message,
                                    string.IsNullOrWhiteSpace(selectedMessage.Body) ? "" : string.Format("\nShort message: {0}", selectedMessage.Body))
                        , null, "OK", null).Show();

                        tableView.DeselectRow(indexPath, true);
                    });
                });
            }
        }

        
    }

}