﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("AgentTabLogs")]
    partial class AgentTabLogs
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView AgentTableLog { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentTabBottomView bottomView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton leftDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton rightDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton scanButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AgentTableLog != null) {
                AgentTableLog.Dispose ();
                AgentTableLog = null;
            }

            if (bottomView != null) {
                bottomView.Dispose ();
                bottomView = null;
            }

            if (leftDown != null) {
                leftDown.Dispose ();
                leftDown = null;
            }

            if (rightDown != null) {
                rightDown.Dispose ();
                rightDown = null;
            }

            if (scanButton != null) {
                scanButton.Dispose ();
                scanButton = null;
            }
        }
    }
}