using AIMS;
using AIMS.Models;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using CoreGraphics;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public partial class AgentTabDevLogs : UIViewController
    {
        List<AgentTabLogsCellData> items;

        //UIImageView bottomBackground;

        public nfloat yOffset;
        public UIButton topButton;
        //public nfloat estimatedHeight;

        public UIView whiteBackground;

        public nfloat ScaleFactor { get; private set; }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() => {
                scanButton.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                scanButton.SetTitle(AppDelegate.Current.LineaDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras
                //scanButton.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);

                this.TabBarItem.Title = AppDelegate.TextProvider.GetText(2040);
            });
        }

        public AgentTabDevLogs(IntPtr handle) : base(handle)
        {
        }

        private void LoadDevLogs()
        {
            AppManager.getAgentDevTabLogs((repository) => {
                InvokeOnMainThread(() => {
                    if (repository == null) return;
                    items = ((IEnumerable<Log>)repository).Select(x => new AgentTabLogsCellData(x)).ToList();
                    logsTableView.Source = new AgentTabDevLogsSource(items, this);// estimatedHeight,
                    //logsTableView.ReloadData();
                    updateTableLayout(ScaleFactor);
                });
            });
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;
            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                InvokeOnMainThread(() => {
                    scanButton.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
                });
            };

            //updateTableLayout();

            whiteBackground = new UIView();
            whiteBackground.BackgroundColor = UIColor.White;
            this.View.AddSubview(whiteBackground);
            this.View.SendSubviewToBack(whiteBackground);

            leftDown.TouchUpInside += downButton;
            rightDown.TouchUpInside += downButton;
            scanButton.TouchUpInside += (this.TabBarController as AgentMenuTabController).scanClicked;

            var swipeLeft = new UISwipeGestureRecognizer(HandleLeftSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Left };
            View.AddGestureRecognizer(swipeLeft);

            var swipeRight = new UISwipeGestureRecognizer(HandleRightSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Right };
            View.AddGestureRecognizer(swipeRight);

            //bottomBackground = new UIImageView(UIImage.FromFile("gradiant.png"));
           // bottomView.AddSubview(bottomBackground);
           // bottomView.SendSubviewToBack(bottomBackground);

            scanButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            scanButton.Layer.BorderWidth = 1f;

            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            //if (true)//???
            //{
            yOffset = AppDelegate.AirlinesView.topPanelHeight;
                topButton = new UIButton();
                topButton.TitleLabel.Text = "";
                topButton.BackgroundColor = UIColor.Clear;

                topButton.TouchUpInside += downButton;
                this.View.Add(topButton);
            //}

            LoadTexts();

        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (this.NavigationController != null)//it is always null
            {
                this.NavigationController.NavigationBarHidden = true;
            }

            updateLayout();
            LoadDevLogs();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            updateLayout();
        }

        public void updateTableLayout(nfloat scaleFactor)
        {
            //ratio based on iphone 6 minimum of 50f
            nfloat estimatedHeight = 50f * scaleFactor;
            estimatedHeight = estimatedHeight < 50f ? 50f : estimatedHeight;

            logsTableView.EstimatedRowHeight = estimatedHeight;
            logsTableView.RowHeight = estimatedHeight;

            //nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
            //    UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ////ratio based on iphone 6 minimum of 50f
            //estimatedHeight = 140f / 650f * height;
            //if(estimatedHeight < 140f)
            //{
            //    estimatedHeight = 140f;
            //}

            ////logsTableView.RowHeight = UITableView.AutomaticDimension;
            ////logsTableView.EstimatedRowHeight = estimatedHeight;

            ////logsTableView.RowHeight = estimatedHeight;
        }

        public void updateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;
            float statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            if (View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ScaleFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                ScaleFactor = height / (667f * (4f / 3f));
            }
            //updateTableLayout();


            if (topButton != null)
            {
                topButton.Frame = new CGRect(0f, statusBarH, this.View.Frame.Width, yOffset);
                whiteBackground.Frame = new CGRect(0f, topButton.Frame.Bottom, this.View.Frame.Width, this.View.Frame.Height);
            }

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ? 0 : (float)View.SafeAreaInsets.Left;

            DownScanViewModule.UpdateScanViewLayout(this.View.Frame, bottomView, leftDown,
                rightDown, scanButton, null,leftMargin);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.66f;

                nfloat childXOffset = (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f);

                logsTableView.Frame = new CGRect(0f, statusBarH + yOffset + 10f,
                this.View.Frame.Width,
                this.View.Frame.Height - statusBarH - yOffset - bottomView.Frame.Height - 10f);
            }
            else
            {
                logsTableView.Frame = new CGRect(0f, statusBarH + yOffset + 10f,
                this.View.Frame.Width,
                this.View.Frame.Height - statusBarH - yOffset - bottomView.Frame.Height - 10f);
            }

            //logsTableView.Frame = new CGRect(0f, statusBarH + yOffset,
            //    this.View.Frame.Width, 
            //    this.View.Frame.Height - statusBarH - yOffset - bottomView.Frame.Height);
        }

        protected void HandleLeftSwipe(UISwipeGestureRecognizer recognizer)
        {
            UITabSwipe.HandleLeftSwipe(/*recognizer,*/ this);
        }

        protected void HandleRightSwipe(UISwipeGestureRecognizer recognizer)
        {
            UITabSwipe.HandleRightSwipe(/*recognizer,*/ this);
        }

        public void downButton(object send, EventArgs e)
        {
            AgentMenuTabController temp = this.TabBarController as AgentMenuTabController;

            temp.goToAirLineViewController();
        }
    }


    public class AgentTabDevLogsSource : UITableViewSource
    {
        List<AgentTabLogsCellData> items;
        protected NSString cellIdentifier = (NSString)"devLogCell";
        //nfloat estimatedHeight;

        public AgentTabDevLogs ParentViewController { get; private set; }

        public AgentTabDevLogsSource(List<AgentTabLogsCellData> items, AgentTabDevLogs viewController)//, nfloat estimatedHeight
        {
            this.items = items;
            //this.estimatedHeight = estimatedHeight;
            ParentViewController = viewController;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            var cell = tableView.DequeueReusableCell(cellIdentifier) as DevLogCell;

            cell.TranslatesAutoresizingMaskIntoConstraints = false;

            cell.UpdateCell(items[indexPath.Row].title, items[indexPath.Row].content, items[indexPath.Row].localTime, 
                            items[indexPath.Row].log.LogLevel == LogLevel.Developer);

            cell.UpdateLayout(tableView.RowHeight, ParentViewController);

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return items.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            new UIAlertView(items[indexPath.Row].title, "[" + items[indexPath.Row].content + "] " + items[indexPath.Row].localTime, null, "Ok", null).Show();

            tableView.DeselectRow(indexPath, true);
        }
        
        //public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        //{
        //    var cell = this.GetCell(tableView, indexPath) as DevLogCell;
        //    return cell.getHeight();
        //}

    }
    
}