using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;
using AIMS;
using AIMS_IOS.Utility;
using Ieg.Mobile.Localization;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;
using AIMS.Models;

namespace AIMS_IOS
{
    public partial class AgentTabHomeView : AIMSViewController
    {
        //UIImageView bottomBackground;
        nfloat yOffset = 0f;

        UIButton topButton;

        LinedView linedView;

        UITapGestureRecognizer tap;

        #region fields
        //loging view
        CGRect loginRect;
        UIView backgroundViewLogin;
        UIButton backgroundButton;
        UIImageView loginLogo;
        RoundedRectView loginView;
        UILabel loginTitle;
        UIView usernameBackground;
        UITextField loginUsername;
        UIView passwordBackground;
        UITextField loginPassword;
        UIButton loginCancel;
        UIButton loginLogin;
        //bool loginCreated = false;
        //dropDown
        UIDropDownView dropDown;
        List<DropDownCell> dropCells;

        #endregion

        public AgentTabHomeView(IntPtr handle) : base(handle)
        {
        }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() =>
            {
                loungeAgentLabel.Text = AppDelegate.TextProvider.GetText(2012);//TODO: role name from server
                lblOccupancy.Text = AppDelegate.TextProvider.GetText(2039);//Occupancy//Next phase
                languageLabel.Text = AppDelegate.TextProvider.GetText(2038);//Language
                settingsLabel.Text = AppDelegate.TextProvider.GetText(2046);//Advanced Settings
                scanButton.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                scanButton.SetTitle(AppDelegate.Current.LineaDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras

                this.TabBarItem.Title = AppDelegate.TextProvider.GetText(2129);

                termsButton.SetTitle(AppDelegate.TextProvider.GetText(2142), UIControlState.Normal);
                privacyButton.SetTitle(AppDelegate.TextProvider.GetText(2143), UIControlState.Normal);

                //scanButton.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);
            });
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            if (AppDelegate.IsDeveloperModeEnabled)
            {
                //settingView.Hidden = true;
                settingsLabel.Hidden = true;
                settingButton.Hidden = true;
                settingImg.Hidden = true;
                settingView.Hidden = true;
            }
            else
            {
                //settingView.Hidden = false;
                settingsLabel.Hidden = false;
                settingButton.Hidden = false;
                settingImg.Hidden = false;
                settingView.Hidden = false;
            }

            initLinedView();

            //if (true)//???
            //{
            yOffset = AppDelegate.AirlinesView.topPanelHeight;
            topButton = new UIButton();
            topButton.TitleLabel.Text = "";
            topButton.BackgroundColor = UIColor.Clear;

            topButton.TouchUpInside += downButton;
            this.View.Add(topButton);
            //}

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;
            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                InvokeOnMainThread(() =>
                {
                    scanButton.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
                });
            };

            setFonts();

            //bottomBackground = new UIImageView(UIImage.FromFile("gradiant.png"));
            //bottomView.AddSubview(bottomBackground);
            //bottomView.SendSubviewToBack(bottomBackground);

            //int tabIndex = 1;
            //string badgeString = "";
            //this.TabBarController.TabBar.Items[tabIndex].BadgeValue = badgeString;

            View.AddGestureRecognizer(new UISwipeGestureRecognizer(HandleLeftSwipe)
            {
                Direction = UISwipeGestureRecognizerDirection.Left
            });
            //View.AddGestureRecognizer(new UISwipeGestureRecognizer(HandleLeftSwipe)
            //{
            //    Direction = UISwipeGestureRecognizerDirection.Up
            //});
            //View.AddGestureRecognizer(new UISwipeGestureRecognizer(HandleLeftSwipe)
            //{
            //    Direction = UISwipeGestureRecognizerDirection.Down
            //});


            //var tabController = (this.TabBarController as AgentMenuTabController);

            //View.AddGestureRecognizer(new UIPanGestureRecognizer(tabController.HandlePanGuestureRecognizer));
            //View.AddGestureRecognizer(new UISwipeGestureRecognizer(tabController.HandleSwipeGuestureRecognizer)
            //{
            //    Direction = UISwipeGestureRecognizerDirection.Left | UISwipeGestureRecognizerDirection.Right | UISwipeGestureRecognizerDirection.Up | UISwipeGestureRecognizerDirection.Down
            //});

            tap = new UITapGestureRecognizer((textfiled) =>
            {
                UIView temp = AutoScrollUtility.findFirstResponder(this.View);

                if (temp != null)
                {
                    temp.ResignFirstResponder();
                }
            });
            tap.CancelsTouchesInView = false;
            this.View.AddGestureRecognizer(tap);

            initLoginView();

            leftDown.TouchUpInside += downButton;
            rightDown.TouchUpInside += downButton;
            scanButton.TouchUpInside += (this.TabBarController as AgentMenuTabController).scanClicked;

            termsButton.TouchUpInside += termsButtonClick;
            privacyButton.TouchUpInside += privacyButtonClick;

            initDropDown();

            scanButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            scanButton.Layer.BorderWidth = 1f;

            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            settingButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            settingButton.Layer.BorderWidth = 1f;

            agentName.AdjustsFontSizeToFitWidth = true;
            loungeAgentLabel.AdjustsFontSizeToFitWidth = true;
            settingsLabel.AdjustsFontSizeToFitWidth = true;
            languageLabel.AdjustsFontSizeToFitWidth = true;

            LoadTexts();

            prgOccupancy.LoadProgressData();
        }

        public void setFonts()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                //UIFont titleFont = UIFont.FromName("font name", 20f);
                //UIFont title2Font = UIFont.FromName("font name", 15f);

                ///agent view
                agentName.Font = UIFont.FromName("Helvetica-Bold", 26f);
                loungeAgentLabel.Font = UIFont.FromName("Helvetica Neue", 22f);

                //lounge view
                loungeDescription.Font = UIFont.FromName("Helvetica Neue", 26f);
                loungeTitle.Font = UIFont.FromName("Helvetica-Bold", 22f);

                //Occupancy view
                lblOccupancy.Font = UIFont.FromName("Helvetica-Bold", 26f);

                //language view
                languageLabel.Font = UIFont.FromName("Helvetica-Bold", 26f);

                //dropDown
                //dropDown.font =  UIFont.FromName("font name", 20f);

                //setting view
                settingsLabel.Font = UIFont.FromName("Helvetica-Bold", 26f);

                //info view
                IEGAmericaLabel.Font = UIFont.FromName("Helvetica-Bold", 26f);
                termsButton.Font = UIFont.FromName("Helvetica Neue", 22f);
                privacyButton.Font = UIFont.FromName("Helvetica Neue", 22f);

                //login view
                //loginTitle.Font = UIFont.FromName("font name", 20f);

                //loginUsername.Font = UIFont.FromName("font name", 20f);
                //loginPassword.Font = UIFont.FromName("font name", 20f);

                //loginCancel.Font = UIFont.FromName("font name", 20f);
                //loginLogin.Font = UIFont.FromName("font name", 20f);

            }


            UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(() =>
            {
                this.PresentViewController(AppDelegate.MapViewController, true, () =>
                {

                });
            });

            loungeTitle.UserInteractionEnabled = true;
            loungeTitle.AddGestureRecognizer(tapGesture);
        }

        public void initLinedView()
        {
            linedView = new LinedView();

            this.homeScrollView.AddSubview(linedView);
        }

        public void initDropDown()
        {

            //Get lang and assign them here

            dropCells = AppManager.getListOfLangugeDrpItems();
            dropDown = new UIDropDownView();

            float yPos = (float)languageView.ConvertPointToView(new CGPoint(languageLabel.Frame.X, languageLabel.Frame.Y),
                homeScrollView).Y;

            int defaultPosition = 0;
            if (AppDelegate.TextProvider.SelectedLanguage == Language.Portuguese)
                defaultPosition = 2;
            else if (AppDelegate.TextProvider.SelectedLanguage == Language.French)
                defaultPosition = 1;

            dropDown.init(dropCells, (float)settingButton.Frame.Height, defaultPosition, (float)settingButton.Frame.Width, homeScrollView);
            //dropDown.init(dropCells, 36f, 0, 150f, languageView);
            dropDown.ItemSelected = new UIDropDownView.eventDelegate((ss, ee) =>
            {
                InvokeOnMainThread(() =>
                {
                    try
                    {
                        AppDelegate.TextProvider.SelectedLanguage = (Language)dropDown.selected;

                        downButton(null, null);
                    }
                    catch { }
                });
            });
            //Omar I need the language selection event of this drp down!

            this.homeScrollView.AddSubview(dropDown);
            //this.languageView.AddSubview(dropDown);

            //updateLayout();
        }

        public void downButton(object send, EventArgs e)
        {
            AgentMenuTabController temp = (AgentMenuTabController)this.TabBarController;

            temp.goToAirLineViewController();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            //updateLayout();
            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            agentName.Text = string.Format("{0} {1}", MembershipProvider.Current.LoggedinUser.FirstName, MembershipProvider.Current.LoggedinUser.LastName);
            loungeTitle.Text = MembershipProvider.Current.SelectedLounge.LoungeName;
            loungeDescription.Text = MembershipProvider.Current.LoggedinUser.AirportName;

            loungeImage.Image = null;
            var airline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
            if (airline != null)
            {
                LoadLoungeImage(airline);

                //if (airline.AirlineLogoBytes != null && airline.AirlineLogoBytes.Length > 0)
                //{
                //    loungeImage.Image = airline.AirlineLogoBytes.ToImage();
                //}
                //else
                //{
                //    ImageAdapter.LoadImage(airline.ImageHandle, (imageBytes) =>
                //    {
                //        var image = imageBytes.ToImage();
                //        InvokeOnMainThread(() =>
                //        {
                //            loungeImage.Image = image;
                //        });
                //    });
                //}
            }
        }

        private void LoadLoungeImage(Airline usersAirline)
        {
            AppData.usersAirLine = new AirlineCellData(usersAirline);

            if (AppData.LoungeImage == null)
            {
                if (usersAirline.AirlineLogoBytes != null && usersAirline.AirlineLogoBytes.Length > 0)
                    loungeImage.Image = usersAirline.AirlineLogoBytes.ToImage();
                else
                {
                    ImageAdapter.LoadImage(
                        usersAirline.ImageHandle,
                        (imageResultBytes) =>
                        {
                            var uiImage = imageResultBytes.ToImage();
                            InvokeOnMainThread(() =>
                            {
                                loungeImage.Image = uiImage;
                            });
                        }, true);
                }
            }
            else
            {
                InvokeOnMainThread(() =>
                {
                    loungeImage.Image = AppData.LoungeImage;
                });
            }
        }


        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            //UpdateLayout();

            prgOccupancy.LoadProgressData();
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            //prgOccupancy.Hidden = true;
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            UpdateLayout();


            prgOccupancy.RefreshProgress();
        }

        private void UpdateLayout()
        {

            linedView.SetNeedsDisplay();

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            nfloat statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            if (View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            if (topButton != null)
            {
                topButton.Frame = new CGRect(0f, statusBarH, this.View.Frame.Width, yOffset);
            }

            nfloat scaleFactor = height / 667f;
            //Commenting to support iPhone 5 and 5s
            //if (scaleFactor < 1f)
            //{
            //    scaleFactor = 1f;
            //}

            nfloat margin = 10f;
            nfloat doubleMargin = 20f;


            CGRect viewFrame = UIScreen.MainScreen.Bounds;

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?   0 : (float)View.SafeAreaInsets.Left;

            DownScanViewModule.UpdateScanViewLayout(this.View.Frame, bottomView, leftDown,
                rightDown, scanButton, null, leftMargin);

            //-----------scroll view -----------------
            homeScrollView.Frame = new CGRect(0, yOffset + statusBarH, viewFrame.Width,
                viewFrame.Height - statusBarH - bottomView.Frame.Height);

            nfloat agentViewH = 110f * scaleFactor;
            nfloat loungeViewH = 110f * scaleFactor;
            nfloat occupancyViewH = 65f * scaleFactor;
            nfloat languageViewH = 60f * scaleFactor;
            nfloat settingViewH = 70f * scaleFactor;
            nfloat infoViewH = 90f * scaleFactor;



            nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.66f;

            nfloat childXOffset = (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f);
    
            nfloat scrollHeight = this.View.Frame.Height - bottomView.Frame.Height - statusBarH + yOffset;

            nfloat scollAbsoluteHeight = scrollHeight;

            //landscape
            if (UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height)
            {
                scollAbsoluteHeight = this.View.Frame.Width - bottomView.Frame.Height - statusBarH -
                    this.TabBarController.TabBar.Frame.Size.Height + yOffset;
            }

            nfloat tempmarg = 6f + AppDelegate.AirlinesView.topPanelHeight / scollAbsoluteHeight;

            //AppDelegate.CurrentDevice == DeviceType.Tablet
            if (UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height)
            {
                agentViewH = scollAbsoluteHeight / tempmarg;
                loungeViewH = scollAbsoluteHeight / tempmarg;
                occupancyViewH = scollAbsoluteHeight / tempmarg;
                languageViewH = scollAbsoluteHeight / tempmarg;
                settingViewH = scollAbsoluteHeight / tempmarg;
                infoViewH = scollAbsoluteHeight / tempmarg;
            }

            if (AppDelegate.IsDeveloperModeEnabled)
            {
                homeScrollView.ContentSize = new CoreGraphics.CGSize(viewFrame.Width,
                    agentViewH + loungeViewH + occupancyViewH + languageViewH + infoViewH + (50f * scaleFactor) + yOffset);
            }
            else
            {
                homeScrollView.ContentSize = new CoreGraphics.CGSize(viewFrame.Width,
                    agentViewH + loungeViewH + occupancyViewH + languageViewH + settingViewH + infoViewH + (50f * scaleFactor) + yOffset);
            }

            //------------- agent view ----------------------
            agentView.Frame = new CGRect(0f, 0f, homeScrollView.ContentSize.Width, agentViewH);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                agentView.Frame = new CGRect(childXOffset, 0f, childWidth, agentViewH);
            }

            nfloat imgSize = (agentView.Frame.Height - 20f) * 0.8f;
            agentImg.Frame = new CGRect(margin + leftMargin, agentView.Frame.Height / 2f - imgSize / 2f, imgSize, imgSize);

            nfloat agentLabelsX = agentImg.Frame.X + agentImg.Frame.Width + margin;
            agentName.Frame = new CGRect(agentLabelsX, agentImg.Frame.Y,
                homeScrollView.ContentSize.Width - agentLabelsX - margin,
                agentImg.Frame.Height / 2f);

            loungeAgentLabel.Frame = new CGRect(agentLabelsX, agentName.Frame.Y + agentName.Frame.Height,
                agentName.Frame.Width, agentName.Frame.Height);

            //---------------lounge view ------------------
            loungeView.Frame = new CGRect(0f, agentView.Frame.Y + agentView.Frame.Height,
                homeScrollView.ContentSize.Width, loungeViewH);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                loungeView.Frame = new CGRect(childXOffset, agentView.Frame.Y + agentView.Frame.Height,
                    childWidth, loungeViewH);
            }

            nfloat loungeimgSize = (loungeView.Frame.Height - 20f) * 0.8f;
            loungeImage.Frame = new CGRect(margin + leftMargin, loungeView.Frame.Height / 2f - loungeimgSize / 2f,
                loungeimgSize, loungeimgSize);

            nfloat loungeLabelsX = loungeImage.Frame.X + loungeImage.Frame.Width + margin;
            loungeTitle.Frame = new CGRect(loungeLabelsX, loungeImage.Frame.Y,
                homeScrollView.ContentSize.Width - loungeLabelsX - margin,
                loungeImage.Frame.Height / 2f);

            loungeDescription.Frame = new CGRect(loungeLabelsX, loungeTitle.Frame.Y + loungeTitle.Frame.Height,
                loungeTitle.Frame.Width, loungeTitle.Frame.Height);

            //------------ Occupancy view ------------------
            //occupancyView.Frame = new CGRect(0f, loungeView.Frame.Y + loungeView.Frame.Height,
            //    homeScrollView.ContentSize.Width, occupancyViewH);
            //if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            //{
            //    occupancyView.Frame = new CGRect(childXOffset, loungeView.Frame.Y + loungeView.Frame.Height,
            //        childWidth, occupancyViewH);
            //}

            //prgOccupancy.Frame = new CGRect(margin, margin, occupancyView.Frame.Width - (margin * 2), occupancyView.Frame.Height - (margin * 2));

            occupancyView.Frame = new CGRect(0f, loungeView.Frame.Y + loungeView.Frame.Height,
                homeScrollView.ContentSize.Width, occupancyViewH);

            nfloat settingwidth;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                occupancyView.Frame = new CGRect(childXOffset, loungeView.Frame.Y + loungeView.Frame.Height,
                    childWidth, occupancyViewH);
                settingwidth = occupancyView.Frame.Width / 2;
            }
            else
            {
                settingwidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width / 2 : UIScreen.MainScreen.Bounds.Height / 2;
            }

            nfloat settingYFactor = 3f;
            nfloat settingHeight = occupancyView.Frame.Height / settingYFactor;
            nfloat settingY = occupancyView.Frame.Height / 2f - settingHeight / 2f;

            lblOccupancy.Frame = new CGRect(margin + leftMargin,
                0f,
                settingwidth,
                settingHeight);

            //prgOccupancy.Frame = new CGRect(margin,
            //  settingY + lblOccupancy.Frame.Height/2,
            //  occupancyView.Frame.Width,
            //  lblOccupancy.Frame.Height);

            prgOccupancy.IsAutoUpdateProgress = true;

            prgOccupancy.Frame = new CGRect(margin + leftMargin,
                  settingHeight + 15f,
                  occupancyView.Frame.Width - (3 * margin),
                  CustomProgressControl.MinSize);//lblOccupancy.Frame.Height

            //TODO: check
            prgOccupancy.BackgroundColor = UIColor.White;
            occupancyView.BackgroundColor = UIColor.White;


            //TODO: now shift the other controls below it to down..

            //------------ language view ------------------
            languageView.Frame = new CGRect(0f, occupancyView.Frame.Y + occupancyView.Frame.Height,
                homeScrollView.ContentSize.Width, languageViewH);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                languageView.Frame = new CGRect(childXOffset, occupancyView.Frame.Y + occupancyView.Frame.Height,
                    childWidth, languageViewH);
            }

            languageLabel.Frame = new CGRect(margin + leftMargin, margin, languageView.Frame.Width, languageView.Frame.Height - margin * 2);


            //--------------tab setting------------------------
            settingView.Frame = new CGRect(0f, languageView.Frame.Y + languageView.Frame.Height,
                homeScrollView.ContentSize.Width, settingViewH);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                settingView.Frame = new CGRect(childXOffset, languageView.Frame.Y + languageView.Frame.Height,
                    childWidth, settingViewH);
                settingwidth = settingView.Frame.Width * 0.35f;
            }
            else
            {
                settingwidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.35f : UIScreen.MainScreen.Bounds.Height * 0.35f;
            }

            settingYFactor = 3f;
            settingHeight = settingView.Frame.Height / settingYFactor;

            settingY = settingView.Frame.Height / 2f - settingHeight / 2f;

            settingButton.Frame = new CGRect(settingView.Frame.Width - margin - settingwidth,
                settingY,
                settingwidth,
                settingHeight);

            nfloat settingImgSize = settingButton.Frame.Height - 4f;
            settingImg.Frame = new CGRect(settingButton.Frame.X + (settingButton.Frame.Width / 2f) - settingImgSize / 2f,
                settingButton.Frame.Y + 2f, settingImgSize, settingImgSize);

            settingsLabel.Frame = new CGRect(margin + leftMargin,
                settingButton.Frame.Y,
                settingView.Frame.Width - ((margin * 2f) * scaleFactor) - settingButton.Frame.Width,
                settingButton.Frame.Height);


            //---------------info view--------------------

            if (AppDelegate.IsDeveloperModeEnabled)
            {
                infoView.Frame = new CGRect(0f, languageView.Frame.Bottom,
                    homeScrollView.ContentSize.Width, infoViewH);
            }
            else
            {
                infoView.Frame = new CGRect(0f, settingView.Frame.Bottom,
                    homeScrollView.ContentSize.Width, infoViewH);
            }

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                infoView.Frame = new CGRect(childXOffset, settingView.Frame.Y + settingView.Frame.Height,
                    childWidth, infoViewH);
            }


            IEGAmericaLabel.Frame = new CGRect(margin + leftMargin, margin, infoView.Frame.Width,
                (infoView.Frame.Height - margin) / 2f);
            //(infoView.Frame.Width / 2f) - (margin * 2)
            nfloat policyLabelH = infoView.Frame.Height - margin * 2f;
            termsButton.Frame = new CGRect(margin + leftMargin, policyLabelH,
                termsButton.TitleLabel.IntrinsicContentSize.Width, infoView.Frame.Height - policyLabelH - 2f);

            nfloat privacyWidth = privacyButton.TitleLabel.IntrinsicContentSize.Width;
            privacyButton.Frame = new CGRect(infoView.Frame.Width - margin - privacyWidth,
                termsButton.Frame.Y,
                privacyWidth, termsButton.Frame.Height);

            //--------Scroll view content size -----------------
            //homeScrollView.ContentSize = new CoreGraphics.CGSize(viewFrame.Width,
            //    agentView.Frame.Height + loungeView.Frame.Height + languageView.Frame.Height +
            //    settingView.Frame.Height + infoView.Frame.Height + 5f);


            //-------------- language dropdown--------------------------
            nfloat dropImgHeight = settingButton.Frame.Height - dropDown.imageYOffset - 4f;

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                dropDown.updateFramePosition((float)settingButton.Frame.X + childXOffset,
                (float)languageView.Frame.Y + settingButton.Frame.Y,
                settingButton.Frame.Width, settingButton.Frame.Height,
                new CGPoint(dropImgHeight * 1.4f, dropImgHeight));
            }
            else
            {
                dropDown.updateFramePosition((float)settingButton.Frame.X,
                (float)languageView.Frame.Y + settingButton.Frame.Y,
                settingButton.Frame.Width, settingButton.Frame.Height,
                new CGPoint(dropImgHeight * 1.4f, dropImgHeight));
            }


            updateLoginLayout(scaleFactor);

            linedView.Frame = new CGRect(0f, agentView.Frame.Y,
                UIScreen.MainScreen.Bounds.Width, infoView.Frame.Bottom);

            if (AppDelegate.IsDeveloperModeEnabled)
            {
                linedView.LinesY = new nfloat[6] { agentView.Frame.Y, loungeView.Frame.Y,  occupancyView.Frame.Y,
                    languageView.Frame.Y, infoView.Frame.Y, infoView.Frame.Bottom};
            }
            else
            {
                linedView.LinesY = new nfloat[7] { agentView.Frame.Y, loungeView.Frame.Y,
                    occupancyView.Frame.Y, languageView.Frame.Y,
                    settingView.Frame.Y, infoView.Frame.Y, infoView.Frame.Bottom};
            }


        }

        private void initLoginView()
        {
            backgroundViewLogin = new UIView(UIScreen.MainScreen.Bounds);
            backgroundViewLogin.BackgroundColor = new UIColor(0.3f, 0.3f, 0.3f, 0.3f);

            backgroundButton = new UIButton(backgroundViewLogin.Frame);
            backgroundButton.BackgroundColor = UIColor.Clear;
            backgroundButton.SetTitle("", UIControlState.Normal);
            backgroundButton.TouchUpInside += backgroundClick;

            loginRect = new CGRect(UIScreen.MainScreen.Bounds.Width * 0.1f,
                UIScreen.MainScreen.Bounds.Height * 0.2f,
                UIScreen.MainScreen.Bounds.Width * 0.8f,
                220f);
            loginView = new RoundedRectView(loginRect);
            loginView.BackgroundColor = UIColor.White;
            loginView.FCornerRadius = 10f;


            loginLogo = new UIImageView(new CGRect(5f, 5f, 40f, 40f));
            loginLogo.Image = UIImage.FromFile("warning_gray.png");

            loginTitle = new UILabel(new CGRect(50f, 5f, loginRect.Width - 50f, 40f));
            loginTitle.Text = "Login to enter Developer Mode";
            loginTitle.AdjustsFontSizeToFitWidth = true;

            usernameBackground = new UIView();
            usernameBackground.BackgroundColor = UIColor.LightGray;
            usernameBackground.Layer.CornerRadius = 5f;
            usernameBackground.Layer.BorderColor = UIColor.Black.CGColor;
            usernameBackground.Layer.BorderWidth = 1f;

            loginUsername = new UITextField(new CGRect(5f, 50f, loginRect.Width - 10f, 44f));
            loginUsername.Placeholder = "UserName";
            loginUsername.BackgroundColor = UIColor.LightGray;
            loginUsername.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            loginUsername.AutocorrectionType = UITextAutocorrectionType.No;

            passwordBackground = new UIView();
            passwordBackground.BackgroundColor = UIColor.LightGray;
            passwordBackground.Layer.CornerRadius = 5f;
            passwordBackground.Layer.BorderColor = UIColor.Black.CGColor;
            passwordBackground.Layer.BorderWidth = 1f;

            loginPassword = new UITextField(new CGRect(5f, 104f, loginRect.Width - 10f, 44f));
            loginPassword.Placeholder = "Password";
            loginPassword.BackgroundColor = UIColor.LightGray;
            loginPassword.SecureTextEntry = true;
            loginPassword.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            loginPassword.AutocorrectionType = UITextAutocorrectionType.No;

            loginCancel = new UIButton(new CGRect(5f, 160f,
                (loginRect.Width - 30f) / 2f, 50f));

            loginCancel.BackgroundColor = UIColor.Gray;
            loginCancel.SetTitle("Cancel", UIControlState.Normal);
            loginCancel.Layer.CornerRadius = 4f;
            loginCancel.TouchUpInside += cancelLogin;
            loginCancel.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f); ;
            loginCancel.Layer.BorderWidth = 1f;
            loginCancel.Layer.CornerRadius = 5f;
            loginCancel.TitleLabel.AdjustsFontSizeToFitWidth = true;

            loginLogin = new UIButton(new CGRect(
                loginCancel.Frame.Width + loginCancel.Frame.X + 10f
                , 160f,
                (loginRect.Width - 30f) / 2f, 50f));

            loginLogin.BackgroundColor = UIColor.Gray;
            loginLogin.SetTitle("login", UIControlState.Normal);
            loginLogin.Layer.CornerRadius = 4f;
            loginLogin.TouchUpInside += confirmLogin;
            loginLogin.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            loginLogin.Layer.BorderWidth = 1f;
            loginLogin.Layer.CornerRadius = 5f;
            loginLogin.TitleLabel.AdjustsFontSizeToFitWidth = true;

            usernameBackground.AddSubview(loginUsername);
            passwordBackground.AddSubview(loginPassword);

            loginView.AddSubviews(loginLogo, loginLogin, loginTitle, usernameBackground,
                passwordBackground, loginCancel, loginLogin);

            backgroundViewLogin.AddSubview(backgroundButton);
            backgroundViewLogin.AddSubview(loginView);

            AppDelegate.TextProvider.OnLanguageChanged += DevLoginLanguageChanged;
            DevLoginLanguageChanged();
        }

        private void DevLoginLanguageChanged(Language? language = null)
        {
            loginUsername.AccessibilityHint = AppDelegate.TextProvider.GetText(2002);
            loginPassword.AccessibilityHint = AppDelegate.TextProvider.GetText(2003);
            loginLogin.SetTitle(AppDelegate.TextProvider.GetText(2005), UIControlState.Normal);
            loginCancel.SetTitle(AppDelegate.TextProvider.GetText(2004), UIControlState.Normal);

            loginTitle.Text = AppDelegate.TextProvider.GetText(2165);
        }

        //TODO adjust size by scale factor
        private void updateLoginLayout(nfloat scaleFactor)
        {
            backgroundViewLogin.Frame = new CGRect(0f, 0f, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height);

            float topMargin = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height ? 0.2f : 0.3f;
            float margin = 5f;
            var leftMargin = backgroundViewLogin.Frame.Width * 0.2f;
            if (AppDelegate.CurrentDevice.Type == DeviceType.Tablet)
            {
                if (UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height)
                    leftMargin = backgroundViewLogin.Frame.Width * 0.3f;//landscape
                else leftMargin = backgroundViewLogin.Frame.Width * 0.2f;//portrate
            }
            loginRect = new CGRect(leftMargin,
                backgroundViewLogin.Frame.Height * topMargin,
                backgroundViewLogin.Frame.Width - (leftMargin * 2),
                220f);

            loginView.Frame = loginRect;
            loginLogo.Frame = new CGRect(5f, 5f, 40f, 40f);
            loginTitle.Frame = new CGRect(50f, 5f, loginRect.Width - 50f - margin, 40f);


            usernameBackground.Frame = new CGRect(5f, 50f, loginRect.Width - 10f, 44f);
            loginUsername.Frame = new CGRect(10f, 0f, usernameBackground.Frame.Width - 20f, usernameBackground.Frame.Height);

            passwordBackground.Frame = new CGRect(5f, 104f, loginRect.Width - 10f, 44f);
            loginPassword.Frame = new CGRect(10f, 0f, passwordBackground.Frame.Width - 20f, passwordBackground.Frame.Height);


            loginCancel.Frame = new CGRect(5f, 160f,
                (loginRect.Width - 30f) / 2f, 50f);
            loginLogin.Frame = new CGRect(
                loginCancel.Frame.Width + loginCancel.Frame.X + 10f
                , 160f,
                (loginRect.Width - 30f) / 2f, 50f);
        }

        partial void devModeButton(UIButton sender)
        {
            this.Add(backgroundViewLogin);

#if DEBUG
            loginUsername.Text = "dev";
            var date = string.Format("{0}{1}{2}dev", DateTime.UtcNow.Month, DateTime.UtcNow.Day, DateTime.UtcNow.Year);
            date = date.Replace("/", "").Replace("-", "").Replace("0", "").Replace(" ", "");//2016/07/15-> 715216
            loginPassword.Text = date;
#endif

            this.TabBarController.TabBar.UserInteractionEnabled = false;
        }

        public void cancelLogin(object sender, EventArgs ea)
        {
            backgroundViewLogin.RemoveFromSuperview();
            loginUsername.Text = "";
            loginPassword.Text = "";
            this.TabBarController.TabBar.UserInteractionEnabled = true;
        }

        public void confirmLogin(object sender, EventArgs ea)
        {
            if (string.IsNullOrWhiteSpace(loginUsername.Text) || string.IsNullOrEmpty(loginPassword.Text))
                AIMSMessage(AppDelegate.TextProvider.GetText(2047), AppDelegate.TextProvider.GetText(1001));
            else
            {
                ShowLoading(AppDelegate.TextProvider.GetText(2502));
                MembershipProvider.Current.ValidateDeveloper(loginUsername.Text, loginPassword.Text, DefaultMobileApplication.Current.Position, (ss, result) =>
                {
                    if (result.Item1)
                    {
                        AppDelegate.IsDeveloperModeEnabled = true;
                        AppData.devMode = true;

                        InvokeOnMainThread(() =>
                        {
                            settingView.Hidden = true;
                            settingsLabel.Hidden = true;
                            settingButton.Hidden = true;
                            settingImg.Hidden = true;
                            backgroundViewLogin.RemoveFromSuperview();
                            AgentMenuTabController temp = this.TabBarController as AgentMenuTabController;
                            temp.setDevMode(true);

                            UpdateLayout();
                            linedView.SetNeedsDisplay();
                        });
                    }
                    else
                    {
                        string errorMessage = AppDelegate.TextProvider.GetText((int)result.Item2);
                        InvokeOnMainThread(() =>
                        {
                            LogsRepository.AddWarning("Developer login failed",
                                string.Format(
                                    "A user with username '{0}' failed to authenticate as developer with username '{1}',\nError Code: {2}\nError Message: {3}",
                                    MembershipProvider.Current.LoggedinUser.Username, loginUsername.Text, result.Item2,
                                    errorMessage));
                        });
                        AIMSMessage(AppDelegate.TextProvider.GetText(2047), errorMessage);
                    }
                    HideLoading();
                    InvokeOnMainThread(() =>
                    {
                        this.TabBarController.TabBar.UserInteractionEnabled = true;
                    });
                });
            }


        }

        public void backgroundClick(object sender, EventArgs e)
        {
            cancelLogin(backgroundButton, EventArgs.Empty);

        }

        public void termsButtonClick(object sender, EventArgs e)
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("https://www.google.ca/intl/en/policies/terms/regional.html"));
        }

        public void privacyButtonClick(object sender, EventArgs e)
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("https://www.google.ca/intl/en/policies/privacy/"));
        }

        protected void HandleLeftSwipe(UISwipeGestureRecognizer recognizer)
        {
            //(this.TabBarController as AgentMenuTabController).fromSwipe = true;
            //var point = recognizer.LocationInView(this.View);
            UITabSwipe.HandleLeftSwipe(/*recognizer,*/ this);
        }


    }

}