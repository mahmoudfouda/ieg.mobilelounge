using System;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class AgentMenuTabController : UITabBarController
    {
        UIViewController[] uiviewListDev;
        UIViewController[] uiViewListNonDev;

        //public bool fromSwipe = false;
        private nint previousSelectedIndex = 0;
        private nint tempSelectedIndex = 0;
        UIView MoreView;

        //CGPoint startLocation, stopLocation;
        private LoadingOverlay loader;

        public bool IsLoading { get; private set; }

        //AirLineViewController vc;
        public AgentMenuTabController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            try
            {
                var titles = new string[]
                {
               AppDelegate.TextProvider.GetText(2129),
               AppDelegate.TextProvider.GetText(2041),
               AppDelegate.TextProvider.GetText(2008), //Last Hour
               AppDelegate.TextProvider.GetText(2011),
               AppDelegate.TextProvider.GetText(2040),
               AppDelegate.TextProvider.GetText(2050)
            };

                for (int i = 0; i < ViewControllers.Length; i++)
                {
                    if (ViewControllers[i].TabBarItem != null)
                        ViewControllers[i].TabBarItem.Title = titles[i];
                }
            }
            catch { }

            uiviewListDev = this.ViewControllers;
            uiViewListNonDev = new List<UIViewController>(uiviewListDev).GetRange(0, uiviewListDev.Length - 2).ToArray();

            this.ViewControllerSelected += handleTabBarClicked;

            //vc = Storyboard.InstantiateViewController("airLineViewController") as AirLineViewController;
            //GET appDataDevMode
            setDevMode(AppData.devMode);

            //this.ViewControllers[1] = Storyboard.InstantiateViewController("agentTabMessageView") as AgentTabMessageView;
            this.MoreNavigationController.ProvidesPresentationContextTransitionStyle = true;
            this.MoreNavigationController.DefinesPresentationContext = true;
            this.MoreNavigationController.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
            this.MoreNavigationController.View.BackgroundColor = UIColor.Clear;
            this.MoreNavigationController.NavigationBarHidden = true;

            //View.AddGestureRecognizer(new UIPanGestureRecognizer(HandlePanGuestureRecognizer));

            MoreView = this.MoreNavigationController.View;
            //UIScreen.MainScreen.Bounds
            MoreView.Frame = new CoreGraphics.CGRect(MoreView.Frame.X,
                AppDelegate.AirlinesView.topPanelHeight + AppDelegate.AirlinesView.topY,
                this.View.Frame.Width, this.View.Frame.Height);
            //this.MoreNavigationController.
        }


        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            //MoreView.Frame = new CoreGraphics.CGRect(MoreView.Frame.X,
            //    AppDelegate.AirlinesView.topH + AppDelegate.AirlinesView.topY,
            //    this.View.Frame.Width, this.View.Frame.Height);
        }

        public void setDevMode(bool mode)
        {
            if (mode)
            {
                this.SetViewControllers(uiviewListDev, false);
            }
            else
            {
                this.SetViewControllers(uiViewListNonDev, false);
            }

            //this.SetViewControllers(uiviewListDev, false);
        }

        public void goToAirLineViewController()
        {
            UIApplication.SharedApplication.SetStatusBarHidden(false, false);
            AppDelegate.AirlinesView.ShowSearchButton();
            this.DismissModalViewController(true);
            //this.PresentViewController(vc, true, null);
        }

        public void handleTabBarClicked(object sender, UITabBarSelectionEventArgs e)
        {
            //fromSwipe = false;

            if (SelectedIndex > 4)
            {
                //this.MoreNavigationController.SetNavigationBarHidden(true, false);
                //MoreView = this.MoreNavigationController.View;
            }

        }

        public void scanClicked(object sender, EventArgs e)
        {
            if (AppDelegate.ActiveCamera == AIMS.CameraType.ExternalCamera) return;
            this.DismissViewController(true, () =>
            {
                AppDelegate.AirlinesView.Scan();
            });
        }

        //call this after enabling the dev mode
        public void setTabBarLang(string[] texts)
        {
            for (int i = 0; i < this.TabBar.Items.Length; i++)
            {
                this.TabBar.Items[i].Title = texts[i];
            }
        }

        public override void ItemSelected(UITabBar tabbar, UITabBarItem item)
        {

            for (int i = 0; i < tabbar.Items.Length; i++)
            {
                if (item == tabbar.Items[i])
                {
                    tempSelectedIndex = i;
                    break;
                }
            }


            //base.ItemSelected(tabbar, item);
        }

        public void ShowLoading(string text = "")
        {
            IsLoading = true;
            InvokeOnMainThread(() =>
            {
                try
                {
                    if (loader == null)
                        loader = new LoadingOverlay(UIScreen.MainScreen.Bounds, AppDelegate.TextProvider.GetText(2001)/*"Loading.."*/);
                    loader.Frame = new CoreGraphics.CGRect(
                        UIScreen.MainScreen.Bounds.X,
                        UIScreen.MainScreen.Bounds.Y,
                        UIScreen.MainScreen.Bounds.Width,
                        UIScreen.MainScreen.Bounds.Height
                        );
                    loader.loadingLabel.Text = text;
                    this.View.Add(loader);
                }
                catch (Exception exx)
                {
                    var m = exx.Message;
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(exx);
                }
            });
        }

        public void HideLoading()
        {
            if (!IsLoading)
                return;

            InvokeOnMainThread(() =>
            {
                try
                {
                    loader.RemoveFromSuperview();
                }
                catch (Exception exx)
                {
                    var m = exx.Message;
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(exx);
                }
                finally
                {
                    IsLoading = false;
                }
            });
        }

        //public void HandlePanGuestureRecognizer(UIPanGestureRecognizer gestureRecognizer)
        //{
        //    if (gestureRecognizer.State == UIGestureRecognizerState.Ended)
        //    {
        //        var location = gestureRecognizer.TranslationInView(this.View);
        //        if (Math.Abs((float)location.X) > Math.Abs((float)location.Y))//Horizontal
        //        {
        //            if (location.X < 0)//Left
        //            {
        //                System.Console.WriteLine("SwipedLeft: {0}", location.X);
        //            }
        //            else if (location.X > 0)//Right
        //            {
        //                System.Console.WriteLine("SwipedRight: {0}", location.X);
        //            }
        //        }
        //        else//Vertical
        //        {
        //            if (location.Y < 0)//Up
        //            {
        //                System.Console.WriteLine("SwipedUp: {0}", location.Y);
        //            }
        //            else if (location.Y > 0)//Down
        //            {
        //                System.Console.WriteLine("SwipedDown: {0}", location.Y);
        //            }
        //        }
        //    }
        //}

        //public void HandleSwipeGuestureRecognizer(UIGestureRecognizer gestureRecognizer)
        //{
        //    var view = this.View;
        //    if (gestureRecognizer.State == UIGestureRecognizerState.Began)
        //    {
        //        startLocation = gestureRecognizer.LocationInView(view);
        //    }
        //    else if (gestureRecognizer.State == UIGestureRecognizerState.Ended)
        //    {
        //        stopLocation = gestureRecognizer.LocationInView(view);
        //        nfloat dX = stopLocation.X - startLocation.X;//+Left    -Right
        //        nfloat dY = stopLocation.Y - startLocation.Y;//+Up      -Down
        //        if (Math.Abs((float)dX) > Math.Abs((float)dY))
        //        {
        //            if (dX > 0)
        //            {
        //                System.Console.WriteLine("SwipedLeft: {0}", dX);
        //            }
        //            else if (dX < 0)
        //            {
        //                System.Console.WriteLine("SwipedRight: {0}", dX);
        //            }
        //        }
        //        else
        //        {
        //            if (dY > 0)
        //            {
        //                System.Console.WriteLine("SwipedUp: {0}", dY);
        //            }
        //            else if (dY < 0)
        //            {
        //                System.Console.WriteLine("SwipedDown: {0}", dY);
        //            }
        //        }
        //    }
        //}
    }
}