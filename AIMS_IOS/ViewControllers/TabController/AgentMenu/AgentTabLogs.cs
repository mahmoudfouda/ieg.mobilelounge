using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;
using AIMS.Models;
using System.Linq;
using AIMS;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public partial class AgentTabLogs : UIViewController
    {
//        UIImageView bottomBackground;

        List<AgentTabLogsCellData> items = new List<AgentTabLogsCellData>();

        public nfloat yOffset;
        public UIButton topButton;

        public UIView whiteBackground;

        public nfloat ScaleFactor { get; private set; }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() => {
                scanButton.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                scanButton.SetTitle(AppDelegate.Current.LineaDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras
                //scanButton.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);
            });
        }

        public AgentTabLogs(IntPtr handle) : base(handle)
        {
        }

        private void LoadLogs()
        {
            AppManager.getAgentTabLogs((repository) => {
                InvokeOnMainThread(() => {
                    if (repository == null) return;

                    items = ((IEnumerable<Log>)repository).Select(x => new AgentTabLogsCellData(x)).ToList();
                    AgentTableLog.Source = new AgentTabLogsSource(items, this);
                    updateTableLayout(ScaleFactor);
                });
            });
        }
        
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            //if (true)//???
            //{
            yOffset = AppDelegate.AirlinesView.topPanelHeight;
                topButton = new UIButton();
                topButton.TitleLabel.Text = "";
                topButton.BackgroundColor = UIColor.Clear;

                topButton.TouchUpInside += downButton;
                this.View.Add(topButton);
            //}

            whiteBackground = new UIView();
            whiteBackground.BackgroundColor = UIColor.White;
            this.View.AddSubview(whiteBackground);
            this.View.SendSubviewToBack(whiteBackground);

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;
            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                scanButton.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
            };

            //updateTableLayout();

            //bottomBackground = new UIImageView(UIImage.FromFile("gradiant.png"));
            //bottomView.AddSubview(bottomBackground);
            //bottomView.SendSubviewToBack(bottomBackground);

            
            leftDown.TouchUpInside += downButton;
            rightDown.TouchUpInside += downButton;
            scanButton.TouchUpInside += (this.TabBarController as AgentMenuTabController).scanClicked;

            var swipeLeft = new UISwipeGestureRecognizer(HandleLeftSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Left };
            View.AddGestureRecognizer(swipeLeft);

            var swipeRight = new UISwipeGestureRecognizer(HandleRightSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Right };
            View.AddGestureRecognizer(swipeRight);

            scanButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            scanButton.Layer.BorderWidth = 1f;

            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            LoadTexts();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            updateLayout();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            updateLayout();
            LoadLogs();//Must be after updateLayout()
        }

        public void updateTableLayout(nfloat scaleFactor)//Must be after updateLayout()
        {
            //ratio based on iphone 6 minimum of 50f
            nfloat estimatedHeight = 50f * scaleFactor;
            estimatedHeight = estimatedHeight < 50f ? 50f : estimatedHeight;

            AgentTableLog.EstimatedRowHeight = estimatedHeight;
            AgentTableLog.RowHeight = estimatedHeight;
        }

        public void updateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;
            float statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            if (View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ScaleFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                ScaleFactor = height / (667f * (4f / 3f));
            }

            if (topButton != null)
            {
                topButton.Frame = new CGRect(0f, statusBarH, this.View.Frame.Width, yOffset);
            }

            DownScanViewModule.UpdateScanViewLayout(this.View.Frame, bottomView, leftDown,
                rightDown, scanButton, null, 0);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.66f;

                nfloat childXOffset = (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f);

                //AgentTableLog.Frame = new CGRect(childXOffset, statusBarH + yOffset,
                //childWidth, frame.Height - statusBarH - yOffset - bottomView.Frame.Height);
            }
            else
            {
                //AgentTableLog.Frame = new CGRect(0f, statusBarH + yOffset,
                //frame.Width, frame.Height - statusBarH - yOffset - bottomView.Frame.Height);
            }

            AgentTableLog.Frame = new CGRect(10f, statusBarH + yOffset + 10f,
                frame.Width - 20f, frame.Height - statusBarH - yOffset - bottomView.Frame.Height);

            whiteBackground.Frame = new CGRect(0f, topButton.Frame.Bottom,
                UIScreen.MainScreen.Bounds.Width, this.View.Frame.Height);



        }

        protected void HandleLeftSwipe(UISwipeGestureRecognizer recognizer)
        {
            UITabSwipe.HandleLeftSwipe(/*recognizer,*/ this);
        }

        protected void HandleRightSwipe(UISwipeGestureRecognizer recognizer)
        {
            UITabSwipe.HandleRightSwipe(/*recognizer,*/ this);
        }

        public void downButton(object send, EventArgs e)
        {
            AgentMenuTabController temp = this.TabBarController as AgentMenuTabController;

            temp.goToAirLineViewController();
        }
    }

    public class AgentTabLogsSource : UITableViewSource
    {

        List<AgentTabLogsCellData> items;
        protected NSString cellIdentifier = (NSString)"logCell";

        public AgentTabLogs ParentViewController { get; private set; }

        public AgentTabLogsSource(List<AgentTabLogsCellData> items, AgentTabLogs viewController)
        {
            this.items = items;
            this.ParentViewController = viewController;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            var cell = tableView.DequeueReusableCell(cellIdentifier) as LogCell;

            cell.updateLayout(tableView.RowHeight, ParentViewController);

            cell.updateCell(items[indexPath.Row].title, items[indexPath.Row].content, items[indexPath.Row].localTime);

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return items.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            new UIAlertView(items[indexPath.Row].title, "[" + items[indexPath.Row].content + "] " + items[indexPath.Row].localTime, null, "Ok", null).Show();

            tableView.DeselectRow(indexPath, true);
        }

    }

    public class AgentTabLogsCellData
    {
        public Log log;
        public string title;
        public string content;
        public string time;
        public string localTime;

        //public AgentTabLogsCellData(string title, string content, string time)
        //{
        //    this.title = title;
        //    this.content = content;
        //    this.time = time;
        //}

        public AgentTabLogsCellData(Log log)
        {
            this.log = log;
            this.title = log.Title;
            this.content = log.Description;
            this.time = string.Format("{0:yyyy/MM/dd - HH:mm:ss (FFF)}", log.Time);
            this.localTime = string.Format("{0:yyyy/MM/dd - HH:mm:ss (FFF)}", log.LocalTime);
        }
    }
}