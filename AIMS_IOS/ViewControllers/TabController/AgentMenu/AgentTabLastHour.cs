using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;
using System.Linq;
using AIMS.Models;
using AIMS;
using Ieg.Mobile.Localization;
using System.Threading.Tasks;

namespace AIMS_IOS
{
    public partial class AgentTabLastHour : UIViewController
    {
        UITapGestureRecognizer tap;
        public nfloat yOffset = 0f;
        public nfloat ScaleFactor { get; private set; }
        public bool IsShowingFilter { get; private set; }

        //UIImageView bottomBackground;
        bool detailView = false;

        public List<AgentLastHourCellData> items = new List<AgentLastHourCellData>();

        private bool devMode;

        private bool failMode = false;
        private UIButton devButton;

        public UIButton topButton;

        private UIButton imgFilter;

        private UITableView tableViewFilter;

        private string failImg = "Passenger/passenger_red.png";
        private string normalImg = "Passenger/passenger_blue.png";

        //private UIView validationView;
        //private bool confirmEventAdded = false;

        List<AgentLastHourCellData> filteredItems = new List<AgentLastHourCellData>();
        List<LasHourFilterCellData> filterCellDatas = new List<LasHourFilterCellData>();

        private UIRefreshControl refreshControl;
        private int currentPage = 1;
        private bool loadMore;
        private LastHourFilterCell currentCell;

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() =>
            {
                scanButton.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                scanButton.SetTitle(AppDelegate.Current.LineaDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras
                //scanButton.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);

                this.TabBarItem.Title = AppDelegate.TextProvider.GetText(2008); //2010
                searchBar.Placeholder = AppDelegate.TextProvider.GetText(2144);
            });
        }

        public AgentTabLastHour(IntPtr handle) : base(handle)
        {
        }

        private void LoadLastSearch()
        {
            currentPage = 1;
            SearchUsingFilter();
        }

        private async Task LoadPassengers()
        {
            if (failMode)
                AppManager.getAgentFailedLastHour((repository) =>
                {
                    InvokeOnMainThread(() =>
                    {
                        if (repository != null)
                        {
                            items = ((IEnumerable<Passenger>)repository).Select(x => new AgentLastHourCellData(x, true)).ToList();
                        }
                        else items = new List<AgentLastHourCellData>();

                        lastHourTableView.Source = new AgentTabLastHourSource(items, this);

                        ((AgentTabLastHourSource)lastHourTableView.Source).LoadMoreEvent += TableSource_LoadMoreEvent;

                        updateTableLayout();
                    });
                });
            else
                AppManager.getAgentLastHour((repository) =>
                {
                    InvokeOnMainThread(() =>
                    {
                        if (repository != null)
                        {
                            var passengers = PassengersRepository.GroupPassengers((IEnumerable<Passenger>)repository);
                            items = passengers.Select(x => new AgentLastHourCellData(x, false)).ToList();
                        }
                        else items = new List<AgentLastHourCellData>();

                        lastHourTableView.Source = new AgentTabLastHourSource(items, this);

                        ((AgentTabLastHourSource)lastHourTableView.Source).LoadMoreEvent += TableSource_LoadMoreEvent;

                        updateTableLayout();
                    });
                });
        }

        private void SearchPassengers(string keyword)
        {
            AppManager.searchAgentLastHour(keyword, (repository) =>
            {
                InvokeOnMainThread(() =>
                {
                    if (repository != null)
                    {
                        items = ((IEnumerable<Passenger>)repository).Select(x => new AgentLastHourCellData(x, false)).ToList();
                    }
                    else items = new List<AgentLastHourCellData>();

                    lastHourTableView.Source = new AgentTabLastHourSource(items, this);

                    ((AgentTabLastHourSource)lastHourTableView.Source).LoadMoreEvent += TableSource_LoadMoreEvent;

                    updateTableLayout();
                });
            });
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
            {
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;
                searchBar.SearchBarStyle = UISearchBarStyle.Default;
                searchBar.SearchTextField.BackgroundColor = UIColor.White;
            }

            //if (true)//???
            //{
            yOffset = AppDelegate.AirlinesView.topPanelHeight;
            topButton = new UIButton();
            topButton.TitleLabel.Text = "";
            topButton.BackgroundColor = UIColor.Clear;

            topButton.TouchUpInside += downButton;
            this.View.Add(topButton);
            //}

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;
            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                scanButton.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
            };

            UISearchBarUtility.removeBackground(searchBar);

            searchBar.Layer.CornerRadius = 5f;
            searchBar.Layer.BorderColor = UIColor.Black.CGColor;
            searchBar.Layer.BorderWidth = 1f;

            imgFilter = new UIButton();
            imgFilter.SetImage(UIImage.FromFile("filter.png"), UIControlState.Normal);
            imgFilter.TouchDown += ImgFilter_TouchDown;


            tableViewFilter = new UITableView();
            filterCellDatas.Clear();
            filterCellDatas.Add(new LasHourFilterCellData());
            tableViewFilter.RegisterClassForCellReuse(typeof(LastHourFilterCell), "lastHourFilterCell");
            tableViewFilter.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            tableViewFilter.Hidden = true;
            this.View.AddSubview(tableViewFilter);
            this.researchView.AddSubview(imgFilter);

            updateTableLayout();

            //bottomBackground = new UIImageView(UIImage.FromFile("gradiant.png"));
            //bottomView.AddSubview(bottomBackground);
            //bottomView.SendSubviewToBack(bottomBackground);

            leftDown.TouchUpInside += downButton;
            rightDown.TouchUpInside += downButton;
            scanButton.TouchUpInside += (this.TabBarController as AgentMenuTabController).scanClicked;

            searchBar.TextChanged += searchTextChanged;

            searchBar.SearchButtonClicked += disMisKeyB;

            updateLayout();

            searchTextChanged(null, null);

            var swipeLeft = new UISwipeGestureRecognizer(HandleLeftSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Left };
            View.AddGestureRecognizer(swipeLeft);

            var swipeRight = new UISwipeGestureRecognizer(HandleRightSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Right };
            View.AddGestureRecognizer(swipeRight);

            tap = new UITapGestureRecognizer((textfiled) =>
            {
                UIView temp = AutoScrollUtility.findFirstResponder(this.View);

                if (temp != null)
                {
                    temp.ResignFirstResponder();
                }
            });
            tap.CancelsTouchesInView = false;
            this.View.AddGestureRecognizer(tap);

            scanButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            scanButton.Layer.BorderWidth = 1f;

            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            refreshControl = new UIRefreshControl();
            refreshControl.ValueChanged += RefreshControl_ValueChanged;
            lastHourTableView.RefreshControl = refreshControl;


            LoadTexts();

        }

        private void ImgFilter_TouchDown(object sender, EventArgs e)
        {
            if (IsShowingFilter)
            {
                tableViewFilter.Hidden = true;
                ((AgentTabLastHourFilterSource)tableViewFilter.Source).Items.Clear();
                tableViewFilter.ReloadData();
                IsShowingFilter = false;
            }
            else
            {
                if (tableViewFilter.Source == null)
                {
                    var tableSource = new AgentTabLastHourFilterSource(filterCellDatas, this);
                    tableViewFilter.Source = tableSource;
                    tableSource.SearchCliked += TableSource_SearchCliked;
                }
                else
                {
                    ((AgentTabLastHourFilterSource)tableViewFilter.Source).Items.Add(new LasHourFilterCellData());
                }

                tableViewFilter.Hidden = false;
                tableViewFilter.ReloadData();
                IsShowingFilter = true;
            }
        }

        private void TableSource_LoadMoreEvent(int pageIndex)
        {
            if (currentCell == null)
                return;

            try
            {

                AppManager.searchAgentLastHour(searchBar.Text, currentCell.AirportSearch, currentCell.SelectedDate, pageIndex, true, (repository) =>
                {
                    InvokeOnMainThread(() =>
                    {
                        if (repository != null)
                        {
                            items = ((IEnumerable<Passenger>)repository).Select(x => new AgentLastHourCellData(x, false)).ToList();
                        }
                        else items = new List<AgentLastHourCellData>();

                        // var oldItems = ((AgentTabLastHourSource)lastHourTableView.Source).Items;

                        //oldItems.AddRange(items.ToList());

                        //lastHourTableView.Source = new AgentTabLastHourSource(oldItems, this, pageIndex);

                        ((AgentTabLastHourSource)lastHourTableView.Source).Items.AddRange(items);

                        ((AgentTabLastHourSource)lastHourTableView.Source).IsFetching = false;

                        updateTableLayout();
                    });
                });
            }
            catch (Exception ef)
            {

            }
        }

        private void TableSource_SearchCliked(object sender, EventArgs e)
        {
            currentCell = (LastHourFilterCell)sender;

            IsShowingFilter = true;
            ImgFilter_TouchDown(sender, e);

            SearchUsingFilter();
        }

        private void SearchUsingFilter()
        {
            AppManager.searchAgentLastHour(searchBar.Text, currentCell.AirportSearch, currentCell.SelectedDate, currentPage, loadMore, (repository) =>
            {
                InvokeOnMainThread(() =>
                {
                    if (repository != null)
                    {
                        items = ((IEnumerable<Passenger>)repository).Select(x => new AgentLastHourCellData(x, false)).ToList();
                    }
                    else items = new List<AgentLastHourCellData>();

                    lastHourTableView.Source = new AgentTabLastHourSource(items, this);

                    ((AgentTabLastHourSource)lastHourTableView.Source).LoadMoreEvent += TableSource_LoadMoreEvent;

                    updateTableLayout();
                });
            });
        }

        private void RefreshControl_ValueChanged(object sender, EventArgs e)
        {
            BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    if (currentCell != null)
                        LoadLastSearch();
                    else
                        await LoadPassengers();

                    if (refreshControl != null)
                        refreshControl.EndRefreshing();
                }
                catch
                {
                }
            });
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            scanButton.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;
            devMode = AppData.devMode;
            AppData.fromAirline = false;
            LoadPassengers();
            updateLayout();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            updateLayout();
            LoadPassengers();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            updateLayout();
        }

        public void disMisKeyB(object sender, EventArgs e)
        {
            searchBar.ResignFirstResponder();
        }

        public void disMisKeyB()
        {
            searchBar.ResignFirstResponder();
        }

        protected void HandleLeftSwipe(UISwipeGestureRecognizer recognizer)
        {
            if (!detailView)
                UITabSwipe.HandleLeftSwipe(/*recognizer,*/ this);
        }

        protected void HandleRightSwipe(UISwipeGestureRecognizer recognizer)
        {
            if (!detailView)
                UITabSwipe.HandleRightSwipe(/*recognizer,*/ this);
        }

        public void searchTextChanged(object sender, UISearchBarTextChangedEventArgs e)
        {
            if (currentCell != null)
                currentCell = null;

            string text = "";
            if (e != null)
            {
                text = e.SearchText.Trim();

                if (text.Length == 0)
                {
                    LoadPassengers();
                }
                else
                {
                    SearchPassengers(text);
                }
            }
        }

        public void updateTableLayout()
        {
            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            //ratio based on iphone 6 minimum of 50f
            nfloat estimatedHeight = 50f / 650f * height;
            estimatedHeight = estimatedHeight < 50f ? 50f : estimatedHeight;

            lastHourTableView.EstimatedRowHeight = estimatedHeight;
            lastHourTableView.RowHeight = estimatedHeight;


            tableViewFilter.RowHeight = 200;
            tableViewFilter.EstimatedRowHeight = 200;
        }

        public void updateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;
            float statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            if (View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            if (topButton != null)
            {
                topButton.Frame = new CGRect(0f, statusBarH, this.View.Frame.Width, yOffset);
            }

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
             0 : (float)View.SafeAreaInsets.Left;


            DownScanViewModule.UpdateScanViewLayout(this.View.Frame, bottomView, leftDown,
                rightDown, scanButton, null, leftMargin);

            nfloat screenHeight = (UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width);

            ScaleFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                ScaleFactor = screenHeight / 667f;
            }

            //-----------------------searchView-----------------------
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.66f;

                nfloat childXOffset = (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f);

                researchView.Frame = new CGRect(0f, statusBarH + yOffset, frame.Width, 54f);
                searchBar.Frame = new CGRect(15f + leftMargin, 5f, researchView.Frame.Width - 30f - 40f, 44f);

                imgFilter.Frame = new CGRect(searchBar.Frame.Right + 5f, searchBar.Frame.Top, 44f, 44f);
            }
            else
            {
                researchView.Frame = new CGRect(0f, statusBarH + yOffset, frame.Width, 49f);
                searchBar.Frame = new CGRect(15f + leftMargin, 5f, researchView.Frame.Width - 30f - leftMargin - 40f, 44f);

                imgFilter.Frame = new CGRect(searchBar.Frame.Right + 5f, searchBar.Frame.Top, 44f, 44f);

            }

            //researchView.Frame = new CGRect(0f, statusBarH + yOffset, frame.Width, 44f);

            tableViewFilter.Frame = new CGRect(researchView.Frame.X, researchView.Frame.Bottom,
                researchView.Frame.Width, frame.Height - researchView.Frame.Bottom - bottomView.Frame.Height - scanButton.Frame.Height - 5f);


            //-------------table view----------------------
            lastHourTableView.Frame = new CGRect(researchView.Frame.X, researchView.Frame.Bottom,
                researchView.Frame.Width, frame.Height - researchView.Frame.Bottom - bottomView.Frame.Height);

            if (devMode)
                updateDevLayout();
        }

        private void updateDevLayout()
        {
            if (devButton == null)
                initDevMode();

            devButton.Frame = new CGRect(searchBar.Frame.Width - 85f, 12f, 40f, 25f);
        }

        private void initDevMode()
        {
            devButton = new UIButton(new CGRect(searchBar.Frame.Width - 75f, 12f, 40f, 25f));
            devButton.SetTitle("", UIControlState.Normal);
            devButton.SetImage(UIImage.FromFile(failImg), UIControlState.Normal);

            devButton.BackgroundColor = UIColor.Clear;//new UIColor(0.85f,0.85f,0.85f,1f);
            devButton.Layer.CornerRadius = 2f;

            devButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            devButton.TouchUpInside += failButton;

            researchView.AddSubview(devButton);
            updateDevLayout();
        }

        public void downButton(object send, EventArgs e)
        {
            AgentMenuTabController temp = this.TabBarController as AgentMenuTabController;

            temp.goToAirLineViewController();
        }

        public void failButton(object sender, EventArgs e)//the initial state of button must be red (for them to click and load the faild ones if they want)
        {
            failMode = !failMode;

            if (failMode)//Button must Show : Switch to normal mode (green)
            {
                devButton.SetImage(UIImage.FromFile(normalImg), UIControlState.Normal);
            }
            else//Button must Show : Switch to failed mode (red)
            {
                devButton.SetImage(UIImage.FromFile(failImg), UIControlState.Normal);
            }

            LoadPassengers();//searchTextChanged(null, null);

        }

        public void onItemSelected(int row)
        {
            try
            {
                if (MembershipProvider.Current == null || MembershipProvider.Current.IsTokenExpired)
                {
                    AppDelegate.ReturnToLoginView();
                    return;
                }


                if (AppDelegate.GuestValidationResultView.IsShown)
                {
                    AppDelegate.GuestValidationResultView.DismissViewController(false, null);
                }

                if (AppDelegate.ValidationView.IsShown)
                {
                    AppDelegate.ValidationView.DismissViewController(false, null);
                }

                if (PresentedViewController != AppDelegate.ValidationView)
                {
                    AppDelegate.ValidationView.ProvidesPresentationContextTransitionStyle = true;
                    AppDelegate.ValidationView.DefinesPresentationContext = true;
                    AppDelegate.ValidationView.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
                    this.PresentViewController(AppDelegate.ValidationView, true, null);
                }
            }
            catch (Exception ex)
            {
                var m = ex.Message;
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }
        }
    }

    public class AgentTabLastHourSource : UITableViewSource
    {
         protected NSString cellIdentifier = (NSString)"lastHourCells";
        private AgentTabLastHour vc;

        public event LoadMoreHandler LoadMoreEvent;

        private int pageIndex = 1;
        private bool isFetching;
        private const int TotalPages = 1000;


        public AgentTabLastHourSource(List<AgentLastHourCellData> items, AgentTabLastHour vc)
        {
            this.Items = items;
            this.vc = vc;
        }

        public AgentTabLastHourSource(List<AgentLastHourCellData> items, AgentTabLastHour vc, int pageIndex) : this(items, vc)
        {
            this.pageIndex = pageIndex;
        }

        public List<AgentLastHourCellData> Items { get; set; }

        public bool IsFetching
        {
            get
            {
                return isFetching;
            }
            set
            {
                isFetching = true;
            }
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var item = Items[indexPath.Row];

            var cell = tableView.DequeueReusableCell(cellIdentifier) as lastHourCell;

            cell.UpdateLayout(tableView.RowHeight, UIScreen.MainScreen.Bounds.Width, vc.ScaleFactor);

            cell.UpdateCell(item);
            cell.SetButtonActions(tableView, indexPath, item, (lastHourPassenger) =>
            {
                AppDelegate.Current.AIMSConfirm("Delete?", AppDelegate.TextProvider.GetText(2520), (ss, ee) =>
                {
                    if (MembershipProvider.Current == null ||
                        lastHourPassenger == null ||
                        tableView == null) return;

                    //TODO: Delete Last Hour Passenger (serverCall)

                    var newIndex = this.Items.IndexOf(lastHourPassenger);
                    indexPath = NSIndexPath.FromRowSection(newIndex, 0);

                    this.Items.Remove(lastHourPassenger);

                    tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
                });
            },
            (lastHour) =>
            {
                AppManager.retrieveMainPassengerOf((isSucceeded, errorCode, errorMetadata, message, passenger) =>
                {
                    if (!isSucceeded)
                    {
                        //TODO: show message
                        return;
                    }

                    InvokeOnMainThread(() =>
                    {
                        MembershipProvider.Current.SelectedPassenger = passenger;
                        vc.disMisKeyB();
                        vc.onItemSelected(indexPath.Row);

                        tableView.DeselectRow(indexPath, true);
                    });
                }, lastHour.passenger);
            });

            if (!this.isFetching && this.pageIndex < TotalPages && indexPath.Row > this.Items.Count * 0.8)
            {
                this.isFetching = true;
                Task.Factory.StartNew(LoadMore);
            }

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Items.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            var selectedPassengerDataCell = Items[indexPath.Row];
            if (!selectedPassengerDataCell.fail)
            {
                MembershipProvider.Current.SelectedPassenger = selectedPassengerDataCell.passenger;
                vc.disMisKeyB();
                vc.onItemSelected(indexPath.Row);

                tableView.DeselectRow(indexPath, true);
            }
            else
            {
                string title = "", text = "";
                if (string.IsNullOrEmpty(selectedPassengerDataCell.passenger.FailedReason))
                {
                    title = AppDelegate.TextProvider.GetText(2045);
                    text = selectedPassengerDataCell.passenger.BarcodeString;
                }
                else
                {
                    var newLineIndex = selectedPassengerDataCell.passenger.FailedReason.IndexOf("\n");
                    if (newLineIndex == -1)
                    {
                        title = selectedPassengerDataCell.passenger.FailedReason;
                        text = selectedPassengerDataCell.passenger.BarcodeString;
                    }
                    else
                    {
                        title = selectedPassengerDataCell.passenger.FailedReason.Substring(0, newLineIndex);
                        text = string.Format("{0}\n\n{1}\n", selectedPassengerDataCell.passenger.FailedReason.Substring(newLineIndex + 1),
                            !string.IsNullOrWhiteSpace(selectedPassengerDataCell.passenger.BarcodeString) ?
                            string.Format("Barcode: {0}", selectedPassengerDataCell.passenger.BarcodeString) :
                            string.Format("Manually entered\nFull Name: {0}\nFlight: [{1}-{2}-{3}]\nFFN: {4}",
                            selectedPassengerDataCell.passenger.FullName, selectedPassengerDataCell.passenger.FlightCarrier,
                            selectedPassengerDataCell.passenger.FlightNumber, selectedPassengerDataCell.passenger.TrackingClassOfService,
                            selectedPassengerDataCell.passenger.FFN));
                    }
                }
                AppDelegate.Current.AIMSMessage(title, text);
            }
        }

        private async void LoadMore()
        {
            InvokeOnMainThread(() =>
            {
                this.pageIndex++;
                LoadMoreEvent?.Invoke(pageIndex);
            });
        }
    }

    public delegate void LoadMoreHandler(int pageIndex);

    public class AgentLastHourCellData
    {
        public Passenger passenger;
        public bool fail = false;

        public AgentLastHourCellData(Passenger passenger, bool? isFailed = null)
        {
            this.passenger = passenger;
            if (string.IsNullOrEmpty(passenger.FailedReason))
            {
                this.fail = isFailed.HasValue ? isFailed.Value : false;
            }
            else
            {
                this.fail = isFailed.HasValue ? isFailed.Value : true;
            }
        }
    }

}