﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("AgentTabDevSettingViewController")]
    partial class AgentTabDevSettingViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel advancedSettingsCameraTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel advancedSettingsLogsTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel advancedSettingsPVStatisticsLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel advancedSettingsPVThresholdLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel advancedSettingsPVUserLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentTabBottomView bottomView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView cameraImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton cameraSwitchButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton clearLogsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView devScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel failedScansLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton leftDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel logsLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentDevSettingTabLogsView logsView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel maxHoursIntervalLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel refreshIntervalLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton rightDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton scanButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel scannerLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentDevSettingTabScanView scanView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel sessionTimeoutLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentDevSettingTabStatisticsView statisticsView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel successScansLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentDevSettingTabThresholdView thresholdView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFailedScanThreshold { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtStatisticsMaxHoursInterval { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtStatisticsRefreshInterval { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtSuccessScanThreshold { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtUsersSessionTimeout { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton uploadLogsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentDevSettingTabUsersView userSettingsView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (advancedSettingsCameraTitle != null) {
                advancedSettingsCameraTitle.Dispose ();
                advancedSettingsCameraTitle = null;
            }

            if (advancedSettingsLogsTitle != null) {
                advancedSettingsLogsTitle.Dispose ();
                advancedSettingsLogsTitle = null;
            }

            if (advancedSettingsPVStatisticsLabel != null) {
                advancedSettingsPVStatisticsLabel.Dispose ();
                advancedSettingsPVStatisticsLabel = null;
            }

            if (advancedSettingsPVThresholdLabel != null) {
                advancedSettingsPVThresholdLabel.Dispose ();
                advancedSettingsPVThresholdLabel = null;
            }

            if (advancedSettingsPVUserLabel != null) {
                advancedSettingsPVUserLabel.Dispose ();
                advancedSettingsPVUserLabel = null;
            }

            if (bottomView != null) {
                bottomView.Dispose ();
                bottomView = null;
            }

            if (cameraImage != null) {
                cameraImage.Dispose ();
                cameraImage = null;
            }

            if (cameraSwitchButton != null) {
                cameraSwitchButton.Dispose ();
                cameraSwitchButton = null;
            }

            if (clearLogsButton != null) {
                clearLogsButton.Dispose ();
                clearLogsButton = null;
            }

            if (devScrollView != null) {
                devScrollView.Dispose ();
                devScrollView = null;
            }

            if (failedScansLabel != null) {
                failedScansLabel.Dispose ();
                failedScansLabel = null;
            }

            if (leftDown != null) {
                leftDown.Dispose ();
                leftDown = null;
            }

            if (logsLabel != null) {
                logsLabel.Dispose ();
                logsLabel = null;
            }

            if (logsView != null) {
                logsView.Dispose ();
                logsView = null;
            }

            if (maxHoursIntervalLabel != null) {
                maxHoursIntervalLabel.Dispose ();
                maxHoursIntervalLabel = null;
            }

            if (refreshIntervalLabel != null) {
                refreshIntervalLabel.Dispose ();
                refreshIntervalLabel = null;
            }

            if (rightDown != null) {
                rightDown.Dispose ();
                rightDown = null;
            }

            if (scanButton != null) {
                scanButton.Dispose ();
                scanButton = null;
            }

            if (scannerLabel != null) {
                scannerLabel.Dispose ();
                scannerLabel = null;
            }

            if (scanView != null) {
                scanView.Dispose ();
                scanView = null;
            }

            if (sessionTimeoutLabel != null) {
                sessionTimeoutLabel.Dispose ();
                sessionTimeoutLabel = null;
            }

            if (statisticsView != null) {
                statisticsView.Dispose ();
                statisticsView = null;
            }

            if (successScansLabel != null) {
                successScansLabel.Dispose ();
                successScansLabel = null;
            }

            if (thresholdView != null) {
                thresholdView.Dispose ();
                thresholdView = null;
            }

            if (txtFailedScanThreshold != null) {
                txtFailedScanThreshold.Dispose ();
                txtFailedScanThreshold = null;
            }

            if (txtStatisticsMaxHoursInterval != null) {
                txtStatisticsMaxHoursInterval.Dispose ();
                txtStatisticsMaxHoursInterval = null;
            }

            if (txtStatisticsRefreshInterval != null) {
                txtStatisticsRefreshInterval.Dispose ();
                txtStatisticsRefreshInterval = null;
            }

            if (txtSuccessScanThreshold != null) {
                txtSuccessScanThreshold.Dispose ();
                txtSuccessScanThreshold = null;
            }

            if (txtUsersSessionTimeout != null) {
                txtUsersSessionTimeout.Dispose ();
                txtUsersSessionTimeout = null;
            }

            if (uploadLogsButton != null) {
                uploadLogsButton.Dispose ();
                uploadLogsButton = null;
            }

            if (userSettingsView != null) {
                userSettingsView.Dispose ();
                userSettingsView = null;
            }
        }
    }
}