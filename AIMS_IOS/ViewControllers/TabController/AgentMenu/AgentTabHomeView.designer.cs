﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("AgentTabHomeView")]
    partial class AgentTabHomeView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView agentImg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel agentName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentHomeTabAgentView agentView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentTabBottomView bottomView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentTabScrollView homeScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel IEGAmericaLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentHomeTabInfoView infoView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel languageLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentHomeTabLanguageView languageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblOccupancy { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton leftDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel loungeAgentLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel loungeDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView loungeImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel loungeTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentHomeTabLoungeView loungeView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView occupancyView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.CustomProgressControl prgOccupancy { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.PrivacyButton privacyButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton rightDown { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton scanButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton settingButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView settingImg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel settingsLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.AgentHomeTabSettingView settingView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.TermsButton termsButton { get; set; }

        [Action ("devModeButton:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void devModeButton (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (agentImg != null) {
                agentImg.Dispose ();
                agentImg = null;
            }

            if (agentName != null) {
                agentName.Dispose ();
                agentName = null;
            }

            if (agentView != null) {
                agentView.Dispose ();
                agentView = null;
            }

            if (bottomView != null) {
                bottomView.Dispose ();
                bottomView = null;
            }

            if (homeScrollView != null) {
                homeScrollView.Dispose ();
                homeScrollView = null;
            }

            if (IEGAmericaLabel != null) {
                IEGAmericaLabel.Dispose ();
                IEGAmericaLabel = null;
            }

            if (infoView != null) {
                infoView.Dispose ();
                infoView = null;
            }

            if (languageLabel != null) {
                languageLabel.Dispose ();
                languageLabel = null;
            }

            if (languageView != null) {
                languageView.Dispose ();
                languageView = null;
            }

            if (lblOccupancy != null) {
                lblOccupancy.Dispose ();
                lblOccupancy = null;
            }

            if (leftDown != null) {
                leftDown.Dispose ();
                leftDown = null;
            }

            if (loungeAgentLabel != null) {
                loungeAgentLabel.Dispose ();
                loungeAgentLabel = null;
            }

            if (loungeDescription != null) {
                loungeDescription.Dispose ();
                loungeDescription = null;
            }

            if (loungeImage != null) {
                loungeImage.Dispose ();
                loungeImage = null;
            }

            if (loungeTitle != null) {
                loungeTitle.Dispose ();
                loungeTitle = null;
            }

            if (loungeView != null) {
                loungeView.Dispose ();
                loungeView = null;
            }

            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (occupancyView != null) {
                occupancyView.Dispose ();
                occupancyView = null;
            }

            if (prgOccupancy != null) {
                prgOccupancy.Dispose ();
                prgOccupancy = null;
            }

            if (privacyButton != null) {
                privacyButton.Dispose ();
                privacyButton = null;
            }

            if (rightDown != null) {
                rightDown.Dispose ();
                rightDown = null;
            }

            if (scanButton != null) {
                scanButton.Dispose ();
                scanButton = null;
            }

            if (settingButton != null) {
                settingButton.Dispose ();
                settingButton = null;
            }

            if (settingImg != null) {
                settingImg.Dispose ();
                settingImg = null;
            }

            if (settingsLabel != null) {
                settingsLabel.Dispose ();
                settingsLabel = null;
            }

            if (settingView != null) {
                settingView.Dispose ();
                settingView = null;
            }

            if (termsButton != null) {
                termsButton.Dispose ();
                termsButton = null;
            }
        }
    }
}