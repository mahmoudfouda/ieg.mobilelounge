﻿using System;
using UIKit;
using AIMS.Models;
using AIMS;

namespace AIMS_IOS
{
    public partial class ManualPassengerTabController : UITabBarController, IPassengerRefresher
    {
        PassengerTabViewController passengerVC;

        public bool IsGuestMode { get; set; } = false;
        
        public Passenger SelectedPassenger
        {
            get
            {
                if (MembershipProvider.Current != null)
                {
                    if (IsGuestMode)
                        return MembershipProvider.Current.SelectedGuest;
                    else return MembershipProvider.Current.SelectedPassenger;
                }
                return null;
            }
            set
            {
                if (MembershipProvider.Current != null)
                {
                    if (IsGuestMode)
                        MembershipProvider.Current.SelectedGuest = value;
                    else MembershipProvider.Current.SelectedPassenger = value;
                }
            }
        }

        public ManualPassengerTabController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            passengerVC = this.ViewControllers[0] as PassengerTabViewController;
        }
        
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            passengerVC.resetAllGradient();
        }
        
        public void Close()
        {
            if(this.SelectedIndex == 1)
            {
                this.SelectedViewController = this.ViewControllers[0];
                this.SelectedIndex = 0;
            }

            //this.DismissModalViewController(true);
            this.DismissViewController(true, () =>
            {
            });
            if (AppDelegate.ContinueScanning && !IsGuestMode)
                AppDelegate.AirlinesView.Scan();
            else IsGuestMode = false;
        }

        public void setTabBarLang(string[] texts)
        {
            for (int i = 0; i < this.TabBar.Items.Length; i++)
            {
                this.TabBar.Items[i].Title = texts[i];
            }
        }

        public void RefreshPassenger()
        {
            var vc = this.SelectedViewController as IPassengerRefresher;
            if (vc != null)
                vc.RefreshPassenger();
        }
    }
}