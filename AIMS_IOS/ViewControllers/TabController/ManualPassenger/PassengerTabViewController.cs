﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UIKit;
using CoreGraphics;
using AIMS;
using AIMS.Models;
using AIMS_IOS.Utility;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public partial class PassengerTabViewController : AIMSViewController, IPassengerRefresher
    {
        NSObject keyboardShowNotification, keyboardHideNotification;
        //private bool hasSlashInside = true;
        //private CGRect keyboardSize;
        private nfloat textFieldYpos = 0f;
        private nfloat scrollMaxHeight = 500f;
        private nfloat scaleFactor = 1f;

        private nfloat margin = 5f;
        private nfloat doubleMargin = 10f;//margin * 2f;
        private nfloat separatorWidth = 20f;

        const int NAME_MAX_LENGTH = 20;
        const int FFN_MAX_LENGTH = 16;
        const int FLIGHT_CARRIER_MAX_LENGTH = 3;
        const int FLIGHT_NO_MAX_LENGTH = 4;
        const int CLASS_MAX_LENGTH = 1;

        public LinedView linedView;

        public UITapGestureRecognizer tap;
        
        public PassengerTabViewController (IntPtr handle) : base (handle)
        {
        }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(()=> {
                //cancelButton.SetTitle(AppDelegate.TextProvider.GetText(2004), UIControlState.Normal);
                
                passengerTabFullNameLabel.Text = AppDelegate.TextProvider.GetText(2024);// string.Format("{0}: ", App.TextProvider.GetText(2024));//Full Name
                passengerTabFirstName.Placeholder = AppDelegate.TextProvider.GetText(2025);//First
                //passengerTabLastName.Placeholder = AppDelegate.TextProvider.GetText(2026);//Last
                passengerTabLastName.Placeholder = AppDelegate.TextProvider.GetText(2024);//Full Name
                //txtFullName.Placeholder = AppDelegate.TextProvider.GetText(2024);//Full Name
                passengerTabloyaltyLabel.Text = AppDelegate.TextProvider.GetText(2027);// string.Format("{0}: ", App.TextProvider.GetText(2027));//Frequent Flyer Number
                PassengerTabNotesLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2028));//Notes: 
                passengerTabCarrierLabel.Text = AppDelegate.TextProvider.GetText(2036);
                flightTitle.Text = AppDelegate.TextProvider.GetText(2016);
                classTitle.Text = AppDelegate.TextProvider.GetText(2014);
                /*passengerTabCarrierLabel.Text = string.Format("{0} - {1} - {2}",
                    AppDelegate.TextProvider.GetText(2036),
                    AppDelegate.TextProvider.GetText(2016),
                    AppDelegate.TextProvider.GetText(2014)); //Carrier - Flight Number - Class*/
                //scanButtonMI1.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                //scanButtonMI1.SetTitle(IPC.SDK.iOS.BaseDTDevice.SharedDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras
                //scanButtonMI1.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);
            });
        }
        
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            initLinedView();

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;

            //setFonts();

            //----init card view----

            passengerTabAirlineCard.Layer.BorderColor = UIColor.Clear.CGColor;

            //---------


            passengerTabFirstName.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            passengerTabLastName.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            passengerTabLoyaltyText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };

            carrierText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            carrierText.AllEditingEvents += carrierTextChanged;

            flightText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            flightText.AllEditingEvents += flightTextChanged;

            classText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            classText.AllEditingEvents += classTextChanged;

            //PassengerTabNotesText. += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            PassengerTabNotesText.Changed += onNoteTextChanged;

            passengerTabLastName.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
                var length = textField.Text.Length - range.Length + replacementString.Length;
                return length <= NAME_MAX_LENGTH;
            };
            passengerTabLoyaltyText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
                var length = textField.Text.Length - range.Length + replacementString.Length;
                return length <= FFN_MAX_LENGTH;
            };
            carrierText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
                var length = textField.Text.Length - range.Length + replacementString.Length;
                return length <= FLIGHT_CARRIER_MAX_LENGTH;
            };
            flightText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
                var length = textField.Text.Length - range.Length + replacementString.Length;
                return length <= FLIGHT_NO_MAX_LENGTH;
            };
            classText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
                var length = textField.Text.Length - range.Length + replacementString.Length;
                return length <= CLASS_MAX_LENGTH;
            };

            //Did it in UI
            //passengerTabFirstName.AllEditingEvents += (oo,ss) => { passengerTabFirstName.Text = passengerTabFirstName.Text.ToUpper(); };
            //passengerTabLastName.AllEditingEvents += (oo, ss) => { passengerTabLastName.Text = passengerTabLastName.Text.ToUpper(); };
            //PassengerTabNotesText.Changed += (oo,ss) => { PassengerTabNotesText.Text = PassengerTabNotesText.Text.ToUpper(); };

            var swipeLeft = new UISwipeGestureRecognizer(HandleLeftSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Left };
            View.AddGestureRecognizer(swipeLeft);

            tap = new UITapGestureRecognizer((textfiled) =>
            {
                UIView temp = AutoScrollUtility.findFirstResponder(this.View);

                if(temp != null)
                {
                    temp.ResignFirstResponder();
                }
            });

            this.View.AddGestureRecognizer(tap);

            cancelButton.TouchUpInside += onCancel;
            topButton.TouchUpInside += onCancel;
            scanButtonMI1.TouchUpInside += scanClicked;

            //PassengerTabNotesText.Layer.MasksToBounds = true;
            PassengerTabNotesText.Layer.CornerRadius = 4f;
            //PassengerTabNotesText.Layer.BorderColor = new CGColor(0f, 0f, 0f);
            PassengerTabNotesText.Layer.BorderWidth = 1f;

            nextButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            nextButton.Layer.BorderWidth = 1f;
            nextButton.TitleLabel.AdjustsFontSizeToFitWidth = false;

            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                InvokeOnMainThread(() =>
                {
                    scanButtonMI1.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
                });
            };

            scanButtonMI1.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            scanButtonMI1.Layer.BorderWidth = 1f;
            scanButtonMI1.TitleLabel.AdjustsFontSizeToFitWidth = false;

            scanButtonMI1.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            cancelButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            cancelButton.Layer.BorderWidth = 1f;
            cancelButton.TitleLabel.AdjustsFontSizeToFitWidth = false;

            //LoadPassenger();

            topTitle.AdjustsFontSizeToFitWidth = true;
            passengerTabAirlineNameLabel.AdjustsFontSizeToFitWidth = true;
            passengerTabAirlineLabel.AdjustsFontSizeToFitWidth = true;

            passengerTabFullNameLabel.AdjustsFontSizeToFitWidth = true;
            passengerTabLastName.AdjustsFontSizeToFitWidth = true;
            passengerTabFirstName.AdjustsFontSizeToFitWidth = true;

            passengerTabLoyaltyText.AdjustsFontSizeToFitWidth = true;

            //passengerTabCarrierLabel.AdjustsFontSizeToFitWidth = true;
            //classTitle.AdjustsFontSizeToFitWidth = true;
            //flightTitle.AdjustsFontSizeToFitWidth = true;
            
            topAirlineImage.Image = AppData.usersAirLine.image;
			topTitle.Text = MembershipProvider.Current.UserHeaderText;

            keyboardShowNotification = UIKeyboard.Notifications.ObserveWillShow(keyboardWillShow);
            keyboardHideNotification = UIKeyboard.Notifications.ObserveWillHide(keyboardWillHide);

            
        }

        public void initLinedView()
        {
            linedView = new LinedView();

            this.passengerTabScrollView.AddSubview(linedView);
        }

        public void setFonts()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                nfloat xxLargeTexts = 26f * scaleFactor, xLargeTexts = 24f * scaleFactor, largeTexts = 22f * scaleFactor, medium = 17f * scaleFactor, smallTexts = 14f * scaleFactor;
                string /*italicFontName = "HelveticaNeue-Italic",*/ boldFontName = "Helvetica-Bold", regularFontName = "Helvetica Neue";

                var titleFont = UIFont.FromName(regularFontName, xxLargeTexts);
                var xLargeFont = UIFont.FromName(regularFontName, xLargeTexts);
                var xLargeBoldFont = UIFont.FromName(boldFontName, xLargeTexts);
                var largeFont = UIFont.FromName(regularFontName, largeTexts);
                var largeBoldFont = UIFont.FromName(boldFontName, largeTexts);
                var smallFont = UIFont.FromName(regularFontName, smallTexts);
                var mediumFont = UIFont.FromName(regularFontName, medium);
                var mediumBoldFont = UIFont.FromName(boldFontName, medium);

                //top view
                //topTitle.Font = titleFont; //UIFont.FromName("font name", 20f);

                //airline view
                passengerTabAirlineNameLabel.Font = titleFont; //UIFont.FromName("Helvetica-Bold", 24f);

                //card view
                passengerTabAirlineLabel.Font = xLargeFont; //UIFont.FromName("Helvetica Neue", 24f);

                //passenger view
                passengerTabFullNameLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                passengerTabLastName.Font = smallFont; //UIFont.FromName("Helvetica-Bold", 22f);
                passengerTabFirstName.Font = smallFont; //UIFont.FromName("Helvetica-Bold", 22f);

                //carrier view
                passengerTabloyaltyLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                passengerTabLoyaltyText.Font = smallFont; //UIFont.FromName("Helvetica Neue", 22f);

                passengerTabCarrierLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                classTitle.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                flightTitle.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);

                carrierText.Font = smallFont; //UIFont.FromName("font name", 20f);
                flightText.Font = smallFont; //UIFont.FromName("font name", 20f);
                classText.Font = smallFont; //UIFont.FromName("font name", 20f);

                //note view
                PassengerTabNotesLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                PassengerTabNotesText.Font = smallFont; //UIFont.FromName("Helvetica Neue", 22f);

                //buttons
                scanButtonMI1.Font = UIFont.SystemFontOfSize(smallTexts); //UIFont.FromName("Helvetica Neue", 17f);
                cancelButton.Font = UIFont.SystemFontOfSize(medium);
                nextButton.Font = UIFont.SystemFontOfSize(medium);
            }
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            this.passengerTabScrollView.ContentSize = new CoreGraphics.CGSize(UIScreen.MainScreen.Bounds.Width, 700);
            
            updateLayout();

            setFonts();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if (MembershipProvider.Current == null)
                AppDelegate.ReturnToLoginView();
            else
            {
                updateLayout();
                LoadPassenger();
                LoadTexts();
            }
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (MembershipProvider.Current == null)
                AppDelegate.ReturnToLoginView();
            else
            {
                updateLayout();
            }
        }

        private void updateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;

            nfloat statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            scaleFactor = height / 667f;
            //Commenting to support iPhone 5 and 5s
            //if (scaleFactor < 1f)
            //{
            //    scaleFactor = 1f;
            //}

            //--------------top view-------------------------------------
            topView.Frame = new CGRect(0f, statusBarH, frame.Width, 44f * scaleFactor);

            nfloat airlineImgSize = topView.Frame.Height - (margin * 2f);
            topAirlineImage.Frame = new CGRect(margin, margin, airlineImgSize, airlineImgSize);

            topTitle.Frame = new CGRect(topAirlineImage.Frame.Right + margin, topAirlineImage.Frame.Y,
                topView.Frame.Width - (margin * 2f), airlineImgSize);

            topButton.Frame = new CGRect(0f, 0f, topView.Frame.Width, topView.Frame.Height);

            //---------------bottom view-----------------------
            nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.66f;

            nfloat childXOffset = (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f);
            nfloat bottomHeight = 60f * scaleFactor;
            //nfloat buttonsWidth = 60f * scaleFactor;

            bottomView.Frame = new CGRect(0f, this.View.Frame.Height - bottomHeight,
                frame.Width, bottomHeight);

            nfloat buttonsTotal = frame.Width - (40f * scaleFactor);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                buttonsTotal -= (childXOffset * 2f);
            }
            nfloat buttonSlice = buttonsTotal / 6f;

            cancelButton.Frame = new CGRect(10f * scaleFactor, bottomView.Frame.Height * 0.2f,
                buttonSlice, bottomView.Frame.Height * 0.6f);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                cancelButton.Frame = new CGRect(childXOffset + (10f * scaleFactor), bottomView.Frame.Height * 0.2f,
                    buttonSlice, bottomView.Frame.Height * 0.6f);
            }

            scanButtonMI1.Frame = new CGRect(cancelButton.Frame.Right + (10f * scaleFactor),
                cancelButton.Frame.Y,
                buttonSlice * 4f, cancelButton.Frame.Height);

            nextButton.Frame = new CGRect(scanButtonMI1.Frame.Right + (10f * scaleFactor),
                cancelButton.Frame.Y,
                cancelButton.Frame.Width, cancelButton.Frame.Height);

            //-------------scroll view---------------------------------------------
            passengerTabScrollView.Frame = new CGRect(0f, topView.Frame.Bottom,
                this.View.Frame.Width , this.View.Frame.Height - topView.Frame.Bottom - bottomView.Frame.Height);

            passengerTabScrollView.ContentSize = new CGSize(this.View.Frame.Width, scrollMaxHeight * scaleFactor);

            //---------------logo view---------------------------------------
            passengerTabLogoView.Frame = new CGRect(0f, 0f, this.View.Frame.Width, 80f * scaleFactor);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                passengerTabLogoView.Frame = new CGRect(childXOffset, 0f,
                    childWidth, 80f * scaleFactor);
            }

            nfloat airlineBigImgSize = passengerTabLogoView.Frame.Height - (margin * 2f);

            airlineImage.Frame = new CGRect(margin, margin, airlineBigImgSize, airlineBigImgSize);

            passengerTabAirlineNameLabel.Frame = new CGRect(airlineImage.Frame.Right + margin,
                airlineImage.Frame.Height * 0.28f + airlineImage.Frame.Y,
                passengerTabLogoView.Frame.Width - airlineImage.Frame.Right - doubleMargin,
                airlineImage.Frame.Height / 2f);

            //-----------------------card view---------------------------
            passengerTabCardView.Frame = new CGRect(0f, passengerTabLogoView.Frame.Bottom,
                this.View.Frame.Width, 60f * scaleFactor);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                passengerTabCardView.Frame = new CGRect(childXOffset, passengerTabLogoView.Frame.Bottom,
                childWidth, 60f * scaleFactor);
            }

            nfloat cardHeight = passengerTabCardView.Frame.Height - doubleMargin;

            passengerTabAirlineCard.Frame = new CGRect(margin, margin / 2f,
                cardHeight * 1.571f, cardHeight);


            passengerTabAirlineLabel.Frame = new CGRect(passengerTabAirlineCard.Frame.Right + margin,
                passengerTabAirlineCard.Frame.Y + passengerTabAirlineCard.Frame.Height * 0.25f,
                passengerTabCardView.Frame.Width - passengerTabAirlineCard.Frame.Right - doubleMargin,
                passengerTabAirlineCard.Frame.Height / 2f);

            //--------------passengerTabNameView-------------------
            passengerTabNameView.Frame = new CGRect(0f, passengerTabCardView.Frame.Bottom, this.View.Frame.Width,
                80f * scaleFactor);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                passengerTabNameView.Frame = new CGRect(childXOffset, passengerTabCardView.Frame.Bottom,
                    childWidth,
                    80f * scaleFactor);
            }

            nfloat agentImgSize = passengerTabNameView.Frame.Height - 4f;
            agentImage.Frame = new CGRect(2f, 2f, agentImgSize, agentImgSize);
            //full name
            passengerTabFullNameLabel.Frame = new CGRect(agentImage.Frame.Right + margin, agentImage.Frame.Y + margin,
                passengerTabNameView.Frame.Width - agentImage.Frame.Right - (margin * 2f),
                agentImgSize / 2f - doubleMargin);
            
            //separator
            nameSeparator.Frame = new CGRect(
                passengerTabFullNameLabel.Frame.X + (passengerTabFullNameLabel.Frame.Width / 2f) - (separatorWidth / 2f),
                passengerTabFullNameLabel.Frame.Bottom + margin,
                separatorWidth, passengerTabFullNameLabel.Frame.Height);


            //last name
            //if (hasSlashInside)
            //{ //Showing [last name] / [first name]
            //    nameSeparator.Hidden = passengerTabFirstName.Hidden = false;

            //    passengerTabLastName.Frame = new CGRect(passengerTabFullNameLabel.Frame.X,
            //        nameSeparator.Frame.Y,
            //        passengerTabFullNameLabel.Frame.Width / 2f - separatorWidth / 2f,
            //        nameSeparator.Frame.Height);
            //}
            //else
            //{ //Showing [ full   name ]
            nameSeparator.Hidden = passengerTabFirstName.Hidden = true;

            passengerTabLastName.Frame = new CGRect(passengerTabFullNameLabel.Frame.X,
                passengerTabFullNameLabel.Frame.Bottom + margin,
                passengerTabFullNameLabel.Frame.Width,
                passengerTabFullNameLabel.Frame.Height);
            //}

            //first name
            passengerTabFirstName.Frame = new CGRect(
                nameSeparator.Frame.Right, 
                nameSeparator.Frame.Y,
                passengerTabFullNameLabel.Frame.Width / 2f - separatorWidth / 2f,
                nameSeparator.Frame.Height);

            //---------------carrier view-----------------------------
            passengerTabCarrierView.Frame = new CGRect(0f, passengerTabNameView.Frame.Bottom,
                this.View.Frame.Width, 140f * scaleFactor);

            nfloat carrierSizeFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                passengerTabCarrierView.Frame = new CGRect(childXOffset, passengerTabNameView.Frame.Bottom,
                childWidth, 140f * scaleFactor);
                carrierSizeFactor = 1f;
            }

            nfloat planeImgSize = passengerTabCarrierView.Frame.Height / 2f;
            planeImage.Frame = new CGRect(margin, 0f, planeImgSize, planeImgSize);
            //------name section
            passengerTabloyaltyLabel.Frame = new CGRect(planeImage.Frame.Right + margin,
                planeImage.Frame.Y + doubleMargin,
                passengerTabCarrierView.Frame.Width - planeImage.Frame.Right - doubleMargin, 
                planeImgSize * 0.2857f);

            nfloat textfieldHeight = passengerTabloyaltyLabel.Frame.Height * 1.4285f;
            passengerTabLoyaltyText.Frame = new CGRect(passengerTabloyaltyLabel.Frame.X,
                passengerTabloyaltyLabel.Frame.Bottom + 2f, passengerTabloyaltyLabel.Frame.Width,
                textfieldHeight);

            //carrier-flight-class setting
            nfloat titleY = planeImage.Frame.Bottom + margin;
            nfloat titleHeight = passengerTabloyaltyLabel.Frame.Height;
            nfloat titleBottom = titleY + titleHeight;

            //carrier text
            carrierText.Frame = new CGRect(margin, titleBottom + margin,
                (passengerTabCarrierView.Frame.Width - doubleMargin)/ (3.2f * carrierSizeFactor),
                textfieldHeight);
            //carrier title
            passengerTabCarrierLabel.Frame = new CGRect(carrierText.Frame.X, titleY,
                carrierText.Frame.Width,titleHeight);
            //carrier separator
            carrierSeparator.Frame = new CGRect(carrierText.Frame.Right, carrierText.Frame.Y, 
                separatorWidth, textfieldHeight);

            //class text
            nfloat classWidth = (passengerTabCarrierView.Frame.Width - doubleMargin) / (3.2f * carrierSizeFactor);
            classText.Frame = new CGRect((passengerTabCarrierView.Frame.Width / carrierSizeFactor) - classWidth - margin,
                carrierText.Frame.Y, classWidth, carrierText.Frame.Height);
            //class title
            classTitle.Frame = new CGRect(classText.Frame.X, titleY,
                classText.Frame.Width, titleHeight);
            //class separator
            classSeparator.Frame = new CGRect(classText.Frame.X - separatorWidth, classText.Frame.Y,
                separatorWidth, classText.Frame.Height);

            //flight text
            flightText.Frame = new CGRect(carrierSeparator.Frame.Right, classText.Frame.Y,
                classSeparator.Frame.X - carrierSeparator.Frame.Right,
                //passengerTabCarrierView.Frame.Width -
                //(carrierSeparator.Frame.Right + classSeparator.Frame.Width + classText.Frame.Width + doubleMargin),
                classSeparator.Frame.Height);
            //class title
            flightTitle.Frame = new CGRect(flightText.Frame.X, titleY,
                flightText.Frame.Width, titleHeight);



            //----------------note view-----------------------
            passengerTabNotesView.Frame = new CGRect(0f, passengerTabCarrierView.Frame.Bottom,
                this.View.Frame.Width, 110f * scaleFactor);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                passengerTabNotesView.Frame = new CGRect(childXOffset, passengerTabCarrierView.Frame.Bottom,
                    childWidth, 110f * scaleFactor);
            }

            PassengerTabNotesLabel.Frame = new CGRect(margin, margin,
                passengerTabNotesView.Frame.Width - doubleMargin, passengerTabNotesView.Frame.Height * 0.15f);

            PassengerTabNotesText.Frame = new CGRect(margin, PassengerTabNotesLabel.Frame.Bottom + margin,
                PassengerTabNotesLabel.Frame.Width,
                passengerTabNotesView.Frame.Height - PassengerTabNotesLabel.Frame.Height);


            //lined view
            linedView.Frame = new CGRect(0f, passengerTabLogoView.Frame.Y,
                UIScreen.MainScreen.Bounds.Width, passengerTabNotesView.Frame.Bottom);

            linedView.LinesY = new nfloat[6] { passengerTabLogoView.Frame.Y, passengerTabCardView.Frame.Y,
                passengerTabNameView.Frame.Y, passengerTabCarrierView.Frame.Y,
                passengerTabNotesView.Frame.Y, passengerTabNotesView.Frame.Bottom };


        }
        
        /// <summary>
        /// This must be called when these tabs are shown or resumed (after selecting card or returning from scanner)
        /// this will load the passenger info into the boxes
        /// </summary>
        private void LoadPassenger()
        {
            InvokeOnMainThread(() => {
                if (AppDelegate.ManualPassengerView.SelectedPassenger != null)
                {
                    airlineImage.Image = null;
                    passengerTabAirlineNameLabel.Text = "";
                    if (MembershipProvider.Current.SelectedAirline != null)
                    {
                        #region Filling the Airline
                        passengerTabAirlineNameLabel.Text = MembershipProvider.Current.SelectedAirline.Name.ToUpper();
                        if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes != null && MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length > 0)
                        {
                            //airlineImage.Image = AppData.selectedAirLine.image;
                            airlineImage.Image = MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.ToImage();
                        }
                        else
                        {
                            ImageAdapter.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (imageBytes) =>
                            {
                                var image = imageBytes.ToImage();
                                InvokeOnMainThread(() =>
                                {
                                    airlineImage.Image = image;
                                });
                            });
                        } 
                        #endregion
                    }
                    passengerTabAirlineCard.Image = null;
                    passengerTabAirlineLabel.Text = "";
                    if (MembershipProvider.Current.SelectedCard != null)
                    {
                        #region Filling the Card
                        passengerTabAirlineLabel.Text = MembershipProvider.Current.SelectedCard.Name;
                        if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                        {
                            //passengerTabAirlineCard.Image = AppData.selectedCard.image;
                            passengerTabAirlineCard.Image = MembershipProvider.Current.SelectedCard.CardPictureBytes.ToImage();
                        }
                        else
                        {
                            ImageAdapter.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (imageBytes) =>
                            {
                                var image = imageBytes.ToImage();
                                InvokeOnMainThread(() =>
                                {
                                    passengerTabAirlineCard.Image = image;
                                });
                            });
                        } 
                        #endregion
                    }

                    #region Show Passenger first and last name
                    var names = new List<string>();
                    if (AppData.ValidationResult.PassengerNames.Count > 0)
                    {
                        names = AppData.ValidationResult.PassengerNames;
                    }
                    else if (!string.IsNullOrEmpty(AppDelegate.ManualPassengerView.SelectedPassenger.FullName))
                        names.Add(AppDelegate.ManualPassengerView.SelectedPassenger.FullName);
                    
                    var fullNameSB = new StringBuilder();//TODO: refactor and clean
                    passengerTabFirstName.Text = "";
                    //var firstNameSB = new StringBuilder();
                    passengerTabLastName.Text = "";
                    //var lastNameSB = new StringBuilder();

                    //hasSlashInside = names.Count == 0 || (names.Count > 0 && names.Any(x => x.Contains("/")));
                    //foreach (var fullName in names)
                    //{
                    //    if (hasSlashInside)
                    //    { //filling last name / first name
                    //        if (!string.IsNullOrWhiteSpace(fullName))
                    //        {
                    //            var nameParts = fullName.Split(new char[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);

                    //            if (lastNameSB.Length > 0) lastNameSB.Append(";");
                    //            lastNameSB.Append(nameParts.Length > 0 ? nameParts[0] : "");

                    //            if (firstNameSB.Length > 0) firstNameSB.Append(";");
                    //            firstNameSB.Append(nameParts.Length > 1 ? nameParts[1] : "");
                    //        }
                    //    }
                    //    else
                    //    { //filling full name
                    //        if (fullNameSB.Length > 0) fullNameSB.Append(";");
                    //        fullNameSB.Append(fullName);
                    //    }
                    //}

                    if(names.Count > 0)
                        fullNameSB.Append(names.Last());

                    //if (!string.IsNullOrWhiteSpace(AppDelegate.ManualPassengerView.SelectedPassenger.FullName))
                    //{
                    //if (hasSlashInside)
                    //{ //Showing last name / first name
                    //    passengerTabLastName.Text = lastNameSB.ToString();
                    //    passengerTabFirstName.Text = firstNameSB.ToString();
                    //}
                    //else
                    //{
                        passengerTabLastName.Text = fullNameSB.ToString();
                    //}
                    //}
                    //else passengerTabFirstName.Text = passengerTabLastName.Text = "";

                    if (AppData.ValidationResult.IsNameRequired || AppData.ValidationResult.PassengerNames.Count > 0)
                    {
                        GradientUtility.addErrorGradiant(passengerTabFirstName);
                        GradientUtility.addErrorGradiant(passengerTabLastName);
                    }
                    else
                    {
                        GradientUtility.removeGradiants(passengerTabFirstName);
                        GradientUtility.removeGradiants(passengerTabLastName);
                    }
                    #endregion

                    #region Show Passenger FFN
                    if (AppData.ValidationResult.IsFFNRequired)
                    {
                        //passengerTabLoyaltyText.BackgroundColor = UIColor.Yellow;
                        GradientUtility.addErrorGradiant(passengerTabLoyaltyText);
                    }
                    else
                    {
                        GradientUtility.removeGradiants(passengerTabLoyaltyText);
                    }
                    
                    passengerTabLoyaltyText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.FFN;
                    #endregion


                    #region Show Passenger Flight Info
                    if (AppData.ValidationResult.IsFlightInfoRequired)
                    {
                        /*carrierText.BackgroundColor = UIColor.Yellow;
                        flightText.BackgroundColor = UIColor.Yellow;
                        classText.BackgroundColor = UIColor.Yellow;*/

                        GradientUtility.addErrorGradiant(carrierText);
                        GradientUtility.addErrorGradiant(flightText);
                        GradientUtility.addErrorGradiant(classText);

                    }
                    else
                    {
                        GradientUtility.removeGradiants(carrierText);
                        GradientUtility.removeGradiants(flightText);
                        GradientUtility.removeGradiants(classText);
                    }

                    #region TODO: show Passenger Flight Carrier
                    if (string.IsNullOrWhiteSpace(AppDelegate.ManualPassengerView.SelectedPassenger.FlightCarrier))
                    {
                        if (MembershipProvider.Current.SelectedAirline != null)//TODO: (Siavash )????
                        {
                            carrierText.Text = MembershipProvider.Current.SelectedAirline.Code;
                        }
                    }
                    else
                    {
                        carrierText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.FlightCarrier;
                    }
                    #endregion
                    
                    flightText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.FlightNumber;
                    classText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.TrackingClassOfService;
                    #endregion

                    #region Show Passenger Note
                    if (AppData.ValidationResult.IsNotesRequired)
                    {
                        //PassengerTabNotesText.BackgroundColor = UIColor.Yellow;

                        GradientUtility.addErrorGradiant(PassengerTabNotesText);
                    }
                    else
                    {
                        GradientUtility.removeGradiants(PassengerTabNotesText);
                    }

                    PassengerTabNotesText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.Notes;
                    #endregion
                }
            });
        }

        public void resetAllGradient()
        {
            GradientUtility.removeGradiants(passengerTabFirstName);
            GradientUtility.removeGradiants(passengerTabLastName);
            GradientUtility.removeGradiants(passengerTabLoyaltyText);
            GradientUtility.removeGradiants(carrierText);
            GradientUtility.removeGradiants(flightText);
            GradientUtility.removeGradiants(classText);
            GradientUtility.removeGradiants(PassengerTabNotesText);
        }

        /// <summary>
        /// This must be called when the tabs are changing and also when we click on the Confirm(check mark) button
        /// </summary>
        public void SavePassenger()
        {
            InvokeOnMainThread(()=> {
                if (MembershipProvider.Current == null) return;//in case of session timeout (which logs out the user)

                if (AppDelegate.ManualPassengerView.SelectedPassenger == null)//Keeping the passenger object allways instanciated and ready...
                    AppDelegate.ManualPassengerView.SelectedPassenger = new Passenger();

                #region Set the passenger Info (You must have all the TextBoxes)
                //Do not try to understand my logic :DDDDDD
                if (string.IsNullOrWhiteSpace(passengerTabLastName.Text) || string.IsNullOrWhiteSpace(passengerTabFirstName.Text))
                {
                    AppDelegate.ManualPassengerView.SelectedPassenger.FullName = passengerTabLastName.Text.Trim() + passengerTabFirstName.Text.Trim();
                }
                else
                {
                    AppDelegate.ManualPassengerView.SelectedPassenger.FullName = passengerTabLastName.Text.Trim() + "/" + passengerTabFirstName.Text.Trim();
                }

                AppDelegate.ManualPassengerView.SelectedPassenger.FFN = passengerTabLoyaltyText.Text;
                AppDelegate.ManualPassengerView.SelectedPassenger.Notes = PassengerTabNotesText.Text;

                AppDelegate.ManualPassengerView.SelectedPassenger.FlightCarrier = carrierText.Text;
                AppDelegate.ManualPassengerView.SelectedPassenger.FlightNumber = flightText.Text;
                AppDelegate.ManualPassengerView.SelectedPassenger.TrackingClassOfService = classText.Text;
                #endregion
            });
        }

        partial void buttonNext(UIButton sender)
		{
            SavePassenger();
            UITabSwipe.HandleLeftSwipe(/*null,*/ this);
        }
        
        protected void HandleLeftSwipe(UISwipeGestureRecognizer recognizer)
        {
            SavePassenger();
            UITabSwipe.HandleLeftSwipe(/*recognizer,*/ this);
        }

        public void onCancel(object sender, EventArgs e)
        {
            if (areTextEmpty() && this.TabBarController.ViewControllers[1] != null && (this.TabBarController.ViewControllers[1] as ManualFlightTabViewController).areTextEmpty())
            {
                var parentController = this.TabBarController as ManualPassengerTabController;
                parentController.Close();
            }
            else
            {
                AIMSConfirm("", AppDelegate.TextProvider.GetText(2519), (ss, ee) =>
                {
                    InvokeOnMainThread(() =>
                    {
                        var parentController = this.TabBarController as ManualPassengerTabController;
                        parentController.Close();
                    });
                });
            }
        }

        public void scanClicked(object sender, EventArgs e)
        {
            SavePassenger();
            if (AppDelegate.ActiveCamera == CameraType.ExternalCamera) return;
            this.DismissViewController(true, () =>
            {
                AppDelegate.AirlinesView.Scan(true);
            });
        }

        public bool areTextEmpty()
        {
            if (passengerTabLastName.Text == "" && passengerTabFirstName.Text == "" &&
                passengerTabLoyaltyText.Text == "" && flightText.Text == "" && classText.Text == "" &&
                PassengerTabNotesText.Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void onNoteTextChanged(object sender, EventArgs e)
        {
            if (PassengerTabNotesText.Text.Contains("\n"))
            {
                PassengerTabNotesText.ResignFirstResponder();
            }
        }
        
        public void classTextChanged(object sender, EventArgs e)
        {
            
            classText.Text = Regex.Replace(classText.Text, @"[^a-zA-Z]+", "");
            classText.Text = Regex.Replace(classText.Text, " ", "");

            //if (classText.Text.Length > 1)
            //{
            //    classText.Text = classText.Text[0].ToString();
            //}
        }

        public void carrierTextChanged(object sender, EventArgs e)
        {
            carrierText.Text = Regex.Replace(carrierText.Text, @"[^a-zA-Z]+", "");
            carrierText.Text = Regex.Replace(carrierText.Text, " ", "");
            carrierText.Text = carrierText.Text.ToUpper();

            //if (carrierText.Text.Length > 2)
            //{
            //    carrierText.Text = carrierText.Text.Substring(0, 2);
            //}

        }

        public void flightTextChanged(object sender, EventArgs e)
        {
            flightText.Text = Regex.Replace(flightText.Text, @"[^0-9]+", "");
            flightText.Text = Regex.Replace(flightText.Text, " ", "");

            //if (flightText.Text.Length > 4)
            //{
            //    flightText.Text = flightText.Text.Substring(0, 4);
            //}

        }

        public void keyboardWillShow(object o, UIKeyboardEventArgs e)
        {
            CGRect keyboardFrame = e.FrameEnd;

            UIView firstResponder = AutoScrollUtility.findFirstResponder(passengerTabScrollView);
            if(firstResponder == null)
            {
                return;
            }

            AutoScrollUtility.autoScrollTo(this, passengerTabScrollView, 
                firstResponder.Superview.Frame.Y + firstResponder.Frame.Bottom, keyboardFrame.Height);
        }

        public void keyboardWillHide(object o, UIKeyboardEventArgs e)
        {

            nfloat currentY = passengerTabScrollView.ContentOffset.Y;

            //go to top
            if((scrollMaxHeight * scaleFactor) < passengerTabScrollView.Frame.Height)
            {
                passengerTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
                passengerTabScrollView.SetContentOffset(new CGPoint(0f, 0f), true);
            }//stay
            else if(currentY < (scrollMaxHeight * scaleFactor) - passengerTabScrollView.Frame.Height)
            {
                passengerTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
            }
            else//go to bottom
            {
                passengerTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
                passengerTabScrollView.SetContentOffset(new CGPoint(0f, (scrollMaxHeight * scaleFactor) - passengerTabScrollView.Frame.Height), true);
            }

            /*
            if(currentY < (scrollMaxHeight * scaleFactor))
            {
                passengerTabScrollView.SetContentOffset(new CGPoint(0f, 0f), true);
                passengerTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
                return;
            }
            else
            {
                passengerTabScrollView.SetContentOffset(new CGPoint(0f, passengerTabNotesView.Frame.Bottom - (scrollMaxHeight * scaleFactor)), true);
                passengerTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
                return;
            }

            nfloat newY = (scrollMaxHeight * scaleFactor) - passengerTabScrollView.Frame.Height;

            if (newY < 0f)
                newY = 0f;
            */





        }

        public void RefreshPassenger()
        {
            resetAllGradient();
            LoadPassenger();
        }
    }
}