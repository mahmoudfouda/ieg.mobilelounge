﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("ManualFlightTabViewController")]
    partial class ManualFlightTabViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView airplane { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton backButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.FlightTabBottomView bottomView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton confirmButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.FlightTabAirportView flightTabAirportView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightTabFromAirportLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField flightTabFromAirportText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightTabPnrLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField flightTabPnrText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView flightTabScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightTabSeatNumberLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField flightTabSeatNumberText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightTabStatusLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField flightTabStatusText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.FlightTabTicketView flightTabTicketView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightTabTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightTabToAirportLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField flightTabToAirportText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton scanButtonMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ticket { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton topCancelButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView topIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.FlightTabTopView topView { get; set; }

        [Action ("buttonBack:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void buttonBack (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (airplane != null) {
                airplane.Dispose ();
                airplane = null;
            }

            if (backButton != null) {
                backButton.Dispose ();
                backButton = null;
            }

            if (bottomView != null) {
                bottomView.Dispose ();
                bottomView = null;
            }

            if (confirmButton != null) {
                confirmButton.Dispose ();
                confirmButton = null;
            }

            if (flightTabAirportView != null) {
                flightTabAirportView.Dispose ();
                flightTabAirportView = null;
            }

            if (flightTabFromAirportLabel != null) {
                flightTabFromAirportLabel.Dispose ();
                flightTabFromAirportLabel = null;
            }

            if (flightTabFromAirportText != null) {
                flightTabFromAirportText.Dispose ();
                flightTabFromAirportText = null;
            }

            if (flightTabPnrLabel != null) {
                flightTabPnrLabel.Dispose ();
                flightTabPnrLabel = null;
            }

            if (flightTabPnrText != null) {
                flightTabPnrText.Dispose ();
                flightTabPnrText = null;
            }

            if (flightTabScrollView != null) {
                flightTabScrollView.Dispose ();
                flightTabScrollView = null;
            }

            if (flightTabSeatNumberLabel != null) {
                flightTabSeatNumberLabel.Dispose ();
                flightTabSeatNumberLabel = null;
            }

            if (flightTabSeatNumberText != null) {
                flightTabSeatNumberText.Dispose ();
                flightTabSeatNumberText = null;
            }

            if (flightTabStatusLabel != null) {
                flightTabStatusLabel.Dispose ();
                flightTabStatusLabel = null;
            }

            if (flightTabStatusText != null) {
                flightTabStatusText.Dispose ();
                flightTabStatusText = null;
            }

            if (flightTabTicketView != null) {
                flightTabTicketView.Dispose ();
                flightTabTicketView = null;
            }

            if (flightTabTitle != null) {
                flightTabTitle.Dispose ();
                flightTabTitle = null;
            }

            if (flightTabToAirportLabel != null) {
                flightTabToAirportLabel.Dispose ();
                flightTabToAirportLabel = null;
            }

            if (flightTabToAirportText != null) {
                flightTabToAirportText.Dispose ();
                flightTabToAirportText = null;
            }

            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (scanButtonMI2 != null) {
                scanButtonMI2.Dispose ();
                scanButtonMI2 = null;
            }

            if (ticket != null) {
                ticket.Dispose ();
                ticket = null;
            }

            if (topCancelButton != null) {
                topCancelButton.Dispose ();
                topCancelButton = null;
            }

            if (topIcon != null) {
                topIcon.Dispose ();
                topIcon = null;
            }

            if (topView != null) {
                topView.Dispose ();
                topView = null;
            }
        }
    }
}