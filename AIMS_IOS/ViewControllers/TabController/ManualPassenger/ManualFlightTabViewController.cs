using Foundation;
using System;
using UIKit;
using CoreGraphics;
using AIMS;
using AIMS.Models;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public partial class ManualFlightTabViewController : AIMSViewController, IPassengerRefresher
    {
        const int ARRIVAL_AIRPORT_MAX_LENGTH = 3;
        const int SEAT_NO_MAX_LENGTH = 4;
        const int PNR_MAX_LENGTH = 7;

        public LinedView linedView;

        private UITapGestureRecognizer tap;
        private nfloat scaleFactor = 1f;

        public ManualFlightTabViewController(IntPtr handle) : base(handle)
        {
        }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() =>
            {

                flightTabFromAirportLabel.Text = AppDelegate.TextProvider.GetText(2032);//From (Airport)
                flightTabToAirportLabel.Text = AppDelegate.TextProvider.GetText(2033);//To (Airport)
                flightTabFromAirportText.Placeholder = AppDelegate.TextProvider.GetText(2018);//Departure
                flightTabToAirportText.Placeholder = AppDelegate.TextProvider.GetText(2019);//Arrival

                flightTabSeatNumberLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2034));//Seat Number:
                flightTabPnrLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2044));//PNR: 
                flightTabStatusLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2035));//Passenger Status: 
                
                scanButtonMI2.SetTitle(AppDelegate.TextProvider.GetText(2007), UIControlState.Normal);
                scanButtonMI2.SetTitle(AppDelegate.Current.LineaDevice.DeviceName, UIControlState.Disabled);//TODO: remove in case of other external cameras
                //scanButtonMI2.SetTitle(AppDelegate.TextProvider.GetText(2057), UIControlState.Disabled);
            });
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            initLinedView();

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;

            //setFonts();

            flightTabToAirportText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };

            flightTabToAirportText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                var length = textField.Text.Length - range.Length + replacementString.Length;
                return length <= ARRIVAL_AIRPORT_MAX_LENGTH;
            };
            flightTabSeatNumberText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                var length = textField.Text.Length - range.Length + replacementString.Length;
                return length <= SEAT_NO_MAX_LENGTH;
            };
            flightTabPnrText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                var length = textField.Text.Length - range.Length + replacementString.Length;
                return length <= PNR_MAX_LENGTH;
            };

            var swipeRight = new UISwipeGestureRecognizer(HandleRightSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Right };
            View.AddGestureRecognizer(swipeRight);

            tap = new UITapGestureRecognizer((textfiled) =>
            {
                UIView temp = AutoScrollUtility.findFirstResponder(this.View);

                if (temp != null)
                {
                    temp.ResignFirstResponder();
                }
            });
            this.View.AddGestureRecognizer(tap);

            confirmButton.TouchUpInside += onConfirm;
            scanButtonMI2.TouchUpInside += scanClicked;
            topCancelButton.TouchUpInside += onCancel;
            
            backButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f); ;
            backButton.Layer.BorderWidth = 1f;
            backButton.TitleLabel.AdjustsFontSizeToFitWidth = true;

            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                scanButtonMI2.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
            };

            scanButtonMI2.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f); ;
            scanButtonMI2.Layer.BorderWidth = 1f;
            scanButtonMI2.TitleLabel.AdjustsFontSizeToFitWidth = false;

            scanButtonMI2.Enabled = AppDelegate.ActiveCamera != CameraType.ExternalCamera;

            confirmButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            confirmButton.Layer.BorderWidth = 1f;
            confirmButton.TitleLabel.AdjustsFontSizeToFitWidth = true;
            

            flightTabTitle.AdjustsFontSizeToFitWidth = true;
            flightTabFromAirportLabel.AdjustsFontSizeToFitWidth = true;
            flightTabToAirportLabel.AdjustsFontSizeToFitWidth = true;

            flightTabSeatNumberLabel.AdjustsFontSizeToFitWidth = true;
            flightTabPnrLabel.AdjustsFontSizeToFitWidth = true;
            flightTabStatusLabel.AdjustsFontSizeToFitWidth = true;

            topIcon.Image = AppData.usersAirLine.image;
            flightTabTitle.Text = MembershipProvider.Current.UserHeaderText;
        }

        public void initLinedView()
        {
            linedView = new LinedView();

            this.flightTabScrollView.AddSubview(linedView);
        }

        public void setFonts()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                nfloat xxLargeTexts = 26f * scaleFactor, xLargeTexts = 24f * scaleFactor, largeTexts = 22f * scaleFactor, medium = 17f * scaleFactor, smallTexts = 14f * scaleFactor;
                string /*italicFontName = "HelveticaNeue-Italic",*/ boldFontName = "Helvetica-Bold", regularFontName = "Helvetica Neue";

                var titleFont = UIFont.FromName(regularFontName, xxLargeTexts);
                var xLargeFont = UIFont.FromName(regularFontName, xLargeTexts);
                var xLargeBoldFont = UIFont.FromName(boldFontName, xLargeTexts);
                var largeFont = UIFont.FromName(regularFontName, largeTexts);
                var largeBoldFont = UIFont.FromName(boldFontName, largeTexts);
                var smallFont = UIFont.FromName(regularFontName, smallTexts);
                var mediumFont = UIFont.FromName(regularFontName, medium);
                var mediumBoldFont = UIFont.FromName(boldFontName, medium);

                //top view
                //flightTabTitle.Font = UIFont.FromName("Helvetica Neue", 26f);

                //airport view
                flightTabFromAirportLabel.Font = mediumFont;//UIFont.FromName("Helvetica Neue", 22f);
                flightTabFromAirportText.Font = smallFont;//UIFont.FromName("Helvetica-Bold", 20f);

                flightTabToAirportLabel.Font = mediumFont;//UIFont.FromName("Helvetica Neue", 22f);
                flightTabToAirportText.Font = smallFont;//UIFont.FromName("Helvetica-Bold", 20f);

                //ticket view
                flightTabSeatNumberLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                flightTabSeatNumberText.Font = smallFont; //UIFont.FromName("Helvetica-Bold", 20f);

                flightTabPnrLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                flightTabPnrText.Font = smallFont; //UIFont.FromName("Helvetica-Bold", 20f);

                flightTabStatusLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                flightTabStatusText.Font = smallFont; //UIFont.FromName("Helvetica-Bold", 20f);

                //buttons
                scanButtonMI2.Font = UIFont.SystemFontOfSize(smallTexts); //UIFont.FromName("Helvetica Neue", 17f);
                backButton.Font = UIFont.SystemFontOfSize(medium);
            }
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            updateLayout();

            setFonts();
        }

        private void updateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;
            nfloat statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;
            scaleFactor = height / 667f;
            //Commenting to support iPhone 5 and 5s
            //if (scaleFactor < 1f)
            //{
            //    scaleFactor = 1f;
            //}

            nfloat margin = 5f;
            nfloat doubleMargin = margin * 2f;
            nfloat separatorWidth = 20f;

            //--------------top view-------------------------------------
            topView.Frame = new CGRect(0f, statusBarH, frame.Width, 44f * scaleFactor);

            nfloat airlineImgSize = topView.Frame.Height - (margin * 2f);
            topIcon.Frame = new CGRect(margin, margin, airlineImgSize, airlineImgSize);

            flightTabTitle.Frame = new CGRect(topIcon.Frame.Right + margin, topIcon.Frame.Y,
                topView.Frame.Width - (margin * 2f), airlineImgSize);

            topCancelButton.Frame = new CGRect(0f, 0f, topView.Frame.Width, topView.Frame.Height);

            //-------------scroll view---------------------------------------------
            flightTabScrollView.Frame = new CGRect(0f, topView.Frame.Bottom,
                this.View.Frame.Width, this.View.Frame.Height - topView.Frame.Bottom - bottomView.Frame.Height);

            this.flightTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, 350d * scaleFactor);

            nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.66f;

            nfloat childXOffset = (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f);

            //---------------bottom view-----------------------
            nfloat bottomHeight = 60f * scaleFactor;
            //nfloat buttonsWidth = 60f * scaleFactor;

            bottomView.Frame = new CGRect(0f, this.View.Frame.Height - bottomHeight,
                frame.Width, bottomHeight);

            nfloat buttonsTotal = frame.Width - (40f * scaleFactor);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                buttonsTotal -= (childXOffset * 2f);
            }
            nfloat buttonSlice = buttonsTotal / 6f;

            backButton.Frame = new CGRect(
                10f * scaleFactor, bottomView.Frame.Height * 0.2f,
                buttonSlice, bottomView.Frame.Height * 0.6f);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                backButton.Frame = new CGRect(childXOffset + (10f * scaleFactor), bottomView.Frame.Height * 0.2f,
                    buttonSlice, bottomView.Frame.Height * 0.6f);
            }

            scanButtonMI2.Frame = new CGRect(backButton.Frame.Right + (10f * scaleFactor),
                backButton.Frame.Y,
                buttonSlice * 4f, backButton.Frame.Height);

            confirmButton.Frame = new CGRect(scanButtonMI2.Frame.Right + (10f * scaleFactor),
                backButton.Frame.Y,
                backButton.Frame.Width, backButton.Frame.Height);

            //---------------airport view------------------------

            flightTabAirportView.Frame = new CGRect(0f, 0f, this.View.Frame.Width, 120f * scaleFactor);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                flightTabAirportView.Frame = new CGRect(childXOffset, 0f,
                    childWidth, 120f * scaleFactor);
            }

            nfloat airplaneSize = flightTabAirportView.Frame.Height * 0.666f;
            nfloat flightLabelHeight = flightTabAirportView.Frame.Height * 0.1666f;
            nfloat flightTextField = flightTabAirportView.Frame.Height * 0.231f;

            airplane.Frame = new CGRect(2f, flightTabAirportView.Frame.Height * 0.1666f,
                airplaneSize, airplaneSize);


            //label group 1
            flightTabFromAirportLabel.Frame = new CGRect(airplane.Frame.Right + margin, margin,
                flightTabAirportView.Frame.Width - airplane.Frame.Right - margin - margin,
                flightLabelHeight);

            flightTabFromAirportText.Frame = new CGRect(flightTabFromAirportLabel.Frame.X,
                flightTabFromAirportLabel.Frame.Bottom + margin, flightTabFromAirportLabel.Frame.Width,
                flightTextField);

            //label group 2
            flightTabToAirportLabel.Frame = new CGRect(flightTabFromAirportText.Frame.X,
                flightTabFromAirportText.Frame.Bottom + doubleMargin,
                flightTabFromAirportText.Frame.Width, flightLabelHeight);

            flightTabToAirportText.Frame = new CGRect(flightTabToAirportLabel.Frame.X,
                flightTabToAirportLabel.Frame.Bottom + margin,
                flightTabToAirportLabel.Frame.Width, flightTextField);



            //-------------ticket view-----------------------
            flightTabTicketView.Frame = new CGRect(0f, flightTabAirportView.Frame.Bottom,
                this.View.Frame.Width, 130f * scaleFactor);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                flightTabTicketView.Frame = new CGRect(childXOffset, flightTabAirportView.Frame.Bottom,
                childWidth, 130f * scaleFactor);
            }
            //image
            ticket.Frame = new CGRect(margin, margin,
                airplane.Frame.Width, airplane.Frame.Width * 1.125f);

            nfloat ticketElementsHeight = flightTabTicketView.Frame.Height * 0.2307f;

            nfloat textFieldW = flightTabTicketView.Frame.Width * 0.293f;
            nfloat textFieldX = flightTabTicketView.Frame.Width - textFieldW - margin;
            nfloat labelX = ticket.Frame.Right + margin;
            nfloat labelW = flightTabTicketView.Frame.Width - labelX - textFieldW - margin;


            //seat number
            flightTabSeatNumberLabel.Frame = new CGRect(labelX, margin,
                labelW, ticketElementsHeight);
            flightTabSeatNumberText.Frame = new CGRect(textFieldX, flightTabSeatNumberLabel.Frame.Y,
                textFieldW, ticketElementsHeight);
            //pnr
            flightTabPnrLabel.Frame = new CGRect(labelX, flightTabSeatNumberLabel.Frame.Bottom + doubleMargin,
                labelW, ticketElementsHeight);
            flightTabPnrText.Frame = new CGRect(textFieldX, flightTabPnrLabel.Frame.Y,
                textFieldW, ticketElementsHeight);
            //status
            flightTabStatusLabel.Frame = new CGRect(labelX, flightTabPnrLabel.Frame.Bottom + doubleMargin,
                labelW, ticketElementsHeight);
            flightTabStatusText.Frame = new CGRect(textFieldX, flightTabStatusLabel.Frame.Y,
                textFieldW, ticketElementsHeight);

            //lined view
            linedView.Frame = new CGRect(0f, flightTabAirportView.Frame.Y,
                UIScreen.MainScreen.Bounds.Width, flightTabTicketView.Frame.Bottom);

            linedView.LinesY = new nfloat[3] { flightTabAirportView.Frame.Y, flightTabTicketView.Frame.Y, flightTabTicketView.Frame.Bottom };
        }
        
        public void resetAllGradient()
        {
            GradientUtility.removeGradiants(flightTabPnrText);
            GradientUtility.removeGradiants(flightTabFromAirportText);
            GradientUtility.removeGradiants(flightTabToAirportText);
        }

        private void LoadPassenger()
        {
            InvokeOnMainThread(() =>
            {

                flightTabFromAirportText.Text = MembershipProvider.Current.SelectedLounge.AirportCode;

                if (AppDelegate.ManualPassengerView.SelectedPassenger != null)
                {
                    flightTabToAirportText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.ToAirport;
                    flightTabSeatNumberText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.SeatNumber;

                    flightTabPnrText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.PNR;
                    flightTabStatusText.Text = AppDelegate.ManualPassengerView.SelectedPassenger.PassengerStatus;
                }
                else
                {
                    flightTabToAirportText.Text = flightTabSeatNumberText.Text =
                    flightTabPnrText.Text = flightTabStatusText.Text = "";
                }

                if (AppData.ValidationResult.IsPNRRequired)
                {
                    GradientUtility.addErrorGradiant(flightTabPnrText);
                }
                else
                {
                    GradientUtility.removeGradiants(flightTabPnrText);
                }

                if (AppData.ValidationResult.IsDestinationOriginRequired)
                {
                    GradientUtility.addErrorGradiant(flightTabFromAirportText);
                    GradientUtility.addErrorGradiant(flightTabToAirportText);
                }
                else
                {
                    GradientUtility.removeGradiants(flightTabFromAirportText);
                    GradientUtility.removeGradiants(flightTabToAirportText);
                }
            });
        }

        /// <summary>
        /// This must be called when the tabs are changing and also when we click on the Confirm(check mark) button
        /// </summary>
        public void SavePassenger()
        {
            InvokeOnMainThread(() =>
            {
                if (MembershipProvider.Current == null) return;//in case of session timeout (which logs the user out)

                if (AppDelegate.ManualPassengerView.SelectedPassenger == null)
                    AppDelegate.ManualPassengerView.SelectedPassenger = new Passenger();
                else
                {
                    AppDelegate.ManualPassengerView.SelectedPassenger.FromAirport = flightTabFromAirportText.Text;
                    AppDelegate.ManualPassengerView.SelectedPassenger.ToAirport = flightTabToAirportText.Text;
                    AppDelegate.ManualPassengerView.SelectedPassenger.SeatNumber = flightTabSeatNumberText.Text;

                    AppDelegate.ManualPassengerView.SelectedPassenger.PNR = flightTabPnrText.Text;
                    AppDelegate.ManualPassengerView.SelectedPassenger.PassengerStatus = flightTabStatusText.Text;
                    AppDelegate.ManualPassengerView.SelectedPassenger.TrackingPassCombinedFlightNumber = "";
                }
            });
        }

        private void OnPassengerChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            HideLoading();
            InvokeOnMainThread(() =>
            {
                AppDelegate.ManualPassengerView.SelectedPassenger = passenger;
                if (isSucceeded)
                {
                    if (errorCode.HasValue && (ErrorCode)errorCode.Value == ErrorCode.PassengerNameMismatch)
                    {
                        #region Passenger name mismatch (agent decision)
                        Question(AppDelegate.TextProvider.GetText(1104),
                                    string.Format("{0}{1}",
                                    (errorCode.HasValue ?
                                        string.Format("{0}\n", AppDelegate.TextProvider.GetText((int)errorCode)) : ""),
                                        message),
                                    (ss, ee) =>
                                    {
                                        AppDelegate.AirlinesView.PresentSinglePageMIForm();
                                    },
                                    (ss, ee) =>
                                    {
                                        if (passenger != null)
                                        {
                                            PassengersRepository.AddFailedPassenger(passenger, message);
                                        }
                                        AppManager.rejectPassenger();
                                        try
                                        {
                                            AppDelegate.ValidationView.DismissViewController(true, null);
                                            AppDelegate.SinglePageMIView2.DismissViewController(true, null);
                                        }
                                        catch (Exception ex)
                                        {
                                            //TODO: test it and remove the try-catch
                                            var m = ex.Message;
                                            Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                                        }
                                    });
                        #endregion
                    }
                    else
                    {
                        #region Passenger check success
                        if (AppDelegate.SinglePageMIView2.IsShown)
                        {
                            this.DismissViewController(true, null);
                        }
                        if (PresentedViewController != AppDelegate.ValidationView)
                        {
                            AppDelegate.ValidationView.ProvidesPresentationContextTransitionStyle = true;
                            AppDelegate.ValidationView.DefinesPresentationContext = true;
                            AppDelegate.ValidationView.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
                            AppDelegate.AirlinesView.PresentViewController(AppDelegate.ValidationView, true, null);
                        }
                        #endregion
                    }

                }
                else
                {
                    AppData.ValidationResult = string.IsNullOrWhiteSpace(errorMetadata) ? new PassengerValidation() : new PassengerValidation(errorMetadata);
                    if (passenger != null)
                    {
                        PassengersRepository.AddFailedPassenger(passenger, message);
                        //LoadPassenger();
                    }
                    AIMSError(AppDelegate.TextProvider.GetText(1104), message, null, 3000); // TODO: Show failed and after 3 seconds return back to this screen (manual input)
                    if (errorCode.HasValue && errorCode.Value == (int)ErrorCode.NeedToSwipeBoardingPass)//only in case of Incomplete Info we don't close the form
                        UITabSwipe.HandleRightSwipe(/*null,*/ this);
                    else
                        AppDelegate.ManualPassengerView.Close();//Closing the MI form in case of passenger validation failed (isSucceeded = false)
                }
            });
        }

        private void OnGuestChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            HideLoading();
            if (AppDelegate.ManualPassengerView.IsGuestMode)
                AppDelegate.GuestServicesView.OnGuestChecked(isSucceeded, errorCode, errorMetadata, message, passenger);
        }

        partial void buttonBack(UIButton sender)
        {
            SavePassenger();
            UITabSwipe.HandleRightSwipe(/*null,*/ this);
            //this.TabBarController.SelectedViewController = this.TabBarController.ViewControllers[0];
        }

        public void onConfirm(object sender, EventArgs e)
        {
            SavePassenger();

            ShowLoading(AppDelegate.TextProvider.GetText(2507));
            
            if (AppDelegate.ManualPassengerView.IsGuestMode)
                AppManager.validateGuestInfo(AppDelegate.ManualPassengerView.SelectedPassenger, (PassengerServiceType)MembershipProvider.Current.SelectedPassengerService.Id, OnGuestChecked);// AppDelegate.GuestServicesView.OnPassengerChecked);//To hide loading before call
            else AppManager.validatePassengerInfo(OnPassengerChecked);
        }

        public void scanClicked(object sender, EventArgs e)
        {
            SavePassenger();
            if (AppDelegate.ActiveCamera == CameraType.ExternalCamera) return;
            this.DismissViewController(true, () =>
            {
                AppDelegate.AirlinesView.Scan(true);
            });
        }

        public bool areTextEmpty()
        {
            if (flightTabToAirportText == null || flightTabToAirportText.Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void onCancel(object sender, EventArgs e)
        {

            if (areTextEmpty() && (this.TabBarController.ViewControllers[0] as PassengerTabViewController).areTextEmpty())
            {
                var tabViewController = this.TabBarController as ManualPassengerTabController;
                tabViewController.Close();
            }
            else
            {
                AIMSConfirm("", AppDelegate.TextProvider.GetText(2519), (ss, ee) =>
                {
                    InvokeOnMainThread(() =>
                    {
                        var tabViewController = this.TabBarController as ManualPassengerTabController;
                        tabViewController.Close();
                    });
                });
            }
        }

        protected void HandleRightSwipe(UISwipeGestureRecognizer recognizer)
        {
            SavePassenger();
            UITabSwipe.HandleRightSwipe(/*recognizer,*/ this);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            updateLayout();

            if (MembershipProvider.Current == null)
                AppDelegate.ReturnToLoginView();
            else
                LoadPassenger();
            LoadTexts();
        }

        public void RefreshPassenger()
        {
            resetAllGradient();
            LoadPassenger();
        }
    }
}