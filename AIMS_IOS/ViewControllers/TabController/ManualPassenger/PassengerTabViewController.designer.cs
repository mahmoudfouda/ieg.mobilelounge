﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("PassengerTabViewController")]
    partial class PassengerTabViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView agentImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView airlineImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.PassengerTabBottomView bottomView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton cancelButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel carrierSeparator { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField carrierText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel classSeparator { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField classText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel classTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField flightText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel flightTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel nameSeparator { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton nextButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView passengerTabAirlineCard { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel passengerTabAirlineLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel passengerTabAirlineNameLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.PassengerTabCardView passengerTabCardView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel passengerTabCarrierLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.PassengerTabCarrierView passengerTabCarrierView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField passengerTabFirstName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel passengerTabFullNameLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField passengerTabLastName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.PassengerTabLogoView passengerTabLogoView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel passengerTabloyaltyLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField passengerTabLoyaltyText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.PassengerTabNameView passengerTabNameView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PassengerTabNotesLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView PassengerTabNotesText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.PassengerTabNotesView passengerTabNotesView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView passengerTabScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView planeImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton scanButtonMI1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView topAirlineImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton topButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel topTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.PassengerTabTopView topView { get; set; }

        [Action ("buttonNext:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void buttonNext (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (agentImage != null) {
                agentImage.Dispose ();
                agentImage = null;
            }

            if (airlineImage != null) {
                airlineImage.Dispose ();
                airlineImage = null;
            }

            if (bottomView != null) {
                bottomView.Dispose ();
                bottomView = null;
            }

            if (cancelButton != null) {
                cancelButton.Dispose ();
                cancelButton = null;
            }

            if (carrierSeparator != null) {
                carrierSeparator.Dispose ();
                carrierSeparator = null;
            }

            if (carrierText != null) {
                carrierText.Dispose ();
                carrierText = null;
            }

            if (classSeparator != null) {
                classSeparator.Dispose ();
                classSeparator = null;
            }

            if (classText != null) {
                classText.Dispose ();
                classText = null;
            }

            if (classTitle != null) {
                classTitle.Dispose ();
                classTitle = null;
            }

            if (flightText != null) {
                flightText.Dispose ();
                flightText = null;
            }

            if (flightTitle != null) {
                flightTitle.Dispose ();
                flightTitle = null;
            }

            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (nameSeparator != null) {
                nameSeparator.Dispose ();
                nameSeparator = null;
            }

            if (nextButton != null) {
                nextButton.Dispose ();
                nextButton = null;
            }

            if (passengerTabAirlineCard != null) {
                passengerTabAirlineCard.Dispose ();
                passengerTabAirlineCard = null;
            }

            if (passengerTabAirlineLabel != null) {
                passengerTabAirlineLabel.Dispose ();
                passengerTabAirlineLabel = null;
            }

            if (passengerTabAirlineNameLabel != null) {
                passengerTabAirlineNameLabel.Dispose ();
                passengerTabAirlineNameLabel = null;
            }

            if (passengerTabCardView != null) {
                passengerTabCardView.Dispose ();
                passengerTabCardView = null;
            }

            if (passengerTabCarrierLabel != null) {
                passengerTabCarrierLabel.Dispose ();
                passengerTabCarrierLabel = null;
            }

            if (passengerTabCarrierView != null) {
                passengerTabCarrierView.Dispose ();
                passengerTabCarrierView = null;
            }

            if (passengerTabFirstName != null) {
                passengerTabFirstName.Dispose ();
                passengerTabFirstName = null;
            }

            if (passengerTabFullNameLabel != null) {
                passengerTabFullNameLabel.Dispose ();
                passengerTabFullNameLabel = null;
            }

            if (passengerTabLastName != null) {
                passengerTabLastName.Dispose ();
                passengerTabLastName = null;
            }

            if (passengerTabLogoView != null) {
                passengerTabLogoView.Dispose ();
                passengerTabLogoView = null;
            }

            if (passengerTabloyaltyLabel != null) {
                passengerTabloyaltyLabel.Dispose ();
                passengerTabloyaltyLabel = null;
            }

            if (passengerTabLoyaltyText != null) {
                passengerTabLoyaltyText.Dispose ();
                passengerTabLoyaltyText = null;
            }

            if (passengerTabNameView != null) {
                passengerTabNameView.Dispose ();
                passengerTabNameView = null;
            }

            if (PassengerTabNotesLabel != null) {
                PassengerTabNotesLabel.Dispose ();
                PassengerTabNotesLabel = null;
            }

            if (PassengerTabNotesText != null) {
                PassengerTabNotesText.Dispose ();
                PassengerTabNotesText = null;
            }

            if (passengerTabNotesView != null) {
                passengerTabNotesView.Dispose ();
                passengerTabNotesView = null;
            }

            if (passengerTabScrollView != null) {
                passengerTabScrollView.Dispose ();
                passengerTabScrollView = null;
            }

            if (planeImage != null) {
                planeImage.Dispose ();
                planeImage = null;
            }

            if (scanButtonMI1 != null) {
                scanButtonMI1.Dispose ();
                scanButtonMI1 = null;
            }

            if (topAirlineImage != null) {
                topAirlineImage.Dispose ();
                topAirlineImage = null;
            }

            if (topButton != null) {
                topButton.Dispose ();
                topButton = null;
            }

            if (topTitle != null) {
                topTitle.Dispose ();
                topTitle = null;
            }

            if (topView != null) {
                topView.Dispose ();
                topView = null;
            }
        }
    }
}