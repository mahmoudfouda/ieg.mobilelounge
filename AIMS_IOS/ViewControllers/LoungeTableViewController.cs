using AIMS;
using AIMS.Models;
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;
using Ieg.Mobile.Localization;
using System.Linq;
using System.Threading.Tasks;

namespace AIMS_IOS
{
    public partial class LoungeTableViewController : UIViewController, IUITableViewDataSourcePrefetching
    {
        private List<LoungeCellData> listLoungeCell = new List<LoungeCellData>();
        private UIRefreshControl refreshControl;
        private bool needStopTimmer = false;

        public nfloat ScaleFactor { get; private set; }

        public LoungeTableViewController(IntPtr handle) : base(handle)
        {
        }

        private void LoadTexts(Language? language = null)
        {
            if (MembershipProvider.Current == null || MembershipProvider.Current.LoggedinUser == null) return;
            InvokeOnMainThread(() =>
            {
                loungeTitle.Text = string.Format("{0} {1}!",
                    AppDelegate.TextProvider.GetText(2006),
                    MembershipProvider.Current.LoggedinUser.FirstName);

                loungeDescription.Text = AppDelegate.TextProvider.GetText(2503);


                logoutButton.SetTitle(AppDelegate.TextProvider.GetText(2056), UIControlState.Normal);
            });
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            //setFonts();

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;//TODO: Should happen on every view controller
            logoutButton.TouchUpInside += onLogoutClick;

            loungeTable.TableFooterView = new UIView();

            logoutButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            logoutButton.Layer.BorderWidth = 1f;

            logoutButton.TitleLabel.AdjustsFontSizeToFitWidth = true;
            loungeTitle.AdjustsFontSizeToFitWidth = true;
            loungeDescription.AdjustsFontSizeToFitWidth = true;

            UpdateLayout();

            refreshControl = new UIRefreshControl();
            refreshControl.ValueChanged += RefreshControl_ValueChanged;
            loungeTable.RefreshControl = refreshControl;
            loungeTable.InsertSubview(refreshControl, 0);
            loungeTable.Bounces = true;

            loungeTable.PrefetchDataSource = this;
        }

        private void RefreshControl_ValueChanged(object sender, EventArgs e)
        {
            var task = new Task(() =>
           {
               BeginInvokeOnMainThread(async () =>
               {
                   try
                   {
                       AppManager.ClearQueue();

                       await UpdateLoungesList();

                       if (refreshControl != null)
                           refreshControl.EndRefreshing();
                   }
                   catch
                   {
                   }
               });
           });
            task.Start();
        }

        private async Task UpdateLoungesList()
        {
            listLoungeCell = AppManager.getListOfLounges();
            listLoungeCell.ForEach(c => c.IsUpdated = false);
            loungeTable.Source = new LoungeTableSource(listLoungeCell, this, loungeTable);
            UpdateLoungesOccupancies();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            //UpdateLayout();
            //updateTableLayout();
            //LoadTexts();
        }

        //public void setFonts()
        //{
        //    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        //    {
        //        //logout view
        //        //logoutButton.TitleLabel.Font = UIFont.FromName("font name", 20f);

        //        //top view
        //        //loungeTitle.Font = UIFont.FromName("font name", 20f);
        //        //loungeDescription.Font = UIFont.FromName("font name", 20f);

        //    }
        //}

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            needStopTimmer = false;

            if (MembershipProvider.Current == null || MembershipProvider.Current.LoggedinUser == null)
            {
                AppDelegate.ReturnToLoginView();
                return;
            }

            var needToLoad = false;

            if (listLoungeCell.Count == 0)
            {
                needToLoad = true;
                listLoungeCell = AppManager.getListOfLounges();
                listLoungeCell.ForEach(c => c.IsUpdated = false);
                loungeTable.Source = new LoungeTableSource(listLoungeCell, this, loungeTable);
            }

            //UpdateLayout();
            updateTableLayout();
            LoadTexts();


            if (needToLoad)
                UpdateLoungesOccupancies();

        }

        private void UpdateLoungesOccupancies()
        {
            //var localDate = DateTime.Now;

            //if (MembershipProvider.Current == null)
            //    return;

            var visibleRows = loungeTable.IndexPathsForVisibleRows;

            GetOccupancyDataByVisibleRows(visibleRows);

            //AppManager.getLoungeOccupanciesList(AppManager.getListOfLoungeIDs(),
            //                                    AppManager.getListOfWorkstationIDs(),
            //                                    AppManager.getListOfAirportCodes(),
            //                                    localDate, localDate, 60,
            //                                    (data) =>
            //                                    {
            //                                        InvokeOnMainThread(() =>
            //                                        {
            //                                            if (data == null)
            //                                                return;

            //                                            var listData = (List<LoungeOccupancy>)data;


            //                                            foreach (var loungeOccupancy in listData)
            //                                            {
            //                                                var item = listLoungeCell.SingleOrDefault(c => c.Workstation.LoungeID == loungeOccupancy.LoungeId);
            //                                                item.Progress = (double)loungeOccupancy.Percentage;
            //                                            }

            //                                            loungeTable.ReloadData();
            //                                        });
            //                                    });
        }

        bool isBusy = false;
        List<Workstation> originalLounges = new List<Workstation>();
        private object lockQuery = new object();


        private void GetOccupancyDataByVisibleRows(NSIndexPath[] visibleRows)
        {
            //var task = new Task(() =>
            //{
            foreach (var visibleRow in visibleRows)
            {
                try
                {
                    var loungeCellData = listLoungeCell[visibleRow.Row];

                    if (loungeCellData.IsUpdated)
                        continue;


                    AppManager.GetLoungeOccupancyWhithQueue(loungeCellData.Workstation, (data) =>
                    {
                        BeginInvokeOnMainThread(() =>
                        {
                            if (data == null)
                                return;
                            var loungeOccupancy = (LoungeOccupancy)data;
                            loungeCellData.IsUpdated = true;
                            loungeCellData.Progress = (double)loungeOccupancy.Percentage;
                            var cell = loungeTable.CellAt(visibleRow) as LoungeCell;
                            if (cell != null)
                                cell.updateCell(loungeCellData.name, loungeCellData.description, loungeCellData.Progress);
                        });
                    });
                }
                catch (Exception ex)
                {
                    var m = ex.Message;
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                }
            }
            //});
            //task.Start();
        }


        /*  private void GetOccupancyDataByVisibleRows(NSIndexPath[] visibleRows)
          {
              lock (lockQuery)
              {
                  if (isBusy)
                      return;


                  List<Workstation> lounges = new List<Workstation>();

                  isBusy = true;

                  foreach (var visibleRow in visibleRows)
                  {
                      if (originalLounges.Count(c => c.LoungeID == listLoungeCell[visibleRow.Row].Workstation.LoungeID) == 0)
                      {
                          originalLounges.Add(listLoungeCell[visibleRow.Row].Workstation);
                          lounges.Add(listLoungeCell[visibleRow.Row].Workstation);
                      }
                  }

                  if (lounges.Count == 0)
                      return;


                  foreach (var lounge in lounges)
                  {

                      AppManager.GetLoungeOccupancyWhithQueue(lounge, (data) =>
                      {
                          BeginInvokeOnMainThread(() =>
                          {
                              if (data != null)
                              {
                                  var occupancy = (LoungeOccupancy)data;

                                  var loungeCellData = listLoungeCell.FirstOrDefault(c => c.Workstation.LoungeID == occupancy.LoungeId);

                                  if (loungeCellData != null)
                                  {
                                      loungeCellData.IsUpdated = true;
                                      loungeCellData.Progress = (double)occupancy.Percentage;

                                      var cell = loungeTable.CellAt(visibleRows[i]) as LoungeCell;
                                      if (cell != null)
                                          cell.updateCell(loungeCellData.name, loungeCellData.description, loungeCellData.Progress);
                                  }
                              }
                          });
                      });



                      /*var dtNow = DateTime.Now;

                      var loungesId = (new decimal[] { lounge.LoungeID }).ToList();
                      var workstationId = (new decimal[] { lounge.WorkstationID }).ToList();
                      var airportCodes = (new string[] { lounge.AirportCode }).ToList();

                      AppManager.getLoungeOccupanciesList(loungesId, workstationId, airportCodes,  dtNow, dtNow, 120, (data) =>
                            {
                                BeginInvokeOnMainThread(() =>
                                {
                                    if (data != null)
                                    {
                                        var occupancies = (List<LoungeOccupancy>)data;

                                        for (int i = 0; i < occupancies.Count; i++)
                                        {
                                            var occupancy = occupancies[i];

                                            var loungeCellData = listLoungeCell.FirstOrDefault(c => c.Workstation.LoungeID == occupancy.LoungeId);

                                            if (loungeCellData != null)
                                            {
                                                loungeCellData.IsUpdated = true;
                                                loungeCellData.Progress = (double)occupancy.Percentage;

                                                var cell = loungeTable.CellAt(visibleRows[i]) as LoungeCell;
                                                if (cell != null)
                                                    cell.updateCell(loungeCellData.name, loungeCellData.description, loungeCellData.Progress);
                                            }
                                        }
                                    }

                                    isBusy = false;
                                });
                            }); 
                  }
              }*/


        /*    foreach (var visibleRow in visibleRows)
            {
                try
                {
                    var loungeCellData = listLoungeCell[visibleRow.Row];

                    if (loungeCellData.IsUpdated)
                        continue;

                    AppManager.GetLoungeOccupancyWhithQueue(loungeCellData.Workstation, (data) =>
                    {
                        BeginInvokeOnMainThread(() =>
                        {
                            if (data == null)
                                return;

                            var loungeOccupancy = (LoungeOccupancy)data;
                            loungeCellData.IsUpdated = true;
                            loungeCellData.Progress = (double)loungeOccupancy.Percentage;

                            var cell = loungeTable.CellAt(visibleRow) as LoungeCell;
                            if (cell != null)
                                cell.updateCell(loungeCellData.name, loungeCellData.description, loungeCellData.Progress);

                        });
                    });

                }
                catch (Exception ex)
                {
                    var m = ex.Message;
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                }
            }
            //});
            //task.Start(); */
    

        //public override void ViewSafeAreaInsetsDidChange()
        //{
        //    base.ViewSafeAreaInsetsDidChange();
        //    UpdateLayout();
        //    UpdateLoungesOccupancies();
        //}

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            UpdateLayout();
        }


        public void UpdateLayout()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;

            float statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;
            float margin = 5f;

            statusBarH += (float)View.SafeAreaInsets.Top;

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                0 : (float)View.SafeAreaInsets.Left;


            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ScaleFactor = height / 667f;
            //Commenting to support iPhone 5 and 5s
            //if (ScaleFactor < 1f)
            //{
            //    ScaleFactor = 1f;
            //}


            //---------logout view---------------
            loungeLogoutView.Frame = new CGRect(leftMargin, statusBarH, frame.Width, 44f * ScaleFactor);

            logoutButton.Frame = new CGRect(margin, margin, 100f * ScaleFactor,
                loungeLogoutView.Frame.Height - (margin * 2f));
            //-------------top view-----------------------
            loungeTopView.Frame = new CGRect(loungeLogoutView.Frame.X, loungeLogoutView.Frame.Bottom,
                loungeLogoutView.Frame.Width, 85f * ScaleFactor);

            nfloat iconSize = loungeTopView.Frame.Height * 0.47f;
            icon.Frame = new CGRect(margin, margin, iconSize, iconSize);

            loungeTitle.Frame = new CGRect(icon.Frame.Right + 2 * margin, icon.Frame.Y,
                frame.Width - (icon.Frame.Right + margin * 4f), icon.Frame.Height);

            loungeDescription.Frame = new CGRect(margin, icon.Frame.Bottom + margin,
                frame.Width - (margin * 2f),
                loungeTopView.Frame.Height - (icon.Frame.Bottom + margin + margin));

            //------------table view----------------------
            loungeTable.Frame = new CGRect(loungeTopView.Frame.X, loungeTopView.Frame.Bottom,
                loungeTopView.Frame.Width, frame.Height - loungeTopView.Frame.Bottom);

            if (loungeTable != null)
                loungeTable.ReloadData();
        }

        public void updateTableLayout()
        {
            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            //ratio based on iphone 6, minimum of 44f
            nfloat estimatedHeight = 85f / 650f * height;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                estimatedHeight *= .75f;

            estimatedHeight = estimatedHeight < 85f ? 85f : estimatedHeight;

            loungeTable.EstimatedRowHeight = estimatedHeight;
            loungeTable.RowHeight = estimatedHeight;
        }

        public void onRowSelected()
        {
            needStopTimmer = true;

            AppManager.StopTimerOccupancy();

            if (originalLounges != null)
                originalLounges.Clear();

            ClearUpdates();

            AppDelegate.AirlinesView.ClearLastValueOnProgress();

            this.PresentViewController(AppDelegate.AirlinesView, true, null);
        }

        public void onLogoutClick(object sender, EventArgs e)
        {
            this.DismissViewController(true, null);

            AppDelegate.ClearAllListenersOnOccupancyUpdateTick();

            if (originalLounges != null)
                originalLounges.Clear();

            ClearUpdates();

            needStopTimmer = true;

            AppManager.StopTimerOccupancy();

            UIViewTransition.displayFromLeftToRightLinear(AppDelegate.LoungeSelectionView, AppDelegate.LoginView, 0.3f);

            if (MembershipProvider.Current != null)
                MembershipProvider.Current.LogOutUser();
        }

        public void PrefetchRows(UITableView tableView, NSIndexPath[] indexPaths)
        {
            if (!needStopTimmer)
                GetOccupancyDataByVisibleRows(indexPaths);
        }

        private void ClearUpdates()
        {
            try
            {
                if (listLoungeCell != null)
                {
                    foreach (var item in listLoungeCell)
                    {
                        item.IsUpdated = false;
                    }
                }
            }
            catch (Exception ef)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ef);
            }
        }
    }
}