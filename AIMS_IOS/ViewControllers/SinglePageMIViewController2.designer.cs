﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("SinglePageMIViewController2")]
    partial class SinglePageMIViewController2
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView agentViewSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView airportTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView bottomViewSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnBackSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnBackTextSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnConfirmSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnScanSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView flightViewSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgAgentAirlineSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgCardSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgFromSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgSelectedAirlineSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView imgSelectedAirlineViewSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgToSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAgentAirlineSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAgentNameSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblCardNameSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFromFullSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSelectedGuestType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblToFullSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainViewSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView passengerViewSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView scrollViewHolderSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView scrollViewSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView selectedAirlineViewSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtCarrierSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtClassSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFFNSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFlightSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFromSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtFullNameSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtNoteSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtPNRSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtSeatSPMI2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtToSPMI2 { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (agentViewSPMI2 != null) {
                agentViewSPMI2.Dispose ();
                agentViewSPMI2 = null;
            }

            if (airportTableView != null) {
                airportTableView.Dispose ();
                airportTableView = null;
            }

            if (bottomViewSPMI2 != null) {
                bottomViewSPMI2.Dispose ();
                bottomViewSPMI2 = null;
            }

            if (btnBackSPMI2 != null) {
                btnBackSPMI2.Dispose ();
                btnBackSPMI2 = null;
            }

            if (btnBackTextSPMI2 != null) {
                btnBackTextSPMI2.Dispose ();
                btnBackTextSPMI2 = null;
            }

            if (btnConfirmSPMI2 != null) {
                btnConfirmSPMI2.Dispose ();
                btnConfirmSPMI2 = null;
            }

            if (btnScanSPMI2 != null) {
                btnScanSPMI2.Dispose ();
                btnScanSPMI2 = null;
            }

            if (flightViewSPMI2 != null) {
                flightViewSPMI2.Dispose ();
                flightViewSPMI2 = null;
            }

            if (imgAgentAirlineSPMI2 != null) {
                imgAgentAirlineSPMI2.Dispose ();
                imgAgentAirlineSPMI2 = null;
            }

            if (imgCardSPMI2 != null) {
                imgCardSPMI2.Dispose ();
                imgCardSPMI2 = null;
            }

            if (imgFromSPMI2 != null) {
                imgFromSPMI2.Dispose ();
                imgFromSPMI2 = null;
            }

            if (imgSelectedAirlineSPMI2 != null) {
                imgSelectedAirlineSPMI2.Dispose ();
                imgSelectedAirlineSPMI2 = null;
            }

            if (imgSelectedAirlineViewSPMI2 != null) {
                imgSelectedAirlineViewSPMI2.Dispose ();
                imgSelectedAirlineViewSPMI2 = null;
            }

            if (imgToSPMI2 != null) {
                imgToSPMI2.Dispose ();
                imgToSPMI2 = null;
            }

            if (lblAgentAirlineSPMI2 != null) {
                lblAgentAirlineSPMI2.Dispose ();
                lblAgentAirlineSPMI2 = null;
            }

            if (lblAgentNameSPMI2 != null) {
                lblAgentNameSPMI2.Dispose ();
                lblAgentNameSPMI2 = null;
            }

            if (lblCardNameSPMI2 != null) {
                lblCardNameSPMI2.Dispose ();
                lblCardNameSPMI2 = null;
            }

            if (lblFromFullSPMI2 != null) {
                lblFromFullSPMI2.Dispose ();
                lblFromFullSPMI2 = null;
            }

            if (lblSelectedGuestType != null) {
                lblSelectedGuestType.Dispose ();
                lblSelectedGuestType = null;
            }

            if (lblToFullSPMI2 != null) {
                lblToFullSPMI2.Dispose ();
                lblToFullSPMI2 = null;
            }

            if (mainViewSPMI2 != null) {
                mainViewSPMI2.Dispose ();
                mainViewSPMI2 = null;
            }

            if (passengerViewSPMI2 != null) {
                passengerViewSPMI2.Dispose ();
                passengerViewSPMI2 = null;
            }

            if (scrollViewHolderSPMI2 != null) {
                scrollViewHolderSPMI2.Dispose ();
                scrollViewHolderSPMI2 = null;
            }

            if (scrollViewSPMI2 != null) {
                scrollViewSPMI2.Dispose ();
                scrollViewSPMI2 = null;
            }

            if (selectedAirlineViewSPMI2 != null) {
                selectedAirlineViewSPMI2.Dispose ();
                selectedAirlineViewSPMI2 = null;
            }

            if (txtCarrierSPMI2 != null) {
                txtCarrierSPMI2.Dispose ();
                txtCarrierSPMI2 = null;
            }

            if (txtClassSPMI2 != null) {
                txtClassSPMI2.Dispose ();
                txtClassSPMI2 = null;
            }

            if (txtFFNSPMI2 != null) {
                txtFFNSPMI2.Dispose ();
                txtFFNSPMI2 = null;
            }

            if (txtFlightSPMI2 != null) {
                txtFlightSPMI2.Dispose ();
                txtFlightSPMI2 = null;
            }

            if (txtFromSPMI2 != null) {
                txtFromSPMI2.Dispose ();
                txtFromSPMI2 = null;
            }

            if (txtFullNameSPMI2 != null) {
                txtFullNameSPMI2.Dispose ();
                txtFullNameSPMI2 = null;
            }

            if (txtNoteSPMI2 != null) {
                txtNoteSPMI2.Dispose ();
                txtNoteSPMI2 = null;
            }

            if (txtPNRSPMI2 != null) {
                txtPNRSPMI2.Dispose ();
                txtPNRSPMI2 = null;
            }

            if (txtSeatSPMI2 != null) {
                txtSeatSPMI2.Dispose ();
                txtSeatSPMI2 = null;
            }

            if (txtToSPMI2 != null) {
                txtToSPMI2.Dispose ();
                txtToSPMI2 = null;
            }
        }
    }
}