﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("LoungeTableViewController")]
    partial class LoungeTableViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView icon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton logoutButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel loungeDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.LoungeLogoutView loungeLogoutView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.LoungeTableView loungeTable { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel loungeTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AIMS_IOS.LoungeTop loungeTopView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView topView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (icon != null) {
                icon.Dispose ();
                icon = null;
            }

            if (logoutButton != null) {
                logoutButton.Dispose ();
                logoutButton = null;
            }

            if (loungeDescription != null) {
                loungeDescription.Dispose ();
                loungeDescription = null;
            }

            if (loungeLogoutView != null) {
                loungeLogoutView.Dispose ();
                loungeLogoutView = null;
            }

            if (loungeTable != null) {
                loungeTable.Dispose ();
                loungeTable = null;
            }

            if (loungeTitle != null) {
                loungeTitle.Dispose ();
                loungeTitle = null;
            }

            if (loungeTopView != null) {
                loungeTopView.Dispose ();
                loungeTopView = null;
            }

            if (topView != null) {
                topView.Dispose ();
                topView = null;
            }
        }
    }
}