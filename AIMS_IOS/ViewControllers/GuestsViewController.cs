﻿using AIMS;
using AIMS.Models;
using AIMS_IOS.Models;
using AIMS_IOS.Utility;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using CoreGraphics;
using MWBarcodeScanner;
using ZXing.Mobile;
using InfineaSDK.iOS;

namespace AIMS_IOS
{
    public partial class GuestsViewController : AIMSViewController
    {
        private EventHandler GuestValidationViewInvoker;

        public GuestCategoryTableViewSource GuestTableViewSource { get; set; }

        public List<GuestCategoryCellData> GuestCategories { get; private set; }

        public static nfloat ScaleFactor { get; set; }

        public GuestsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.guestsBackButton.TouchUpInside += Close;
            this.guestsBackButtonLabel.TouchUpInside += Close;
            AppDelegate.Current.PeripheralEvents.BarcodeNSDataType += OnBarcodeScanned;
            GuestValidationViewInvoker = (s,e) => {
                this.InvokeOnMainThread(() => {
                    if (AppDelegate.GuestValidationResultView.IsBeingPresented)
                        AppDelegate.GuestValidationResultView.DismissViewController(true, () =>
                        {
                            OpenGuestValidator();
                        });
                    else
                    {
                        AppDelegate.GuestValidationResultView.ProvidesPresentationContextTransitionStyle = true;
                        AppDelegate.GuestValidationResultView.DefinesPresentationContext = true;
                        AppDelegate.GuestValidationResultView.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
                        AppDelegate.GuestValidationResultView.IsGuestMode = true;
                        this.PresentViewController(AppDelegate.GuestValidationResultView, true, null);
                    }
                });
            };
        }

        public void OpenGuestValidator()
        {
            this.Invoke(() => {
                GuestValidationViewInvoker?.Invoke(this, null);
            }, .1);
        }

        private void StopExternalScanner()//TODO: What is stop exactly??????!!!!
        {
            if (AppDelegate.Current.LineaDevice != null)
            {
                try
                {
                    if (AppDelegate.Current.LineaDevice.ConnState == ConnStates.ConnDisconnected)
                        AppDelegate.Current.LineaDevice.Connect();

                    NSError error;// = new NSError();
                    if (AppDelegate.Current.LineaDevice.BarcodeSetScanMode(ScanModes.ModeMotionDetect, out error))
                    {
                        //nothing for now
                    }
                    else if (error != null && error.Code != 0)
                    {
                        LogsRepository.AddError("Error in BarcodeSetScanMode", error.Description); //AIMSMessage("Error in BarcodeSetScanMode", error.Description);
                    }
                    if (AppDelegate.Current.LineaDevice.BarcodeStopScan(out error))
                    {
                        //nothing for now
                    }
                    else if (error != null && error.Code != 0)
                    {
                        LogsRepository.AddError("Error in BarcodeStopScan", error.Description); //AIMSMessage("Error in BarcodeStopScan", error.Description);
                    }
                }
                catch { }
            }
        }

        private void StartExternalScanner()
        {
            if (AppDelegate.Current.LineaDevice != null)
            {
                try
                {
                    if (AppDelegate.Current.LineaDevice.ConnState != ConnStates.ConnConnected)
                        AppDelegate.Current.LineaDevice.Connect();

                    NSError error;// = new NSError();
                    if (AppDelegate.Current.LineaDevice.BarcodeSetScanMode( ScanModes.ModeMotionDetect, out error))
                    {
                        //nothing for now
                    }
                    else if (error != null && error.Code != 0)
                    {
                        LogsRepository.AddError("Error in BarcodeSetScanMode", error.Description); //AIMSMessage("Error in BarcodeSetScanMode", error.Description);
                    }
                    if (AppDelegate.Current.LineaDevice.BarcodeStartScan(out error))
                    {
                        //nothing for now
                    }
                    else if (error != null && error.Code != 0)
                    {
                        LogsRepository.AddError("Error in BarcodeStartScan", error.Description); //AIMSMessage("Error in BarcodeStartScan", error.Description);
                    }
                }
                catch { }
            }
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            AppDelegate.GuestScanning = true;
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null) return;
            if (AppDelegate.ActiveCamera == CameraType.ExternalCamera)
                StopExternalScanner();
            
            updateLayout();

            lblHostName.Text = MembershipProvider.Current.SelectedPassenger.FullName;

            #region Loading passenger's first card
            if (MembershipProvider.Current.SelectedCard != null)
            {
                if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                {
                    imgGuestCard1.Image = MembershipProvider.Current.SelectedCard.CardPictureBytes.ToImage();
                }
                else
                {
                    imgGuestCard1.Image = UIImage.FromFile("Error/not_available_pic_off");
                    ImageAdapter.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (imageBytes) =>
                    {
                        var image = imageBytes.ToImage();
                        InvokeOnMainThread(() =>
                        {
                            //MembershipProvider.Current.SelectedCard.CardPictureBytes = imageBytes;
                            //CardsRepository.SaveCardImage(MembershipProvider.Current.SelectedCard);//Saved to local DB : Already done

                            imgGuestCard1.Image = image;
                        });
                    });
                }
            }
            #endregion

            #region Loading Passenger's second card (We don't want to show)
            //imgGuestCard2.Hidden = false;
            //if (MembershipProvider.Current.SelectedPassenger.OtherCardID != 0)
            //{
            //    var otherCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedPassenger.OtherCardID);
            //    if (otherCard != null)
            //    {
            //        if (otherCard.CardPictureBytes != null && otherCard.CardPictureBytes.Length > 0)
            //        {
            //            imgGuestCard2.Image = otherCard.CardPictureBytes.ToImage();
            //        }
            //        else
            //        {
            //            imgGuestCard2.Image = UIImage.FromFile("Error/not_available_pic_off");
            //            ImageAdapter.LoadImage(otherCard.ImageHandle, (imageBytes) =>
            //            {
            //                InvokeOnMainThread(() =>
            //                {
            //                    //otherCard.CardPictureBytes = imageBytes;
            //                    //CardsRepository.SaveCardImage(otherCard);//Saved to local DB : Already done

            //                    imgGuestCard2.Image = imageBytes.ToImage();
            //                });
            //            });
            //        }
            //    }
            //    else imgGuestCard2.Hidden = true;
            //}
            //else imgGuestCard2.Hidden = true;
            #endregion

            #region Filling passenger services and guests
            //Better to do this manually from outside! (not everytime)
            RefreshCategories(MembershipProvider.Current.SelectedPassenger.PassengerServices);
            #endregion
        }

        //public override void ViewDidDisappear(bool animated)
        //{
        //    base.ViewDidDisappear(animated);

        //    if (MembershipProvider.Current.SelectedPassengerService != null && GuestTableViewSource.SelectedParentIndex != -1)
        //    {
        //        //var selectionIndexPath = NSIndexPath.FromRowSection(GuestTableViewSource.SelectedParentIndex, 0);
        //        try
        //        {
        //            GuestTableViewSource.CollapseSubItemsAtIndex(guestServiceTableView, GuestTableViewSource.SelectedParentIndex);
        //        }
        //        catch (Exception ex)
        //        {
        //            var m = ex.Message;
        //        }
        //    }
        //}

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            AppDelegate.AirlinesView.InitScanner();
            //if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null) return;

            RefreshSelection();
        }

        public void RefreshSelection()
        {
            if (MembershipProvider.Current.SelectedPassengerService != null)
            {
                int selectedIndex = -1;

                var foundItem = GuestCategories.FirstOrDefault(x => x.PassengerServiceData != null && x.PassengerServiceData.Id == MembershipProvider.Current.SelectedPassengerService.Id);
                if (foundItem != null)
                    selectedIndex = GuestCategories.IndexOf(foundItem);

                if (selectedIndex != -1)
                {
                    var selectionIndexPath = NSIndexPath.FromRowSection(selectedIndex, 0);
                    GuestTableViewSource.RowSelected(guestServiceTableView, selectionIndexPath);
                }
            }
        }

        public void RefreshCategories(Dictionary<PassengerService,List<Passenger>> services, bool refreshSelection = false)
        {
            if (!AppDelegate.GuestServicesView.IsViewLoaded || services == null || guestServiceTableView == null) return;

            #region Loading Passenger services
            GuestCategories = new List<GuestCategoryCellData>();
            foreach (var guestCatKeyVal in services)
            {
                GuestCategories.Add(new GuestCategoryCellData(guestCatKeyVal.Key, guestCatKeyVal.Value));
            }
            #endregion

            GuestTableViewSource = new GuestCategoryTableViewSource(GuestCategories.Select(x => (Item)x).ToList());

            GuestTableViewSource.ScaleFactor = ScaleFactor;
            GuestTableViewSource.ScreenWidth = UIScreen.MainScreen.Bounds.Width;

            GuestTableViewSource.OnRowExpanded += (genericCell, rowIndexPath) => {
                var cell = (GuestCategoryCell)genericCell;
                if (cell != null)
                    InvokeOnMainThread(() => { cell.IsRowExpanded = true; });
            };
            GuestTableViewSource.OnRowCollapsed += (genericCell, rowIndexPath) => {
                var cell = (GuestCategoryCell)genericCell;
                if (cell != null)
                    InvokeOnMainThread(() => { cell.IsRowExpanded = false; });
            };
            GuestTableViewSource.OnItemSelected += (item, isChild) =>
            {
                if (item == null) return;
                if (!isChild)//if Parent selected
                {
                    if (item is ExpandableItem)
                    {
                        var guestCat = (GuestCategoryCellData)item;
                        if (guestCat != null)
                            MembershipProvider.Current.SelectedPassengerService = guestCat.PassengerServiceData;
                        else MembershipProvider.Current.SelectedPassengerService = null;
                    }
                    return;
                }

                var category = (GuestCategoryCellData)item.Parent;
                if (category != null)
                    MembershipProvider.Current.SelectedPassengerService = category.PassengerServiceData;
                else MembershipProvider.Current.SelectedPassengerService = null;
                if (item.Id != -1)
                {
                    var guest = (GuestCellData)item;
                    if (guest != null)
                    {
                        MembershipProvider.Current.SelectedGuest = guest.PassengerData;
                        
                        OpenGuestValidator();
                    }
                }
                else AddPassengerClicked();
            };
            guestServiceTableView.Source = GuestTableViewSource;
            guestServiceTableView.ReloadData();
            if (refreshSelection)
                System.Threading.Tasks.Task.Delay(100).ContinueWith((task) => {
                    InvokeOnMainThread(()=> {
                        RefreshSelection();
                    });
                });
        }

        //public void RefreshCategories()//Didn't work
        //{
        //    guestServiceTableView.ReloadData();
        //}

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            AppDelegate.GuestScanning = false;
        }

        private void OnBarcodeScanned(object sender, BarcodeNSDataTypeEventArgs e)
        {
            if (IsLoading || !AppDelegate.GuestScanning) return;
            try
            {
                InvokeOnMainThread(() =>
                {
                    if (AppDelegate.SinglePageMIView2 != null && AppDelegate.SinglePageMIView2.IsViewLoaded)
                        AppDelegate.SinglePageMIView2.ShowLoading(AppDelegate.TextProvider.GetText(2507));
                    else
                        ShowLoading(AppDelegate.TextProvider.GetText(2507));
                });
                //Is Guest's Second Scan?
                AppManager.validateGuestInfo(e.Barcode.ToString(), MembershipProvider.Current.SelectedPassengerService.Id, OnGuestChecked);
            }
            catch { }
        }

        public void OnGuestChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null) return;
            AppData.ValidationResult = string.IsNullOrWhiteSpace(errorMetadata) ? new PassengerValidation() : new PassengerValidation(errorMetadata);

            if (MembershipProvider.Current.SelectedPassenger.PassengerServices[MembershipProvider.Current.SelectedPassengerService] == null)//TODO: Move this to client core library and make the PassengerServices a full-property
                MembershipProvider.Current.SelectedPassenger.PassengerServices[MembershipProvider.Current.SelectedPassengerService] = new List<Passenger>();
            
            HideLoading();

            InvokeOnMainThread(() =>
            {
                if (AppDelegate.SinglePageMIView2 != null && AppDelegate.SinglePageMIView2.IsViewLoaded)
                    AppDelegate.SinglePageMIView2.HideLoading();

                //WriteDebug("checking to close");
                // (AppDelegate.ActiveCamera == CameraType.ExternalCamera && AppDelegate.GuestValidationResultView.IsViewLoaded)
                //if (AppDelegate.GuestValidationResultView != null && AppDelegate.GuestValidationResultView.IsViewLoaded)
                //{
                //    WriteDebug("closing");
                //    AppDelegate.GuestServicesView.InvokeOnMainThread(()=> {
                //        AppDelegate.GuestValidationResultView.DismissViewController(false, () => {
                //            WriteDebug("closed");
                //        });
                //    });
                //    //AppDelegate.GuestValidationResultView.DismissViewController(false,() => {
                //    //    WriteDebug("closed");
                //    //});
                //    //AppDelegate.GuestValidationResultView.Dispose();
                //    //WriteDebug("Disposed");
                //    //AppDelegate.GuestValidationResultView = Storyboard.InstantiateViewController("validationResultController") as ValidationResultViewController;
                //    //WriteDebug("Re instantiated");
                //}

                if (isSucceeded)
                {
                    #region Guest check success
                    try
                    {
                        //AppDelegate.ManualPassengerView.Close(false);//TODO: Test
                        AppDelegate.SinglePageMIView2.Close(false);//TODO: Test

                        //WriteDebug("setting GuestValidationResultView.IsGuestMode");
                        //AppDelegate.GuestValidationResultView.IsGuestMode = true;
                        
                        //WriteDebug("Presenting");
                        AppDelegate.GuestServicesView.OpenGuestValidator();
                        //WriteDebug("Presented");


                        AppManager.UpdateProgress();
                    }
                    catch (Exception exx)
                    {
                        //WriteDebug("Exception: " + exx.StackTrace);
                        AIMSError(AppDelegate.TextProvider.GetText(2527), exx.Message);
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(exx);
                    }
                    #endregion
                }
                else
                {
                    #region Guest check failed
                    if (errorCode.HasValue && (ErrorCode)errorCode.Value == ErrorCode.TaskAlreadyPerformedException)
                    {
                        handleGuestFailed(errorCode);
                        return;
                    }

                    if (passenger != null)
                    {
                        PassengersRepository.AddFailedPassenger(passenger, message);
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        if (message.Contains("\n"))
                            AIMSError(message.Substring(0, message.IndexOf("\n")).Trim()/* + "XX"*/, message.Substring(message.IndexOf("\n") + 1).Trim()/* + "XX"*/, (sender, e) =>
                            {
                                handleGuestFailed(errorCode);
                            });
                        else
                            AIMSError(AppDelegate.TextProvider.GetText(1104), message, (sender, e) =>
                            {
                                handleGuestFailed(errorCode);
                            });//title is "Validation failed!"
                    }
                    else
                        AIMSError(AppDelegate.TextProvider.GetText(1104), AppDelegate.TextProvider.GetText(1051)/* + "XX"*/, (sender, e) =>
                        {
                            handleGuestFailed(errorCode);
                        });//title is "Validation failed!" 
                    #endregion
                }
            });
        }

        public void PresentManualInputForm()
        {
            //if (AppDelegate.ManualPassengerView.IsViewLoaded)
            //    AppDelegate.ManualPassengerView.RefreshPassenger();
            //else
            //{
            //    AppDelegate.ManualPassengerView.ProvidesPresentationContextTransitionStyle = true;
            //    AppDelegate.ManualPassengerView.DefinesPresentationContext = true;
            //    AppDelegate.ManualPassengerView.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;

            //    this.PresentViewController(AppDelegate.ManualPassengerView, true, null);
            //}
            InvokeOnMainThread(() =>
            {
                if (AppDelegate.SinglePageMIView2.IsViewLoaded)
                    AppDelegate.SinglePageMIView2.RefreshPassenger();
                else
                {
                    AppDelegate.SinglePageMIView2.ProvidesPresentationContextTransitionStyle = true;
                    AppDelegate.SinglePageMIView2.DefinesPresentationContext = true;
                    AppDelegate.SinglePageMIView2.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;

                    this.PresentViewController(AppDelegate.SinglePageMIView2, true, null);
                }
            });
        }

        private void handleGuestFailed(int? errorCode)
        {
            if (errorCode.HasValue && (ErrorCode)errorCode.Value == ErrorCode.NeedToSwipeBoardingPass)//This is an incomplete passenger!!!
            {
                //this.PresentViewController(AppDelegate.ManualPassengerView, true, null);//with guest mode

                PresentManualInputForm();
            }
            else if (AppDelegate.ContinueScanning)
                StartScan(AppDelegate.SinglePageMIView2);//TODO: check if it is correct
        }

        public void StartScan(UIViewController parentViewController = null)
        {
            InvokeOnMainThread(async () =>
            {
                if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR)
                {
                    AIMSMessage(AppDelegate.TextProvider.GetText(2531), AppDelegate.TextProvider.GetText(2532));
                    return;
                }

                #region Camera access permission request
                var camReq = AppDelegate.Current.RequestCameraAccess();
                camReq.Wait();
                if (!camReq.Result.IsSucceeded)
                {
                    AIMSError(AppDelegate.TextProvider.GetText(2047), camReq.Result.Message);
                    return;
                }
                #endregion

                //AppDelegate.ContinueScanning = true;      //Not required
                //AppDelegate.IsScanningCancelled = false;  //Not required

                if (MembershipProvider.Current.IsTrialUser)
                {
                    #region Zxing Barcode Scanner
                    if (AppDelegate.Current.GuestZxingScanner != null)
                    {
                        try
                        {
                            AppDelegate.Current.GuestZxingScanner.Cancel();
                        }
                        catch (Exception ex)
                        {
                            var m = ex.Message;
                            Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                        }
                    }

                    AppDelegate.Current.GuestZxingScanner = new MobileBarcodeScanner(parentViewController);
                    var result = await AppDelegate.Current.GuestZxingScanner.Scan(AppDelegate.Current.ZxingOptions);//.ConfigureAwait(false);
                    if (result != null)
                    {
                        ShowLoading(AppDelegate.TextProvider.GetText(2507));

                        try
                        {
                            AppManager.validateGuestInfo(result.Text, MembershipProvider.Current.SelectedPassengerService.Id, OnGuestChecked);
                        }
                        catch (Exception ex)
                        {
                            LogsRepository.AddError("Scanner recognition failed", ex);
                            Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                        }
                    }
                    else
                    {
                        AppDelegate.IsScanningCancelled = true;
                        AppDelegate.ContinueScanning = false;
                    }
                    #endregion
                }
                else
                {
                    #region Manatee Barcode Scanner
                    MWScannerViewController.param_UseFrontCamera = (AppDelegate.ActiveCamera == CameraType.FrontCamera);

                    MWScannerViewController.param_CloseScannerOnSuccess = true;
                    MWScannerViewController.param_EnableClose = true;
                    MWScannerViewController.closeFrame = new CGRect(0, 0, 64, 64);
                    MWScannerViewController.param_PauseScanner = false;

                    MWScannerViewController scanVC = new MWScannerViewController();

                    scanVC.Init();

                    //var fixecVC = new NoRotationViewController(scanVC);

                    Action<ScannerResult> OnResultCallback = null;
                    OnResultCallback = (obj) =>
                    {
                        if (OnResultCallback != null)
                            scanVC.OnResult -= OnResultCallback;

                        if (obj != null)
                        {
                            ShowLoading(AppDelegate.TextProvider.GetText(2507));
                            try
                            {
                        //        fixecVC.DismissViewController(false, null);
                            }
                            catch { }
                            try
                            {
                                AppManager.validateGuestInfo(obj.code, MembershipProvider.Current.SelectedPassengerService.Id, OnGuestChecked);
                            }
                            catch (Exception ex)
                            {
                                LogsRepository.AddError("Scanner recognition failed", ex);
                                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                            }
                        }
                        else
                        {
                            AppDelegate.IsScanningCancelled = true;
                            AppDelegate.ContinueScanning = false;
                        }
                    };


                    scanVC.OnResult += OnResultCallback;

                    if (parentViewController != null)
                        parentViewController.PresentViewController(scanVC, false, null);

                    else this.PresentViewController(scanVC, false, null);
                    #endregion
                }
            });
            
        }

        private void AddPassengerClicked()
        {
            AppDelegate.SinglePageMIView2.ProvidesPresentationContextTransitionStyle = true;
            AppDelegate.SinglePageMIView2.DefinesPresentationContext = true;
            AppDelegate.SinglePageMIView2.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
            AppDelegate.SinglePageMIView2.IsGuestMode = true;

            //The code below is not required, since it is already filled up for the host passenger
            //MembershipProvider.Current.SelectedCard = items[indexPath.Row].Card;
            //AppData.selectedCard = items[indexPath.Row];
            AppData.ValidationResult = new PassengerValidation();

            //TODO: Move this to Client Core library
            MembershipProvider.Current.SelectedGuest = new Passenger
            {
                CardID = MembershipProvider.Current.SelectedCard.ID,
                AirlineId = MembershipProvider.Current.SelectedPassenger.AirlineId
            };
            
            this.PresentViewController(AppDelegate.SinglePageMIView2, true, () => {
                if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR) return;

                #region Start Scanning
                if (AppDelegate.ActiveCamera == CameraType.ExternalCamera)//start the external scanner
                    StartExternalScanner();
                else//start the camera scanner
                    StartScan(AppDelegate.SinglePageMIView2);
                #endregion
            });
        }

        private void updateLayout()
        {
            nfloat width = UIScreen.MainScreen.Bounds.Width;

            nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ScaleFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                //ScaleFactor = (frame.Height - statusBarH - detailsView.Frame.Height);
                ScaleFactor = height / (667f * (4f / 3f));
            }

            nfloat margin = 10f * ScaleFactor;

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ? 0 : (float)View.SafeAreaInsets.Left;

            pnlTop.Frame = new CGRect(0, 20f * ScaleFactor, width, 44f * ScaleFactor);
            pnlHost.Frame = new CGRect(0, pnlTop.Frame.Bottom, width, 60f * ScaleFactor);
            guestServiceTableView.Frame = new CGRect(leftMargin, pnlHost.Frame.Bottom, width - leftMargin, height - pnlHost.Frame.Bottom);
            guestServiceTableView.ShowsHorizontalScrollIndicator = false;
            guestServiceTableView.ShowsVerticalScrollIndicator = true;


            var serviceCategoryCellHeight = 54f * ScaleFactor;
            guestServiceTableView.RowHeight = serviceCategoryCellHeight;


            guestsBackButton.Frame = new CGRect(
                margin, 18f * ScaleFactor,
                20f * ScaleFactor, 30f * ScaleFactor);

            guestsBackButtonLabel.Frame = new CGRect(
                guestsBackButton.Frame.Right, guestsBackButton.Frame.Top,
                46f * ScaleFactor, guestsBackButton.Frame.Height);
            
            lblHostName.Frame = new CGRect(margin + leftMargin, 5f * ScaleFactor, width - (3f * margin) - (90f * ScaleFactor) - leftMargin, 50f * ScaleFactor);
            

            nfloat largeTexts = 27f * ScaleFactor, mediumTexts = 22f * ScaleFactor, smallTexts = 17f * ScaleFactor;
            //string italicFont = "HelveticaNeue-Italic";
            string boldFont = "Helvetica-Bold"; // regularFont = "Helvetica Neue";

            var systemFont = UIFont.SystemFontOfSize(smallTexts, UIFontWeight.Regular);
            var systemFontBold = UIFont.SystemFontOfSize(smallTexts, UIFontWeight.Semibold);
            var systemFontItalic = UIFont.ItalicSystemFontOfSize(smallTexts);

            //lblHostName.AdjustsFontSizeToFitWidth = true;
            lblHostName.Font = UIFont.FromName(boldFont, 22f * ScaleFactor);
            //lblHostLabel.AdjustsFontSizeToFitWidth = true;
            //lblHostLabel.Font = UIFont.FromName(italicFont, smallTexts);
            //lblHostCard.AdjustsFontSizeToFitWidth = true;
            //lblHostCard.Font = UIFont.FromName(italicFont, smallTexts);

            imgGuestCard1.Frame = new CGRect(width - (90f * ScaleFactor) - margin, 5f * ScaleFactor, 90f *ScaleFactor, 50f * ScaleFactor);
            //imgGuestCard2.Frame = new CGRect(imgGuestCard1.Frame.Right + (5f * ScaleFactor), imgGuestCard1.Frame.Top, imgGuestCard1.Frame.Width, imgGuestCard1.Frame.Height);

            guestsBackButton.Font = UIFont.SystemFontOfSize(mediumTexts, UIFontWeight.Regular);
            guestsBackButtonLabel.Font = systemFont;

            updateTableLayout();
        }

        private void updateTableLayout()
        {
            //nfloat height = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
            //    UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ////ratio based on iphone 6 minimum of 50f
            //estimatedHeight = 140f / 650f * height;
            //if (estimatedHeight < 140f)
            //{
            //    estimatedHeight = 140f;
            //}
        }

        private void Close(object sender, EventArgs e)
        {
            this.DismissViewController(true, null);
            if (AppDelegate.ActiveCamera == CameraType.ExternalCamera)
                StartExternalScanner();
            AppDelegate.GuestScanning = false;
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            updateLayout();
        }
    }

    public class GuestCellData : Item
    {
        public Passenger PassengerData { get; private set; }
        
        public GuestCellData(Passenger passenger)
        {
            PassengerData = passenger;

            if (PassengerData == null) return;

            Name = PassengerData.FullName;
            Id = passenger.TrackingRecordID;
            //Text = string.Format("{0} FFN:{1}", PassengerData.FullName, PassengerData.FFN);
        }
    }

    public class GuestCategoryCellData : ExpandableItem
    {
        public PassengerService PassengerServiceData { get; private set; }
        public UIImage GuestCatImage { get; set; }
        
        public new string Name
        {
            get {
                if (PassengerServiceData == null) return "";
                return PassengerServiceData.Name;
                //return string.Format("{0}  [{1}/{2}]", PassengerServiceData.Name, this.Children == null ? 0 : this.Children.Count, PassengerServiceData.MaximumQuantity);
            }
            set {  }
        }


        public GuestCategoryCellData(PassengerService passengerService, List<Passenger> passengers)
        {
            PassengerServiceData = passengerService;
            if (PassengerServiceData == null) return;

            Children = (passengers == null || passengers.Count == 0) ? new List<Item>() : passengers.Select(x => new GuestCellData(x)).Cast<Item>().ToList();
            PassengerServiceData.Quantity = this.Children.Count;//TODO: Why was commented??
            //Name = string.Format("{0}  [{1}/{2}]", PassengerServiceData.Name, this.Children.Count, PassengerServiceData.MaximumQuantity);
        }
    }
    
    /// <summary>
    /// Child Adding supported Table View Source
    /// </summary>
    public class GuestCategoryTableViewSource : ExpandableTableSource<Item>
    {
        protected readonly string ParentCellIdentifier = "guestServiceCategory";
        protected readonly string ChildCellIndentifier = "guestCell";

        public nfloat ScaleFactor { get; set; } = 1f;

        public nfloat ScreenWidth { get; set; }

        public GuestCategoryTableViewSource(IList<Item> items)
            : base(items)
        {

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var item = this.GetItem(indexPath);

            MyCell cell = tableView.DequeueReusableCell(isChild(indexPath) ? ChildCellIndentifier : ParentCellIdentifier) as MyCell;
            
            cell.UpdateRow(item, ScreenWidth, ScaleFactor);
            cell.SetDeleteButtonAction(tableView, indexPath, item, (guest) => {
                AppDelegate.Current.AIMSConfirm("Delete?", AppDelegate.TextProvider.GetText(2520), (ss,ee)=> {
                    if (this.CurrentChildItems.Contains(item))
                    {
                        if (MembershipProvider.Current == null ||
                        MembershipProvider.Current.SelectedPassenger == null ||
                        MembershipProvider.Current.SelectedPassenger.PassengerServices == null ||
                        MembershipProvider.Current.SelectedPassengerService == null) return;
                        //TODO: Delete Guest Passenger (serverCall)
                        var service = MembershipProvider.Current.SelectedPassenger.PassengerServices.FirstOrDefault(x => x.Key.Id == MembershipProvider.Current.SelectedPassengerService.Id);
                        if (service.Value == null || service.Value.Count == 0) return;

                        var passengerToDelete = service.Value.FirstOrDefault(x => x.TrackingRecordID == guest.Id && x.FullName.Equals(guest.Name));//TODO: TrackingID needs to be checked
                        if (passengerToDelete == null) return;
                        service.Value.Remove(passengerToDelete);
                        service.Key.Quantity = service.Value.Count;
                        MembershipProvider.Current.SelectedPassenger.PassengerServices[service.Key] = service.Value;

                        var newIndex = CurrentChildItems.IndexOf(guest);
                        if (newIndex == -1) return;
                        newIndex += SelectedParentIndex + 1;
                        indexPath = NSIndexPath.FromRowSection(newIndex, 0);

                        this.CurrentChildItems.Remove(guest);
                        tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
                        #region Refreshing service list again
                        AppDelegate.ValidationView.RefreshServices();
                        var parentCell = tableView.CellAt(NSIndexPath.FromRowSection(this.SelectedParentIndex, 0));
                        if (parentCell == null) return;
                        var parentCatCell = (GuestCategoryCell)parentCell;
                        var parentItem = this.Items[this.SelectedParentIndex] as GuestCategoryCellData;
                        if (parentItem != null && parentCatCell != null)
                        {
                            parentItem.Children.Remove(parentItem.Children.FirstOrDefault(x=>x.Id == guest.Id && x.Name.Equals(guest.Name)));//TODO: TrackingID needs to be checked
                            parentCatCell.UpdateRow(parentItem, this.ScreenWidth, this.ScaleFactor);
                        }
                        //AppDelegate.GuestServicesView.RefreshCategories();//Didn't work
                        #endregion
                    }
                });
            });

            return cell;
        }

        protected override IEnumerable<Item> GetCurrentChildrenForParent(Item parent)
        {
            if (parent == null) return new List<Item>();
            var children = new List<Item>();

            var parentItem = parent as ExpandableItem;
            if (parentItem != null && parentItem.Children != null)
                children.AddRange(parentItem.Children.Cast<Item>());
            //else
            //    return Enumerable.Empty<Item>();

            var parentCat = parent as GuestCategoryCellData;
                if (parentCat != null && parentCat.PassengerServiceData.MaximumQuantity > children.Count)
                    children.Add(new Item {
                    Parent = parentItem,
                    Id = -1,//meaning the new guest button (TODO: shouldn't be button)
                    Name = "Add New"
                });
            
            return children;
        }
    }
}