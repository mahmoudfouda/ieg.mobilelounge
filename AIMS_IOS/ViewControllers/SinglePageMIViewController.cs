﻿using AIMS;
using AIMS.Models;
using Ieg.Mobile.Localization;
using System;
using UIKit;

namespace AIMS_IOS
{
    public partial class SinglePageMIViewController : UIViewController
    {
        const int NAME_MAX_LENGTH = 20;
        const int FFN_MAX_LENGTH = 16;
        const int FLIGHT_CARRIER_MAX_LENGTH = 3;
        const int FLIGHT_NO_MAX_LENGTH = 4;
        const int CLASS_MAX_LENGTH = 1;

        private nfloat scaleFactor = 1f;

        public LinedView linedView;

        public bool IsGuestMode { get; set; } = false;

        public Passenger SelectedPassenger
        {
            get
            {
                if (MembershipProvider.Current != null)
                {
                    if (IsGuestMode)
                        return MembershipProvider.Current.SelectedGuest;
                    else return MembershipProvider.Current.SelectedPassenger;
                }
                return null;
            }
            set
            {
                if (MembershipProvider.Current != null)
                {
                    if (IsGuestMode)
                        MembershipProvider.Current.SelectedGuest = value;
                    else MembershipProvider.Current.SelectedPassenger = value;
                }
            }
        }

        public SinglePageMIViewController (IntPtr handle) : base (handle)
        {
        }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() => {
                ////cancelButton.SetTitle(AppDelegate.TextProvider.GetText(2004), UIControlState.Normal);

                //passengerTabFullNameLabel.Text = AppDelegate.TextProvider.GetText(2024);// string.Format("{0}: ", App.TextProvider.GetText(2024));//Full Name
                //passengerTabFirstName.Placeholder = AppDelegate.TextProvider.GetText(2025);//First
                ////passengerTabLastName.Placeholder = AppDelegate.TextProvider.GetText(2026);//Last
                //passengerTabLastName.Placeholder = AppDelegate.TextProvider.GetText(2024);//Full Name
                ////txtFullName.Placeholder = AppDelegate.TextProvider.GetText(2024);//Full Name
                //passengerTabloyaltyLabel.Text = AppDelegate.TextProvider.GetText(2027);// string.Format("{0}: ", App.TextProvider.GetText(2027));//Frequent Flyer Number
                //PassengerTabNotesLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2028));//Notes: 
                //passengerTabCarrierLabel.Text = AppDelegate.TextProvider.GetText(2036);
                //flightTitle.Text = AppDelegate.TextProvider.GetText(2016);
                //classTitle.Text = AppDelegate.TextProvider.GetText(2014);
                ///*passengerTabCarrierLabel.Text = string.Format("{0} - {1} - {2}",
                //    AppDelegate.TextProvider.GetText(2036),
                //    AppDelegate.TextProvider.GetText(2016),
                //    AppDelegate.TextProvider.GetText(2014)); //Carrier - Flight Number - Class*/

                //flightTabFromAirportLabel.Text = AppDelegate.TextProvider.GetText(2032);//From (Airport)
                //flightTabToAirportLabel.Text = AppDelegate.TextProvider.GetText(2033);//To (Airport)
                //flightTabFromAirportText.Placeholder = AppDelegate.TextProvider.GetText(2018);//Departure
                //flightTabToAirportText.Placeholder = AppDelegate.TextProvider.GetText(2019);//Arrival

                //flightTabSeatNumberLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2034));//Seat Number:
                //flightTabPnrLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2044));//PNR: 
                //flightTabStatusLabel.Text = string.Format("{0}: ", AppDelegate.TextProvider.GetText(2035));//Passenger Status: 
            });
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            initLinedView();

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;

            
            ////----init card view----
            //passengerTabAirlineCard.Layer.BorderColor = UIColor.Clear.CGColor;

            ////---------


            //passengerTabFirstName.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            //passengerTabLastName.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            //passengerTabLoyaltyText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };

            //carrierText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            //carrierText.AllEditingEvents += carrierTextChanged;

            //flightText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            //flightText.AllEditingEvents += flightTextChanged;

            //classText.ShouldReturn += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            //classText.AllEditingEvents += classTextChanged;

            ////PassengerTabNotesText. += (textfiled) => { ((UITextField)textfiled).ResignFirstResponder(); return true; };
            //PassengerTabNotesText.Changed += onNoteTextChanged;

            //passengerTabLastName.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
            //    var length = textField.Text.Length - range.Length + replacementString.Length;
            //    return length <= NAME_MAX_LENGTH;
            //};
            //passengerTabLoyaltyText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
            //    var length = textField.Text.Length - range.Length + replacementString.Length;
            //    return length <= FFN_MAX_LENGTH;
            //};
            //carrierText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
            //    var length = textField.Text.Length - range.Length + replacementString.Length;
            //    return length <= FLIGHT_CARRIER_MAX_LENGTH;
            //};
            //flightText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
            //    var length = textField.Text.Length - range.Length + replacementString.Length;
            //    return length <= FLIGHT_NO_MAX_LENGTH;
            //};
            //classText.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) => {
            //    var length = textField.Text.Length - range.Length + replacementString.Length;
            //    return length <= CLASS_MAX_LENGTH;
            //};

            ////Did it in UI
            ////passengerTabFirstName.AllEditingEvents += (oo,ss) => { passengerTabFirstName.Text = passengerTabFirstName.Text.ToUpper(); };
            ////passengerTabLastName.AllEditingEvents += (oo, ss) => { passengerTabLastName.Text = passengerTabLastName.Text.ToUpper(); };
            ////PassengerTabNotesText.Changed += (oo,ss) => { PassengerTabNotesText.Text = PassengerTabNotesText.Text.ToUpper(); };

            //var swipeLeft = new UISwipeGestureRecognizer(HandleLeftSwipe)
            //{ Direction = UISwipeGestureRecognizerDirection.Left };
            //View.AddGestureRecognizer(swipeLeft);

            //tap = new UITapGestureRecognizer((textfiled) =>
            //{
            //    UIView temp = AutoScrollUtility.findFirstResponder(this.View);

            //    if (temp != null)
            //    {
            //        temp.ResignFirstResponder();
            //    }
            //});

            //this.View.AddGestureRecognizer(tap);

            //cancelButton.TouchUpInside += onCancel;
            //topButton.TouchUpInside += onCancel;
            //scanButtonMI1.TouchUpInside += scanClicked;

            ////PassengerTabNotesText.Layer.MasksToBounds = true;
            //PassengerTabNotesText.Layer.CornerRadius = 4f;
            ////PassengerTabNotesText.Layer.BorderColor = new CGColor(0f, 0f, 0f);
            //PassengerTabNotesText.Layer.BorderWidth = 1f;

            //nextButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            //nextButton.Layer.BorderWidth = 1f;
            //nextButton.TitleLabel.AdjustsFontSizeToFitWidth = false;

            //scanButtonMI1.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            //scanButtonMI1.Layer.BorderWidth = 1f;
            //scanButtonMI1.TitleLabel.AdjustsFontSizeToFitWidth = false;

            //cancelButton.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            //cancelButton.Layer.BorderWidth = 1f;
            //cancelButton.TitleLabel.AdjustsFontSizeToFitWidth = false;

            ////LoadPassenger();

            //topTitle.AdjustsFontSizeToFitWidth = true;
            //passengerTabAirlineNameLabel.AdjustsFontSizeToFitWidth = true;
            //passengerTabAirlineLabel.AdjustsFontSizeToFitWidth = true;

            //passengerTabFullNameLabel.AdjustsFontSizeToFitWidth = true;
            //passengerTabLastName.AdjustsFontSizeToFitWidth = true;
            //passengerTabFirstName.AdjustsFontSizeToFitWidth = true;

            //passengerTabLoyaltyText.AdjustsFontSizeToFitWidth = true;

            ////passengerTabCarrierLabel.AdjustsFontSizeToFitWidth = true;
            ////classTitle.AdjustsFontSizeToFitWidth = true;
            ////flightTitle.AdjustsFontSizeToFitWidth = true;

            //topAirlineImage.Image = AppData.usersAirLine.image;
            //topTitle.Text = MembershipProvider.Current.UserHeaderText;

            //keyboardShowNotification = UIKeyboard.Notifications.ObserveWillShow(keyboardWillShow);
            //keyboardHideNotification = UIKeyboard.Notifications.ObserveWillHide(keyboardWillHide);
        }

        public void initLinedView()
        {
            linedView = new LinedView();
            

            //this.View.add
            //this.passengerTabScrollView.AddSubview(linedView);
        }


        public void setFonts()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                nfloat xxLargeTexts = 26f * scaleFactor, xLargeTexts = 24f * scaleFactor, largeTexts = 22f * scaleFactor, medium = 17f * scaleFactor, smallTexts = 14f * scaleFactor;
                string /*italicFontName = "HelveticaNeue-Italic",*/ boldFontName = "Helvetica-Bold", regularFontName = "Helvetica Neue";

                var titleFont = UIFont.FromName(regularFontName, xxLargeTexts);
                var xLargeFont = UIFont.FromName(regularFontName, xLargeTexts);
                var xLargeBoldFont = UIFont.FromName(boldFontName, xLargeTexts);
                var largeFont = UIFont.FromName(regularFontName, largeTexts);
                var largeBoldFont = UIFont.FromName(boldFontName, largeTexts);
                var smallFont = UIFont.FromName(regularFontName, smallTexts);
                var mediumFont = UIFont.FromName(regularFontName, medium);
                var mediumBoldFont = UIFont.FromName(boldFontName, medium);

                ////top view
                ////topTitle.Font = titleFont; //UIFont.FromName("font name", 20f);

                ////airline view
                //passengerTabAirlineNameLabel.Font = titleFont; //UIFont.FromName("Helvetica-Bold", 24f);

                ////card view
                //passengerTabAirlineLabel.Font = xLargeFont; //UIFont.FromName("Helvetica Neue", 24f);

                ////passenger view
                //passengerTabFullNameLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                //passengerTabLastName.Font = smallFont; //UIFont.FromName("Helvetica-Bold", 22f);
                //passengerTabFirstName.Font = smallFont; //UIFont.FromName("Helvetica-Bold", 22f);

                ////carrier view
                //passengerTabloyaltyLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                //passengerTabLoyaltyText.Font = smallFont; //UIFont.FromName("Helvetica Neue", 22f);

                //passengerTabCarrierLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                //classTitle.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                //flightTitle.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);

                //carrierText.Font = smallFont; //UIFont.FromName("font name", 20f);
                //flightText.Font = smallFont; //UIFont.FromName("font name", 20f);
                //classText.Font = smallFont; //UIFont.FromName("font name", 20f);

                ////note view
                //PassengerTabNotesLabel.Font = mediumFont; //UIFont.FromName("Helvetica Neue", 22f);
                //PassengerTabNotesText.Font = smallFont; //UIFont.FromName("Helvetica Neue", 22f);

                ////buttons
                //scanButtonMI1.Font = UIFont.SystemFontOfSize(smallTexts); //UIFont.FromName("Helvetica Neue", 17f);
                //cancelButton.Font = UIFont.SystemFontOfSize(medium);
                //nextButton.Font = UIFont.SystemFontOfSize(medium);
            }
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            //this.passengerTabScrollView.ContentSize = new CoreGraphics.CGSize(UIScreen.MainScreen.Bounds.Width, 700);

            updateLayout();

            setFonts();
        }

        private void updateLayout()
        {
            //throw new NotImplementedException();
        }

        public void Close()
        {
            this.DismissViewController(true, null);

            if (AppDelegate.ContinueScanning && !IsGuestMode)
                AppDelegate.AirlinesView.Scan();
            else IsGuestMode = false;
        }
    }
}