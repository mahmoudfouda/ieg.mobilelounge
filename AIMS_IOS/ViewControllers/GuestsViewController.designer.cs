﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AIMS_IOS
{
    [Register ("GuestsViewController")]
    partial class GuestsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton guestsBackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton guestsBackButtonLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView guestServiceTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgGuestCard1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblHostName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView pnlHost { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView pnlTop { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (guestsBackButton != null) {
                guestsBackButton.Dispose ();
                guestsBackButton = null;
            }

            if (guestsBackButtonLabel != null) {
                guestsBackButtonLabel.Dispose ();
                guestsBackButtonLabel = null;
            }

            if (guestServiceTableView != null) {
                guestServiceTableView.Dispose ();
                guestServiceTableView = null;
            }

            if (imgGuestCard1 != null) {
                imgGuestCard1.Dispose ();
                imgGuestCard1 = null;
            }

            if (lblHostName != null) {
                lblHostName.Dispose ();
                lblHostName = null;
            }

            if (pnlHost != null) {
                pnlHost.Dispose ();
                pnlHost = null;
            }

            if (pnlTop != null) {
                pnlTop.Dispose ();
                pnlTop = null;
            }
        }
    }
}