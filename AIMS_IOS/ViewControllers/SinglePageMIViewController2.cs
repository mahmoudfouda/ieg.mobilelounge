﻿using AIMS;
using AIMS.Models;
using AIMS_IOS.Utility;
using CoreGraphics;
using Foundation;
using Ieg.Mobile.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace AIMS_IOS
{
    public partial class SinglePageMIViewController2 : AIMSViewController, IPassengerRefresher
    {
        const int NAME_MAX_LENGTH = 20;
        const int FFN_MAX_LENGTH = 16;
        const int FLIGHT_CARRIER_MAX_LENGTH = 3;
        const int FLIGHT_NO_MAX_LENGTH = 5;
        const int CLASS_MAX_LENGTH = 1;
        const int AIRPORT_MAX_LENGTH = 10;//Just for search. Else it was 3
        const int SEAT_NO_MAX_LENGTH = 4;
        const int PNR_MAX_LENGTH = 7;

        LinedView linedView;
        UITextField editingAirportBox = null;
        AirportsTableSource airportsSource;
        bool isLandScape = false, isPad = false;

        public bool IsGuestMode { get; set; } = false;

        public Passenger SelectedPassenger
        {
            get
            {
                if (MembershipProvider.Current != null)
                {
                    if (IsGuestMode)
                        return MembershipProvider.Current.SelectedGuest;
                    else return MembershipProvider.Current.SelectedPassenger;
                }
                return null;
            }
            set
            {
                if (MembershipProvider.Current != null)
                {
                    if (IsGuestMode)
                        MembershipProvider.Current.SelectedGuest = value;
                    else MembershipProvider.Current.SelectedPassenger = value;
                }
            }
        }

        public void SavePassenger()
        {
            InvokeOnMainThread(() =>
            {
                if (MembershipProvider.Current == null) return;//in case of session timeout (which logs out the user)

                if (SelectedPassenger == null)//Keeping the passenger object allways instanciated and ready...
                    SelectedPassenger = new Passenger();

                #region Set the passenger Info (You must have all the TextBoxes)
                SelectedPassenger.FullName = txtFullNameSPMI2.Text.Trim().ToUpper();

                SelectedPassenger.FFN = txtFFNSPMI2.Text.ToUpper();
                SelectedPassenger.Notes = txtNoteSPMI2.Text.ToUpper();

                SelectedPassenger.FlightCarrier = txtCarrierSPMI2.Text.ToUpper();
                SelectedPassenger.FlightNumber = string.IsNullOrWhiteSpace(txtFlightSPMI2.Text) ? string.Empty : txtFlightSPMI2.Text.ToUpper().PadLeft(4,'0');
                SelectedPassenger.TrackingClassOfService = txtClassSPMI2.Text.ToUpper();

                SelectedPassenger.FromAirport = txtFromSPMI2.Text.ToUpper();
                SelectedPassenger.ToAirport = txtToSPMI2.Text.ToUpper();
                SelectedPassenger.SeatNumber = txtSeatSPMI2.Text.ToUpper();

                SelectedPassenger.PNR = txtPNRSPMI2.Text.ToUpper();
                //SelectedPassenger.PassengerStatus = txtStatus.Text;
                SelectedPassenger.TrackingPassCombinedFlightNumber = "";
                #endregion
            });
        }

        private void LoadPassenger()
        {
            InvokeOnMainThread(() =>
            {
                #region Resetting Inputs
                imgSelectedAirlineSPMI2.Image = null;
                imgCardSPMI2.Image = null;
                txtFullNameSPMI2.Text = txtToSPMI2.Text = txtSeatSPMI2.Text = txtClassSPMI2.Text =
                    txtPNRSPMI2.Text = txtFromSPMI2.Text = txtCarrierSPMI2.Text = txtFlightSPMI2.Text = string.Empty;
                #endregion

                if (SelectedPassenger == null) return;

                #region Filling Images
                if (MembershipProvider.Current.SelectedAirline != null)
                {
                    #region Filling the Airline
                    //passengerTabAirlineNameLabel.Text = MembershipProvider.Current.SelectedAirline.Name.ToUpper();
                    if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes != null && MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length > 0)
                    {
                        //airlineImage.Image = AppData.selectedAirLine.image;
                        imgSelectedAirlineSPMI2.Image = MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.ToImage();
                    }
                    else
                    {
                        ImageAdapter.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (imageBytes) =>
                        {
                            var image = imageBytes.ToImage();
                            InvokeOnMainThread(() =>
                            {
                                imgSelectedAirlineSPMI2.Image = image;
                            });
                        });
                    }
                    #endregion
                }

                if (MembershipProvider.Current.SelectedCard != null)
                {
                    #region Filling the Card
                    lblCardNameSPMI2.Text = MembershipProvider.Current.SelectedCard.Name;//DisplayName has dots at the end (avoid that)

                    if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                    {
                        imgCardSPMI2.Image = MembershipProvider.Current.SelectedCard.CardPictureBytes.ToImage();
                    }
                    else
                    {
                        ImageAdapter.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (imageBytes) =>
                        {
                            var image = imageBytes.ToImage();
                            InvokeOnMainThread(() =>
                            {
                                imgCardSPMI2.Image = image;
                            });
                        });
                    }
                    #endregion
                }
                #endregion

                #region Show Passenger first and last name
                var names = new List<string>();
                if (AppData.ValidationResult.PassengerNames.Count > 0)
                {
                    names = AppData.ValidationResult.PassengerNames;
                }
                else if (!string.IsNullOrEmpty(SelectedPassenger.FullName))
                    names.Add(SelectedPassenger.FullName);

                if (names.Count > 0)
                    txtFullNameSPMI2.Text = names.Last();

                if (AppData.ValidationResult.IsNameRequired || AppData.ValidationResult.PassengerNames.Count > 0)
                {
                    GradientUtility.addErrorGradiant(txtFullNameSPMI2);
                }
                else
                {
                    GradientUtility.removeGradiants(txtFullNameSPMI2);
                }
                #endregion

                #region Show Passenger FFN
                if (AppData.ValidationResult.IsFFNRequired)
                {
                    //passengerTabLoyaltyText.BackgroundColor = UIColor.Yellow;
                    GradientUtility.addErrorGradiant(txtFFNSPMI2);
                }
                else
                {
                    GradientUtility.removeGradiants(txtFFNSPMI2);
                }

                txtFFNSPMI2.Text = SelectedPassenger.FFN;
                #endregion


                #region Show Passenger Flight Info
                if (AppData.ValidationResult.IsFlightInfoRequired)
                {
                    GradientUtility.addErrorGradiant(txtCarrierSPMI2);
                    GradientUtility.addErrorGradiant(txtFlightSPMI2);
                    GradientUtility.addErrorGradiant(txtClassSPMI2);

                }
                else
                {
                    GradientUtility.removeGradiants(txtCarrierSPMI2);
                    GradientUtility.removeGradiants(txtFlightSPMI2);
                    GradientUtility.removeGradiants(txtClassSPMI2);
                }

                #region TODO: show Passenger Flight Carrier
                if (string.IsNullOrWhiteSpace(SelectedPassenger.FlightCarrier))
                {
                    if (MembershipProvider.Current.SelectedAirline != null)//TODO: (Siavash )????
                    {
                        txtCarrierSPMI2.Text = MembershipProvider.Current.SelectedAirline.Code;
                    }
                }
                else
                {
                    txtCarrierSPMI2.Text = SelectedPassenger.FlightCarrier;
                }
                #endregion
                
                txtFlightSPMI2.Text = string.IsNullOrWhiteSpace(SelectedPassenger.FlightNumber) ? string.Empty : SelectedPassenger.FlightNumber.TrimStart('0');
                txtClassSPMI2.Text = SelectedPassenger.TrackingClassOfService;
                #endregion

                #region Show Passenger Note
                if (AppData.ValidationResult.IsNotesRequired)
                {
                    GradientUtility.addErrorGradiant(txtNoteSPMI2);
                }
                else
                {
                    GradientUtility.removeGradiants(txtNoteSPMI2);
                }

                txtNoteSPMI2.Text = SelectedPassenger.Notes;
                #endregion


                txtSeatSPMI2.Text = SelectedPassenger.SeatNumber;

                #region Filling the PNR
                txtPNRSPMI2.Text = SelectedPassenger.PNR;
                //flightTabStatusText.Text = SelectedPassenger.PassengerStatus;

                if (AppData.ValidationResult.IsPNRRequired)
                {
                    GradientUtility.addErrorGradiant(txtPNRSPMI2);
                }
                else
                {
                    GradientUtility.removeGradiants(txtPNRSPMI2);
                }
                #endregion

                #region Filling the Origin Destination
                txtFromSPMI2.Text = MembershipProvider.Current.SelectedLounge.AirportCode;
                LoadFullAirportName(txtFromSPMI2.Text, lblFromFullSPMI2);
                txtToSPMI2.Text = SelectedPassenger.ToAirport;

                if (AppData.ValidationResult.IsDestinationOriginRequired)
                {
                    GradientUtility.addErrorGradiant(txtFromSPMI2);
                    GradientUtility.addErrorGradiant(txtToSPMI2);
                }
                else
                {
                    GradientUtility.removeGradiants(txtFromSPMI2);
                    GradientUtility.removeGradiants(txtToSPMI2);
                }
                #endregion

                #region Fake Filling
                lblFromFullSPMI2.Text = this.txtFromSPMI2.Text;
                lblToFullSPMI2.Text = this.txtToSPMI2.Text;

                #endregion


                lblSelectedGuestType.Hidden = !IsGuestMode;
                if (IsGuestMode)
                {
                    //selectedAirlineViewSPMI2.BackgroundColor = UIColor.FromCGColor(new CGColor(1f, 102f / 255f, 36f / 255f, 1));//Orange
                    selectedAirlineViewSPMI2.BackgroundColor = UIColor.Clear.FromHexString("#9370db");
                    lblSelectedGuestType.Text = $"{MembershipProvider.Current.SelectedPassengerService.Name} {AppDelegate.TextProvider.GetText(2154)}"; 
                }
                else
                    selectedAirlineViewSPMI2.BackgroundColor = UIColor.Clear.FromHexString("#0e79d8");
                    //selectedAirlineViewSPMI2.BackgroundColor = UIColor.FromCGColor(new CGColor(14f / 255f, 121f / 255f, 216f / 255f, 1));//Blue

                imgSelectedAirlineViewSPMI2.Layer.BorderColor = selectedAirlineViewSPMI2.BackgroundColor.CGColor;
                
                if (AppDelegate.AirlinesView.IsSecondScan && !AppDelegate.IsScanningCancelled)
                    StartScanning();
            });
        }

        public void resetAllGradient()
        {
            GradientUtility.removeGradiants(txtFullNameSPMI2);
            GradientUtility.removeGradiants(txtFFNSPMI2);
            GradientUtility.removeGradiants(txtCarrierSPMI2);
            GradientUtility.removeGradiants(txtFlightSPMI2);
            GradientUtility.removeGradiants(txtClassSPMI2);
            GradientUtility.removeGradiants(txtNoteSPMI2);
        }

        public SinglePageMIViewController2(IntPtr handle) : base(handle)
        {
        }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() =>
            {
                //cancelButton.SetTitle(AppDelegate.TextProvider.GetText(2004), UIControlState.Normal);

                txtFlightSPMI2.Placeholder = AppDelegate.TextProvider.GetText(2015);//Flight
                txtPNRSPMI2.Placeholder = AppDelegate.TextProvider.GetText(2044);//PNR
                txtFullNameSPMI2.Placeholder = AppDelegate.TextProvider.GetText(2024);//Full Name
                txtFFNSPMI2.Placeholder = AppDelegate.TextProvider.GetText(2027);//Loyalty Number

                var fromText = AppDelegate.TextProvider.GetText(2032);//From (Airport)
                if (fromText.IndexOf(" ") >= 0)
                    fromText = fromText.Substring(0, fromText.IndexOf(" "));//From
                txtFromSPMI2.Placeholder = fromText;

                var toText = AppDelegate.TextProvider.GetText(2033);//To (Airport)
                if (toText.IndexOf(" ") >= 0)
                    toText = toText.Substring(0, toText.IndexOf(" "));//To
                txtToSPMI2.Placeholder = toText;

                txtNoteSPMI2.Placeholder = AppDelegate.TextProvider.GetText(2028);//Notes
                txtSeatSPMI2.Placeholder = AppDelegate.TextProvider.GetText(2063);//Seat

                if (IsGuestMode)
                    lblSelectedGuestType.Text = $"{MembershipProvider.Current.SelectedPassengerService.Name} {AppDelegate.TextProvider.GetText(2154)}";


                //lblSelectedGuestType;
            });
        }

        private bool CheckLength(UITextField textField, NSRange range, string replacementString, int maxLength)
        {
            if (!string.IsNullOrWhiteSpace(replacementString))
                replacementString = replacementString.ToUpper();
            var length = textField.Text.Length - range.Length + replacementString.Length;
            if (length <= maxLength)
            {
                textField.Text = textField.Text.ToUpper();
                return true;
            }
            return false;
        }

        private void LoadFullAirportName(string IATA_Code, UILabel label)
        {
            InvokeOnMainThread(() =>
            {
                label.Text = "...";
            });
            using (var ar = new AirportsRepository())
            {
                ar.OnRepositoryChanged += (data) =>
                {
                    var airport = (Airport)data;
                    if (airport != null)
                        InvokeOnMainThread(() =>
                        {
                            label.Text = airport.Name;
                        });
                };
                ar.GetAirport(IATA_Code, true);
            }
        }

        private void ResetAutoComplete(UITextField textBox)
        {
            airportTableView.Hidden = true;
            if (textBox == null) return;

            editingAirportBox = textBox;
            airportTableView.Frame = new CGRect(textBox.Frame.X, textBox.Frame.Bottom,
                280f * ScaleFactor, 110f * ScaleFactor);
            airportTableView.Layer.CornerRadius = 4f * ScaleFactor;
            airportTableView.RowHeight = AirportsTableSource.AUTOCOMPLETE_ROW_HEIGHT * ScaleFactor;
            airportTableView.Layer.BorderColor = UIColor.DarkGray.CGColor;
            airportTableView.Layer.BorderWidth = 1f;
        }

        private void StartScanning()
        {
            AppDelegate.IsScanningCancelled = false;
            SavePassenger();
            if (AppDelegate.ActiveCamera == CameraType.ExternalCamera) return;
            if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR)
            {
                AIMSMessage(AppDelegate.TextProvider.GetText(2531), AppDelegate.TextProvider.GetText(2532));
                return;
            }
            //if (closeThis)
            this.DismissViewController(true, () =>
            {
                AppDelegate.AirlinesView.Scan(true);
            });
            //else AppDelegate.AirlinesView.Scan(true);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;

            InitLinedView();



            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;

            btnBackSPMI2.TouchUpInside += (ss, ee) => { Close(); };
            btnBackTextSPMI2.TouchUpInside += (ss, ee) => { Close(); };
            btnScanSPMI2.TouchUpInside += (ss, ee) =>
            {
                StartScanning();
            };
            btnConfirmSPMI2.TouchUpInside += Confirm;

            airportTableView.Source = airportsSource = new AirportsTableSource(this, airportTableView);

            airportsSource.OnAirportSelected += (selectedAirport) =>
            {
                if (editingAirportBox != null)
                {
                    editingAirportBox.Text = selectedAirport.IATA_Code;
                    //var responder = AutoScrollUtility.findFirstResponder(this.View);

                    if (editingAirportBox == txtFromSPMI2)
                    {
                        lblFromFullSPMI2.Text = selectedAirport.Name;
                        txtToSPMI2.BecomeFirstResponder();
                    }
                    else
                    {
                        lblToFullSPMI2.Text = selectedAirport.Name;
                        txtNoteSPMI2.BecomeFirstResponder();
                    }
                }
            };

            txtFromSPMI2.EditingDidBegin += (ss, ee) =>
            {
                ResetAutoComplete((UITextField)ss);
            };
            txtFromSPMI2.EditingDidEnd += (ss, ee) =>
            {
                ResetAutoComplete(null);
            };

            txtToSPMI2.EditingDidBegin += (ss, ee) =>
            {
                ResetAutoComplete((UITextField)ss);
            };
            txtToSPMI2.EditingDidEnd += (ss, ee) =>
            {
                ResetAutoComplete(null);
            };

            ResetAutoComplete(null);


            AppDelegate.Current.OnExternalScannerStatusChanged += (sender, e) =>
            {
                InvokeOnMainThread(() =>
                {
                    try
                    {
                        btnScanSPMI2.Enabled = e.State != InfineaSDK.iOS.ConnStates.ConnConnected;
                    }
                    catch { }
                });
            };

            txtCarrierSPMI2.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                return CheckLength(textField, range, replacementString, FLIGHT_CARRIER_MAX_LENGTH);
            };
            txtCarrierSPMI2.ShouldReturn += (textfiled) =>
            {
                txtFlightSPMI2.BecomeFirstResponder();
                return true;
            };
            txtFlightSPMI2.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                GradientUtility.removeGradiants(txtFlightSPMI2);
                var text = textField.Text + replacementString;
                if (string.IsNullOrEmpty(replacementString) && range.Length == 1 && text.Length > 0)
                    text = text.Substring(0, text.Length - 1);
                if (!text.IsFlightNumber())
                    GradientUtility.addErrorGradiant(txtFlightSPMI2);
                return CheckLength(textField, range, replacementString, FLIGHT_NO_MAX_LENGTH);
            };
            txtFlightSPMI2.ShouldReturn += (textfiled) =>
            {
                txtClassSPMI2.BecomeFirstResponder();
                return true;
            };
            txtClassSPMI2.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                return CheckLength(textField, range, replacementString, CLASS_MAX_LENGTH);
            };
            txtClassSPMI2.ShouldReturn += (textfiled) =>
            {
                txtPNRSPMI2.BecomeFirstResponder();
                return true;
            };
            txtPNRSPMI2.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                return CheckLength(textField, range, replacementString, PNR_MAX_LENGTH);
            };
            txtPNRSPMI2.ShouldReturn += (textfiled) =>
            {
                txtFullNameSPMI2.BecomeFirstResponder();
                return true;
            };

            txtFullNameSPMI2.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                return CheckLength(textField, range, replacementString, NAME_MAX_LENGTH);
            };
            txtFullNameSPMI2.ShouldReturn += (textfiled) =>
            {
                txtFFNSPMI2.BecomeFirstResponder();
                return true;
            };
            txtFFNSPMI2.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                return CheckLength(textField, range, replacementString, FFN_MAX_LENGTH);
            };
            txtFFNSPMI2.ShouldReturn += (textfiled) =>
            {
                txtFromSPMI2.BecomeFirstResponder();
                return true;
            };

            txtFromSPMI2.ShouldChangeCharacters = txtToSPMI2.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                var keyword = string.Empty;
                if (range.Length == 1 && string.IsNullOrEmpty(replacementString))
                {
                    keyword = textField.Text.Trim().Substring(0, textField.Text.Length - (int)range.Length);
                }
                else keyword = string.Format("{0}{1}", textField.Text.Trim(), replacementString.Trim());
                airportsSource.SearchAirports(keyword);
                return CheckLength(textField, range, replacementString, AIRPORT_MAX_LENGTH);
            };
            txtFromSPMI2.ShouldReturn += (textfiled) =>
            {
                txtToSPMI2.BecomeFirstResponder();
                return true;
            };
            txtToSPMI2.ShouldReturn += (textfiled) =>
            {
                txtNoteSPMI2.BecomeFirstResponder();
                return true;
            };

            txtNoteSPMI2.ShouldReturn += (textfiled) =>
            {
                txtSeatSPMI2.BecomeFirstResponder();
                return true;
            };

            txtSeatSPMI2.ShouldChangeCharacters = (UITextField textField, NSRange range, string replacementString) =>
            {
                return CheckLength(textField, range, replacementString, SEAT_NO_MAX_LENGTH);
            };
            txtSeatSPMI2.ShouldReturn += (textfiled) =>
            {
                textfiled.ResignFirstResponder();
                Confirm(this, null);
                return true;
            };

            var tap = new UITapGestureRecognizer((textfiled) =>
            {
                UIView temp = AutoScrollUtility.findFirstResponder(this.View);

                if (!airportTableView.Hidden && (temp == txtFromSPMI2 || temp == txtToSPMI2)) return;

                if (temp != null)
                {
                    temp.ResignFirstResponder();
                }
            });

            tap.CancelsTouchesInView = false;
            View.AddGestureRecognizer(tap);

            //keyboardShowNotification = UIKeyboard.Notifications.ObserveWillShow(keyboardWillShow);
            //keyboardHideNotification = UIKeyboard.Notifications.ObserveWillHide(keyboardWillHide);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            UpdateLayout();
            HideLoading();
            if (MembershipProvider.Current == null)
            {
                AppDelegate.ReturnToLoginView();
            }
            else
            {
                if(!IsGuestMode)
                    AppDelegate.AirlinesView.IsSecondScan = true;
                LoadPassenger();
                LoadTexts();
                txtCarrierSPMI2.BecomeFirstResponder();
            }
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            //A simple trick for refreshing keyboard
            System.Threading.Tasks.Task.Run(() =>
            {
                System.Threading.Tasks.Task.Delay(100).Wait();

                InvokeOnMainThread(() =>
                {
                    txtFlightSPMI2.BecomeFirstResponder();
                });
            });
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            resetAllGradient();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            //this.passengerTabScrollView.ContentSize = new CoreGraphics.CGSize(UIScreen.MainScreen.Bounds.Width, 700);//=> new height 647*scaleFactor
            UpdateLayout();

            setFonts();
        }

        public void keyboardWillShow(object o, UIKeyboardEventArgs e)
        {
            //CGRect keyboardFrame = e.FrameEnd;

            //UIView firstResponder = AutoScrollUtility.findFirstResponder(passengerTabScrollView);
            //if (firstResponder == null)
            //{
            //    return;
            //}

            //AutoScrollUtility.autoScrollTo(this, passengerTabScrollView,
            //    firstResponder.Superview.Frame.Y + firstResponder.Frame.Bottom, keyboardFrame.Height);
        }

        public void keyboardWillHide(object o, UIKeyboardEventArgs e)
        {
            //nfloat currentY = passengerTabScrollView.ContentOffset.Y;

            ////go to top
            //if ((scrollMaxHeight * scaleFactor) < passengerTabScrollView.Frame.Height)
            //{
            //    passengerTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
            //    passengerTabScrollView.SetContentOffset(new CGPoint(0f, 0f), true);
            //}//stay
            //else if (currentY < (scrollMaxHeight * scaleFactor) - passengerTabScrollView.Frame.Height)
            //{
            //    passengerTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
            //}
            //else//go to bottom
            //{
            //    passengerTabScrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width, scrollMaxHeight * scaleFactor);
            //    passengerTabScrollView.SetContentOffset(new CGPoint(0f, (scrollMaxHeight * scaleFactor) - passengerTabScrollView.Frame.Height), true);
            //}

        }

        public void Confirm(object sender, EventArgs e)
        {
            if (MembershipProvider.Current == null)
            {
                AppDelegate.ReturnToLoginView();
                return;
            }
            SavePassenger();

            ShowLoading(AppDelegate.TextProvider.GetText(2507));

            if (IsGuestMode)
                AppManager.validateGuestInfo(SelectedPassenger, (PassengerServiceType)MembershipProvider.Current.SelectedPassengerService.Id, OnGuestChecked);
            else AppManager.validatePassengerInfo(OnPassengerChecked);
        }

        private void OnPassengerChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            HideLoading();
            if (MembershipProvider.Current == null)
            {
                AppDelegate.ReturnToLoginView();
                return;
            }

            InvokeOnMainThread(() =>
            {
                SelectedPassenger = passenger;
                if (isSucceeded)
                {
                    if (errorCode.HasValue && (ErrorCode)errorCode.Value == ErrorCode.PassengerNameMismatch)
                    {
                        #region Passenger name mismatch (agent decision)
                        Question(AppDelegate.TextProvider.GetText(1104),
                                    string.Format("{0}{1}",
                                    (errorCode.HasValue ?
                                        string.Format("{0}\n", AppDelegate.TextProvider.GetText((int)errorCode)) : ""),
                                        message),
                                    (ss, ee) =>
                                    {
                                        //Focus on the name text box
                                        InvokeOnMainThread(() =>
                                        {
                                            txtFullNameSPMI2.BecomeFirstResponder();
                                        });
                                    },
                                    (ss, ee) =>
                                    {
                                        if (passenger != null)
                                        {
                                            PassengersRepository.AddFailedPassenger(passenger, message);
                                        }
                                        AppManager.rejectPassenger();
                                        try
                                        {
                                            AppDelegate.ValidationView.DismissViewController(true, null);
                                            AppDelegate.SinglePageMIView2.DismissViewController(true, null);
                                        }
                                        catch (Exception ex)
                                        {
                                            //TODO: test it and remove the try-catch
                                            var m = ex.Message;
                                            Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                                        }
                                    });
                        #endregion
                    }
                    else
                    {
                        #region Passenger check success
                        //AppDelegate.SinglePageMIView2.Close();//Why??!
                        if (AppDelegate.SinglePageMIView2.IsShown)
                        {
                            this.DismissViewController(true, null);
                        }
                        if (PresentedViewController != AppDelegate.ValidationView)
                        {
                            AppDelegate.ValidationView.ProvidesPresentationContextTransitionStyle = true;
                            AppDelegate.ValidationView.DefinesPresentationContext = true;
                            AppDelegate.ValidationView.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
                            AppDelegate.AirlinesView.PresentViewController(AppDelegate.ValidationView, true, null);
                        }
                        #endregion

                        AppManager.UpdateProgress();
                    }
                }
                else
                {
                    AppData.ValidationResult = string.IsNullOrWhiteSpace(errorMetadata) ? new PassengerValidation() : new PassengerValidation(errorMetadata);
                    if (passenger != null)
                    {
                        PassengersRepository.AddFailedPassenger(passenger, message);
                        //LoadPassenger();
                    }
                    AIMSError(AppDelegate.TextProvider.GetText(1104), message, null, 3000); // TODO: Show failed and after 3 seconds return back to this screen (manual input)
                    //if (!(errorCode.HasValue && errorCode.Value == (int)ErrorCode.NeedToSwipeBoardingPass))//only in case of Incomplete Info we don't close the form
                    //    AppDelegate.ManualPassengerView.Close();//Closing the MI form in case of passenger validation failed (isSucceeded = false)
                    if (errorCode.HasValue && errorCode.Value == (int)ErrorCode.NeedToSwipeBoardingPass)//only in case of Incomplete Info we don't close the form
                    {
                        RefreshPassenger();
                    }
                    else
                        AppDelegate.SinglePageMIView2.Close();//Closing the MI form in case of passenger validation failed (isSucceeded = false)
                }
            });
        }

        private void OnGuestChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            //InvokeOnMainThread(() => {
            HideLoading();
            if (MembershipProvider.Current == null)
            {
                AppDelegate.ReturnToLoginView();
                return;
            }
            if (IsGuestMode)
                AppDelegate.GuestServicesView.OnGuestChecked(isSucceeded, errorCode, errorMetadata, message, passenger);
            //});
        }

        private void InitLinedView()
        {
            linedView = new LinedView();

            this.scrollViewSPMI2.AddSubview(linedView);
        }

        private void setFonts()
        {
            //if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            //{
            nfloat xxLargeTexts = 26f * ScaleFactor, xLargeTexts = 24f * ScaleFactor, largeTexts = 22f * ScaleFactor, medium = 17f * ScaleFactor, smallTexts = 15f * ScaleFactor, xSmallTexts = 13f * ScaleFactor;
            string /*italicFontName = "HelveticaNeue-Italic",*/ boldFontName = "Helvetica-Bold", regularFontName = "Helvetica Neue";

            var titleFont = UIFont.FromName(regularFontName, xxLargeTexts);
            var xLargeFont = UIFont.FromName(regularFontName, xLargeTexts);
            var xLargeBoldFont = UIFont.FromName(boldFontName, xLargeTexts);
            var largeFont = UIFont.FromName(regularFontName, largeTexts);
            var largeBoldFont = UIFont.FromName(boldFontName, largeTexts);
            var smallFont = UIFont.FromName(regularFontName, smallTexts);
            //var xSmallFont = UIFont.FromName(regularFontName, xSmallTexts);
            var mediumFont = UIFont.FromName(regularFontName, medium);
            var mediumBoldFont = UIFont.FromName(boldFontName, medium);

            //top view
            //topTitle.Font = titleFont; //UIFont.FromName("font name", 20f);
            lblAgentAirlineSPMI2.Font = largeFont;
            lblAgentNameSPMI2.Font = smallFont;
            lblCardNameSPMI2.Font = smallFont;

            //airline view
            lblSelectedGuestType.Font = mediumBoldFont;
            txtCarrierSPMI2.Font = smallFont;
            txtFlightSPMI2.Font = smallFont;
            txtClassSPMI2.Font = smallFont;
            txtPNRSPMI2.Font = smallFont;

            //passenger view
            txtFullNameSPMI2.Font = mediumFont;
            txtFFNSPMI2.Font = mediumFont;

            //Flight View
            txtFromSPMI2.Font = mediumFont;
            txtToSPMI2.Font = mediumFont;
            lblFromFullSPMI2.Font = smallFont;
            lblToFullSPMI2.Font = smallFont;

            txtNoteSPMI2.Font = mediumFont;
            txtSeatSPMI2.Font = mediumFont;

            //buttons
            btnScanSPMI2.Font = mediumFont;
            btnBackSPMI2.Font = btnBackTextSPMI2.Font = smallFont;
            //}
        }

        private void UpdateLayout()
        {
            if (MembershipProvider.Current == null) return;

            #region Preparations
            CGRect frame = UIScreen.MainScreen.Bounds;

            nfloat statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;
            isLandScape = UIScreen.MainScreen.Bounds.Width > UIScreen.MainScreen.Bounds.Height;
            isPad = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            linedView.SetNeedsDisplay();

            nfloat height = !isLandScape ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ScaleFactor = height / 667f;
            nfloat originFactor = ScaleFactor;

            if (isPad)//Experimental
                ScaleFactor *= ((float)2.0f / 3.0f);
            else if (!isLandScape && ScaleFactor > 1f)
                ScaleFactor = 1f;


            //Commenting to support iPhone 5 and 5s
            //if (scaleFactor < 1f)
            //{
            //    scaleFactor = 1f;
            //}

            nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                    UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.6f;

            nfloat childXOffset = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad ? (UIScreen.MainScreen.Bounds.Width / 2f) - (childWidth / 2f) : 0f;
            #endregion

            if ((float)View.SafeAreaInsets.Top > 0)
                statusBarH = (float)View.SafeAreaInsets.Top;

            childXOffset += UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                0 : (float)View.SafeAreaInsets.Left;


            #region Bottom View
            nfloat bottomHeight = 50f * ScaleFactor;

            bottomViewSPMI2.Frame = new CGRect(0f, frame.Height - bottomHeight - (float)View.SafeAreaInsets.Bottom, frame.Width, bottomHeight+View.SafeAreaInsets.Bottom);

            nfloat buttonsTotal = frame.Width - (30f * ScaleFactor);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                buttonsTotal -= (childXOffset * 2f);
            }
            nfloat buttonSlice = buttonsTotal / 3f;

            buttonSlice -= (float)View.SafeAreaInsets.Left;

            btnScanSPMI2.Frame = new CGRect(childXOffset + (10f * ScaleFactor), (bottomViewSPMI2.Frame.Height * 0.15f),
                buttonSlice * 2f, (bottomViewSPMI2.Frame.Height - View.SafeAreaInsets.Bottom) * 0.7f );
            btnScanSPMI2.ContentMode = UIViewContentMode.ScaleAspectFit;
            btnScanSPMI2.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

            btnConfirmSPMI2.Frame = new CGRect(btnScanSPMI2.Frame.Right + (10f * ScaleFactor), btnScanSPMI2.Frame.Y,
                buttonSlice, btnScanSPMI2.Frame.Height);
            btnConfirmSPMI2.ContentMode = UIViewContentMode.ScaleAspectFit;
            btnConfirmSPMI2.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

            //btnConfirmSPMI2.Layer.BorderColor = new CoreGraphics.CGColor(0f, 1f);
            //btnConfirmSPMI2.Layer.BorderWidth = 0f;
            //btnConfirmSPMI2.TitleLabel.AdjustsFontSizeToFitWidth = true;
            #endregion

            #region Top View


            agentViewSPMI2.Frame = new CGRect(0, statusBarH, frame.Width, 44f * originFactor);

            btnBackSPMI2.Frame = new CGRect(childXOffset + (8f * ScaleFactor), 0f,
                10f * ScaleFactor, agentViewSPMI2.Frame.Height);
            btnBackSPMI2.ContentMode = UIViewContentMode.ScaleAspectFit;
            btnBackSPMI2.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

            btnBackTextSPMI2.Frame = new CGRect(btnBackSPMI2.Frame.Right + (5f * originFactor), 0f,
                40f * ScaleFactor, agentViewSPMI2.Frame.Height);

            imgAgentAirlineSPMI2.Frame = new CGRect(btnBackTextSPMI2.Frame.Right, 2f * originFactor,
                agentViewSPMI2.Frame.Height - (4f * originFactor), agentViewSPMI2.Frame.Height - (4f * originFactor));

            lblAgentAirlineSPMI2.Frame = new CGRect(imgAgentAirlineSPMI2.Frame.Right + (5f * originFactor), 0f,
                (150f * originFactor), (24f * originFactor));
            lblAgentNameSPMI2.Frame = new CGRect(lblAgentAirlineSPMI2.Frame.Left, lblAgentAirlineSPMI2.Frame.Bottom,
                (150f * originFactor), (20f * originFactor));

            imgAgentAirlineSPMI2.Image = AppData.usersAirLine.image;
            lblAgentAirlineSPMI2.Text = MembershipProvider.Current.SelectedLounge.LoungeName;
            lblAgentNameSPMI2.Text = MembershipProvider.Current.UserFullName;
            #endregion

            #region Selected Airline View
            nfloat guestDifference = IsGuestMode ? 24f * originFactor : 0f;
            selectedAirlineViewSPMI2.Frame = new CGRect(0, (44f * originFactor) + statusBarH,
                frame.Width, (44f * ScaleFactor) + guestDifference);

            lblCardNameSPMI2.Frame = new CGRect(selectedAirlineViewSPMI2.Frame.Right - childXOffset - (98f * ScaleFactor), 0f,
                90f * ScaleFactor, agentViewSPMI2.Frame.Height);
            lblCardNameSPMI2.AdjustsFontSizeToFitWidth = true;
            lblCardNameSPMI2.AdjustsLetterSpacingToFitWidth = true;

            lblSelectedGuestType.Frame = new CGRect(childXOffset + (7f * ScaleFactor), 6f * ScaleFactor,
                220f * ScaleFactor, 20f * ScaleFactor);

            txtCarrierSPMI2.Frame = new CGRect(childXOffset + (7f * ScaleFactor), (7f * ScaleFactor) + guestDifference,
                47f * ScaleFactor, 30f * ScaleFactor);
            txtCarrierSPMI2.Layer.BorderColor = UIColor.White.CGColor;
            txtCarrierSPMI2.Layer.BorderWidth = 0f;

            txtFlightSPMI2.Frame = new CGRect(txtCarrierSPMI2.Frame.Right + (4f * ScaleFactor), txtCarrierSPMI2.Frame.Top,
                64f * ScaleFactor, txtCarrierSPMI2.Frame.Height);
            txtFlightSPMI2.Layer.BorderColor = UIColor.White.CGColor;
            txtFlightSPMI2.Layer.BorderWidth = 0f;

            txtClassSPMI2.Frame = new CGRect(txtFlightSPMI2.Frame.Right + (4f * ScaleFactor), txtCarrierSPMI2.Frame.Top,
                29f * ScaleFactor, txtCarrierSPMI2.Frame.Height);
            txtClassSPMI2.Layer.BorderColor = UIColor.White.CGColor;
            txtClassSPMI2.Layer.BorderWidth = 0f;

            imgSelectedAirlineViewSPMI2.Frame = new CGRect(selectedAirlineViewSPMI2.Frame.Right - childXOffset - (97f * ScaleFactor), selectedAirlineViewSPMI2.Frame.Top,
                90f * ScaleFactor, 90f * ScaleFactor);
            imgSelectedAirlineViewSPMI2.Layer.CornerRadius = 5f * ScaleFactor;
            imgSelectedAirlineViewSPMI2.Layer.BorderWidth = 3f * ScaleFactor;
            imgSelectedAirlineViewSPMI2.Layer.BorderColor = selectedAirlineViewSPMI2.BackgroundColor.CGColor;
            imgSelectedAirlineViewSPMI2.BackgroundColor = UIColor.White;

            txtPNRSPMI2.Frame = new CGRect(imgSelectedAirlineViewSPMI2.Frame.X - (103f * ScaleFactor), txtCarrierSPMI2.Frame.Top,
                98f * ScaleFactor, txtCarrierSPMI2.Frame.Height);
            txtPNRSPMI2.Layer.BorderColor = UIColor.White.CGColor;
            txtPNRSPMI2.Layer.BorderWidth = 0f;

            imgSelectedAirlineSPMI2.Frame = new CGRect(5f * ScaleFactor, 5f * ScaleFactor,
                imgSelectedAirlineViewSPMI2.Frame.Width - (10f * ScaleFactor), imgSelectedAirlineViewSPMI2.Frame.Height - (10f * ScaleFactor));
            imgSelectedAirlineSPMI2.BackgroundColor = UIColor.White;
            #endregion

            #region Passenger View
            if (!IsGuestMode)
                passengerViewSPMI2.Frame = new CGRect(0, 0f,
                    frame.Width, 105f * ScaleFactor);
            else
                passengerViewSPMI2.Frame = new CGRect(0, 0f,
                    frame.Width, 81f * ScaleFactor);

            var imgCardHeight = 50f * ScaleFactor;
            imgCardSPMI2.Frame = new CGRect(imgSelectedAirlineViewSPMI2.Frame.Left, passengerViewSPMI2.Frame.Height - (imgCardHeight + (4f * ScaleFactor))/*txtFFNSPMI2.Frame.Y - ((25f * ScaleFactor) - (txtFFNSPMI2.Frame.Height / 2f))*//*55f * scaleFactor*/,
                90f * ScaleFactor, imgCardHeight);

            if (!IsGuestMode)
            {
                txtFullNameSPMI2.Frame = new CGRect(childXOffset + (8f * ScaleFactor), 15f * ScaleFactor,
                    imgSelectedAirlineViewSPMI2.Frame.Left - (17f * ScaleFactor) - childXOffset /*260f * scaleFactor*/, txtCarrierSPMI2.Frame.Height);
                txtFullNameSPMI2.Layer.BorderColor = UIColor.White.CGColor;
                txtFullNameSPMI2.Layer.BorderWidth = 0f;

                txtFFNSPMI2.Frame = new CGRect(txtFullNameSPMI2.Frame.Left, 60f * ScaleFactor,
                    txtFullNameSPMI2.Frame.Width, txtCarrierSPMI2.Frame.Height);
                txtFFNSPMI2.Layer.BorderColor = UIColor.White.CGColor;
                txtFFNSPMI2.Layer.BorderWidth = 0f;
            }
            else
            {
                txtFFNSPMI2.Frame = new CGRect(childXOffset + (8f * ScaleFactor), imgCardSPMI2.Frame.Bottom - txtCarrierSPMI2.Frame.Height,//60f * ScaleFactor,
                    imgSelectedAirlineViewSPMI2.Frame.Left - (17f * ScaleFactor) - childXOffset , txtCarrierSPMI2.Frame.Height);
                txtFFNSPMI2.Layer.BorderColor = UIColor.White.CGColor;
                txtFFNSPMI2.Layer.BorderWidth = 0f;

                txtFullNameSPMI2.Frame = new CGRect(txtFFNSPMI2.Frame.Left, txtFFNSPMI2.Frame.Y - (txtCarrierSPMI2.Frame.Height) - (10f * ScaleFactor),
                    txtFFNSPMI2.Frame.Width, txtCarrierSPMI2.Frame.Height);
                txtFullNameSPMI2.Layer.BorderColor = UIColor.White.CGColor;
                txtFullNameSPMI2.Layer.BorderWidth = 0f;
            }
            #endregion

            #region Flight View
            flightViewSPMI2.Frame = new CGRect(0, passengerViewSPMI2.Frame.Bottom,
                    frame.Width, 395f * ScaleFactor);// frame.Height - (bottomViewSPMI2.Frame.Height - passengerViewSPMI2.Frame.Bottom));


            imgFromSPMI2.Frame = new CGRect(childXOffset + (8f * ScaleFactor), 15f * ScaleFactor,
                    txtCarrierSPMI2.Frame.Height, txtCarrierSPMI2.Frame.Height);

            txtFromSPMI2.Frame = new CGRect(imgFromSPMI2.Frame.Right + (8f * ScaleFactor), imgFromSPMI2.Frame.Top,
                    70f * ScaleFactor, txtCarrierSPMI2.Frame.Height);
            txtFromSPMI2.Layer.BorderColor = UIColor.White.CGColor;
            txtFromSPMI2.Layer.BorderWidth = 0f;

            lblFromFullSPMI2.Frame = new CGRect(imgFromSPMI2.Frame.Left, imgFromSPMI2.Frame.Bottom + (5f * ScaleFactor),
                    frame.Width - (2f * childXOffset) - (8f * ScaleFactor), 20f * ScaleFactor);


            imgToSPMI2.Frame = new CGRect(imgFromSPMI2.Frame.Left, lblFromFullSPMI2.Frame.Bottom + (10f * ScaleFactor),
                    txtCarrierSPMI2.Frame.Height, txtCarrierSPMI2.Frame.Height);

            txtToSPMI2.Frame = new CGRect(imgToSPMI2.Frame.Right + (8f * ScaleFactor), imgToSPMI2.Frame.Top,
                    txtFromSPMI2.Frame.Width, txtCarrierSPMI2.Frame.Height);
            txtToSPMI2.Layer.BorderColor = UIColor.White.CGColor;
            txtToSPMI2.Layer.BorderWidth = 0f;

            lblToFullSPMI2.Frame = new CGRect(imgToSPMI2.Frame.Left, imgToSPMI2.Frame.Bottom + (5f * ScaleFactor),
                    lblFromFullSPMI2.Frame.Width, 20f * ScaleFactor);

            txtNoteSPMI2.Frame = new CGRect(imgFromSPMI2.Frame.Left, lblToFullSPMI2.Frame.Bottom + (20f * ScaleFactor),
                    lblFromFullSPMI2.Frame.Width, txtCarrierSPMI2.Frame.Height);
            //txtNoteSPMI2.Layer.CornerRadius = 4f;
            txtNoteSPMI2.Layer.BorderColor = UIColor.White.CGColor;
            txtNoteSPMI2.Layer.BorderWidth = 0f;


            txtSeatSPMI2.Frame = new CGRect(imgFromSPMI2.Frame.Left, txtNoteSPMI2.Frame.Bottom + (15f * ScaleFactor),
                    65f * ScaleFactor, txtCarrierSPMI2.Frame.Height);
            txtSeatSPMI2.Layer.BorderColor = UIColor.White.CGColor;
            txtSeatSPMI2.Layer.BorderWidth = 0f;
            #endregion

            #region Scroll View
            scrollViewHolderSPMI2.Frame = new CGRect(selectedAirlineViewSPMI2.Frame.Left, selectedAirlineViewSPMI2.Frame.Bottom,
                selectedAirlineViewSPMI2.Frame.Width, bottomViewSPMI2.Frame.Top - selectedAirlineViewSPMI2.Frame.Bottom);// screenHeight /*- statusBarH*/ - detailsViewVR2.Frame.Height);

            scrollViewSPMI2.Frame = new CGRect(0, 0f,
                scrollViewHolderSPMI2.Frame.Width, scrollViewHolderSPMI2.Frame.Height);

            scrollViewSPMI2.ContentSize = new CGSize(scrollViewSPMI2.Frame.Width, (!isPad && isLandScape) ? flightViewSPMI2.Frame.Top + (250f * ScaleFactor) : flightViewSPMI2.Frame.Bottom /*scrollViewVR2.Frame.Height*/);//TODO: Test and check the value
            scrollViewSPMI2.ShowsVerticalScrollIndicator = true;
            scrollViewSPMI2.ShowsHorizontalScrollIndicator = false;
            #endregion

            #region Lined View
            linedView.Frame = new CGRect(passengerViewSPMI2.Frame.Left + (8f * ScaleFactor), passengerViewSPMI2.Frame.Y,
                    scrollViewSPMI2.Frame.Width - (16f * ScaleFactor), scrollViewSPMI2.ContentSize.Height);

            linedView.LinesY = new nfloat[2] { imgCardSPMI2.Frame.Bottom + (8f * ScaleFactor) /*flightViewSPMI2.Frame.Y*/, flightViewSPMI2.Frame.Bottom };
            #endregion
        }

        public void Close(bool savePassenger = true)
        {
            if (savePassenger)
                SavePassenger();
            this.DismissViewController(true, null);

            if (AppDelegate.ContinueScanning && !IsGuestMode)
                AppDelegate.AirlinesView.Scan();
            else IsGuestMode = false;
        }

        public void RefreshPassenger()
        {
            resetAllGradient();
            LoadPassenger();
        }
    }
}