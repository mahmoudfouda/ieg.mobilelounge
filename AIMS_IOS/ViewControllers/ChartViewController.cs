﻿using AIMS;
using AIMS.Models;
using AIMS_IOS.View_designer.Template;
using CoreGraphics;
using CoreLocation;
using Foundation;
using MapKit;
using Syncfusion.SfChart.iOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UIKit;
using AIMS_IOS.Utility;
using Ieg.Mobile.Localization;

namespace AIMS_IOS
{
    public partial class ChartViewController : AIMSViewController , IUITableViewDataSourcePrefetching
    {
        private UISlider slider;
        private UILabel lblLoungeCapacity, lblTotalPax, lblTitle, lblPerc1, lblPerc2, lblSubTitle, lblCustomers, lblGuests;
        private string formatedGuestText;
        private string formatedPrimaryText;
        private string formatedLoungeText, formatedCustomersText;
        private LoungeOccupancyElapsedData listData;
        private SFNumericalAxis secondaryAxis;
        private DateTime fromLocalDate, toLocalDate, currentFromLocalDate;
        private UIScrollView scrollView;
        private UITableView loungeTableView;
        private List<LoungeCellData> loungeItems;
        private TableSourceStatistics tableSource;
        private Workstation currentLounge;
        private UIRefreshControl refreshControl;
        private string titleOfPage = "Lounge Traffic - ";
        private string titleOfPieChart = "Current data - Last update: {0}";
        private System.Threading.CancellationTokenSource throttleCts = new System.Threading.CancellationTokenSource();
        private int TypeChartButtonSize = 35;

        public ChartViewController(IntPtr handle) : base(handle)
        {
            currentFromLocalDate = fromLocalDate = DateTime.MinValue;
            toLocalDate = DateTime.MinValue;
        }

        public SFChart Chart { get; private set; }
        public SFChart ChartPie { get; private set; }
        public SFChart ChartColumnBar { get; private set; }

        public SFAreaSeries ChartSeriesPax { get; private set; }
        public SFAreaSeries ChartSeriesGuest { get; private set; }
        public SFAreaSeries ChartSeriesReentry { get; private set; }
        public SFAreaSeries ChartSeriesUnknown { get; private set; }

        public SFColumnSeries ChartColumnBarSeriesActual { get; private set; }
        public SFColumnSeries ChartColumnBarSeriesWeekly { get; private set; }


        private UICollectionViewFlowLayout flowLayout;
        private UICollectionView uICollectionViewButtons;

        public SFChartZoomPanBehavior ZoomPan { get; private set; }

        private nfloat heightMainScreen;

        public SFPieSeries PieSeries { get; private set; }

        private CustomSFChartTrackballBehavior trackballBehavior;
        private TableSourceButtonsStatistics tableSourceButtonsStatistics;
        private AgentMenuTabController agentMenuTabController;
        private SFDateTimeAxis primaryBarAxis;
        private SFNumericalAxis secondaryBarAxis;
        private SFDateTimeAxis primaryAxis;

        public async override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if (MembershipProvider.Current != null)
                currentLounge = MembershipProvider.Current.SelectedLounge;

            loungeItems = AppManager.getListOfLounges().Where(c => c.Workstation.LoungeID != currentLounge.LoungeID).ToList();
            tableSource = new TableSourceStatistics(loungeItems);
            loungeTableView.Source = tableSource;
            tableSource.TableRowSelected += TableSourceLoungeRowSelected;

            lblTitle.Text = titleOfPage + MembershipProvider.Current.SelectedLounge.WorkstationName;
            slider.Value = 0;
            slider.MaxValue = AppDelegate.HoursOccupancyHistory - 0.99f;
            ZoomPan.Reset();

            AgentMenuTabController.ShowLoading();

            if (!Chart.Hidden)
                await RefreshLineChart();

            else if (!ChartColumnBar.Hidden)
                await RefreshBarChart();

            AppDelegate.OnOccupancyUpdateTick -= AppOccupancyUpdateTick;
            AppDelegate.OnOccupancyUpdateTick += AppOccupancyUpdateTick;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            var indexPath = NSIndexPath.FromRowSection(0, 0);
            uICollectionViewButtons.SelectItem(indexPath, true, UICollectionViewScrollPosition.None);

           // UpdateProgressLounges();
        }

        private void AppOccupancyUpdateTick(object sender, EventArgs e)
        {
            BeginInvokeOnMainThread(async () =>
            {
                await RefreshAllData();
            });
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            AppDelegate.OnOccupancyUpdateTick -= AppOccupancyUpdateTick;
        }

        //public async override void ViewSafeAreaInsetsDidChange()
        //{
        //    base.ViewSafeAreaInsetsDidChange();
        //    UpdateLayout();
        //    await RefreshAllData();
        //}

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            UpdateLayout();


            Chart.ReloadData();
            ChartPie.ReloadData();
            ChartColumnBar.ReloadData();
            loungeTableView.ReloadData();

            //await RefreshAllData();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            AppDelegate.TextProvider.OnLanguageChanged += LoadTexts;

            if (MembershipProvider.Current != null)
                currentLounge = MembershipProvider.Current.SelectedLounge;

            heightMainScreen = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
               UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            ScaleFactor = 1f;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                ScaleFactor = heightMainScreen / 667f;


            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height));
            View.AddSubview(scrollView);

            ChartSettings();

            loungeTableView = new UITableView
            {
                Bounces = true,
                //  PrefetchDataSource = this
            };
            scrollView.AddSubview(loungeTableView);
            loungeTableView.SetNeedsLayout();


            #region Slider
            slider = new UISlider();
            slider.MinValue = 0;
            slider.MaxValue = AppDelegate.HoursOccupancyHistory - 0.99f;
            slider.Value = 0;
            slider.ValueChanged += Slider_ValueChanged;
            //slider.TouchUpInside += SliderDidEndSliding;
            //slider.TouchUpOutside += SliderDidEndSliding;
            scrollView.AddSubview(slider);
            #endregion

            #region Label's 
            lblTitle = new UILabel
            {
                Text = titleOfPage + MembershipProvider.Current.SelectedLounge.WorkstationName,
                TextAlignment = UITextAlignment.Center
            };
            scrollView.AddSubview(lblTitle);

            lblSubTitle = new UILabel
            {
                Text = string.Format("{0} | {1}", MembershipProvider.Current.SelectedLounge.AirportCode, MembershipProvider.Current.SelectedLounge.LoungeName),
                TextAlignment = UITextAlignment.Center
            };
            scrollView.AddSubview(lblSubTitle);

            formatedLoungeText = "Capacity: {0}";
            lblLoungeCapacity = new UILabel
            {
                Text = formatedLoungeText
            };
            scrollView.AddSubview(lblLoungeCapacity);

            formatedCustomersText = "Current Customers: {0}";
            lblTotalPax = new UILabel
            {
                Text = formatedCustomersText
            };
            scrollView.AddSubview(lblTotalPax);

            formatedPrimaryText = "Primary: {0}";
            lblCustomers = new UILabel
            {
                Text = formatedPrimaryText,
                TextAlignment = UITextAlignment.Right
            };
            scrollView.AddSubview(lblCustomers);

            formatedGuestText = "Guests: {0}";
            lblGuests = new UILabel
            {
                Text = formatedGuestText,
                TextAlignment = UITextAlignment.Right
            };
            scrollView.AddSubview(lblGuests);

            lblPerc1 = new UILabel
            {
                Text = "0%"
            };
            scrollView.AddSubview(lblPerc1);

            lblPerc2 = new UILabel
            {
                Text = "100%"
            };
            scrollView.AddSubview(lblPerc2);
            #endregion

            backButton.TouchUpInside += (ss, ee) => { Close(); };
            backButtonTile.TouchUpInside += (ss, ee) => { Close(); };

            flowLayout = new UICollectionViewFlowLayout
            {
                ItemSize = new CGSize(TypeChartButtonSize * ScaleFactor, TypeChartButtonSize * ScaleFactor),
                SectionInset = new UIEdgeInsets(0, 5, 0, 5),
                ScrollDirection = UICollectionViewScrollDirection.Horizontal,
                MinimumInteritemSpacing = 0
            };
            uICollectionViewButtons = new UICollectionView(new CGRect(0, 0, 0, 0), flowLayout);
            scrollView.AddSubview(uICollectionViewButtons);

            refreshControl = new UIRefreshControl();
            refreshControl.ValueChanged += RefreshControl_ValueChanged;
            scrollView.AlwaysBounceVertical = true;
            scrollView.AddSubview(refreshControl);

            UpdateLayout();


            var swipeRight = new UISwipeGestureRecognizer(HandleRightSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Right };

            scrollView.AddGestureRecognizer(swipeRight);
            Chart.AddGestureRecognizer(swipeRight);

            var swipeLeft = new UISwipeGestureRecognizer(HandleLeftSwipe)
            { Direction = UISwipeGestureRecognizerDirection.Left };

            scrollView.AddGestureRecognizer(swipeLeft);
            ChartColumnBar.AddGestureRecognizer(swipeLeft);

            LoadTexts();
        }

        private void HandleRightSwipe(UISwipeGestureRecognizer recognizer)
        {
            uICollectionViewButtons.SelectItem(NSIndexPath.FromRowSection(1, 0), true, UICollectionViewScrollPosition.None);
            tableSourceButtonsStatistics.ItemSelected(uICollectionViewButtons, NSIndexPath.FromRowSection(1, 0));
        }

        private void HandleLeftSwipe(UISwipeGestureRecognizer recognizer)
        {
            uICollectionViewButtons.SelectItem(NSIndexPath.FromRowSection(0, 0), true, UICollectionViewScrollPosition.None);
            tableSourceButtonsStatistics.ItemSelected(uICollectionViewButtons, NSIndexPath.FromRowSection(0, 0));
        }

        private void RefreshControl_ValueChanged(object sender, EventArgs e)
        {
            var task = new Task(() =>
            {
                BeginInvokeOnMainThread(async () =>
                {
                    try
                    {
                        if (originalLounges != null)
                            originalLounges.Clear();

                        await RefreshAllData();


                        if (refreshControl != null)
                            refreshControl.EndRefreshing();
                    }
                    catch (Exception ef)
                    {
                        if (refreshControl != null)
                            refreshControl.EndRefreshing();
                    }
                });
            });
            task.Start();
        }

        private async Task RefreshAllData()
        {
            AppManager.ClearQueue();


            AgentMenuTabController.ShowLoading();

            slider.Hidden = Chart.Hidden;
            lblPerc1.Hidden = Chart.Hidden;
            lblPerc2.Hidden = Chart.Hidden;
            lblLoungeCapacity.Hidden = Chart.Hidden;
            lblTotalPax.Hidden = Chart.Hidden;
            lblCustomers.Hidden = Chart.Hidden;
            lblGuests.Hidden = Chart.Hidden;

            await Task.WhenAll(RefreshLineChart(), RefreshBarChart()); //, UpdateProgressLounges());
        }

        private void Slider_ValueChanged(object sender, EventArgs e)
        {
            //if (listData != null && listData.Count > 0)
            //    ZoomPan.Zoom(Chart.PrimaryAxis, listData.First().Time.LocalDateTime.AddHours(slider.Value), DateTime.Now.ToLocalTime());

            if (listData != null && listData.OccupancyRecords.Count > 0)
                ZoomPan.Zoom(Chart.PrimaryAxis, listData.OccupancyRecords.First().LocalTime.AddHours(slider.Value), DateTime.Now.ToLocalTime());
        }

        private void TrackballBehavior_TrackBallChanged(List<SFChartPointInfo> pointsInfo)
        {
            try
            {
                System.Threading.Interlocked.Exchange(ref this.throttleCts, new System.Threading.CancellationTokenSource()).Cancel();

                Task.Delay(TimeSpan.FromMilliseconds(750), this.throttleCts.Token)
                    .ContinueWith(delegate
                    {
                        LoadPieChartFromTouch(pointsInfo);
                    },
                    System.Threading.CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.FromCurrentSynchronizationContext());
            }
            catch { }
        }

        private void LoadPieChartFromTouch(List<SFChartPointInfo> pointsInfo)
        {
            try
            {
                var listData = pointsInfo.Select(c => c.Data as GraphDataCell);

                var actualTotal = (nfloat)listData.Sum(c => c.Total);
                var customersTotal = (nfloat)listData.Where(c => c.PassengerType == 0).Sum(c => c.Total);
                var guestsTotal = (nfloat)listData.Where(c => c.PassengerType == 1).Sum(c => c.Total);

                if (actualTotal > 0)
                {
                    lblTotalPax.Text = string.Format(formatedCustomersText, actualTotal);
                    lblCustomers.Text = string.Format(formatedPrimaryText, customersTotal);
                    lblGuests.Text = string.Format(formatedGuestText, guestsTotal);

                    PieSeries.ItemsSource = pointsInfo.Select(x => new
                    {
                        Total = (x.Data as GraphDataCell).Total,
                        TypePassenger = GetTypePassenger((x.Data as GraphDataCell).PassengerType)
                    });
                }
                else
                {
                    NoDataPieChart();
                }

                ChartPie.Title.Text = string.Empty;
                ChartPie.ReloadData();
            }
            catch { }
        }

        private void NoDataPieChart()
        {
            var list = new[]
            {
                        new { Total = 0, TypePassenger = GetTypePassenger(0) },
                        new { Total = 0, TypePassenger = GetTypePassenger(1) },
                        new { Total = 1, TypePassenger = GetTypePassenger(6) },
                    }.ToList();

            PieSeries.ItemsSource = list;
        }

        private void SliderDidEndSliding(object sender, EventArgs e)
        {
            currentFromLocalDate = fromLocalDate.AddHours(-slider.Value);
            RefreshLineChart();
        }

        private void ChartSettings()
        {
            Chart = new SFChart();
            ZoomPan = new SFChartZoomPanBehavior();
            ZoomPan.ZoomMode = SFChartZoomMode.X;
            Chart.AddChartBehavior(ZoomPan);

            trackballBehavior = new CustomSFChartTrackballBehavior(ScaleFactor);
            trackballBehavior.LineStyle.Visible = true;
            trackballBehavior.LineStyle.LineWidth = new NSNumber(1);
            trackballBehavior.LineStyle.LineColor = UIColor.Blue;
            trackballBehavior.LabelDisplayMode = SFChartTrackballLabelDisplayMode.FloatAllPoints;
            trackballBehavior.TrackBallChanged += TrackballBehavior_TrackBallChanged;
            Chart.Behaviors.Add(trackballBehavior);

            //Chart.Title.Text = "Lounge Occupancy Graph - " + MembershipProvider.Current.SelectedLounge.LoungeName;
            //Chart.AreaBorderColor = UIColor.Gray;
            //Chart.AreaBorderWidth = 0.5f;


            //Chart.Delegate = new ChartDelegate(slider);

            ChartPie = new SFChart
            {
                //   AreaBorderColor = UIColor.Gray,
                //   AreaBorderWidth = 0.5f
            };

            primaryAxis = new SFDateTimeAxis();
            primaryAxis.Title.Text = new NSString("Time");
            // primaryAxis.ShowMajorGridLines = false;
            // primaryAxis.ShowMinorGridLines = false;
            primaryAxis.LabelStyle.LabelFormatter = new NSDateFormatter() { DateFormat = "HH:mm" };
            Chart.PrimaryAxis = primaryAxis;

            //Adding Secondary Axis for the Chart.
            secondaryAxis = new SFNumericalAxis();
            secondaryAxis.Title.Text = new NSString("Total Customers");
            //secondaryAxis.ShowMajorGridLines = true;
            //secondaryAxis.ShowMinorGridLines = true;
            secondaryAxis.Interval = new NSNumber(10);
            secondaryAxis.Minimum = new NSNumber(0);
            Chart.SecondaryAxis = secondaryAxis;

            #region Creating Series of main chart
            ChartSeriesPax = new SFAreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Primary",
                //Color = UIColor.Clear.FromHexString("#0e79d8"),
                ShowTrackballInfo = true,
                //Alpha = 0.5f,
                EnableAnimation = true
            };
            ChartSeriesPax.ColorModel.Palette = SFChartColorPalette.Custom;
            ChartGradientColor gradientColorPax = new ChartGradientColor() { StartPoint = new CGPoint(0.5f, 1), EndPoint = new CGPoint(0.5f, 0) };
            //ChartGradientColor gradientColorPax2 = new ChartGradientColor() { StartPoint = new CGPoint(0.7f, 1), EndPoint = new CGPoint(0.7f, 0) };

            ChartGradientStop stopP1 = new ChartGradientStop() { Color = UIColor.Clear.FromHexString("#0e79d8").ColorWithAlpha(0.4f), Offset = 0 };
            ChartGradientStop stopP2 = new ChartGradientStop() { Color = UIColor.Clear.FromHexString("#0e79d8"), Offset = 1 };
            //ChartGradientStop stopP12 = new ChartGradientStop() { Color = UIColor.Clear.FromHexString("#0e79d8").ColorWithAlpha(0.2f), Offset = 0.2f };
            //ChartGradientStop stopP22 = new ChartGradientStop() { Color = UIColor.Clear.FromHexString("#0e79d8"), Offset = .8f };

            gradientColorPax.GradientStops.Add(stopP1);
            gradientColorPax.GradientStops.Add(stopP2);

            //gradientColorPax2.GradientStops.Add(stopP12);
            //gradientColorPax2.GradientStops.Add(stopP22);

            ChartGradientColorCollection gradientColorsPax = new ChartGradientColorCollection()
            {
                gradientColorPax//,
                //gradientColorPax2
            };
            ChartSeriesPax.ColorModel.CustomGradientColors = gradientColorsPax;

            ChartSeriesGuest = new SFAreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                //Color = UIColor.Clear.FromHexString("#9370db"),
                Label = "Guest",
                ShowTrackballInfo = true,
                //Alpha = 0.5f,
                EnableAnimation = true
            };
            ChartSeriesGuest.ColorModel.Palette = SFChartColorPalette.Custom;
            ChartGradientColor gradientColorGuest = new ChartGradientColor() { StartPoint = new CGPoint(0.5f, 1), EndPoint = new CGPoint(0.5f, 0) };
            ChartGradientStop stopG1 = new ChartGradientStop() { Color = UIColor.Clear.FromHexString("#9370db").ColorWithAlpha(0.4f), Offset = 0 };
            ChartGradientStop stopG2 = new ChartGradientStop() { Color = UIColor.Clear.FromHexString("#9370db"), Offset = 1 };
            gradientColorGuest.GradientStops.Add(stopG1);
            gradientColorGuest.GradientStops.Add(stopG2);
            ChartGradientColorCollection gradientColorsGuest = new ChartGradientColorCollection()
            {
                gradientColorGuest
            };
            ChartSeriesGuest.ColorModel.CustomGradientColors = gradientColorsGuest;

            ChartSeriesReentry = new SFAreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Reentry",
                ShowTrackballInfo = true,
                Alpha = 0.5f,
                VisibleOnLegend = false
            };

            ChartSeriesUnknown = new SFAreaSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Unknown",
                VisibleOnLegend = false,
                Color = UIColor.Gray,
                Alpha = 0.5f
            };

            Chart.Series.Add(ChartSeriesPax);
            Chart.Series.Add(ChartSeriesGuest);
            Chart.Series.Add(ChartSeriesReentry);
            Chart.Legend.Visible = true;
            #endregion

            #region Settings for Pie Chart
            PieSeries = new SFPieSeries()
            {
                XBindingPath = "TypePassenger",
                YBindingPath = "Total",
                CircularCoefficient = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad ? 0.85 : 0.90,
                DataMarkerPosition = SFChartCircularSeriesLabelPosition.OutsideExtended,
                EnableAnimation = true
            };

            ChartPie.Series.Add(PieSeries);
            PieSeries.DataMarker.LabelContent = SFChartLabelContent.Percentage;
            PieSeries.DataMarker.ShowLabel = true;

            PieSeries.ColorModel.Palette = SFChartColorPalette.Custom;

            var customColors = new List<UIColor>();
            NSArray nsArray = NSArray.FromObjects(new UIColor[] { UIColor.Clear.FromHexString("#0e79d8"), UIColor.Clear.FromHexString("#9370db"), UIColor.LightGray });
            PieSeries.ColorModel.CustomColors = nsArray;
            #endregion

            #region Settings for ColumnBar Chart

            primaryBarAxis = new SFDateTimeAxis();
            primaryBarAxis.Title.Text = new NSString("Time");
            primaryBarAxis.LabelStyle.LabelFormatter = new NSDateFormatter() { DateFormat = "HH:mm" };

            secondaryBarAxis = new SFNumericalAxis();
            secondaryBarAxis.Title.Text = new NSString("Total Customers");
            secondaryBarAxis.Interval = new NSNumber(10);
            secondaryBarAxis.Minimum = new NSNumber(0);

            ChartColumnBar = new SFChart
            {
                PrimaryAxis = primaryBarAxis,
                SecondaryAxis = secondaryBarAxis,
                SideBySideSeriesPlacement = false
            };

            ChartColumnBarSeriesActual = new SFColumnSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Label = "Actual",
                //Color = UIColor.Clear.FromHexString("#0e79d8"),
                Color = UIColor.Orange,
                ShowTrackballInfo = true,
                //Alpha = 0.5f,
                //EnableAnimation = true
            };
            ChartColumnBarSeriesWeekly = new SFColumnSeries
            {
                XBindingPath = "Time",
                YBindingPath = "Total",
                Color = UIColor.LightGray,
                Label = "Today's Projections",
                ShowTrackballInfo = true,
                Alpha = 0.4f,
                //EnableAnimation = true,
            };

            var trackballBehaviorBar = new CustomSFChartTrackballBehavior(ScaleFactor);
            trackballBehaviorBar.LineStyle.Visible = true;
            trackballBehaviorBar.LineStyle.LineWidth = new NSNumber(1);
            trackballBehaviorBar.LineStyle.LineColor = UIColor.Blue;
            trackballBehaviorBar.LabelDisplayMode = SFChartTrackballLabelDisplayMode.FloatAllPoints;
            ChartColumnBar.Behaviors.Add(trackballBehaviorBar);

            ChartColumnBar.Series.Add(ChartColumnBarSeriesActual);
            ChartColumnBar.Series.Add(ChartColumnBarSeriesWeekly);
            ChartColumnBar.Legend.Visible = true;
            ChartColumnBar.Hidden = true;
            ChartColumnBar.Title.Text = DateTime.Now.ToLongDateString() + " - Dwell time analysis";
            #endregion

            scrollView.AddSubview(Chart);
            scrollView.AddSubview(ChartPie);
            scrollView.AddSubview(ChartColumnBar);
        }

        public void Close()
        {
            slider.Value = 0;
            fromLocalDate = toLocalDate = currentFromLocalDate = DateTime.MinValue;
            //this.DismissViewController(true, null);

            AgentMenuTabController.goToAirLineViewController();
        }

        public AgentMenuTabController AgentMenuTabController
        {
            get
            {
                if (agentMenuTabController == null)
                {
                    agentMenuTabController = this.TabBarController as AgentMenuTabController;

                    agentMenuTabController.ViewControllerSelected += AgentMenuTabController_ViewControllerSelected;
                }

                return agentMenuTabController;
            }
        }

        private void AgentMenuTabController_ViewControllerSelected(object sender, UITabBarSelectionEventArgs e)
        {
            if (!(e.ViewController is ChartViewController))
                AppManager.StopTimerOccupancy();
        }

        public void UpdateLayout()
        {
            nfloat statusBarH = UIApplication.SharedApplication.StatusBarHidden ? 0f : 20f;

            CGRect frame = UIScreen.MainScreen.Bounds;

            topView.Frame = new CGRect(0f, statusBarH, frame.Width, 44f * ScaleFactor);

            nfloat childWidth = UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width ?
                                UIScreen.MainScreen.Bounds.Width * 0.75f : UIScreen.MainScreen.Bounds.Width * 0.6f;


            backButton.Frame = new CGRect((8f * ScaleFactor), 10f, 10f * ScaleFactor, topView.Frame.Height);
            backButtonTile.ContentMode = UIViewContentMode.ScaleAspectFit;
            backButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            backButtonTile.Frame = new CGRect(backButton.Frame.Right + (5f * ScaleFactor), 10f,
                40f * ScaleFactor, topView.Frame.Height);


            string regularFontName = "Helvetica Neue";
            string boldFontName = "Helvetica-Bold";
            nfloat xxLargeTexts = 26f * ScaleFactor, xLargeTexts = 24f * ScaleFactor, largeTexts = 22f * ScaleFactor, medium = 17f * ScaleFactor, smallTexts = 15f * ScaleFactor,
                                                     xSmallTexts = 13f * ScaleFactor, microTexts = 10f * ScaleFactor, xmicroTexts = 9f * ScaleFactor;


            UIFont microFont = UIFont.FromName(regularFontName, microTexts);
            UIFont xmicroFont = UIFont.FromName(regularFontName, xmicroTexts);
            UIFont smallFont = UIFont.FromName(regularFontName, smallTexts);
            var mediumFont = UIFont.FromName(regularFontName, medium);


            backButton.Font = backButtonTile.Font = mediumFont;

            scrollView.Frame = new CGRect(0, this.topView.Frame.Height + 10f, View.Frame.Width, View.Frame.Height);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                lblLoungeCapacity.Font = UIFont.FromName(regularFontName, smallTexts);
                lblTotalPax.Font = UIFont.FromName(regularFontName, smallTexts);
                lblCustomers.Font = UIFont.FromName(regularFontName, smallTexts);
                lblGuests.Font = UIFont.FromName(regularFontName, smallTexts);
                lblTitle.Font = UIFont.FromName(boldFontName, largeTexts);
                lblSubTitle.Font = UIFont.ItalicSystemFontOfSize(medium);
                ChartPie.Title.Font = microFont;
            }
            else
            {
                lblLoungeCapacity.Font = UIFont.FromName(regularFontName, xSmallTexts);
                lblTotalPax.Font = UIFont.FromName(regularFontName, xSmallTexts);
                lblCustomers.Font = UIFont.FromName(regularFontName, xSmallTexts);
                lblGuests.Font = UIFont.FromName(regularFontName, xSmallTexts);
                lblTitle.Font = UIFont.FromName(boldFontName, medium);
                lblSubTitle.Font = UIFont.ItalicSystemFontOfSize(xSmallTexts);
                ChartPie.Title.Font = xmicroFont;
            }

            //lblLoungeCapacity.Hidden = true;
            //lblTotalPax.Hidden = true;
            //lblCustomers.Hidden = true;
            //lblGuests.Hidden = true;

            lblPerc1.Font = xmicroFont;
            lblPerc2.Font = xmicroFont;

            Chart.Legend.LabelStyle.Font = microFont;
            Chart.PrimaryAxis.LabelStyle.Font = xmicroFont;
            Chart.SecondaryAxis.LabelStyle.Font = xmicroFont;
            Chart.PrimaryAxis.Title.Font = microFont;
            Chart.SecondaryAxis.Title.Font = microFont;
            PieSeries.DataMarker.LabelStyle.Font = xmicroFont;
            ChartColumnBar.Legend.LabelStyle.Font = microFont;
            ChartColumnBar.Title.Font = microFont;

            ChartColumnBar.PrimaryAxis.LabelStyle.Font = xmicroFont;
            ChartColumnBar.SecondaryAxis.LabelStyle.Font = xmicroFont;
            ChartColumnBar.PrimaryAxis.Title.Font = microFont;
            ChartColumnBar.SecondaryAxis.Title.Font = microFont;

            var chartH = 200 * ScaleFactor;
            var titleH = 30 * ScaleFactor;
            var subTitleH = 30 * ScaleFactor;
            var totalPaxH = 20 * ScaleFactor;
            var sliderH = 20 * ScaleFactor;
            var buttonListH = 40 * ScaleFactor;
            var pieH = 128 * ScaleFactor;

            float titleInsets = 10f;

            float leftMargin = UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ? 0 : (float)View.SafeAreaInsets.Left;


            lblTitle.Frame = new CGRect(scrollView.Frame.X, titleInsets, this.scrollView.Frame.Width, titleH);
            //lblTitle.Lines = 0;
            //lblTitle.LineBreakMode = UILineBreakMode.WordWrap;
            //lblTitle.PreferredMaxLayoutWidth = this.scrollView.Frame.Width;

            lblSubTitle.Frame = new CGRect(scrollView.Frame.X, lblTitle.Frame.Bottom, this.scrollView.Frame.Width, subTitleH);

            var usedHeight = lblTitle.Frame.Height + lblSubTitle.Frame.Height + (10f * ScaleFactor);

            lblLoungeCapacity.Frame = new CGRect(this.scrollView.Frame.X + (10f * ScaleFactor) + leftMargin, usedHeight, (this.scrollView.Frame.Width / 2) - leftMargin, totalPaxH);
            lblCustomers.Frame = new CGRect((this.scrollView.Frame.Width / 2) + leftMargin, usedHeight, (this.scrollView.Frame.Width / 2) - (10f * ScaleFactor) - leftMargin, totalPaxH);
            lblTotalPax.Frame = new CGRect(this.scrollView.Frame.X + (10f * ScaleFactor), lblLoungeCapacity.Frame.Bottom, this.scrollView.Frame.Width / 2, totalPaxH);
            lblGuests.Frame = new CGRect(this.scrollView.Frame.Width / 2, lblCustomers.Frame.Bottom, (this.scrollView.Frame.Width / 2) - (10f * ScaleFactor), totalPaxH);
            usedHeight += lblLoungeCapacity.Frame.Height + (25f * ScaleFactor);

            Chart.Frame = new CGRect(this.scrollView.Frame.X + leftMargin, usedHeight, this.scrollView.Frame.Width - leftMargin, chartH);
            Chart.EdgeInsets = new UIEdgeInsets(0, (10f * ScaleFactor), 0, (20f * ScaleFactor));

            ChartColumnBar.Frame = new CGRect(this.scrollView.Frame.X + leftMargin, usedHeight - (lblLoungeCapacity.Frame.Height + (20f * ScaleFactor)), this.scrollView.Frame.Width - leftMargin, chartH + sliderH + lblLoungeCapacity.Frame.Height + (30f * ScaleFactor));
            ChartColumnBar.EdgeInsets = new UIEdgeInsets(0, (10f * ScaleFactor), 0, (20f * ScaleFactor));

            usedHeight += Chart.Frame.Height + (10f * ScaleFactor);

            slider.Frame = new CGRect(this.scrollView.Frame.X + (40f * ScaleFactor), Chart.Frame.Bottom, this.scrollView.Frame.Width - (80f * ScaleFactor), sliderH);
            lblPerc1.Frame = new CGRect(this.scrollView.Frame.X + (40f * ScaleFactor), slider.Frame.Bottom, 50f, sliderH);
            lblPerc2.Frame = new CGRect(this.scrollView.Frame.Width - (60f * ScaleFactor), slider.Frame.Bottom, 60f, sliderH);
            usedHeight += slider.Frame.Height + (10f * ScaleFactor);

            CollectionViewButtonsSettings(usedHeight, buttonListH);
            usedHeight += uICollectionViewButtons.Frame.Height + (10f * ScaleFactor);

            ChartPie.Frame = new CGRect(this.scrollView.Frame.X + leftMargin, usedHeight, (this.scrollView.Frame.Width / 2) - leftMargin, pieH);
            //ChartPie.EdgeInsets = new UIEdgeInsets(0, (20f * ScaleFactor), 0, (20f * ScaleFactor));

            pieH = heightMainScreen - usedHeight;
            TableViewSettings(usedHeight, pieH);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone &&
               (UIScreen.MainScreen.Bounds.Height == 568 || UIScreen.MainScreen.Bounds.Width == 568)) //5s
                scrollView.ContentSize = new CoreGraphics.CGSize(frame.Width, chartH + pieH + sliderH + titleH + subTitleH + totalPaxH + buttonListH + (99f * ScaleFactor));
            else
                scrollView.ContentSize = new CoreGraphics.CGSize(frame.Width, chartH + pieH + sliderH + titleH + subTitleH + totalPaxH + buttonListH + (10f * ScaleFactor));
        }

        private void CollectionViewButtonsSettings(nfloat usedHeight, nfloat buttonListH)
        {
            uICollectionViewButtons.BackgroundColor = UIColor.Clear;

            uICollectionViewButtons.Frame = new CGRect(this.scrollView.Frame.X + (20f * ScaleFactor), usedHeight, (this.scrollView.Frame.Width - (40f * ScaleFactor)), buttonListH);
            var CellWidth = TypeChartButtonSize * ScaleFactor;
            var CellCount = 2;
            var CellSpacing = 5f * ScaleFactor;

            var totalCellWidth = CellWidth * CellCount;
            var totalSpacingWidth = CellSpacing * (CellCount - 1);

            var leftInset = (uICollectionViewButtons.Frame.Width - (totalCellWidth + totalSpacingWidth)) / 2;
            var rightInset = leftInset;
            flowLayout.SectionInset = new UIEdgeInsets(0, leftInset, 0, rightInset);

            uICollectionViewButtons.RegisterClassForCell(typeof(StatisticButtonCell), (NSString)"StatisticButtonCell");

            var border = CoreAnimation.CALayer.Create();
            border.BackgroundColor = UIColor.LightGray.CGColor;
            border.Frame = new CGRect(0, uICollectionViewButtons.Frame.Size.Height - 0.5f,
                                      uICollectionViewButtons.Frame.Size.Width, 0.5f);
            uICollectionViewButtons.Layer.AddSublayer(border);

            if (tableSourceButtonsStatistics == null)
            {
                tableSourceButtonsStatistics = new TableSourceButtonsStatistics(new List<string>() { "1", "2" }, AgentMenuTabController.TabBar.TintColor, ScaleFactor);
                uICollectionViewButtons.Source = tableSourceButtonsStatistics;
                tableSourceButtonsStatistics.TableItemSelected += TableSourceButtonsStatistics_ItemSelected;

                uICollectionViewButtons.SelectItem(NSIndexPath.FromItemSection(0, 0), false, UICollectionViewScrollPosition.None);
            }
        }

        private void TableSourceButtonsStatistics_ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            try
            {
                Chart.Hidden = indexPath.Row == 1;
                slider.Hidden = indexPath.Row == 1;
                lblPerc1.Hidden = indexPath.Row == 1;
                lblPerc2.Hidden = indexPath.Row == 1;
                lblLoungeCapacity.Hidden = indexPath.Row == 1;
                lblTotalPax.Hidden = indexPath.Row == 1;
                lblCustomers.Hidden = indexPath.Row == 1;
                lblGuests.Hidden = indexPath.Row == 1;
                ChartColumnBar.Hidden = indexPath.Row == 0;

                if (!ChartColumnBar.Hidden)
                    RefreshBarChart();
                else
                    Chart.ReloadData();
            }
            catch
            {

            }
        }

        private void TableViewSettings(nfloat usedHeight, nfloat pieH)
        {
            loungeTableView.Frame = new CGRect((this.scrollView.Frame.Width / 2), usedHeight, (this.scrollView.Frame.Width / 2 - (20f * ScaleFactor)), pieH);

            loungeTableView.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;

            loungeTableView.RegisterClassForCellReuse(typeof(LoungeCellStatistics), (NSString)"LoungeCellStatistics");

            //ratio based on iphone 6, minimum of 44f
            nfloat estimatedHeight = 85f / 650f * heightMainScreen;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                estimatedHeight *= .75f;

            estimatedHeight = estimatedHeight < 85f ? 85f : estimatedHeight;

            loungeTableView.ContentInset = new UIEdgeInsets(0, 0, estimatedHeight, 0);

            loungeTableView.EstimatedRowHeight = estimatedHeight;
            loungeTableView.RowHeight = estimatedHeight;

            loungeTableView.TableFooterView = new UIView(new CGRect(0, 0, loungeTableView.Frame.Size.Width, 1));
            loungeTableView.TableHeaderView = new UIView(new CGRect(0, 0, loungeTableView.Frame.Size.Width, 1));

            //loungeTableView.Layer.BorderColor = UIColor.Clear.CGColor;
            //loungeTableView.Layer.BorderWidth = 0.5f;

            loungeTableView.SeparatorColor = UIColor.Clear;
            loungeTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            loungeTableView.PrefetchDataSource = this;


            // UpdateProgressLounges();
        }

        private async void TableSourceLoungeRowSelected(Workstation workstation)
        {
            if (workstation != null)
            {
                currentLounge = workstation;

                loungeItems = AppManager.getListOfLounges().Where(c => c.Workstation.LoungeID != currentLounge.LoungeID).ToList();
                tableSource.UpdateItems(loungeItems);

                await RefreshAllData();

                lblTitle.Text = titleOfPage + workstation.WorkstationName;
                lblSubTitle.Text = string.Format("{0} | {1}", workstation.AirportCode, workstation.LoungeName);

                loungeTableView.ReloadData();

            }
        }

        private async Task UpdateProgressLounges()
        {
            if (loungeTableView == null) return;

            var index = loungeTableView.IndexPathForSelectedRow;
            if (index != null)
            {
                loungeTableView.DeselectRow(index, false);
            }

            var visibleRows = loungeTableView.IndexPathsForVisibleRows;

            if (visibleRows != null)
                GetOccupancyDataByVisibleRows(visibleRows, false);
        }

        private bool _refreshingLineChart = true;

        /// <summary>
        /// Gets the latest lounge occupancies and refreshes the list
        /// </summary>
        public async Task RefreshLineChart()
        {
            if (ChartSeriesPax == null || Chart.Hidden)
                return;

            _refreshingLineChart = true;

            await Task.Run(() =>
            {
                try
                {
                    toLocalDate = DateTime.Now;
                    currentFromLocalDate = toLocalDate.Subtract(new TimeSpan(AppDelegate.HoursOccupancyHistory, 0, 0));
                    int intervalInSeconds = (int)Math.Ceiling(toLocalDate.Subtract(currentFromLocalDate).TotalSeconds / 30f);

                    AppManager.GetRetrieveCachedLoungeOccupancies(currentLounge.LoungeID,
                                                      currentLounge, currentFromLocalDate, toLocalDate, intervalInSeconds, (data) =>
                                                      {
                                                          _refreshingLineChart = false;

                                                      BeginInvokeOnMainThread(async () =>
                                                          {
                                                              ChartPie.Title.Text = "";

                                                              if (ChartSeriesPax == null || data == null)
                                                              {
                                                                  AgentMenuTabController.HideLoading();

                                                                  UpdateProgressLounges();

                                                                  return;
                                                              }

                                                              //listData = (List<LoungeOccupancy>)data;

                                                              listData = (LoungeOccupancyElapsedData)data;

                                                              if(listData.OccupancyRecords == null)
                                                              {
                                                                  _refreshingLineChart = false;

                                                                  ChartSeriesPax.ItemsSource = new List<GraphDataCell>();
                                                                  ChartSeriesGuest.ItemsSource = new List<GraphDataCell>();

                                                                  lblLoungeCapacity.Text = string.Format(formatedLoungeText, listData.Capacity);
                                                                  lblTotalPax.Text = string.Format(formatedCustomersText, 0);
                                                                  lblCustomers.Text = string.Format(formatedPrimaryText, 0);
                                                                  lblGuests.Text = string.Format(formatedGuestText, 0);

                                                                  NoDataPieChart();

                                                                  AgentMenuTabController.HideLoading();

                                                                  UpdateProgressLounges();

                                                                  return;
                                                              }


                                                              try
                                                              {
                                                                  var primaryIndex = (int)OccupancyValueType.Primary;
                                                                  var guestIndex = (int)OccupancyValueType.Guest;

                                                                  decimal maxSum = 0, totalOfPrimary = 0, totalOfGuests = 0;
                                                                  if (Chart != null && listData.OccupancyRecords.Count > 0)
                                                                  {
                                                                      maxSum = listData.OccupancyRecords.Max(x => x.Values[primaryIndex] + x.Values[guestIndex]);
                                                                      totalOfPrimary = listData.OccupancyRecords.Last().Values[primaryIndex];
                                                                      totalOfGuests = listData.OccupancyRecords.Last().Values[guestIndex];

                                                                      //total = totalOfPrimary + totalOfGuests;

                                                                      if (maxSum > 0)
                                                                      {
                                                                          var graphMax = (int)Math.Ceiling((float)maxSum * 1.1);
                                                                          Chart.SecondaryAxis.Maximum = new NSNumber(graphMax);

                                                                          var interval = (float)graphMax / 10;
                                                                          if (interval < 1)
                                                                              Chart.SecondaryAxis.Interval = new NSNumber(1);
                                                                          else Chart.SecondaryAxis.Interval = new NSNumber(Math.Floor(interval));
                                                                      }

                                                                      //((SFDateTimeAxis)Chart.PrimaryAxis).Interval = //If we need to change the time interval on horizontal axis
                                                                  }
                                                                  ChartSeriesPax.ItemsSource = listData.OccupancyRecords.Select(x => new GraphDataCell()
                                                                  {
                                                                      Time = x.LocalTime,
                                                                      Total = x.Values[primaryIndex],
                                                                      PassengerType = primaryIndex
                                                                  }).ToList();

                                                                  ChartSeriesGuest.ItemsSource = listData.OccupancyRecords.Select(x => new GraphDataCell()
                                                                  {
                                                                      Time = x.LocalTime,
                                                                      Total = x.Values[guestIndex],
                                                                      PassengerType = guestIndex
                                                                  }).ToList();

                                                                  if (maxSum > 0)
                                                                  {
                                                                      ChartPie.Hidden = false;

                                                                      var pieDataSource = new[] {
                                                                          new {
                                                                              Total = totalOfPrimary,
                                                                              TypePassenger = GetTypePassenger(primaryIndex)},
                                                                          new {
                                                                              Total = totalOfGuests,
                                                                              TypePassenger = GetTypePassenger(guestIndex)}
                                                                      }.ToList();

                                                                      //var groups = (from s in listData.OrderBy(c => c.OccupancyTypePassenger)
                                                                      //              group s by s.OccupancyTypePassenger into g
                                                                      //              select new { OccupancyTypePassenger = g.Key, Total = g.Last().OccupancyTotal });

                                                                      //PieSeries.ItemsSource = groups.Where(c => c.OccupancyTypePassenger != 3).Select(x => new
                                                                      //{
                                                                      //    Total = x.Total,
                                                                      //    TypePassenger = GetTypePassenger(x.OccupancyTypePassenger)
                                                                      //});

                                                                      PieSeries.ItemsSource = pieDataSource;

                                                                      if (!pieDataSource.Max(c => c.Total > 0))
                                                                      {
                                                                          NoDataPieChart();
                                                                          ChartPie.Title.Text = string.Format(titleOfPieChart, DateTime.Now.ToString("HH:mm"));
                                                                      }

                                                                      lblLoungeCapacity.Hidden = false;
                                                                      lblTotalPax.Hidden = false;
                                                                      lblCustomers.Hidden = false;
                                                                      lblGuests.Hidden = false;

                                                                      lblLoungeCapacity.Text = string.Format(formatedLoungeText, listData.Capacity);
                                                                      lblTotalPax.Text = string.Format(formatedCustomersText, (nfloat)pieDataSource.Sum(c => c.Total));
                                                                      lblCustomers.Text = string.Format(formatedPrimaryText, totalOfPrimary);
                                                                      lblGuests.Text = string.Format(formatedGuestText, totalOfGuests);
                                                                  }
                                                                  else
                                                                  {
                                                                      lblLoungeCapacity.Text = string.Format(formatedLoungeText, listData.Capacity);
                                                                      lblTotalPax.Text = string.Format(formatedCustomersText, 0);
                                                                      lblCustomers.Text = string.Format(formatedPrimaryText, 0);
                                                                      lblGuests.Text = string.Format(formatedGuestText, 0);

                                                                      NoDataPieChart();
                                                                  }

                                                                  ChartPie.Title.Text = string.Format(titleOfPieChart, DateTime.Now.ToString("HH:mm"));

                                                              }
                                                              catch (Exception ex)
                                                              {
                                                                  _refreshingLineChart = false;

                                                                  lblLoungeCapacity.Text = string.Format(formatedLoungeText, listData.Capacity);
                                                                  lblTotalPax.Text = string.Format(formatedCustomersText, 0);
                                                                  lblCustomers.Text = string.Format(formatedPrimaryText, 0);
                                                                  lblGuests.Text = string.Format(formatedGuestText, 0);

                                                                  NoDataPieChart();


                                                                  LogsRepository.AddError("Get Lounge Occupancy List call error.", ex);
                                                                  Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                                                              }
                                                              finally
                                                              {
                                                                  _refreshingLineChart = false;

                                                                  UpdateProgressLounges();

                                                                  await Task.Delay(new TimeSpan(0, 0, (int)ChartSeriesGuest.AnimationDuration));
                                                                  AgentMenuTabController.HideLoading();
                                                              }
                                                          });
                                                      });
                }
                catch
                {
                    _refreshingLineChart = false;

                    AgentMenuTabController.HideLoading();
                }
            });
        }

        public async Task RefreshBarChart()
        {
            if (ChartColumnBar.Hidden || ChartColumnBarSeriesActual == null)
                return;

            await Task.Run(() =>
            {
                try
                {
                    var dtNow = DateTime.Now;
                    toLocalDate = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 23, 0, 0, DateTimeKind.Local);
                    currentFromLocalDate = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 0, 0, 0, DateTimeKind.Local);
                    int intervalInSeconds = (int)Math.Ceiling(toLocalDate.Subtract(currentFromLocalDate).TotalSeconds / 30f);

                    AppManager.GetDwellLoungeOccupancies(currentLounge.LoungeID,
                                                      currentLounge, currentFromLocalDate, toLocalDate, intervalInSeconds, (data) =>
                                                      {
                                                          BeginInvokeOnMainThread(async () =>
                                                          {
                                                              try
                                                              {
                                                                  if (ChartColumnBarSeriesActual == null || data == null ||
                                                                      ((LoungeOccupancyElapsedData)data).OccupancyRecords == null)
                                                                  {
                                                                      AgentMenuTabController.HideLoading();
                                                                      return;
                                                                  }

                                                                  var listData = new List<LoungeOccupancy>();


                                                                  foreach (var item in ((LoungeOccupancyElapsedData)data).OccupancyRecords)
                                                                  {
                                                                      if (item.UtcTime.Day == dtNow.Day)
                                                                      {
                                                                          listData.Add(new LoungeOccupancy()
                                                                          {
                                                                              Time = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, item.LocalTime.Hour, 0, 0),
                                                                              OccupancyTotal = item.Values[0],
                                                                              OccupancyTypePassenger = 0
                                                                          });
                                                                      }
                                                                      else
                                                                      {
                                                                          listData.Add(new LoungeOccupancy()
                                                                          {
                                                                              Time = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, item.LocalTime.Hour, 0, 0),
                                                                              OccupancyTotal = item.Values[0],
                                                                              OccupancyTypePassenger = 1
                                                                          });
                                                                      }
                                                                  }


                                                                  decimal total = 0;
                                                                  if (ChartColumnBar != null && listData.Count > 0)
                                                                  {
                                                                      if ((total = listData.Max(x => x.OccupancyTotal)) > 0)
                                                                      {
                                                                          var graphMax = (int)Math.Ceiling((float)total * 1.1);
                                                                          ChartColumnBar.SecondaryAxis.Maximum = new NSNumber(graphMax);

                                                                          var interval = (float)graphMax / 10;
                                                                          if (interval < 1)
                                                                              ChartColumnBar.SecondaryAxis.Interval = new NSNumber(1);
                                                                          else ChartColumnBar.SecondaryAxis.Interval = new NSNumber(Math.Floor(interval));
                                                                      }

                                                                       ((SFDateTimeAxis)ChartColumnBar.PrimaryAxis).IntervalType = SFChartDateTimeIntervalType.Hours;
                                                                      ((SFDateTimeAxis)ChartColumnBar.PrimaryAxis).Interval = new NSNumber(1);
                                                                  }

                                                                  ChartColumnBarSeriesActual.ItemsSource = listData.Where(c => c.OccupancyTypePassenger == 0).Select(x => new GraphDataCell()
                                                                  {
                                                                      Time = x.Time.DateTime,
                                                                      Total = (double)x.OccupancyTotal,
                                                                      PassengerType = 0
                                                                  }).ToList();

                                                                  ChartColumnBarSeriesWeekly.ItemsSource = listData.Where(c => c.OccupancyTypePassenger == 1).Select(x => new GraphDataCell()
                                                                  {
                                                                      Time = x.Time.DateTime,
                                                                      Total = (double)x.OccupancyTotal,
                                                                      PassengerType = 1
                                                                  }).ToList();

                                                              }
                                                              catch (Exception ex)
                                                              {
                                                                  LogsRepository.AddError("Get Lounge Occupancy List call error.", ex);
                                                                  Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
                                                              }
                                                              finally
                                                              {
                                                                  await Task.Delay(new TimeSpan(0, 0, (int)ChartSeriesGuest.AnimationDuration));
                                                                  AgentMenuTabController.HideLoading();
                                                              }

                                                          });

                                                      });
                }
                catch
                {

                }
            });
        }

        private string GetTypePassenger(decimal type)
        {
            switch (type)
            {
                case 0:
                    return "Pax";
                case 1:
                case 2:
                    return "Guest";
                case 3:
                    return "Reentry";
                case 5:
                    return "Refuse";
                default:
                    return "Unknown";
            }
        }

        public void PrefetchRows(UITableView tableView, NSIndexPath[] indexPaths)
        {
            if (indexPaths != null && !_refreshingLineChart)
                GetOccupancyDataByVisibleRows(indexPaths, false);
        }

        private object lockQuery = new object();
        private bool isBusy = false;
        List<Workstation> originalLounges = new List<Workstation>();

        private void GetOccupancyDataByVisibleRows(NSIndexPath[] visibleRows, bool noForce = true)
        {
            try
            {
                lock (lockQuery)
                {
                    if (isBusy && noForce)
                        return;

                  /*  List<Workstation> lounges = new List<Workstation>();

                    isBusy = true;
                    foreach (var visibleRow in visibleRows)
                    {
                        if (originalLounges.Count(c => c.LoungeID == loungeItems[visibleRow.Row].Workstation.LoungeID) == 0)
                        {
                            originalLounges.Add(loungeItems[visibleRow.Row].Workstation);
                            lounges.Add(loungeItems[visibleRow.Row].Workstation);
                        }
                    }

                    if (lounges.Count == 0)
                    {
                        return;
                    }

                    var dtNow = DateTime.Now; */

                    /*  AppManager.getLoungeOccupanciesList(lounges.Select(c => c.LoungeID).ToList(), lounges.Select(c => c.WorkstationID).ToList(), lounges.Select(c => c.AirportCode).ToList(), dtNow, dtNow, 120, (data) =>
                      {
                          BeginInvokeOnMainThread(() =>
                          {
                              if (data != null)
                              {
                                  var occupancies = (List<LoungeOccupancy>)data;

                                  for (int i = 0; i < occupancies.Count; i++)
                                  {
                                      var occupancy = occupancies[i];

                                      var loungeCellData = loungeItems.FirstOrDefault(c => c.Workstation.LoungeID == occupancy.LoungeId);

                                      if (loungeCellData != null)
                                      {
                                          loungeCellData.IsUpdated = true;
                                          loungeCellData.Progress = (double)occupancy.Percentage;

                                          var cell = loungeTableView.CellAt(visibleRows[i]) as LoungeCell;
                                          if (cell != null)
                                              cell.updateCell(loungeCellData.name, loungeCellData.description, loungeCellData.Progress);
                                      }
                                  }
                              }


                              loungeTableView.ReloadData();

                              isBusy = false;
                          });

                      });*/

                    foreach (var visibleRow in visibleRows)
                    {
                        var loungeCellData = loungeItems[visibleRow.Row];

                        if (loungeCellData.IsUpdated)
                            continue;

                        AppManager.GetLoungeOccupancyWhithQueue(loungeCellData.Workstation, (data) =>
                        {
                            BeginInvokeOnMainThread(() =>
                            {
                                if (data == null)
                                    return;

                                var loungeOccupancy = (LoungeOccupancy)data;
                                loungeCellData.IsUpdated = true;

                                loungeCellData.Progress = (double)loungeOccupancy.Percentage;

                                var cell = loungeTableView.CellAt(visibleRow) as LoungeCellStatistics;
                                if (cell != null)
                                    cell.UpdateCell(loungeCellData.name, loungeCellData.description, loungeCellData.Progress);

                                //loungeTableView.ReloadRows(new NSIndexPath[] { visibleRow }, UITableViewRowAnimation.None);
                            });
                        });
                    }

                  //  loungeTableView.ReloadData();

                }
            }
            catch (Exception ef)
            {
                LogsRepository.AddError("Get Lounge Occupancies List call error.", ef);
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ef);
            }


            /*  if (_refreshingLineChart)
                  return;

              int i = 0;

              foreach (var visibleRow in visibleRows)
              {
                  try
                  {
                      if (i > 0)
                          return;

                      i++;

                      var loungeCellData = loungeItems[visibleRow.Row];

                      AppManager.GetLoungeOccupancyWhithQueue(loungeCellData.Workstation, (data) =>
                      {
                          BeginInvokeOnMainThread(() =>
                          {
                              if (data == null)
                                  return;

                              var loungeOccupancy = (LoungeOccupancy)data;
                              loungeCellData.Progress = (double)loungeOccupancy.Percentage;

                              var cell = loungeTableView.CellAt(visibleRow) as LoungeCellStatistics;
                              if (cell != null)
                                  cell.UpdateCell(loungeCellData.name, loungeCellData.description, loungeCellData.Progress);

                              //loungeTableView.ReloadRows(new NSIndexPath[] { visibleRow }, UITableViewRowAnimation.None);
                          });
                      });
                  }
                  catch
                  {
                  }
              }*/
        }

        private void LoadTexts(Language? language = null)
        {
            InvokeOnMainThread(() =>
            {
                this.TabBarItem.Title = AppDelegate.TextProvider.GetText(2011);

                titleOfPage = AppDelegate.TextProvider.GetText(2145);
                titleOfPieChart = AppDelegate.TextProvider.GetText(2146);

                formatedLoungeText = AppDelegate.TextProvider.GetText(2147);
                formatedCustomersText = AppDelegate.TextProvider.GetText(2148);
                formatedPrimaryText = AppDelegate.TextProvider.GetText(2149);
                formatedGuestText = AppDelegate.TextProvider.GetText(2150);

                secondaryAxis.Title.Text = new NSString(AppDelegate.TextProvider.GetText(2151));
                secondaryBarAxis.Title.Text = new NSString(AppDelegate.TextProvider.GetText(2151));

                primaryAxis.Title.Text = new NSString(AppDelegate.TextProvider.GetText(2152));
                primaryBarAxis.Title.Text = new NSString(AppDelegate.TextProvider.GetText(2152));

                ChartSeriesPax.Label = new NSString(AppDelegate.TextProvider.GetText(2153));
                ChartSeriesGuest.Label = new NSString(AppDelegate.TextProvider.GetText(2154));
                ChartColumnBarSeriesActual.Label = new NSString(AppDelegate.TextProvider.GetText(2155));
                ChartColumnBarSeriesWeekly.Label = new NSString(AppDelegate.TextProvider.GetText(2156));
                ChartColumnBar.Title.Text = DateTime.Now.ToLongDateString() + AppDelegate.TextProvider.GetText(2157);

                TypeChartButtonSize = AppDelegate.TextProvider.SelectedLanguage == Language.Portuguese ? 52 : 35;
            });
        }
    }

    public class CustomSFChartTrackballBehavior : SFChartTrackballBehavior
    {
        private nfloat scaleFactor;

        public CustomSFChartTrackballBehavior(nfloat scaleFactor)
        {
            this.scaleFactor = scaleFactor;
        }

        public override void TrackballLabelsGenerated(List<SFChartPointInfo> pointsInfo)
        {
            base.TrackballLabelsGenerated(pointsInfo);

            TrackBallChanged?.Invoke(pointsInfo);
        }

        public override UIView ViewForTrackballLabel(SFChartPointInfo pointInfo)
        {
            pointInfo.MarkerStyle.BorderColor = pointInfo.Series.Color;

            UIView customView = new UIView
            {
                Frame = new CGRect(0, 0, 48, 30)
            };

            UILabel xLabel = new UILabel
            {
                Frame = new CGRect(7, 0, 50, 15),
                TextColor = UIColor.White,
                Font = UIFont.FromName("HelveticaNeue-BoldItalic", 13f),
                Text = (pointInfo.Data as GraphDataCell).Total.ToString()
            };

            UILabel yLabel = new UILabel
            {
                Frame = new CGRect(7, 15, 50, 15),
                TextColor = UIColor.White,
                Font = UIFont.FromName("Helvetica", 8f),
                Text = ""
            };

            customView.AddSubview(xLabel);
            customView.AddSubview(yLabel);

            return customView;
        }

        public async override void Hide()
        {
            await Task.Delay(1000);

            base.Hide();
        }

        public event TrackedBallHandler TrackBallChanged;

        public delegate void TrackedBallHandler(List<SFChartPointInfo> pointsInfo);
    }
}