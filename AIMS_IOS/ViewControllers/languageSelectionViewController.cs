using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class languageSelectionViewController : UIViewController
    {
        List<LanguageCellData> items = new List<LanguageCellData>();

        public languageSelectionViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
            updateTableLayout();
            items = AppManager.getListOfLanguges();

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            langTable.Source = new LanguageTableSource(items, this, langTable);

			langTable.TableFooterView = new UIView();

            updateLayout();
		}

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            updateLayout();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            
            updateLayout();
            
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            
            #region Requesting the Location Access
            var locRes = AppDelegate.Current.RequestLocationAccess();
            if (!locRes.IsSucceeded)
            {
                InvokeOnMainThread(() =>//We are not inheriting AIMSViewController... No need here
                {
                    UIAlertView alert = new UIAlertView()
                    {
                        Title = "Error",
                        Message = locRes.Message
                    };
                    alert.AddButton("Ok");

                    alert.Show();
                });
                return;
            } 
            #endregion
        }

        public void updateLayout()
        {
            CGRect viewFrame = UIScreen.MainScreen.Bounds;
            float mainMargin = 20f;
            //----------------------------Title view----------------------------------
            titleView.Frame = new CGRect(0f, 0f, viewFrame.Width, viewFrame.Height * 0.3f);

            logo.Frame = new CGRect(mainMargin, titleView.Frame.Height * 0.2f,
                viewFrame.Width - (mainMargin * 2), titleView.Frame.Height * 0.5f);

            //--------------------lang table--------------------
            langTable.Frame = new CGRect(0f,
                titleView.Frame.Y + titleView.Frame.Height + 5f,
                viewFrame.Width,
                viewFrame.Height - titleView.Frame.Y);
        }

        public void updateTableLayout()
        {
            nfloat height =  UIScreen.MainScreen.Bounds.Width < UIScreen.MainScreen.Bounds.Height ?
                UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Width;

            //ratio based on iphone 6 minimum of 28f
            nfloat estimatedHeight = 32f / 650f * height;
            estimatedHeight = estimatedHeight < 28f ? 44f : estimatedHeight;

            langTable.EstimatedRowHeight = estimatedHeight;
            langTable.RowHeight = estimatedHeight;
        }

        public void onRowSelected()
        {
            UIViewTransition.displayFromRightToLeftLinear(this, AppDelegate.LoginView, 0.3f);
        }
    }
}