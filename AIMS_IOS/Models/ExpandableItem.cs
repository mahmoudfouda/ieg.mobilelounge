﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace AIMS_IOS.Models
{
    public class ExpandableItem : Item
    {
        private List<Item> children = null;
        public List<Item> Children {
            get {
                return this.children;
            }
            set {
                this.children = value;
                if(value != null && value.Count > 0)
                {
                    foreach (var children in value)
                    {
                        if(children.Parent != this)
                            children.Parent = this;
                    }
                }
            }
        }

        public bool HasChild => Children != null && Children.Count > 0;

        public bool IsChild => !HasChild;
    }
}