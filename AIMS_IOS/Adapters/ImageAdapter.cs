using AIMS;
using AIMS_IOS.Utility;
using CoreGraphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;

namespace AIMS_IOS
{
    public delegate void UIImageLoadHandler(byte[] imageBytes);
    public delegate void UIAirlineImageLoadHandler(byte[] imageBytes, AirlineCell cell, AirlineCellData item);
    public delegate void UICardImageLoadHandler(byte[] imageBytes, CardsCell cell, CardCellData item, CGPoint frameSize);

    public static class ImageAdapter
    {
        private static Dictionary<string, DateTime> loadRequests = new Dictionary<string, DateTime>();

        private const int imageLoadTimeout = 20;

        public static void LoadImage(string imageName, UIImageLoadHandler callback, bool forceGet = false)
        {
            if (callback == null) return;

            var ir = new ImageRepository();

            ir.LoadImage(imageName, (imageResult) =>
            {
                if (imageResult == null)
                {
                    LogsRepository.AddError("Error in ImageAdapter.LoadImage()","The result is null");
                    return;
                }

                if (imageResult.IsSucceeded)
                {
                    var bytes = imageResult.ReturnParam;

                    //if (bytes == null || bytes.Length == 0) return;

                    try
                    {
                        callback.Invoke(bytes);
                    }
                    catch (Exception eex)
                    {
                        var m = eex.Message;
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                    }
                }
            }, forceGet);
        }

        public static void LoadCardImage(string imageName, CardsCell cell, CardCellData item, CGPoint frameSize, UICardImageLoadHandler callback, bool forceGet = false)
        {
            if (callback == null) return;

            /*ImageRepository.Current*/
            var ir = new ImageRepository();

            ir.LoadImage(imageName, (imageResult) => 
            {
                if (imageResult == null)
                {
                    LogsRepository.AddError("Error in ImageAdapter.LoadCardImage()", "The result is null");
                    return;
                }

                if (imageResult.IsSucceeded)
                {
                    var bytes = imageResult.ReturnParam;

                    if (bytes == null || bytes.Length == 0) return;

                    try
                    {
                        callback.Invoke(bytes, cell, item, frameSize);
                    }
                    catch (Exception eex)
                    {
                        var m = eex.Message;
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                    }
                }
            });
        }

        public static void LoadAirlineImage(string imageName, AirlineCell cell, AirlineCellData item, UIAirlineImageLoadHandler callback, bool forceGet = false)
        {
            if (callback == null) return;
            
            var ir = new ImageRepository();

            ir.LoadImage(imageName, (imageResult) =>
            {
                if (imageResult == null)
                {
                    LogsRepository.AddError("Error in ImageAdapter.LoadCardImage()", "The result is null");
                    return;
                }

                if (imageResult.IsSucceeded)
                {
                    var bytes = imageResult.ReturnParam;

                    if (bytes == null || bytes.Length == 0) return;
                    try
                    {
                        callback.Invoke(bytes, cell, item);
                    }
                    catch (Exception eex)
                    {
                        var m = eex.Message;
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(eex);
                    }
                }
            });
        }
    }
}