﻿using AIMS;
using System;
using System.Collections.Generic;
using System.Text;
using AIMS.Models;
using CoreLocation;
using System.Linq;
using Ieg.Mobile.DataContracts.MobileLounge.ServiceModel;
using Ieg.Mobile.DataContracts.Models;

namespace AIMS_IOS.Adapters
{
    public class GPSAdapter : CLLocationManager, IGpsAdapter
    {
        public event EventHandler<Position> GpsAdapter_OnLocationChanged;
        public event EventHandler<Exception> OnLog;

        public static GPSAdapter Current { get; set; }

        public GPSAdapter() : base()
        {
            //try
            //{
            this.LocationsUpdated += GPSAdapter_LocationsUpdated;
            Current = this;
            //}
            //catch (Exception ex)
            //{
            //    var msg = ex.Message;//just for debugging
            //    //TODO: Log here
            //}
        }

        //public void TestFenceValidation()//Just for testing the fence validation failed behavior
        //{
        //    var loc = new CLLocation(new CLLocationCoordinate2D(5.11111f,-4.55555f),35.1f,65.2f,10f, new Foundation.NSDate());

        //    GPSAdapter_LocationsUpdated(this,new CLLocationsUpdatedEventArgs(new CLLocation[] {
        //        loc
        //    }));
        //}

        private void GPSAdapter_LocationsUpdated(object sender, CLLocationsUpdatedEventArgs e)
        {
            if (e == null) return;
            if (e.Locations == null || e.Locations.Length == 0) return;
            var loc = e.Locations.ToList().OrderByDescending(x => x.Timestamp).First();
            if (GpsAdapter_OnLocationChanged != null)
            {
                Position p = null;
                try
                {
                    p = new Position
                    {
                        Latitude = loc.Coordinate.Latitude,
                        Longitude = loc.Coordinate.Longitude,
                        Accuracy = (float)loc.HorizontalAccuracy,
                        Altitude = loc.Altitude
                    };
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;//just for debugging
                    //TODO: Log here
                }

                GpsAdapter_OnLocationChanged.Invoke(this, p);
            }
        }

        public void Start()
        {
            this.PausesLocationUpdatesAutomatically = false;
            this.DesiredAccuracy = CLLocation.AccuracyHundredMeters;//Weired accuracies have been detected like 7610 meters
            this.StartUpdatingLocation();
            if (this.Location != null && this.Location.Coordinate.Latitude != 0 && this.Location.Coordinate.Longitude != 0)
                GPSAdapter_LocationsUpdated(this, new CLLocationsUpdatedEventArgs(new CLLocation[] { this.Location }));
        }

        public void Stop()
        {
            this.StopUpdatingLocation();
        }
    }
}
