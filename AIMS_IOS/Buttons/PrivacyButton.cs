﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public partial class PrivacyButton : UIButton
    {
        public UIEdgeInsets BorderWidth = new UIEdgeInsets(1f, 1f, 1f, 1f);

        public UIColor BorderColor = new UIColor(CoreImage.CIColor.BlueColor);

        public PrivacyButton (IntPtr handle) : base (handle)
        {
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            UIDrawBorder.DrawBottomtBorder(context, rect, this.Frame, BorderColor, BorderWidth);
        }
    }
}