﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;

namespace AIMS_IOS
{
    //public abstract class ExpandableTableChildItem
    //{
    //    public int Index { get; set; }
    //    public string Text { get; set; }
    //}

    //public abstract class ExpandableTableItem : ExpandableTableChildItem
    //{
    //    public abstract List<ExpandableTableChildItem> Children { get; set; }
    //}

    //public delegate void RowExpandCollapseHandler(UITableView tableView, NSIndexPath rowIndex, bool isExpanded);

    //public abstract class ExpandableTableSource : UITableViewSource
    //{
    //    public List<GuestCategoryCellData> Items;
    //    protected Action<GuestCategoryCellData> TSelected;
    //    protected int currentExpandedIndex = -1;

    //    //public event EventHandler<GuestCategoryCellData> OnChildSelected;
    //    public event RowExpandCollapseHandler OnRowExpandedOrCollapsed;

    //    public ExpandableTableSource() { }

    //    public ExpandableTableSource(Action<GuestCategoryCellData> TSelected)
    //    {
    //        this.TSelected = TSelected;
    //    }

    //    public override nint RowsInSection(UITableView tableview, nint section)
    //    {
    //        var realRows = Items.Count + ((currentExpandedIndex > -1) ? (Items[currentExpandedIndex].Guests == null ? 0 : Items[currentExpandedIndex].Guests.Count) + 1 : 0);
    //        return realRows;
    //        //return Items.Count + ((currentExpandedIndex > -1) ? 1 : 0);
    //    }

    //    void collapseSubItemsAtIndex(UITableView tableView, int index)
    //    {
    //        //tableView.BeginUpdates();
    //        if (Items[index].Guests != null)
    //        {
    //            for (int i = Items[index].Guests.Count; i >= 0; i--)
    //            {
    //                tableView.DeleteRows(new[] { NSIndexPath.FromRowSection(index + 1 + i, 0) }, UITableViewRowAnimation.Fade);
    //            }
    //        }
    //        else  
    //            tableView.DeleteRows(new[] { NSIndexPath.FromRowSection(index + 1, 0) }, UITableViewRowAnimation.Fade);
    //        //foreach (var item in Items[index].Children)
    //        //{
    //        //    tableView.DeleteRows(new[] { NSIndexPath.FromRowSection(index + 1, 0) }, UITableViewRowAnimation.Fade);
    //        //}
    //        //tableView.EndUpdates();
    //    }

    //    void expandItemAtIndex(UITableView tableView, int index)
    //    {
    //        //tableView.BeginUpdates();
    //        int insertPos = index + 1;
    //        if (Items[index].Guests != null)
    //        {
    //            for (int i = 0; i <= Items[index].Guests.Count; i++)
    //            {
    //                tableView.InsertRows(new[] { NSIndexPath.FromRowSection(insertPos++, 0) }, UITableViewRowAnimation.Fade);
    //            }
    //        }
    //        else
    //            tableView.InsertRows(new[] { NSIndexPath.FromRowSection(insertPos++, 0) }, UITableViewRowAnimation.Fade);
    //        //foreach (var item in Items[index].Children)
    //        //{
    //        //    tableView.InsertRows(new[] { NSIndexPath.FromRowSection(insertPos++, 0) }, UITableViewRowAnimation.Fade);
    //        //}
    //        //tableView.EndUpdates();
    //    }

    //    protected bool isChild(NSIndexPath indexPath)
    //    {
    //        var item = Items[currentExpandedIndex > -1 ? currentExpandedIndex : indexPath.Row];
    //        var childrenCount = item.Guests == null ? 0 : item.Guests.Count;
    //        var isExpandedAtAll = currentExpandedIndex > -1;
    //        var isSelectedRowAfterExpandedRow = indexPath.Row > currentExpandedIndex;
    //        var isSelectedRowInChildrensRange = indexPath.Row <= currentExpandedIndex + 1 + childrenCount;//TODO: plus children count

    //        return isExpandedAtAll && isSelectedRowAfterExpandedRow && isSelectedRowInChildrensRange;
    //    }

    //    public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
    //    {
    //        if (isChild(indexPath))
    //        {
    //            //Handle selection of child cell
    //            Console.WriteLine("You touched a child!");
    //            tableView.DeselectRow(indexPath, true);
    //            //if (OnChildSelected != null) OnChildSelected.Invoke(this, Items[indexPath.Row]);
    //            return;
    //        }
    //        tableView.BeginUpdates();
    //        if (currentExpandedIndex == indexPath.Row)
    //        {
    //            this.collapseSubItemsAtIndex(tableView, currentExpandedIndex);
    //            if (OnRowExpandedOrCollapsed != null) OnRowExpandedOrCollapsed.Invoke(tableView, NSIndexPath.FromRowSection(currentExpandedIndex, 0), false);
    //            currentExpandedIndex = -1;
    //        }
    //        else
    //        {
    //            var shouldCollapse = currentExpandedIndex > -1;
    //            if (shouldCollapse)
    //            {
    //                this.collapseSubItemsAtIndex(tableView, currentExpandedIndex);
    //                if (OnRowExpandedOrCollapsed != null) OnRowExpandedOrCollapsed.Invoke(tableView, NSIndexPath.FromRowSection(currentExpandedIndex, 0), false);
    //            }
    //            currentExpandedIndex = (shouldCollapse && indexPath.Row > currentExpandedIndex) ? indexPath.Row - 1 : indexPath.Row;
    //            this.expandItemAtIndex(tableView, currentExpandedIndex);
    //            if (OnRowExpandedOrCollapsed != null) OnRowExpandedOrCollapsed.Invoke(tableView, indexPath, true);
    //        }
    //        tableView.EndUpdates();
    //        tableView.DeselectRow(indexPath, true);
    //    }

    //    public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
    //    {
    //        if (isChild(indexPath))
    //            return GetChildCell(tableView, currentExpandedIndex, indexPath.Row - currentExpandedIndex - 1);//TODO
    //        else return GetParentCell(tableView, indexPath.Row);
    //    }

    //    //TODO: Implement in child class
    //    public abstract UITableViewCell GetChildCell(UITableView tableView, int parentIndex, int childIndex);

    //    //TODO: Implement in child class
    //    public abstract UITableViewCell GetParentCell(UITableView tableView, int parentIndex);
    //}

    public abstract class ExpandableTableSource<T> : UITableViewSource
        where T : class
    {
        public delegate void OnItemSelectionHandler(T selectedItem, bool isChild);
        public delegate void OnItemExpansionHandler(UITableViewCell cell, NSIndexPath rowIndexPath);
        protected IList<T> CurrentChildItems { get; set; }
        protected IList<T> Items;
        protected readonly Action<T> TSelected;
        //protected readonly string ParentCellIdentifier = "parentCell";
        //protected readonly string ChildCellIndentifier = "childCell";
        protected int currentExpandedIndex = -1;

        public event OnItemSelectionHandler OnItemSelected;
        public event OnItemExpansionHandler OnRowExpanded;
        public event OnItemExpansionHandler OnRowCollapsed;

        public int SelectedParentIndex { get; set; } = -1;

        public int SelectedChildIndex { get; set; } = -1;

        public ExpandableTableSource(IList<T> items)
        : this(items, (i) => { }) { }

        public ExpandableTableSource(IList<T> items, Action<T> TSelected)
        {
            if (items == null) throw new ArgumentNullException("items");

            this.CurrentChildItems = items;
            this.Items = items;

            this.TSelected = TSelected;
        }

        public override nint RowsInSection(UITableView cell, nint section)
        {
            var rows = Items.Count + ((currentExpandedIndex > -1) ? this.CurrentChildItems.Count() : 0);
            return rows;
        }

        private void notifyRowExpand(UITableViewCell cell, NSIndexPath rowIndex)
        {
            if (OnRowExpanded == null || cell == null) return;

            OnRowExpanded.Invoke(cell, rowIndex);
        }

        private void notifyRowCollapse(UITableViewCell cell, NSIndexPath rowIndex)
        {
            if (OnRowCollapsed == null || cell == null) return;

            OnRowCollapsed.Invoke(cell, rowIndex);
        }

        public virtual void CollapseSubItemsAtIndex(UITableView tableView, int index)
        {
            for (int i = index + CurrentChildItems.Count; i > index; i--)
                tableView.DeleteRows(new[] { NSIndexPath.FromRowSection(i, 0) }, UITableViewRowAnimation.Fade);
        }

        public virtual void ExpandItemAtIndex(UITableView tableView, int index)
        {
            int insertPos = index + 1;

            foreach (var curtrentItem in this.CurrentChildItems)
                tableView.InsertRows(new[] { NSIndexPath.FromRowSection(insertPos++, 0) }, UITableViewRowAnimation.Fade);
        }

        protected T GetItem(NSIndexPath indexPath)
        {
            var child = this.isChild(indexPath);
            if (child)
            {
                return this.CurrentChildItems[indexPath.Row - this.currentExpandedIndex - 1];
            }
            else
            {
                return this.CurrentChildItems[indexPath.Row];
            }
        }

        protected bool isChild(NSIndexPath indexPath)
        {
            return currentExpandedIndex > -1 &&
                   indexPath.Row > currentExpandedIndex &&
                   indexPath.Row <= currentExpandedIndex + ((currentExpandedIndex > -1) ? this.CurrentChildItems.Count() : 0);
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            int collapsedIndex = -1, expandedIndex = -1;
            if (isChild(indexPath))
            {
                tableView.DeselectRow(indexPath, true);
                var currentChildIndex = (indexPath.Row > currentExpandedIndex) ? indexPath.Row - currentExpandedIndex - 1 : indexPath.Row;
                try
                {
                    SelectedChildIndex = currentChildIndex;
                    if (OnItemSelected != null && currentChildIndex > -1 && currentChildIndex < this.CurrentChildItems.Count)
                        OnItemSelected.Invoke(this.CurrentChildItems[currentChildIndex], true);
                }
                catch { }
                return;
            }
            tableView.BeginUpdates();
            if (currentExpandedIndex == indexPath.Row)
            {
                this.CollapseSubItemsAtIndex(tableView, currentExpandedIndex);
                collapsedIndex = currentExpandedIndex;
                currentExpandedIndex = -1;
            }
            else
            {
                var shouldCollapse = currentExpandedIndex > -1;//false;//
                if (shouldCollapse)
                {
                    this.CollapseSubItemsAtIndex(tableView, currentExpandedIndex);
                    collapsedIndex = currentExpandedIndex;
                }

                //currentExpandedIndex = (shouldCollapse && indexPath.Row > currentExpandedIndex) ? indexPath.Row - 1 : indexPath.Row;
                currentExpandedIndex = (shouldCollapse && indexPath.Row > currentExpandedIndex) ? indexPath.Row - CurrentChildItems.Count : indexPath.Row;

                this.CurrentChildItems = new List<T>(this.GetCurrentChildrenForParent(this.Items[currentExpandedIndex]));
                this.ExpandItemAtIndex(tableView, currentExpandedIndex);
                expandedIndex = currentExpandedIndex;
            }
            tableView.EndUpdates();
            tableView.DeselectRow(indexPath, true);


            if(collapsedIndex != -1)
            {
                if (expandedIndex != -1 && collapsedIndex > expandedIndex)
                    collapsedIndex += CurrentChildItems.Count;
                var ip = NSIndexPath.FromRowSection(collapsedIndex, 0);
                notifyRowCollapse(tableView.CellAt(ip), ip);
            }

            if (expandedIndex != -1)
            {
                var ip = NSIndexPath.FromRowSection(expandedIndex, 0);
                notifyRowExpand(tableView.CellAt(ip), ip);
            }

            SelectedParentIndex = currentExpandedIndex;
            if (OnItemSelected != null && currentExpandedIndex > -1 && currentExpandedIndex < this.Items.Count)
            {
                OnItemSelected.Invoke(this.Items[currentExpandedIndex], false);
            }
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            throw new NotImplementedException();
        }

        protected virtual IEnumerable<T> GetCurrentChildrenForParent(T parent)
        {
            return Enumerable.Empty<T>();
        }
    }
}
