﻿using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public static class UITabSwipe
    {

        public static void HandleLeftSwipe(/*UISwipeGestureRecognizer recognizer,*/ UIViewController view )
        {
            if (view.TabBarController.SelectedIndex + 1 < view.TabBarController.ViewControllers.Length)
            {
                UIView fromView = view.TabBarController.SelectedViewController.View;
                UIView toView = view.TabBarController.ViewControllers[view.TabBarController.SelectedIndex + 1].View;

                // Get the size of the view area.
                var viewSize = new CGRect(fromView.Frame.X, fromView.Frame.Y,
                    fromView.Frame.Width, fromView.Frame.Height);

                // Add the to view to the tab bar view.
                fromView.Superview.AddSubview(toView);

                var minAlpha = -view.View.Bounds.Width;
                var maxAlpha = view.View.Bounds.Width;
                var midAlpha = 0.0;
                // Position it off screen.
                toView.Frame = new CGRect(maxAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                UIView.Animate(0.3f, .1, UIViewAnimationOptions.CurveEaseOut,
                    () =>
                    {
                        fromView.Frame = new CGRect(minAlpha, fromView.Frame.Y, maxAlpha,
                            fromView.Frame.Height);
                        toView.Frame = new CGRect(midAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                        toView.UpdateFocusIfNeeded();
                    },
                    () =>
                    {
                        fromView.RemoveFromSuperview();
                        toView.Frame = new CGRect(midAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                        //toView.UpdateConstraints();
                        view.TabBarController.SelectedIndex = view.TabBarController.SelectedIndex + 1;
                        toView.UpdateFocusIfNeeded();
                    }
                );

            }
        }

        public static void GotoTabRight(UITabBarController view, nint fromIndex ,nint toIndex)
        {

            UIView fromView = view.TabBarController.ViewControllers[fromIndex].View;
            UIView toView = view.TabBarController.ViewControllers[toIndex].View;

            // Get the size of the view area.
            var viewSize = new CGRect(fromView.Frame.X, fromView.Frame.Y,
                    fromView.Frame.Width, fromView.Frame.Height);

                // Add the to view to the tab bar view.
                fromView.Superview.AddSubview(toView);

                var minAlpha = -view.View.Bounds.Width;
                var maxAlpha = view.View.Bounds.Width;
                var midAlpha = 0.0;
                // Position it off screen.
                toView.Frame = new CGRect(maxAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                UIView.Animate(0.3f, .1, UIViewAnimationOptions.CurveEaseOut,
                    () =>
                    {
                        fromView.Frame = new CGRect(minAlpha, fromView.Frame.Y, maxAlpha,
                            fromView.Frame.Height);
                        toView.Frame = new CGRect(midAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                        toView.UpdateFocusIfNeeded();
                    },
                    () =>
                    {
                        fromView.RemoveFromSuperview();
                        toView.Frame = new CGRect(midAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                        //toView.UpdateConstraints();
                        toView.UpdateFocusIfNeeded();
                        view.TabBarController.SelectedIndex = toIndex;
                    }
                );

            
        }

        public static void HandleRightSwipe(/*UISwipeGestureRecognizer recognizer,*/ UIViewController view)
        {
            if (view.TabBarController.SelectedIndex != 0)
            {
                UIView fromView = view.TabBarController.SelectedViewController.View;
                UIView toView = view.TabBarController.ViewControllers[view.TabBarController.SelectedIndex - 1].View;

                // Get the size of the view area.
                var viewSize = fromView.Frame;

                // Add the to view to the tab bar view.
                fromView.Superview.AddSubview(toView);

                var minAlpha = -view.View.Bounds.Width;
                var maxAlpha = view.View.Bounds.Width;
                var midAlpha = 0.0;
                // Position it off screen.
                toView.Frame = new CGRect(minAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                UIView.Animate(.3, .1, UIViewAnimationOptions.CurveEaseOut,
                    () =>
                    {
                        fromView.Frame = new CGRect(maxAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                        toView.Frame = new CGRect(midAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                    },
                    () =>
                    {
                        fromView.RemoveFromSuperview();
                        view.TabBarController.SelectedIndex = view.TabBarController.SelectedIndex - 1;
                    }
                );

            }
        }



    }
}
