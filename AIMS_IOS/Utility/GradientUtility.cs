﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using CoreGraphics;
using CoreAnimation;

namespace AIMS_IOS
{
    public static class GradientUtility
    {

        public static void addErrorGradiant(UIView item)
        {

            CAGradientLayer gradient = new CAGradientLayer();

            gradient.Frame = item.Bounds;
            gradient.NeedsDisplayOnBoundsChange = true;
            gradient.MasksToBounds = true;
            gradient.CornerRadius = item.Layer.CornerRadius;

            #region Siavash changes
            //gradient.Colors = new CGColor[] { (new UIColor(0.9296875f, 0.9296875f,0f, 0.8f)).CGColor,
            //    new UIColor(1f, 1f, 1f, 0.8f).CGColor };

            gradient.BorderColor = new UIColor(1f, 0.1686274f, 0.1686274f, 0.8f).CGColor;
            gradient.BorderWidth = 2f;
            gradient.Colors = new CGColor[] { (new UIColor(1f, 0.7333333f,0f, 0.8f)).CGColor,
                new UIColor(1f, 1f, 1f, 0.8f).CGColor };
            #endregion 

            gradient.Locations = new Foundation.NSNumber[] { 0, 1 };
            gradient.CornerRadius = 5f;
            gradient.Name = "gradient";


            item.Layer.AddSublayer(gradient);

        }

        public static void removeGradiants(UIView item)
        {
            for (int i = 0; i < item.Layer.Sublayers.Length; i++)
            {
                if (item.Layer.Sublayers[i].Name == "gradient")
                {
                    item.Layer.Sublayers[i].RemoveFromSuperLayer();
                }
            }
            
        }

    }
}
