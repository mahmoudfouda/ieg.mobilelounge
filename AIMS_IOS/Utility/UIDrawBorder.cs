﻿using System;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
	public static class UIDrawBorder
	{

		public static void DrawBorders(CGContext context, CGRect rect, CGRect Frame, UIColor BorderColor, UIEdgeInsets BorderWidth)
		{
			var xMin = rect.GetMinX();
			var xMax = rect.GetMaxX();

			var yMin = rect.GetMinY();
			var yMax = rect.GetMaxY();

			var fWidth = Frame.Size.Width;
			var fHeight = Frame.Size.Height;

			context.SetFillColor(BorderColor.CGColor);
			context.FillRect(new CGRect(xMin, yMin, fWidth, BorderWidth.Top));

			context.SetFillColor(BorderColor.CGColor);
			context.FillRect(new CGRect(xMin, yMin, BorderWidth.Left, fHeight));

			context.SetFillColor(BorderColor.CGColor);
			context.FillRect(new CGRect(xMax - BorderWidth.Right, yMin, BorderWidth.Right, fHeight));

			context.SetFillColor(BorderColor.CGColor);
			context.FillRect(new CGRect(xMin, yMax - BorderWidth.Bottom, fWidth, BorderWidth.Bottom));
		}

		public static void DrawLeftBorder(CGContext context, CGRect rect, CGRect Frame, UIColor BorderColor, UIEdgeInsets BorderWidth)
		{
			
			var xMin = rect.GetMinX();
			var yMin = rect.GetMinY();

			var fHeight = Frame.Size.Height;

			context.SetFillColor(BorderColor.CGColor);
			context.FillRect(new CGRect(xMin, yMin, BorderWidth.Left, fHeight));
		}

		public static void DrawRightBorder(CGContext context, CGRect rect, CGRect Frame, UIColor BorderColor, UIEdgeInsets BorderWidth)
		{

			var xMax = rect.GetMaxX();

			var yMin = rect.GetMinY();

			var fHeight = Frame.Size.Height;

			context.SetFillColor(BorderColor.CGColor);
			context.FillRect(new CGRect(xMax - BorderWidth.Right, yMin, BorderWidth.Right, fHeight));
		}

		public static void DrawBottomtBorder(CGContext context, CGRect rect, CGRect Frame, UIColor BorderColor, UIEdgeInsets BorderWidth)
		{
			var xMin = rect.GetMinX();
			var yMax = rect.GetMaxY();

			var fWidth = Frame.Size.Width;

			context.SetFillColor(BorderColor.CGColor);
			context.FillRect(new CGRect(xMin, yMax - BorderWidth.Bottom, fWidth, BorderWidth.Bottom));
		}

		public static void DrawTopBorder(CGContext context, CGRect rect, CGRect Frame, UIColor BorderColor, UIEdgeInsets BorderWidth)
		{
			var xMin = rect.GetMinX();

			var yMin = rect.GetMinY();

			var fWidth = Frame.Size.Width;

			context.SetFillColor(BorderColor.CGColor);
			context.FillRect(new CGRect(xMin, yMin, fWidth, BorderWidth.Top));
		}

        public static void DrawTopSeparator(CGContext context, CGRect rect, nfloat fWidth, UIColor BorderColor, UIEdgeInsets BorderWidth)
        {
            var xMin = 0f;

            var yMin = rect.GetMinY();

            context.SetFillColor(BorderColor.CGColor);
            context.FillRect(new CGRect(xMin, yMin, fWidth, BorderWidth.Top));
        }


    }
}
