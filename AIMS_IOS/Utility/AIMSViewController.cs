﻿using AIMS;
using AudioToolbox;
using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UIKit;

namespace AIMS_IOS
{
    public class AIMSViewController : UIViewController
    {
        private SystemSound beepSound;
        private LoadingOverlay loader;
        private object isLoadingLocker = new object();

        private bool isLoading = false;
        public bool IsLoading
        {
            get {
                lock (isLoadingLocker)
                {
                    return isLoading;
                }
            }
            private set
            {
                lock (isLoadingLocker)
                {
                    isLoading = value;
                }
            }
        }
        
        public bool IsShown { get; private set; } = false;

        public nfloat ScaleFactor { get; set; } = 1f;

        public AIMSViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            beepSound = new SystemSound(1054);//Beep

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                OverrideUserInterfaceStyle = UIUserInterfaceStyle.Light;


            loader = new LoadingOverlay(UIScreen.MainScreen.Bounds, AppDelegate.TextProvider.GetText(2001)/*"Loading.."*/);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            this.IsShown = true;
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            this.IsShown = false;
        }

        public void ShowLoading(string text)
        {
            IsLoading = true;
            InvokeOnMainThread(() =>
            {
                try
                {
                    if(loader == null)
                        loader = new LoadingOverlay(UIScreen.MainScreen.Bounds, AppDelegate.TextProvider.GetText(2001)/*"Loading.."*/);
                    loader.Frame = new CoreGraphics.CGRect(
                        UIScreen.MainScreen.Bounds.X,
                        UIScreen.MainScreen.Bounds.Y,
                        UIScreen.MainScreen.Bounds.Width,
                        UIScreen.MainScreen.Bounds.Height
                        );
                    loader.loadingLabel.Text = text;
                    this.View.Add(loader);
                }
                catch (Exception exx)
                {
                    var m = exx.Message;
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(exx);
                }
            });
        }

        public void HideLoading()
        {
            IsLoading = false;
            InvokeOnMainThread(() =>
            {
                try
                {
                    loader.RemoveFromSuperview();
                }
                catch (Exception exx)
                {
                    var m = exx.Message;
                    Microsoft.AppCenter.Crashes.Crashes.TrackError(exx);
                }
            });
        }
        
        public void AIMSMessage(string title, string text)
        {
            InvokeOnMainThread(() =>
            {
                new UIAlertView(title, text, null, "Ok", null).Show();
            });
        }

        public void AIMSError(string title, string text, EventHandler okCallback = null, int? timeOut = null)
        {
            InvokeOnMainThread(() =>
            {
                UIAlertView alert = new UIAlertView()
                {
                    Title = title,
                    Message = text
                };
                alert.AddButton("Ok");

                if (okCallback != null)
                    alert.Clicked += (ss, ee) =>
                    {
                        if (ee.ButtonIndex == 0)
                            okCallback.Invoke(this, EventArgs.Empty);
                    };

#if !DEBUG
                if (AppDelegate.CurrentDevice.Type == DeviceType.Phone)
                    beepSound.PlayAlertSoundAsync();//If it was iPhone
                else
                    beepSound.PlaySystemSoundAsync();//If it was iPad (no vibrate)
#endif

                alert.Show();
            });
        }

        public void AIMSErrorAsync(string title, string text, int? timeOut = null)
        {
            InvokeOnMainThread(() =>
            {
                UIAlertController alertController = UIAlertController.Create(title, text, UIAlertControllerStyle.Alert);
                alertController.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Cancel, null));
                this.PresentViewController(alertController, true, null);

#if !DEBUG
                if (AppDelegate.CurrentDevice.Type == DeviceType.Phone)
                    beepSound.PlayAlertSoundAsync();//If it was iPhone
                else
                    beepSound.PlaySystemSoundAsync();//If it was iPad (no vibrate)
#endif

                if (timeOut.HasValue)
                {
                    var closer = new Task(() => {
                        Task.Delay(timeOut.Value).Wait();
                        InvokeOnMainThread(() =>
                        {
                            alertController.RemoveFromParentViewController();
                            alertController.DismissViewController(true, null);
                        });
                    });
                    closer.Start();
                }
            });
        }

        public void AIMSConfirm(string title, string text, EventHandler OkCallback = null)
        {
            InvokeOnMainThread(() =>
            {
                UIAlertView alert = new UIAlertView()
                {
                    Title = title,
                    Message = text
                };
                alert.AddButton("Ok");
                alert.AddButton(AppDelegate.TextProvider.GetText(2004)/*"Cancel"*/);

                alert.Clicked += (ss, ee) =>
                {
                    if (OkCallback != null && ee.ButtonIndex == 0)
                        OkCallback.Invoke(this, EventArgs.Empty);
                };
                alert.Show();

            });
        }

        public void Question(string title, string text, EventHandler acceptCallback = null, EventHandler rejectCallback = null)
        {
            InvokeOnMainThread(async () =>
            {
                var result = await CustomQuestion(title, text, AppDelegate.TextProvider.GetText(2541), AppDelegate.TextProvider.GetText(2542)).ConfigureAwait(true);
                if(result == 1)
                    acceptCallback?.Invoke(this,new EventArgs());
                else rejectCallback?.Invoke(this, new EventArgs());
            });
        }

        public static Task<int> CustomQuestion(string title, string text, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = text
            };
            if(buttons != null)
                foreach (var button in buttons)
                    alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        #region keyboard from https://gist.github.com/redent/7263276

        /// <summary>
        /// Set this field to any view inside the textfield to center this view instead of the current responder
        /// </summary>
        protected UIView ViewToCenterOnKeyboardShown;
        protected UIScrollView ScrollToCenterOnKeyboardShown;

        /// <summary>
        /// Override point for subclasses, return true if you want to handle keyboard notifications
        /// to center the active responder in the scroll above the keyboard when it appears
        /// </summary>
        public virtual bool HandlesKeyboardNotifications()
        {
            return false;
        }

        NSObject _keyboardShowObserver;
        NSObject _keyboardHideObserver;
        protected virtual void RegisterForKeyboardNotifications()
        {
            if (_keyboardShowObserver == null)
                _keyboardShowObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, OnKeyboardNotification);
            if (_keyboardHideObserver == null)
                _keyboardHideObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, OnKeyboardNotification);
        }

        protected virtual void UnregisterForKeyboardNotifications()
        {
            if (_keyboardShowObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardShowObserver);
                _keyboardShowObserver.Dispose();
                _keyboardShowObserver = null;
            }

            if (_keyboardHideObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardHideObserver);
                _keyboardHideObserver.Dispose();
                _keyboardHideObserver = null;
            }
        }

        /// <summary>
        /// Gets the UIView that represents the "active" user input control (e.g. textfield, or button under a text field)
        /// </summary>
        /// <returns>
        /// A <see cref="UIView"/>
        /// </returns>
        protected virtual UIView KeyboardGetActiveView()
        {
            return View.FindFirstResponder();
        }

        private void OnKeyboardNotification(NSNotification notification)
        {
            if (!IsViewLoaded) return;

            //Check if the keyboard is becoming visible
            var visible = notification.Name == UIKeyboard.WillShowNotification;

            //Start an animation, using values from the keyboard
            UIView.BeginAnimations("AnimateForKeyboard");
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(UIKeyboard.AnimationDurationFromNotification(notification));
            UIView.SetAnimationCurve((UIViewAnimationCurve)UIKeyboard.AnimationCurveFromNotification(notification));

            //Pass the notification, calculating keyboard height, etc.
            var keyboardFrame = visible
                ? UIKeyboard.FrameEndFromNotification(notification)
                : UIKeyboard.FrameBeginFromNotification(notification);

            OnKeyboardChanged(visible, keyboardFrame);

            //Commit the animation
            UIView.CommitAnimations();
        }

        /// <summary>
        /// Override this method to apply custom logic when the keyboard is shown/hidden
        /// </summary>
        /// <param name='visible'>
        /// If the keyboard is visible
        /// </param>
        /// <param name='keyboardFrame'>
        /// Frame of the keyboard
        /// </param>
        protected virtual void OnKeyboardChanged(bool visible, CGRect keyboardFrame)
        {
            var activeView = ViewToCenterOnKeyboardShown ?? KeyboardGetActiveView();
            if (activeView == null)
                return;

            var scrollView = ScrollToCenterOnKeyboardShown ??
                activeView.FindTopSuperviewOfType(View, typeof(UIScrollView)) as UIScrollView;
            if (scrollView == null)
                return;

            if (!visible)
                scrollView.RestoreScrollPosition();
            else
                scrollView.CenterView(activeView, keyboardFrame);
        }

        /// <summary>
        /// Call it to force dismiss keyboard when background is tapped
        /// </summary>
        protected void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard
            var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
            tap.AddTarget(() => View.EndEditing(true));
            tap.ShouldReceiveTouch = (recognizer, touch) =>
                !(touch.View is UIControl || touch.View.FindSuperviewOfType(View, typeof(UITableViewCell)) != null);
            View.AddGestureRecognizer(tap);
        }

        #endregion
    }

    public static class ViewExtensions
    {
        /// <summary>
        /// Find the first responder in the <paramref name="view"/>'s subview hierarchy
        /// </summary>
        /// <param name="view">
        /// A <see cref="UIView"/>
        /// </param>
        /// <returns>
        /// A <see cref="UIView"/> that is the first responder or null if there is no first responder
        /// </returns>
        public static UIView FindFirstResponder(this UIView view)
        {
            if (view.IsFirstResponder)
            {
                return view;
            }
            foreach (UIView subView in view.Subviews)
            {
                var firstResponder = subView.FindFirstResponder();
                if (firstResponder != null)
                    return firstResponder;
            }
            return null;
        }

        /// <summary>
        /// Find the first Superview of the specified type (or descendant of)
        /// </summary>
        /// <param name="view">
        /// A <see cref="UIView"/>
        /// </param>
        /// <param name="stopAt">
        /// A <see cref="UIView"/> that indicates where to stop looking up the superview hierarchy
        /// </param>
        /// <param name="type">
        /// A <see cref="Type"/> to look for, this should be a UIView or descendant type
        /// </param>
        /// <returns>
        /// A <see cref="UIView"/> if it is found, otherwise null
        /// </returns>
        public static UIView FindSuperviewOfType(this UIView view, UIView stopAt, Type type)
        {
            if (view.Superview != null)
            {
                if (type.IsInstanceOfType(view.Superview))
                {
                    return view.Superview;
                }

                if (view.Superview != stopAt)
                    return view.Superview.FindSuperviewOfType(stopAt, type);
            }

            return null;
        }

        public static UIView FindTopSuperviewOfType(this UIView view, UIView stopAt, Type type)
        {
            var superview = view.FindSuperviewOfType(stopAt, type);
            var topSuperView = superview;
            while (superview != null && superview != stopAt)
            {
                superview = superview.FindSuperviewOfType(stopAt, type);
                if (superview != null)
                    topSuperView = superview;

            }
            return topSuperView;
        }

        public static UIMotionEffect SetParallaxIntensity(this UIView view, float parallaxDepth, float? verticalDepth = null)
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
            {
                float vertical = verticalDepth ?? parallaxDepth;

                var verticalMotionEffect = new UIInterpolatingMotionEffect("center.y", UIInterpolatingMotionEffectType.TiltAlongVerticalAxis);
                verticalMotionEffect.MinimumRelativeValue = new NSNumber(-vertical);
                verticalMotionEffect.MaximumRelativeValue = new NSNumber(vertical);

                var horizontalMotionEffect = new UIInterpolatingMotionEffect("center.x", UIInterpolatingMotionEffectType.TiltAlongHorizontalAxis);
                horizontalMotionEffect.MinimumRelativeValue = new NSNumber(-parallaxDepth);
                horizontalMotionEffect.MaximumRelativeValue = new NSNumber(parallaxDepth);

                var group = new UIMotionEffectGroup();
                group.MotionEffects = new UIMotionEffect[] { horizontalMotionEffect, verticalMotionEffect };

                view.AddMotionEffect(group);

                return group;
            }
            return null;
        }
    }

    public static class ScrollExtensions
    {
        public static void CenterView(this UIScrollView scrollView, UIView viewToCenter, CGRect keyboardFrame, bool animated = false)
        {
            var scrollFrame = scrollView.Frame;

            var adjustedFrame = UIApplication.SharedApplication.KeyWindow.ConvertRectFromView(scrollFrame, scrollView.Superview);

            var intersect = CGRect.Intersect(adjustedFrame, keyboardFrame);

            var height = intersect.Height;
            if (!UIDevice.CurrentDevice.CheckSystemVersion(8, 0) && IsLandscape())
            {
                height = intersect.Width;
            }
            scrollView.CenterView(viewToCenter, height, animated: animated);
        }

        public static void CenterView(this UIScrollView scrollView, UIView viewToCenter, nfloat keyboardHeight = default(nfloat), bool adjustContentInsets = true, bool animated = false)
        {
            if (adjustContentInsets)
            {
                var contentInsets = new UIEdgeInsets(0.0f, 0.0f, keyboardHeight, 0.0f);
                scrollView.ContentInset = contentInsets;
                scrollView.ScrollIndicatorInsets = contentInsets;
            }

            // Position of the active field relative isnside the scroll view
            CGRect relativeFrame = viewToCenter.Superview.ConvertRectToView(viewToCenter.Frame, scrollView);

            var spaceAboveKeyboard = scrollView.Frame.Height - keyboardHeight;

            // Move the active field to the center of the available space
            var offset = relativeFrame.Y - (spaceAboveKeyboard - viewToCenter.Frame.Height) / 2;
            if (scrollView.ContentOffset.Y < offset)
            {
                scrollView.SetContentOffset(new CGPoint(0, offset), animated);
            }
        }

        public static void RestoreScrollPosition(this UIScrollView scrollView)
        {
            scrollView.ContentInset = UIEdgeInsets.Zero;
            scrollView.ScrollIndicatorInsets = UIEdgeInsets.Zero;
        }

        public static bool IsLandscape()
        {
            var orientation = UIApplication.SharedApplication.StatusBarOrientation;
            bool landscape = orientation == UIInterfaceOrientation.LandscapeLeft
                        || orientation == UIInterfaceOrientation.LandscapeRight;
            return landscape;
        }
    }
}
