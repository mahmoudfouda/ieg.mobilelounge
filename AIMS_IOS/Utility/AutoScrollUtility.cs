﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public static class AutoScrollUtility
    {
        public static UIView findFirstResponder(UIView view)
        {
            if(view.IsFirstResponder)
            {
                return view;
            }

            foreach(UIView subview in view.Subviews)
            {
                UIView firstResponder = findFirstResponder(subview);
                if(firstResponder != null)
                {
                    return firstResponder;
                }
            }

            return null;

        }

        public static void autoScrollTo(UIViewController vc, UIScrollView scrollView, nfloat objectY, nfloat keyBoardHeight)
        {
            //visible height
            nfloat viewH = UIScreen.MainScreen.Bounds.Height - keyBoardHeight;
            //absolute position of element inside the scroll view
            nfloat yAbsolutePos =  scrollView.Frame.Y + objectY;

            //y content offset
            nfloat yscroll = scrollView.Frame.Y + objectY - viewH;

            if(yAbsolutePos - scrollView.ContentOffset.Y < viewH)
            {
                return;
            }

            if (yscroll > scrollView.ContentSize.Height)
            {
                //scrollView.ContentSize = new CGSize(scrollView.ContentSize.Width, yscroll);
            }

            scrollView.SetContentOffset(new CGPoint(0f, yscroll), false);

        }

        //public static void keyBoardWillHide(UIViewController vc, UIScrollView scrollView, )

    }
}
