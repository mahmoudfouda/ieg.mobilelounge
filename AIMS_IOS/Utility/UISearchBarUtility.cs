﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace AIMS_IOS
{
    public static class UISearchBarUtility
    {
        public static void removeBackground(UISearchBar bar)
        {
            UIImage background = UIImage.FromFile("alpha");

            bar.SetBackgroundImage(background, UIBarPosition.Any, UIBarMetrics.Default);
            bar.ScopeBarBackgroundImage = background;


        }




    }
}
