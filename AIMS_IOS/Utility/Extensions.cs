﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UIKit;

namespace AIMS_IOS.Utility
{
    public static class Extensions
    {

        public static byte[] ToNSData(this UIImage image)
        {

            if (image == null)
            {
                return null;
            }
            NSData data = null;

            try
            {
                data = image.AsPNG();
                return data.ToArray();
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (image != null)
                {
                    image.Dispose();
                    image = null;
                }
                if (data != null)
                {
                    data.Dispose();
                    data = null;
                }
            }
        }

        public static UIImage ToImage(this byte[] data)
        {
            if (data == null || data.Length == 0)
            {
                return null;
            }
            UIImage image = null;
            try
            {

                image = new UIImage(NSData.FromArray(data));
                data = null;
            }
            catch (Exception)
            {
                return null;
            }
            return image;
        }

        public static bool IsFlightNumber(this string text)
        {
            if (string.IsNullOrWhiteSpace(text)) return false;

            var regEx = new Regex(@"^(\d{1,4}[A-Za-z]?)$", RegexOptions.IgnoreCase);

            var m = regEx.Match(text);
            return m.Success && m.Index == 0;
            //return regEx.IsMatch(text,0);
        }

        public static UIColor FromHexString(this UIColor color, string hexValue, float alpha = 1.0f)
        {
            var colorString = hexValue.Replace("#", "");
            if (alpha > 1.0f)
            {
                alpha = 1.0f;
            }
            else if (alpha < 0.0f)
            {
                alpha = 0.0f;
            }

            float red, green, blue;

            switch (colorString.Length)
            {
                case 3: // #RGB
                    {
                        red = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(0, 1)), 16) / 255f;
                        green = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(1, 1)), 16) / 255f;
                        blue = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(2, 1)), 16) / 255f;
                        return UIColor.FromRGBA(red, green, blue, alpha);
                    }
                case 6: // #RRGGBB
                    {
                        red = Convert.ToInt32(colorString.Substring(0, 2), 16) / 255f;
                        green = Convert.ToInt32(colorString.Substring(2, 2), 16) / 255f;
                        blue = Convert.ToInt32(colorString.Substring(4, 2), 16) / 255f;
                        return UIColor.FromRGBA(red, green, blue, alpha);
                    }

                default:
                    throw new ArgumentOutOfRangeException(string.Format("Invalid color value {0} is invalid. It should be a hex value of the form #RBG, #RRGGBB", hexValue));

            }
        }

        public static void MoveFrame(this UIView view, nfloat x, nfloat y)
        {
            var frameCopy = view.Frame;
            frameCopy.X = x;
            frameCopy.Y = y;
            view.Frame = frameCopy;
        }

        public static void ResizeFrame(this UIView view, nfloat width, nfloat height)
        {
            var frameCopy = view.Frame;
            frameCopy.Width = width;
            frameCopy.Height = height;
            view.Frame = frameCopy;
        }

        public static void ResizeFrame(this UIView view, CGSize size)
        {
            var frameCopy = view.Frame;
            frameCopy.Width = size.Width;
            frameCopy.Height = size.Height;
            view.Frame = frameCopy;
        }

        public static void ResizeWidth(this UIView view, nfloat width)
        {
            var frameCopy = view.Frame;
            frameCopy.Width = width;
            view.Frame = frameCopy;
        }

        public static void ResizeHeight(this UIView view, nfloat height)
        {
            var frameCopy = view.Frame;
            frameCopy.Height = height;
            view.Frame = frameCopy;
        }

        public static void InflateX(this UIView view, nfloat x)
        {
            var frameCopy = view.Frame;
            frameCopy.X += x;
            view.Frame = frameCopy;
        }

        public static void InflateY(this UIView view, nfloat y)
        {
            var frameCopy = view.Frame;
            frameCopy.Y += y;
            view.Frame = frameCopy;
        }

        public static void InflateHeight(this UIView view, nfloat height)
        {
            var frameCopy = view.Frame;
            frameCopy.Height += height;
            view.Frame = frameCopy;
        }

        public static void InflateWidth(this UIView view, nfloat width)
        {
            var frameCopy = view.Frame;
            frameCopy.Width += width;
            view.Frame = frameCopy;
        }

        public static void MoveAndResizeFrame(this UIView view, nfloat x, nfloat y, nfloat width, nfloat height)
        {
            var frameCopy = view.Frame;
            frameCopy.X = x;
            frameCopy.Y = y;
            frameCopy.Width = width;
            frameCopy.Height = height;
            view.Frame = frameCopy;
        }

        public static void CenterOnView(this UIView view, UIView parentView)
        {
            var frame = view.Frame;
            frame.X = (parentView.Frame.Width / 2) - (view.Frame.Width / 2);
            frame.Y = (parentView.Frame.Height / 2) - (view.Frame.Height / 2);
            view.Frame = frame;
        }

        public static void CenterFrameX(this UIView view, nfloat x)
        {
            view.Center = new CGPoint(x, view.Center.Y);
        }

        public static void CenterFrameInParentX(this UIView view)
        {
            CenterFrameInParentX(view, view.Superview.Frame.Width);
        }

        public static void CenterFrameInParentX(this UIView view, nfloat width)
        {
            var frame = view.Frame;
            frame.X = (width / 2) - (view.Frame.Width / 2);
            view.Frame = frame;
        }

        public static void CenterFrameY(this UIView view, nfloat y)
        {
            view.Center = new CGPoint(view.Center.X, y);
        }

        public static void CenterFrameInParentY(this UIView view)
        {
            CenterFrameInParentY(view, view.Superview.Frame.Height);
        }

        public static void CenterFrameInParentY(this UIView view, nfloat height)
        {
            var frame = view.Frame;
            frame.Y = (height / 2) - (view.Frame.Height / 2);
            view.Frame = frame;
        }

        public static void CenterFrameInParent(this UIView view)
        {
            CenterFrameInParent(view, view.Superview.Frame.Width, view.Superview.Frame.Height);
        }

        public static void CenterFrameInParent(this UIView view, nfloat width, nfloat height)
        {
            var frame = view.Frame;
            frame.X = (width / 2) - (view.Frame.Width / 2);
            frame.Y = (height / 2) - (view.Frame.Height / 2);
            view.Frame = frame;
        }
    }
}
