﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using CoreGraphics;

namespace AIMS_IOS
{
    public static class UIViewTransition
    {

        public static void displayFromRightToLeftLinear(UIViewController fromController, UIViewController toController, nfloat speed)
        {
            UIView fromView = fromController.View;
            UIView toView = toController.View;
            // Get the size of the view area.
            var viewSize = new CGRect(fromView.Frame.X, fromView.Frame.Y,
                fromView.Frame.Width, fromView.Frame.Height);

            // Add the to view to the tab bar view.
            fromView.Superview.AddSubview(toView);

            var minAlpha = -fromController.View.Bounds.Width;
            var maxAlpha = fromController.View.Bounds.Width;
            var midAlpha = 0.0;
            // Position it off screen.
            toView.Frame = new CGRect(maxAlpha, viewSize.Y, maxAlpha, viewSize.Height);
            UIView.Animate(speed, .01, UIViewAnimationOptions.CurveEaseOut,
                () =>
                {
                    fromView.Frame = new CGRect(minAlpha, fromView.Frame.Y, maxAlpha,
                        fromView.Frame.Height);
                    toView.Frame = new CGRect(midAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                    toView.UpdateFocusIfNeeded();
                },
                () =>
                {
                    fromView.RemoveFromSuperview();

                    
                }
            );
        }

        public static void displayFromLeftToRightLinear(UIViewController fromController, UIViewController toController, nfloat speed)
        {
            UIView fromView = fromController.View;
            UIView toView = toController.View;
            // Get the size of the view area.

            // Get the size of the view area.
            var viewSize = fromView.Frame;

            // Add the to view to the tab bar view.
            fromView.Superview.AddSubview(toView);

            var minAlpha = -fromController.View.Bounds.Width;
            var maxAlpha = fromController.View.Bounds.Width;
            var midAlpha = 0.0;
            // Position it off screen.
            toView.Frame = new CGRect(minAlpha, viewSize.Y, maxAlpha, viewSize.Height);
            UIView.Animate(speed, .01, UIViewAnimationOptions.CurveEaseOut,
                () =>
                {
                    fromView.Frame = new CGRect(maxAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                    toView.Frame = new CGRect(midAlpha, viewSize.Y, maxAlpha, viewSize.Height);
                },
                () =>
                {
                    fromView.RemoveFromSuperview();
                }

            );
        }

        public static void displayFromTopToBottomLinear(UIViewController fromController, UIViewController toController, nfloat speed)
        {
            UIView fromView = fromController.View;
            UIView toView = toController.View;
            // Get the size of the view area.

            // Get the size of the view area.
            var viewSize = fromView.Frame;

            // Add the to view to the tab bar view.
            fromView.Superview.AddSubview(toView);

            var minAlpha = -fromController.View.Bounds.Height;
            var maxAlpha = fromController.View.Bounds.Height;
            //var midAlpha = 0.0;
            // Position it off screen.
            toView.Frame = new CGRect(0f, minAlpha, viewSize.Width, viewSize.Height);
            UIView.Animate(speed, .01, UIViewAnimationOptions.CurveEaseOut,
                () =>
                {
                    fromView.Frame = new CGRect(0f, maxAlpha, viewSize.Width, viewSize.Height);
                    toView.Frame = new CGRect(0f, 0f, viewSize.Width, viewSize.Height);
                },
                () =>
                {
                    fromView.RemoveFromSuperview();
                    //fromController.PresentViewController(toController, false, null);
                }

            );

        }


    }
}
