using Foundation;
using System;
using UIKit;
using CoreGraphics;

using System.Collections.Generic;

namespace AIMS_IOS
{
	public class UIDropDownView : UIView
	{
		public List<UIView> views;
		private List<UIButton> buttons;
		private List<UIImageView> images;
        private List<UILabel> labels;

		private UIView viewController;

		public int selected = 0;
		public string selectedName;

		public bool activated = false;

		public delegate void eventDelegate(object sender, EventArgs e);
		public eventDelegate ItemSelected;
		public eventDelegate Open;

		//private int count;

		//private CGRect buttonsFrame;
		//private CGRect imagesFrame;

		public List<DropDownCell> data;
		public UIColor backgroundColor;
		public float heightFrame = 25f;

		public float imageWidth = 22f;
		public float imageHeight = 18f;
		public float imageXOffset = 6f;
		public float imageYOffset = 6f;

		public UIButton outsideButton;

        public UIFont font;

        public CGRect frame
        {
            get { return this.Frame; }
        }

		public UIDropDownView() : base()
		{
		}

		public void init(List<DropDownCell> dt, float _heightFrame, int defaultSelection, float width, UIView parent)
		{
			viewController = parent;
            this.Layer.MasksToBounds = true;
            this.Layer.CornerRadius = 5f;

			selected = defaultSelection;
			heightFrame = _heightFrame;
			data = dt;

			outsideButton = new UIButton(new CGRect(0f,0f,0f,0f));
			outsideButton.BackgroundColor = UIColor.Clear;
			outsideButton.SetTitle("", UIControlState.Normal);
			outsideButton.TouchDown += OnClickOutside;

            viewController.Add(outsideButton);
            //this.AddSubview(outsideButton);

            this.Frame = new CGRect(this.Frame.X, this.Frame.Y, width,
				heightFrame);

			this.BackgroundColor = UIColor.Clear;

			views = new List<UIView>();
			buttons = new List<UIButton>();
			images = new List<UIImageView>();
            labels = new List<UILabel>();

			for (int i = 0; i < data.Count; i++)
			{
				//view setting
				views.Add(new UIView(new CGRect(0f, i * heightFrame, this.Frame.Width, heightFrame)));
				//button settings
				buttons.Add(new UIButton(new CGRect(0f, 0f,
					this.Frame.Width,
					heightFrame)));
				//button text
                //border
                buttons[i].Layer.MasksToBounds = true;
                //buttons[i].Layer.CornerRadius = 3f;
                buttons[i].Layer.BorderColor = new CGColor(0f, 0f, 0f, 1f);
                buttons[i].Layer.BorderWidth = 1f;
                //background color
                buttons[i].BackgroundColor = UIColor.LightGray;
				buttons[i].TouchUpInside += OnClick;

				//image settings
				images.Add(new UIImageView(new CGRect(imageXOffset, imageYOffset, imageWidth, imageHeight)));
				images[i].Image = data[i].image;

                labels.Add(new UILabel(new CGRect(images[i].Frame.X + images[i].Frame.Width + 7f,
                    7f,
                    buttons[i].Frame.Width - (images[i].Frame.X + images[i].Frame.Width + 4f),
                    images[i].Frame.Height + 2)));
                labels[i].Text = data[i].label;

                if(font != null)
                {
                    labels[i].Font = font;
                }

				//todo image aspect fit

				views[i].AddSubviews(buttons[i], images[images.Count - 1], labels[i]);

				if (i != selected)
				{
					views[i].Hidden = true;
				}

				this.AddSubview(views[i]);
			}

			swapCells(0, selected);

            

            //this.AddSubviews(views);

        }

		public void updateFramePosition(nfloat x, nfloat y, nfloat width, nfloat height, CGPoint imgSize)
		{
            this.heightFrame = (float)height;

			if(activated)
            {
                this.Frame = new CGRect(x, y, width, heightFrame * views.Count);
            }
            else
            {
                this.Frame = new CGRect(x, y, width, heightFrame);
            }
            

            for(int i = 0; i < selected; i++)
            {
                views[i].Frame = new CGRect(0f, heightFrame * i + heightFrame, width, heightFrame);

                buttons[i].Frame = new CGRect(0f, 0f, views[i].Frame.Width, views[i].Frame.Height);

                images[i].Frame = new CGRect(imageXOffset, imageYOffset, imgSize.X, imgSize.Y);

                labels[i].Frame = new CGRect(images[i].Frame.Right + 5f, 5f,
                    views[i].Frame.Width - images[i].Frame.Right - 10f, images[i].Frame.Height);
            }

            views[selected].Frame = new CGRect(0f, 0f, width, heightFrame);

            buttons[selected].Frame = new CGRect(0f, 0f, views[selected].Frame.Width, views[selected].Frame.Height);

            images[selected].Frame = new CGRect(imageXOffset, imageYOffset, imgSize.X, imgSize.Y);

            labels[selected].Frame = new CGRect(images[selected].Frame.Right + 5f, 5f,
                views[selected].Frame.Width - images[selected].Frame.Right - 10f, images[selected].Frame.Height);

            for (int i = selected + 1; i < views.Count; i++)
            {

                views[i].Frame = new CGRect(0f, heightFrame * i, width, heightFrame);

                buttons[i].Frame = new CGRect(0f, 0f, views[i].Frame.Width, views[i].Frame.Height);

                images[i].Frame = new CGRect(imageXOffset, imageYOffset, imgSize.X, imgSize.Y);

                labels[i].Frame = new CGRect(images[i].Frame.Right + 5f, 5f,
                    views[i].Frame.Width - images[i].Frame.Right - 10f, images[i].Frame.Height);
            }


            updateOutsideButton();
		}

		private void updateOutsideButton()
		{ 
		
			if (activated)
			{
				outsideButton.Frame = new CGRect(0f, 0f, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height);
			}
			else
			{
				outsideButton.Frame = new CGRect(0f, 0f, 0f, 0f);
			}
		}

		public void OnClick(object sender, EventArgs e)
		{
			if (activated)
			{
				UIButton temp = (UIButton)sender;
				int tempSelected = -1;

				for (int i = 0; i < buttons.Count; i++)
				{
					if (temp == buttons[i])
					{
						selectedName = labels[i].Text;
						tempSelected = i;
					}
				}

				if(tempSelected != selected)
					swapCells(selected, tempSelected);
				
				selected = tempSelected;

				for (int i = 0; i < views.Count; i++)
				{
					if (i != selected)
						views[i].Hidden = true;

				}
				activated = false;

				this.Frame = new CGRect(this.Frame.X, this.Frame.Y, this.Frame.Width, heightFrame);

				OnItemSelected(EventArgs.Empty);
			}
			else
			{
				displayChoices();

				OnOpen(EventArgs.Empty);

			}

			updateOutsideButton();
		}

		public void OnClickOutside(object sender, EventArgs e) 
		{
			if (activated)
			{
				OnClick(buttons[selected], EventArgs.Empty);

			}
		}


		protected virtual void OnItemSelected(EventArgs e)
		{
			if (ItemSelected != null)
				ItemSelected(this, e);

		}

		protected virtual void OnOpen(EventArgs e)
		{
			if (Open != null)
				Open(this, e);
		}

		public void displayChoices()
		{
			activated = true;

			for (int i = 0; i < views.Count; i++)
			{
				views[i].Hidden = false;

			}
			this.Frame = new CGRect(this.Frame.X, this.Frame.Y, this.Frame.Width, heightFrame * views.Count);
		}

		public void hideChoices()
		{
			for (int i = 0; i < views.Count; i++)
			{
				if (i != selected)
					views[i].Hidden = true;

			}
			this.Frame = new CGRect(this.Frame.X, this.Frame.Y, this.Frame.Width, heightFrame);
		}

		public void setSelection(int index)
		{


		}


		private void swapCells(int it1, int it2)
		{
			CGRect r = new CGRect(views[it1].Frame.X, views[it1].Frame.Y,
				views[it1].Frame.Width, views[it1].Frame.Height);

			views[it1].Frame = new CGRect(views[it2].Frame.X, views[it2].Frame.Y,
				views[it2].Frame.Width, views[it2].Frame.Height);

			views[it2].Frame = r;
		}
	}

	public class DropDownCell
	{
		public UIImage image;
		public string label;

		public DropDownCell(UIImage image, string label)
		{
			this.image = image;
			this.label = label;
		}
	}
}