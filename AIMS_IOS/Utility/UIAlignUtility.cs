﻿using System;
using CoreGraphics;

namespace AIMS_IOS
{
	public static class UIAlignUtility
	{

		public static void AlignToCenter(ref CGRect leftItem,ref CGRect rightItem, nfloat areaWidth, nfloat height, nfloat borderOffset, nfloat topOffset, nfloat middleOffset)
		{
			nfloat width = (areaWidth - (borderOffset * 2) - middleOffset)/2f;
			leftItem = new CGRect(borderOffset, topOffset, width, height);

			rightItem = new CGRect(leftItem.X + leftItem.Width + middleOffset, topOffset, width, height);

		}

	}
}
