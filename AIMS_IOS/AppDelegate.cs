﻿using AIMS;
using Foundation;
using System.Threading.Tasks;
using UIKit;
using System;
using AIMS_IOS.Adapters;
using AVFoundation;
using CoreLocation;
using Security;
using AIMS.Models;
using Ieg.Mobile.Localization;
using ZXing.Mobile;
using System.Linq;
using System.Collections.Generic;
using InfineaSDK.iOS;
using System.Net.Http;

namespace AIMS_IOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        #region MobileLounge application context
        private static readonly DefaultMobileApplication UnderlyingApplication = DefaultMobileApplication.Create();
        public static DefaultMobileApplication AIMSApplication { get { return UnderlyingApplication; } }

        public static AppDelegate Current { get; private set; }

        public event EventHandler<ConnectionStateEventArgs> OnExternalScannerStatusChanged;
        #endregion

        #region ViewControllers
        public static UIViewController LoginView { get; set; }
        public static AirLineViewController AirlinesView { get; set; }
        public static ZXingScannerViewController ZxingScannerView { get; set; }
        public static UIViewController LoungeSelectionView { get; set; }
        public static ManualPassengerTabController ManualPassengerView { get; set; }
        public static SinglePageMIViewController SinglePageMIView { get; set; }
        public static SinglePageMIViewController2 SinglePageMIView2 { get; set; }
        public static ValidationResultViewController2 ValidationView { get; set; }
        public static ValidationResultViewController2 GuestValidationResultView { get; set; }

        public static ChartViewController ChartViewController { get; set; }
        public static MapViewController MapViewController { get; set; }
        

        public static GuestsViewController GuestServicesView { get; set; }
        #endregion
        // class-level declarations

        public void AIMSMessage(string title, string text)
        {
            try
            {
                InvokeOnMainThread(() =>
                {
                    new UIAlertView(title, text, null, "OK", null).Show();
                });
            }
            catch (Exception ex)
            {
                var m = ex.Message;
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }
        }

        public void AIMSConfirm(string title, string text, EventHandler OkCallback = null)
        {
            try
            {
                InvokeOnMainThread(() =>
                {
                    UIAlertView alert = new UIAlertView()
                    {
                        Title = title,
                        Message = text
                    };
                    alert.AddButton("Ok");
                    alert.AddButton(AppDelegate.TextProvider.GetText(2004)/*"Cancel"*/);

                    alert.Clicked += (ss, ee) =>
                    {
                        if (OkCallback != null && ee.ButtonIndex == 0)
                            OkCallback.Invoke(this, EventArgs.Empty);
                    };
                    alert.Show();

                });
            }
            catch (Exception ex)
            {
                var m = ex.Message;
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);
            }
        }

        public AppDelegate() : base()
        {
            UnderlyingApplication.ApplicationInit += OnApplicationStart;

            LogsRepository.OnSavedLog += OnSavedLogRepository;
            LogsRepository.OnTrackEvents += LogsRepository_OnTrackEvents;

            DefaultMobileApplication.OnValidationFailed += (sender, code) =>
            {
                //Siavash changes
                AIMSMessage("", TextProvider.GetText((int)code));
                InvokeOnMainThread(() =>
                {
                    ReturnToLoginView();
                });

                //UIStoryboard storyBoard = this.Window.RootViewController.Storyboard;

                //UIViewController firstView = storyBoard.InstantiateViewController("LoginViewController");

                //this.Window.RootViewController = firstView;
            };
            Current = this;
        }

        private void LogsRepository_OnTrackEvents(Dictionary<string, Dictionary<string, string>> log)
        {
            try
            {
                if (log == null)
                    return;

                var key = log.Keys.ToList().First();
                var value = log.Values.ToList().First();

                if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedLounge != null)
                    key = MembershipProvider.Current.SelectedLounge.LoungeName + " - " + key;

                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(key, value);
            }
            catch (Exception ef)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ef);
            }
        }

        private void OnSavedLogRepository(object model)
        {
            try
            {
                Log log = (Log)model;

                if (log != null)
                {
                    if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedLounge != null)
                        log.Title += " - " + MembershipProvider.Current.SelectedLounge.LoungeName;

                    if (log.LogType == LogType.Info || log.LogType == LogType.Warning)
                    {
                        Microsoft.AppCenter.Analytics.Analytics.TrackEvent(log.LogType.ToString(), new Dictionary<string, string>
                    {
                        { log.Title, string.IsNullOrEmpty(log.Description) ? log.Title :  log.Description.Replace("\t"," ").Replace("\n", " ") }
                    });
                    }
                    else
                    {
                        Microsoft.AppCenter.Crashes.Crashes.TrackError(new Exception(log.Description), new Dictionary<string, string>
                        {
                            { log.LogType.ToString(),  log.Title }
                        });
                    }

                    //Microsoft.AppCenter.Crashes.Crashes.TrackError(new Exception(log.LogType.ToString()), new Dictionary<string, string>
                    //{
                    //    { log.Title, string.IsNullOrEmpty(log.Description) ? log.Title :  log.Description.Replace("\t"," ").Replace("\n", " ") }
                    //});
                }
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex, new Dictionary<string, string> {
                        { "FunctionName", "AIMSApp.OnSavedLogRepository" }});
            }
        }

        public static MobileDevice CurrentDevice { get; set; }
        //get { return UnderlyingApplication.CurrentDeviceType; }
        //}

        public static CameraType? ActiveCamera
        {
            get { return UnderlyingApplication.ActiveCamera; }
            set { UnderlyingApplication.ActiveCamera = value; }
        }

        public static int SessionTimeoutMinutes
        {
            get { return DefaultMobileApplication.SessionTimeoutMinutes; }
            set { DefaultMobileApplication.SessionTimeoutMinutes = value; }
        }

        public static int HoursOccupancyHistory
        {
            get { return UnderlyingApplication.HoursOccupancyHistory; }
            set { UnderlyingApplication.HoursOccupancyHistory = value; }
        }

        public static int RefreshIntervalMinutes
        {
            get { return UnderlyingApplication.RefreshIntervalMinutes; }
            set { UnderlyingApplication.RefreshIntervalMinutes = value; }
        }

        public IPCDTDevices LineaDevice { get; set; } = IPCDTDevices.Instance;

        public IPCDTDeviceDelegateEvents PeripheralEvents { get; set; } = new IPCDTDeviceDelegateEvents();


        public MobileBarcodeScanner MainZxingScanner { get; set; }

        public MobileBarcodeScanner GuestZxingScanner { get; set; }

        public MobileBarcodeScanningOptions ZxingOptions { get; set; }

        public static bool GuestScanning
        {
            get;
            set;
        }

        public static bool ContinueScanning
        {
            get { return UnderlyingApplication.ContinueScanning; }
            set { UnderlyingApplication.ContinueScanning = value; }
        }

        public static bool IsScanningCancelled
        {
            get;
            set;
        } = false;

        public static bool IsDeveloperModeEnabled
        {
            get { return UnderlyingApplication.IsDeveloperModeEnabled; }
            set { UnderlyingApplication.IsDeveloperModeEnabled = value; }
        }

        public static string UniqueID
        {
            get
            {
                var query = new SecRecord(SecKind.GenericPassword);
                query.Service = NSBundle.MainBundle.BundleIdentifier;
                query.Account = "IEG_UniqueID";

                NSData uniqueId = SecKeyChain.QueryAsData(query);
                if (uniqueId == null)
                {
                    query.ValueData = NSData.FromString(Guid.NewGuid().ToString());
                    var err = SecKeyChain.Add(query);
                    if (err != SecStatusCode.Success && err != SecStatusCode.DuplicateItem)
                        throw new Exception("Cannot store Unique ID");

                    return query.ValueData.ToString();
                }
                else
                {
                    return uniqueId.ToString();
                }
            }
        }

        public static LanguageRepository TextProvider
        {
            get { return UnderlyingApplication.TextProvider; }
        }

        private static List<BarcodeScannerSerialNumber> iOSSerialNumbers = null;
        public static List<BarcodeScannerSerialNumber> BarcodeScannerSerialNembers
        {
            get
            {
                if (iOSSerialNumbers == null)
                {
                    iOSSerialNumbers = MembershipProvider.Current.BarcodeScannerAPIKeys.Where(x => x.OS == PlatformType.iOS).ToList();
                }
                return iOSSerialNumbers;
            }
        }

        public override UIWindow Window
        {
            get;
            set;
        }
        public static bool IsMWInitiated { get; set; } = false;

        private static NSTimer OccupancyTimer { get; set; }

        public static event EventHandler OnOccupancyUpdateTick;

        public static void ClearAllListenersOnOccupancyUpdateTick()
        {
            if (OnOccupancyUpdateTick != null)
            {
                foreach (Delegate eventDelegate in OnOccupancyUpdateTick.GetInvocationList())
                {
                    OnOccupancyUpdateTick -= (EventHandler)OnOccupancyUpdateTick;
                }
            }
        }

        public static void UpdateOccupancyTimer()
        {
            if (OccupancyTimer != null)
            {
                OccupancyTimer.Invalidate();
                OccupancyTimer.Dispose();
                OccupancyTimer = null;
            }

            OccupancyTimer = NSTimer.CreateRepeatingTimer(AppDelegate.RefreshIntervalMinutes * 60, (_) =>
            {
                try
                {
                    var selectedLounge = MembershipProvider.Current.SelectedLounge;

                    AppManager.GetLoungeOccupancy(selectedLounge, (data) =>
                    {
                        if (data == null)
                            return;

                        MembershipProvider.Current.SelectedLounge.LastPercentage = (int)Math.Ceiling(((LoungeOccupancy)data).Percentage);

                        if (OnOccupancyUpdateTick != null)
                            OnOccupancyUpdateTick.Invoke(null, null);
                    });
                }
                catch
                {
                }
            });

            NSRunLoop.Current.AddTimer(OccupancyTimer, NSRunLoopMode.Default);
        }

        protected void DetermineCurrentDevice()
        {
            if (UIDevice.CurrentDevice == null) return;

            var name = UIDevice.CurrentDevice.Name;
            var model = UIDevice.CurrentDevice.Model;
            var localizedModel = UIDevice.CurrentDevice.LocalizedModel;
            var systemName = UIDevice.CurrentDevice.SystemName;
            var systemVersion = UIDevice.CurrentDevice.SystemVersion;
            var zone = UIDevice.CurrentDevice.Zone;
            var description = string.Format(
                "{0} device (named: \"{1}\") running {2} ver. {3}, registered and activated by {4} ver. {5}.",
                model,
                name,
                systemName,
                systemVersion,
                NSBundle.MainBundle.InfoDictionary["CFBundleName"].ToString(),
                NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString());
            //string serialNumber = "039d21ec43819fd0";//this is your nexus device UniqueIdentifier we have received here..
            //string serialNumber = UIDevice.CurrentDevice.IdentifierForVendor.ToString();//It's unique per device per installation
#if DEBUG
            string serialNumber = "c76837ab-eb66-426b-b0f7-176fb7b06b0e";//Apple Inc. Device
#else
                string serialNumber = UniqueID;//It's unique per device
#endif

            CurrentDevice = new MobileDevice
            {
                DeviceModel = model,
                DeviceName = localizedModel,
                DeviceVersion = string.Format("{0} {1}", systemName, systemVersion),
                DeviceUniqueId = serialNumber,
                OwnerSpecificName = name,
                Description = description
            };

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                CurrentDevice.Type = DeviceType.Tablet;
                //ActiveCamera = CameraType.FrontCamera;
            }
            else
            {
                CurrentDevice.Type = DeviceType.Phone;
                //ActiveCamera = CameraType.BackCamera;
            }
        }

        public async Task<OperationResult<string>> RequestCameraAccess()
        {
            //#if !DEBUG

            if (AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video) != AVAuthorizationStatus.Authorized)
            {
                await AVCaptureDevice.RequestAccessForMediaTypeAsync(AVMediaType.Video).ConfigureAwait(false);
            }


            if (AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video) != AVAuthorizationStatus.Authorized)
            {
                //TODO: add message to language
                var locDevErrorMessage = "This application needs to have access to camera for barcode scanning. But, user denied that permission request.";
                LogsRepository.AddError("RequestCameraAccess() error", locDevErrorMessage);

                var locClientErrorMessage = "This application needs to have access to camera for barcode scanning.\n" +
                    "Please go to: Settings > Privacy > Camera\n" +
                    "and turn on the \"Mobile Lounge\".";
                LogsRepository.AddClientError("Camera access permission denied", locClientErrorMessage);

                //throw new Exception("RequestCameraAccess() error", new Exception(locDevErrorMessage));
                return new OperationResult<string>
                {
                    IsSucceeded = false,
                    Message = locClientErrorMessage
                };
            }
            //#endif
            return new OperationResult<string>
            {
                IsSucceeded = true
            };
        }

        public void NotifyExternalCameraChanged(object sender, ConnectionStateEventArgs e)
        {
            if (OnExternalScannerStatusChanged != null) OnExternalScannerStatusChanged.Invoke(sender, e);
        }

        public OperationResult<string> RequestLocationAccess()
        {
            //#if !DEBUG
            if (CLLocationManager.Status == CLAuthorizationStatus.AuthorizedAlways) return new OperationResult<string> { IsSucceeded = true };


            //InvokeOnMainThread(()=> {
            CLLocationManager locMgr = new CLLocationManager();
            locMgr.PausesLocationUpdatesAutomatically = false;


            // iOS 8 has additional permissions requirements
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                locMgr.RequestAlwaysAuthorization(); // works in background
                                                     //locMgr.RequestWhenInUseAuthorization (); // only in foreground
            }

            if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
            {
                locMgr.AllowsBackgroundLocationUpdates = true;
            }

            //locMgr.RequestAlwaysAuthorization();
            //locMgr.RequestWhenInUseAuthorization();
            //});

            //LocMgr.AuthorizationChanged += (object sender, CLAuthorizationChangedEventArgs e) => { };

            while (CLLocationManager.Status == CLAuthorizationStatus.NotDetermined)//TODO: detect if it couldn't ask the client for permissions
            {
                Task.Delay(100).Wait();
            }

            if (!CLLocationManager.LocationServicesEnabled)
            {
                var msg = "User must enable the location service in the device settings";
                LogsRepository.AddClientError("Location service is disabled", msg);
                //throw new Exception("RequestLocationAccess() error", new Exception("Location Service is diabled"));
                return new OperationResult<string> { IsSucceeded = false, Message = msg };
            }

            if (CLLocationManager.Status == CLAuthorizationStatus.Denied)
            {
                //TODO: add to language
                var locDevErrorMessage = "This application needs permission to access the device location. But, user denied that permission request.";
                LogsRepository.AddError("RequestLocationAccess() error", locDevErrorMessage);

                var locClientErrorMessage = "This application needs permission to access the device location.\n" +
                    "Please go to: Settings > Privacy > Location Services > Mobile Lounge\n" +
                    "and select the \"Always\" option.";
                LogsRepository.AddClientError("Location access permission denied", locClientErrorMessage);
                //AIMSError("Error", locClientErrorMessage);
                //throw new Exception("RequestLocationAccess() error", new Exception(locDevErrorMessage));
                return new OperationResult<string> { IsSucceeded = false, Message = locClientErrorMessage };
            }
            //#endif
            return new OperationResult<string> { IsSucceeded = true };
        }

        private void OnApplicationStart(object sender, EventArgs e)
        {

        }

        public static void TriggerApplicationStart()
        {
            UnderlyingApplication.TriggerApplicationStart();
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            bool result = false;

            NSUserDefaults shared = new NSUserDefaults("MobileLounge.iegamerica.com", NSUserDefaultsType.SuiteName);

            var BackButtonEnabled = shared.BoolForKey("enabled_preference");

            try
            {
                Microsoft.AppCenter.AppCenter.Start("f65050a9-fdcd-4f08-9a05-70ee505af446", typeof(Microsoft.AppCenter.Analytics.Analytics),
                                                                                            typeof(Microsoft.AppCenter.Crashes.Crashes));

                Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjM0OTQ4QDMxMzgyZTMxMmUzMGIxak5ycnNJQldGYW8yeWRhYXRoQjY5VzQ3MUVyQ3dJOGlpaElVYjhXQnM9");

                NSUrlSessionConfiguration.DefaultSessionConfiguration.TimeoutIntervalForRequest = 120;
                NSUrlSessionConfiguration.DefaultSessionConfiguration.TimeoutIntervalForResource = 120.0;

                InfineaIQ.Instance.SetDeveloperKey("UPJPtVhdfwBtelF8nEIThouZiM3eY8seElxCXeiYWqxRFpEWmH1UOAUxN7GLWYcB7DSf+23tF9gFXNyZFcQIsQ==");


                AIMS.Contracts.WebApiServiceContext.OnLoadHttpClientNative += WebApiServiceContext_OnLoadHttpClientHandler;

                DetermineCurrentDevice();

                #region Detecting device path
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
                string startupFolder = System.IO.Path.Combine(documentsPath, "../Library/"); // SQLite DB folder
                #endregion

                #region Creating the GPS adapter
                var gpsAdapter = new GPSAdapter();
                #endregion

                UnderlyingApplication.TriggerApplicationInit(startupFolder, gpsAdapter, CurrentDevice);

                LogsRepository.AddClientInfo("Application started");
                result = true;
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Crashes.Crashes.TrackError(ex);

                //TODO: must show the error using the default language
                if (ex.Message.Contains("GPS Error"))
                {
                    LogsRepository.AddError("Error in initiating GPS.", ex);
                    AIMSMessage(AppDelegate.TextProvider.GetText(2543), TextProvider.GetText((int)ErrorCode.LocationRetrievalException));
                    //var progressDialog = ProgressDialog.Show(this, "GPS Error", app.TextProvider.GetText((int)ErrorCode.LocationRetrievalException), true, true,
                    //    (sender, e) =>
                    //    {
                    //        applicationMustClose = true;
                    //    });
                }
                else if (ex.Message.Contains("Cannot store Unique ID"))
                {
                    LogsRepository.AddError("Keychain Access Error", ex);
                    AIMSMessage(AppDelegate.TextProvider.GetText(2544), AppDelegate.TextProvider.GetText(2545) + ex.Message);
                }
                else
                {
                    LogsRepository.AddError("Startup Error", ex);
                    if (TextProvider != null)
                        AIMSMessage(AppDelegate.TextProvider.GetText(2546), TextProvider.GetText((int)ErrorCode.UnknownException));
                    //var progressDialog = ProgressDialog.Show(this, "Startup Error", app.TextProvider.GetText((int)ErrorCode.UnknownException), true, true,
                    //    (sender, e) =>
                    //    {
                    //        applicationMustClose = true;
                    //    });
                }
            }

            UIStoryboard storyBoard = this.Window.RootViewController.Storyboard;

            LoginView = storyBoard.InstantiateViewController("LoginViewController");
            LoungeSelectionView = storyBoard.InstantiateViewController("loungeController");
            AirlinesView = storyBoard.InstantiateViewController("airLineViewController") as AirLineViewController;
            ManualPassengerView = storyBoard.InstantiateViewController("manualTabController") as ManualPassengerTabController;
            SinglePageMIView = storyBoard.InstantiateViewController("singlePageMIViewController") as SinglePageMIViewController;
            SinglePageMIView2 = storyBoard.InstantiateViewController("singlePageMIViewController2") as SinglePageMIViewController2;
            GuestServicesView = storyBoard.InstantiateViewController("guestServicesViewController") as GuestsViewController;

            ValidationView = storyBoard.InstantiateViewController("validationResultController2") as ValidationResultViewController2;
            GuestValidationResultView = storyBoard.InstantiateViewController("validationResultController2") as ValidationResultViewController2;

            ChartViewController = storyBoard.InstantiateViewController("chartViewController") as ChartViewController;
            MapViewController = storyBoard.InstantiateViewController("mapViewController") as MapViewController;

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
            {
                AirlinesView.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
                ManualPassengerView.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
                SinglePageMIView.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
                SinglePageMIView2.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
                GuestServicesView.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
                ValidationView.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
                GuestValidationResultView.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
                MapViewController.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;
            }

            AppDelegate.UpdateOccupancyTimer();

            AppManager.initialCurrentLang();

            /*while (DefaultMobileApplication.Position == null && result)//waiting for the GPS position detection
            {
                Task.Delay(10).Wait();
            }*/

            if (result)
            {

                if (AppData.selectedLanguage != null)
                {
                    this.Window.RootViewController = LoginView;
                }

                return true;
            }

            //UIAlertView alertView = new UIAlertView("ERROR", "error initialisation : 400", null, null, null);

            return false;
        }

        private HttpClient WebApiServiceContext_OnLoadHttpClientHandler()
        {
            NSUrlSessionConfiguration sessionConfig = NSUrlSessionConfiguration.DefaultSessionConfiguration;
            sessionConfig.TimeoutIntervalForRequest = 60;
            sessionConfig.TimeoutIntervalForResource = 300;
            sessionConfig.WaitsForConnectivity = true;

            NSUrlSessionHandler sessionHandler = new NSUrlSessionHandler(sessionConfig);

            HttpClient httpClient = new HttpClient(sessionHandler);

            return httpClient;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations(UIApplication application,
             UIWindow forWindow)
        {
            return UIInterfaceOrientationMask.All;//Now, first it checks for each UIViewController orientations in runtime.
        }

        public static void ReturnToLoginView()//TODO: must dismiss and dispose all the other forms
        {
            var vc2 = LoungeSelectionView.PresentedViewController;

            if (vc2 == AirlinesView)
            {
                vc2.DismissViewController(false, () =>
                {
                    try
                    {
                        UIViewTransition.displayFromRightToLeftLinear(LoungeSelectionView, LoginView, 0.01f);
                    }
                    catch
                    {
                        vc2.DismissViewController(false, () =>
                        {
                            try
                            {
                                UIViewTransition.displayFromRightToLeftLinear(LoungeSelectionView, LoginView, 0.01f);
                            }
                            catch (Exception ex)
                            {
#if DEBUG
                                AppDelegate.Current.AIMSMessage(AppDelegate.TextProvider.GetText(2047), string.Format("{0}:\n{1}", ex.Message, ex.StackTrace));
#endif
                            }
                        });
                    }
                });
            }
            else
            {
                UIViewTransition.displayFromRightToLeftLinear(LoungeSelectionView, LoginView, 0.01f);
            }
            if (MembershipProvider.Current != null)
                MembershipProvider.Current.LogOutUser();
        }

        
    }
}

