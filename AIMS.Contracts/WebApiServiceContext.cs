﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Globalization;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using AIMS.Contracts.Security;
using System.Linq;
using AIMS.Contracts.DataContracts;
using System.Reflection;
using System.Net;
using System.Text.RegularExpressions;

namespace AIMS.Contracts
{
    public enum CallPriory
    {
        Low = 0,
        Heigh = 1,
        NoQueue = 3
    }

    public class WebApiServiceContext : IWebApiServiceContext
    {
        #region Fields
        private static string aimsServerAddress;
        private static MobileDevice _device;
        private static string _message;
        private static string _version;
        private static TokenResponse _clientToken;
        private static DateTime _tokenExpirationTime;
        private static IWebProxy _proxy;


        public static event LoadHttpClientNative OnLoadHttpClientNative;

        

        private static JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings { DateFormatHandling = DateFormatHandling.IsoDateFormat };

#if DEBUG
         //private const string aimsIdentityServiceAddress = "http://192.168.6.237/Ieg.OAuth/connect/token"; //Local test service <!--Jhonatan Physical Environment-->
       // private const string aimsIdentityServiceAddress = "http://192.168.95.115/Ieg.OAuth/connect/token"; //Local test service <!--Jhonatan Virtual Environment-->
        //private const string aimsIdentityServiceAddress = "https://AIMS2.ieg-america.com/AIMS_2_SERVICES/Mobile/IdentityService/connect/token";

        //private const string aimsIdentityServiceAddress = "https://ieg-identity-server-canada-east-staging.azurewebsites.net/connect/token";

        private const string aimsIdentityServiceAddress = "https://login.ieg-america.com/connect/token";//Azure service <!--Azure PROD Environment-->
                                                                                                        //private const string aimsIdentityServiceAddress = "http://192.168.6.105/Ieg.OAuth/connect/token"; //Local test service <!--Siavash Environment-->
                                                                                                        //private const string aimsIdentityServiceAddress = "http://192.168.95.147/Mobile/IdentityService/connect/token";//DEV (147) Environment
#else
        private const string aimsIdentityServiceAddress = "https://login.ieg-america.com/connect/token";//Azure service <!--Azure Environment-->
        //private const string aimsIdentityServiceAddress = "https://aims2.ieg-america.com/AIMS_2_SERVICES/IdentityServer/v1/connect/token";

        //private const string aimsIdentityServiceAddress = "https://ieg-identity-server-canada-east-staging.azurewebsites.net/connect/token";

        //private const string aimsIdentityServiceAddress = "https://AIMS2.ieg-america.com/AIMS_2_SERVICES/Mobile/IdentityService/connect/token";
#endif

        //#if DEBUG
        //private static string aimsServerAddress = "https://aimsua.ieg-america.com/aims_2_Services/mobile/V2719082/Ieg.MobileWebApi/";//UnitedAirline (Production with WhiteList)
        //private static string aimsServerAddress = "https://aims2.ieg-america.com/aims_2_Services/mobile/V2719082/Ieg.MobileWebApi/";//DEMO
        //private static string aimsServerAddress = "http://192.168.6.105/Ieg.NewWebApi/";//Local test service <!--TODO: Siavash Environment-->
        //#else
        //private static string aimsServerAddress = "https://aimsua.ieg-america.com/aims_2_Services/mobile/V2719082/Ieg.MobileWebApi/";//UnitedAirline (Production with WhiteList)
        //private static string aimsServerAddress = "https://aims2.ieg-america.com/aims_2_Services/mobile/V2719082/Ieg.MobileWebApi/";//DEMO
        //#endif

#if DEBUG

          //private const string aimsLicensingServiceAddress = "https://ieg-licensing-service-canada-east-staging.azurewebsites.net/"; //UA DataPower QA

        //private const string aimsLicensingServiceAddress = "https://ieg-licensing-service-canada-east.azurewebsites.net/";

        // private const string aimsLicensingServiceAddress = "http://192.168.6.237/LicensingService/";//Local test service <!--Jhonatan Physical Environment-->
        //private const string aimsLicensingServiceAddress = "http://192.168.95.115/LicensingService/";//Local test service <!--Jhonatan Virtual Environment-->

           private const string aimsLicensingServiceAddress = "https://api.ieg-america.com/security/licensing/v1/";//Azure service <!--Azure PROD Environment-->

        //private const string aimsLicensingServiceAddress = "https://ieg-licensing-service-canada-east-staging.azurewebsites.net/";//Azure service <!--Azure Staging Environment-->
        //private const string aimsLicensingServiceAddress = "http://192.168.6.105/LicensingService/";//Local test service <!--Siavash Environment-->
        //private const string aimsLicensingServiceAddress = "https://AIMS2.ieg-america.com/AIMS_2_SERVICES/Mobile/LicensingService/";
        //private const string aimsLicensingServiceAddress = "http://192.168.95.147/Mobile/LicensingService/";//DEV (147) Environment
#else
        private const string aimsLicensingServiceAddress = "https://api.ieg-america.com/security/licensing/v1/";//Azure service <!--Azure Environment-->
        //private const string aimsLicensingServiceAddress = "https://aims2.ieg-america.com/AIMS_2_SERVICES/LicensingServer/v1/";

        //private const string aimsLicensingServiceAddress = "https://ieg-licensing-service-canada-east-staging.azurewebsites.net/"; //UA DataPower QA

        //private const string aimsLicensingServiceAddress = "https://aims2.ieg-america.com/AIMS_2_SERVICES/Mobile/LicensingService/";
#endif

        #endregion

        #region Properties

        public static MobilePrincipal Principal { get; private set; }

        public static UserConfigurations Configurations { get; private set; }

        public bool IsAuthenticated
        {
            get
            {
                return isAuthenticated();
            }
        }
        public bool IsExpired
        {
            get
            {
                return isExpired();
            }
        }
        public string Message
        {
            get
            {
                return _message;
            }
        }

        //Keep it static as it may affect the performance
        public static string Version
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_version))
                {
                    var assembly = typeof(WebApiServiceContext).GetTypeInfo().Assembly;
                    // In some PCL profiles the above line is: var assembly = typeof(MyType).Assembly;
                    var assemblyName = new AssemblyName(assembly.FullName);
                    _version = assemblyName.Version.ToString();//.Major + "." + assemblyName.Version.Minor;
                }
                return _version;
            }
        }
        #endregion

        #region Constants
        private const string CLIENT_ID = "iegmobilelounge";
        private const string CLIENT_SECRET = "iegsecrethuman";
        #endregion

        #region Application Lifetime Scope
        private static bool isAuthenticated()
        {
            if (_clientToken == null || !string.IsNullOrWhiteSpace(_clientToken.Error) || isExpired()) return false;
            return true;
        }

        private static bool isExpired()
        {
            if (_tokenExpirationTime == default(DateTime)) return false;
            if (_tokenExpirationTime.CompareTo(DateTime.Now) <= 0) return true;
            return false;
        }

        private static void setRefreshTokenTime(string expiresIn)
        {
            int expIn = 0;
            float realExpIn = 0;
            float.TryParse(expiresIn, out realExpIn);
            expIn = (int)Math.Floor(realExpIn);

            if (expIn > 60)
                _tokenExpirationTime = DateTime.Now.AddSeconds(expIn - 10);
            else
                _tokenExpirationTime = DateTime.Now.AddSeconds(expIn - Math.Ceiling((float)expIn / 5));
        }

        /// <summary>
        /// Creates HttpClient with the required headers and current active token if needed. The creation takes longer of the token is expired
        /// </summary>
        /// <param name="position">Current device position</param>
        /// <param name="withToken">If token is required</param>
        /// <param name="timeoutSeconds">Configurable timeout</param>
        /// <returns>An instance of HttpClient ready to use for WebAPI calls</returns>
        private static HttpClient CreateHttpClient(Position position, bool withToken = true, int timeoutSeconds = 30, bool tokenCheck = true)
        {
            HttpClient client = null;
            if (_proxy != null)
            {
                var clientHandler = new HttpClientHandler();

                if (clientHandler.SupportsAutomaticDecompression)
                    clientHandler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                if (clientHandler.SupportsProxy)
                {
                    clientHandler.UseProxy = true;
                    clientHandler.Proxy = _proxy;
                }

                client = new HttpClient(clientHandler);
            }
            else
            {
                if (OnLoadHttpClientNative != null)
                {
                    client = OnLoadHttpClientNative.Invoke();
                }
                else
                {
                    client = new HttpClient();
                }
            }

            //var httpClient = new HttpClient(new ModernHttpClient.NativeMessageHandler());//TODO: Test the ModernHttpClient from nuget


#if DEBUG
            timeoutSeconds = 60;//1 minutes for debug only
#endif
            client.Timeout = new TimeSpan(0, 0, timeoutSeconds);

            if (withToken)
            {
                if (isExpired() && tokenCheck)
                    RefreshUser();
                client.DefaultRequestHeaders.Authorization
                            = new AuthenticationHeaderValue("Bearer", _clientToken.AccessToken);
            }

            Regex reg = new Regex("[^0-9A-Za-z ,]");

            client.DefaultRequestHeaders.Add("Lat", position.Latitude.ToString(CultureInfo.InvariantCulture));
            client.DefaultRequestHeaders.Add("Lon", position.Longitude.ToString(CultureInfo.InvariantCulture));
            client.DefaultRequestHeaders.Add("Alt", position.Altitude.HasValue ? position.Altitude.Value.ToString(CultureInfo.InvariantCulture) : "0");
            client.DefaultRequestHeaders.Add("Acc", position.Accuracy.HasValue ? position.Accuracy.Value.ToString(CultureInfo.InvariantCulture) : "0");
            client.DefaultRequestHeaders.Add("UId", _device.DeviceUniqueId);
            client.DefaultRequestHeaders.Add("Ver", Version);
           // client.DefaultRequestHeaders.Add("Own", _device.OwnerSpecificName);
            client.DefaultRequestHeaders.Add("Nam", _device.DeviceName);
            client.DefaultRequestHeaders.Add("Mod", _device.DeviceModel);
            // client.DefaultRequestHeaders.Add("Des", _device.Description);

            client.DefaultRequestHeaders.Add("Own", reg.Replace(_device.OwnerSpecificName, string.Empty));
            client.DefaultRequestHeaders.Add("Des", reg.Replace(_device.Description, string.Empty));

            return client;
        }

        public static bool AuthenticateUser(string userName, string password, MobileDevice device, IWebProxy proxy)
        {
            _message = "";
            _device = device;
            _proxy = proxy;

            try
            {
                using (var client = new HttpClient())
                {
                    var basicAuth =
                        Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", CLIENT_ID, CLIENT_SECRET)));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", basicAuth);

                    var rawResult = client.PostAsync(aimsIdentityServiceAddress, new FormUrlEncodedContent(
                                                                                          new[]
                                                                                          {
                                                                                          new KeyValuePair<string, string>("grant_type", "password"),
                                                                                          //new KeyValuePair<string, string>("scope", "ieg_aims_mobile"), 
                                                                                          new KeyValuePair<string, string>("scope", "ieg_aims_mobile offline_access"),// "offline_access" is needed for acquiring the tocken (no need for UA DataPower)
                                                                                          new KeyValuePair<string, string>("username", userName),
                                                                                          new KeyValuePair<string, string>("password", password),
                                                                                          })).Result;

                    if (!rawResult.IsSuccessStatusCode)
                    {
                        _message = rawResult.ReasonPhrase;
                        return false;
                    }

                    var data = rawResult.Content.ReadAsStringAsync().Result;

                    _clientToken = JsonConvert.DeserializeObject<TokenResponse>(data);

                    data = data.ToLower();//Do not lower it before parsing (AccessTocken string will be changed)

                    if (!string.IsNullOrWhiteSpace(_clientToken.Error) || (data.Contains("message") && data.Contains("error")))
                    {
                        _message = _clientToken.Error;
                        return false;
                    }
                    setRefreshTokenTime(_clientToken.ExpiresIn);

                    try
                    {
                        var deserializedToken = JWT.JsonWebToken.Decode(_clientToken.AccessToken, basicAuth, false);
                        var claims = JsonConvert.DeserializeObject<Dictionary<string, object>>(deserializedToken)
                            .Select(x => new MobileClaim { Key = x.Key, Value = Convert.ToString(x.Value) });

                        Principal = new MobilePrincipal(claims);

                        using (var securedClient = new HttpClient())
                        {
                            securedClient.DefaultRequestHeaders.Authorization
                                            = new AuthenticationHeaderValue("Bearer", _clientToken.AccessToken);

                            Regex reg = new Regex("[^0-9A-Za-z ,]");

                            securedClient.DefaultRequestHeaders.Add("UId", _device.DeviceUniqueId);
                            //securedClient.DefaultRequestHeaders.Add("Lat", position.Latitude.ToString(CultureInfo.InvariantCulture));
                            //securedClient.DefaultRequestHeaders.Add("Lon", position.Longitude.ToString(CultureInfo.InvariantCulture));
                            //securedClient.DefaultRequestHeaders.Add("Alt", position.Altitude.HasValue ? position.Altitude.Value.ToString(CultureInfo.InvariantCulture) : "0");
                            //securedClient.DefaultRequestHeaders.Add("Acc", position.Accuracy.HasValue ? position.Accuracy.Value.ToString(CultureInfo.InvariantCulture) : "0");
                            //securedClient.DefaultRequestHeaders.Add("UId", _device.DeviceUniqueId);
                            securedClient.DefaultRequestHeaders.Add("Ver", Version);
                           // securedClient.DefaultRequestHeaders.Add("Own", _device.OwnerSpecificName);
                            securedClient.DefaultRequestHeaders.Add("Nam", _device.DeviceName);
                            securedClient.DefaultRequestHeaders.Add("Mod", _device.DeviceModel);
                           // securedClient.DefaultRequestHeaders.Add("Des", _device.Description);


                            securedClient.DefaultRequestHeaders.Add("Own", reg.Replace(_device.OwnerSpecificName, string.Empty));
                            securedClient.DefaultRequestHeaders.Add("Des", reg.Replace(_device.Description, string.Empty));

                            HttpContent content = new StringContent(JsonConvert.SerializeObject(new ConfigurationRequest
                            {
                                MobileKey = Principal.MobileKey,
                                Username = userName,
                                DeviceUniqueId = _device.DeviceUniqueId
                            }, jsonSerializerSettings), Encoding.UTF8, "application/json");

                            rawResult = securedClient.PostAsync(string.Format("{0}userconfigurations", aimsLicensingServiceAddress), content).Result;

                            if (!rawResult.IsSuccessStatusCode)
                            {
                                _message = rawResult.ReasonPhrase;
                                _clientToken = null;
                                return false;
                            }

                            data = rawResult.Content.ReadAsStringAsync().Result;

                            var configRes = JsonConvert.DeserializeObject<ResultPack<UserConfigurations>>(data);
                            if (!configRes.IsSucceeded || configRes.ReturnParam == null)
                            {
                                _message = "Could not retrieve the user configurations.\n" + configRes.Message;
                                _clientToken = null;
                                return false;
                            }
                            Configurations = configRes.ReturnParam;
                            if (Configurations.IsBlackListed)
                            {
                                _message = "Blacklisted";
                                _clientToken = null;
                                return false;
                            }

                            if (string.IsNullOrWhiteSpace(Configurations.MobileServerURL))
                            {
                                _message = "Could not retrieve the server URL";
                                _clientToken = null;
                                return false;
                            }

                            aimsServerAddress = Configurations.MobileServerURL;
#if DEBUG
                            //aimsServerAddress = "https://aims2.ieg-america.com/AIMS_2_SERVICES/Mobile/v271909_Search/Ieg.MobileWebApi/";
                            // aimsServerAddress = "http://192.168.95.115/Ieg.NewWebApi/";
                            //aimsServerAddress = "https://AIMS2.ieg-america.com/AIMS_2_SERVICES/Mobile/v271909_Occupancy/Ieg.MobileWebApi/";
                            //aimsServerAddress = "https://aims2.ieg-america.com/aims_2_Services/mobile/V2719082/Ieg.MobileWebApi/";
                            //aimsServerAddress = "https://aimsua.ieg-america.com/aims_2_Services/mobile/V2719082/Ieg.MobileWebApi/";
                            //aimsServerAddress = "https://aimsua.ieg-america.com/aims_2_Services/mobile/V271909_Occu/Ieg.MobileWebApi/";
                            //aimsServerAddress = "http://192.168.6.105/Ieg.NewWebApi/";
                            //aimsServerAddress = "https://aims2.ieg-america.com/AIMS_2_SERVICES/Mobile/v271909/Ieg.MobileWebApi/"; //DEMO
                            //aimsServerAddress = "http://192.168.6.240/Ieg.NewWebApi/";

                            //OLD DEMO: https://aims2.ieg-america.com/AIMS_2_SERVICES/Mobile/V2902/Ieg.MobileWebApi/

                            //aimsServerAddress = "https://aimsua.ieg-america.com/AIMS_2_SERVICES/Mobile/V300702/Ieg.MobileWebApi/";

                            //  aimsServerAddress = "https://aimsua.ieg-america.com/AIMS_2_SERVICES/Mobile/V3012_Test/Ieg.MobileWebApi/";

                            // aimsServerAddress = "https://aimsua.ieg-america.com/AIMS_2_SERVICES/Mobile/V3012_PROD/Ieg.MobileWebApi/";

                           // aimsServerAddress = "http://192.168.6.237/Ieg.NewWebApi/";


                            System.Diagnostics.Debug.WriteLine($"AIMS.Contracts.WebApiServiceContext: The Uri of the aims server address is set to \"{aimsServerAddress}\"");
#endif
                        }
                    }
                    catch (Exception ex)
                    {
                        _message = string.Format("{0}: {1}", ex.Message, ex.StackTrace);
                        _clientToken = null;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _message = ex.InnerException.Message;
                else _message = ex.Message;

                _clientToken = null;
                return false;
            }
            return isAuthenticated();
        }

        public static bool RefreshUser()
        {
            if (_clientToken == null || string.IsNullOrWhiteSpace(_clientToken.RefreshToken))
                return false;

            _message = "";
            try
            {
                using (var client = new HttpClient())
                {
                    var basicAuth =
                        Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", CLIENT_ID, CLIENT_SECRET)));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", basicAuth);

                    var rawResult = client.PostAsync(aimsIdentityServiceAddress, new FormUrlEncodedContent(
                                                                                          new[]
                                                                                          {
                                                                                          new KeyValuePair<string, string>("grant_type", "refresh_token"),
                                                                                          new KeyValuePair<string, string>("refresh_token", _clientToken.RefreshToken)
                                                                                          })).Result;

                    if (!rawResult.IsSuccessStatusCode)
                    {
                        _message = rawResult.ReasonPhrase;
                        return false;
                    }

                    var data = rawResult.Content.ReadAsStringAsync().Result;

                    _clientToken = JsonConvert.DeserializeObject<TokenResponse>(data);

                    data = data.ToLower();//Do not lower it before parsing (AccessTocken string will be changed)

                    if (!string.IsNullOrWhiteSpace(_clientToken.Error) || (data.Contains("message") && data.Contains("error")))
                    {
                        _message = _clientToken.Error;
                        return false;
                    }
                    setRefreshTokenTime(_clientToken.ExpiresIn);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _message = ex.InnerException.Message;
                else _message = ex.Message;

                _clientToken = null;
                return false;
            }
            return isAuthenticated();
        }
        #endregion

        #region Constructors / Destructors
        /// <summary>
        /// For the first time instantiation this method must be used
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="device"></param>
        public WebApiServiceContext(string username, string password, MobileDevice device, IWebProxy proxy)
        {
            if (!IsAuthenticated)
            {
                AuthenticateUser(username, password, device, proxy);
            }
        }

        /// <summary>
        /// For the second or further instantiations this method can be used
        /// You are already authorized
        /// </summary>
        public WebApiServiceContext()
        {
            //if (!IsAuthenticated)
            //    throw new Exception("You are not authorized to use WebApi ServiceContext");
        }

        public void Dispose()
        {
            //Token can not be set to null
        }
        #endregion

        #region Methods
        public void SendJson<T>(string actionURL, object jsonObject, Position position, WebApiEventHandler successCallback, WebApiExceptionEventHandler failedCallBack)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine($">>>> Post: {actionURL} <<<<<");
#endif


            var process = new Task(new Action(async () =>
            {
                var sb = new StringBuilder();
                DateTimeOffset prepTime, requestTime, responseTime;

                try
                {
                    var returnType = typeof(T);
                    prepTime = DateTimeOffset.Now;
                    sb.Append(returnType.Name);
                    if (returnType.GenericTypeArguments.Count() > 0)
                        sb.Append("<").Append(returnType.GenericTypeArguments[0].Name).Append(">");


                    using (var client = CreateHttpClient(position)) //using (var client = new HttpClient())
                    {
                        HttpContent content = new StringContent(JsonConvert.SerializeObject(jsonObject, jsonSerializerSettings), Encoding.UTF8, "application/json");
                        //var resultJson = client.PostAsync(string.Format("{0}{1}", aimsServerAddress, actionURL), content).Result;

                        requestTime = DateTimeOffset.Now;

                        var resultJson = await client.PostAsync(string.Format("{0}{1}", aimsServerAddress, actionURL), content).ConfigureAwait(false);

                        responseTime = DateTimeOffset.Now;
                        var prepTimeLength = requestTime - prepTime;
                        var responseTimeLength = responseTime - requestTime;
                        var saveToAppCenter = new Dictionary<string, Dictionary<string, string>>();
                        saveToAppCenter.Add($"Post<{sb.ToString()}>", new Dictionary<string, string> {
                            { "Action Url", string.Format("{0}{1}", aimsServerAddress, actionURL) },
                            { "length of preparation", "< " + Math.Ceiling(prepTimeLength.TotalSeconds).ToString() + "s" },
                            { "length of call", "< " + Math.Ceiling(responseTimeLength.TotalSeconds).ToString() + "s" }
                        });


                        //var response = client.PutAsync(actionURL, inputMessage.Content).Result;
                        if (resultJson.IsSuccessStatusCode)
                        {
                            //var inter = resultJson.Content.ReadAsStringAsync().Result;

                            var inter = await resultJson.Content.ReadAsStringAsync();

                            if (successCallback != null)
                            {
                                var jsonObj = JsonConvert.DeserializeObject<T>(inter);
                                successCallback.Invoke(jsonObj, saveToAppCenter);
                            }

                        }
                        else
                            if (failedCallBack != null)
                            failedCallBack.Invoke(new ResultPack<Exception>
                            {
                                IsSucceeded = false,
                                ReturnParam = new Exception(resultJson.ReasonPhrase),
                                Message = "Error calling service"
                            }, saveToAppCenter);
                    }
                }
                catch (Exception ex)
                {
                    responseTime = DateTimeOffset.Now;
                    var prepTimeLength = requestTime - prepTime;
                    var responseTimeLength = responseTime - requestTime;
                    var saveToAppCenter = new Dictionary<string, Dictionary<string, string>>();
                    saveToAppCenter.Add($"Post<{sb.ToString()}>", new Dictionary<string, string> {
                            { "Action Url", string.Format("{0}{1}", aimsServerAddress, actionURL) },
                            { "length of preparation", "< " + Math.Ceiling(prepTimeLength.TotalSeconds).ToString() + "s" },
                            { "length of call", "< " + Math.Ceiling(responseTimeLength.TotalSeconds).ToString() + "s" }
                        });


                    if (failedCallBack != null)
                        failedCallBack.Invoke(new ResultPack<Exception>
                        {
                            IsSucceeded = false,
                            ReturnParam = ex,
                            Message = "Error calling service"
                        }, saveToAppCenter);
                }
            }));
            process.Start();
        }

        public void GetString(string actionURL, Position position, WebApiEventHandler successCallback, WebApiExceptionEventHandler failedCallBack)
        {
            var process = new Task(new Action(async () =>
            {
                try
                {
                    using (var client = CreateHttpClient(position)) //using (var client = new HttpClient())
                    {
                        //webApiResult = client.GetStringAsync(string.Format("{0}{1}", aimsServerAddress, actionURL)).Result;

                        var asyncResult = await client.GetAsync(string.Format("{0}{1}", aimsServerAddress, actionURL)).ConfigureAwait(false);
                        asyncResult.EnsureSuccessStatusCode();
                        string webApiResult = await asyncResult.Content.ReadAsStringAsync();

                        if (successCallback != null)
                            successCallback.Invoke(webApiResult, null);
                    }
                }
                catch (Exception ex)
                {
                    if (failedCallBack != null)
                        failedCallBack.Invoke(new ResultPack<Exception>
                        {
                            IsSucceeded = false,
                            ReturnParam = ex,
                            Message = "Error calling service"
                        });
                }
            }));
            process.Start();
        }

        public void GetJson<T>(string actionURL, Position position, WebApiEventHandler successCallback, WebApiExceptionEventHandler failedCallBack)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine($">>>> Get: {actionURL} <<<<<");
#endif

            var process = new Task(new Action(async () =>
            {

                var sb = new StringBuilder();
                DateTimeOffset prepTime, requestTime, responseTime;

                try
                {
                    var returnType = typeof(T);
                    prepTime = DateTimeOffset.Now;
                    sb.Append(returnType.Name);
                    if (returnType.GenericTypeArguments.Count() > 0)
                        sb.Append("<").Append(returnType.GenericTypeArguments[0].Name).Append(">");


                    int? timeout = (int?)null;
                    if (actionURL.Contains("lounge/") || actionURL.Contains("workstation/"))// for GetMobileWorkstations and GetAirlines only longer timeouts
                        timeout = 240;

                    using (var client = timeout.HasValue ? CreateHttpClient(position, true, timeout.Value) : CreateHttpClient(position))
                    {
                        requestTime = DateTimeOffset.Now;

                        var asyncResult = await client.GetAsync(string.Format("{0}{1}", aimsServerAddress, actionURL)).ConfigureAwait(false);

                        responseTime = DateTimeOffset.Now;
                        var prepTimeLength = requestTime - prepTime;
                        var responseTimeLength = responseTime - requestTime;
                        var saveToAppCenter = new Dictionary<string, Dictionary<string, string>>();
                        saveToAppCenter.Add($"Get<{sb.ToString()}>", new Dictionary<string, string> {
                            { "Action Url", string.Format("{0}{1}", aimsServerAddress, actionURL) },
                            { "length of preparation", "< " + Math.Ceiling(prepTimeLength.TotalSeconds).ToString() + "s" },
                            { "length of call", "< " + Math.Ceiling(responseTimeLength.TotalSeconds).ToString() + "s" }
                        });


                        asyncResult.EnsureSuccessStatusCode();
                        string webApiResult = await asyncResult.Content.ReadAsStringAsync();

                        if (!string.IsNullOrWhiteSpace(webApiResult))
                        {
                            if (successCallback != null)
                                successCallback.Invoke(JsonConvert.DeserializeObject<T>(webApiResult), saveToAppCenter);
                        }
                        else
                        if (failedCallBack != null)
                            failedCallBack.Invoke(new ResultPack<Exception>
                            {
                                IsSucceeded = false,
                                ReturnParam = new Exception(webApiResult),
                                Message = "Error calling service"
                            });
                    }
                }
                catch (Exception ex)
                {
                    if (failedCallBack != null)
                        failedCallBack.Invoke(new ResultPack<Exception>
                        {
                            IsSucceeded = false,
                            ReturnParam = ex,
                            Message = "Error calling service"
                        });
                }
            }));
            process.Start();
            var i = 0;
            i++;
        }

        public async Task<T> SendJsonAsync<T>(string actionURL, object jsonObject, Position position)
        {
            int? timeout = (int?)null;
            if (actionURL.Contains("lounge/") || actionURL.Contains("workstation/"))// for GetMobileWorkstations and GetAirlines only longer timeouts
                timeout = 240;

            using (var client = timeout.HasValue ? CreateHttpClient(position, true, timeout.Value) : CreateHttpClient(position))
            {
                HttpContent content = new StringContent(JsonConvert.SerializeObject(jsonObject, jsonSerializerSettings), Encoding.UTF8, "application/json");

                var resultJson = await client.PostAsync(string.Format("{0}{1}", aimsServerAddress, actionURL), content).ConfigureAwait(false);
                resultJson.EnsureSuccessStatusCode();

                string webApiResult = await resultJson.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(webApiResult);
            }
        }

        public async Task<T> GetJsonAsync<T>(string actionURL, Position position)
        {
            int? timeout = (int?)null;
            if (actionURL.Contains("lounge/") || actionURL.Contains("workstation/"))// for GetMobileWorkstations and GetAirlines only longer timeouts
                timeout = 240;

            using (var client = timeout.HasValue ? CreateHttpClient(position, true, timeout.Value) : CreateHttpClient(position))
            {
                var asyncResult = await client.GetAsync(string.Format("{0}{1}", aimsServerAddress, actionURL)).ConfigureAwait(false);
                asyncResult.EnsureSuccessStatusCode();
                string webApiResult = await asyncResult.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(webApiResult);
            }
        }

        //public Task<string> GetJsonAsync(string actionURL, Position position)
        //{
        //    int? timeout = (int?)null;
        //    if (actionURL.Contains("lounge/") || actionURL.Contains("workstation/"))// for GetMobileWorkstations and GetAirlines only longer timeouts
        //        timeout = 240;

        //    using (var client = timeout.HasValue ? CreateHttpClient(position, true, timeout.Value) : CreateHttpClient(position))
        //    {
        //        var t = client.GetAsync(string.Format("{0}{1}", aimsServerAddress, actionURL));

        //        return t.ContinueWith(t1 => t1.Result.Content.ReadAsStringAsync().Result);
        //    }
        //}

        //public Task<T> GetJsonAsync<T>(string actionURL, Position position)
        //{
        //    try
        //    {
        //        int? timeout = (int?)null;
        //        if (actionURL.Contains("lounge/") || actionURL.Contains("workstation/"))// for GetMobileWorkstations and GetAirlines only longer timeouts
        //            timeout = 240;

        //        using (var client = timeout.HasValue ? CreateHttpClient(position, true, timeout.Value) : CreateHttpClient(position))
        //        {
        //            var asyncResult = client.GetAsync(string.Format("{0}{1}", aimsServerAddress, actionURL));

        //            return asyncResult.ContinueWith<T>(t1=> {
        //                var t2 = t1.Result.Content.ReadAsStringAsync();
        //                return t2.ContinueWith(t3 => JsonConvert.DeserializeObject<T>(t3.Result));
        //            });
        //            //asyncResult.EnsureSuccessStatusCode();
        //            //string webApiResult = await asyncResult.Content.ReadAsStringAsync();

        //            //if (!string.IsNullOrWhiteSpace(webApiResult))
        //            //{
        //            //    return JsonConvert.DeserializeObject<T>(webApiResult);
        //            //}
        //            //else
        //            //{
        //            //    throw new Exception("Recieved result is empty");
        //            //}
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //        //return default(T);
        //        //if (failedCallBack != null)
        //        //    failedCallBack.Invoke(new ResultPack<Exception>
        //        //    {
        //        //        IsSucceeded = false,
        //        //        ReturnParam = ex,
        //        //        Message = "Error calling service"
        //        //    });
        //    }
        //}

        public static void AnonymousGetJson<T>(string actionURL, Dictionary<string, string> headers, WebApiEventHandler successCallback = null, WebApiExceptionEventHandler failedCallBack = null)
        {
            var process = new Task(new Action(async () =>
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        if (headers != null)
                            foreach (var header in headers)
                            {
                                client.DefaultRequestHeaders.Add(header.Key, header.Value);
                            }

                        //webApiResult = client.GetStringAsync(string.Format("{0}{1}", aimsServerAddress, actionURL)).Result;

                        var asyncResult = await client.GetAsync(string.Format("{0}{1}", aimsServerAddress, actionURL)).ConfigureAwait(false);
                        asyncResult.EnsureSuccessStatusCode();
                        string webApiResult = await asyncResult.Content.ReadAsStringAsync();

                        if (!string.IsNullOrWhiteSpace(webApiResult))
                        {
                            if (successCallback != null)
                                successCallback.Invoke(JsonConvert.DeserializeObject<T>(webApiResult), null);
                        }
                        else
                        if (failedCallBack != null)
                            failedCallBack.Invoke(new ResultPack<Exception>
                            {
                                IsSucceeded = false,
                                ReturnParam = new Exception(webApiResult),
                                Message = "Error calling service"
                            });
                    }
                }
                catch (Exception ex)
                {
                    if (failedCallBack != null)
                        failedCallBack.Invoke(new ResultPack<Exception>
                        {
                            IsSucceeded = false,
                            ReturnParam = ex,
                            Message = "Error calling service"
                        });
                }
            }));
            process.Start();
        }

        public void SignOutUser()
        {
            _clientToken = null;
            _tokenExpirationTime = DateTime.Now.Subtract(new TimeSpan(0, 0, 1));
        }


        public static ResultPack<CapacityRecord> GetCapacity(CapacityDataRequest capacityRequest)
        {
            if (_clientToken == null) // || string.IsNullOrWhiteSpace(_clientToken.RefreshToken))
                return new ResultPack<CapacityRecord>
                {
                    IsSucceeded = false,
                    Message = "client Token Null request received."
                };

            try
            {
                if (isExpired())
                    RefreshUser();

                using (var securedClient = new HttpClient())
                {
                    securedClient.DefaultRequestHeaders.Authorization
                                    = new AuthenticationHeaderValue("Bearer", _clientToken.AccessToken);

                    securedClient.DefaultRequestHeaders.Add("UId", _device.DeviceUniqueId);
                    securedClient.DefaultRequestHeaders.Add("Ver", Version);
                    securedClient.DefaultRequestHeaders.Add("Own", _device.OwnerSpecificName);
                    securedClient.DefaultRequestHeaders.Add("Nam", _device.DeviceName);
                    securedClient.DefaultRequestHeaders.Add("Mod", _device.DeviceModel);
                    securedClient.DefaultRequestHeaders.Add("Des", _device.Description);

                    HttpContent content = new StringContent(JsonConvert.SerializeObject(capacityRequest, jsonSerializerSettings), Encoding.UTF8, "application/json");

                    var rawResult = securedClient.PostAsync(string.Format("{0}capacity", aimsLicensingServiceAddress), content).Result;

                    if (!rawResult.IsSuccessStatusCode)
                    {
                        _message = rawResult.ReasonPhrase;
                        return new ResultPack<CapacityRecord>
                        {
                            IsSucceeded = false,
                            Message = _message
                        };
                    }

                    var data = rawResult.Content.ReadAsStringAsync().Result;

                    var result = JsonConvert.DeserializeObject<ResultPack<CapacityRecord>>(data);
                    if (!result.IsSucceeded || result.ReturnParam == null)
                    {
                        _message = "Could not retrieve the user CapacityRecord.\n" + result.Message;
                        return new ResultPack<CapacityRecord>
                        {
                            IsSucceeded = false,
                            Message = "Null request received."
                        };
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _message = ex.InnerException.Message;
                else _message = ex.Message;

                return new ResultPack<CapacityRecord>
                {
                    IsSucceeded = false,
                    Message = _message
                };
            }
        }

        public static ResultPack<List<CapacityRecord>> GetCapacities(List<CapacityDataRequest> capacityRequest)
        {
            if (_clientToken == null || string.IsNullOrWhiteSpace(_clientToken.RefreshToken))
                return new ResultPack<List<CapacityRecord>>
                {
                    IsSucceeded = false,
                    Message = "Null request received."
                };

            try
            {
                if (isExpired())
                    RefreshUser();

                using (var securedClient = new HttpClient())
                {
                    securedClient.DefaultRequestHeaders.Authorization
                                    = new AuthenticationHeaderValue("Bearer", _clientToken.AccessToken);

                    securedClient.DefaultRequestHeaders.Add("UId", _device.DeviceUniqueId);
                    securedClient.DefaultRequestHeaders.Add("Ver", Version);
                    securedClient.DefaultRequestHeaders.Add("Own", _device.OwnerSpecificName);
                    securedClient.DefaultRequestHeaders.Add("Nam", _device.DeviceName);
                    securedClient.DefaultRequestHeaders.Add("Mod", _device.DeviceModel);
                    securedClient.DefaultRequestHeaders.Add("Des", _device.Description);

                    HttpContent content = new StringContent(JsonConvert.SerializeObject(capacityRequest, jsonSerializerSettings), Encoding.UTF8, "application/json");

                    var rawResult = securedClient.PostAsync(string.Format("{0}capacities", aimsLicensingServiceAddress), content).Result;

                    if (!rawResult.IsSuccessStatusCode)
                    {
                        _message = rawResult.ReasonPhrase;
                        return new ResultPack<List<CapacityRecord>>
                        {
                            IsSucceeded = false,
                            Message = _message
                        };
                    }

                    var data = rawResult.Content.ReadAsStringAsync().Result;

                    var result = JsonConvert.DeserializeObject<ResultPack<List<CapacityRecord>>>(data);
                    if (!result.IsSucceeded || result.ReturnParam == null)
                    {
                        _message = "Could not retrieve the user Capacities Records.\n" + result.Message;
                        return new ResultPack<List<CapacityRecord>>
                        {
                            IsSucceeded = false,
                            Message = "Null request received."
                        };
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    _message = ex.InnerException.Message;
                else _message = ex.Message;

                return new ResultPack<List<CapacityRecord>>
                {
                    IsSucceeded = false,
                    Message = _message
                };
            }

        }

        #endregion
    }

    public delegate HttpClient LoadHttpClientNative();
}
