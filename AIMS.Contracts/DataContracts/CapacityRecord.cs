﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class CapacityRecord
    {
        public decimal ClientId { get; set; }
        public decimal LoungeId { get; set; }
        public int Capacity { get; set; }
        public int PAXAvgStayMinutes { get; set; }
    }
}
