﻿namespace AIMS.Contracts
{
    public class Position
    {
        public long Id { get; set; }
        public long? FenceId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double? Altitude { get; set; }
        public float? Accuracy { get; set; }
        public double MaxGpsFence { get; set; }
    }
}
