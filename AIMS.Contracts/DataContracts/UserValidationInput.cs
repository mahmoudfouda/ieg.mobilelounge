﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class UserValidationInput
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public Dictionary<string, object> Properties { get; set; }
    }
}
