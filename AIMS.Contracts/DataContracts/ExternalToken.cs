﻿namespace AIMS.Contracts.DataContracts
{
    public class ExternalToken
    {
        public string Username { get; set; }

        public string MobileKey { get; set; }

        public TokenResponse Token { get; set; }
    }
}