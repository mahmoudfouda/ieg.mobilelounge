﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class ConfigurationRequest : RequestBase
    {
        public string DeviceUniqueId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public decimal? ClientId { get; set; }

        public string MobileKey { get; set; }

        public string Name { get; set; }
    }
}
