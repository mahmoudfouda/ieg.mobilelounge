﻿using System;
using System.Runtime.Serialization;

namespace AIMS.Contracts
{
	public class ResultPack<T>
	{
        public ResultPack()
        {
            GeneratedDate = DateTime.Now;
        }

        [DataMember]
		public DateTime GeneratedDate { get; set;}

        [DataMember]
        public bool IsSucceeded { get; set;}

        [DataMember]
        public string Message { get; set;}

        [DataMember]
        public T ReturnParam { get; set; }

        [DataMember]
        public int? ErrorCode { get; set; }

        [DataMember]
        public string ErrorMetadata { get; set; }

        [DataMember]
        public Guid? UID {
			get;
			set;
		}
	}
}

