﻿using System;

namespace AIMS.Contracts.DataContracts
{
    public class ExceptionalItem
    {
        public Guid Id { get; set; }
        public Nullable<decimal> ClientId { get; set; }
        public string UserName { get; set; }
        public Nullable<bool> DisableGPSCheck { get; set; }
        public Nullable<bool> DisableDeviceCheck { get; set; }
        public Nullable<bool> UseTrialBarcodeScanner { get; set; }
        public Nullable<bool> AllowDeviceRegistration { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Registerar { get; set; }
        public string Modifier { get; set; }
        public string Description { get; set; }
    }
}
