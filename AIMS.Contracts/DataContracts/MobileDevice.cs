﻿using System;

namespace AIMS.Contracts.DataContracts
{
    public class MobileDevice
    {
        public string DeviceUniqueId { get; set; }
        public decimal? ClientId { get; set; }
        public Guid ServicePlanId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceVersion { get; set; }
        public string OwnerSpecificName { get; set; }
        public DateTime? LastUserActivityDate { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Registerar { get; set; }
        public string Modifier { get; set; }
        public string Description { get; set; }
    }
}
