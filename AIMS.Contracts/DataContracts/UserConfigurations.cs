﻿using System.Collections.Generic;

namespace AIMS.Contracts.DataContracts
{
    public class UserConfigurations
    {
        public string Username { get; set; }

        public string MobileKey { get; set; }

        public decimal ClientId { get; set; }

        public List<ConfigurationRecord> Configurations { get; set; }

        public List<ExternalToken> OtherTokens { get; set; }

        public string MobileServerURL { get; set; }

        public List<Fence> Fences { get; set; }

        public ExceptionalItem Exceptions { get; set; }

        public List<ServicePlan> ServicePlans { get; set; }

        public bool IsBlackListed { get; set; }

        public string Descriptions { get; set; }
    }
}
