﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class LoungeRecord<T> : LoungeIdentifier
    {
        public string LoungeName { get; set; }

        public string WorkstationName { get; set; }

        public int Capacity { get; set; }

        public List<T> OccupancyRecords { get; set; }
    }
}
