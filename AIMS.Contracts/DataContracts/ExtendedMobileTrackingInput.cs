﻿using AIMS.Contracts.AIMSClient;

namespace AIMS.Contracts.DataContracts
{
    public class ExtendedMobileTrackingInput
    {
        public MobileTrackingInput TrackingInput { get; set; }

        public string LocalTime { get; set; }

        public string UTCTime { get; set; }
    }
}
