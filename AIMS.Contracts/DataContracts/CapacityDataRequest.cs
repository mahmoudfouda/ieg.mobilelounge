﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class CapacityDataRequest : RequestBase
    {
        public CapacityDataRequest() : base()
        {
        }

        public decimal LoungeId { get; set; }

        public decimal ClientId { get; set; }
    }
}
