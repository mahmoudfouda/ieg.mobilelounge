﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class LoungeDataRequest : RequestBase
    {
        public LoungeDataRequest() : base()
        {
            FromUtcDate = FromLocalDate = DateTime.MinValue;
            ToUtcDate = ToLocalDate = DateTime.MaxValue;
            IntervalInSeconds = 60;
        }

        public int IntervalInSeconds { get; set; }

        //public IEnumerable<decimal> LoungeIDs { get; set; }

        //public IEnumerable<decimal> WorkstationIDs { get; set; }

        public decimal WorkstationId { get; set; }

        public decimal LoungeId { get; set; }

        public int? Capacity { get; set; }

        public int PAXAvgStayMinutes { get; set; }

        public string AirportCode { get; set; }

        public DateTime FromUtcDate { get; set; }

        public DateTime ToUtcDate { get; set; }

        public DateTime FromLocalDate { get; set; }

        public DateTime ToLocalDate { get; set; }
    }
}
