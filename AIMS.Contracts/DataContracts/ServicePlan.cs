﻿using System;
using System.Collections.Generic;

namespace AIMS.Contracts.DataContracts
{
    public class ServicePlan
    {
        public Guid Id { get; set; }
        public decimal? ClientId { get; set; }
        public string Name { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int DeviceQuantity { get; set; }
        public double? Price { get; set; }
        public bool IsActive { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Registerar { get; set; }
        public string Modifier { get; set; }
        public string Description { get; set; }

        public IEnumerable<MobileDevice> RegisteredDevices { get; set; }
    }
}
