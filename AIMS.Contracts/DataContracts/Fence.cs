﻿using System.Collections.Generic;

namespace AIMS.Contracts.DataContracts
{
    public class Fence
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IEnumerable<Position> Positions { get; set; }
    }
}
