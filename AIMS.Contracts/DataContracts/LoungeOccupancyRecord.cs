﻿using System;
using System.Runtime.Serialization;

namespace AIMS.Contracts.DataContracts
{
    public class LoungeOccupancyRecord
    {
        [DataMember]
        public decimal OccupancyPercentage { get; set; }

        [DataMember]
        public DateTimeOffset OccupancyTime { get; set; }

        [DataMember]
        public decimal OccupancyTotal { get; set; }

        [DataMember]
        public decimal OccupancyTypePassenger { get; set; }

        [DataMember]
        public bool IsGroup { get; set; }

        [DataMember]
        public decimal LoungeId { get; set; }
    }
}
