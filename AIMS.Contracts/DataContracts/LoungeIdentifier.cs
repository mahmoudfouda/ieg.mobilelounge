﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class LoungeIdentifier
    {
        public decimal ClientId { get; set; }
        public decimal LoungeId { get; set; }
    }
}
