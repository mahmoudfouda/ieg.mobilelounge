﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class RequestBase
    {
        public RequestBase()
        {
            UniqueId = Guid.NewGuid();
            UtcGeneratedTime = DateTime.UtcNow;
        }

        public DateTime UtcGeneratedTime { get; }
        public Guid UniqueId { get; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
    }
}
