﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.DataContracts
{
    public class MobileClientRequestClaims
    {
        public Position Position { get; set; }

        public string DeviceUniqueId { get; set; }//946783DAF07CBEFF73F49F977553C326C2C5A0C7
        public string DeviceSerialNumber { get; set; }//DLXS30UJGMLL
        public string DeviceName { get; set; }//John's Phone, Jane's iPad...
        public string DeviceBrand { get; set; }//Apple, Samsung, Microsoft, HTC, LG...
        public string DeviceModel { get; set; }//iPhone, Galaxy, Lumia...
        public string DeviceBuild { get; set; }//6s Plus, S6 edge, 950XL...
        public string DeviceOS { get; set; }//iOS, Android, Windows
        public string DeviceFirmwareVersion { get; set; }//iOS 10.1.1
        public string DeviceType { get; set; }//Phone, Tablet, TV, Watch

        public HttpClient SetHttpClientHeaders(HttpClient httpClient = null)
        {
            if (httpClient == null)
                httpClient = new HttpClient();
            if (Position != null)
            {
                httpClient.DefaultRequestHeaders.Add("Lat", Position.Latitude.ToString(CultureInfo.InvariantCulture));
                httpClient.DefaultRequestHeaders.Add("Lon", Position.Longitude.ToString(CultureInfo.InvariantCulture));
                httpClient.DefaultRequestHeaders.Add("Alt", Position.Altitude.HasValue ? Position.Altitude.Value.ToString(CultureInfo.InvariantCulture) : "0");
                httpClient.DefaultRequestHeaders.Add("Acc", Position.Accuracy.HasValue ? Position.Accuracy.Value.ToString(CultureInfo.InvariantCulture) : "0");
            }
            else {
                httpClient.DefaultRequestHeaders.Add("Lat", "0");
                httpClient.DefaultRequestHeaders.Add("Lon", "0");
                httpClient.DefaultRequestHeaders.Add("Alt", "0");
                httpClient.DefaultRequestHeaders.Add("Acc", "0");
            }
            httpClient.DefaultRequestHeaders.Add("UId", DeviceUniqueId);
            httpClient.DefaultRequestHeaders.Add("DSN", DeviceSerialNumber);
            httpClient.DefaultRequestHeaders.Add("DName", DeviceName);
            httpClient.DefaultRequestHeaders.Add("DBrand", DeviceBrand);
            httpClient.DefaultRequestHeaders.Add("DModel", DeviceModel);
            httpClient.DefaultRequestHeaders.Add("DBuild", DeviceBuild);
            httpClient.DefaultRequestHeaders.Add("DOS", DeviceOS);
            httpClient.DefaultRequestHeaders.Add("DFirm", DeviceFirmwareVersion);
            httpClient.DefaultRequestHeaders.Add("DType", DeviceType);

            return httpClient;
        }
    }
}
