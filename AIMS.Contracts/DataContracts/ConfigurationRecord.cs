﻿using System;

namespace AIMS.Contracts.DataContracts
{
    public class ConfigurationRecord
    {
        public long Id { get; set; }
        public string MobileKey { get; set; }
        public decimal ClientId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Registerar { get; set; }
        public string Modifier { get; set; }
        public string Description { get; set; }
    }
}
