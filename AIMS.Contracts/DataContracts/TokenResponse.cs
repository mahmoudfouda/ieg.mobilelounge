﻿using Newtonsoft.Json;
using System;

namespace AIMS.Contracts
{
    public class TokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonIgnore]
        public bool HasError { get { return !String.IsNullOrWhiteSpace(this.Error); } }
        [JsonIgnore]
        public bool IsSuccess { get { return !this.HasError; } }
    }
}
