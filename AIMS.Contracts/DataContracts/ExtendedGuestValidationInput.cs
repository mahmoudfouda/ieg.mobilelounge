﻿using AIMS.Contracts.AIMSClient;

namespace AIMS.Contracts.DataContracts
{
    public class ExtendedGuestValidationInput
    {
        public GuestValidationInput GuestValidationInput { get; set; }

        public string LocalTime { get; set; }

        public string UTCTime { get; set; }
    }
}
