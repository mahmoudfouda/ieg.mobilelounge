﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.Validators
{
    public delegate void TokenCallback(ResultPack<TokenResponse> tokenResult);
    public interface IMobileValidator
    {
        bool IsTokenExpired { get; }

        TokenResponse ExternalToken { get; }

        void Authenticate(string username, string password, string mobileKey, IWebApiServiceContext proxy, Position position, TokenCallback callback);

        void RefreshToken(string username, string mobileKey, Position position, TokenCallback callback);

        Task<ResultPack<TokenResponse>> Authenticate(string username, string password, string mobileKey, IWebApiServiceContext proxy, Position position);

        Task<ResultPack<TokenResponse>> RefreshToken(string username, string mobileKey, Position position);
    }
}
