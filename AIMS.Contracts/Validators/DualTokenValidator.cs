﻿using AIMS.Contracts.DataContracts;
using System;
using System.Threading.Tasks;

namespace AIMS.Contracts.Validators
{
//    internal class DualTokenValidator : IMobileValidator
//    {
//        private DateTime _tokenExpirationTime;
//        private IWebApiServiceContext _proxy;

//        public TokenResponse ExternalToken
//        {
//            get; private set;
//        }

//        public bool IsTokenExpired
//        {
//            get
//            {
//                if (_tokenExpirationTime == default(DateTime)) return false;
//                if (_tokenExpirationTime.CompareTo(DateTime.Now) <= 0) return true;
//                return false;
//            }
//        }

//        private void setRefreshTokenTime(string expiresIn)
//        {
//            int expIn = 0;
//            float realExpIn = 0;
//            float.TryParse(expiresIn, out realExpIn);
//            expIn = (int)Math.Floor(realExpIn);
//#if DEBUG
//            expIn = 60;
//#endif
//            if (expIn > 60)
//                _tokenExpirationTime = DateTime.Now.AddSeconds(expIn - 10);
//            else
//                _tokenExpirationTime = DateTime.Now.AddSeconds(expIn - Math.Ceiling((float)expIn / 5));
//        }

//        public void Authenticate(string username, string password, string mobileKey, IWebApiServiceContext proxy, Position position, TokenCallback callback)
//        {
//            var props = new System.Collections.Generic.Dictionary<string, object>();
//            props.Add("mobilekey", mobileKey);
//            if (proxy == null) return;
//            _proxy = proxy;

//            //proxy.GetJson<ResultPack<TokenResponse>>(string.Format("externalauthentication/{0}/{1}/{2}", username, password, mobileKey), (Contracts.Position)DefaultMobileApplication.Position, (extAuthResponse) =>
//            _proxy.SendJson<ResultPack<TokenResponse>>("externaltoken",
//                new UserValidationInput { Username = username, Password = password, Properties = props },
//                position,//(Contracts.Position)DefaultMobileApplication.Position,
//                (extAuthResponse) =>
//                {
//                    var res = (ResultPack<TokenResponse>)extAuthResponse;
//                    if (res.IsSucceeded)
//                    {
//                        ExternalToken = res.ReturnParam;
//                        if (ExternalToken != null)
//                            setRefreshTokenTime(ExternalToken.ExpiresIn);
//                        else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
//                    }
//                    else ExternalToken = null;

//                    if (callback != null)
//                        callback.Invoke(res);
//                },
//                (extAuthResponse) =>
//                {
//                    if (callback != null)
//                        callback.Invoke(new ResultPack<TokenResponse>
//                        {
//                            IsSucceeded = false,
//                            Message = extAuthResponse != null && extAuthResponse.ReturnParam != null ? extAuthResponse.ReturnParam.Message : "Unknown error in DualTokenValidator.Authenticate()"
//                        });
//                });
//        }

//        public void RefreshToken(string username, string mobileKey, Position position, TokenCallback callback)
//        {
//            var props = new System.Collections.Generic.Dictionary<string, object>();
//            props.Add("mobilekey", mobileKey);
//            //proxy.GetJson<ResultPack<TokenResponse>>(string.Format("refreshexternaltoken/{0}/{1}", username, mobileKey), (Contracts.Position)DefaultMobileApplication.Position, (extAuthResponse) =>
//            _proxy.SendJson<ResultPack<TokenResponse>>("externaltokenrefresh",
//                new UserValidationInput { Username = username, Properties = props },
//                position,//(Contracts.Position)DefaultMobileApplication.Position,
//                (extAuthResponse) =>
//                {
//                    var res = (ResultPack<TokenResponse>)extAuthResponse;
//                    if (res.IsSucceeded)
//                    {
//                        ExternalToken = res.ReturnParam;
//                        if (ExternalToken != null)
//                            setRefreshTokenTime(ExternalToken.ExpiresIn);
//                        else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
//                    }
//                    //else ExternalToken = null;//Maybe we can try again

//                    if (callback != null)
//                        callback.Invoke(res);
//                },
//                (extAuthResponse) =>
//                {
//                    if (callback != null)
//                        callback.Invoke(new ResultPack<TokenResponse>
//                        {
//                            IsSucceeded = false,
//                            Message = extAuthResponse.ReturnParam.Message
//                        });
//                });
//        }

//        public async Task<ResultPack<TokenResponse>> Authenticate(string username, string password, string mobileKey, IWebApiServiceContext proxy, Position position)
//        {
//            var props = new System.Collections.Generic.Dictionary<string, object>();
//            props.Add("mobilekey", mobileKey);
//            if (proxy == null)
//                return new ResultPack<TokenResponse>
//                {
//                    IsSucceeded = false,
//                    Message = "The proxy is null"
//                };
//            _proxy = proxy;

//            try
//            {
//                var res = await _proxy.SendJsonAsync<ResultPack<TokenResponse>>("externaltoken",
//                    new UserValidationInput { Username = username, Password = password, Properties = props },
//                    position//(Contracts.Position)DefaultMobileApplication.Position
//                    ).ConfigureAwait(false);

//                if (res.IsSucceeded)
//                {
//                    ExternalToken = res.ReturnParam;
//                    if (ExternalToken != null)
//                        setRefreshTokenTime(ExternalToken.ExpiresIn);
//                    else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
//                }
//                else ExternalToken = null;

//                return res;
//            }
//            catch (Exception ex)
//            {
//                ExternalToken = null;
//                return new ResultPack<TokenResponse>
//                {
//                    IsSucceeded = false,
//                    Message = string.Format("Exception in DualTokenValidator.Authenticate(): {0}", ex.StackTrace)
//                };
//            }
//        }

//        public async Task<ResultPack<TokenResponse>> RefreshToken(string username, string mobileKey, Position position)
//        {
//            var props = new System.Collections.Generic.Dictionary<string, object>();
//            props.Add("mobilekey", mobileKey);

//            try
//            {
//                var res = await _proxy.SendJsonAsync<ResultPack<TokenResponse>>("externaltokenrefresh",
//                    new UserValidationInput { Username = username, Properties = props },
//                    position//(Contracts.Position)DefaultMobileApplication.Position
//                    ).ConfigureAwait(false);

//                if (res.IsSucceeded)
//                {
//                    ExternalToken = res.ReturnParam;
//                    if (ExternalToken != null)
//                        setRefreshTokenTime(ExternalToken.ExpiresIn);
//                    else _tokenExpirationTime = DateTime.MaxValue;//TODO: if we don't need a token
//                }

//                return res;
//            }
//            catch (Exception ex)
//            {
//                return new ResultPack<TokenResponse>
//                {
//                    IsSucceeded = false,
//                    Message = string.Format("Exception in DualTokenValidator.RefreshToken(): {0}", ex.StackTrace)
//                };
//            }
//        }
//    }
}
