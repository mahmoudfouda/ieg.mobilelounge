﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace AIMS.Contracts.Security
{
    public class MobileIdentity : IIdentity
    {
        public MobileIdentity(string authenticationType, string username)
        {
            this.AuthenticationType = authenticationType;
            this.Name = username;
        }

        public string AuthenticationType { get; private set; }

        public bool IsAuthenticated => !String.IsNullOrWhiteSpace(this.Name);

        public string Name { get; private set; }
    }
}
