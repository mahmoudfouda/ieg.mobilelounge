﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace AIMS.Contracts.Security
{
    public class MobilePrincipal : IPrincipal
    {
        #region Constants (TODO: Move to Ieg.Mobile.Contracts)
        private const string Role_Key = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
        private const string ClientId_Key = "AIMS_CLIENT_ID";
        private const string MobileKey_Key = "MOBILE_KEY";
        private const string LoungeId_Key = "AIMS_LOUNGE_ID";
        private const string FirstName_Key = "given_name";
        private const string LastName_Key = "family_name";
        private const string DualTokenMobileKeys_Key = "DUAL_TOKEN_MOBILE_KEYS";
        #endregion

        private IEnumerable<MobileClaim> Claims;

        private IEnumerable<string> Scopes;

        public string MobileKey { get; private set; }

        public string DualTokenMobileKeysString { get; private set; }

        public string LoungeIDs { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public decimal ClientId { get; private set; }

        public IEnumerable<string> Roles { get; private set; }

        public MobilePrincipal(IEnumerable<MobileClaim> claims)
        {
            if(claims == null)
            {
                throw new ArgumentNullException(nameof(claims));
            }

            var issuerClaim = claims.FirstOrDefault(x => x.Key.Equals("iss"));
            var usernameClaim = claims.FirstOrDefault(x => x.Key.Equals("sub"));

            if (issuerClaim == null || issuerClaim == null)
            {
                throw new ArgumentException("Missing claims in order to have a proper identity");
            }


            #region Flexible Claims

            #region Scopes
            var claim = claims.FirstOrDefault(x => x.Key.Equals("scope"));
            if (claim == null)
            {
                Scopes = new List<string>();
            }
            else
            {
                try
                {
                    Scopes = JsonConvert.DeserializeObject<List<string>>(claim.Value);
                }
                catch
                {
                    Scopes = new List<string>() { claim.Value };
                }
            }
                
                 
            #endregion


            #region First and Last name
            claim = claims.FirstOrDefault(x => x.Key.Equals(FirstName_Key));
            if (claim == null)
            {
                FirstName = "";
            }
            else FirstName = claim.Value;

            claim = claims.FirstOrDefault(x => x.Key.Equals(LastName_Key));
            if (claim == null)
            {
                LastName = "";
            }
            else LastName = claim.Value;
            #endregion


            #region LoungeIds, MobileKey and ClientId
            claim = claims.FirstOrDefault(x => x.Key.Equals(MobileKey_Key));
            if (claim == null)
            {
                MobileKey = "";
            }
            else MobileKey = claim.Value;

            claim = claims.FirstOrDefault(x => x.Key.Equals(LoungeId_Key));
            if (claim == null)
            {
                LoungeIDs = "";
            }
            else LoungeIDs = claim.Value;

            claim = claims.FirstOrDefault(x => x.Key.Equals(ClientId_Key));
            if (claim == null)
            {
                ClientId = 0;
            }
            else
            {
                decimal aimsClientId = 0;
                if (decimal.TryParse(claim.Value, out aimsClientId))
                    ClientId = aimsClientId;
            }
            #endregion


            #region Roles
            claim = claims.FirstOrDefault(x => x.Key.Equals(Role_Key));
            if (claim == null || string.IsNullOrWhiteSpace(claim.Value))
            {
                Roles = new List<string>();
            }
            else
            {
                Roles = claim.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            #endregion

            #region DualTokenMobileKeys
            claim = claims.FirstOrDefault(x => x.Key.Equals(DualTokenMobileKeys_Key));
            if (claim == null || string.IsNullOrWhiteSpace(claim.Value))
            {
                DualTokenMobileKeysString = "";
            }
            else
            {
                DualTokenMobileKeysString = claim.Value;
            }
            #endregion

            #endregion

            Claims = claims;
            Identity = new MobileIdentity(issuerClaim.Value, usernameClaim.Value);
        }

        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role)
        {
            //Probably multiple roles are to be handeled here
            //return Claims.Where(x => x.Key.Equals(RoleKey)).Any(x => x.Value.Equals(role));
            return Roles.Any(x => x.Equals(role));
        }

        public bool HasScope(string scope)
        {
            if (Scopes.Count() == 0) return false;

            return Scopes.Any(x => x.Equals(scope));
        }
    }
}
