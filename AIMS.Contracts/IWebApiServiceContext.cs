﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AIMS.Contracts
{
    public delegate void WebApiEventHandler(object response, Dictionary<string, Dictionary<string, string>> log = null);

    public delegate void WebAppCenterApiEventHandler(Dictionary<string,Dictionary<string,string>> response);

    public delegate void WebApiExceptionEventHandler(ResultPack<Exception> exception, Dictionary<string, Dictionary<string, string>> log = null);
    public interface IWebApiServiceContext : IDisposable
    {
        //event WebApiEventHandler OnRequestCompleted;
        //event WebApiExceptionEventHandler OnErrorOccured;
        bool IsAuthenticated { get; }
        bool IsExpired { get; }
        string Message { get; }
        
        void SendJson<T>(string action, object jsonObject, Position location, WebApiEventHandler successCallback, WebApiExceptionEventHandler failedCallBack);

        void GetString(string actionUrl, Position location, WebApiEventHandler successCallback, WebApiExceptionEventHandler failedCallBack);
        void GetJson<T>(string action, Position location, WebApiEventHandler successCallback, WebApiExceptionEventHandler failedCallBack);

        Task<T> SendJsonAsync<T>(string actionURL, object jsonObject, Position position);
        Task<T> GetJsonAsync<T>(string actionURL, Position position);

        void SignOutUser();
    }
}
